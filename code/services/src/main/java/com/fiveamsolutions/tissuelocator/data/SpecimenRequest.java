/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.AssertTrue;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.dynamicextensions.ExtendableEntity;
import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.Deletable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.Searchable;
import com.fiveamsolutions.tissuelocator.data.annotation.OnSave;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableUtil;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenRequestMTACertification;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenRequestUniqueReviewer;
import com.fiveamsolutions.tissuelocator.data.validation.ValidExtensions;
import com.fiveamsolutions.tissuelocator.util.RequestStatusTransitionHistoryHandler;

/**
 * @author smiller
 */
@Entity
@Table(name = "specimen_request")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@SpecimenRequestUniqueReviewer
@ValidExtensions
@SuppressWarnings({ "PMD.TooManyFields", "PMD.ExcessiveClassLength", "PMD.SingularField" })
public class SpecimenRequest implements PersistentObject, Auditable, AccessRestricted, Deletable, ExtendableEntity,
StatusTransitionable<RequestStatusTransition, RequestStatus> {

    private static final long serialVersionUID = 1L;
    private static final int MAX_COMMENT_LENGTH = 1000;
    private static final int MAX_MTA_CERTIFICATION_LENGTH = 255;
    private static final String REQUEST_ID = "request_id";

    private Long id;
    private Set<SpecimenRequestLineItem> lineItems = new HashSet<SpecimenRequestLineItem>();
    private Set<AggregateSpecimenRequestLineItem> aggregateLineItems =
        new LinkedHashSet<AggregateSpecimenRequestLineItem>();
    private List<ReviewComment> comments = new ArrayList<ReviewComment>();
    private TissueLocatorUser requestor;
    private Date updatedDate = new Date();
    private Date requestedDate = new Date();
    private Date nextReminderDate;
    private RequestStatus status;
    private RequestStatus previousStatus;
    private List<RequestStatusTransition> statusTransitionHistory
        = new ArrayList<RequestStatusTransition>();
    private Date statusTransitionDate;
    private String revisionComment;
    private String externalComment;
    private RequestStatus finalVote;
    private String prospectiveCollectionNotes;
    private Study study = new Study();
    private PrincipalInvestigator investigator = new PrincipalInvestigator();
    private ShippingInformation shipment = new ShippingInformation();
    private String mtaCertification;
    private String generalComment;

    private Set<SpecimenRequestReviewVote> consortiumReviews
        = new HashSet<SpecimenRequestReviewVote>();
    private Set<SpecimenRequestReviewVote> institutionalReviews =
        new HashSet<SpecimenRequestReviewVote>();
    private Set<Institution> reviewers = new HashSet<Institution>();
    private Set<Shipment> orders = new HashSet<Shipment>();
    private Map<String, Object> customProperties = new HashMap<String, Object>();

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the lineItems
     */
    @ManyToMany
    @Valid
    @JoinTable(name = "request_line_items",
                joinColumns = @JoinColumn(name = REQUEST_ID),
                inverseJoinColumns = @JoinColumn(name = "line_item_id"))
    @ForeignKey(name = "LINE_ITEM_REQUEST_FK",
                inverseName = "REQUEST_LINE_ITEM_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = CascadeType.ALL)
    @OrderBy(value = "id")
    public Set<SpecimenRequestLineItem> getLineItems() {
        return lineItems;
    }

    /**
     * @param lineItems the lineItems to set
     */
    public void setLineItems(Set<SpecimenRequestLineItem> lineItems) {
        this.lineItems = lineItems;
    }

    /**
     * @return the aggregateLineItems
     */
    @ManyToMany
    @Valid
    @JoinTable(name = "request_aggregate_line_items",
                joinColumns = @JoinColumn(name = REQUEST_ID),
                inverseJoinColumns = @JoinColumn(name = "line_item_id"))
    @ForeignKey(name = "AGGREGATE_LINE_ITEM_REQUEST_FK",
                inverseName = "REQUEST_AGGREGATE_LINE_ITEM_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = CascadeType.ALL)
    @OrderBy(value = "id")
    public Set<AggregateSpecimenRequestLineItem> getAggregateLineItems() {
        return aggregateLineItems;
    }

    /**
     * @param aggregateLineItems the aggregateLineItems to set
     */
    public void setAggregateLineItems(Set<AggregateSpecimenRequestLineItem> aggregateLineItems) {
        this.aggregateLineItems = aggregateLineItems;
    }

    /**
     * @return the comments
     */
    @Fetch(FetchMode.SELECT)
    @ManyToMany
    @Valid
    @JoinTable(name = "request_comments",
                joinColumns = @JoinColumn(name = REQUEST_ID),
                inverseJoinColumns = @JoinColumn(name = "comment_id"))
    @ForeignKey(name = "COMMENT_REQUEST_FK",
                inverseName = "REQUEST_COMMENT_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = CascadeType.ALL)
    @OrderBy(value = "date")
    public List<ReviewComment> getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(List<ReviewComment> comments) {
        this.comments = comments;
    }

    /**
     * @return the requestor
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "requestor_id")
    @ForeignKey(name = "request_requestor_fk")
    @Index(name = "request_requestor_idx")
    @Searchable
    public TissueLocatorUser getRequestor() {
        return requestor;
    }

    /**
     * @param requestor the requestor to set
     */
    public void setRequestor(TissueLocatorUser requestor) {
        this.requestor = requestor;
    }

    /**
     * @return the updatedDate
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * @param updatedDate the updatedDate to set
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * @return the requestedDate
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "requested_date")
    public Date getRequestedDate() {
        return requestedDate;
    }

    /**
     * @param requestedDate the requestedDate to set
     */
    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }

    /**
     * @return the nextReminderDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "next_reminder_date")
    public Date getNextReminderDate() {
        return nextReminderDate;
    }

    /**
     * @param nextReminderDate the nextReminderDate to set
     */
    public void setNextReminderDate(Date nextReminderDate) {
        this.nextReminderDate = nextReminderDate;
    }

    /**
     * @return the status
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    @Searchable
    @Index(name = "specimen_request_status_idx")
    public RequestStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(RequestStatus status) {
        this.status = status;
        boolean isValidationDisabled = status != null && status.equals(RequestStatus.DRAFT);
        DisableableUtil.setValidationDisabled(isValidationDisabled);
    }

    /**
     * @return the specimen's status
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "status", updatable = false, insertable = false, nullable = false)
    public RequestStatus getPreviousStatus() {
        return previousStatus;
    }

    /**
     * @param previousStatus the status to set
     */
    public void setPreviousStatus(RequestStatus previousStatus) {
        this.previousStatus = previousStatus;
    }

    /**
     * @return the status transitions.
     */
    @Fetch(FetchMode.SELECT)
    @ManyToMany
    @Valid
    @JoinTable(name = "request_status_transition_history",
                joinColumns = @JoinColumn(name = "request_id"),
                inverseJoinColumns = @JoinColumn(name = "status_transition_id"))
    @ForeignKey(name = "REQUEST_TRANSITION_FK",
                inverseName = "TRANSITION_REQUEST_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = CascadeType.ALL)
    @OrderBy(value = "systemTransitionDate")
    public List<RequestStatusTransition> getStatusTransitionHistory() {
        return statusTransitionHistory;
    }

    /**
     * @param statusTransitionHistory the status transitions.
     */
    public void setStatusTransitionHistory(
            List<RequestStatusTransition> statusTransitionHistory) {
        this.statusTransitionHistory = statusTransitionHistory;
    }


    /**
     * @return the date of the transition to the current status, if not the current date.
     */
    @Transient
    public Date getStatusTransitionDate() {
        return statusTransitionDate;
    }

    /**
     * {@inheritDoc}
     */
    public void setStatusTransitionDate(Date statusTransitionDate) {
        this.statusTransitionDate = statusTransitionDate;
    }

    /**
     * Update the status transition history based on the current and previous status.
     */
    @OnSave
    public void updateStatusTransitionHistory() {
        for (SpecimenRequestLineItem lineItem : getLineItems()) {
            lineItem.getSpecimen().updateStatusTransitionHistory();
        }
        new RequestStatusTransitionHistoryHandler().updateStatusTransitionHistory(this);
    }

    /**
     * @return the revisionComment
     */
    @Column(name = "revision_comment")
    @Length(max = MAX_COMMENT_LENGTH)
    public String getRevisionComment() {
        return revisionComment;
    }

    /**
     * @param revisionComment the revisionComment to set
     */
    public void setRevisionComment(String revisionComment) {
        this.revisionComment = revisionComment;
    }

    /**
     * @return the externalComment
     */
    @Column(name = "external_comment")
    @Length(max = MAX_COMMENT_LENGTH)
    public String getExternalComment() {
        return externalComment;
    }

    /**
     * @param externalComment the externalComment to set
     */
    public void setExternalComment(String externalComment) {
        this.externalComment = externalComment;
    }

    /**
     * @return the finalVote
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "final_vote")
    public RequestStatus getFinalVote() {
        return finalVote;
    }

    /**
     * @param finalVote the finalVote to set
     */
    public void setFinalVote(RequestStatus finalVote) {
        this.finalVote = finalVote;
    }

    /**
     * @return the prospectiveCollectionNotes
     */
    @Length(max = MAX_COMMENT_LENGTH)
    @Column(name = "prospective_collection_notes")
    public String getProspectiveCollectionNotes() {
        return prospectiveCollectionNotes;
    }

    /**
     * @param prospectiveCollectionNotes the prospectiveCollectionNotes to set
     */
    public void setProspectiveCollectionNotes(String prospectiveCollectionNotes) {
        this.prospectiveCollectionNotes = prospectiveCollectionNotes;
    }

    /**
     * @return the study
     */
    @Valid
    @NotNull
    @ManyToOne
    @JoinColumn(name = "study_id")
    @ForeignKey(name = "specimen_request_study_fk")
    @Index(name = "specimen_request_study_idx")
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    public Study getStudy() {
        return study;
    }

    /**
     * Checks whether the study IRB approval expiration date is in the future.
     *
     * @return true if the expiration date is in the future otherwise false.
     */
    @Transient
    @AssertTrue(message = "IRB Approval Expiration Date must be a future date")
    public boolean isStudyIrbApprovalExpirationDateValid() {
        boolean isRequestUndecided =
            status == null
                || status.equals(RequestStatus.DRAFT)
                || status.equals(RequestStatus.PENDING)
                || status.equals(RequestStatus.PENDING_REVISION);

        if (isRequestUndecided && study != null
                && IrbApprovalStatus.APPROVED.equals(study.getIrbApprovalStatus())
                && study.getIrbApprovalExpirationDate() != null) {
            return study.getIrbApprovalExpirationDate().after(new Date());
        }

        return true;
    }

    /**
     * @param study the study to set
     */
    public void setStudy(Study study) {
        this.study = study;
    }

    /**
     * @return the investigator
     */
    @Valid
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "principal_investigator_id")
    @ForeignKey(name = "specimen_request_pi_fk")
    @Index(name = "specimen_request_pi_idx")
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    public PrincipalInvestigator getInvestigator() {
        return investigator;
    }

    /**
     * @param investigator the investigator to set
     */
    public void setInvestigator(PrincipalInvestigator investigator) {
        this.investigator = investigator;
    }

    /**
     * @return the shipment
     */
    @Valid
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shipment_id")
    @ForeignKey(name = "specimen_request_shipment_fk")
    @Index(name = "specimen_request_shipment_idx")
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    public ShippingInformation getShipment() {
        return shipment;
    }

    /**
     * @param shipment the shipment to set
     */
    public void setShipment(ShippingInformation shipment) {
        this.shipment = shipment;
    }

    /**
     * @return the votes
     */
    @ManyToMany
    @Valid
    @JoinTable(name = "request_consortium_review",
                joinColumns = @JoinColumn(name = REQUEST_ID),
                inverseJoinColumns = @JoinColumn(name = "review_id"))
    @ForeignKey(name = "CONSORTIUM_REVIEW_REQUEST_FK",
                inverseName = "REQUEST_CONSORTIUM_REVIEW_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @OrderBy(value = "institution")
    public Set<SpecimenRequestReviewVote> getConsortiumReviews() {
        return consortiumReviews;
    }

    /**
     * @param votes the votes to set
     */
    public void setConsortiumReviews(Set<SpecimenRequestReviewVote> votes) {
        consortiumReviews = votes;
    }

    /**
     * @return the reviewers
     */
    @ManyToMany
    @Valid
    @JoinTable(name = "request_reviewers",
               joinColumns = @JoinColumn(name = REQUEST_ID),
               inverseJoinColumns = @JoinColumn(name = "institution_id"))
    @ForeignKey(name = "INSTITUTION_REQUEST_FK",
                inverseName = "REQUEST_INSTITUTION_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = CascadeType.SAVE_UPDATE)
    public Set<Institution> getReviewers() {
        return reviewers;
    }

    /**
     * @param reviewers the reviewers to set
     */
    public void setReviewers(Set<Institution> reviewers) {
        this.reviewers = reviewers;
    }

    /**
     * @return the institutionalReviews
     */
    @ManyToMany
    @Valid
    @JoinTable(name = "request_inst_review",
                joinColumns = @JoinColumn(name = REQUEST_ID),
                inverseJoinColumns = @JoinColumn(name = "review_id"))
    @ForeignKey(name = "INST_REVIEW_REQUEST_FK",
                inverseName = "REQUEST_INST_REVIEW_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @OrderBy(value = "institution")
    public Set<SpecimenRequestReviewVote> getInstitutionalReviews() {
        return institutionalReviews;
    }

    /**
     * @param institutionalReviews the institutionalReviews to set
     */
    public void setInstitutionalReviews(Set<SpecimenRequestReviewVote> institutionalReviews) {
        this.institutionalReviews = institutionalReviews;
    }

    /**
     * @return the orders
     */
    @OneToMany(mappedBy = "request")
    @OrderBy(value = "id")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Set<Shipment> getOrders() {
        return orders;
    }

    /**
     * @param orders the orders to set
     */
    public void setOrders(Set<Shipment> orders) {
        this.orders = orders;
    }

    /**
     * @return the customProperties
     */
    @Transient
    public Map<String, Object> getCustomProperties() {
        return customProperties;
    }

    /**
     * @param customProperties the customProperties to set
     */
    public void setCustomProperties(Map<String, Object> customProperties) {
        this.customProperties = customProperties;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public Object getCustomProperty(String name) {
        return getCustomProperties().get(name);
    }

    /**
     * {@inheritDoc}
     */
    public void setCustomProperty(String name, Object value) {
        getCustomProperties().put(name, value);
    }

    /**
     * Get the line items for the orders associated with this request.
     * @return a collection of the line items for the orders associated with this request.
     */
    @Transient
    public Collection<SpecimenRequestLineItem> getOrderLineItems() {
        Collection<SpecimenRequestLineItem> specimens = new HashSet<SpecimenRequestLineItem>();
        for (Shipment order : getOrders()) {
            specimens.addAll(order.getLineItems());
        }
        return specimens;
    }

    /**
     * Get the aggregate line items that have been reviewed.
     * @return a collection of the aggregate line items that have been reviewed.
     */
    @Transient
    public Collection<AggregateSpecimenRequestLineItem> getReviewedAggregateLineItems() {
        Collection<AggregateSpecimenRequestLineItem> reviewedLineItems =
            new HashSet<AggregateSpecimenRequestLineItem>();
        for (AggregateSpecimenRequestLineItem lineItem : getAggregateLineItems()) {
            if (lineItem.getVote() != null && lineItem.getVote().getVote() != null) {
                reviewedLineItems.add(lineItem);
            }
        }
        return reviewedLineItems;
    }

    /**
     * Get the aggregate line items that have been reviewed.
     * @return a collection of the aggregate line items that have been reviewed.
     */
    @Transient
    public Collection<AggregateSpecimenRequestLineItem> getUnreviewedAggregateLineItems() {
        Collection<AggregateSpecimenRequestLineItem> unreviewedLineItems =
            new HashSet<AggregateSpecimenRequestLineItem>();
        for (AggregateSpecimenRequestLineItem lineItem : getAggregateLineItems()) {
            if (lineItem.getVote() == null || lineItem.getVote().getVote() == null) {
                unreviewedLineItems.add(lineItem);
            }
        }
        return unreviewedLineItems;
    }

    /**
     * Get the aggregate line items for the orders associated with this request.
     * @return a collection of the aggregate line items for the orders associated with this request.
     */
    @Transient
    public Collection<AggregateSpecimenRequestLineItem> getOrderAggregateLineItems() {
        Collection<AggregateSpecimenRequestLineItem> lis =
            new HashSet<AggregateSpecimenRequestLineItem>();
        for (Shipment order : getOrders()) {
            lis.addAll(order.getAggregateLineItems());
        }
        return lis;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isAccessible(TissueLocatorUser user) {
        if (getRequestor().getUsername().equals(user.getUsername())) {
            return true;
        }
        if (user.inRole(Role.TISSUE_REQUEST_ADMIN)) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public boolean isDeletable() {
        return id != null && status.equals(RequestStatus.DRAFT);
    }

    /**
     * @return the mta certification text.
     */
    @Column(name = "mta_certification")
    @Length(max = MAX_MTA_CERTIFICATION_LENGTH)
    @SpecimenRequestMTACertification
    public String getMtaCertification() {
        return mtaCertification;
    }

    /**
     * @param mtaCertification the mta certification text.
     */
    public void setMtaCertification(String mtaCertification) {
        this.mtaCertification = mtaCertification;
    }

    /**
     * @return the generalComment
     */
    @Column(name = "general_comment")
    @Length(max = MAX_COMMENT_LENGTH)
    public String getGeneralComment() {
        return generalComment;
    }

    /**
     * @param generalComment the generalComment to set
     */
    public void setGeneralComment(String generalComment) {
        this.generalComment = generalComment;
    }
}
