/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import javax.ejb.Local;
import javax.mail.MessagingException;

import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;

/**
 * @author ddasgupta
 *
 */
@Local
public interface FullConsortiumReviewServiceLocal extends SpecimenRequestServiceLocal {

    /**
     * Save a request with a new assignee for an institution, and send an email to the assignee.
     * @param request the request to assign
     * @param assignee the assignee
     * @param votingPeriod the length of the voting period
     * @param reviewPeriod the length of the review period
     * @throws MessagingException on error sending assignment email
     */
    void assignRequest(SpecimenRequest request, TissueLocatorUser assignee, int votingPeriod,
            int reviewPeriod) throws MessagingException;

    /**
     * Save a request with a new comment, and notify all consortium voters.
     * @param request the request to save
     * @param votingPeriod the length of the voting period
     * @throws MessagingException on error sending assignment email
     */
    void addComment(SpecimenRequest request, int votingPeriod) throws MessagingException;
}
