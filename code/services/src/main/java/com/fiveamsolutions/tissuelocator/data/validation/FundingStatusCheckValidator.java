/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.validation;

import java.io.Serializable;

import org.hibernate.mapping.Property;
import org.hibernate.validator.PropertyConstraint;
import org.hibernate.validator.Validator;

import com.fiveamsolutions.tissuelocator.data.FundingStatus;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * @author zmelnick
 *
 */
public class FundingStatusCheckValidator implements
Validator<FundingStatusCheck>, PropertyConstraint, Serializable {

    private static final long serialVersionUID = -4814230003348411169L;
    @Inject
    private static Provider<ApplicationSettingServiceLocal> appSettingServiceProvider;
    private static FundingStatus minimumFunding;


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid(Object value) {
        if (validateFundingStatus((FundingStatus) value) || DisableableUtil.isValidationDisabled()) {
            return true;
        }
        return false;
    }


    /**
     * @param localValue the funding status passed in through the application.
     * @return true if above minimum funding status, false otherwise.
     */
    private static synchronized boolean validateFundingStatus(FundingStatus localValue) {
        if (minimumFunding == null) {
            minimumFunding = appSettingServiceProvider.get().getMinimunRequiredFundingStatus();
        }


        if (minimumFunding == FundingStatus.FUNDED) {
            return localValue == FundingStatus.FUNDED ? true : false;
        } else if (minimumFunding == FundingStatus.PENDING) {
            return localValue == FundingStatus.NOT_FUNDED ? false : true;
        } else {
            return true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(FundingStatusCheck parameters) {
        // do nothing
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void apply(Property property) {
        //do nothing
    }

    /**
     * Allows the minimum funding status setting to be reset.
     * Should only be used for unit testing.
     */
    public static void reset() {
        minimumFunding = null;
    }
}
