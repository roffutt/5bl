/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import com.fiveamsolutions.tissuelocator.util.Email;

/**
 * @author smiller
 *
 */
@SuppressWarnings({"PMD.ExcessiveParameterList" })
public class NotEnoughVotesException extends Exception {

    private static final long serialVersionUID = 1L;

    private final boolean consortiumReviewComplete;
    private final boolean institutionalReviewComplete;
    private final boolean voteDeadLocked;
    private final Email email;

    /**
     * Construct the exception.
     * @param consortiumReviewComplete true or false
     * @param institutionalReviewComplete true or false
     * @param voteDeadlocked true or false
     * @param email the email to send
     */
    public NotEnoughVotesException(boolean consortiumReviewComplete, boolean institutionalReviewComplete, 
            boolean voteDeadlocked, Email email) {
        this.consortiumReviewComplete = consortiumReviewComplete;
        this.institutionalReviewComplete = institutionalReviewComplete;
        this.voteDeadLocked = voteDeadlocked;
        this.email = email;
    }

    /**
     * @return the consortiumReviewComplete
     */
    public boolean isConsortiumReviewComplete() {
        return consortiumReviewComplete;
    }

    /**
     * @return the institutionalReviewComplete
     */
    public boolean isInstitutionalReviewComplete() {
        return institutionalReviewComplete;
    }
    
    /**
     * @return the voteDeadLocked
     */
    public boolean isVoteDeadLocked() {
        return voteDeadLocked;
    }
    
    /**
     * @return the email
     */
    public Email getEmail() {
        return this.email;
    }
}
