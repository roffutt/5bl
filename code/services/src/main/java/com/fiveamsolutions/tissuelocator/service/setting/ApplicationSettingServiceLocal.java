/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.setting;

import java.util.Date;

import javax.ejb.Local;

import com.fiveamsolutions.tissuelocator.data.ApplicationSetting;
import com.fiveamsolutions.tissuelocator.data.FundingStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;

/**
 * @author ddasgupta
 */
@Local
@SuppressWarnings("PMD.TooManyMethods")
public interface ApplicationSettingServiceLocal extends GenericServiceLocal<ApplicationSetting> {

    /**
     * Get the number of specimen request reviewers.
     * @return the number of specimen request reviewers
     */
    int getNumberOfReviewers();

    /**
     * Get the number of days before an email reminder should be sent reminding people to
     *  mark the shipment as received. 0 means never send a reminder.
     * @return the grace period in days.
     */
    int getShipmentReceiptGracePeriod();

    /**
     * Get the number of days before an email reminder should be sent reminding line item
     * reviewers to review a specimen request. 0 means never send a reminder.
     * @return the grace period in days.
     */
    int getLineItemReviewGracePeriod();

    /**
     * Get the time at which the scheduled reminder thread should start.
     * @return the time at which the scheduled reminder thread should start.
     */
    Date getScheduledReminderThreadStartTime();

    /**
     * Get whether the PI legal fields should be displayed.
     * @return whether the PI legal fields should be displayed
     */
    boolean isDisplayPiLegalFields();

    /**
     * Get whether the prospective collection field should be displayed.
     * @return whether the prospective collection field should be displayed
     */
    boolean isDisplayProspectiveCollection();

    /**
     * Get whether consortium member institutions should be displayed
     * in the registration page.
     * @return whether consortium member institutions should be displayed in the registration page.
     */
    boolean isDisplayConsortiumRegistration();

    /**
     * Get whether the user can choose between simple and advanced search on the specimen
     * search page.
     * @return whether the user can choose between simple and advanced search on the specimen search page.
     */
    boolean isSimpleSearchEnabled();

    /**
     * Returns whether tabbed search on the specimen search page, and sectioned categories on the specimen view/edit
     * pages, views should be displayed.
     *
     * @return true if tabbed search on the specimen search page, and sectioned categories on the specimen view/edit
     * pages, views should be displayed; false otherwise.
     */
    boolean isTabbedSearchAndSectionedCategoriesViewsEnabled();

    /**
     * Get the widget in the left position on the public browse page.
     * @return widget fullname that should be in the left position.
     */
    String getLeftBrowseWidget();

    /**
     * Get the widget in the center position on the public browse page.
     * @return widget fullname that should be in the center position.
     */
    String getCenterBrowseWidget();

    /**
     * Get the widget in the right position on the public browse page.
     * @return widget fullname that should be in the right position.
     */
    String getRightBrowseWidget();

    /**
     * Get the widget in the left position on the logged in user home page.
     * @return widget fullname that should be in the left position.
     */
    String getLeftHomeWidget();

    /**
     * Get the widget in the center position on the logged in user home page.
     * @return widget fullname that should be in the center position.
     */
    String getCenterHomeWidget();

    /**
     * Get the widget in the right position on the logged in user home page.
     * @return widget fullname that should be in the right position.
     */
    String getRightHomeWidget();

    /**
     * Get the group by properties for the specimens.
     * @return SpecimenGroupCriterion name
     */
    String getSpecimenGrouping();

    /**
     * Get the aggregate cart sort criteria.
     *
     * @return a string containing all of the sort criteria for aggregate cart display
     */
    String getGeneralPopulationSortRegex();

    /**
     * Get whether request review votes should be displayed to the user after the review process is complete.
     * @return whether request review votes should be displayed to the user after the review process is complete.
     */
    boolean isDisplayRequestReviewVotes();
    /**
     * Get whether the voting task should be run.
     * @return Whether the voting task should be run.
     */
    boolean isVotingTaskEnabled();

    /**
     * @return the review process that is active for this deployment.
     */
    ReviewProcess getReviewProcess();

    /**
     * @return whether the account approval process is active
     */
    boolean isAccountApprovalActive();

    /**
     * @return whether a researcher must certify understanding of an MTA before requesting specimens.
     */
    boolean isMtaCertificationRequired();

    /**
     * @return the additional condition to be used when counting the number of new requests.
     */
    String getNewRequestCountCondition();

    /**
     * @return Whether irb approval is required before a request can be submitted.
     */
    boolean isIrbApprovalRequired();

    /**
     * @return The minimum number of aggregate search results that can be displayed to a user.
     */
    int getMinimumAggregateResultsDisplayed();

    /**
     * @return the minimum required funding status (FUNDED, NOT_FUNDED, PENDING)
     *         needed to request a specimen as the appropriate enum.
     */
    FundingStatus getMinimunRequiredFundingStatus();

    /**
     * Gets the label for column 1 of the specimen details panel.
     *
     * @return specimen details panel column 1 label
     */
    String getSpecimenDetailsPanelColumn1Label();

    /**
     * Gets the value for column 1 of the specimen details panel.
     *
     * @return specimen details panel column 1 value
     */
    String getSpecimenDetailsPanelColumn1Value();

    /**
     * Gets the label for column 2 of the specimen details panel.
     *
     * @return specimen details panel column 2 label
     */
    String getSpecimenDetailsPanelColumn2Label();

    /**
     * Gets the value for column 2 of the specimen details panel.
     *
     * @return specimen details panel column 2 value
     */
    String getSpecimenDetailsPanelColumn2Value();

    /**
     * Gets the label for column 3 of the specimen details panel.
     *
     * @return specimen details panel column 3 label
     */
    String getSpecimenDetailsPanelColumn3Label();

    /**
     * Gets the value for column 3 of the specimen details panel.
     *
     * @return specimen details panel column 3 value
     */
    String getSpecimenDetailsPanelColumn3Value();

    /**
     * Gets the label for column 4 of the specimen details panel.
     *
     * @return specimen details panel column 4 label
     */
    String getSpecimenDetailsPanelColumn4Label();

    /**
     * Gets the value for column 4 of the specimen details panel.
     *
     * @return specimen details panel column 4 value
     */
    String getSpecimenDetailsPanelColumn4Value();

    /**
     * Whether specimen collection protocol details should be displayed.
     * @return Whether specimen collection protocol details should be displayed.
     */
    boolean isDisplayCollectionProtocol();

    /**
     * Get whether the biospecimen usage restrictions should be displayed.
     * @return whether the biospecimen usage restrictions should be displayed
     */
    boolean isDisplayUsageRestrictions();

    /**
     * Get whether the protocol document is a required field on the specimen details page.
     * @return whether the protocol document is a required field on the specimen details page
     */
    boolean isProtocolDocumentRequired();

    /**
     * Get the interval with which the cache of data for the home page is refreshed, in milliseconds.
     * @return the interval with which the cache of data for the home page is refreshed
     */
    int getHomePageDataCacheRefreshInterval();

    /**
     * Get the number of days before an email reminder should be sent reminding support letter
     * request reviewers to review a support letter request. 0 means never send a reminder.
     * @return the grace period in days.
     */
    int getSupportLetterRequestReviewGracePeriod();

    /**
     * Get the number of days before an email reminder should be sent reminding support question
     * responders to respond to a question. 0 means never send a reminder.
     * @return the grace period in days.
     */
    int getQuestionResponseGracePeriod();

    /**
     * Get whether to display the progress bar on the various pages of the request process.
     * @return whether to display the progress bar on the various pages of the request process
     */
    boolean isDisplayRequestProcessSteps();

    /**
     * Whether a funding status of 'pending' should be displayed.
     * @return Whether a funding status of 'pending' should be displayed.
     */
    boolean isDisplayFundingStatusPending();

    /**
     * Whether a shipment billing recipient should be displayed.
     * @return Whether a shipment billing recipient should be displayed.
     */
    boolean isDisplayShipmentBillingRecipient();

    /**
     * Get whether the resume is a required field on the user registration page.
     * @return whether the resume is a required field on the user registration page
     */
    boolean isUserResumeRequired();
}
