/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config.help;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;

/**
 * Help configuration for an individual data field.
 * @author gvaughn
 *
 */
@Entity(name = "field_help_config")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class FieldHelpConfig implements PersistentObject {
    
    private static final long serialVersionUID = 5535541491933316938L;
    private static final int MAX_LENGTH = 255;
    private static final int HELP_TEXT_LENGTH = 1000;
    
    private Long id;
    private String entityClassName;
    private String fieldName;
    private String helpText;
    private List<HelpTextLink> helpTextLinks;
    
    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * @return Class name of the entity containing this field.
     */
    @NotEmpty
    @Length(max = MAX_LENGTH)
    @Column(name = "entity_class_name")
    public String getEntityClassName() {
        return entityClassName;
    }

    /**
     * @param entityClassName Class name of the entity containing this field.
     */
    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    /**
     * @return The field corresponding to this help configuration.
     */
    @NotEmpty
    @Length(max = MAX_LENGTH)
    @Column(name = "field_name")
    public String getFieldName() {
        return fieldName;
    }
    
    /**
     * @param fieldName The field corresponding to this help configuration.
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    
    /**
     * @return The help text for this field.
     */
    @NotEmpty
    @Length(max = HELP_TEXT_LENGTH)
    @Column(name = "help_text")
    public String getHelpText() {
        return helpText;
    }
    
    /**
     * @param helpText The help text for this field.
     */
    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }
    
    /**
     * @return Links embedded in the help text. Links are
     * ordered in accordance with their position in the help text.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "help_text_links",
                joinColumns = @JoinColumn(name = "field_help_config_id"),
                inverseJoinColumns = @JoinColumn(name = "help_text_link_id"))
    @ForeignKey(name = "HELP_CONFIG_ID",
                inverseName = "HELP_LINK_ID")
    @Cascade(value = CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @OrderBy(value = "id")
    public List<HelpTextLink> getHelpTextLinks() {
        return helpTextLinks;
    }

    /**
     * @param helpTextLinks Links embedded in the help text.
     */
    public void setHelpTextLinks(List<HelpTextLink> helpTextLinks) {
        this.helpTextLinks = helpTextLinks;
    }
    
    /**
     * Replaces link placeholders in the help text with the given string
     * replacements and returns the resulting help text.
     * @param replacements List of html links to be inserted in the help text.
     * @return Help text with the replacements applied.
     */
    @Transient
    public String getHelpTextWithReplacements(List<String> replacements) {
        String retVal = getHelpText();
        for (int i = 0; i < replacements.size(); i++) {
            String key = "\\{" + i + "\\}";
            retVal = retVal.replaceAll(key, replacements.get(i));
        }
        return retVal;
    }
}
