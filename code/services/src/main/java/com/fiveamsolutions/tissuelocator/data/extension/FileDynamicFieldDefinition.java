/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.extension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.cfg.Configuration;
import org.hibernate.validator.NotEmpty;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.dynamicextensions.EntityDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.ExtendableEntity;
import com.fiveamsolutions.nci.commons.validator.ValidationError;
import com.fiveamsolutions.tissuelocator.hibernate.interceptor.OrphanDeletableEntity;

/**
 * @author ddasgupta
 *
 */
@Entity
@Table(name = "dynamic_field_definition_file")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@OrphanDeletableEntity(idProperty = "id")
public class FileDynamicFieldDefinition extends EntityDynamicFieldDefinition {

    private static final long serialVersionUID = -5646815898391651034L;

    private String contentTypeFieldName;
    private String fileNameFieldName;

    /**
     * {@inheritDoc}
     */
    @Override
    public void addToConfiguration(Configuration cfg) {
        super.addToConfiguration(cfg);
        addToConfiguration(cfg, getContentTypeFieldName(), String.class.getName());
        addToConfiguration(cfg, getFileNameFieldName(), String.class.getName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<ValidationError> validate(ExtendableEntity entity) {
        Collection<ValidationError> errors = new ArrayList<ValidationError>();
        if (!isExtensionNullable(entity)
                && (entity.getCustomProperty(getFieldName()) == null
                        || entity.getCustomProperty(getContentTypeFieldName()) == null
                        || entity.getCustomProperty(getFileNameFieldName()) == null)) {
            errors.add(new ValidationError(getFieldDisplayName(), getMessage(NotNull.class)));
        }
        return errors;
    }

    /**
     * @return the contentTypeFieldName
     */
    @NotEmpty
    @Column(name = "content_type_field_name")
    public String getContentTypeFieldName() {
        return contentTypeFieldName;
    }

    /**
     * @param contentTypeFieldName the contentTypeFieldName to set
     */
    public void setContentTypeFieldName(String contentTypeFieldName) {
        this.contentTypeFieldName = contentTypeFieldName;
    }

    /**
     * @return the fileNameFieldName
     */
    @NotEmpty
    @Column(name = "file_name_field_name")
    public String getFileNameFieldName() {
        return fileNameFieldName;
    }

    /**
     * @param fileNameFieldName the fileNameFieldName to set
     */
    public void setFileNameFieldName(String fileNameFieldName) {
        this.fileNameFieldName = fileNameFieldName;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    @Override
    public Collection<String> getFieldNames() {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add(getFieldName());
        fieldNames.add(getContentTypeFieldName());
        fieldNames.add(getFileNameFieldName());
        return fieldNames;
    }
}
