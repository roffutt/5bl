/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data;

import java.util.Date;
import java.util.List;

/**
 * An object that tracks status transition history.
 * 
 * @author gvaughn
 * @param <E> The type of status.
 * @param <T> The type of status transition.
 */
public interface StatusTransitionable<T extends AbstractStatusTransition<E>, 
    E extends Enum<E>> {

    /**
     * @return the status transitions ordered by date.
     */
    List<T> getStatusTransitionHistory();
    
    /**
     * @return the date of the transition to the current status.
     */
    Date getStatusTransitionDate();
    
    /** 
     * @param statusTransitionDate the date of the transition to the current status.
     */
    void setStatusTransitionDate(Date statusTransitionDate);
    
    /**
     * @return the current status.
     */
    E getStatus();
    
    /**
     * @return the previous status.
     */
    E getPreviousStatus();
    
    /**
     * @param status The previous status.
     */
    void setPreviousStatus(E status);
}
