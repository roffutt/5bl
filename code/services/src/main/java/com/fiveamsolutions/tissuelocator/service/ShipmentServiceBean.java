/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.ShipmentStatus;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.SpecimenDisposition;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.util.Email;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.google.inject.Inject;

/**
 * @author ddasgupta
 *
 */
@Stateless
@SuppressWarnings("PMD.TooManyMethods")
public class ShipmentServiceBean extends GenericServiceBean<Shipment> implements ShipmentServiceLocal {

    private final EmailHelper emailHelper = new EmailHelper();
    private static final String REPLACEMENT_ZERO = "{0}";
    private static final Map<SpecimenClass, QuantityUnits> UNITS_MAP;
    static {
        UNITS_MAP = new HashMap<SpecimenClass, QuantityUnits>();
        UNITS_MAP.put(SpecimenClass.CELLS, QuantityUnits.CELLS);
        UNITS_MAP.put(SpecimenClass.FLUID, QuantityUnits.ML);
        UNITS_MAP.put(SpecimenClass.MOLECULAR, QuantityUnits.UG);
        UNITS_MAP.put(SpecimenClass.TISSUE, QuantityUnits.MG);
    }

    @EJB
    private SpecimenServiceLocal specimenService;
    private ApplicationSettingServiceLocal appSettingService;

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void removeAllLineItems(Shipment shipment) {
        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.getSpecimen().setStatus(SpecimenStatus.AVAILABLE);
            getSpecimenService().savePersistentObject(li.getSpecimen());
        }
        shipment.getLineItems().clear();
        savePersistentObject(shipment);
    }

    /**
     * {@inheritDoc}
     */
    public void removeLineItem(Shipment shipment, SpecimenRequestLineItem lineItem) {
        lineItem.getSpecimen().setStatus(SpecimenStatus.AVAILABLE);
        getSpecimenService().savePersistentObject(lineItem.getSpecimen());
        shipment.getLineItems().remove(lineItem);
        savePersistentObject(shipment);
    }

    /**
     * {@inheritDoc}
     */
    public void removeAggregateLineItem(Shipment shipment,
            AggregateSpecimenRequestLineItem lineItem) {
        shipment.getAggregateLineItems().remove(lineItem);
        savePersistentObject(shipment);
    }

    /**
     * {@inheritDoc}
     */
    public void removeAllAggregateLineItems(Shipment shipment) {
        shipment.getAggregateLineItems().clear();
        savePersistentObject(shipment);
    }

    /**
     * {@inheritDoc}
     */
    public void addSpecimenToOrder(Set<Specimen> specimens, Shipment shipment) {
        for (Specimen s : specimens) {
            SpecimenRequestLineItem li = new SpecimenRequestLineItem();
            li.setSpecimen(s);
            setRequestedQuantity(li);
            s.setStatus(SpecimenStatus.PENDING_SHIPMENT);
            shipment.getLineItems().add(li);
            getSpecimenService().savePersistentObject(li.getSpecimen());
        }
        savePersistentObject(shipment);
    }

    private void setRequestedQuantity(SpecimenRequestLineItem li) {
        if (li.getSpecimen().getAvailableQuantity() == null) {
            li.setQuantity(new BigDecimal("0.0"));
            li.setQuantityUnits(UNITS_MAP.get(li.getSpecimen().getSpecimenType().getSpecimenClass()));
        } else {
            li.setQuantity(li.getSpecimen().getAvailableQuantity());
            li.setQuantityUnits(li.getSpecimen().getAvailableQuantityUnits());
        }
    }

    /**
     * {@inheritDoc}
     */
    public void updateLineItemSpecimenStatus(Shipment shipment, SpecimenStatus status) {
        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.getSpecimen().setStatus(status);
        }
    }

    private void updateLineItemStatusTransitionDate(Shipment shipment, Date date) {
        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.getSpecimen().setStatusTransitionDate(date);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void updateLineItemDispostitions(Shipment shipment) {
        ShipmentStatus previousStatus = shipment.getStatus();
        shipment.setStatus(ShipmentStatus.RECEIVED);
        shipment.setReadyForResearcherReview(true);
        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            SpecimenDisposition sd = li.getDisposition();
            if (sd != null
                    && (sd.equals(SpecimenDisposition.DAMAGED)
                        || sd.equals(SpecimenDisposition.PARTIALLY_CONSUMED_DESTROYED))) {
                li.getSpecimen().setStatus(SpecimenStatus.DESTROYED);
            }
        }
        savePersistentObject(shipment);

        if (!ShipmentStatus.RECEIVED.equals(previousStatus)) {
            notifyTechOfReceivedShipment(shipment);
        }
    }

    private void notifyTechOfReceivedShipment(Shipment shipment) {
        Set<AbstractUser> tissueTechs = getUserService().
            getUsersInRole(Role.SHIPMENT_ADMIN.getName());

        List<String> emails = new ArrayList<String>();
        for (AbstractUser user : tissueTechs) {
            TissueLocatorUser tech = (TissueLocatorUser) user;
            if (tech.getInstitution().equals(shipment.getSendingInstitution())) {
                emails.add(tech.getEmail());
            }
        }

        emailHelper.sendEmail("shipment.email.received", emails.toArray(new String[emails.size()]),
            new String[] {shipment.getId().toString()});
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @SuppressWarnings("unchecked")
    public Shipment getShipmentWithSpecimen(Specimen specimen) {
        String queryString = "select shipment from " + SpecimenRequestLineItem.class.getName()
                           +  " srli join srli.shipments as shipment where srli.specimen.id = :specimenId and "
                           +  "shipment.request.status != :status";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setLong("specimenId", specimen.getId());
        q.setParameter("status", RequestStatus.DENIED);
        q.setMaxResults(1);
        List<Shipment> shipments = q.list();
        return shipments.isEmpty() ? null : shipments.get(0);
    }

    /**
     * {@inheritDoc}
     */
    public void markOrderAsShipped(Shipment order) throws MessagingException {
        order.setStatus(ShipmentStatus.SHIPPED);
        if (order.getShipmentDate() == null) {
            order.setShipmentDate(new Date());
        }
        order.setReadyForResearcherReview(true);
        updateLineItemSpecimenStatus(order, SpecimenStatus.SHIPPED);
        updateLineItemStatusTransitionDate(order, order.getShipmentDate());
        updateRequestStatus(order.getRequest());
        setSignedMtas(order);
        savePersistentObject(order);
        Email shipmentEmail = getRequestorShipmentEmail(order, "specimenRequest.email.shipment");
        sendMtaContactEmail(order, shipmentEmail);
        shipmentEmail.send();
    }

    private void updateRequestStatus(SpecimenRequest request) {
        ReviewProcess process = getAppSettingService().getReviewProcess();
        process.getRequestStatusUpdater().updateStatus(request);
    }

    private void setSignedMtas(Shipment order) {
        SignedMaterialTransferAgreement signedSenderMta =
            getSignedSenderMta(order);
        if (isSignedMtaValid(signedSenderMta)) {
            order.setSignedSenderMta(signedSenderMta);
        }
        SignedMaterialTransferAgreement signedRecipientMta =
            getSignedRecipientMta(order);
        if (isSignedMtaValid(signedRecipientMta)) {
            order.setSignedRecipientMta(signedRecipientMta);
        }
    }

    private void sendMtaContactEmail(Shipment order, Email shipmentEmail) throws MessagingException {
        Person mtaContact = order.getRequest().getRequestor().getInstitution().getMtaContact();
        if (mtaContact != null) {
            ResourceBundle rb = emailHelper.getResourceBundle();
            String recipientName = order.getRequest().getRequestor().getDisplayName();
            String textHeader = rb.getString("mtaContact.shipmentEmail.header.text");
            textHeader = textHeader.replace(REPLACEMENT_ZERO, recipientName);
            String htmlHeader = rb.getString("mtaContact.shipmentEmail.header.html");
            htmlHeader = htmlHeader.replace(REPLACEMENT_ZERO, recipientName);
            Email mtaContactEmail = new Email();
            mtaContactEmail.setRecipient(mtaContact.getEmail());
            mtaContactEmail.setSubject(shipmentEmail.getSubject());
            mtaContactEmail.setText(textHeader + shipmentEmail.getText());
            mtaContactEmail.setHtml(htmlHeader + shipmentEmail.getHtml());
            mtaContactEmail.send();
        }
    }

    /**
     * {@inheritDoc}
     */
    public SignedMaterialTransferAgreement getSignedSenderMta(
            Shipment shipment) {
        if (shipment.getSignedSenderMta() != null) {
            return shipment.getSignedSenderMta();
        }
        if (shipment.getSendingInstitution() != null) {
            InstitutionServiceLocal service = TissueLocatorRegistry.getServiceLocator()
                .getInstitutionService();
            return service.getInstitution(shipment.getSendingInstitution().getId())
                .getCurrentSignedSenderMta();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public SignedMaterialTransferAgreement getSignedRecipientMta(Shipment shipment) {
        if (shipment.getSignedRecipientMta() != null) {
            return shipment.getSignedRecipientMta();
        }
        if (shipment.getRecipient() != null) {
            InstitutionServiceLocal service = TissueLocatorRegistry.getServiceLocator()
                .getInstitutionService();
            return service.getInstitution(shipment.getRecipient().getOrganization().getId())
                .getCurrentSignedRecipientMta();
        }
        return null;
    }

    private boolean isSignedMtaValid(SignedMaterialTransferAgreement mta) {
        return mta != null && SignedMaterialTransferAgreementStatus.APPROVED.equals(mta.getStatus());
    }

    private Email getRequestorShipmentEmail(Shipment order, String keyBase) {
        ResourceBundle rb = emailHelper.getResourceBundle();
        String baseUrl = rb.getString("tissuelocator.baseUrl");
        String orderId = String.valueOf(order.getId());
        String requestId = String.valueOf(order.getRequest().getId());
        String parts = "";
        String trackingTxt = "";
        String trackingHtml = "";
        String sendingInstitution = order.getSendingInstitution().getName();

        if (order.getRequest().getOrders().size() > 1) {
            parts = rb.getString("specimenRequest.email.shipment.multiple");
            parts = parts.replace(REPLACEMENT_ZERO, String.valueOf(order.getRequest().getOrders().size()));
        } else {
            parts = rb.getString("specimenRequest.email.shipment.single");
        }

        String trackingUrl = order.getShippingMethod().getFormattedTrackingUrl(order.getTrackingNumber());
        if (StringUtils.isNotEmpty(trackingUrl)) {
            trackingTxt = rb.getString("specimenRequest.email.shipment.tracking.text");
            trackingTxt = trackingTxt.replace(REPLACEMENT_ZERO, trackingUrl);
            trackingHtml = rb.getString("specimenRequest.email.shipment.tracking.html");
            trackingHtml = trackingHtml.replace(REPLACEMENT_ZERO, trackingUrl);
            trackingHtml = trackingHtml.replace("{1}", order.getTrackingNumber());
        } else {
            trackingTxt = rb.getString("specimenRequest.email.shipment.tracking.none");
            trackingHtml = rb.getString("specimenRequest.email.shipment.tracking.none");
        }
        String[] argsTxt = {baseUrl, requestId, parts, orderId, trackingTxt, sendingInstitution};
        String[] argsHtml = {baseUrl, requestId, parts, orderId, trackingHtml, sendingInstitution};

        return getFullyConfiguredRequestorEmail(order, keyBase, argsTxt, argsHtml);
    }

    /**
     * {@inheritDoc}
     */
    public void cancelOrder(Shipment order) throws MessagingException {
        order.setStatus(ShipmentStatus.CANCELED);
        order.setReadyForResearcherReview(false);
        updateLineItemSpecimenStatus(order, SpecimenStatus.AVAILABLE);
        updateRequestStatus(order.getRequest());
        savePersistentObject(order);
        getRequestorShipmentCancelledEmail(order, "specimenRequest.email.shipmentCanceled").send();
    }
    
    private Email getRequestorShipmentCancelledEmail(final Shipment order, final String keyBase) {
        ResourceBundle rb = emailHelper.getResourceBundle();
        String baseUrl = rb.getString("tissuelocator.baseUrl");
        String orderId = String.valueOf(order.getId());
        String requestId = String.valueOf(order.getRequest().getId());
        String sendingInstitution = order.getSendingInstitution().getName();
        
        String[] argsTxt = {baseUrl, requestId, sendingInstitution, orderId};
        String[] argsHtml = {baseUrl, requestId, sendingInstitution, orderId};
        
        return getFullyConfiguredRequestorEmail(order, keyBase, argsTxt, argsHtml);
    }
    
    private Email getFullyConfiguredRequestorEmail(final Shipment order, final String keyBase, final String[] argsTxt,
            final String[] argsHtml) {
        ResourceBundle rb = emailHelper.getResourceBundle();
        String subject = rb.getString(keyBase + ".subject");
        String text = rb.getString(keyBase + ".text");
        String html = rb.getString(keyBase + ".html");
        for (int i  = 0; i < argsTxt.length; i++) {
            subject = subject.replace("{" + i + "}", argsTxt[i]);
            text = text.replace("{" + i + "}", argsTxt[i]);
            html = html.replace("{" + i + "}", argsHtml[i]);
        }
        Email shipmentCancellationEmail = new Email();
        shipmentCancellationEmail.setRecipient(order.getRequest().getRequestor().getEmail());
        shipmentCancellationEmail.setSubject(subject);
        shipmentCancellationEmail.setHtml(html);
        shipmentCancellationEmail.setText(text);
        return shipmentCancellationEmail;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Shipment> getUnreceivedShippedOrders(int gracePeriod) {
        Date shipmentDate = DateUtils.addDays(new Date(), -1 * gracePeriod);

        String hql = "from " + Shipment.class.getName() + " where status = :status "
                + " and shipmentDate < :shipmentDate"
                + " and (nextReminderDate is null or nextReminderDate < :nextReminderDate)";
        Query query = TissueLocatorHibernateUtil.getCurrentSession().createQuery(hql);
        query.setParameter("status", ShipmentStatus.SHIPPED);
        query.setTimestamp("shipmentDate", shipmentDate);
        query.setTimestamp("nextReminderDate", new Date());
        return query.list();
    }

    /**
     * @return the specimenService
     */
    public SpecimenServiceLocal getSpecimenService() {
        return specimenService;
    }

    /**
     * @param specimenService the specimenService to set
     */
    public void setSpecimenService(SpecimenServiceLocal specimenService) {
        this.specimenService = specimenService;
    }

    /**
     * {@inheritDoc}
     */
    public void saveOrder(Shipment order) throws MessagingException {
        order.setProcessingUser(getUserService().getByUsername(UsernameHolder.getUser()));
        boolean sendEmail = order.isReadyForResearcherReview() && !order.isPreviousReadyForResearcherReview();
        savePersistentObject(order);
        if (sendEmail) {
            emailHelper.sendEmail("shipment.email.readyForReview",
                    new String[] {order.getRequest().getRequestor().getEmail()},
                    new String[] {String.valueOf(order.getRequest().getId()), order.getId().toString(),
                        order.getSendingInstitution().getName()});
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Long savePersistentObject(Shipment o) {
        //overriding save method to relax access restrictions that prevented shipments from
        //being saved during background processing, where there is no logged in user
        onSave(o);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(o);
        return o.getId();
    }

    /**
     * {@inheritDoc}
     */
    public int getShippedOrderCount(Date startDate, Date endDate) {
        String queryString = "select count(*) "
            + "from " + Shipment.class.getName() + " shipment "
            + " where shipment.shipmentDate >= :startDate and shipment.shipmentDate < :endDate ";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        q.setDate("startDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        q.setDate("endDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        return ((Long) q.uniqueResult()).intValue();
    }

    /**
     * @return the appSettingService
     */
    public ApplicationSettingServiceLocal getAppSettingService() {
        return appSettingService;
    }

    /**
     * @param appSettingService the appSettingService to set
     */
    @Inject
    public void setAppSettingService(ApplicationSettingServiceLocal appSettingService) {
        this.appSettingService = appSettingService;
    }
}
