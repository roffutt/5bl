/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.tissuelocator.data.ReviewComment;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.util.OutstandingSpecimenRequestRequirements;
import com.fiveamsolutions.tissuelocator.util.RequestProcessingConfiguration;

/**
 * @author ddasgupta
 *
 */
public class SpecimenRequestStatusAnalyzer {

    private static final Logger LOG = Logger.getLogger(SpecimenRequestStatusAnalyzer.class);

    private final RequestProcessingConfiguration config;

    /**
     * Constructor.
     * @param config the config
     */
    public SpecimenRequestStatusAnalyzer(RequestProcessingConfiguration config) {
        this.config = config;
    }

    /**
     * Analyze the status of a request and return a set of emails to send.
     * @param request the request to analyze
     * @return a map of users to emails to send.
     */
    public OutstandingSpecimenRequestRequirements analyzeRequestStatus(SpecimenRequest request) {
        OutstandingSpecimenRequestRequirements issues = new OutstandingSpecimenRequestRequirements();
        analyzeReviewerAssignmentStatus(request, issues);
        analyzeReviewCompletionStatus(request, issues);
        return issues;
    }

    private void analyzeReviewerAssignmentStatus(SpecimenRequest request,
            OutstandingSpecimenRequestRequirements issues) {
        LOG.info("analyzing reviewer assignment status for request: " + request.getId());
        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        cal.add(Calendar.DATE, config.getReviewerAssignmentPeriod());
        if (cal.getTime().after(new Date())) {
            LOG.info("reviewer assignment period not complete for request " + request.getId());
            return;
        }

        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() == null) {
                LOG.info("no reviewer assigned for institution: " + vote.getInstitution().getName());
                issues.getOutstandingReviewAssignments().add(vote.getInstitution());
            }
        }
    }

    private void analyzeReviewCompletionStatus(SpecimenRequest request,
            OutstandingSpecimenRequestRequirements issues) {
        Set<AbstractUser> outstandingLeadReviewers = getOutstandingLeadReviewers(request);
        if (!outstandingLeadReviewers.isEmpty()) {
            LOG.trace("have outstanding reviewers for request " + request.getId());
            if (isReviewPeriodDone(request)) {
                LOG.trace("review period done, emailing outsanding reviews for request " + request.getId());
                issues.getOutstandingLeadReviewers().addAll(outstandingLeadReviewers);
            }
        }
    }

    private Set<AbstractUser> getOutstandingLeadReviewers(SpecimenRequest request) {
        Set<AbstractUser> leadReviewers = new HashSet<AbstractUser>();
        //create a set of all of the assigned lead reviewers
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null && request.getReviewers().contains(vote.getUser().getInstitution())) {
                leadReviewers.add(vote.getUser());
            }
        }

        //after this loop, leadReviewers will only contain those assigned lead reviewers that haven't commented yet
        for (ReviewComment comment : request.getComments()) {
            leadReviewers.remove(comment.getUser());
        }
        return leadReviewers;
    }

    private boolean isReviewPeriodDone(SpecimenRequest request) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(request.getUpdatedDate());
        LOG.trace("update date: " + cal.getTime().toString());
        cal.add(Calendar.DATE, config.getReviewPeriod());
        LOG.trace("review period close: " + cal.getTime().toString());
        return cal.getTime().before(new Date());
    }
}
