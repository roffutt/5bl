/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.config.search;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;


/**
 * Base class for search configuration objects encapsulating a single field.
 * @author gvaughn
 *
 */
@MappedSuperclass
public class SimpleSearchFieldConfig extends AbstractSearchFieldConfig {

    private static final long serialVersionUID = 5746882910629448017L;
    private static final Logger LOG = Logger.getLogger(SimpleSearchFieldConfig.class);

    private GenericFieldConfig fieldConfig;
    private String note;
    private boolean objectProperty;
    private String errorProperty;
    private String cssClasses;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public String getSearchFieldName() {
        return getFieldConfig().getSearchFieldName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public String getSearchFieldDisplayName() {
        return getFieldConfig().getSearchFieldDisplayName();
    }

    /**
     * Returns the generic field configuration for this search field config.
     * Contains data shared among this configuration and other field-level
     * configurations for the same field.
     * @return The generic field configuration for this search field config.
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "generic_field_config_id")
    @Override
    public GenericFieldConfig getFieldConfig() {
        return fieldConfig;
    }

    /**
     * Set the generic field configuration for this search field config.
     * Contains data shared among this configuration and other field-level
     * configurations for the same field.
     * @param fieldConfig The field configuration for this display setting.
     */
    public void setFieldConfig(GenericFieldConfig fieldConfig) {
        this.fieldConfig = fieldConfig;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transient
    public ApplicationRole getRequiredRole() {
        return getFieldConfig().getRequiredRole();
    }

    /**
     * Resource key for additional notes displayed with
     * the search field.
     * @return Resource key for additional notes displayed with the search field.
     */
    @Length(max = MAX_LENGTH)
    @Column(name = "note")
    public String getNote() {
        return note;
    }

    /**
     * Resource key for additional notes displayed with
     * the search field.
     * @param note Resource key for additional notes displayed with the search field.
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * Indicates whether the property name relates to an example
     * search object.
     * @return whether the property name relates to an example search object.
     */
    @NotNull
    @Column(name = "object_property")
    public boolean isObjectProperty() {
        return objectProperty;
    }

    /**
     * Sets whether the property name relates to an example
     * search object.
     * @param objectProperty whether the property name relates to an example search object.
     */
    public void setObjectProperty(boolean objectProperty) {
        this.objectProperty = objectProperty;
    }

    /**
     * This property is useful for search fields that are dynamic extensions, where some field errors,
     * namely type conversion errors, are placed under keys matching the display name of the extension.
     * @return the errorProperty
     */
    @Length(max = MAX_LENGTH)
    @Column(name = "error_property")
    public String getErrorProperty() {
        return errorProperty;
    }

    /**
     * @param errorProperty the errorProperty to set
     */
    public void setErrorProperty(String errorProperty) {
        this.errorProperty = errorProperty;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCriteriaString(Object object, String mapName) {
        String value = getValueString(object, mapName);
        if (StringUtils.isBlank(value)) {
            return super.getCriteriaString(object, mapName);
        }

        StringBuffer criteria = new StringBuffer();
        criteria.append(getSearchFieldDisplayName());
        criteria.append(CONNECTOR);
        criteria.append(value);
        return criteria.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<AbstractSearchFieldConfig, Object> getValues(Object object, String mapName) {
        Map<AbstractSearchFieldConfig, Object> configMap = new HashMap<AbstractSearchFieldConfig, Object>();
        configMap.put(this, getValue(object, getValueExpression(), mapName));
        return configMap;
    }

    /**
     * @return The expression to be evaluated to retrieve the
     * value of this config.
     */
    @Transient
    protected String getValueExpression() {
        return getSearchFieldName();
    }

    /**
     * @param object the object containing the criteria value
     * @param mapName If the indicated property is contained in a map (not named in the property),
     * the name of the map property.
     * @return a string representation of the criteria value defined by this search field config.
     */
    protected String getValueString(Object object, String mapName) {
        Object value = getValue(object, getSearchFieldName(), mapName);
        if (value != null) {
            return value.toString();
        }
        return StringUtils.EMPTY;
    }

    /**
     * @param object the object containing the criteria value
     * @param property the name of the property to retrieve from the object
     * @param mapName If the indicated property is contained in a map (not named in the property),
     * the name of the map property.
     * @return a string representation of the criteria value defined by this search field config.
     */
    protected Object getValue(Object object, String property, String mapName) {
        StringBuffer buffer = new StringBuffer();
        if (isObjectProperty()) {
            buffer.append("object.");
        }
        if (mapName != null) {
            buffer.append(mapName).append("['");
        }
        buffer.append(property);
        if (mapName != null) {
            buffer.append("']");
        }
        String expression = buffer.toString();
        try {
            if (expression.contains("[")) {
                return getMappedProperty(object, expression);
            }
            return PropertyUtils.getProperty(object, expression);
        } catch (Exception e) {
            LOG.warn("Invalid property: " + expression + " for " + object.getClass().getName(), e);
        }
        return null;
    }

    private Object getMappedProperty(Object object, String property) throws IllegalAccessException,
        InvocationTargetException, NoSuchMethodException {
        String expression = StringUtils.replace(property, "'", "");
        String key = StringUtils.substringBetween(expression, "[", "]");
        expression = StringUtils.replace(expression, "[", "(");
        expression = StringUtils.replace(expression, "]", ")");
        if (expression.lastIndexOf('.', expression.indexOf('(')) > -1) {
            Object o = PropertyUtils.getProperty(object, expression.substring(0,
                    expression.lastIndexOf('.', expression.indexOf('('))));
            return PropertyUtils.getMappedProperty(o, expression.substring(expression.lastIndexOf('.',
                    expression.indexOf('(')) + 1), key);
        }
        return PropertyUtils.getMappedProperty(object, expression, key);
    }

    /**
     * @return One or more space-separated css class names to be applied to ui elements.
     */
    @Length(max = MAX_LENGTH)
    @Column(name = "css_classes")
    public String getCssClasses() {
        return cssClasses;
    }

    /**
     * @param cssClasses One or more space-separated css class names to be applied to ui elements.
     */
    public void setCssClasses(String cssClasses) {
        this.cssClasses = cssClasses;
    }

}
