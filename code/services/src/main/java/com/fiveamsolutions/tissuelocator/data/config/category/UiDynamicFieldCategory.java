/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config.category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField.FieldType;

/**
 * User interface category used to group dynamic field definitions.
 *
 * @author jstephens
 */
@Entity
@Table(name = "ui_dynamic_field_category")
public class UiDynamicFieldCategory extends AbstractUiCategory {

    private static final long serialVersionUID = -1028841336254756757L;
    private List<UiDynamicCategoryField> uiDynamicCategoryFields = new ArrayList<UiDynamicCategoryField>();
    private Map<FieldType, List<UiDynamicCategoryField>> fieldsByTypeMap;
    
    /**
     * Gets the list of fields that belong to the category.
     *
     * @return the category fields
     */
    @OneToMany(mappedBy = "uiDynamicFieldCategory", fetch = FetchType.EAGER)
    @OrderBy(value = "id")
    public List<UiDynamicCategoryField> getUiDynamicCategoryFields() {
        return uiDynamicCategoryFields;
    }

    /**
     * Sets the list of fields that belong to the category.
     *
     * @param uiDynamicCategoryFields the category fields
     */
    public void setUiDynamicCategoryFields(List<UiDynamicCategoryField> uiDynamicCategoryFields) {
        this.uiDynamicCategoryFields = uiDynamicCategoryFields;
    }

    /**
     * Returns all fields associated with custom properties.
     * @return all fields associated with custom properties.
     */
    @Transient
    public List<UiDynamicCategoryField> getCustomPropertyFields() {
        return getFieldsByType(FieldType.CUSTOM_PROPERTY);
    }
    
    private List<UiDynamicCategoryField> getFieldsByType(FieldType fieldType) {
        List<UiDynamicCategoryField> fields = new ArrayList<UiDynamicCategoryField>();
        if (getFieldsByTypeMap().containsKey(fieldType)) {
            fields.addAll(getFieldsByTypeMap().get(fieldType));
        }
        return fields;
    }
    
    @Transient
    private synchronized Map<FieldType, List<UiDynamicCategoryField>> getFieldsByTypeMap() {
        if (fieldsByTypeMap == null) {
            fieldsByTypeMap = new HashMap<UiDynamicCategoryField.FieldType, List<UiDynamicCategoryField>>();
            for (UiDynamicCategoryField field : getUiDynamicCategoryFields()) {
                if (!fieldsByTypeMap.containsKey(field.getFieldType())) {
                    fieldsByTypeMap.put(field.getFieldType(), new ArrayList<UiDynamicCategoryField>());
                }
                fieldsByTypeMap.get(field.getFieldType()).add(field);
            }
        }
        return fieldsByTypeMap;
    }
}