/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.validation.study;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.ObjectUtils;

import com.fiveamsolutions.nci.commons.validator.AbstractMultipleCriteriaValidator;
import com.fiveamsolutions.nci.commons.validator.ValidationError;
import com.fiveamsolutions.tissuelocator.data.FundingStatus;
import com.fiveamsolutions.tissuelocator.data.Study;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableUtil;

/**
 * Validates fields related to a study's funding status.
 * @author ddasgupta
 *
 */
public class StudyFundingStatusValidator extends AbstractMultipleCriteriaValidator<StudyFundingStatus> {

    private static final long serialVersionUID = 523006488421023311L;

    private static final String FIELD_NAME = "study.fundingSources";
    private static final String MESSAGE_KEY = "{validator.study.fundingSource.required}";

    /**
     * {@inheritDoc}
     */
    @Override
    protected Collection<ValidationError> validateObject(Object value) {
        Collection<ValidationError> errors = new ArrayList<ValidationError>();
        if (value instanceof Study && !DisableableUtil.isValidationDisabled()) {
            Study study = (Study) value;
            validateFundingSource(study, errors);
        }

        return errors;
    }

    private void validateFundingSource(Study study, Collection<ValidationError> errors) {
        if (ObjectUtils.equals(FundingStatus.FUNDED, study.getFundingStatus())) {
            validateFundedFields(study, errors);
        }
    }

    private void validateFundedFields(Study study, Collection<ValidationError> errors) {
        if (study.getFundingSources() == null || study.getFundingSources().isEmpty()) {
            ValidationError ve = new ValidationError(FIELD_NAME, MESSAGE_KEY);
            errors.add(ve);
        }
    }
}
