/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.AssertTrue;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.validation.ConsistentInstitutions;


/**
 * @author ddasgupta
 *
 */
@Entity
@Table(name = "specimen_request_review_vote")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@ConsistentInstitutions
public class SpecimenRequestReviewVote implements PersistentObject, Auditable {

    private static final int MAX_COMMENT_LENGTH = 1000;
    private static final long serialVersionUID = -1866562227557803239L;

    private Long id;
    private Institution institution;
    private TissueLocatorUser user;
    private Date date = new Date();
    private Vote vote;
    private String comment;

    private Set<SpecimenRequest> consortiumReviewRequests = new HashSet<SpecimenRequest>();
    private Set<SpecimenRequest> institutionalReviewRequests = new HashSet<SpecimenRequest>();
    private Set<AggregateSpecimenRequestLineItem> aggregateLineItems = new HashSet<AggregateSpecimenRequestLineItem>();

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the user
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    @ForeignKey(name = "request_vote_user_fk")
    @Index(name = "request_vote_user_idx")
    public TissueLocatorUser getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(TissueLocatorUser user) {
        this.user = user;
    }

    /**
     * @return the date
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the vote
     */
    @Enumerated(EnumType.STRING)
    public Vote getVote() {
        return vote;
    }

    /**
     * @param vote the vote to set
     */
    public void setVote(Vote vote) {
        this.vote = vote;
    }

    /**
     * @return the comment
     */
    @Length(max = MAX_COMMENT_LENGTH)
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return true or false depending on validity.
     */
    @AssertTrue(message = "{validator.specimenRequestReviewVote.commentRequired}")
    @Transient
    public boolean isCommentValid() {
        return getVote() == null || Vote.APPROVE.equals(getVote()) || !StringUtils.isBlank(getComment());
    }

    /**
     * @return the institution
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "institution_id")
    @ForeignKey(name = "inst_review_inst_fk")
    @Index(name = "inst_review_inst_idx")
    public Institution getInstitution() {
        return institution;
    }

    /**
     * @param institution the institution to set
     */
    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    /**
     * @return the consortiumReviewRequests
     */
    @ManyToMany(mappedBy = "consortiumReviews")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    public Set<SpecimenRequest> getConsortiumReviewRequests() {
        return consortiumReviewRequests;
    }

    /**
     * @param consortiumReviewRequests the consortiumReviewRequests to set
     */
    public void setConsortiumReviewRequests(Set<SpecimenRequest> consortiumReviewRequests) {
        this.consortiumReviewRequests = consortiumReviewRequests;
    }

    /**
     * @return the institutionalReviewRequests
     */
    @ManyToMany(mappedBy = "institutionalReviews")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    public Set<SpecimenRequest> getInstitutionalReviewRequests() {
        return institutionalReviewRequests;
    }

    /**
     * @param institutionalReviewRequests the institutionalReviewRequests to set
     */
    public void setInstitutionalReviewRequests(Set<SpecimenRequest> institutionalReviewRequests) {
        this.institutionalReviewRequests = institutionalReviewRequests;
    }

    /**
     * @return the aggregateLineItems
     */
    @OneToMany(mappedBy = "vote")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    public Set<AggregateSpecimenRequestLineItem> getAggregateLineItems() {
        return aggregateLineItems;
    }

    /**
     * @param aggregateLineItems the aggregateLineItems to set
     */
    public void setAggregateLineItems(Set<AggregateSpecimenRequestLineItem> aggregateLineItems) {
        this.aggregateLineItems = aggregateLineItems;
    }

    /**
     * Checks that this vote is associated with exactly one consortium review, institutional review,
     * or aggregate line item.
     * @return true if the vote has valid associations to request and line items, false otherwise.
     */
    @Transient
    @AssertTrue(message = "{validator.specimenRequestReviewVote.association}")
    public boolean isVoteAssociationValid() {
        int consortiumSize = getConsortiumReviewRequests().size();
        int institutionalSize = getInstitutionalReviewRequests().size();
        int aggregateSize = getAggregateLineItems().size();
        return consortiumSize + institutionalSize + aggregateSize <= 1;
    }
}
