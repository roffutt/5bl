/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service;

import java.util.Date;
import java.util.Map;

import javax.ejb.Local;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;

import com.fiveamsolutions.tissuelocator.util.RequestReviewReport;
import com.fiveamsolutions.tissuelocator.util.UsageReport;

/**
 * @author ddasgupta
 *
 */
@Local
public interface ReportServiceLocal {

    /**
     * Get a populated Biolocator usage report for a time period.
     * @param startDate the start of the time period
     * @param endDate the end of the time period
     * @return a UsageReport object populated with data corresponding to the given date range.
     */
    UsageReport getUsageReport(Date startDate, Date endDate);

    /**
     * Get a populated Biolocator request review report for a time period.
     * @param startDate the start of the time period
     * @param endDate the end of the time period
     * @param votingPeriod the length of the voting period
     * @return a RequestReviewReport object populated with data corresponding to the given date range.
     */
    RequestReviewReport getRequestReviewReport(Date startDate, Date endDate, int votingPeriod);

    /**
     * run jasper report.
     * @param reportName the name of the report.
     * @param params the report paramters
     * @return the JasperPrint object representing the filed report.
     * @throws JRException on error
     */
    JasperPrint runJasperReport(String reportName, Map<String, Object> params) throws JRException;

    /**
     * run the new user jasper report.
     * @param reportName the name of the report.
     * @param params the report paramters
     * @return the JasperPrint object representing the filed report.
     * @throws JRException on error
     */
    JasperPrint runNewUserReport(String reportName, Map<String, Object> params) throws JRException;
}
