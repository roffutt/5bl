/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.report;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSource;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * @author ddasgupta
 *
 */
public class JasperReportDataSource extends JRAbstractBeanDataSource {

    private final ScrollingDataSource dataSource;
    private int index = 0;
    private Object bean;
    private final Map<String, String> fieldNameMap = new HashMap<String, String>();

    /**
     * @param dataSource the scrolling data source for this report.
     */
    public JasperReportDataSource(ScrollingDataSource dataSource) {
        super(true);
        this.dataSource = dataSource;
     }

    /**
     * {@inheritDoc}
     */
    public boolean next() throws JRException {
        bean = dataSource.getObject(index++);
        return bean != null;
     }

    /**
     * {@inheritDoc}
     */
    public void moveFirst() throws JRException {
        index = 0;
        bean = dataSource.getObject(index);
    }

    /**
     * {@inheritDoc}
     */
   public Object getFieldValue(JRField field) throws JRException {
        try {
            return PropertyUtils.getProperty(bean, getFieldName(field));
        } catch (IllegalAccessException e) {
            throw new JRException(e);
        } catch (InvocationTargetException e) {
            throw new JRException(e);
        } catch (NoSuchMethodException e) {
            throw new JRException(e);
        }
    }

    private String getFieldName(JRField field) {
        String fieldName = field.getName();
        String filteredFieldName = fieldNameMap.get(fieldName);
        if (filteredFieldName == null) {
            filteredFieldName = fieldName.replace('_', '.');
            fieldNameMap.put(fieldName, filteredFieldName);
        }
        return filteredFieldName;
    }
}
