/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.NameCountPair;
import com.fiveamsolutions.tissuelocator.util.ParticipantReturnRequestData;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 */
@Stateless
@SuppressWarnings({ "PMD.TooManyMethods", "PMD.AvoidDuplicateLiterals", "PMD.ExcessiveClassLength" })
public class SpecimenServiceBean extends GenericServiceBean<Specimen> implements SpecimenServiceLocal {

    private static final int PARTICIPANT_ID_INDEX = 0;
    private static final int PARTICIPANT_EXTERNAL_ID_INDEX = 1;
    private static final int PARTICIPANT_EXTERNAL_ID_ASSIGNER_INDEX = 2;
    private static final int SPECIMEN_COUNT_INDEX = 3;

    /**
     * Error message for specimens for which consent cannot be withdrawn.
     */
    public static final String CONSENT_NOT_WITHDRAWABLE = "Consent cannot be withdrawn for the selected specimen.";
    /**
     * Error message for specimens that cannot be returned to the source institution.
     */
    public static final String SPECIMEN_NOT_RETURNABLE =
        "The selected specimen cannot be returned to the source institution.";

    /**
     * The number of top specimen types to return.
     */
    private static final int TOP_SPECIMEN_TYPE_COUNT = 3;

    private final EmailHelper emailHelper = new EmailHelper();
    private static final String NORMAL_SAMPLE_NAME_KEY = "specimen.search.normal.name";

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Map<SpecimenClass, Long> getCountsByClass() {
        String queryString = "select s.specimenType.specimenClass, count(*) from " + Specimen.class.getName()
            + " s where s.status = :status group by s.specimenType.specimenClass";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setParameter("status", SpecimenStatus.AVAILABLE);
        List<Object[]> results = q.list();
        Map<SpecimenClass, Long> resultMap = new HashMap<SpecimenClass, Long>();
        for (Object[] result : results) {
            resultMap.put((SpecimenClass) result[0], (Long) result[1]);
        }
        return resultMap;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Map<String, Long[]> getCountsByPathologicalCharacteristic() {
        String queryString = "select name, id, useCount from " + AdditionalPathologicFinding.class.getName()
            + " where useCount > :useCount"
            + " order by lower(name)";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setParameter("useCount", 0L);
        List<Object[]> results = q.list();
        Map<String, Long[]> resultMap = new LinkedHashMap<String, Long[]>();
        for (Object[] result : results) {
            Long[] idCount = new Long[]{(Long) result[1], (Long) result[2]};
            resultMap.put((String) result[0], idCount);
        }

        String normalName = emailHelper.getResourceBundle().getString(NORMAL_SAMPLE_NAME_KEY);
        if (!resultMap.containsKey(normalName)) {
            return resultMap;
        }
        Long[] normalResult = resultMap.remove(normalName);
        Map<String, Long[]> reorderedMap = new LinkedHashMap<String, Long[]>();
        reorderedMap.put(normalName, normalResult);
        reorderedMap.putAll(resultMap);
        return reorderedMap;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Map<String, Long> getPathologicalCharacteristics() {
        String queryString = "select name, id from " + AdditionalPathologicFinding.class.getName() 
            + " where useCount > :useCount";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setParameter("useCount", 0L);
        List<Object[]> results = q.list();
        Map<String, Long> resultMap = new TreeMap<String, Long>(String.CASE_INSENSITIVE_ORDER);
        for (Object[] result : results) {
            resultMap.put((String) result[0], (Long) result[1]);
        }

        String normalName = emailHelper.getResourceBundle().getString(NORMAL_SAMPLE_NAME_KEY);
        if (!resultMap.containsKey(normalName)) {
            return resultMap;
        }
        Long normalResult = resultMap.remove(normalName);
        Map<String, Long> reorderedMap = new LinkedHashMap<String, Long>();
        reorderedMap.put(normalName, normalResult);
        reorderedMap.putAll(resultMap);
        return reorderedMap;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Map<String, Long> getInstitutions() {
        String queryString = "select distinct s.externalIdAssigner.name, s.externalIdAssigner.id from "
            + Specimen.class.getName()
            + " s where s.status = :status"
            + " order by s.externalIdAssigner.name";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setParameter("status", SpecimenStatus.AVAILABLE);
        List<Object[]> results = q.list();
        Map<String, Long> resultMap = new LinkedHashMap<String, Long>();
        for (Object[] result : results) {
            resultMap.put((String) result[0], (Long) result[1]);
        }
        return resultMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public Long savePersistentObject(Specimen o) {
        boolean inactivatedOrDestroyed = SpecimenStatus.DESTROYED.equals(o.getStatus())
                || SpecimenStatus.UNAVAILABLE.equals(o.getStatus());

        boolean previouslyInactivatedOrDestroyed = SpecimenStatus.DESTROYED.equals(o.getPreviousStatus())
                || SpecimenStatus.UNAVAILABLE.equals(o.getPreviousStatus());

        Collection<Email> emails = new ArrayList<Email>();
        if (inactivatedOrDestroyed && !previouslyInactivatedOrDestroyed) {
            // determine correct notifications and send them out
            if (SpecimenStatus.PENDING_SHIPMENT.equals(o.getPreviousStatus())) {
                emails.add(removeSpecimenFromPendingShipment(o));
            } else if (SpecimenStatus.UNDER_REVIEW.equals(o.getPreviousStatus())) {
                emails.addAll(removeSpecimenFromRequestUnderReview(o));
            }
        }

        Long id = super.savePersistentObject(o);
        emails.removeAll(Collections.singleton(null));
        for (Email email : emails) {
            emailHelper.sendEmail(email.resourceKeyBase, email.emails, email.replacements);
        }

        return id;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public Map<String, Specimen> getSpecimens(Long institutionId, Collection<String> extIds) {
        Map<String, Specimen> specimenMap = new HashMap<String, Specimen>();
        if (!extIds.isEmpty()) {
            String queryString = "from " + Specimen.class.getName()
            + " where externalId in (:externalIds) and externalIdAssigner.id = :institutionId";
            Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
            q.setParameterList("externalIds", extIds);
            q.setParameter("institutionId", institutionId);
            List<Specimen> specimens = q.list();
            for (Specimen specimen : specimens) {
                specimenMap.put(specimen.getExternalId(), specimen);
            }
        }
        return specimenMap;
    }

    /**
     * {@inheritDoc}
     */
    public Specimen importSpecimen(Specimen specimen) {
        importObject(specimen.getExternalIdAssigner());
        specimen.getParticipant().setExternalIdAssigner(specimen.getExternalIdAssigner());
        specimen.getProtocol().setInstitution(specimen.getExternalIdAssigner());
        importObject(specimen.getParticipant());
        importObject(specimen.getProtocol());
        importObject(specimen);
        return getPersistentObject(Specimen.class, specimen.getId());
    }

    private void importObject(PersistentObject object) {
        if (object != null) {
            if (object.getId() != null) {
                TissueLocatorHibernateUtil.getCurrentSession().merge(object);
            } else {
                TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(object);
            }
        }
    }

    private Email removeSpecimenFromPendingShipment(Specimen o) {
        // remove from shipment
        Shipment s = TissueLocatorRegistry.getServiceLocator().getShipmentService().getShipmentWithSpecimen(o);
        SpecimenRequestLineItem li = null;
        for (SpecimenRequestLineItem curLi : s.getLineItems()) {
            if (curLi.getSpecimen().equals(o)) {
                li = curLi;
                break;
            }
        }
        s.getLineItems().remove(li);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(s);

        // notify the tissue tech
        return notifyTissueTechOfRemovedItem(s, o);
    }

    private Email notifyTissueTechOfRemovedItem(Shipment shipment, Specimen specimen) {
        Set<AbstractUser> tissueTechs = getUserService().getUsersInRole(Role.SHIPMENT_ADMIN.getName());

        List<String> emails = new ArrayList<String>();
        for (AbstractUser user : tissueTechs) {
            TissueLocatorUser tech = (TissueLocatorUser) user;
            if (tech.getInstitution().equals(shipment.getSendingInstitution())) {
                emails.add(tech.getEmail());
            }
        }

        return new Email("specimenRequest.email.specimenUnavailableInShipment",
            emails.toArray(new String[emails.size()]),
            new String[] {specimen.getId().toString(), shipment.getId().toString()});
    }

    private Collection<Email> removeSpecimenFromRequestUnderReview(Specimen o) {
        // remove from the request
        SpecimenRequest s =
            TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService().getRequestWithSpecimen(o);
        SpecimenRequestLineItem li = null;
        for (SpecimenRequestLineItem curLi : s.getLineItems()) {
            if (curLi.getSpecimen().equals(o)) {
                li = curLi;
                break;
            }
        }
        s.getLineItems().remove(li);

        Collection<Email> emails = new ArrayList<Email>();

        // notify requester that the tissue in the order was modified
        emails.add(notifyRequesterThatTissueWasRemovedFromRequest(s, o));

        emails.add(notifyReviewersOfChangeToRequest(o, s));

        emails.add(notifyAdminsOfChangeToRequest(o, s));

        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(s);

        return emails;
    }

    private Email notifyAdminsOfChangeToRequest(Specimen o, SpecimenRequest s) {
        // check to see if this institution has more tissue in the order
        Institution inst = o.getExternalIdAssigner();
        boolean foundMoreTissueFromInst = checkIfInstHasMoreTissueInRequest(s, inst);

        // notify inst admin
        TissueLocatorUser instAdmin = null;
        for (SpecimenRequestReviewVote vote : s.getInstitutionalReviews()) {
            if (vote.getInstitution().equals(inst)) {
                instAdmin = vote.getUser();
                if (!foundMoreTissueFromInst) {
                    // if not then remove the inst from the inst reviewers
                    s.getInstitutionalReviews().remove(vote);

                    // and notify the institution admin.
                    return instAdmin == null ? null : notifyAdminOfEmptyRequest(s, o, instAdmin);
                } else if (instAdmin != null) {
                    // notify inst admin of modified request
                    return notifyAdminOfModifiedRequest(s, o, instAdmin);
                }
            }
        }
        return null;
    }

    private Email notifyReviewersOfChangeToRequest(Specimen o, SpecimenRequest s) {
        // notify reviewers
        if (s.getLineItems().isEmpty()) {
            // request is now empty, notify the scientific reviewers
            return notifyReviewersOfEmptyRequest(s, o);
        } else {
            // notify scientific reviewers of modified request
            return notifyReviewersOfModifiedRequest(s, o);
        }
    }

    private Email notifyRequesterThatTissueWasRemovedFromRequest(SpecimenRequest request, Specimen specimen) {
        return new Email("specimenRequest.email.specimenUnavailableInRequest.requester",
                new String[] {request.getRequestor().getEmail()},
                new String[] {specimen.getId().toString(), request.getId().toString()});
    }

    private Email notifyReviewersOfModifiedRequest(SpecimenRequest request, Specimen specimen) {
        List<String> emails = new ArrayList<String>();
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null) {
                emails.add(vote.getUser().getEmail());
            }
        }
        return new Email("specimenRequest.email.specimenUnavailableInRequest.reviewers.modified",
                emails.toArray(new String[emails.size()]),
                new String[] {specimen.getId().toString(), request.getId().toString()});
    }

    private Email notifyReviewersOfEmptyRequest(SpecimenRequest request, Specimen specimen) {
        List<String> emails = new ArrayList<String>();
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            if (vote.getUser() != null) {
                emails.add(vote.getUser().getEmail());
            }
        }
        return new Email("specimenRequest.email.specimenUnavailableInRequest.reviewers.empty",
                emails.toArray(new String[emails.size()]),
                new String[] {specimen.getId().toString(), request.getId().toString()});
    }

    private Email notifyAdminOfModifiedRequest(SpecimenRequest request, Specimen specimen,
            TissueLocatorUser instAdmin) {
        return new Email("specimenRequest.email.specimenUnavailableInRequest.reviewers.modified",
                new String[] {instAdmin.getEmail()},
                new String[] {specimen.getId().toString(), request.getId().toString()});
    }

    private Email notifyAdminOfEmptyRequest(SpecimenRequest request, Specimen specimen, TissueLocatorUser instAdmin) {
        return new Email("specimenRequest.email.specimenUnavailableInRequest.instAdmin.empty",
                new String[] {instAdmin.getEmail()},
                new String[] {specimen.getId().toString(), request.getId().toString()});
    }

    private boolean checkIfInstHasMoreTissueInRequest(SpecimenRequest s, Institution inst) {
        boolean foundMoreTissueFromInst = false;
        for (SpecimenRequestLineItem curLi : s.getLineItems()) {
            if (curLi.getSpecimen().getExternalIdAssigner().equals(inst)) {
                foundMoreTissueFromInst = true;
                break;
            }
        }
        return foundMoreTissueFromInst;
    }

    /**
     * Holder for email properties.
     */
    private final class Email {
        private final String resourceKeyBase;
        private final String[] emails;
        private final String[] replacements;

        @SuppressWarnings("PMD.ArrayIsStoredDirectly")
        public Email(String resourceKeyBase, String[] emails, String[] replacements) {
            this.resourceKeyBase = resourceKeyBase;
            this.emails = emails;
            this.replacements = replacements;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void withdrawConsentForParticipant(Participant participant) {
        List<Specimen> specimens = new ArrayList<Specimen>();
        String queryString = "from " + Specimen.class.getName() + " where "
            + "participant.id = :pid";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setLong("pid", participant.getId());
        specimens.addAll(q.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list());
        for (Specimen specimen : specimens) {
            if (specimen.isConsentWithdrawable()) {
                withdrawConsent(specimen);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void withdrawConsent(Specimen specimen) {
        if (specimen.isConsentWithdrawable()) {
            specimen.setConsentWithdrawn(true);
            specimen.setConsentWithdrawnDate(new Date());
            updateConsentWithdrawnStatus(specimen);
            savePersistentObject(specimen);
        } else {
            throw new IllegalStateException(CONSENT_NOT_WITHDRAWABLE);
        }
    }

    private void updateConsentWithdrawnStatus(Specimen specimen) {
        SpecimenStatus status = specimen.getStatus();
        if (SpecimenStatus.AVAILABLE.equals(status) || SpecimenStatus.PENDING_SHIPMENT.equals(status)
                || SpecimenStatus.UNDER_REVIEW.equals(status)) {
            specimen.setStatus(SpecimenStatus.UNAVAILABLE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void returnToSourceInstitution(Specimen specimen) {
        if (specimen.isReturnableToSourceInstitution()) {
            specimen.setStatus(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
            savePersistentObject(specimen);
        } else {
            throw new IllegalStateException(SPECIMEN_NOT_RETURNABLE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<NameCountPair> getTopRequestedSpecimenTypes(Date startDate, Date endDate) {
        String queryString = "select s.specimenType.name, count(*) "
            + " from " + Specimen.class.getName() + " s "
            + " left join s.lineItems as lineItem "
            + " left join lineItem.requests as request "
            + " where request.requestedDate is not null "
            + " and request.requestedDate >= :startDate and request.requestedDate < :endDate "
            + " group by s.specimenType.name "
            + " order by count(*) desc ";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        q.setDate("startDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        q.setDate("endDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        q.setMaxResults(TOP_SPECIMEN_TYPE_COUNT);
        List<Object[]> results = q.list();

        List<NameCountPair> resultList = new ArrayList<NameCountPair>();
        for (Object[] result : results) {
            resultList.add(new NameCountPair((String) result[0], (Long) result[1]));
        }
        return resultList;
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public Map<Long, ParticipantReturnRequestData> getReturnRequestDataByParticipant(Date startDate, Date endDate) {
        Map<Long, ParticipantReturnRequestData> returnData = new TreeMap<Long, ParticipantReturnRequestData>();
        Map<Long, ParticipantReturnRequestData> returnRequestCounts =
            getReturnRequestCountByParticipant(startDate, endDate);
        for (Map.Entry<Long, ParticipantReturnRequestData> returnRequestCount : returnRequestCounts.entrySet()) {
            returnData.put(returnRequestCount.getKey(), returnRequestCount.getValue());
        }
        Map<Long, ParticipantReturnRequestData> unfulfilledReturnCounts =
            getUnfulfilledReturnCountByParticipant(endDate);
        for (Map.Entry<Long, ParticipantReturnRequestData> unfulfilledReturnCount
                : unfulfilledReturnCounts.entrySet()) {
            if (!returnData.containsKey(unfulfilledReturnCount.getKey())) {
                returnData.put(unfulfilledReturnCount.getKey(), unfulfilledReturnCount.getValue());
            } else {
                returnData.get(unfulfilledReturnCount.getKey())
                    .setUnfulfilledReturnRequests(unfulfilledReturnCount.getValue().getUnfulfilledReturnRequests());
            }
        }
        Map<Long, ParticipantReturnRequestData> fulfilledReturnCounts =
            getFulfilledReturnCountByParticipant(startDate, endDate);
        for (Map.Entry<Long, ParticipantReturnRequestData> fulfilledReturnCount : fulfilledReturnCounts.entrySet()) {
            if (!returnData.containsKey(fulfilledReturnCount.getKey())) {
                returnData.put(fulfilledReturnCount.getKey(), fulfilledReturnCount.getValue());
            } else {
                returnData.get(fulfilledReturnCount.getKey())
                    .setReturnsFulfilled(fulfilledReturnCount.getValue().getReturnsFulfilled());
            }
        }
        return returnData;
    }

    @SuppressWarnings("unchecked")
    private Map<Long, ParticipantReturnRequestData> getReturnRequestCountByParticipant(Date startDate, Date endDate) {
        Map<Long, ParticipantReturnRequestData> returnRequestCounts = new HashMap<Long, ParticipantReturnRequestData>();
        String queryString = "select s.participant.id, s.participant.externalId,"
            + " s.participant.externalIdAssigner.id, count(*) from "
            + Specimen.class.getName() + " s"
            + " where s.consentWithdrawn = :consentWithdrawn"
            + " and s.consentWithdrawnDate >= :startDate and s.consentWithdrawnDate < :endDate"
            + " group by s.participant.id, s.participant.externalId, s.participant.externalIdAssigner.id";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        q.setDate("startDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        q.setDate("endDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        q.setBoolean("consentWithdrawn", true);
        List<Object[]> results = q.list();
        for (Object[] result : results) {
            ParticipantReturnRequestData data = new ParticipantReturnRequestData();
            data.setExternalId((String) result[PARTICIPANT_EXTERNAL_ID_INDEX]);
            data.setExternalIdAssignerId((Long) result[PARTICIPANT_EXTERNAL_ID_ASSIGNER_INDEX]);
            data.setReturnsRequested((Long) result[SPECIMEN_COUNT_INDEX]);
            returnRequestCounts.put((Long) result[PARTICIPANT_ID_INDEX], data);
        }
        return returnRequestCounts;
    }

    @SuppressWarnings("unchecked")
    private Map<Long, ParticipantReturnRequestData> getUnfulfilledReturnCountByParticipant(Date cutoffDate) {
        Map<Long, ParticipantReturnRequestData> unfulfilledReturnCounts =
            new HashMap<Long, ParticipantReturnRequestData>();
        String queryString = " select s.participant.id, s.participant.externalId,"
            + " s.participant.externalIdAssigner.id, count(*) from "
            + Specimen.class.getName() + " s"
            + " where  s.consentWithdrawn = :consentWithdrawn"
            + " and s.consentWithdrawnDate < :cutoffDate"
            + " and s.id not in"
            + " (select spec.id from " + Specimen.class.getName()
            + " spec inner join spec.statusTransitionHistory as transition"
            + " where transition.newStatus = :newStatus"
            + " and transition.transitionDate <= :cutoffDate)"
            + " group by s.participant.id, s.participant.externalId, s.participant.externalIdAssigner.id";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(cutoffDate);
        cal.add(Calendar.DATE, 1);
        q.setDate("cutoffDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        q.setBoolean("consentWithdrawn", true);
        q.setString("newStatus", SpecimenStatus.RETURNED_TO_PARTICIPANT.name());
        List<Object[]> results = q.list();
        for (Object[] result : results) {
            ParticipantReturnRequestData data = new ParticipantReturnRequestData();
            data.setExternalId((String) result[PARTICIPANT_EXTERNAL_ID_INDEX]);
            data.setExternalIdAssignerId((Long) result[PARTICIPANT_EXTERNAL_ID_ASSIGNER_INDEX]);
            data.setUnfulfilledReturnRequests((Long) result[SPECIMEN_COUNT_INDEX]);
            unfulfilledReturnCounts.put((Long) result[PARTICIPANT_ID_INDEX], data);
        }
        return unfulfilledReturnCounts;
    }

    @SuppressWarnings("unchecked")
    private Map<Long, ParticipantReturnRequestData> getFulfilledReturnCountByParticipant(Date startDate, Date endDate) {
        Map<Long, ParticipantReturnRequestData> fulfilledReturnCounts =
            new HashMap<Long, ParticipantReturnRequestData>();
        String queryString = "select s.participant.id, s.participant.externalId,"
            + " s.participant.externalIdAssigner.id, count(DISTINCT s.id)"
            + " from " + Specimen.class.getName() + " s inner join s.statusTransitionHistory as transition"
            + " where transition.newStatus = :newStatus"
            + " and transition.transitionDate >= :startDate and transition.transitionDate < :endDate"
            + " group by s.participant.id, s.participant.externalId, s.participant.externalIdAssigner.id";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        q.setDate("startDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        q.setDate("endDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        q.setString("newStatus", SpecimenStatus.RETURNED_TO_PARTICIPANT.name());
        List<Object[]> results = q.list();
        for (Object[] result : results) {
            ParticipantReturnRequestData data = new ParticipantReturnRequestData();
            data.setExternalId((String) result[PARTICIPANT_EXTERNAL_ID_INDEX]);
            data.setExternalIdAssignerId((Long) result[PARTICIPANT_EXTERNAL_ID_ASSIGNER_INDEX]);
            data.setReturnsFulfilled((Long) result[SPECIMEN_COUNT_INDEX]);
            fulfilledReturnCounts.put((Long) result[PARTICIPANT_ID_INDEX], data);
        }
        return fulfilledReturnCounts;
    }
}
