/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.util;


/**
 * Holder for request processing configuration parameters.
 * @author ddasgupta
 *
 */
public final class RequestProcessingConfiguration {

    private int reviewPeriod;
    private int votingPeriod;
    private int minPercentAvailableVotes;
    private int minPercentWinningVotes;
    private Email reviewLateEmail;
    private Email voteLateEmail;
    private Email deadlockEmail;
    private Email deadlockInstAdminEmail;
    private int reviewerAssignmentPeriod;
    private Email reviewerAssignmentLateEmail;
    private Email institutionalAdminEmail;
    private Email voteFinalizedEmail;
    private Email voteFinalizedAssignmentEmail;
    private Email requestPendingReleaseEmail;

    /**
     * the length of the review period in days.
     * @return the reviewPeriod
     */
    public int getReviewPeriod() {
        return this.reviewPeriod;
    }

    /**
     * @param reviewPeriod the reviewPeriod to set
     */
    public void setReviewPeriod(int reviewPeriod) {
        this.reviewPeriod = reviewPeriod;
    }

    /**
     * the length of the voting period in days.
     * @return the votingPeriod
     */
    public int getVotingPeriod() {
        return this.votingPeriod;
    }

    /**
     * @param votingPeriod the votingPeriod to set
     */
    public void setVotingPeriod(int votingPeriod) {
        this.votingPeriod = votingPeriod;
    }

    /**
     * the required percentage of all scientific review committee members that have to have voted on each request.
     * @return the minPercentAvailableVotes
     */
    public int getMinPercentAvailableVotes() {
        return this.minPercentAvailableVotes;
    }

    /**
     * @param minPercentAvailableVotes the minPercentAvailableVotes to set
     */
    public void setMinPercentAvailableVotes(int minPercentAvailableVotes) {
        this.minPercentAvailableVotes = minPercentAvailableVotes;
    }

    /**
     * the required percentage of all votes cast for a request that the winning vote must have achieved.
     * @return the minPercentWinningVotes
     */
    public int getMinPercentWinningVotes() {
        return this.minPercentWinningVotes;
    }

    /**
     * @param minPercentWinningVotes the minPercentWinningVotes to set
     */
    public void setMinPercentWinningVotes(int minPercentWinningVotes) {
        this.minPercentWinningVotes = minPercentWinningVotes;
    }

    /**
     * the email sent to the reviewers when their review is late.
     * @return the reviewLateEmail
     */
    public Email getReviewLateEmail() {
        return this.reviewLateEmail;
    }

    /**
     * @param reviewLateEmail the reviewLateEmail to set
     */
    public void setReviewLateEmail(Email reviewLateEmail) {
        this.reviewLateEmail = reviewLateEmail;
    }

    /**
     * the email sent to the members of the review committee whose votes are late.
     * @return the voteLateEmail
     */
    public Email getVoteLateEmail() {
        return this.voteLateEmail;
    }

    /**
     * @param voteLateEmail the voteLateEmail to set
     */
    public void setVoteLateEmail(Email voteLateEmail) {
        this.voteLateEmail = voteLateEmail;
    }

    /**
     * the email sent to the members of the review committee when there is deadlock.
     * @return the deadlockEmail
     */
    public Email getDeadlockEmail() {
        return this.deadlockEmail;
    }

    /**
     * @param deadlockEmail the deadlockEmail to set
     */
    public void setDeadlockEmail(Email deadlockEmail) {
        this.deadlockEmail = deadlockEmail;
    }


    /**
     * the email sent to the institutional admins who have assigned scientific reviewers when a deadlock occurs.
     * @return the deadlockInstAdminEmail
     */
    public Email getDeadlockInstAdminEmail() {
        return this.deadlockInstAdminEmail;
    }

    /**
     * @param deadlockInstAdminEmail the deadlockInstAdminEmail to set
     */
    public void setDeadlockInstAdminEmail(Email deadlockInstAdminEmail) {
        this.deadlockInstAdminEmail = deadlockInstAdminEmail;
    }


    /**
     * the length of the reviewer assignment period in days.
     * @return the reviewerAssignmentPeriod
     */
    public int getReviewerAssignmentPeriod() {
        return this.reviewerAssignmentPeriod;
    }

    /**
     * @param reviewerAssignmentPeriod the reviewerAssignmentPeriod to set
     */
    public void setReviewerAssignmentPeriod(int reviewerAssignmentPeriod) {
        this.reviewerAssignmentPeriod = reviewerAssignmentPeriod;
    }

    /**
     * the email sent to the reviewer assigners when their review assignment is late.
     * @return the reviewerAssignmentLateEmail
     */
    public Email getReviewerAssignmentLateEmail() {
        return this.reviewerAssignmentLateEmail;
    }

    /**
     * @param reviewerAssignmentLateEmail the reviewerAssignmentLateEmail to set
     */
    public void setReviewerAssignmentLateEmail(Email reviewerAssignmentLateEmail) {
        this.reviewerAssignmentLateEmail = reviewerAssignmentLateEmail;
    }

    /**
     * @return the institutionalAdminEmail
     */
    public Email getInstitutionalAdminEmail() {
        return this.institutionalAdminEmail;
    }

    /**
     * @param institutionalAdminEmail the institutionalAdminEmail to set
     */
    public void setInstitutionalAdminEmail(Email institutionalAdminEmail) {
        this.institutionalAdminEmail = institutionalAdminEmail;
    }

    /**
     * the email sent to the lead reviewer when the vote has been finalized.
     * @return the voteFinalizedEmail
     */
    public Email getVoteFinalizedEmail() {
        return this.voteFinalizedEmail;
    }

    /**
     * @param voteFinalizedEmail the voteFinalizedEmail to set
     */
    public void setVoteFinalizedEmail(Email voteFinalizedEmail) {
        this.voteFinalizedEmail = voteFinalizedEmail;
    }

    /**
     * @return the voteFinalizedAssignmentEmail
     */
    public Email getVoteFinalizedAssignmentEmail() {
        return this.voteFinalizedAssignmentEmail;
    }

    /**
     * @param voteFinalizedAssignmentEmail the voteFinalizedAssignmentEmail to set
     */
    public void setVoteFinalizedAssignmentEmail(Email voteFinalizedAssignmentEmail) {
        this.voteFinalizedAssignmentEmail = voteFinalizedAssignmentEmail;
    }

    /**
     * @return the requestPendingReleaseEmail
     */
    public Email getRequestPendingReleaseEmail() {
        return requestPendingReleaseEmail;
    }

    /**
     * @param requestPendingReleaseEmail the requestPendingReleaseEmail to set
     */
    public void setRequestPendingReleaseEmail(Email requestPendingReleaseEmail) {
        this.requestPendingReleaseEmail = requestPendingReleaseEmail;
    }
}
