/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.xwork.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
@Stateless
public class MaterialTransferAgreementServiceBean extends GenericServiceBean<MaterialTransferAgreement> implements
        MaterialTransferAgreementServiceLocal {

    private static final int MTA_SUBMISSION_DEADLINE = 30;

    @EJB
    private InstitutionServiceLocal institutionService;

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Long saveMaterialTransferAgreement(MaterialTransferAgreement mta) throws IOException {
        Long id = savePersistentObject(mta);

        Collection<Institution> institutions = getInstitutionService().getAll(Institution.class);
        Set<String> instAdmins = new HashSet<String>();
        Set<String> mtaContacts = new HashSet<String>();
        for (Institution institution : institutions) {
            updateSignedMta(institution.getSignedSenderMtas());
            updateSignedMta(institution.getSignedRecipientMtas());

            if (BooleanUtils.isTrue(institution.getConsortiumMember())) {
                Set<TissueLocatorUser> admins =
                    getUserService().getByRoleAndInstitution(Role.SIGNED_MTA_VIEWER.getName(), institution);
                for (TissueLocatorUser admin : admins) {
                    instAdmins.add(admin.getEmail());
                }
            } else if (institution.getMtaContact() != null) {
                mtaContacts.add(institution.getMtaContact().getEmail());
            }
        }

        MaterialTransferAgreement savedMta = getPersistentObject(MaterialTransferAgreement.class, id);
        sendEmail(instAdmins, "mta.email.instAdmin", savedMta);
        sendEmail(mtaContacts, "mta.email.mtaContact", savedMta);

        return id;
    }

    private void updateSignedMta(List<SignedMaterialTransferAgreement> signedMtaCollection) {
        if (!signedMtaCollection.isEmpty()) {
            for (SignedMaterialTransferAgreement signedMta : signedMtaCollection) {
                if (!SignedMaterialTransferAgreementStatus.OUT_OF_DATE.equals(signedMta.getStatus())) {
                    signedMta.setStatus(SignedMaterialTransferAgreementStatus.OUT_OF_DATE);
                    TissueLocatorRegistry.getServiceLocator().getGenericService().savePersistentObject(signedMta);
                }
            }
        }
    }

    private void sendEmail(Set<String> emails, String keyBase, MaterialTransferAgreement savedMta)
        throws IOException {
        EmailHelper helper = new EmailHelper();
        String deadline = helper.getDefaultDateFormat().format(DateUtils.addDays(new Date(), MTA_SUBMISSION_DEADLINE));
        String version = helper.getDefaultDateFormat().format(savedMta.getVersion());
        String[] args = new String[] {deadline, version};
        helper.sendEmail(keyBase, emails.toArray(new String[emails.size()]), args);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<MaterialTransferAgreement> getAll() {
        return getCriteria().list();
    }

    private Criteria getCriteria() {
        Criteria c = TissueLocatorHibernateUtil.getCurrentSession().createCriteria(MaterialTransferAgreement.class);
        return c.addOrder(Order.desc("uploadTime"));
    }

    /**
     * {@inheritDoc}
     */
    public MaterialTransferAgreement getCurrentMta() {
        return (MaterialTransferAgreement) getCriteria().setMaxResults(1).uniqueResult();
    }

    /**
     * @return the institutionService
     */
    public InstitutionServiceLocal getInstitutionService() {
        return institutionService;
    }

    /**
     * @param institutionService the institutionService to set
     */
    public void setInstitutionService(InstitutionServiceLocal institutionService) {
        this.institutionService = institutionService;
    }

}
