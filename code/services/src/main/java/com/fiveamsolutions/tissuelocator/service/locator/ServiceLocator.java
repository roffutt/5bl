/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.locator;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceLocal;
import com.fiveamsolutions.tissuelocator.service.FixedConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.FullConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.HelpConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.LineItemReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ReportServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SignedMaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestProcessingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceLocal;

/**
 * The service locator interface.
 *
 * @author smiller
 */
public interface ServiceLocator {
    /**
     * @return local service
     */
    GenericServiceLocal<PersistentObject> getGenericService();

    /**
     * @return the institution service
     */
    InstitutionServiceLocal getInstitutionService();

    /**
     * @return the code service
     */
    CodeServiceLocal getCodeService();

    /**
     * @return the specimen service
     */
    SpecimenServiceLocal getSpecimenService();

    /**
     * @return the collection protocol service.
     */
    CollectionProtocolServiceLocal getCollectionProtocolService();

    /**
     * @return the service.
     */
    ParticipantServiceLocal getParticipantService();

    /**
     * @return the specimen request service
     */
    SpecimenRequestServiceLocal getSpecimenRequestService();

    /**
     * @return the service.
     */
    ShipmentServiceLocal getShipmentService();

    /**
     * @return the specimen request processing service
     */
    SpecimenRequestProcessingServiceLocal getSpecimenRequestProcessingService();

    /**
     * @return the full consortium review service
     */
    FullConsortiumReviewServiceLocal getFullConsortiumReviewService();

    /**
     * @return the fixed consortium review service
     */
    FixedConsortiumReviewServiceLocal getFixedConsortiumReviewService();

    /**
     * @return the line item review service
     */
    LineItemReviewServiceLocal getLineItemReviewService();

    /**
     * @return the material transfer agreement service
     */
    MaterialTransferAgreementServiceLocal getMaterialTransferAgreementService();

    /**
     * @return the signed material transfer agreement service
     */
    SignedMaterialTransferAgreementServiceLocal getSignedMaterialTransferAgreementService();

    /**
     * @return the report service
     */
    ReportServiceLocal getReportService();

    /**
     * @return the help configuration service
     */
    HelpConfigServiceLocal getHelpConfigService();
}