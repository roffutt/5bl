/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.util;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Utility methods for retrieving property values from domain objects.
 * @author gvaughn
 *
 */
public class TissueLocatorPropertyUtil {

    private static final String CUSTOM_PROPERTIES_OBJECT_PROPERTY_NAME = "customProperties";
    
    /**
     * Retrieve an object's property value.
     * 
     * @param targetObject target object
     * @param property name of the property to be retrieved
     * @return the value of the property
     * @throws IllegalAccessException thrown by apache PropertyUtils
     * @throws InvocationTargetException thrown by apache PropertyUtils
     * @throws NoSuchMethodException if the property name is not a valid property for the target object
     */
    public static Object getObjectProperty(Object targetObject, String property) 
        throws IllegalAccessException, InvocationTargetException,
            NoSuchMethodException {
        String currentProperty = property;
        Object currentTargetObject = targetObject;
        for (int propertySeparatorIndex = currentProperty.indexOf('.'); propertySeparatorIndex != -1
                && currentTargetObject != null; propertySeparatorIndex = currentProperty.indexOf('.')) {
            String currentPropertyKey = currentProperty.substring(0, propertySeparatorIndex);
            currentTargetObject = PropertyUtils.getProperty(currentTargetObject, currentPropertyKey);
            currentProperty = currentProperty.substring(propertySeparatorIndex + 1);
        }
        return getSimpleObjectProperty(currentTargetObject, currentProperty);
    }

    private static Object getSimpleObjectProperty(Object targetObject, String property)
        throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Object value = null;
        if (targetObject != null && StringUtils.isNotBlank(property)) {
            if (PropertyUtils.getPropertyDescriptor(targetObject, property) != null) {
                value = PropertyUtils.getProperty(targetObject, property);
            } else if (PropertyUtils.getPropertyDescriptor(targetObject, 
                    CUSTOM_PROPERTIES_OBJECT_PROPERTY_NAME) != null) {
                value = PropertyUtils.getMappedProperty(targetObject, CUSTOM_PROPERTIES_OBJECT_PROPERTY_NAME
                        + "(" + property + ")");
            } else {
                throw new NoSuchMethodException("Property " + property + " not found for a "
                        + targetObject.getClass().getName() + " object");
            }
        }
        return value;
    }

}
