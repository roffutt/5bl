/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
    *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
    *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.mta;

import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.AssertTrue;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.Searchable;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;


/**
 * @author ddasgupta
 *
 */
@Entity
@Table(name = "signed_material_transfer_agreement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SignedMaterialTransferAgreement implements PersistentObject {

    private static final long serialVersionUID = -2526558475326569867L;

    private Long id;
    private Institution sendingInstitution;
    private Institution receivingInstitution;
    private MaterialTransferAgreement originalMta;
    private TissueLocatorFile document;
    private SignedMaterialTransferAgreementStatus status;
    private Date uploadTime;
    private TissueLocatorUser uploader;

    /**
     * {@inheritDoc}
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the sendingInstitution
     */
    @ManyToOne
    @JoinColumn(name = "sending_institution_id")
    @ForeignKey(name = "signed_mta_sending_institution_fk")
    @Index(name = "signed_mta_sending_institution_idx")
    @Cascade(CascadeType.SAVE_UPDATE)
    public Institution getSendingInstitution() {
        return this.sendingInstitution;
    }

    /**
     * @param sendingInstitution the sendingInstitution to set
     */
    public void setSendingInstitution(Institution sendingInstitution) {
        this.sendingInstitution = sendingInstitution;
    }

    /**
     * @return the receivingInstitution
     */
    @ManyToOne
    @JoinColumn(name = "receiving_institution_id")
    @ForeignKey(name = "signed_mta_receiving_institution_fk")
    @Index(name = "signed_mta_receiving_institution_idx")
    @Cascade(CascadeType.SAVE_UPDATE)
    public Institution getReceivingInstitution() {
        return this.receivingInstitution;
    }

    /**
     * @param receivingInstitution the receivingInstitution to set
     */
    public void setReceivingInstitution(Institution receivingInstitution) {
        this.receivingInstitution = receivingInstitution;
    }

    /**
     * @return the originalMta
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "original_mta_id")
    @ForeignKey(name = "signed_mta_original_mta_fk")
    @Index(name = "signed_mta_original_mta_idx")
    public MaterialTransferAgreement getOriginalMta() {
        return this.originalMta;
    }

    /**
     * @param originalMta the originalMta to set
     */
    public void setOriginalMta(MaterialTransferAgreement originalMta) {
        this.originalMta = originalMta;
    }

    /**
     * @return the document
     */
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "contentType", column = @Column(name = "document_content_type", nullable = false)),
        @AttributeOverride(name = "name", column = @Column(name = "document_name", nullable = false))
    })
    @AssociationOverrides({
        @AssociationOverride(name = "lob", joinColumns = @JoinColumn(name = "document_lob_id", nullable = false))
    })
    public TissueLocatorFile getDocument() {
        return this.document;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(TissueLocatorFile document) {
        this.document = document;
    }

    /**
     * @return the status
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Searchable
    @Index(name = "signed_mta_status_idx")
    public SignedMaterialTransferAgreementStatus getStatus() {
        return this.status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(SignedMaterialTransferAgreementStatus status) {
        this.status = status;
    }

    /**
     * @return the uploadTime
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "upload_time")
    @Index(name = "signed_mta_upload_time_idx")
    public Date getUploadTime() {
        return this.uploadTime;
    }

    /**
     * @param uploadTime the uploadTime to set
     */
    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    /**
     * @return the uploader
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "loader_id")
    @ForeignKey(name = "signed_mta_loader_fk")
    @Index(name = "signed_mta_loader_idx")
    public TissueLocatorUser getUploader() {
        return this.uploader;
    }

    /**
     * @param uploader the uploader to set
     */
    public void setUploader(TissueLocatorUser uploader) {
        this.uploader = uploader;
    }

    /**
     * validate that the signed MTA is used only for a sending institution or a receiving institution,
     * and not both.
     * @return true if valid.
     */
    @Transient
    @AssertTrue
    public boolean areInstitutionsValid() {
        return (getSendingInstitution() == null) != (getReceivingInstitution() == null);
    }
}
