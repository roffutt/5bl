/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.util;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;

/**
 * Updates the use count of an AdditionalPathologicFinding.
 * @author gvaughn
 *
 */
public class AdditionalPathologicFindingUseCountHandler {

    /**
     * Updates the use count of the pathological characteristics
     * associated with the given specimen.
     * @param specimen Specimen whose codes will be updated.
     */
    public void updateAdditionalPathologicFindings(Specimen specimen) {
        if (SpecimenStatus.AVAILABLE.equals(specimen.getStatus())) {
            handleAvailableSpecimen(specimen);
        } else {
            handleUnavailableSpecimen(specimen);
        }
    }
   
    private void handleAvailableSpecimen(Specimen specimen) {
        if (isNewlyAvailable(specimen)) {
            incrementCount(specimen.getPathologicalCharacteristic());
        } else if (isCodeUpdated(specimen)) {
            incrementCount(specimen.getPathologicalCharacteristic());
            decrementCount(specimen.getPreviousPathologicalCharacteristic());
        }
    }
    
    private void handleUnavailableSpecimen(Specimen specimen) {
        if (isNewlyUnavailable(specimen)) {
            if (!isCodeUpdated(specimen)) {
                decrementCount(specimen.getPathologicalCharacteristic());
            } else {
                decrementCount(specimen.getPreviousPathologicalCharacteristic());
            }            
        }
    }
    
    private boolean isNewlyAvailable(Specimen specimen) {
       return SpecimenStatus.AVAILABLE.equals(specimen.getStatus()) 
           && !SpecimenStatus.AVAILABLE.equals(specimen.getPreviousStatus());
    }
    
    private boolean isNewlyUnavailable(Specimen specimen) {
        return !SpecimenStatus.AVAILABLE.equals(specimen.getStatus()) 
            && SpecimenStatus.AVAILABLE.equals(specimen.getPreviousStatus());
    }
    
    private boolean isCodeUpdated(Specimen specimen) {
        return !specimen.getPathologicalCharacteristic().getId().equals(
                specimen.getPreviousPathologicalCharacteristic().getId());
    }
    
    private void incrementCount(AdditionalPathologicFinding code) {
        code.setUseCount(code.getUseCount() + 1);
    }
    
    private void decrementCount(AdditionalPathologicFinding code) {
        code.setUseCount(code.getUseCount() - 1);
    }
}
