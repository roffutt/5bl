/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ejb.Local;
import javax.mail.MessagingException;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;

/**
 * @author ddasgupta
 *
 */
@Local
public interface ShipmentServiceLocal extends GenericServiceLocal<Shipment> {

    /**
     * Remove the given line item.
     * @param shipment the shipment.
     * @param lineItem the line item.
     */
    void removeLineItem(Shipment shipment, SpecimenRequestLineItem lineItem);

    /**
     * Remove all line items.
     * @param shipment the line items.
     */
    void removeAllLineItems(Shipment shipment);

    /**
     * Remove the given aggregate line item.
     * @param shipment the shipment.
     * @param lineItem the line item.
     */
    void removeAggregateLineItem(Shipment shipment, AggregateSpecimenRequestLineItem lineItem);

    /**
     * Remove all aggregate line items.
     * @param shipment the shipment.
     */
    void removeAllAggregateLineItems(Shipment shipment);

    /**
     * Add the specimen to the order.
     * @param specimens the specimen
     * @param order the order.
     */
    void addSpecimenToOrder(Set<Specimen> specimens, Shipment order);


    /**
     * Updates the line item biospecimens in the shipment to the given status.
     * @param shipment the shipment containing the line items
     * @param status the status to set
     */
    void updateLineItemSpecimenStatus(Shipment shipment, SpecimenStatus status);

    /**
     * Marks all line item specimens as destroyed if they have a disposition of destroyed or damaged.
     * @param shipment the shipment containing the line items.
     */
    void updateLineItemDispostitions(Shipment shipment);

    /**
     * Gets the shipment containing the given specimen.
     * @param specimen the specimen
     * @return the shipment containing the given specimen
     */
    Shipment getShipmentWithSpecimen(Specimen specimen);

    /**
     * Marks a shipment as shipped and notifies the requestor that their shipment has been shipped.
     * @param order The shipment being shipped.
     * @throws MessagingException on error
     */
    void markOrderAsShipped(Shipment order) throws MessagingException;

    /**
     * Returns the signed mta for the sending institution that should
     * be associated with this shipment, if one exists.
     * @param shipment Shipment for which an mta will be returned.
     * @return the appropriate signed mta for the sending institution.
     */
    SignedMaterialTransferAgreement getSignedSenderMta(Shipment shipment);

    /**
     * Returns the signed mta for the recipient institution that should
     * be associated with this shipment, if one exists.
     * @param shipment Shipment for which an mta will be returned.
     * @return the appropriate signed mta for the recipient institution.
     */
    SignedMaterialTransferAgreement getSignedRecipientMta(Shipment shipment);

    /**
     * Cancels and order and notifies the requestor that their order has been canceled.
     * @param order The order being canceled.
     * @throws MessagingException on error
     */
    void cancelOrder(Shipment order) throws MessagingException;

    /**
     * Gets orders that have been shipped but have not been marked received.
     * @param gracePeriod the grace period to allow between shipment and receipt.
     * @return the list of orders.
     */
    List<Shipment> getUnreceivedShippedOrders(int gracePeriod);

    /**
     * Save an order and notify the requestor if the order is ready for review.
     * @param order the order to save
     * @throws MessagingException on error
     */
    void saveOrder(Shipment order) throws MessagingException;

    /**
     * Get the number of orders that have shipped within a date range.
     * @param startDate the start date of the range
     * @param endDate the end date of the range
     * @return a count of the orders that were marked as shipped between the start date and the end date
     */
    int getShippedOrderCount(Date startDate, Date endDate);
}
