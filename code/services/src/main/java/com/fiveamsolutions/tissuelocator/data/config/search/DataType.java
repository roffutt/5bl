package com.fiveamsolutions.tissuelocator.data.config.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * Data types available for use with configurable search fields.
 * @author gvaughn
 *
 */
public enum DataType {

    /**
     * Code data type.
     */
    CODE(new DataRetriever() {

        @SuppressWarnings("unchecked")
        public <T> List<T> getData(Class<T> dataClass) {
            CodeServiceLocal codeService = TissueLocatorRegistry.getServiceLocator().getCodeService();
            Class<? extends Code> codeClass = (Class<? extends Code>) dataClass;
            List<T> data = new ArrayList<T>();
            data.addAll((List<T>) codeService.getActiveCodes(codeClass));
            return data;
        }

        public String getValueString(Object object) {
            if (Code.class.isAssignableFrom(object.getClass())) {
                return ((Code) object).getName();
            }
            return object.toString();
        }

        public <T> Collection<String> getValueStrings(Collection<Object> objects, Class<T> dataClass) {
            Collection<String> values = new ArrayList<String>();
            List<T> codes = getData(dataClass);
            for (Object object : objects) {
                for (T code : codes) {
                    if (object.equals(((Code) code).getName())) {
                        values.add(((Code) code).getName());
                        break;
                    }
                }
            }
            return values;
        }
    }),

    /**
     * Enumeration data type.
     */
    ENUM(new DataRetriever() {

        public <T> List<T> getData(Class<T> dataClass) {
            List<T> data = new ArrayList<T>();
            data.addAll(Arrays.asList(dataClass.getEnumConstants()));
            return data;
        }

        public String getValueString(Object object) {
            try {
                String resourceKey = (String) PropertyUtils.getProperty(object, "resourceKey");
                return new EmailHelper().getResourceBundle().getString(resourceKey);
            } catch (Exception e) {
                LOG.error(e);
                return object.toString();
            }
        }

        public <T> Collection<String> getValueStrings(Collection<Object> objects, Class<T> dataType) {
            Collection<String> values = new ArrayList<String>();
            EmailHelper emailHelper = new EmailHelper();
            for (Object object : objects) {
                String resourceKey;
                try {
                    resourceKey = (String) PropertyUtils.getProperty(object, "resourceKey");
                    values.add(emailHelper.getResourceBundle().getString(resourceKey));
                } catch (Exception e) {
                    LOG.error(e);
                    values.add(object.toString());
                }
            }
            return values;
        }
    }),

    /**
     * Institution data type.
     */
    @SuppressWarnings("unchecked")
    INSTITUTION(new DataRetriever() {

        public <T> List<T> getData(Class<T> dataClass) {
            InstitutionServiceLocal service = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
            List<T> data = new ArrayList<T>();
            data.addAll((List<T>) service.getConsortiumMembers());
            Collections.sort(data, new Comparator<T>() {

                public int compare(T o1, T o2) {
                    return ((Institution) o1).getName().compareTo(((Institution) o2).getName());
                }

            });
            return data;
        }

        public String getValueString(Object object) {
            return ((Institution) object).getName();
        }

        public <T> Collection<String> getValueStrings(Collection<Object> objects, Class<T> dataClass) {
            Collection<String> values = new ArrayList<String>();
            List<T> institutions = getData(dataClass);
            for (Object object : objects) {
                for (T institution : institutions) {
                    if (object.equals(((Institution) institution).getId())) {
                        values.add(((Institution) institution).getName());
                        break;
                    }
                }
            }
            return values;
        }
    });

    /**
     * Returns a list of data for search field configurations.
     * @author gvaughn
     *
     */
    public interface DataRetriever {

        /**
         * Returns a list of data for search field configurations.
         * @param <T> Class type.
         * @param dataClass Class name of the data to be returned.
         * @return a list of data for search field configurations.
         */
        <T> List<T> getData(Class<T> dataClass);

        /**
         * @param object the object to convert to a string
         * @return a string representation of an object retrieved by this data retriever
         */
        String getValueString(Object object);

        /**
         * @param <T> Class type for this data type.
         * @param objects Objects from which values will be retrieved.
         * @param dataClass Class type for this data type.
         * @return String representations of the objects retrieved by this data retriever.
         */
        <T> Collection<String> getValueStrings(Collection<Object> objects, Class<T> dataClass);
    }

    private static final Logger LOG = Logger.getLogger(DataType.class);

    private DataRetriever dataRetriever;

    DataType(DataRetriever dataRetriever) {
        this.dataRetriever = dataRetriever;
    }

    /**
     * Returns a list of data corresponding to this data type.
     * @param <T> Class type to be returned.
     * @param dataClass Class name of the data to be returned, if applicable.
     * @return a list of data corresponding to this data type.
     */
    public <T> List<T> getData(Class<T> dataClass) {
        return dataRetriever.getData(dataClass);
    }

    /**
     * @return the data retriever for this data type
     */
    public DataRetriever getDataRetriever() {
        return this.dataRetriever;
    }
}
