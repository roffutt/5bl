/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.search.SearchableUtils;

/**
 * Range search condition.
 * 
 * @author jstephens
 */
public class RangeSearchCondition extends AbstractSearchCondition {
    
    private static final long serialVersionUID = 1L;

    //CHECKSTYLE:OFF - Missing javadoc comments.
    public static final String RANGE_MIN_PREFIX   = "min_";
    public static final String RANGE_MAX_PREFIX   = "max_";
    public static final String RANGE_PREFIX_REGEX = "^(min|max)_";
    //CHECKSTYLE:ON

    /**
     * Gets the name of the property corresponding to the minimum value in the range.
     *
     * @return the property name of the minimum value in the range.
     */
    public String getMinPropertyName() {
        return RANGE_MIN_PREFIX + getPropertyName();
    }

    /**
     * Gets the name of the property corresponding to the maximum value in the range.
     *
     * @return the property name of the maximum value in the range.
     */
    public String getMaxPropertyName() {
        return RANGE_MAX_PREFIX + getPropertyName();
    }

    /**
     * Gets the minimum value of the property in the range search.
     *
     * @return the minimum property value in the range search
     */
    public Object getMinPropertyValue() {
        return getConditionParameters().get(getMinPropertyName());
    }

    /**
     * Gets the maximum value of the property in the range search.
     *
     * @return the maximum property value in the range search
     */
    public Object getMaxPropertyValue() {
        return getConditionParameters().get(getMaxPropertyName());
    }

    /**
     * Appends this search condition to a query.
     *
     * @param object the example object
     * @param whereClause the query where clause
     * @param bindParameters the query bind parameters
     */
    @Override
    public void appendCondition(Object object, StringBuffer whereClause, Map<String, Object> bindParameters) {
        addStatement(whereClause, ">=", getMinPropertyName(), getMinPropertyValue(), bindParameters);
        addStatement(whereClause, "<=", getMaxPropertyName(), getMaxPropertyValue(), bindParameters);
    }

    private void addStatement(StringBuffer whereClause,
        String operator, String parameterName, Object parameterValue, Map<String, Object> bindParameters) {
        if (parameterValue != null && StringUtils.isNotBlank(parameterValue.toString())) {
            String condition =
                String.format("%s.%s %s :%s",
                    SearchableUtils.ROOT_OBJ_ALIAS,
                    getPropertyName(),
                    operator,
                    parameterName);

            whereClause.append(whereOrAnd(whereClause));
            whereClause.append(condition);
            
            bindParameters.put(parameterName, parameterValue);
        }
    }

}