/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.search.SearchableUtils;

/**
 * Abstract search condition.
 * 
 * @author jstephens
 */
@SuppressWarnings({ "PMD.AvoidDeeplyNestedIfStmts" })
public abstract class AbstractSearchCondition implements SearchCondition, Serializable {

    private static final long serialVersionUID = 1L;
    
    private Map<String, Object> conditionParameters = new HashMap<String, Object>();
    private String propertyName;

    /**
     * Appends this search condition to a query.
     *
     * @param object the example object.
     * @param whereClause the query where clause.
     * @param queryParameters the query parameters.
     */
    @Override
    public abstract void appendCondition(Object object, StringBuffer whereClause, Map<String, Object> queryParameters);

    /**
     * Gets the name of the property on which the search condition applies.
     *
     * @return the property name on which the search condition applies
     */
    @Override
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * Sets the name of the property on which the search condition applies.
     *
     * @param propertyName the property name on which the search condition applies
     */
    @Override
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * Gets the map of parameters associated with the search condition.
     *
     * @return the map of the search condition parameters
     */
    @Override
    public Map<String, Object> getConditionParameters() {
        return conditionParameters;
    }

    /**
     * Sets the map of parameters associated with the search condition.
     *
     * @param conditionParameters the map of the search condition parameters
     */
    @Override
    public void setConditionParameters(Map<String, Object> conditionParameters) {
        this.conditionParameters = conditionParameters;
    }

    /**
     * Gets the appropriate conjunction for the where clause based on whether or
     * not it already has conditions.
     *
     * @param whereClause the where clause
     * @return the appropriate conjunction
     */
    @SuppressWarnings({ "PMD.OnlyOneReturn" })
    protected String whereOrAnd(StringBuffer whereClause) {
        if (StringUtils.isBlank(whereClause.toString())) {
            return SearchableUtils.WHERE;
        }
        return SearchableUtils.AND;
    }

}