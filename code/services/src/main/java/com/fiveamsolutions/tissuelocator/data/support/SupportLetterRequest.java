/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.support;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.Future;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;
import org.hibernate.validator.Valid;

import com.fiveamsolutions.nci.commons.audit.Auditable;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.search.Searchable;
import com.fiveamsolutions.tissuelocator.data.AccessRestricted;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.StatusTransitionable;
import com.fiveamsolutions.tissuelocator.data.Timestampable;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.annotation.OnSave;
import com.fiveamsolutions.tissuelocator.data.code.FundingSource;
import com.fiveamsolutions.tissuelocator.data.validation.NotFuture;
import com.fiveamsolutions.tissuelocator.data.validation.support.SupportLetterRequestStatus;
import com.fiveamsolutions.tissuelocator.data.validation.support.SupportLetterRequestType;
import com.fiveamsolutions.tissuelocator.util.SupportLetterRequestStatusTransitionHistoryHandler;

/**
 * @author ddasgupta
 */
@Entity
@SupportLetterRequestStatus
@SupportLetterRequestType
@Table(name = "support_letter_request")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@SuppressWarnings("PMD.TooManyFields")
public class SupportLetterRequest implements PersistentObject, Auditable, AccessRestricted,
    StatusTransitionable<SupportLetterRequestStatusTransition, SupportRequestStatus>, Timestampable {

    private static final long serialVersionUID = -6657214337420610857L;
    private static final String NAME = "name";
    private static final String CONTENT_TYPE = "contentType";
    private static final String LOB = "lob";
    private static final int LENGTH = 254;
    private static final int COMMENTS_LENGTH = 3999;

    private Long id;
    private TissueLocatorUser requestor;
    private Person investigator;
    private TissueLocatorFile resume;

    private SupportLetterType type;
    private String grantType;
    private List<FundingSource> fundingSources;
    private String otherType;

    private TissueLocatorFile draftProtocol;
    private TissueLocatorFile draftLetter;
    private Date intendedSubmissionDate;
    private String comments;

    private String response;
    private TissueLocatorFile letter;
    private String offlineResponseNotes;

    private SupportRequestStatus status;
    private SupportRequestStatus previousStatus;
    private Date createdDate = new Date();
    private Date lastUpdatedDate = new Date();
    private Date nextReminderDate;

    private List<SupportLetterRequestStatusTransition> statusTransitionHistory
        = new ArrayList<SupportLetterRequestStatusTransition>();
    private Date statusTransitionDate;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the requestor
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "requestor_id")
    @ForeignKey(name = "support_letter_request_requestor_fk")
    @Index(name = "support_letter_request_requestor_idx")
    @Searchable
    public TissueLocatorUser getRequestor() {
        return requestor;
    }

    /**
     * @param requestor the requestor to set
     */
    public void setRequestor(TissueLocatorUser requestor) {
        this.requestor = requestor;
    }

    /**
     * @return the investigator
     */
    @Valid
    @NotNull
    @ManyToOne
    @JoinColumn(name = "investigator_id")
    @ForeignKey(name = "support_letter_request_investigator_fk")
    @Index(name = "support_request_investigator_idx")
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    public Person getInvestigator() {
        return investigator;
    }

    /**
     * @param investigator the investigator to set
     */
    public void setInvestigator(Person investigator) {
        this.investigator = investigator;
    }

    /**
     * @return the resume
     */
    @NotNull
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = CONTENT_TYPE, column = @Column(name = "resume_content_type")),
        @AttributeOverride(name = NAME, column = @Column(name = "resume_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = LOB, joinColumns = @JoinColumn(name = "resume_lob_id"))
    })
    public TissueLocatorFile getResume() {
        return resume;
    }

    /**
     * @param resume the resume to set
     */
    public void setResume(TissueLocatorFile resume) {
        this.resume = resume;
    }

    /**
     * @return the type
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    public SupportLetterType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(SupportLetterType type) {
        this.type = type;
    }

    /**
     * @return the grantType
     */
    @Column(name = "grant_type")
    @Length(max = LENGTH)
    public String getGrantType() {
        return grantType;
    }

    /**
     * @param grantType the grantType to set
     */
    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    /**
     * @return the fundingSources
     */
    @ManyToMany
    @JoinTable(name = "support_letter_request_funding_source",
                joinColumns = @JoinColumn(name = "request_id"),
                inverseJoinColumns = @JoinColumn(name = "funding_source_id"))
    @ForeignKey(name = "SUPPORT_LETTER_REQUEST_FUNDING_SOURCE_FK",
                inverseName = "FUNDING_SOURCE_SUPPORT_LETTER_REQUEST_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @OrderBy(value = "id")
    public List<FundingSource> getFundingSources() {
        return fundingSources;
    }

    /**
     * @param fundingSources the fundingSources to set
     */
    public void setFundingSources(List<FundingSource> fundingSources) {
        this.fundingSources = fundingSources;
    }

    /**
     * @return the otherType
     */
    @Column(name = "other_type")
    @Length(max = LENGTH)
    public String getOtherType() {
        return otherType;
    }

    /**
     * @param otherType the otherType to set
     */
    public void setOtherType(String otherType) {
        this.otherType = otherType;
    }

    /**
     * @return the draftProtocol
     */
    @NotNull
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = CONTENT_TYPE, column = @Column(name = "draft_protocol_content_type")),
        @AttributeOverride(name = NAME, column = @Column(name = "draft_protocol_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = LOB, joinColumns = @JoinColumn(name = "draft_protocol_lob_id"))
    })
    public TissueLocatorFile getDraftProtocol() {
        return draftProtocol;
    }

    /**
     * @param draftProtocol the draftProtocol to set
     */
    public void setDraftProtocol(TissueLocatorFile draftProtocol) {
        this.draftProtocol = draftProtocol;
    }

    /**
     * @return the draftLetter
     */
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = CONTENT_TYPE, column = @Column(name = "draft_letter_content_type")),
        @AttributeOverride(name = NAME, column = @Column(name = "draft_letter_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = LOB, joinColumns = @JoinColumn(name = "draft_letter_lob_id"))
    })
    public TissueLocatorFile getDraftLetter() {
        return draftLetter;
    }

    /**
     * @param draftLetter the draftLetter to set
     */
    public void setDraftLetter(TissueLocatorFile draftLetter) {
        this.draftLetter = draftLetter;
    }

    /**
     * @return the intendedSubmissionDate
     */
    @Future
    @Temporal(TemporalType.DATE)
    @Column(name = "intended_submission_date")
    public Date getIntendedSubmissionDate() {
        return intendedSubmissionDate;
    }

    /**
     * @param intendedSubmissionDate the intendedSubmissionDate to set
     */
    public void setIntendedSubmissionDate(Date intendedSubmissionDate) {
        this.intendedSubmissionDate = intendedSubmissionDate;
    }

    /**
     * @return the comments
     */
    @Length(max = COMMENTS_LENGTH)
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the response
     */
    @Length(max = COMMENTS_LENGTH)
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return the letter
     */
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = CONTENT_TYPE, column = @Column(name = "letter_content_type")),
        @AttributeOverride(name = NAME, column = @Column(name = "letter_name"))
    })
    @AssociationOverrides({
        @AssociationOverride(name = LOB, joinColumns = @JoinColumn(name = "letter_lob_id"))
    })
    public TissueLocatorFile getLetter() {
        return letter;
    }

    /**
     * @param letter the letter to set
     */
    public void setLetter(TissueLocatorFile letter) {
        this.letter = letter;
    }

    /**
     * @return the offlineResponseNotes
     */
    @Length(max = COMMENTS_LENGTH)
    @Column(name = "offline_response_notes")
    public String getOfflineResponseNotes() {
        return offlineResponseNotes;
    }

    /**
     * @param offlineResponseNotes the offlineResponseNotes to set
     */
    public void setOfflineResponseNotes(String offlineResponseNotes) {
        this.offlineResponseNotes = offlineResponseNotes;
    }

    /**
     * @return the status
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    @Searchable
    @Index(name = "support_letter_request_status_idx")
    public SupportRequestStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(SupportRequestStatus status) {
        this.status = status;
    }

    /**
     * @return the specimen's status
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "status", updatable = false, insertable = false, nullable = false)
    public SupportRequestStatus getPreviousStatus() {
        return previousStatus;
    }

    /**
     * @param previousStatus the status to set
     */
    public void setPreviousStatus(SupportRequestStatus previousStatus) {
        this.previousStatus = previousStatus;
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @NotFuture
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @NotFuture
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated_date")
    @Override
    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return the nextReminderDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "next_reminder_date")
    public Date getNextReminderDate() {
        return nextReminderDate;
    }

    /**
     * @param nextReminderDate the nextReminderDate to set
     */
    public void setNextReminderDate(Date nextReminderDate) {
        this.nextReminderDate = nextReminderDate;
    }

    /**
     * @return the status transitions.
     */
    @Fetch(FetchMode.SELECT)
    @ManyToMany(fetch = FetchType.EAGER)
    @Valid
    @JoinTable(name = "support_letter_request_status_transition_history",
                joinColumns = @JoinColumn(name = "request_id"),
                inverseJoinColumns = @JoinColumn(name = "status_transition_id"))
    @ForeignKey(name = "SUPPORT_LETTER_REQUEST_TRANSITION_FK",
                inverseName = "TRANSITION_SUPPORT_LETTER_REQUEST_FK")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Cascade(value = CascadeType.ALL)
    @OrderBy(value = "systemTransitionDate")
    public List<SupportLetterRequestStatusTransition> getStatusTransitionHistory() {
        return statusTransitionHistory;
    }

    /**
     * @param statusTransitionHistory the status transitions.
     */
    public void setStatusTransitionHistory(List<SupportLetterRequestStatusTransition> statusTransitionHistory) {
        this.statusTransitionHistory = statusTransitionHistory;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public Date getStatusTransitionDate() {
        return statusTransitionDate;
    }

    /**
     * {@inheritDoc}
     */
    public void setStatusTransitionDate(Date statusTransitionDate) {
        this.statusTransitionDate = statusTransitionDate;
    }

    /**
     * Update the status transition history based on the current and previous status.
     */
    @OnSave
    public void updateStatusTransitionHistory() {
        new SupportLetterRequestStatusTransitionHistoryHandler().updateStatusTransitionHistory(this);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isAccessible(TissueLocatorUser user) {
        if (getRequestor().getUsername().equals(user.getUsername())) {
            return true;
        }
        if (user.inRole(Role.SUPPORT_LETTER_REQUEST_REVIEWER)) {
            return true;
        }
        return false;
    }
}
