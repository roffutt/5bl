/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.data.config.search;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;

/**
 * Base class for search field configuration objects.
 * @author gvaughn
 *
 */
@MappedSuperclass
public abstract class AbstractSearchFieldConfig implements PersistentObject {

    private static final long serialVersionUID = 7885364429484995753L;
    /**
     * Max length of String fields.
     */
    protected static final int MAX_LENGTH = 255;

    /**
     * The connector between the criteria name and criteria value.
     */
    protected static final String CONNECTOR = ": ";

    /**
     * Delimiter for criteria values with multiple elements.
     */
    protected static final String DELIMITER = ", ";

    private Long id;
    private SearchSetType searchSetType;
    private boolean filterCriteriaString;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the name of the property being searched. This property is overridden
     * by search configuration subclasses.
     *
     * @return the name of the property being searched.
     */
    @Transient
    public abstract String getSearchFieldName();

    /**
     * Resource key for the search field display name. This property is overridden
     * by search configuration subclasses.
     *
     * @return Resource key for the search field display name.
     */
    @Transient
    public abstract String getSearchFieldDisplayName();

    /**
     * Returns the ApplicationRole required in order to view search field.
     * @return The ApplicationRole required in order to view search field.
     */
    @Transient
    public abstract ApplicationRole getRequiredRole();
    
    /**
     * Returns the generic field configuration associated
     * with this search field configuration.
     * @return the generic field configuration associated
     * with this search field configuration.
     */
    @Transient
    public abstract GenericFieldConfig getFieldConfig();
    
    /**
     * The search set to which this config belongs.
     * @return The search set to which this config belongs.
     */
    @NotNull
    @Column(name = "search_field_set")
    @Enumerated(EnumType.STRING)
    public SearchSetType getSearchSetType() {
        return searchSetType;
    }

    /**
     * @param searchSetType The search set to which this config belongs.
     */
    public void setSearchSetType(SearchSetType searchSetType) {
        this.searchSetType = searchSetType;
    }

    /**
     * @param object the object containing the criteria value
     * @param mapName If the indicated property is contained in a map (not named in the property),
     * the name of the map property.
     * @return a string representation of the criteria defined by this search field config.
     */
    public String getCriteriaString(Object object, String mapName) {
        return StringUtils.EMPTY;
    }

    /**
     * Retrieves the current value(s) for this search field from the given object.
     * @param object the object containing the search field value.
     * @param mapName If the indicated property is contained in a map (not named in the property),
     * the name of the map property.
     * @return Mapping of search config to value.
     */
    public Map<AbstractSearchFieldConfig, Object> getValues(Object object, String mapName) {
        // no-op - subclasses must implement appropriately
        return new HashMap<AbstractSearchFieldConfig, Object>();
    }

    /**
     * Gets the name of the optional search condition class used by the search criteria.
     * Configurations that contain a search condition must override this method.
     *
     * @return the search condition class name
     */
    @Transient
    public String getSearchConditionClass() {
        return StringUtils.EMPTY;
    }

    /**
     * @return the filterCriteriaString
     */
    @NotNull
    @Column(name = "filter_criteria_string")
    public boolean isFilterCriteriaString() {
        return filterCriteriaString;
    }

    /**
     * @param filterCriteriaString the filterCriteriaString to set
     */
    public void setFilterCriteriaString(boolean filterCriteriaString) {
        this.filterCriteriaString = filterCriteriaString;
    }
}