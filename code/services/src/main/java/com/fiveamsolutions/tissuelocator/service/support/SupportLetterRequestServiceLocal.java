/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.support;

import java.util.List;

import javax.ejb.Local;

import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;

/**
 * @author ddasgupta
 *
 */
@Local
public interface SupportLetterRequestServiceLocal extends GenericServiceLocal<SupportLetterRequest> {

    /**
     * Save a new request for a letter of support.
     * @param request the request to save
     * @return the id of the newly saved request
     */
    Long saveSupportLetterRequest(SupportLetterRequest request);

    /**
     * Review a request for a letter of support.
     * @param request the request being reviewed
     * @return the id of the reviewed request
     */
    Long reviewSupportLetterRequest(SupportLetterRequest request);

    /**
     * Get the support letter requests that haven't been reviewed within a given grace period.
     * @param gracePeriod the grace period during which reminders are not sent.
     * @return a list of unreviewed support letter requests
     */
    List<SupportLetterRequest> getUnreviewedRequests(int gracePeriod);

    /**
     * Update a request for a letter of support.
     * @param request the request to update
     * @return the id of the request
     */
    Long updateSupportLetterRequest(SupportLetterRequest request);
}
