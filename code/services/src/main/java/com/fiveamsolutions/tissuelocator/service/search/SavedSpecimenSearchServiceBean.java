/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import java.io.IOException;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.SavedSpecimenSearch;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;

/**
 * Class for managing the saved specimen searches.
 *
 * @author smiller
 */
@Stateless
public class SavedSpecimenSearchServiceBean extends GenericServiceBean<SavedSpecimenSearch>
        implements SavedSpecimenSearchServiceLocal {

    /**
     * {@inheritDoc}
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Long persistSavedSpecimenSearch(SavedSpecimenSearch search, SpecimenSearchCriteria criteria)
            throws IOException {
        search.setSearchCriteria(criteria);
        return savePersistentObject(search);
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SavedSpecimenSearch getSavedSpecimenSearchAndData(Long id) {
        SavedSpecimenSearch search = super.getPersistentObject(SavedSpecimenSearch.class, id);
        // force the lazy loading.
        search.getCriteriaData().getData();
        search.getOwner().getId();
        return search;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<SavedSpecimenSearch> getSavedSpecimenSearchesByOwner(TissueLocatorUser owner) {
        SavedSpecimenSearch example = new SavedSpecimenSearch();
        example.setOwner(owner);
        SearchCriteria<SavedSpecimenSearch> criteria =
            new TissueLocatorAnnotatedBeanSearchCriteria<SavedSpecimenSearch>(example);
        return search(criteria);
    }

}