/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.report;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author ddasgupta
 *
 */
public class ReportDataProvider {

    private final String queryString;
    private final Map<String, Object> queryParameters;

    /**
     * @param queryString the query string
     * @param queryParameters the query parameters
     */
    public ReportDataProvider(String queryString, Map<String, Object> queryParameters) {
        this.queryString = queryString;
        this.queryParameters = queryParameters;
    }

    /**
     * Return a page of report data.
     * @param firstResult the first result to be returned
     * @param maxResults the maximum number of results to be returned
     * @return a page of objects from the query results
     */
    @SuppressWarnings("unchecked")
    public List<PersistentObject> getReportData(int firstResult, int maxResults) {
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        for (Map.Entry<String, Object> queryParam : queryParameters.entrySet()) {
            q.setParameter(queryParam.getKey(), queryParam.getValue());
        }
        q.setFirstResult(firstResult);
        q.setMaxResults(maxResults);
        return q.list();
    }
}
