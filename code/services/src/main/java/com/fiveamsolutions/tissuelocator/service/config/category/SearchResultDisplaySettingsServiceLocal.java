/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.config.category;

import java.util.Collection;
import java.util.List;

import javax.ejb.Local;

import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.category.SearchResultFieldDisplaySetting;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;

/**
 * @author cgoina
 *
 */
@Local
public interface SearchResultDisplaySettingsServiceLocal extends GenericServiceLocal<SearchResultFieldDisplaySetting> {

    /**
     * Retrieve search result display settings by field category name.
     *
     * @return the list of display settings for the fields from the specified category
     */
    List<SearchResultFieldDisplaySetting> getDefaultSearchResultDisplaySettings();

    /**
     * Retrieve search result display settings by field category name for the specified user.
     *
     * @param userName the user name for which to retrieve the preferences
     * @return the list of display settings for the fields from the specified category for the specified user
     */
    List<SearchResultFieldDisplaySetting> getUserSearchResultDisplaySettings(String userName);

    /**
     * Store the specified field display preferences for the specified user.
     *
     * @param user name of the user
     * @param fieldDisplaySettings field display options
     */
    void storeUserSearchResultDisplaySettings(TissueLocatorUser user,
            Collection<SearchResultFieldDisplaySetting> fieldDisplaySettings);

}
