/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;




/**
 * The United States and territories.
 * 
 * @author ddasgupta
 * 
 */
public enum State {

    /** Alabama. */
    ALABAMA("state.ALABAMA"),

    /** Alaska. */
    ALASKA("state.ALASKA"),

    /** American Samoa. */
    AMERICAN_SAMOA("state.AMERICAN_SAMOA"),

    /** Arizona. */
    ARIZONA("state.ARIZONA"),

    /** Arkansas. */
    ARKANSAS("state.ARKANSAS"),

    /** California. */
    CALIFORNIA("state.CALIFORNIA"),

    /** Colorado. */
    COLORADO("state.COLORADO"),

    /** Connecticut. */
    CONNECTICUT("state.CONNECTICUT"),

    /** Deleware. */
    DELAWARE("state.DELAWARE"),

    /** DC. */
    DISTRICT_OF_COLUMBIA("state.DISTRICT_OF_COLUMBIA"),

    /** Federated States of Micronesia. */
    FEDERATED_STATES_OF_MICRONESIA("state.FEDERATED_STATES_OF_MICRONESIA"),

    /** Florida. */
    FLORIDA("state.FLORIDA"),

    /** Georgia. */
    GEORGIA("state.GEORGIA"),

    /** Guam. */
    GUAM("state.GUAM"),

    /** Hawaii. */
    HAWAII("state.HAWAII"),

    /** Idaho. */
    IDAHO("state.IDAHO"),

    /** Illinois. */
    ILLINOIS("state.ILLINOIS"),

    /** Indiana. */
    INDIANA("state.INDIANA"),

    /** Iowa. */
    IOWA("state.IOWA"),

    /** Kansas. */
    KANSAS("state.KANSAS"),

    /** Kentucky. */
    KENTUCKY("state.KENTUCKY"),

    /** Lousiana. */
    LOUISIANA("state.LOUISIANA"),

    /** Maine. */
    MAINE("state.MAINE"),

    /** Marshall Islands. */
    MARSHALL_ISLANDS("state.MARSHALL_ISLANDS"),

    /** Maryland. */
    MARYLAND("state.MARYLAND"),

    /** Massachusetts. */
    MASSACHUSETTS("state.MASSACHUSETTS"),

    /** Michigan. */
    MICHIGAN("state.MICHIGAN"),

    /** Minnesota. */
    MINNESOTA("state.MINNESOTA"),

    /** Mississippi. */
    MISSISSIPPI("state.MISSISSIPPI"),

    /** Missouri. */
    MISSOURI("state.MISSOURI"),

    /** Montana. */
    MONTANA("state.MONTANA"),

    /** Nebraska. */
    NEBRASKA("state.NEBRASKA"),

    /** Nevada. */
    NEVADA("state.NEVADA"),

    /** New Hampshire. */
    NEW_HAMPSHIRE("state.NEW_HAMPSHIRE"),

    /** New Jersey. */
    NEW_JERSEY("state.NEW_JERSEY"),

    /** New Mexico. */
    NEW_MEXICO("state.NEW_MEXICO"),

    /** New York. */
    NEW_YORK("state.NEW_YORK"),

    /** North Carolina. */
    NORTH_CAROLINA("state.NORTH_CAROLINA"),

    /** North Dakota. */
    NORTH_DAKOTA("state.NORTH_DAKOTA"),

    /** Northern Mariana Islands. */
    NORTHERN_MARIANA_ISLANDS("state.NORTHERN_MARIANA_ISLANDS"),

    /** Ohio. */
    OHIO("state.OHIO"),

    /** Oklahoma. */
    OKLAHOMA("state.OKLAHOMA"),

    /** Oregon. */
    OREGON("state.OREGON"),

    /** Palau. */
    PALAU("state.PALAU"),

    /** Pennsylvania. */
    PENNSYLVANIA("state.PENNSYLVANIA"),

    /** Puerto Rico. */
    PUERTO_RICO("state.PUERTO_RICO"),

    /** Rhode Island. */
    RHODE_ISLAND("state.RHODE_ISLAND"),

    /** South Carolina. */
    SOUTH_CAROLINA("state.SOUTH_CAROLINA"),

    /** South Dakota. */
    SOUTH_DAKOTA("state.SOUTH_DAKOTA"),

    /** Tennessee. */
    TENNESSEE("state.TENNESSEE"),

    /** Texas. */
    TEXAS("state.TEXAS"),

    /** Utah. */
    UTAH("state.UTAH"),

    /** Vermont. */
    VERMONT("state.VERMONT"),

    /** Virgin Islands. */
    VIRGIN_ISLANDS("state.VIRGIN_ISLAND"),

    /** Virginia. */
    VIRGINIA("state.VIRGINIA"),

    /** Washington. */
    WASHINGTON("state.WASHINGTON"),

    /** West Virginia. */
    WEST_VIRGINIA("state.WEST_VIRGINIA"),

    /** Wisconsin. */
    WISCONSIN("state.WISCONSIN"),

    /** Wyoming. */
    WYOMING("state.WYOMING");

    private String resourceKey;

    private State(String key) {
        this.resourceKey = key;
    }

    /**
     * @return the resourceKey
     */
    public String getResourceKey() {
        return resourceKey;
    }

    /**
     * Array of all states considered us states.
     */
    private static final State[] US_STATES = {ALABAMA, ALASKA, ARIZONA, ARKANSAS, CALIFORNIA, COLORADO, 
            CONNECTICUT, DELAWARE,
            DISTRICT_OF_COLUMBIA, FLORIDA, GEORGIA, HAWAII, IDAHO, ILLINOIS, INDIANA, IOWA, KANSAS, KENTUCKY,
            LOUISIANA, MAINE, MARYLAND, MASSACHUSETTS, MICHIGAN, MINNESOTA, MISSISSIPPI, MISSOURI, MONTANA, NEBRASKA,
            NEVADA, NEW_HAMPSHIRE, NEW_JERSEY, NEW_MEXICO, NEW_YORK, NORTH_CAROLINA, NORTH_DAKOTA, OHIO, OKLAHOMA,
            OREGON, PENNSYLVANIA, RHODE_ISLAND, SOUTH_CAROLINA, SOUTH_DAKOTA, TENNESSEE, TEXAS, UTAH, VERMONT,
            VIRGINIA, WASHINGTON, WEST_VIRGINIA, WISCONSIN, WYOMING};

    /**
     * get the states considered us states.
     * @return array of <code>State</code> enums.
     */
    public static State[] usStates() {
        return US_STATES.clone();
    }

}
