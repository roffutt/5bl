/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.validation;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author ddasgupta
 *
 */
public class UniqueConstraintValidatorHelper {

    private static final Logger LOG = Logger.getLogger(UniqueConstraintValidator.class);
    private String[] propertyNames;

    /**
     * @param propertyNames the propertyNames to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setPropertyNames(String[] propertyNames) {
        this.propertyNames = propertyNames;
    }

    /**
     * Whether a given object satisfies its unique constraint.
     * @param value the object to be validated
     * @return Whether a given object satisfies its unique constraint
     */
    public boolean isValid(Object value) {
        if (!(value instanceof PersistentObject)) {
            return false;
        }

        PersistentObject obj = (PersistentObject) value;
        return checkForDuplicate(value, obj);
    }

    @SuppressWarnings({"deprecation", "PMD.AvoidThrowingRawExceptionTypes" })
    private boolean checkForDuplicate(Object value, PersistentObject obj) {
        Session s = null;
        try {
            s = TissueLocatorHibernateUtil.getHibernateHelper().getSessionFactory().openSession();

            if (nullPropertiesArePresent(value)) {
                // Note this is because this validator assumes all parts of the unique key are required.
                // so in this case, the required validators for those fields will handle the error message.
                return true;
            }

            StringBuffer queryString = new StringBuffer("select obj.id from " + obj.getClass().getName()
                + " obj where ");

            addWhereClauses(obj, queryString);

            Query q = s.createQuery(queryString.toString());

            setQueryValues(obj, q);

            Long otherId = (Long) q.uniqueResult();
            return otherId == null || otherId.equals(obj.getId());
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } finally {
            Connection c = s.connection();
            s.close();
            try {
                c.close();
            } catch (SQLException e) {
                LOG.error("error closing connection", e);
            }
        }
    }

    private void setQueryValues(PersistentObject obj, Query q) throws IllegalAccessException,
            InvocationTargetException, NoSuchMethodException {
        for (String propName : propertyNames) {
            Object propValue = PropertyUtils.getProperty(obj, propName);
            if (propValue instanceof String) {
                q.setString(propName, (String) propValue);
            } else if (propValue instanceof PersistentObject) {
                q.setLong(propName, ((PersistentObject) propValue).getId());
            }
        }
    }

    private void addWhereClauses(PersistentObject obj, StringBuffer queryString) throws IllegalAccessException,
            InvocationTargetException, NoSuchMethodException {
        boolean first = true;
        for (String propName : propertyNames) {
            if (!first) {
                queryString.append(" and ");
            } else {
                first = false;
            }
            Object propValue = PropertyUtils.getProperty(obj, propName);
            if (propValue instanceof String) {
                queryString.append(" lower(" + propName + ") = lower(:" + propName + ") ");
            } else if (propValue instanceof PersistentObject) {
                queryString.append(" " + propName + ".id = :" + propName + " ");
            } else {
                throw new UnsupportedOperationException("type not supported by the unique constraint validator.");
            }
        }
    }

    private boolean nullPropertiesArePresent(Object obj)  throws IllegalAccessException,
            InvocationTargetException, NoSuchMethodException {
        for (String propName : propertyNames) {
            Object prop = PropertyUtils.getProperty(obj, propName);
            if (prop == null
                    || prop instanceof PersistentObject && ((PersistentObject) prop).getId() == null) {
                return true;
            }
        }
        return false;
    }
}
