/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.util;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.TimeUnits;

/**
 * Converts age values between time units.
 * @author gvaughn
 *
 */
@SuppressWarnings("PMD.TooManyMethods")
public class TimeConverter {

    private static final Map<TimeUnits, Map<TimeUnits, TimeConversion>> CONVERSION_MAP
        = new LinkedHashMap<TimeUnits, Map<TimeUnits, TimeConversion>>();
    private static final double MONTHS_IN_YEAR = 12;
    private static final double DAYS_IN_MONTH = 30;
    private static final double DAYS_IN_YEAR = 365;
    private static final double HOURS_IN_DAY = 24;
    
    static {
        // This approach is pretty verbose, but ensures (more) accurate conversion between years
        // and days than simply moving up or down a conversion chain.
        // Maybe that's why java devs stopped at days :-)
        
        // Years
        Map<TimeUnits, TimeConversion> conversionMap = new LinkedHashMap<TimeUnits, TimeConversion>();
        conversionMap.put(TimeUnits.MONTHS, new TimeConversion() {          
            public double convert(double timeValue) {
                return timeValue * MONTHS_IN_YEAR;
            }
        });
        conversionMap.put(TimeUnits.DAYS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue * DAYS_IN_YEAR;
            }
        });
        conversionMap.put(TimeUnits.HOURS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue * DAYS_IN_YEAR * HOURS_IN_DAY;
            }
        });
        CONVERSION_MAP.put(TimeUnits.YEARS, conversionMap);
        
        // Months
        conversionMap = new LinkedHashMap<TimeUnits, TimeConversion>();
        conversionMap.put(TimeUnits.YEARS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue / MONTHS_IN_YEAR;
            }
        });
        conversionMap.put(TimeUnits.DAYS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue * DAYS_IN_MONTH;
            }
        });
        conversionMap.put(TimeUnits.HOURS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue * DAYS_IN_MONTH * HOURS_IN_DAY;
            }
        });
        CONVERSION_MAP.put(TimeUnits.MONTHS, conversionMap);
        
        // Days
        conversionMap = new LinkedHashMap<TimeUnits, TimeConversion>();
        conversionMap.put(TimeUnits.YEARS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue / DAYS_IN_YEAR;
            }
        });
        conversionMap.put(TimeUnits.MONTHS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue / DAYS_IN_MONTH;
            }
        });
        conversionMap.put(TimeUnits.HOURS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue * HOURS_IN_DAY;
            }
        });
        CONVERSION_MAP.put(TimeUnits.DAYS, conversionMap);
        
        // Hours
        conversionMap = new LinkedHashMap<TimeUnits, TimeConversion>();
        conversionMap.put(TimeUnits.DAYS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue / HOURS_IN_DAY;
            }
        });
        conversionMap.put(TimeUnits.MONTHS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue / (HOURS_IN_DAY * DAYS_IN_MONTH);
            }
        });
        conversionMap.put(TimeUnits.YEARS, new TimeConversion() {
            public double convert(double timeValue) {
                return timeValue / (HOURS_IN_DAY * DAYS_IN_YEAR);
            }
        });
        CONVERSION_MAP.put(TimeUnits.HOURS, conversionMap);
    }
    
    /**
     * Performs a time conversion with age units implied.
     */
    interface TimeConversion {
        
        /**
         * Performs a time conversion with units implied.
         * @param timeValue Time value to be converted.
         * @return The converted time value.
         */
        double convert(double timeValue);
    }
    
    /**
     * Converts a time value from the given units to the target units.
     * @param timeValue Time value to convert.
     * @param currentUnits Units in which timeValue is represented.
     * @param targetUnits Units in which a time value will be returned.
     * @return The time value converted from the given units to the target units.
     */
    public static double convert(int timeValue, TimeUnits currentUnits, TimeUnits targetUnits) {
        if (currentUnits.equals(targetUnits)) {
            return timeValue;
        }
        return CONVERSION_MAP.get(currentUnits).get(targetUnits).convert(timeValue);
    }
    
    /**
     * Returns a mapping of time units to values for all possible
     * representations of the given time and units, including the time
     * and units given.
     * @param timeValue Time value to be converted.
     * @param currentUnits Units in which timeValue is represented.
     * @return All possible time values mapped to the corresponding units.
     */
    public static Map<TimeUnits, Double> getAllValues(int timeValue, TimeUnits currentUnits) {
        Map<TimeUnits, Double> values = new LinkedHashMap<TimeUnits, Double>();
        for (TimeUnits units : CONVERSION_MAP.keySet()) {
            values.put(units, convert(timeValue, currentUnits, units));
        }
        return values;
    }
}
