/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config.category;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.hibernate.validator.NotNull;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;

/**
 * User interface category field.
 *
 * @author jstephens
 */
@MappedSuperclass
public abstract class AbstractUiCategoryField implements PersistentObject {

    private static final long serialVersionUID = 1L;
    
    /**
     * Max string field length.
     */
    protected static final int MAX_LENGTH = 255;
    private Long id;
    private GenericFieldConfig fieldConfig;
    

    /**
     * Gets the id of the category field.
     *
     * @return the category field id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Sets the id of the category field.
     *
     * @param id the category field id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the generic field configuration for this categorized field.
     * Contains data shared among this configuration and other field-level
     * configurations for the same field.
     * @return The generic field configuration for this search field config.
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "generic_field_config_id")
    public GenericFieldConfig getFieldConfig() {
        return fieldConfig;
    }

    /**
     * Set the generic field configuration for this categorized field.
     * Contains data shared among this configuration and other field-level
     * configurations for the same field.
     * @param fieldConfig The field configuration for this display setting.
     */
    public void setFieldConfig(GenericFieldConfig fieldConfig) {
        this.fieldConfig = fieldConfig;
    }
}