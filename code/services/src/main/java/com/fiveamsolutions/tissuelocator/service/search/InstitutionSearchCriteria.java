/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service.search;

import java.util.Date;
import java.util.Map;

import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.search.SearchableUtils;
import com.fiveamsolutions.nci.commons.search.SearchableUtils.AfterIterationHelper;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;

/**
 * @author ddasgupta
 *
 */
public class InstitutionSearchCriteria extends TissueLocatorAnnotatedBeanSearchCriteria<Institution> {

    private static final long serialVersionUID = 1L;
    private final Date currentVersion;

    /**
     * @param example the example institution
     * @param currentVersion the current mta version;
     */
    public InstitutionSearchCriteria(Institution example, Date currentVersion) {
        super(example);
        this.currentVersion = currentVersion;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Query getQuery(String orderByProperty, boolean isCountOnly) {
        return getQuery(orderByProperty, null, isCountOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Query getQuery(String orderByProperty, String leftJoinClause, boolean isCountOnly) {
        return SearchableUtils.getQueryBySearchableFields(getCriteria(), isCountOnly, orderByProperty, leftJoinClause,
                getSession(), new InstitutionHelper());
    }

    /**
     * Helper that adds signed mta conditions.
     */
    private class InstitutionHelper implements AfterIterationHelper {

        /**
         * {@inheritDoc}
         */
        public void afterIteration(Object obj, boolean isCountOnly,
                                   StringBuffer whereClause, Map<String, Object> params) {
            String previousVersionCondition = "(select max(mta.id) from "
                    + SignedMaterialTransferAgreement.class.getName()
                    + " mta where mta.receivingInstitution.id = " + SearchableUtils.ROOT_OBJ_ALIAS + ".id"
                    + " and mta.originalMta.version <> :currentVersion) is not null";
            String currentVersionCondition = "(select max(mta.id) from "
                    + SignedMaterialTransferAgreement.class.getName()
                    + " mta where mta.receivingInstitution.id = " + SearchableUtils.ROOT_OBJ_ALIAS + ".id"
                    + " and mta.originalMta.version = :currentVersion "
                    + " and (mta.status = :approved or mta.status = :pending) ) is not null";
            whereClause.append(whereOrAnd(whereClause));
            whereClause.append(" ( ");
            whereClause.append(previousVersionCondition);
            whereClause.append(SearchableUtils.OR);
            whereClause.append(currentVersionCondition);
            whereClause.append(" ) ");
            params.put("currentVersion", currentVersion);
            params.put("approved", SignedMaterialTransferAgreementStatus.APPROVED);
            params.put("pending", SignedMaterialTransferAgreementStatus.PENDING_REVIEW);
        }
    }
}
