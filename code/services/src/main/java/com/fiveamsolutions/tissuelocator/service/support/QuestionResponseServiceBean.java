/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service.support;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Query;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.support.QuestionResponse;
import com.fiveamsolutions.tissuelocator.data.support.QuestionStatus;
import com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.util.EmailHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author ddasgupta
 *
 */
@Stateless
public class QuestionResponseServiceBean extends GenericServiceBean<QuestionResponse>
    implements QuestionResponseServiceLocal {

    private final EmailHelper emailHelper = new EmailHelper();

    /**
     * {@inheritDoc}
     */
    @Override
    public Long submitQuestionResponse(QuestionResponse response) {
        response.setLastUpdatedDate(new Date());
        response.setResponder(getUserService().getByUsername(UsernameHolder.getUser()));
        response.getQuestion().setStatus(QuestionStatus.FULLY_RESPONDED);
        for (QuestionResponse r : response.getQuestion().getResponses()) {
            if (SupportRequestStatus.PENDING.equals(r.getStatus())) {
                response.getQuestion().setStatus(QuestionStatus.PARTIALLY_RESPONDED);
            }
        }
        response.getQuestion().setLastUpdatedDate(response.getLastUpdatedDate());
        Long id = savePersistentObject(response);

        String[] args = new String[] {response.getQuestion().getId().toString(),
                StringUtils.defaultString(response.getResponse())};
        emailHelper.sendEmail("questionResponse.responded.requestor.email",
                new String[] {response.getQuestion().getRequestor().getEmail()}, args);
        emailHelper.sendEmail("questionResponse.responded.administrator.email", getAdminEmails(), args);

        return id;
    }

    private String[] getAdminEmails() {
        List<String> emails = new ArrayList<String>();
        Set<AbstractUser> reviewers = getUserService().getUsersInRole(Role.QUESTION_ADMINISTRATOR.getName());
        for (AbstractUser reviewer : reviewers) {
            emails.add(reviewer.getEmail());
        }
        return emails.toArray(new String[emails.size()]);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @Override
    public List<QuestionResponse> getUnrespondedQuestions(int gracePeriod) {
        Date createdDate = DateUtils.addDays(new Date(), -1 * gracePeriod);
        String hql = "from " + QuestionResponse.class.getName() + " where status = :status "
                + " and createdDate < :createdDate"
                + " and (nextReminderDate is null or nextReminderDate < :nextReminderDate)";
        Query query = TissueLocatorHibernateUtil.getCurrentSession().createQuery(hql);
        query.setParameter("status", SupportRequestStatus.PENDING);
        query.setTimestamp("createdDate", createdDate);
        query.setTimestamp("nextReminderDate", new Date());
        return query.list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long updateQuestionResponse(QuestionResponse response) {
        onSave(response);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(response);
        return response.getId();
    }
}
