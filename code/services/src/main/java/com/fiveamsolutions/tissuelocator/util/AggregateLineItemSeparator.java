/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;

/**
 * Divides aggregate line item collections into specific and general
 * line items.
 * @author gvaughn
 *
 */
public class AggregateLineItemSeparator {

    private final ApplicationSettingServiceLocal appSettingService;
    private final List<AggregateSpecimenRequestLineItem> specificLineItems =
        new ArrayList<AggregateSpecimenRequestLineItem>();
    private final List<AggregateSpecimenRequestLineItem> generalLineItems =
        new ArrayList<AggregateSpecimenRequestLineItem>();

    private static String generalPattern;


    /**
     * Initializes an AggregateLineItemSeparator.
     * @param lineItems The line items to be separated.
     * @param appSettingService the application setting service
     */
    public AggregateLineItemSeparator(Collection<AggregateSpecimenRequestLineItem> lineItems,
            ApplicationSettingServiceLocal appSettingService) {
        this.appSettingService = appSettingService;
        setPattern();
        for (AggregateSpecimenRequestLineItem li : lineItems) {
            if (li.getCriteria().matches(generalPattern)) {
                generalLineItems.add(li);
            } else {
                specificLineItems.add(li);
            }
        }
        Collections.sort(generalLineItems, getComparator());
        Collections.sort(specificLineItems, getComparator());
    }

    private Comparator<AggregateSpecimenRequestLineItem> getComparator() {
        return new Comparator<AggregateSpecimenRequestLineItem>() {
            @Override
            public int compare(AggregateSpecimenRequestLineItem li1, AggregateSpecimenRequestLineItem li2) {
                return new CompareToBuilder()
                        .append(li1.getInstitution().getName(), li2.getInstitution().getName())
                        .append(li1.getCriteria(), li2.getCriteria())
                        .toComparison();
            }
        };
    }

    private void setPattern() {
        if (StringUtils.isBlank(generalPattern)) {
            generalPattern = appSettingService.getGeneralPopulationSortRegex();
        }
    }

    /**
     * @return the specificLineItems
     */
    public List<AggregateSpecimenRequestLineItem> getSpecificLineItems() {
        return specificLineItems;
    }

    /**
     * @return the generalLineItems
     */
    public List<AggregateSpecimenRequestLineItem> getGeneralLineItems() {
        return generalLineItems;
    }
}
