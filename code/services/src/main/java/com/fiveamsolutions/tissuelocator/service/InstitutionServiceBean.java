/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Query;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author smiller
 */
@Stateless
@SuppressWarnings("PMD.AvoidDuplicateLiterals")
public class InstitutionServiceBean extends GenericServiceBean<Institution> implements InstitutionServiceLocal {

    @EJB
    private CollectionProtocolServiceLocal protocolService;

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Institution> getReviewers(int numberReviewers) {
        //Have to do this query in two different parts because different databases sort nulls differently.
        //Specifically, hsqldb puts them first and postgresql puts them last.  We want them first always.
        String queryBase = "from " + Institution.class.getName() + " where consortiumMember = true ";
        String nullQuery = queryBase + "and lastReviewAssignment is null";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(nullQuery);
        q.setMaxResults(numberReviewers);
        List<Institution> results = q.list();
        if (results.size() < numberReviewers) {
            String notNullQuery = queryBase + "and lastReviewAssignment is not null order by lastReviewAssignment";
            q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(notNullQuery);
            q.setMaxResults(numberReviewers - results.size());
            results.addAll(q.list());
        }
        return results;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<Institution> getConsortiumMembers() {
        String queryString = "from " + Institution.class.getName() + " where consortiumMember = :consortiumMember";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setBoolean("consortiumMember", true);
        return q.list();
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Institution getInstitution(Long institutionId) {
        return getPersistentObject(Institution.class, institutionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Institution getInstitutionByName(String name) {
        String queryString = "from " + Institution.class.getName() + " where name = :name";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setString("name", name);
        return (Institution) q.uniqueResult();
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Map<String, Map<Vote, Long>> getScientificReviewVoteCounts(Date startDate, Date endDate) {
        return getVoteCounts("consortiumReviews", startDate, endDate);
    }

    /**
     * {@inheritDoc}
     */
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Map<String, Map<Vote, Long>> getInstitutionalReviewVoteCounts(Date startDate, Date endDate) {
        return getVoteCounts("institutionalReviews", startDate, endDate);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Map<Long, List<String>> getScientificReviewRejectionComments(Date startDate, Date endDate, 
            String institutionName) {
        Map<Long, List<String>> comments = new HashMap<Long, List<String>>();
        String queryString = "select request.id, vote.comment"
            + " from " + SpecimenRequestReviewVote.class.getName() + " vote, "
            + SpecimenRequest.class.getName() + " request"
            + " where vote in elements(request.consortiumReviews)"
            + " and vote.institution.name = :institutionName"
            + " and vote.vote = :vote"
            + " and vote.date >= :startDate and vote.date < :endDate ";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setString("vote", Vote.DENY.name());
        q.setString("institutionName", institutionName);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        q.setDate("startDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        q.setDate("endDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        List<Object[]> results = q.list();
        for (Object[] result : results) {
            Long id = (Long) result[0];
            if (!comments.containsKey(id)) {
                comments.put(id, new ArrayList<String>());
            }
            comments.get(id).add((String) result[1]);
        }
        return comments;
    }
    
    private Map<String, Map<Vote, Long>> getVoteCounts(String voteCollection, Date startDate, Date endDate) {
        List<Object[]> queryResults = runVoteCountQuery(voteCollection, startDate, endDate);
        Map<String, Map<Vote, Long>> results = processQueryResults(queryResults);
        addMissingData(results);
        return results;
    }

    @SuppressWarnings("unchecked")
    private List<Object[]> runVoteCountQuery(String voteCollection, Date startDate, Date endDate) {
        String queryString = "select vote.institution.name, vote.vote, count(*) "
            + "from " + SpecimenRequestReviewVote.class.getName() + " vote, "
            + SpecimenRequest.class.getName() + " request "
            + " where vote in elements(request." + voteCollection + ") "
            + " and vote.vote is not null "
            + " and vote.date >= :startDate and vote.date < :endDate "
            + " group by vote.institution.name, vote.vote "
            + " order by vote.institution.name";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        q.setDate("startDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        q.setDate("endDate", DateUtils.truncate(cal.getTime(), Calendar.DATE));
        return q.list();
    }

    private Map<String, Map<Vote, Long>> processQueryResults(List<Object[]> results) {
        Map<String, Map<Vote, Long>> resultMap = new TreeMap<String, Map<Vote, Long>>();
        String currentInstitution = null;
        Map<Vote, Long> currentVoteCounts = new HashMap<Vote, Long>();
        for (Object[] result : results) {
            if (StringUtils.isNotBlank(currentInstitution) && !currentInstitution.equals(result[0])) {
                resultMap.put(currentInstitution, currentVoteCounts);
                currentVoteCounts = new HashMap<Vote, Long>();
            }

            currentInstitution = (String) result[0];
            currentVoteCounts.put((Vote) result[1], (Long) result[2]);
        }
        if (!currentVoteCounts.isEmpty()) {
            resultMap.put(currentInstitution, currentVoteCounts);
        }

        return resultMap;
    }

    private void addMissingData(Map<String, Map<Vote, Long>> results) {
        for (Institution consortiumMember : getConsortiumMembers()) {
            if (!results.containsKey(consortiumMember.getName())) {
                results.put(consortiumMember.getName(), new HashMap<Vote, Long>());
            }
        }

        for (Map<Vote, Long> voteCounts : results.values()) {
            for (Vote vote : Vote.values()) {
                if (!voteCounts.containsKey(vote)) {
                    voteCounts.put(vote, 0L);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public Long saveNewInstitution(Institution institution) {
        Long id = savePersistentObject(institution);
        if (BooleanUtils.isTrue(institution.getConsortiumMember())) {
            getProtocolService().createDefaultProtocol(institution);
        }
        return id;
    }

    /**
     * @return the protocolService
     */
    public CollectionProtocolServiceLocal getProtocolService() {
        return protocolService;
    }

    /**
     * @param protocolService the protocolService to set
     */
    public void setProtocolService(CollectionProtocolServiceLocal protocolService) {
        this.protocolService = protocolService;
    }
}
