/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data.config.category;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;

import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;

/**
 * Configuration for an individual categorized field displayed on an edit or details page.
 * @author gvaughn
 *
 */
@Entity
@Table(name = "ui_dynamic_category_field")
public class UiDynamicCategoryField extends AbstractUiCategoryField {

    private static final long serialVersionUID = 6918541008596957851L;

    private FieldType fieldType;
    private String displayableDeciderClass;
    private UiDynamicFieldCategory uiDynamicFieldCategory;

    /**
     * @return the fieldType
     */
    @NotNull
    @Column(name = "field_type")
    @Enumerated(EnumType.STRING)
    public FieldType getFieldType() {
        return fieldType;
    }

    /**
     * @param fieldType the fieldType to set
     */
    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }

    /**
     * @return the displayableDeciderClass
     */
    @Column(name = "displayable_decider_class")
    @Length(max = MAX_LENGTH)
    public String getDisplayableDeciderClass() {
        return displayableDeciderClass;
    }

    /**
     * @param displayableDeciderClass the displayableDeciderClass to set
     */
    public void setDisplayableDeciderClass(String displayableDeciderClass) {
        this.displayableDeciderClass = displayableDeciderClass;
    }

    /**
     * Gets the user interface category to which the field belongs.
     *
     * @return the user interface category
     */
    @ManyToOne
    @JoinColumn(name = "ui_dynamic_category_id")
    @ForeignKey(name = "dynamic_field_category_fk")
    public UiDynamicFieldCategory getUiDynamicFieldCategory() {
        return uiDynamicFieldCategory;
    }

    /**
     * Sets the user interface category to which the field belongs.
     *
     * @param uiDynamicFieldCategory the user interface category
     */
    public void setUiDynamicFieldCategory(UiDynamicFieldCategory uiDynamicFieldCategory) {
        this.uiDynamicFieldCategory = uiDynamicFieldCategory;
    }


    /**
     * Whether this field should be displayed.
     * @param object Object to which the field belongs.
     * @param user the current user
     * @return True if the field should be displayed, false otherwise.
     */
    @Transient
    public boolean isFieldDisplayed(Object object, TissueLocatorUser user) {
        if (getDisplayableDeciderClass() == null) {
            return true;
        }
        try {
            DisplayableDecider decider = (DisplayableDecider) Class.forName(getDisplayableDeciderClass()).newInstance();
            return decider.isFieldDisplayed(object, user);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid DisplayableDecider class name", e);
        }
    }

    /**
     * Types of categorized fields.
     * @author gvaughn
     *
     */
    public enum FieldType {

        /**
         * Standard text field.
         */
        TEXT,

        /**
         * Code field.
         */
        CODE,

        /**
         * Enum field.
         */
        ENUM,

        /**
         * Custom property field.
         */
        CUSTOM_PROPERTY,

        /**
         * Quantity field.
         */
        QUANTITY,

        /**
         * Age field.
         */
        AGE
    }
}
