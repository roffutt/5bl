alter table study add constraint UNIQUE_IRB_APPROVAL unique(irb_approval_lob_id);
alter table study add constraint UNIQUE_IRB_EXEMPTION unique(irb_exemption_lob_id);
alter table study add constraint UNIQUE_PROTOCOL_DOCUMENT unique(protocol_document_lob_id);
alter table principal_investigator add constraint UNIQUE_PI_RESUME unique(resume_lob_id);