create table search_result_field_display_user_preference (
    id int8 not null, 
    field_display_setting_id int8 not null,
    display_flag varchar(80) not null,
    user_id int8 not null, 
    primary key (id)
);

alter table search_result_field_display_user_preference 
add constraint FIELD_DISPLAY_SETTING_FK 
foreign key (field_display_setting_id) references search_result_field_display_setting;

alter table search_result_field_display_user_preference 
add constraint TISSUE_LOCATOR_USER_FK 
foreign key (user_id) references tissue_locator_user;

