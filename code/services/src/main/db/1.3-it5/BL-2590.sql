create table login_attempt (
    id int8 not null,
    user_id int8 not null,
    successful bool not null,
    date timestamp not null,
    primary key (id)
);

create index login_attempt_user_idx on login_attempt (user_id);
alter table login_attempt add constraint login_attempt_user_fk foreign key (user_id) references AbstractUser;
