create table specimen_receipt_quality (
    id int8 not null,
    active bool not null,
    name varchar(254) not null,
    value varchar(254),
    primary key (id)
);
CREATE UNIQUE INDEX specimen_receipt_quality_name_lower_unique_key on specimen_receipt_quality(lower(name));
CREATE INDEX specimen_receipt_quality_active_idx ON specimen_receipt_quality(active);
CREATE INDEX specimen_receipt_quality_name_idx ON specimen_receipt_quality(lower(name));

alter table specimen_request_line_item add column receipt_quality_id int8;
alter table specimen_request_line_item add constraint LINE_ITEM_RECEIPT_QUALITY_FK foreign key (receipt_quality_id) references specimen_receipt_quality;
create index line_item_receipt_quality_idx on specimen_request_line_item(receipt_quality_id);

alter table aggregate_specimen_request_line_item add column receipt_quality_id int8;
alter table aggregate_specimen_request_line_item add constraint aggregate_line_item_receipt_quality_fk foreign key (receipt_quality_id) references specimen_receipt_quality;
create index aggregate_line_item_receipt_quality_idx on aggregate_specimen_request_line_item(receipt_quality_id);
