insert into application_setting (id, name, value) values ((select max(id) + 1 from application_setting), 'user_resume_required', 'false');

alter table tissue_locator_user add column resume_lob_id int8;
alter table tissue_locator_user add column resume_name varchar(255);
alter table tissue_locator_user add column resume_content_type varchar(255);
alter table tissue_locator_user add constraint FK508D2AEEB55088D0 foreign key (resume_lob_id) references lob_holder;
