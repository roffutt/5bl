CREATE TABLE ui_section (
    id                  INTEGER         PRIMARY KEY,
    section_name        VARCHAR(256)    NOT NULL,
    entity_class_name   VARCHAR(256)    NOT NULL
);

CREATE TABLE ui_section_ui_dynamic_field_category (
    ui_section_id                   INTEGER     REFERENCES ui_section (id),
    ui_dynamic_field_category_id    INTEGER     REFERENCES ui_dynamic_field_category (id),
    PRIMARY KEY (ui_section_id, ui_dynamic_field_category_id)
);

CREATE TABLE ui_section_ui_search_field_category (
    ui_section_id                   INTEGER     REFERENCES ui_section (id),
    ui_search_field_category_id     INTEGER     REFERENCES ui_search_field_category (id),
    PRIMARY KEY (ui_section_id, ui_search_field_category_id)
);
