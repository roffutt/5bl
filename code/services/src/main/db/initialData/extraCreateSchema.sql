create view user_role_view as
SELECT au.username, ar.name
FROM abstractuser au, applicationrole ar, user_roles ur
WHERE au.id = ur.user_id AND ur.role_id = ar.id
UNION
SELECT au.username, ar.name
FROM abstractuser au, user_group ug, usergroup g, user_group_role ugr, applicationrole ar
WHERE au.id = ug.user_id and ug.group_id = g.id and g.id = ugr.group_id and ugr.role_id = ar.id;

create sequence AUDIT_ID_SEQ;
create unique index abstractuser_username_index on abstractuser (lower(username));
create index participant_races_participant_idx on participant_races(participant_id);
create index participant_races_race_idx on participant_races(element);

create index additional_pathologic_finding_active_idx on additional_pathologic_finding(active);
create index specimen_type_active_idx on specimen_type(active);

create index additional_pathologic_finding_name_idx on additional_pathologic_finding(name);
create index specimen_type_name_idx on specimen_type(name);


alter table institution alter column consortiummember set default false;
alter table study alter column currently_funded set default false;

alter table autocomplete_config alter column search_field_set set default 'ADVANCED';
alter table checkbox_autocomplete_composite_config alter column search_field_set set default 'ADVANCED';
alter table checkbox_config alter column search_field_set set default 'ADVANCED';
alter table range_config alter column search_field_set set default 'ADVANCED';
alter table range_select_composite_config alter column search_field_set set default 'ADVANCED';
alter table select_config alter column search_field_set set default 'ADVANCED';
alter table select_dynamic_extension_config alter column search_field_set set default 'ADVANCED';
alter table text_config alter column search_field_set set default 'ADVANCED';
alter table text_select_composite_config alter column search_field_set set default 'ADVANCED';

ALTER SEQUENCE hibernate_sequence RESTART WITH 1000;