alter table question_response add column next_reminder_date timestamp;

insert into application_setting (id, name, value) values ((select max(id) + 1 from application_setting), 'question_response_grace_period', '0');