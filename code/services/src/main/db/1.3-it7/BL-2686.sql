alter table study add column irb_approval_status varchar(255);
update study set irb_approval_status = 'APPROVED' where irb_approved = 'YES';
update study set irb_approval_status = 'EXEMPT' where exempt_from_approval = 'YES';
update study set irb_approval_status = 'NON_HUMAN_SUBJECT' where involves_human_subjects = 'YES';
update study set irb_approval_status = 'NOT_APPROVED' where id not in (select study_id from specimen_request sr where sr.status = 'DRAFT') and irb_approval_status is null;
alter table study drop column irb_approved;
alter table study drop column exempt_from_approval;
alter table study drop column involves_human_subjects;

insert into field_help_config (id, entity_class_name, field_name, help_text) values (
    nextval('hibernate_sequence'), 
    'com.fiveamsolutions.tissuelocator.data.SpecimenRequest', 
    'study.irbApprovalStatus.NON_HUMAN_SUBJECT', 
    'Research not involving human subjects is considered to be outside of human subject research. A human subject is defined by Federal Regulations as �a living individual about whom an investigator conducting research obtains (1) data through intervention or interaction with the individual, or (2) identifiable private information.� (45 CFR 46.102(f)(1),(2))'
);