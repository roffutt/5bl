create table material_transfer_agreement (
    id int8 not null,
    document_lob_id int8 not null,
    document_name varchar(255) not null,
    document_content_type varchar(255) not null,
    version varchar(254) not null unique,
    upload_time timestamp not null,
    primary key (id)
);

alter table material_transfer_agreement add constraint FK3E3CC4CE28203822 foreign key (document_lob_id) references lob_holder;
create index mta_version_idx on material_transfer_agreement (version);
create index mta_upload_time_idx on material_transfer_agreement (upload_time);


create table signed_material_transfer_agreement (
    id int8 not null,
    document_lob_id int8 not null,
    document_content_type varchar(255) not null,
    document_name varchar(255) not null,
    sending_institution_id int8,
    receiving_institution_id int8,
    original_mta_id int8 not null,
    status varchar(255) not null,
    upload_time timestamp not null,
    loader_id int8 not null,
    primary key (id)
);

alter table signed_material_transfer_agreement add constraint signed_mta_loader_fk foreign key (loader_id) references tissue_locator_user;
alter table signed_material_transfer_agreement add constraint signed_mta_sending_institution_fk foreign key (sending_institution_id) references institution;
alter table signed_material_transfer_agreement add constraint signed_mta_receiving_institution_fk foreign key (receiving_institution_id) references institution;
alter table signed_material_transfer_agreement add constraint FKDD546E2B28203822 foreign key (document_lob_id) references lob_holder;
alter table signed_material_transfer_agreement add constraint signed_mta_original_mta_fk foreign key (original_mta_id) references material_transfer_agreement;
create index signed_mta_sending_institution_idx on signed_material_transfer_agreement (sending_institution_id);
create index signed_mta_receiving_institution_idx on signed_material_transfer_agreement (receiving_institution_id);
create index signed_mta_status_idx on signed_material_transfer_agreement (status);
create index signed_mta_loader_idx on signed_material_transfer_agreement (loader_id);
create index signed_mta_upload_time_idx on signed_material_transfer_agreement (upload_time);
create index signed_mta_original_mta_idx on signed_material_transfer_agreement (original_mta_id);


alter table institution add column mta_contact_email varchar(255);
alter table institution add column mta_contact_fax varchar(255);
alter table institution add column mta_contact_first_name varchar(255);
alter table institution add column mta_contact_last_name varchar(255);
alter table institution add column mta_contact_phone varchar(255);

alter table collection_protocol add column contact_fax varchar(255);
alter table institution add column contact_fax varchar(255);
