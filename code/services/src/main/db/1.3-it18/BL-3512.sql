create table support_letter_request (
    id int8 not null,
    requestor_id int8 not null,
    investigator_id int8 not null,
    resume_name varchar(255) not null,
    resume_content_type varchar(255) not null,
    resume_lob_id int8 not null,
    type varchar(255) not null,
    grant_type varchar(254),
    other_type varchar(254),
    draft_protocol_name varchar(255) not null,
    draft_protocol_content_type varchar(255) not null,
    draft_protocol_lob_id int8 not null,
    draft_letter_name varchar(255),
    draft_letter_content_type varchar(255),
    draft_letter_lob_id int8,
    comments varchar(3999),
    response varchar(3999),
    letter_name varchar(255),
    letter_content_type varchar(255),
    letter_lob_id int8,
    offline_response_notes varchar(3999),
    status varchar(255) not null,
    created_date timestamp not null,
    last_updated_date timestamp not null,
    primary key (id)
);
alter table support_letter_request add constraint FKC372ADE6B55088D0 foreign key (resume_lob_id) references lob_holder;
alter table support_letter_request add constraint support_letter_request_investigator_fk foreign key (investigator_id) references person;
alter table support_letter_request add constraint FKC372ADE6632572B7 foreign key (letter_lob_id) references lob_holder;
alter table support_letter_request add constraint FKC372ADE654813167 foreign key (draft_protocol_lob_id) references lob_holder;
alter table support_letter_request add constraint support_letter_request_requestor_fk foreign key (requestor_id) references tissue_locator_user;
alter table support_letter_request add constraint FKC372ADE63BDFD099 foreign key (draft_letter_lob_id) references lob_holder;
create index support_letter_request_requestor_idx on support_letter_request (requestor_id);
create index support_request_investigator_idx on support_letter_request (investigator_id);
create index support_letter_request_status_idx on support_letter_request (status);

create table support_letter_request_funding_source (
    request_id int8 not null,
    funding_source_id int8 not null,
    primary key (request_id, funding_source_id)
);
alter table support_letter_request_funding_source add constraint SUPPORT_LETTER_REQUEST_FUNDING_SOURCE_FK foreign key (request_id) references support_letter_request;
alter table support_letter_request_funding_source add constraint FUNDING_SOURCE_SUPPORT_LETTER_REQUEST_FK foreign key (funding_source_id) references funding_source;

create table support_letter_request_status_transition (
    id int8 not null,
    new_status varchar(255) not null,
    previous_status varchar(255) not null,
    system_transition_date timestamp not null,
    transition_date timestamp not null,
    primary key (id)
);

create table support_letter_request_status_transition_history (
    request_id int8 not null,
    status_transition_id int8 not null,
    primary key (request_id, status_transition_id)
);
alter table support_letter_request_status_transition_history add constraint SUPPORT_LETTER_REQUEST_TRANSITION_FK foreign key (request_id) references support_letter_request;
alter table support_letter_request_status_transition_history add constraint TRANSITION_SUPPORT_LETTER_REQUEST_FK foreign key (status_transition_id) references support_letter_request_status_transition;

insert into applicationrole (id, name) values (30, 'supportLetterRequestor');
insert into applicationrole (id, name) values (31, 'supportLetterRequestReviewer');
