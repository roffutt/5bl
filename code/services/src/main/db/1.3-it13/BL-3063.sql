alter table dynamic_field_definition_decimal add column monetary bool default false not null;
alter table dynamic_field_definition_integer add column monetary bool default false not null;
