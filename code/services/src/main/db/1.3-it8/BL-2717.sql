alter table dynamic_field_definition_boolean add column nullable_extension_decider_class varchar(254);
alter table dynamic_field_definition_controlled_vocab add column nullable_extension_decider_class varchar(254);
alter table dynamic_field_definition_decimal add column nullable_extension_decider_class varchar(254);
alter table dynamic_field_definition_entity add column nullable_extension_decider_class varchar(254);
alter table dynamic_field_definition_integer add column nullable_extension_decider_class varchar(254);
alter table dynamic_field_definition_string add column nullable_extension_decider_class varchar(254);

drop table disableable_boolean_field_definition;

UPDATE study SET funding_source = 'OTHER' where id in (select study_id from specimen_request sr where sr.status <> 'DRAFT') and funding_status = 'FUNDED' and funding_source is null;
