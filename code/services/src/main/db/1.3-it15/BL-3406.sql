alter table address add column phone_extension varchar(255);
alter table collection_protocol add column contact_phone_extension varchar(255);
alter table institution add column contact_phone_extension varchar(255);