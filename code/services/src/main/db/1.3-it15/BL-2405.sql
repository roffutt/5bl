create table account_status_transition (id int8 not null, previous_status varchar(255) not null, new_status varchar(255) not null, transition_date timestamp not null, system_transition_date timestamp not null, comment varchar(255), primary key (id));
create table account_status_transition_history (tissue_locator_user_id int8 not null, status_transition_id int8 not null, primary key (tissue_locator_user_id, status_transition_id));
alter table account_status_transition_history add constraint USER_TRANSITION_FK foreign key (tissue_locator_user_id) references tissue_locator_user;
alter table account_status_transition_history add constraint TRANSITION_USER_FK foreign key (status_transition_id) references account_status_transition;

create or replace function createDenialTransition() returns void as 'declare users record; begin for users in select tu.id as id, tu.denial_reason as denial_reason from tissue_locator_user tu, AbstractUser au where au.id = tu.id and au.status = ''INACTIVE'' and tu.denial_reason is not null loop insert into account_status_transition (id, previous_status, new_status, transition_date, system_transition_date, comment) values (nextval(''hibernate_sequence''), ''PENDING'', ''INACTIVE'', now(), now(), users.denial_reason ); insert into account_status_transition_history (tissue_locator_user_id, status_transition_id) values (users.id, (select max(id) from account_status_transition)); end loop; end; ' language plpgsql;
select createDenialTransition();

alter table tissue_locator_user drop column denial_reason;
drop function createDenialTransition();