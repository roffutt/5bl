alter table autocomplete_config add column css_classes varchar(255);
alter table checkbox_config add column css_classes varchar(255);
alter table range_config add column css_classes varchar(255);
alter table select_config add column css_classes varchar(255);
alter table select_dynamic_extension_config add column css_classes varchar(255);
alter table text_config add column css_classes varchar(255);