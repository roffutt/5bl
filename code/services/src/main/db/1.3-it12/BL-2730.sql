ALTER TABLE specimen DROP CONSTRAINT specimen_external_id_key;
CREATE UNIQUE INDEX specimen_external_id_key ON specimen(lower(external_id), external_id_assigner_id);

ALTER TABLE participant DROP CONSTRAINT participant_externalid_key;
CREATE UNIQUE INDEX participant_externalid_key ON participant(lower(externalid), external_id_assigner_id);

ALTER TABLE collection_protocol DROP CONSTRAINT collection_protocol_name_key;
CREATE UNIQUE INDEX collection_protocol_name_key ON collection_protocol(lower(name), institution_id);

ALTER TABLE institution DROP CONSTRAINT institution_name_key;
CREATE UNIQUE INDEX institution_name_key ON institution(lower(name));

ALTER TABLE institution_type DROP CONSTRAINT institution_type_name_key;
CREATE UNIQUE INDEX institution_type_name_key ON institution_type(lower(name));

ALTER TABLE material_transfer_agreement DROP CONSTRAINT material_transfer_agreement_version_key;
CREATE UNIQUE INDEX material_transfer_agreement_version_key ON material_transfer_agreement(lower(version));

DROP INDEX specimen_external_id_idx;
CREATE INDEX specimen_external_id_idx ON specimen(lower(external_id));

DROP INDEX participant_external_id_idx;
CREATE INDEX participant_external_id_idx ON participant(lower(externalid));

DROP INDEX protocol_name_index;
CREATE INDEX protocol_name_index ON collection_protocol(lower(name));

DROP INDEX institution_name_index;
CREATE INDEX institution_name_index ON institution(lower(name));

DROP INDEX institution_type_name_idx;
CREATE INDEX institution_type_name_idx ON institution_type(lower(name));

DROP INDEX additional_pathologic_finding_name_idx;
CREATE INDEX additional_pathologic_finding_name_idx ON additional_pathologic_finding(lower(name));

DROP INDEX specimen_type_name_idx;
CREATE INDEX specimen_type_name_idx ON specimen_type(lower(name));

DROP INDEX mta_version_idx;
CREATE INDEX mta_version_idx on material_transfer_agreement(lower(version));
