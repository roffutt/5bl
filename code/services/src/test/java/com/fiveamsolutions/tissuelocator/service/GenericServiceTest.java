/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.nci.commons.data.security.UserGroup;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.DummyDeletableEntity;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * Class to test the generic service.
 * @author smiller
 */
public class GenericServiceTest extends AbstractHibernateTestCase {

    /**
     * tests retrieving a persistent object.
     */
    @Test
    public void testObjectRetrieval() {
        GenericServiceLocal<InstitutionType> typeService = EjbTestHelper.getGenericServiceBean(InstitutionType.class);
        InstitutionType type = new InstitutionType();
        type.setName("test institute type");
        Long typeId = typeService.savePersistentObject(type);
        type = typeService.getPersistentObject(InstitutionType.class, typeId);

        GenericServiceLocal<Institution> service = EjbTestHelper.getGenericServiceBean(Institution.class);
        Institution institution = new Institution();
        institution.setName("test institute");
        institution.setHomepage("http://www.google.com");
        institution.setType(type);

        Long id = service.savePersistentObject(institution);
        Institution retrievedInstitution = service.getPersistentObject(Institution.class, id);
        assertEquals(institution.getName(), retrievedInstitution.getName());
        assertEquals(institution.getHomepage(), retrievedInstitution.getHomepage());
        assertEquals(institution.getInstitutionProfileUrl(), retrievedInstitution.getInstitutionProfileUrl());
        assertEquals(id, retrievedInstitution.getId());

        try {
            service.deletePersistentObject(retrievedInstitution);
            fail();
        } catch (IllegalArgumentException e) {
            //expected - object not Deletable
        }
    }

    /**
     * Tests failing to retrieve an object.
     */
    @Test
    public void testNullObjectRetrieval() {
        assertNull(EjbTestHelper.getGenericServiceBean(Institution.class).getPersistentObject(Institution.class, -1L));

        // purely a code coverage line here
        new TissueLocatorHibernateUtil();
    }

    /**
     * Test of get all.
     */
    @Test
    public void testGetAll() {
        testObjectRetrieval();
        List<Institution> institutions =
            EjbTestHelper.getGenericServiceBean(Institution.class).getAll(Institution.class);
        assertNotNull(institutions);
        assertEquals(2, institutions.size());  // add one because of abstract test creating an institution
        List<InstitutionType> types =
            EjbTestHelper.getGenericServiceBean(Institution.class).getAll(InstitutionType.class);
        assertNotNull(types);
        assertEquals(2, types.size());


    }

    /**
     * tests institution restriction.
     */
    @Test
    public void testInstituteRestriction() {
        GenericServiceLocal<InstitutionType> typeService = EjbTestHelper.getGenericServiceBean(InstitutionType.class);
        InstitutionType type = new InstitutionType();
        type.setName("test institute type");
        Long typeId = typeService.savePersistentObject(type);
        type = typeService.getPersistentObject(InstitutionType.class, typeId);

        TissueLocatorUser loggedInUser = getCurrentUser();
        Institution altInst = new Institution();
        altInst.setName("test institute");
        altInst.setType(type);
        TissueLocatorHibernateUtil.getCurrentSession().save(altInst);

        GenericServiceLocal<DummyDeletableEntity> service =
            EjbTestHelper.getGenericServiceBean(DummyDeletableEntity.class);
        DummyDeletableEntity dde = new DummyDeletableEntity();
        dde.setDeletable(false);

        UsernameHolder.setUser("doesnotexist");
        try {
            service.savePersistentObject(dde);
            fail();
        } catch (IllegalArgumentException e) {
            //expected - invalid user
        }

        loggedInUser = createUserSkipRole();
        UsernameHolder.setUser(loggedInUser.getUsername());
        dde.setInstitution(altInst);
        try {
            service.savePersistentObject(dde);
            fail();
        } catch (IllegalArgumentException e) {
            //expected - invalid institution
        }

        dde.setInstitution(loggedInUser.getInstitution());
        Long ddeId = service.savePersistentObject(dde);
        assertNotNull(ddeId);
        DummyDeletableEntity retrieved = service.getPersistentObject(DummyDeletableEntity.class, ddeId);
        assertNotNull(retrieved);
        assertEquals(ddeId, retrieved.getId());

        //save an object not from the users's institution.  will work because he now has the
        //cross institution role
        setupRoles(loggedInUser);
        dde = new DummyDeletableEntity();
        dde.setDeletable(false);
        dde.setInstitution(altInst);
        ddeId = service.savePersistentObject(dde);
        assertNotNull(ddeId);
        retrieved = service.getPersistentObject(DummyDeletableEntity.class, ddeId);
        assertNotNull(retrieved);
        assertEquals(ddeId, retrieved.getId());
    }

    /**
     * test deletion security.
     */
    @Test
    public void testDeletion() {
        GenericServiceLocal<InstitutionType> typeService = EjbTestHelper.getGenericServiceBean(InstitutionType.class);
        InstitutionType type = new InstitutionType();
        type.setName("test institute type");
        Long typeId = typeService.savePersistentObject(type);
        type = typeService.getPersistentObject(InstitutionType.class, typeId);

        TissueLocatorUser loggedInUser = getCurrentUser();
        Institution userInst = loggedInUser.getInstitution();
        Institution altInst = new Institution();
        altInst.setName("test institute");
        altInst.setType(type);
        TissueLocatorHibernateUtil.getCurrentSession().save(altInst);

        GenericServiceLocal<DummyDeletableEntity> service =
            EjbTestHelper.getGenericServiceBean(DummyDeletableEntity.class);
        DummyDeletableEntity dde = new DummyDeletableEntity();
        dde.setDeletable(false);
        dde.setInstitution(altInst);
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(dde);
        DummyDeletableEntity savedDde = service.getPersistentObject(DummyDeletableEntity.class, id);

        UsernameHolder.setUser("doesnotexist");
        try {
            service.deletePersistentObject(savedDde);
            fail();
        } catch (IllegalArgumentException e) {
            //expected - invalid user
        }

        UsernameHolder.setUser(loggedInUser.getUsername());
        try {
            service.deletePersistentObject(savedDde);
            fail();
        } catch (IllegalArgumentException e) {
            //expected - invalid institution
        }

        savedDde.setInstitution(userInst);
        TissueLocatorHibernateUtil.getCurrentSession().update(savedDde);
        savedDde = service.getPersistentObject(DummyDeletableEntity.class, id);
        try {
            service.deletePersistentObject(savedDde);
            fail();
        } catch (IllegalArgumentException e) {
            //expected - isDeletable is false
        }

        savedDde.setDeletable(true);
        TissueLocatorHibernateUtil.getCurrentSession().update(savedDde);
        savedDde = service.getPersistentObject(DummyDeletableEntity.class, id);
        service.deletePersistentObject(savedDde);
        assertNull(service.getPersistentObject(DummyDeletableEntity.class, id));

        setupRoles(loggedInUser);
        dde = new DummyDeletableEntity();
        dde.setDeletable(true);
        dde.setInstitution(altInst);
        id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(dde);
        savedDde = service.getPersistentObject(DummyDeletableEntity.class, id);
        service.deletePersistentObject(savedDde);
        assertNull(service.getPersistentObject(DummyDeletableEntity.class, id));
    }

    private void setupRoles(TissueLocatorUser user) {
        //give user some roles, including cross institution role
        ApplicationRole role = new ApplicationRole();
        role.setName("arole");
        TissueLocatorHibernateUtil.getCurrentSession().save(role);
        ApplicationRole crossRole = getCrossInstitutionRole();
        UserGroup ug = new UserGroup();
        ug.setName("administrators");
        ug.getRoles().add(crossRole);
        TissueLocatorHibernateUtil.getCurrentSession().save(ug);
        user.getGroups().add(ug);
        user.getRoles().add(role);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(user);
    }
}
