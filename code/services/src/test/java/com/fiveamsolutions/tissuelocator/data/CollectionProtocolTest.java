/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.hibernate.PropertyValueException;
import org.hibernate.validator.InvalidStateException;
import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.CollectionProtocolSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.test.InstitutionPersistenceHelper;
import com.fiveamsolutions.tissuelocator.test.SpecimenPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author smiller
 *
 */
public class CollectionProtocolTest extends AbstractPersistenceTest<CollectionProtocol> {

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectionProtocolServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectionProtocol[] getValidObjects() {
        Institution institution = getCurrentUser().getInstitution();
        CollectionProtocol[] protocols = new CollectionProtocol[2];
        protocols[0] = getDefaultValidObject(institution);
        protocols[1] = getDefaultValidObject(institution);
        protocols[1].setName("test name 2");
        return protocols;
    }

    private CollectionProtocol getDefaultValidObject(Institution institution) {
        CollectionProtocol p = new CollectionProtocol();
        p.setEndDate(new Date());
        p.setName("test name");
        p.setStartDate(new Date());
        p.setInstitution(institution);
        p.setContact(new Contact());
        p.getContact().setEmail("test@test.com");
        return p;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(CollectionProtocol expected, CollectionProtocol actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual, new String[] {"institution"}));
        if (expected.getInstitution() == null) {
            assertNull(actual.getInstitution());
        } else {
            assertEquals(expected.getInstitution().getId(), actual.getInstitution().getId());
        }
    }

    /**
     * Verify that name is required.
     */
    @Test(expected = InvalidStateException.class)
    public void testNameIsRequired() {
        CollectionProtocol o = getValidObjects()[0];
        o.setName("");
        getService().savePersistentObject(o);
    }

    /**
     * Verify that institution is required.
     */
    @Test(expected = PropertyValueException.class)
    public void testInstitutionIsRequired() {
        CollectionProtocol o = getValidObjects()[0];
        o.setInstitution(null);
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(o);
    }

    /**
     * Verify that institution is required.
     */
    @Test(expected = InvalidStateException.class)
    public void testUniqeConstraint() {
        CollectionProtocol o1 = getValidObjects()[0];
        CollectionProtocol o2 = getDefaultValidObject(o1.getInstitution());
        CollectionProtocol o3 = getDefaultValidObject(o1.getInstitution());

        o2.setName("new name");
        getService().savePersistentObject(o1);
        try {
            getService().savePersistentObject(o2);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        getService().savePersistentObject(o3);
    }

    /**
     * test search.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        CollectionProtocol example = new CollectionProtocol();
        example.setName("test");
        CollectionProtocolServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();
        SearchCriteria<CollectionProtocol> sc =
            new TissueLocatorAnnotatedBeanSearchCriteria<CollectionProtocol>(example);
        PageSortParams<CollectionProtocol> psp =
              new PageSortParams<CollectionProtocol>(PAGE_SIZE, 0, CollectionProtocolSortCriterion.NAME, true);
        List<CollectionProtocol> results = service.search(sc, psp);
        assertEquals(2, results.size());
        assertTrue(results.get(0).getName().compareTo(results.get(1).getName()) > 0);
    }

    /**
     * Test default protocol retrieval.
     */
    @Test
    public void testGetDefaultProtocol() {
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution inst1 = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test 1", true);
        Institution inst2 = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test 2", true);
        InstitutionServiceLocal instService = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        Long id = instService.savePersistentObject(inst1);
        inst1 = instService.getPersistentObject(Institution.class, id);
        id = instService.savePersistentObject(inst2);
        inst2 = instService.getPersistentObject(Institution.class, id);
        CollectionProtocolServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();

        assertNull(service.getDefaultProtocol(inst1.getId()));
        assertNull(service.getDefaultProtocol(inst2.getId()));

        int index = 0;
        CollectionProtocol protocol = SpecimenPersistenceHelper.createProtocol(inst1, index++);
        id = service.savePersistentObject(protocol);
        assertEquals(id, service.getDefaultProtocol(inst1.getId()).getId());
        assertNull(service.getDefaultProtocol(inst2.getId()));

        protocol = SpecimenPersistenceHelper.createProtocol(inst1, index++);
        service.savePersistentObject(protocol);
        assertNull(service.getDefaultProtocol(inst1.getId()));
        assertNull(service.getDefaultProtocol(inst2.getId()));

        protocol = SpecimenPersistenceHelper.createProtocol(inst2, index++);
        id = service.savePersistentObject(protocol);
        assertEquals(id, service.getDefaultProtocol(inst2.getId()).getId());
        assertNull(service.getDefaultProtocol(inst1.getId()));

        protocol = SpecimenPersistenceHelper.createProtocol(inst2, index++);
        id = service.savePersistentObject(protocol);
        assertNull(service.getDefaultProtocol(inst1.getId()));
        assertNull(service.getDefaultProtocol(inst2.getId()));
    }

    /**
     * Test retrieval of default protocol map.
     */
    @Test
    public void testGetDefaultProtocolMap() {
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution inst1 = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test 1", true);
        Institution inst2 = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test 2", true);
        InstitutionServiceLocal instService = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        Long id = instService.savePersistentObject(inst1);
        inst1 = instService.getPersistentObject(Institution.class, id);
        id = instService.savePersistentObject(inst2);
        inst2 = instService.getPersistentObject(Institution.class, id);
        CollectionProtocolServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();

        assertNull(service.getDefaultProtocols().get(inst1.getId()));
        assertNull(service.getDefaultProtocols().get(inst2.getId()));

        int index = 0;
        CollectionProtocol protocol = SpecimenPersistenceHelper.createProtocol(inst1, index++);
        id = service.savePersistentObject(protocol);
        assertEquals(id, service.getDefaultProtocols().get(inst1.getId()).getId());
        assertNull(service.getDefaultProtocols().get(inst2.getId()));

        protocol = SpecimenPersistenceHelper.createProtocol(inst1, index++);
        service.savePersistentObject(protocol);
        assertNull(service.getDefaultProtocols().get(inst1.getId()));
        assertNull(service.getDefaultProtocols().get(inst2.getId()));

        protocol = SpecimenPersistenceHelper.createProtocol(inst2, index++);
        id = service.savePersistentObject(protocol);
        assertEquals(id, service.getDefaultProtocols().get(inst2.getId()).getId());
        assertNull(service.getDefaultProtocols().get(inst1.getId()));

        protocol = SpecimenPersistenceHelper.createProtocol(inst2, index++);
        id = service.savePersistentObject(protocol);
        assertNull(service.getDefaultProtocols().get(inst1.getId()));
        assertNull(service.getDefaultProtocols().get(inst2.getId()));
    }

    /**
     * Test creation of a default protocol for an institution.
     */
    @Test
    public void testCreateDefaultProtocol() {
        InstitutionType type = InstitutionPersistenceHelper.getInstance().createAndSaveDefaultType();
        Institution inst = InstitutionPersistenceHelper.getInstance().createInstitution(type, "test 1", true);
        InstitutionServiceLocal instService = TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        Long instId = instService.savePersistentObject(inst);

        CollectionProtocolServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();
        CollectionProtocol cp = service.createDefaultProtocol(inst);
        assertNotNull(cp.getId());
        assertNotNull(cp.getName());
        assertTrue(cp.getName().contains("Collection Protocol"));
        assertNotNull(cp.getInstitution());
        assertEquals(instId, cp.getInstitution().getId());
    }
}
