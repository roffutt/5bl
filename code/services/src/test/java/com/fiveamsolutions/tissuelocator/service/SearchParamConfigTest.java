/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.config.search.MultiSelectValueType;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;

/**
 * @author gvaughn
 *
 */
public class SearchParamConfigTest extends AbstractHibernateTestCase {

    /**
     * Test value conversion of Longs.
     */
    @Test
    public void testGetLongValues() {
        List<Object> values = new ArrayList<Object>();
        List<Object> convertedValues = MultiSelectValueType.LONG.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add("0");
        values.add("1");
        convertedValues = MultiSelectValueType.LONG.getValues(values);
        assertEquals(0L, convertedValues.get(0));
        assertEquals(1L, convertedValues.get(1));
    }

    /**
     * Test value conversion of Genders.
     */
    @Test
    public void testGetGenderValues() {
        List<Object> values = new ArrayList<Object>();
        List<Object> convertedValues = MultiSelectValueType.GENDER.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add("  ");
        convertedValues = MultiSelectValueType.GENDER.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add(Gender.MALE.name());
        values.add(Gender.FEMALE.name());
        convertedValues = MultiSelectValueType.GENDER.getValues(values);
        assertEquals(Gender.MALE, convertedValues.get(0));
        assertEquals(Gender.FEMALE, convertedValues.get(1));
    }

    /**
     * Test value conversion of Genders.
     */
    @Test
    public void testGetEthinicityValues() {
        List<Object> values = new ArrayList<Object>();
        List<Object> convertedValues = MultiSelectValueType.ETHNICITY.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add("  ");
        convertedValues = MultiSelectValueType.ETHNICITY.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add(Ethnicity.HISPANIC_OR_LATINO.name());
        values.add(Ethnicity.UNKNOWN.name());
        convertedValues = MultiSelectValueType.ETHNICITY.getValues(values);
        assertEquals(Ethnicity.HISPANIC_OR_LATINO, convertedValues.get(0));
        assertEquals(Ethnicity.UNKNOWN, convertedValues.get(1));
    }
    
    /**
     * Test value conversion of Race.
     */
    @Test
    public void testGetRaceValues() {
        List<Object> values = new ArrayList<Object>();
        List<Object> convertedValues = MultiSelectValueType.RACE.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add("  ");
        convertedValues = MultiSelectValueType.RACE.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add(Race.ASIAN.name());
        values.add(Race.BLACK_OR_AFRICAN_AMERICAN.name());
        convertedValues = MultiSelectValueType.RACE.getValues(values);
        assertEquals(Race.ASIAN, convertedValues.get(0));
        assertEquals(Race.BLACK_OR_AFRICAN_AMERICAN, convertedValues.get(1));
    }

    /**
     * Test value conversion of Genders.
     */
    @Test
    public void testGetStringValues() {
        List<Object> values = new ArrayList<Object>();
        List<Object> convertedValues = MultiSelectValueType.STRING.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add("  ");
        convertedValues = MultiSelectValueType.STRING.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add("test1");
        values.add("test2");
        convertedValues = MultiSelectValueType.STRING.getValues(values);
        assertEquals("test1", convertedValues.get(0));
        assertEquals("test2", convertedValues.get(1));
    }

    /**
     * Test value conversion of SpecimenClass.
     */
    @Test
    public void testGetSpecimenClassValues() {
        List<Object> values = new ArrayList<Object>();
        List<Object> convertedValues = MultiSelectValueType.SPECIMENCLASS.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add("  ");
        convertedValues = MultiSelectValueType.SPECIMENCLASS.getValues(values);
        assertEquals(0, convertedValues.size());
        values.add(SpecimenClass.CELLS);
        values.add(SpecimenClass.FLUID);
        convertedValues = MultiSelectValueType.SPECIMENCLASS.getValues(values);
        assertEquals(SpecimenClass.CELLS, convertedValues.get(0));
        assertEquals(SpecimenClass.FLUID, convertedValues.get(1));
    }

}
