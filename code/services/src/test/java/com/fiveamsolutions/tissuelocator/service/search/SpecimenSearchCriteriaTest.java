/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.RangeConfig;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.test.SpecimenPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * Specimen search criteria integration tests.
 *
 * @author jstephens
 */
public class SpecimenSearchCriteriaTest extends AbstractHibernateTestCase {

    private static final int MAX_AGE_YEARS  = 90;
    private static final int MONTHS_IN_YEAR = 12;
    private static final int DAYS_IN_MONTH  = 30;
    private static final int DAYS_IN_YEAR   = 365;
    private static final int HOURS_IN_DAY   = 24;
    private static int codeSuffix = 0;
    private SpecimenServiceLocal specimenService;

    /**
     * Set up test.
     */
    @Before
    public void setUpTest() {
        initializeSpecimenService();
        saveSpecimens();
    }

    /**
     * Test available quantity search.
     */
    @Test
    public void testAvailableQuantitySearch() {
        Specimen example = new Specimen();
        example.setAvailableQuantityUnits(QuantityUnits.MG);

        SpecimenSearchCriteriaBuilder builder = new SpecimenSearchCriteriaBuilder(example);
        builder.minAvailableQuantity(new BigDecimal("0.5"));
        SearchCriteria<Specimen> sc = builder.build();

        List<Specimen> results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        builder.minAvailableQuantity(new BigDecimal("1.5"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        example.setAvailableQuantityUnits(QuantityUnits.UG);
        builder.minAvailableQuantity(new BigDecimal("500"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        builder.minAvailableQuantity(new BigDecimal("1500"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));
        
        example.setAvailableQuantityUnits(QuantityUnits.G);
        builder.minAvailableQuantity(new BigDecimal("0.0005"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));
        
        builder.minAvailableQuantity(new BigDecimal("1.0"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        example.setAvailableQuantityUnits(QuantityUnits.OZ);
        builder.minAvailableQuantity(new BigDecimal("0.5"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));
        
        builder.minAvailableQuantity(new BigDecimal("1.5"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));
        
        example.setAvailableQuantityUnits(QuantityUnits.ML);
        builder.minAvailableQuantity(new BigDecimal("29"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));
        
        builder.minAvailableQuantity(new BigDecimal("30"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));
        
        example.setAvailableQuantityUnits(QuantityUnits.COUNT);
        builder.minAvailableQuantity(new BigDecimal("1.0"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        builder.minAvailableQuantity(new BigDecimal("0.5"));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));
    }

    /**
     * Test race crucial.
     */
    @Test
    public void testRaceSearch() {
        SpecimenSearchCriteriaBuilder builder = new SpecimenSearchCriteriaBuilder();
        builder.races(new HashSet<Race>(Collections.singleton(Race.WHITE)));
        SearchCriteria<Specimen> sc = builder.build();

        List<Specimen> results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        builder.races(new HashSet<Race>(Collections.singleton(Race.BLACK_OR_AFRICAN_AMERICAN)));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        builder.races(new HashSet<Race>(Collections.singleton(Race.ASIAN)));
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        Set<Race> races = new HashSet<Race>();
        races.add(Race.WHITE);
        races.add(Race.BLACK_OR_AFRICAN_AMERICAN);
        builder.races(races);
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        races.clear();
        races.add(Race.WHITE);
        races.add(Race.ASIAN);
        builder.races(races);
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));
    }

    /**
     * Test minimum patient age at collection in years search.
     */
    @Test
    public void testMinimumPatientAgeAtCollectionInYearsSearch() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.minRangeValue(0);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.minRangeValue(1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));
    }

    /**
     * Test minimum patient age at collection in months search.
     */
    @Test
    public void testMinimumPatientAgeAtCollectionInMonthsSearch() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.MONTHS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.minRangeValue(MONTHS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(MONTHS_IN_YEAR + 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue(MONTHS_IN_YEAR * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue(2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.minRangeValue((2 + 1));
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));
    }

    /**
     * Test minimum patient age at collection in days search.
     */
    @Test
    public void testMinimumPatientAgeAtCollectionInDaysSearch() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.DAYS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.minRangeValue(DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_YEAR * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_MONTH * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_MONTH * 2 + 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_MONTH * (2 + 1));
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));
    }

    /**
     * Test minimum patient age at collection in hours search.
     */
    @Test
    public void testMinimumPatientAgeAtCollectionInHoursSearch() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.HOURS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.minRangeValue(DAYS_IN_YEAR * HOURS_IN_DAY);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));


        rangeBuilder.minRangeValue(DAYS_IN_YEAR * 2 * HOURS_IN_DAY);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_MONTH * 2 * HOURS_IN_DAY);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.minRangeValue((DAYS_IN_MONTH * 2 + 2) * HOURS_IN_DAY);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_MONTH * (2 + 1) * HOURS_IN_DAY);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));
    }

    /**
     * Test maximum patient age at collection in years search.
     */
    @Test
    public void testMaximumPatientAgeAtCollectionInYearsSearch() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.maxRangeValue(0);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.maxRangeValue(1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.maxRangeValue(2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));
    }

    /**
     * Test maximum patient age at collection in months search.
     */
    @Test
    public void testMaximumPatientAgeAtCollectionInMonths() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.MONTHS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.maxRangeValue(1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.maxRangeValue(2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.maxRangeValue(MONTHS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.maxRangeValue(MONTHS_IN_YEAR * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));
    }

    /**
     * Test maximum patient age at collection in days search.
     */
    @Test
    public void testMaximumPatientAgeAtCollectionInDays() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.DAYS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.maxRangeValue(DAYS_IN_MONTH);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.maxRangeValue(DAYS_IN_MONTH * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.maxRangeValue(DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.maxRangeValue(DAYS_IN_YEAR * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));
    }

    /**
     * Test maximum patient age at collection in hours search.
     */
    @Test
    public void testMaximumPatientAgeAtCollectionInHours() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.HOURS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.maxRangeValue(HOURS_IN_DAY * DAYS_IN_MONTH);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.maxRangeValue(HOURS_IN_DAY * DAYS_IN_MONTH * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.maxRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.maxRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));
    }

    /**
     * Test patient age at collection in years range search.
     */
    @Test
    public void testPatientAgeAtCollectionInYearsRangeSearch() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.minRangeValue(0);
        rangeBuilder.maxRangeValue(0);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue(0);
        rangeBuilder.maxRangeValue(1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.minRangeValue(1);
        rangeBuilder.maxRangeValue(1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(1);
        rangeBuilder.maxRangeValue(2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(2);
        rangeBuilder.maxRangeValue(1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));
    }

    /**
     * Test patient age at collection in months range search.
     */
    @Test
    public void testPatientAgeAtCollectionInMonthsRangeSearch() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.MONTHS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.minRangeValue(0);
        rangeBuilder.maxRangeValue(0);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue(0);
        rangeBuilder.maxRangeValue(MONTHS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.minRangeValue(MONTHS_IN_YEAR);
        rangeBuilder.maxRangeValue(MONTHS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(MONTHS_IN_YEAR);
        rangeBuilder.maxRangeValue(MONTHS_IN_YEAR * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(MONTHS_IN_YEAR * 2);
        rangeBuilder.maxRangeValue(MONTHS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue((2 + 1));
        rangeBuilder.maxRangeValue(MONTHS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));
    }

    /**
     * Test patient age at collection in days range search.
     */
    @Test
    public void testPatientAgeAtCollectionInDaysRangeSearch() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.DAYS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.minRangeValue(0);
        rangeBuilder.maxRangeValue(0);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue(0);
        rangeBuilder.maxRangeValue(DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_YEAR);
        rangeBuilder.maxRangeValue(DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_YEAR);
        rangeBuilder.maxRangeValue(DAYS_IN_YEAR * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_YEAR * 2);
        rangeBuilder.maxRangeValue(DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_MONTH * 2);
        rangeBuilder.maxRangeValue(DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.minRangeValue(DAYS_IN_MONTH * (2 + 1));
        rangeBuilder.maxRangeValue(DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));
    }

    /**
     * Test patient age at collection in hours range search.
     */
    @Test
    public void testPatientAgeAtCollectionInHoursRangeSearch() {
        Specimen example = new Specimen();
        example.setPatientAgeAtCollectionUnits(TimeUnits.HOURS);
        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder(example);

        RangeSearchConditionBuilder rangeBuilder =
            new RangeSearchConditionBuilder("patientAgeAtCollection", new TimeUnitRangeSearchCondition());

        rangeBuilder.minRangeValue(0);
        rangeBuilder.maxRangeValue(0);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue(0);
        rangeBuilder.maxRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.minRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR);
        rangeBuilder.maxRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR);
        rangeBuilder.maxRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR * 2);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR * 2);
        rangeBuilder.maxRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));

        rangeBuilder.minRangeValue(HOURS_IN_DAY * DAYS_IN_MONTH * 2);
        rangeBuilder.maxRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        rangeBuilder.minRangeValue(HOURS_IN_DAY * DAYS_IN_MONTH * (2 + 1));
        rangeBuilder.maxRangeValue(HOURS_IN_DAY * DAYS_IN_YEAR);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));
    }

    /**
     * Test minimum collection year search.
     */
    @Test
    public void testMinimumCollectionYearSearch() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int currentYear = cal.get(Calendar.YEAR);

        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder();
        RangeSearchConditionBuilder rangeBuilder = new RangeSearchConditionBuilder("collectionYear");

        rangeBuilder.minRangeValue(currentYear - 1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(currentYear + 1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));
    }

    /**
     * Test maximum collection year search.
     */
    @Test
    public void testMaximumCollectionYearSearch() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int currentYear = cal.get(Calendar.YEAR);

        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder();
        RangeSearchConditionBuilder rangeBuilder = new RangeSearchConditionBuilder("collectionYear");

        rangeBuilder.maxRangeValue(currentYear + 1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.maxRangeValue(currentYear - 1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));
    }

    /**
     * Test collection year range search.
     */
    @Test
    public void testCollectionYearRangeSearch() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int currentYear = cal.get(Calendar.YEAR);

        SpecimenSearchCriteriaBuilder criteriaBuilder = new SpecimenSearchCriteriaBuilder();
        RangeSearchConditionBuilder rangeBuilder = new RangeSearchConditionBuilder("collectionYear");

        rangeBuilder.minRangeValue(currentYear - 1);
        rangeBuilder.maxRangeValue(currentYear + 1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        SearchCriteria<Specimen> sc = criteriaBuilder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(1, results.size());
        assertEquals(1, specimenService.count(sc));

        rangeBuilder.minRangeValue(currentYear + 1);
        rangeBuilder.maxRangeValue(currentYear - 1);
        criteriaBuilder.searchConditions(rangeBuilder.buildConditions());
        sc = criteriaBuilder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));
    }

    /**
     * Test multi-select search parameters.
     */
    @Test
    public void testMultiSelectSearchParameters() {
        Map<String, Collection<Object>> paramValueMap = new HashMap<String, Collection<Object>>();
        String propertyName = "externalIdAssigner.id";
        List<Object> paramValueList = new ArrayList<Object>();
        paramValueMap.put(propertyName, paramValueList);
        SpecimenSearchCriteriaBuilder builder = new SpecimenSearchCriteriaBuilder();
        builder.multiSelectSearchParameters(paramValueMap);
        SearchCriteria<Specimen> sc = builder.build();
        List<Specimen> results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        Long institutionId = specimenService.getAll(Specimen.class).get(0).getExternalIdAssigner().getId();
        paramValueList.add(institutionId);
        paramValueMap.put(propertyName, paramValueList);
        builder.multiSelectSearchParameters(paramValueMap);
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        paramValueList.add(institutionId + 1);
        builder.multiSelectSearchParameters(paramValueMap);
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(2, results.size());
        assertEquals(2, specimenService.count(sc));

        paramValueMap.get(propertyName).clear();
        paramValueList.add(institutionId + 1);
        builder.multiSelectSearchParameters(paramValueMap);
        sc = builder.build();
        results = specimenService.search(sc);
        assertEquals(0, results.size());
        assertEquals(0, specimenService.count(sc));
    }

    /**
     * Test aggregate search criteria.
     */
    @Test
    public void testAggregateSearchCriteria() {
        SpecimenSearchServiceLocal searchService = new SpecimenSearchServiceBean();
        Long institutionId = specimenService.getAll(Specimen.class).get(0).getExternalIdAssigner().getId();
        AggregateSpecimenSearchCriteria sc = new AggregateSpecimenSearchCriteria(new Specimen(), null, null,
                new HashMap<String, Collection<Object>>(), new ArrayList<SearchCondition>());
        List<SpecimenGroupCriterion> groupByCriteria = new ArrayList<SpecimenGroupCriterion>();
        groupByCriteria.add(SpecimenGroupCriterion.EXTERNAL_ID_ASSIGNER);
        Map<Institution, Long> results = searchService.aggregate(sc, null, groupByCriteria);
        assertEquals(1, results.size());
        assertNotNull(results.keySet().iterator().next());
        assertEquals(institutionId, results.keySet().iterator().next().getId());
        assertEquals(2, results.values().iterator().next().intValue());
        
        sc.setMinimumCountThreshold(2);
        results = searchService.aggregate(sc, null, groupByCriteria);
        assertEquals(1, results.size());
        assertNotNull(results.keySet().iterator().next());
        assertEquals(institutionId, results.keySet().iterator().next().getId());
        assertEquals(2, results.values().iterator().next().intValue());
        
        //CHECKSTYLE:OFF - magic number
        sc.setMinimumCountThreshold(3);
        results = searchService.aggregate(sc, null, groupByCriteria);
        assertNull(results);
        //CHECKSTYLE:ON
        
        sc.setMaximumCountThreshold(2);
        results = searchService.aggregate(sc, null, groupByCriteria);
        assertEquals(1, results.size());
        assertNotNull(results.keySet().iterator().next());
        assertEquals(institutionId, results.keySet().iterator().next().getId());
        assertEquals(2, results.values().iterator().next().intValue());
        
        sc.setMaximumCountThreshold(1);
        results = searchService.aggregate(sc, null, groupByCriteria);
        assertNull(results);
    }

    /**
     * Specimen search criteria builder.
     */
    public class SpecimenSearchCriteriaBuilder {

        private final Specimen specimen;
        private BigDecimal quantity = null;
        private Map<String, Collection<Object>> collectionParameters = null;
        private Map<String, Collection<Object>> parameters = null;
        private Collection<SearchCondition> conditions = null;

        /**
         * Creates an instance of the specimen search criteria builder.
         */
        public SpecimenSearchCriteriaBuilder() {
            this(new Specimen());
        }

        /**
         * Creates an instance of the specimen search criteria builder.
         *
         * @param specimen the query specimen
         */
        public SpecimenSearchCriteriaBuilder(Specimen specimen) {
            this.specimen = specimen;
        }

        /**
         * Sets the minimum available quantity.
         *
         * @param quantityArg the minimum quantity available
         * @return a reference to this criteria builder
         */
        public SpecimenSearchCriteriaBuilder minAvailableQuantity(BigDecimal quantityArg) {
            quantity = quantityArg;
            return this;
        }

        /**
         * Sets the race.
         *
         * @param racesArg collectionParameters of specimen participant
         * @return a reference to this criteria builder
         */
        public SpecimenSearchCriteriaBuilder races(Set<Race> racesArg) {
            collectionParameters = new HashMap<String, Collection<Object>>();
            collectionParameters.put("participant.races", new ArrayList<Object>(racesArg));
            return this;
        }

        /**
         * Sets the multi-select search parameters.
         *
         * @param parametersArg the parameters
         * @return a reference to this criteria builder
         */
        public SpecimenSearchCriteriaBuilder
                multiSelectSearchParameters(Map<String, Collection<Object>> parametersArg) {
            parameters = parametersArg;
            return this;
        }

        /**
         * Sets the search conditions.
         *
         * @param conditionsArg the search conditions
         * @return a reference to this criteria builder
         */
        public SpecimenSearchCriteriaBuilder
                searchConditions(Collection<SearchCondition> conditionsArg) {
            conditions = conditionsArg;
            return this;
        }

        /**
         * Builds the search criteria.
         *
         * @return the search criteria.
         */
        public SearchCriteria<Specimen> build() {
            return new SpecimenSearchCriteria(specimen, quantity, collectionParameters, parameters, conditions);
        }

    }

    /**
     * Range search condition builder.
     */
    public class RangeSearchConditionBuilder {

        private final RangeConfig config = new RangeConfig();
        private final RangeSearchCondition condition;
        private final Map<String, Object> parameters = new HashMap<String, Object>();

        /**
         * Creates an instance of the search condition builder.
         *
         * @param propertyName the property name
         */
        public RangeSearchConditionBuilder(String propertyName) {
            this(propertyName, new RangeSearchCondition());
        }

        /**
         * Creates an instance of the search condition builder.
         *
         * @param propertyName property name associated with the search
         * @param condition the range search condition
         */
        public RangeSearchConditionBuilder(String propertyName, RangeSearchCondition condition) {
            this.condition = condition;
            config.setFieldConfig(new GenericFieldConfig());
            config.getFieldConfig().setSearchFieldName(propertyName);
        }

        /**
         * Sets the minimum range value.
         *
         * @param minValue the minimum range value
         * @return a reference to this search condition builder
         */
        public RangeSearchConditionBuilder minRangeValue(Object minValue) {
           parameters.put(config.getMinSearchFieldName(), minValue);
           return this;
        }

        /**
         * Sets the maximum range value.
         *
         * @param maxValue the maximum range value
         * @return a reference to this search condition builder
         */
        public RangeSearchConditionBuilder maxRangeValue(Object maxValue) {
           parameters.put(config.getMaxSearchFieldName(), maxValue);
           return this;
        }

        /**
         * Builds the range search condition.
         *
         * @return the range search condition
         */
        public SearchCondition build() {
            condition.setPropertyName(config.getSearchFieldName());
            condition.setConditionParameters(parameters);
            return condition;
        }

        /**
         * Builds a collection of range search conditions.
         *
         * @return a collection of range search conditions
         */
        public Collection<SearchCondition> buildConditions() {
            Collection<SearchCondition> conditions = new ArrayList<SearchCondition>();
            conditions.add(build());
            return conditions;
        }

    }

    /**
     * Gets the specimen service.
     *
     * @return the specimen service.
     */
    private void initializeSpecimenService() {
        specimenService = TissueLocatorRegistry.getServiceLocator().getSpecimenService();
    }

    /**
     * Save specimens.
     */
    private void saveSpecimens() {
        Specimen[] specimens = createSpecimens();
        for (Specimen specimen : specimens) {
            specimenService.savePersistentObject(specimen);
        }
    }

    /**
     * Create specimens.
     */
    private Specimen[] createSpecimens() {
        codeSuffix++;

        AdditionalPathologicFinding apf = SpecimenPersistenceHelper
            .createCode(AdditionalPathologicFinding.class, codeSuffix);

        SpecimenType st = SpecimenPersistenceHelper.createCode(SpecimenType.class, codeSuffix);

        TissueLocatorUser currentUser =
            EjbTestHelper.getTissueLocatorUserServiceBean()
                .getByUsername(UsernameHolder.getUser());

        Institution i = EjbTestHelper.getGenericServiceBean(Institution.class)
            .getPersistentObject(Institution.class, currentUser.getInstitution().getId());

        Participant participant = SpecimenPersistenceHelper.createAndStoreParticipant(i, codeSuffix);
        CollectionProtocol protocol = SpecimenPersistenceHelper.createAndStoreProtocol(i, codeSuffix);

        Specimen s1 = new Specimen();
        s1.setExternalId("ABRC-1234" + codeSuffix);
        s1.setPatientAgeAtCollection(MAX_AGE_YEARS);
        s1.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
        assertTrue(s1.isPatientAgeAtCollectionAtHippaMax());
        s1.setPatientAgeAtCollection(1);
        assertFalse(s1.isPatientAgeAtCollectionAtHippaMax());
        assertNotNull(TimeUnits.YEARS.getResourceKey());
        s1.setAvailableQuantity(new BigDecimal("1.0"));
        s1.setAvailableQuantityUnits(QuantityUnits.MG);
        assertNotNull(QuantityUnits.MG.getResourceKey());
        s1.setPathologicalCharacteristic(apf);
        s1.setSpecimenType(st);
        s1.setExternalIdAssigner(i);
        s1.setParticipant(participant);
        s1.setProtocol(protocol);
        s1.setCollectionYear(Calendar.getInstance().get(Calendar.YEAR));
        s1.setMinimumPrice(new BigDecimal("12.34"));
        s1.setMaximumPrice(new BigDecimal("56.78"));
        s1.setPriceNegotiable(false);
        s1.setStatus(SpecimenStatus.UNAVAILABLE);

        Specimen s2 = new Specimen();
        s2.setExternalId("ABRC-3456" + codeSuffix);
        s2.setPatientAgeAtCollection(2);
        s2.setPatientAgeAtCollectionUnits(TimeUnits.MONTHS);
        s2.setAvailableQuantity(new BigDecimal("1.0"));
        s2.setAvailableQuantityUnits(QuantityUnits.OZ);
        s2.setPathologicalCharacteristic(apf);
        s2.setSpecimenType(st);
        s2.setExternalIdAssigner(i);
        s2.setParticipant(participant);
        s2.setProtocol(protocol);
        s2.setPriceNegotiable(true);
        s2.setStatus(SpecimenStatus.AVAILABLE);

        return new Specimen[] {s1, s2};
    }

}