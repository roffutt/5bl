/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.persister.collection.AbstractCollectionPersister;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.junit.After;
import org.junit.Before;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.nci.commons.util.SecurityUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Address;
import com.fiveamsolutions.tissuelocator.data.ApplicationSetting;
import com.fiveamsolutions.tissuelocator.data.Contact;
import com.fiveamsolutions.tissuelocator.data.Country;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.State;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.validation.FundingStatusCheckValidator;
import com.fiveamsolutions.tissuelocator.data.validation.RequiredUserResumeValidator;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenRequestMTACertificationValidator;
import com.fiveamsolutions.tissuelocator.data.validation.StudyIrbApprovalRequiredValidator;
import com.fiveamsolutions.tissuelocator.data.validation.study.RequiredProtocolDocumentValidator;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceBean;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceBean;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceBean;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * @author Scott Miller
 */
public class AbstractHibernateTestCase {

    private static final Logger LOG = Logger.getLogger(AbstractHibernateTestCase.class);
    private static SchemaExport schemaExporter;
    private static int nameSuffix = 1;

    /**
     * the Guice injector.
     */
    private final Injector guiceInjector;

    /**
     * the transaction.
     */
    private Transaction transaction;
    private TissueLocatorUser currentUser;
    private ApplicationRole crossInstitutionRole;

    /**
     * set up the test.
     */
    public AbstractHibernateTestCase() {
        guiceInjector = Guice.createInjector(new AbstractModule() {

            /**
             * {@inheritDoc}
             */
            @Override
            protected void configure() {
                bind(TissueLocatorUserServiceLocal.class).to(TissueLocatorUserServiceBean.class);
                bind(ApplicationSettingServiceLocal.class).to(ApplicationSettingServiceBean.class);
                bind(SearchFieldConfigServiceLocal.class).to(SearchFieldConfigServiceBean.class);
                requestStaticInjection(RequiredProtocolDocumentValidator.class);
                requestStaticInjection(FundingStatusCheckValidator.class);
                requestStaticInjection(StudyIrbApprovalRequiredValidator.class);
                requestStaticInjection(SpecimenRequestMTACertificationValidator.class);
                requestStaticInjection(RequiredUserResumeValidator.class);
            }

        });
        EjbTestHelper.setInjector(guiceInjector);
    }

    /**
     * Method that runs before the test to clean up the db and start the transaction.
     */
    @SuppressWarnings("unchecked")
    @Before
    public final void setUp() {
        // clean up the hibernate second level cache between runs
        SessionFactory sf = TissueLocatorHibernateUtil.getCurrentSession().getSessionFactory();
        Map<?, EntityPersister> classMetadata = sf.getAllClassMetadata();
        for (EntityPersister ep : classMetadata.values()) {
            if (ep.hasCache()) {
                sf.evictEntity(ep.getCacheAccessStrategy().getRegion().getName());
            }
        }

        Map<?, AbstractCollectionPersister> collMetadata = sf.getAllCollectionMetadata();
        for (AbstractCollectionPersister acp : collMetadata.values()) {
            if (acp.hasCache()) {
                sf.evictCollection(acp.getCacheAccessStrategy().getRegion().getName());
            }
        }
        transaction = TissueLocatorHibernateUtil.getHibernateHelper().beginTransaction();
        TissueLocatorRegistry.getInstance().setServiceLocator(new TestServiceLocator(guiceInjector));

    }

    /**
     * method to clean up after test is done.
     */
    @After
    public final void tearDown() {
        try {
            transaction.commit();
        } catch (Exception e) {
            TissueLocatorHibernateUtil.getHibernateHelper().rollbackTransaction(transaction);
        }
    }

    /**
     * method to init the db if needed.
     * @throws SQLException on error
     */
    @Before
    @SuppressWarnings({"unchecked", "deprecation" })
    public final void initDbIfNeeded() throws SQLException {
     // First drop the audit sequence (& associated table, see
        // http://opensource.atlassian.com/projects/hibernate/browse/HHH-2472)
        Transaction tx = TissueLocatorHibernateUtil.getHibernateHelper().beginTransaction();
        Statement s = TissueLocatorHibernateUtil.getCurrentSession().connection().createStatement();
        try {
            s.execute("drop sequence AUDIT_ID_SEQ");
            s.execute("drop table if exists dual_AUDIT_ID_SEQ");
        } catch (SQLException e) {
            // expected
        }
        tx.commit();

        tx = TissueLocatorHibernateUtil.getHibernateHelper().beginTransaction();
        List<Long> counts = TissueLocatorHibernateUtil.getCurrentSession().createQuery(
                "select count(*) from " + Object.class.getName()).list();
        s = TissueLocatorHibernateUtil.getCurrentSession().connection().createStatement();
        s.execute("create sequence AUDIT_ID_SEQ");
        s.execute("create table dual_AUDIT_ID_SEQ(test boolean)");
        for (Long count : counts) {
            if (count > 0) {
                LOG.debug("DB contains data, dropping and readding.");
                if (schemaExporter == null) {
                    schemaExporter = new
                        SchemaExport(TissueLocatorHibernateUtil.getHibernateHelper().getConfiguration());
                }
                schemaExporter.drop(false, true);
                schemaExporter.create(false, true);
                break;
            }
        }
        tx.commit();

        tx = TissueLocatorHibernateUtil.getHibernateHelper().beginTransaction();
        currentUser = createUser(false, null);
        tx.commit();
    }

    /**
     * Create and save a user in the database.
     * @return the created user
     */
    protected TissueLocatorUser createUserSkipRole() {
        return createUser(true, null);
    }

    /**
     * Create and save a user in the database.
     * @param skipRole skip the role creation or not
     * @param institution the institution
     * @return the user
     */
    protected TissueLocatorUser createUser(boolean skipRole, Institution institution) {
        ApplicationSettingServiceLocal service = new ApplicationSettingServiceBean();
        try {
            service.isUserResumeRequired();
        } catch (Exception e) {
            ApplicationSetting setting = new ApplicationSetting();
            setting.setName("user_resume_required");
            setting.setValue("false");
            service.savePersistentObject(setting);
        }

        TissueLocatorUser user = new TissueLocatorUser();
        if (!skipRole) {
            if (crossInstitutionRole == null) {
                crossInstitutionRole = new ApplicationRole();
                crossInstitutionRole.setName(Role.CROSS_INSTITUTION.getName());
                TissueLocatorHibernateUtil.getCurrentSession().save(crossInstitutionRole);
            }
            user.getRoles().add(crossInstitutionRole);
        }
        user.setFirstName("first");
        user.setLastName("last");
        user.setEmail("test" + nameSuffix + "@example.com");
        user.setUsername("test" + nameSuffix + "@example.com");
        user.setPassword(SecurityUtils.create("test"));
        user.setAddress(getAddress());
        user.setTitle("title");
        user.setDegree("degree");
        user.setDepartment("department");
        user.setResearchArea("research area");
        if (institution == null) {
            InstitutionType type = new InstitutionType();
            type.setName("test type" + nameSuffix);
            EjbTestHelper.getGenericServiceBean(InstitutionType.class).savePersistentObject(type);
            Institution i = new Institution();
            i.setName("institute" + nameSuffix);
            i.setType(type);
            i.setConsortiumMember(true);
            i.setBillingAddress(new Address());
            i.getBillingAddress().setLine1("123 Main Street");
            i.getBillingAddress().setLine2("apartment 3");
            i.getBillingAddress().setCity("City");
            i.getBillingAddress().setState(State.MARYLAND);
            i.getBillingAddress().setZip("12345");
            i.getBillingAddress().setCountry(Country.UNITED_STATES);
            i.getBillingAddress().setPhone("1234567890");
            i.getBillingAddress().setFax("0987654321");
            i.setContact(new Contact());
            i.getContact().setEmail("test@example.com");
            i.getContact().setFirstName("fname");
            i.getContact().setLastName("lname");
            i.getContact().setPhone("1234567890");
            i.getContact().setPhoneExtension("1234");
            user.setInstitution(i);
        } else {
            user.setInstitution(institution);
        }

        UsernameHolder.setUser(user.getUsername());
        TissueLocatorHibernateUtil.getCurrentSession().save(user);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorUser testtest = EjbTestHelper.getTissueLocatorUserServiceBean().getByUsername(user.getUsername());
        assertNotNull(testtest);
        assertNotNull(testtest.getInstitution());
        Institution retrievedInst = testtest.getInstitution();
        assertEquals((skipRole ? 0 : 1), testtest.getRoles().size());
        assertEquals(1, testtest.getRelatedInstitutions().size());
        assertEquals(retrievedInst, testtest.getRelatedInstitutions().iterator().next());
        nameSuffix++;
        return testtest;
    }

    /**
     * create a user in a role and institution.
     * @param role the role for the user
     * @param inst the institution for the user
     * @return a new user in the given role and institution
     */
    protected TissueLocatorUser createUserInRole(Role role, Institution inst) {
        return createUserInRoles(inst, role);
    }

    /**
     * create a user in multiple roles and an institution.
     * @param roles the roles for the user
     * @param inst the institution for the user
     * @return a new user in the given role and institution
     */
    protected TissueLocatorUser createUserInRoles(Institution inst, Role...roles) {
        TissueLocatorUser user = createUser(true, inst);
        for (Role role : roles) {
            user.getRoles().add(getRole(role));
        }
        TissueLocatorHibernateUtil.getCurrentSession().save(user);
        return user;
    }

    private ApplicationRole getRole(Role role) {
        String queryString = "from " + ApplicationRole.class.getName() + " where name = :name";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setString("name", role.getName());
        ApplicationRole ar = (ApplicationRole) q.uniqueResult();

        if (ar == null) {
            // create role
            ar = new ApplicationRole();
            ar.setName(role.getName());
            TissueLocatorHibernateUtil.getCurrentSession().save(ar);
        }
        return ar;
    }

    /**
     * create a role.
     * @param role the role to create
     * @return the created role
     */
    protected ApplicationRole createApplicationRole(Role role) {
        ApplicationRole ar = new ApplicationRole();
        ar.setName(role.getName());
        TissueLocatorHibernateUtil.getCurrentSession().save(ar);
        return ar;
    }

    /**
     * Retrieves an application role by role name. If the role does not exist and
     * createIfNotFound is set, the role will be created.
     * @param role name of the role to be retrieved
     * @param createIfNotFound if true and the role is not found a new role with the specified name will be created
     * @return an ApplicationRole with the specified name
     */
    protected ApplicationRole getApplicationRoleByName(Role role, boolean createIfNotFound) {
        String queryString = "from " + ApplicationRole.class.getName() + " where name = :name";
        Query q = TissueLocatorHibernateUtil.getCurrentSession().createQuery(queryString);
        q.setString("name", role.getName());
        ApplicationRole ar = (ApplicationRole) q.uniqueResult();

        if (ar == null && createIfNotFound) {
            // create role
            ar = createApplicationRole(role);
        }

        return ar;
    }

    /**
     * @return the currentUser
     */
    public TissueLocatorUser getCurrentUser() {
        if (currentUser != null) {
            return EjbTestHelper.getTissueLocatorUserServiceBean().getByUsername(currentUser.getUsername());
        } else {
            return EjbTestHelper.getTissueLocatorUserServiceBean().getByUsername(UsernameHolder.getUser());
        }
    }

    /**
     * @return the crossInstitutionRole
     */
    public ApplicationRole getCrossInstitutionRole() {
        return EjbTestHelper.getGenericServiceBean(ApplicationRole.class).getPersistentObject(
                ApplicationRole.class, crossInstitutionRole.getId());
    }

    /**
     * verify an email.
     * @param recipient the recipient of the email
     * @param subjectContent the content for the subject
     * @param content the expected content
     * @throws MessagingException on error
     * @throws IOException on error
     */
    protected void testEmail(String recipient, String subjectContent, String... content)
        throws MessagingException, IOException {
        Mailbox inbox = Mailbox.get(recipient);
        assertNotNull(inbox);
        MimeMessage message = (MimeMessage) inbox.get(0);
        assertNotNull(message);
        testEmailPart(message.getSubject(), subjectContent);
        MimeMultipart mmp = (MimeMultipart) message.getContent();
        testEmailPart(mmp.getBodyPart(0).getContent().toString(), content);
        testEmailPart(mmp.getBodyPart(1).getContent().toString(), content);
        inbox.remove(message);
    }

    private void testEmailPart(String emailPart, String... content) {
        assertTrue(StringUtils.isNotBlank(emailPart));
        if (!emailPart.contains("N/A")) {
            for (String contentPart : content) {
                assertTrue(emailPart.toLowerCase().contains(contentPart.toLowerCase()));
            }
        }
        assertFalse(emailPart.contains("{"));
        assertFalse(emailPart.contains("}"));
    }

    /**
     * @return the guiceInjector
     */
    protected Injector getGuiceInjector() {
        return guiceInjector;
    }

    /**
     * @return A populated address.
     */
    protected Address getAddress() {
        Address address = new Address();
        address.setLine1("123 Main Street");
        address.setLine2("Apt 4");
        address.setCity("City");
        address.setState(State.MARYLAND);
        address.setZip("12345");
        address.setCountry(Country.UNITED_STATES);
        address.setPhone("1234567890");
        address.setPhoneExtension("1234");
        address.setFax("0987654321");
        return address;
    }

    /**
     * Create and save a generic field config.
     * @param fieldName Config field name.
     * @return A generic field config.
     */
    protected GenericFieldConfig createAndPersistGenericFieldConfig(String fieldName) {
        GenericFieldConfig config = new GenericFieldConfig();
        config.setFieldName(fieldName);
        config.setSearchFieldName("search" + fieldName);
        config.setFieldDisplayName("display" + fieldName);
        config.setSearchFieldDisplayName("displaySearch" + fieldName);
        GenericServiceLocal<PersistentObject> service = TissueLocatorRegistry.getServiceLocator().getGenericService();
        Long id = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(config);
        return service.getPersistentObject(GenericFieldConfig.class, id);
    }
}
