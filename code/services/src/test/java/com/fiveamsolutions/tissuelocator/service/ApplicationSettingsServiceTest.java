/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.ApplicationSetting;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceBean;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author smiller
 *
 */
public class ApplicationSettingsServiceTest extends AbstractHibernateTestCase {

    private static final int MAX_MINUTES = 59;
    private static final int MAX_HOURS = 23;

    private ApplicationSettingServiceLocal serviceToTest;

    /**
     * Create the user service for test.
     */
    @Before
    public void createServiceForTest() {
        serviceToTest = new ApplicationSettingServiceBean();
    }

    /**
     * Test the num reviewers.
     */
    @Test
    public void testNumReviewers() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("number_reviewers");
        setting.setValue("1");

        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        int value = serviceToTest.getNumberOfReviewers();

        assertEquals(1, value);
    }

    /**
     * Test the grace period.
     */
    @Test
    public void testGracePeriod() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("shipment_receipt_grace_period");
        setting.setValue("1");

        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        int value = serviceToTest.getShipmentReceiptGracePeriod();

        assertEquals(1, value);
    }

    /**
     * Test the line item review grace period.
     */
    @Test
    public void testLineItemReviewGracePeriod() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("institutional_review_grace_period");
        setting.setValue("1");

        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        int value = serviceToTest.getLineItemReviewGracePeriod();

        assertEquals(1, value);
    }

    /**
     * Test the support letter request review grace period.
     */
    @Test
    public void testSupportLetterRequesteviewGracePeriod() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("support_letter_request_review_grace_period");
        setting.setValue("2");

        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        int value = serviceToTest.getSupportLetterRequestReviewGracePeriod();

        assertEquals(2, value);
    }

    /**
     * Test the support letter request review grace period.
     */
    @Test
    public void testQuestionResponseGracePeriod() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("question_response_grace_period");
        setting.setValue("2");

        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        int value = serviceToTest.getQuestionResponseGracePeriod();

        assertEquals(2, value);
    }

    /**
     * Test the start time.
     */
    @Test
    public void testStartTime1() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("scheduled_reminder_thread_start_time");
        setting.setValue("0001");

        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        Date startTime = serviceToTest.getScheduledReminderThreadStartTime();

        Calendar c = Calendar.getInstance();
        c.setTime(startTime);
        assertEquals(1, c.get(Calendar.MINUTE));
        assertEquals(0, c.get(Calendar.HOUR_OF_DAY));
        assertTrue(c.after(Calendar.getInstance()));
    }

    /**
     * Test the start time.
     */
    @Test
    public void testStartTime2() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("scheduled_reminder_thread_start_time");
        setting.setValue("2359");

        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        Date startTime = serviceToTest.getScheduledReminderThreadStartTime();

        Calendar c = Calendar.getInstance();
        c.setTime(startTime);
        assertEquals(MAX_MINUTES, c.get(Calendar.MINUTE));
        assertEquals(MAX_HOURS, c.get(Calendar.HOUR_OF_DAY));
        assertTrue(c.after(Calendar.getInstance()));
    }

    /**
     * Test an invalid thread start time.
     */
    @Test
    public void testInvalidStartTime() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("scheduled_reminder_thread_start_time");
        setting.setValue("1");

        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        Date startTime = serviceToTest.getScheduledReminderThreadStartTime();

        assertNull(startTime);
    }

    /**
     * Test the start time.
     */
    @Test
    public void testDisplayPILegalFields() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("display_pi_legal_fields");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isDisplayPiLegalFields());
    }

    /**
     * Test the display prospective collection method.
     */
    @Test
    public void testDisplayProspectiveCollection() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("display_prospective_collection");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isDisplayProspectiveCollection());
    }

    /**
     * Test the display consortium registration method.
     */
    @Test
    public void testDisplayConsortiumRegistration() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("display_consortium_registration");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isDisplayConsortiumRegistration());
    }

    /**
     * Test the simple search configuration.
     */
    @Test
    public void testSimpleSearchEnabled() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("simple_search_enabled");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isSimpleSearchEnabled());
    }

    /**
     * Test the tabbed search and sectioned categories views configuration.
     */
    @Test
    public void testTabbedSearchAndSectionedCategoriesViewsEnabled() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("tabbed_search_and_sectioned_categories_views_enabled");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isTabbedSearchAndSectionedCategoriesViewsEnabled());
    }

    /**
     * Test the enabling of the voting task.
     */
    @Test
    public void testVotingTaskEnabled() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("voting_task_enabled");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isVotingTaskEnabled());
    }

    /**
     * Test the display request review votes method.
     */
    @Test
    public void testDisplayRequestReviewVotes() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("display_request_review_votes");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isDisplayRequestReviewVotes());
    }

    /**
     * Test the get review process method.
     */
    @Test
    public void testGetReviewProcess() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("review_process");
        setting.setValue(ReviewProcess.FULL_CONSORTIUM_REVIEW.name());
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertNotNull(serviceToTest.getReviewProcess());
        assertEquals(ReviewProcess.FULL_CONSORTIUM_REVIEW, serviceToTest.getReviewProcess());
    }

    /**
     * Test the is account approval active method.
     */
    @Test
    public void testIsAccountApprovalActive() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("account_approval_active");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isAccountApprovalActive());
    }

    /**
     * Test mta certification required method.
     */
    @Test
    public void testIsMtaCertificationRequired() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("mta_certification_required");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isMtaCertificationRequired());
    }

    /**
     * Test the get new request count condition method.
     */
    @Test
    public void testGetNewRequestCountCondition() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("new_request_count_condition");
        setting.setValue("1 = 1");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertNotNull(serviceToTest.getNewRequestCountCondition());
        assertEquals("1 = 1", serviceToTest.getNewRequestCountCondition());
    }

    /**
     * Test the irb approval required method.
     */
    @Test
    public void testIsIrbApprovalRequired() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("irb_approval_required");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isIrbApprovalRequired());
    }

    /**
     * Test the minimum aggregate results displayed method.
     */
    @Test
    public void testMinimumAggregateResultsDisplayed() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("minimum_aggregate_results_displayed");
        setting.setValue("0");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertEquals(0, serviceToTest.getMinimumAggregateResultsDisplayed());
    }

    /**
     * Test the display collection protocol method.
     */
    public void testDisplayCollectionProtocol() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("display_collection_protocol");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isDisplayCollectionProtocol());
    }

    /**
     * Test the specimen details panel columns.
     */
    @Test
    public void testSpecimenDetailsPanelColumns() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("specimen_details_panel_column1_label");
        setting.setValue("Anatomic Source");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);

        setting = new ApplicationSetting();
        setting.setName("specimen_details_panel_column1_value");
        setting.setValue("customProperty(anatomicSource)");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);

        setting = new ApplicationSetting();
        setting.setName("specimen_details_panel_column2_label");
        setting.setValue("Type");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);

        setting = new ApplicationSetting();
        setting.setName("specimen_details_panel_column2_value");
        setting.setValue("specimenType");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);

        setting = new ApplicationSetting();
        setting.setName("specimen_details_panel_column3_label");
        setting.setValue("Preservation Type");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);

        setting = new ApplicationSetting();
        setting.setName("specimen_details_panel_column3_value");
        setting.setValue("customProperty(preservationType)");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);

        setting = new ApplicationSetting();
        setting.setName("specimen_details_panel_column4_label");
        setting.setValue("Available Quantity");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);

        setting = new ApplicationSetting();
        setting.setName("specimen_details_panel_column4_value");
        setting.setValue("availableQuantity");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        assertEquals("Anatomic Source", serviceToTest.getSpecimenDetailsPanelColumn1Label());
        assertEquals("customProperty(anatomicSource)", serviceToTest.getSpecimenDetailsPanelColumn1Value());
        assertEquals("Type", serviceToTest.getSpecimenDetailsPanelColumn2Label());
        assertEquals("specimenType", serviceToTest.getSpecimenDetailsPanelColumn2Value());
        assertEquals("Preservation Type", serviceToTest.getSpecimenDetailsPanelColumn3Label());
        assertEquals("customProperty(preservationType)", serviceToTest.getSpecimenDetailsPanelColumn3Value());
        assertEquals("Available Quantity", serviceToTest.getSpecimenDetailsPanelColumn4Label());
        assertEquals("availableQuantity", serviceToTest.getSpecimenDetailsPanelColumn4Value());
    }

    /**
     * Test the display usage restrictions setting.
     */
    @Test
    public void testDisplayUsageRestrictions() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("display_usage_restrictions");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isDisplayUsageRestrictions());
    }

    /**
     * Test the protocol document required method.
     */
    @Test
    public void testIsProtocolDocumentRequired() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("protocol_document_required");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isProtocolDocumentRequired());
    }

    /**
     * Test home page data cache refresh interval.
     */
    @Test
    public void testHomePageDataCacheRefreshInterval() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("home_page_data_cache_refresh_interval");
        setting.setValue("2");

        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        int value = serviceToTest.getHomePageDataCacheRefreshInterval();

        assertEquals(2, value);
    }

    /**
     * Test settings defining the widgets on the home page.
     */
    @Test
    public void testHomePageWidgetSettings() {
        String widgetName = "testWidgetName";
        for (String position : new String[] {"left", "center", "right"}) {
            for (String page : new String[] {"home", "browse"}) {
                ApplicationSetting setting = new ApplicationSetting();
                setting.setName(position + "_" + page + "_widget");
                setting.setValue(widgetName);
                TissueLocatorHibernateUtil.getCurrentSession().save(setting);
                TissueLocatorHibernateUtil.getCurrentSession().flush();
                TissueLocatorHibernateUtil.getCurrentSession().clear();
            }
        }
        assertEquals(widgetName, serviceToTest.getLeftBrowseWidget());
        assertEquals(widgetName, serviceToTest.getCenterBrowseWidget());
        assertEquals(widgetName, serviceToTest.getRightBrowseWidget());
        assertEquals(widgetName, serviceToTest.getLeftHomeWidget());
        assertEquals(widgetName, serviceToTest.getCenterHomeWidget());
        assertEquals(widgetName, serviceToTest.getRightHomeWidget());
    }

    /**
     * Test the display request process steps setting.
     */
    @Test
    public void testDisplayRequestProcessSteps() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("display_request_process_steps");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isDisplayRequestProcessSteps());
    }

    /**
     * Test the pending funding status display settings.
     */
    @Test
    public void testDisplayFundingStatusPending() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("display_funding_status_pending");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isDisplayFundingStatusPending());
    }
    
    /**
     * Test the shipment billing address display settings.
     */
    @Test
    public void testDisplayShipmentBillingRecipient() {
        ApplicationSetting setting = new ApplicationSetting();
        setting.setName("display_shipment_billing_recipient");
        setting.setValue("true");
        TissueLocatorHibernateUtil.getCurrentSession().save(setting);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        assertTrue(serviceToTest.isDisplayShipmentBillingRecipient());
    }
}
