/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.config.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Before;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;

/**
 * Search field user interface category service integration tests.
 *
 * @author jstephens
 */
public class UiSearchFieldCategoryServiceTest extends AbstractHibernateTestCase {

    // CHECKSTYLE:OFF

    @Before
    public void setUpTest() {
        saveUiSearchFieldCategories(createUiSearchFieldCategories(3));
    }

    @Test
    public void testUiSearchFieldCategoryPersistence() {
        UiSearchFieldCategory expectedCategory = new UiSearchFieldCategory();
        expectedCategory.setCategoryName("Search Field Category");
        expectedCategory.setViewable(Boolean.TRUE);
        expectedCategory.setEntityClassName(Object.class.getName());
        expectedCategory.setSearchSetType(SearchSetType.ADVANCED.name());
        expectedCategory.setUiSearchCategoryFields(createUiCategoryFields(1));
        List<GenericFieldConfig> fieldConfigs = new ArrayList<GenericFieldConfig>();
        for (UiSearchCategoryField field : expectedCategory.getUiSearchCategoryFields()) {
            fieldConfigs.add(field.getFieldConfig());
        }
        expectedCategory.getUiSearchCategoryFields().get(0).setUiSearchFieldCategory(expectedCategory);
        Long id = getService().savePersistentObject(expectedCategory);
        UiSearchFieldCategory returnedCategory = getService().getPersistentObject(UiSearchFieldCategory.class, id);
        assertTrue(EqualsBuilder.reflectionEquals(expectedCategory, returnedCategory));
    }

    @Test
    public void testGetUiSearchFieldCategoriesReturnsCatagoriesInOrder() {
        List<UiSearchFieldCategory> expectedCategories = createUiSearchFieldCategories(3);
        List<UiSearchFieldCategory> returnedCategories = getService().getUiSearchFieldCategories(Specimen.class,
                SearchSetType.ADVANCED);
        assertEquals(expectedCategories.get(0).getCategoryName(), returnedCategories.get(0).getCategoryName());
        assertEquals(expectedCategories.get(1).getCategoryName(), returnedCategories.get(1).getCategoryName());
        assertEquals(expectedCategories.get(2).getCategoryName(), returnedCategories.get(2).getCategoryName());
    }

    @Test
    public void testGetUiSearchFieldCategoriesReturnsCatagoryFieldsInOrder() {
        List<UiSearchFieldCategory> expectedCategories = createUiSearchFieldCategories(3);
        List<UiSearchFieldCategory> returnedCategories = getService().getUiSearchFieldCategories(Specimen.class,
                SearchSetType.ADVANCED);
        List<UiSearchCategoryField> expectedFields = expectedCategories.get(2).getUiSearchCategoryFields();
        List<UiSearchCategoryField> returnedFields = returnedCategories.get(2).getUiSearchCategoryFields();
        assertEquals(expectedFields.get(0).getFieldConfig().getSearchFieldName(), returnedFields.get(0).getFieldConfig().getSearchFieldName());
        assertEquals(expectedFields.get(1).getFieldConfig().getSearchFieldName(), returnedFields.get(1).getFieldConfig().getSearchFieldName());
        assertEquals(expectedFields.get(2).getFieldConfig().getSearchFieldName(), returnedFields.get(2).getFieldConfig().getSearchFieldName());
    }

    @Test
    public void testGetUiSearchFieldCategoriesReturnsEmptyListWhenNoCategoriesDefined() {
        List<UiSearchFieldCategory> returnedCategories = getService().getUiSearchFieldCategories(Specimen.class,
                SearchSetType.SIMPLE);
        assertTrue(returnedCategories.isEmpty());
    }

    /**
     * Create a list of search field categories.
     *
     * @param numberOfCategories how many categories to create
     * @return the list of search categories
     */
    List<UiSearchFieldCategory> createUiSearchFieldCategories(int numberOfCategories) {
        List<UiSearchFieldCategory> categories = new ArrayList<UiSearchFieldCategory>();
        for (int count = 1; count <= numberOfCategories; count++) {
            UiSearchFieldCategory category = new UiSearchFieldCategory();
            category.setCategoryName("Search Field Category " + count);
            category.setViewable(Boolean.TRUE);
            category.setEntityClassName(Specimen.class.getName());
            category.setSearchSetType(SearchSetType.ADVANCED.name());
            category.setUiSearchCategoryFields(createUiCategoryFields(count));
            categories.add(category);
        }
        return categories;
    }

    /**
     * Persist the list of search categories.
     *
     * @param uiSearchCategories the list of categories to persist
     */
    void saveUiSearchFieldCategories(List<UiSearchFieldCategory> uiSearchCategories) {
        for (UiSearchFieldCategory uiSearchFieldCategory : uiSearchCategories) {
            getService().savePersistentObject(uiSearchFieldCategory);
        }
    }

    private UiSearchFieldCategoryServiceLocal getService() {
        return new UiSearchFieldCategoryServiceBean();
    }

    private List<UiSearchCategoryField> createUiCategoryFields(int numberOfFields) {
        List<UiSearchCategoryField> categoryFields = new ArrayList<UiSearchCategoryField>();
        for (int count = 1; count <= numberOfFields; count++) {
            UiSearchCategoryField categoryField = new UiSearchCategoryField();
            categoryField.setFieldConfig(createAndPersistGenericFieldConfig("fieldName" + count));
            categoryField.getFieldConfig().setSearchFieldName("fieldName" + count);
            categoryFields.add(categoryField);
        }
        return categoryFields;
    }
    // CHECKSTYLE:ON
}
