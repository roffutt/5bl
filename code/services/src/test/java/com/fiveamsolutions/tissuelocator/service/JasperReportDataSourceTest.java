/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.design.JRDesignField;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.report.JasperReportDataSource;
import com.fiveamsolutions.tissuelocator.service.report.ReportDataProvider;
import com.fiveamsolutions.tissuelocator.service.report.ScrollingDataSource;
import com.fiveamsolutions.tissuelocator.test.AbstractHibernateTestCase;

/**
 * @author ddasgupta
 *
 */
public class JasperReportDataSourceTest extends AbstractHibernateTestCase {

    private static final int RECORD_COUNT = 75;

    /**
     * Test the report data provider.
     */
    @Test
    public void testReportDataProvider() {
        TissueLocatorUser u = createUser(true, getCurrentUser().getInstitution());
        String queryString = "from " + TissueLocatorUser.class.getName() + " where id = :id ";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", u.getId());
        ReportDataProvider provider = new ReportDataProvider(queryString, params);
        List<PersistentObject> results = provider.getReportData(0, 2);
        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(u.getClass(), results.get(0).getClass());
        assertEquals(u.getId(), results.get(0).getId());
    }

    /**
     * Test the scrolling data source.
     */
    @Test
    public void testScrollingDataSource() {
        MockReportDataProvider provider = new MockReportDataProvider(null, null);
        ScrollingDataSource dataSource = new ScrollingDataSource(provider);

        for (int i = 0; i < RECORD_COUNT; i++) {
            assertNotNull(dataSource.getObject(i));
        }
        assertEquals(2, provider.queryCount);
    }

    /**
     * Test the jasper report data source.
     * @throws JRException on error
     */
    @Test
    public void testJasperReportDataSource() throws JRException {
        MockReportDataProvider provider = new MockReportDataProvider(null, null);
        ScrollingDataSource dataSource = new ScrollingDataSource(provider);
        JasperReportDataSource jrDataSource = new JasperReportDataSource(dataSource);
        assertTrue(jrDataSource.next());
        jrDataSource.moveFirst();
        assertTrue(jrDataSource.next());
        JRDesignField field = new JRDesignField();
        field.setName("username");
        assertNull(jrDataSource.getFieldValue(field));
        assertNull(jrDataSource.getFieldValue(field));
    }

    /**
     * Test the invalid field in jasper report data source.
     * @throws JRException on error
     */
    @Test(expected = JRException.class)
    public void testInvalidField() throws JRException {
        MockReportDataProvider provider = new MockReportDataProvider(null, null);
        ScrollingDataSource dataSource = new ScrollingDataSource(provider);
        JasperReportDataSource jrDataSource = new JasperReportDataSource(dataSource);
        assertTrue(jrDataSource.next());
        jrDataSource.moveFirst();
        assertTrue(jrDataSource.next());
        JRDesignField invalidfield = new JRDesignField();
        invalidfield.setName("invalid");
        jrDataSource.getFieldValue(invalidfield);
    }

    /**
     * Mock report data provider that keeps track of how many times the query is called.
     * @author ddasgupta
     *
     */
    private class MockReportDataProvider extends ReportDataProvider {

        private int queryCount = 0;

        /**
         * @param queryString
         * @param queryParameters
         */
        public MockReportDataProvider(String queryString, Map<String, Object> queryParameters) {
            super(queryString, queryParameters);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public List<PersistentObject> getReportData(int firstResult, int maxResults) {
            List<PersistentObject> results = new ArrayList<PersistentObject>();
            for (int i = 0; i < maxResults; i++) {
                results.add(new TissueLocatorUser());
            }
            queryCount++;
            return results;
        }
    }
}
