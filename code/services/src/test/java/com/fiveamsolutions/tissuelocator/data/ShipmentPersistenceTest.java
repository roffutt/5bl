/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.data.security.ApplicationRole;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenReceiptQuality;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceBean;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SignedMaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.reminder.ScheduledReminderServiceBean;
import com.fiveamsolutions.tissuelocator.service.search.ShipmentSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.test.AbstractSpecimenRequestTest;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
public class ShipmentPersistenceTest extends AbstractPersistenceTest<Shipment> {

    private static final int GRACE_PERIOD = 3;
    private static final int NUM_SHIPMENTS = 10;

    /**
     * {@inheritDoc}
     */
    @Override
    public Shipment[] getValidObjects() {
        AbstractSpecimenRequestTest requestTest = new SpecimenRequestPersistenceTest();
        SpecimenRequest[] requests = requestTest.getValidObjects();
        for (SpecimenRequest request : requests) {
            TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService().savePersistentObject(request);
        }
        Shipment[] shipments = new Shipment[requests.length];
        for (int i = 0; i < requests.length; i++) {
            Shipment shipment = new Shipment();
            shipment.setRequest(requests[i]);
            shipment.setSender(requests[i].getRequestor());
            shipment.setSendingInstitution(requests[i].getRequestor().getInstitution());
            shipment.setShippingMethod(ShippingMethod.OTHER);
            shipment.setStatus(ShipmentStatus.PENDING);
            shipment.setTrackingNumber("1234");
            shipment.setShippingPrice(new BigDecimal("12.34"));
            shipment.setFees(new BigDecimal("12.34"));
            shipment.setUpdatedDate(new Date());
            shipment.setRecipient(requests[i].getShipment().getRecipient());
            shipment.setBillingRecipient(requests[i].getShipment().getBillingRecipient());
            shipment.getLineItems().addAll(requests[i].getLineItems());
            shipment.getAggregateLineItems().addAll(requests[i].getAggregateLineItems());
            shipment.setReadyForResearcherReview(true);
            shipment.setPreviousReadyForResearcherReview(true);
            shipment.setMtaAmendment(new TissueLocatorFile(new byte[]{1}, "fileName", "contentType"));

            for (SpecimenRequestLineItem li : shipment.getLineItems()) {
                li.getSpecimen().setStatus(SpecimenStatus.PENDING_SHIPMENT);
                li.getSpecimen().setPreviousStatus(SpecimenStatus.PENDING_SHIPMENT);
            }

            shipment.setProcessingUser(getCurrentUser());
            shipments[i] = shipment;
        }
        return shipments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(Shipment expected, Shipment actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
        assertNotNull(actual.getStatus());
        assertTrue(StringUtils.isNotBlank(actual.getStatus().getResourceKey()));
        assertNotNull(actual.getShippingMethod());
        assertTrue(StringUtils.isNotBlank(actual.getShippingMethod().getResourceKey()));
        BigDecimal total = BigDecimal.ZERO;
        for (SpecimenRequestLineItem li : expected.getLineItems()) {
            if (li.getFinalPrice() != null) {
                total = total.add(li.getFinalPrice());
            }
        }
        total = total.add(new BigDecimal("24.68"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShipmentServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getShipmentService();
    }

    /**
     * test search.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        Shipment example = new Shipment();
        example.setStatus(ShipmentStatus.PENDING);
        example.setSendingInstitution(getCurrentUser().getInstitution());
        SearchCriteria<Shipment> sc = new TissueLocatorAnnotatedBeanSearchCriteria<Shipment>(example);
        PageSortParams<Shipment> psp =
              new PageSortParams<Shipment>(PAGE_SIZE, 0, ShipmentSortCriterion.ID, true);
        List<Shipment> results = getService().search(sc, psp);
        assertEquals(2, results.size());
        assertEquals(2, getService().count(sc));
        assertTrue(results.get(0).getId().compareTo(results.get(1).getId()) > 0);
        example.setStatus(ShipmentStatus.RECEIVED);
        assertEquals(0, getService().search(sc, psp).size());
        assertEquals(0, getService().count(sc));
        example.setStatus(ShipmentStatus.PENDING);
        example.setSendingInstitution(createUserSkipRole().getInstitution());
        assertEquals(0, getService().search(sc, psp).size());
        assertEquals(0, getService().count(sc));
        example.setStatus(ShipmentStatus.SHIPPED);
        example.setSendingInstitution(createUserSkipRole().getInstitution());
        assertEquals(0, getService().search(sc, psp).size());
        assertEquals(0, getService().count(sc));
    }

    /**
     * Tests the basic search functionality.
     */
    @Test
    public void testBasicSearch() {
        testSaveRetrieve();
        Shipment example = new Shipment();
        example.setStatus(ShipmentStatus.PENDING);
        example.setSendingInstitution(getCurrentUser().getInstitution());
        SearchCriteria<Shipment> sc = new TissueLocatorAnnotatedBeanSearchCriteria<Shipment>(example);
        List<Shipment> results2 = getService().search(sc);
        example.setSendingInstitution(getCurrentUser().getInstitution());
        assertEquals(2, results2.size());
        assertEquals(2, getService().count(sc));
        example.setStatus(ShipmentStatus.RECEIVED);
        assertEquals(0, getService().search(sc).size());
        assertEquals(0, getService().count(sc));
        example.setStatus(ShipmentStatus.PENDING);
        example.setSendingInstitution(createUserSkipRole().getInstitution());
        assertEquals(0, getService().search(sc).size());
        assertEquals(0, getService().count(sc));
        example.setStatus(ShipmentStatus.SHIPPED);
        example.setSendingInstitution(createUserSkipRole().getInstitution());
        assertEquals(0, getService().search(sc).size());
        assertEquals(0, getService().count(sc));
    }

    /**
     * Test shipment access.
     */
    @Test
    public void testShipmentAccess() {
        Shipment shipment = getValidObjects()[0];
        assertTrue(shipment.isAccessible(shipment.getRequest().getRequestor()));
        TissueLocatorUser user = new TissueLocatorUser();
        user.setId(shipment.getRequest().getRequestor().getId() + 1);
        user.setInstitution(new Institution());
        user.getInstitution().setId(shipment.getSendingInstitution().getId() + 1);
        assertFalse(shipment.isAccessible(user));

        ApplicationRole shipmentAdmin = new ApplicationRole();
        shipmentAdmin.setName(Role.SHIPMENT_ADMIN.getName());
        ApplicationRole voter = new ApplicationRole();
        voter.setName(Role.LINE_ITEM_REVIEW_VOTER.getName());
        ApplicationRole leadReviewer = new ApplicationRole();
        leadReviewer.setName(Role.LEAD_REVIEWER.getName());

        user.getRoles().add(leadReviewer);
        assertTrue(shipment.isAccessible(user));
        user.getRoles().clear();
        user.getRoles().add(shipmentAdmin);
        assertFalse(shipment.isAccessible(user));
        user.getRoles().clear();
        user.getRoles().add(voter);
        assertFalse(shipment.isAccessible(user));

        user.getInstitution().setId(shipment.getSendingInstitution().getId());
        user.getRoles().add(leadReviewer);
        assertTrue(shipment.isAccessible(user));
        user.getRoles().clear();
        user.getRoles().add(shipmentAdmin);
        assertTrue(shipment.isAccessible(user));
        user.getRoles().clear();
        user.getRoles().add(voter);
        assertTrue(shipment.isAccessible(user));
    }

    /**
     * Test the remove line item functionality.
     */
    @Test
    public void testRemoveLineItem() {
       Shipment[] shipments = getValidObjects();

       for (Shipment shipment : shipments) {
           if (!shipment.getLineItems().isEmpty()) {
               int size = shipment.getLineItems().size();
               getService().removeLineItem(shipment, shipment.getLineItems().iterator().next());
               assertEquals(shipment.getLineItems().size(), size - 1);
           }
       }

    }

    /**
     * Test the remove line items functionality.
     */
    @Test
    public void testRemoveLineItems() {
       Shipment[] shipments = getValidObjects();

       for (Shipment shipment : shipments) {
           if (!shipment.getLineItems().isEmpty()) {
               getService().removeAllLineItems(shipment);
               assertTrue(shipment.getLineItems().isEmpty());
           }

       }

    }

    /**
     * Tests removal of individual aggregate line items.
     */
    public void testRemoveAggregateLineItem() {
        Shipment[] shipments = getValidObjects();
        for (Shipment shipment : shipments) {
            if (!shipment.getAggregateLineItems().isEmpty()) {
                int size = shipment.getAggregateLineItems().size();
                getService().removeAggregateLineItem(shipment, shipment.getAggregateLineItems().iterator().next());
                assertEquals(shipment.getAggregateLineItems().size(), size - 1);
            }
        }
     }

    /**
     * Tests removal of all aggregate line items.
     */
    @Test
    public void testRemoveAggregateLineItems() {
        Shipment[] shipments = getValidObjects();
        for (Shipment shipment : shipments) {
            if (!shipment.getAggregateLineItems().isEmpty()) {
                getService().removeAllAggregateLineItems(shipment);
                assertTrue(shipment.getAggregateLineItems().isEmpty());
            }
        }
    }

    /**
     * Tests adding specimens to shipments.
     */
    @Test
    public void testAddSpecimenToShipment() {
        SpecimenPersistenceTest spt = new SpecimenPersistenceTest();
        Specimen[] specimens = spt.getValidObjects();
        Shipment shipment = getValidObjects()[0];
        Set<Specimen> specs = new HashSet<Specimen>();
        CollectionUtils.addAll(specs, specimens);

        getService().addSpecimenToOrder(specs, shipment);
        for (Specimen s : specs) {
            assertEquals(s.getStatus(), SpecimenStatus.PENDING_SHIPMENT);
        }
        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            assertNotNull(li.getQuantity());
            assertNotNull(li.getQuantityUnits());
        }

    }

    /**
     * test the total price.
     */
    @Test
    public void testTotalPrice() {
        Shipment s = new Shipment();
        assertEquals(BigDecimal.ZERO, s.getTotalPrice());

        s.setFees(BigDecimal.ONE);
        assertEquals(BigDecimal.ONE, s.getTotalPrice());

        s.setShippingPrice(BigDecimal.ONE);
        assertEquals(new BigDecimal(2), s.getTotalPrice());

        s.getLineItems().add(new SpecimenRequestLineItem());
        assertEquals(new BigDecimal(2), s.getTotalPrice());

        s.getLineItems().iterator().next().setFinalPrice(BigDecimal.ONE);
        assertEquals(new BigDecimal(2).add(BigDecimal.ONE), s.getTotalPrice());

        s.getAggregateLineItems().add(new AggregateSpecimenRequestLineItem());
        assertEquals(new BigDecimal(2).add(BigDecimal.ONE), s.getTotalPrice());

        s.getAggregateLineItems().iterator().next().setFinalPrice(BigDecimal.ONE);
        assertEquals(new BigDecimal(2).add(new BigDecimal(2)), s.getTotalPrice());
    }

    /**
     * test the get formatted tracking url method of the ShippingMethod enum.
     */
    @Test
    public void testGetFormattedTrackingUrl() {
        verifyShippingMethod(ShippingMethod.DHL, true);
        verifyShippingMethod(ShippingMethod.FEDEX, true);
        verifyShippingMethod(ShippingMethod.UPS, true);
        verifyShippingMethod(ShippingMethod.USPS, true);
        verifyShippingMethod(ShippingMethod.OTHER, false);
    }

    /**
     * tests updating line item statuses.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testShipmentLineItemStatusUpdates() throws MessagingException, IOException {
        Shipment shipment = getValidObjects()[0];
        getService().updateLineItemSpecimenStatus(shipment, SpecimenStatus.PENDING_SHIPMENT);
        getService().updateLineItemSpecimenStatus(shipment, SpecimenStatus.SHIPPED);
        SpecimenRequestLineItem[] lis =
            shipment.getLineItems().toArray(new SpecimenRequestLineItem[shipment.getLineItems().size()]);

        for (SpecimenRequestLineItem li : lis) {
            assertEquals(li.getSpecimen().getStatus(), SpecimenStatus.SHIPPED);
        }

        SpecimenRequestLineItem li1 = lis[0];
        SpecimenRequestLineItem li2 = lis[1];
        li1.setDisposition(SpecimenDisposition.PARTIALLY_CONSUMED_DESTROYED);
        li2.setDisposition(SpecimenDisposition.DAMAGED);

        MailUtils.setMailEnabled(true);
        TissueLocatorUser tech = new SpecimenRequestPersistenceTest().
            createTissueTechnician(shipment.getSendingInstitution());
        TissueLocatorUser tech2 = new SpecimenRequestPersistenceTest().createTissueTechnician(null);
        UsernameHolder.setUser(shipment.getSender().getEmail());
        getService().updateLineItemDispostitions(shipment);
        shipment = getService().getPersistentObject(Shipment.class, shipment.getId());
        assertEquals(ShipmentStatus.RECEIVED, shipment.getStatus());
        assertTrue(shipment.isReadyForResearcherReview());
        for (SpecimenRequestLineItem li : lis) {
            assertEquals(li.getSpecimen().getStatus(), SpecimenStatus.DESTROYED);
        }
        assertFalse(Mailbox.get(tech.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(tech.getEmail()).size());
        testEmail(tech.getEmail(), "Order " + shipment.getId() + " has been updated",
                "The requester has updated the quality");
        assertTrue(Mailbox.get(tech2.getEmail()).isEmpty());
        assertEquals(0, Mailbox.get(tech2.getEmail()).size());
        Mailbox.clearAll();

        getService().updateLineItemDispostitions(shipment);
        assertTrue(Mailbox.get(tech.getEmail()).isEmpty());
        assertEquals(0, Mailbox.get(tech.getEmail()).size());
        assertTrue(Mailbox.get(tech2.getEmail()).isEmpty());
        assertEquals(0, Mailbox.get(tech2.getEmail()).size());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(true);
    }

    /**
     * Tests getting shipments by specimen.
     */
    @Test
    public void testGetShipmentWithSpecimen() {
        Shipment[] shipments = getValidObjects();
        for (Shipment s : shipments) {
            getService().savePersistentObject(s);
        }
        for (Shipment shipment : shipments) {
            for (SpecimenRequestLineItem li : shipment.getLineItems()) {
                Shipment s = getService().getShipmentWithSpecimen(li.getSpecimen());
                assertEquals(s.getId(), shipment.getId());
            }
        }
    }

    /**
     * Tests make specimen in shipment unavailable.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testUnavailableSpecimen() throws MessagingException, IOException {
        testRemovedSpecimen(SpecimenStatus.UNAVAILABLE);
    }

    /**
     * Tests make specimen in shipment destroyed.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testDestroyedSpecimen() throws MessagingException, IOException {
        testRemovedSpecimen(SpecimenStatus.DESTROYED);
    }

    private void testRemovedSpecimen(SpecimenStatus status) throws MessagingException, IOException {
        MailUtils.setMailEnabled(true);

        Shipment shipment = getValidObjects()[0];
        TissueLocatorUser tech = new SpecimenRequestPersistenceTest().
            createTissueTechnician(shipment.getSendingInstitution());
        UsernameHolder.setUser(shipment.getSender().getEmail());
        getService().savePersistentObject(shipment);

        Mailbox.clearAll();
        assertTrue(Mailbox.get(shipment.getProcessingUser().getEmail()).isEmpty());
        int lineItemCount = shipment.getLineItems().size();
        Specimen s = shipment.getLineItems().iterator().next().getSpecimen();

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        s = TissueLocatorRegistry.getServiceLocator().getSpecimenService().
            getPersistentObject(Specimen.class, s.getId());
        s.setStatus(status);
        TissueLocatorRegistry.getServiceLocator().getSpecimenService().savePersistentObject(s);

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        shipment = getService().getPersistentObject(Shipment.class, shipment.getId());
        assertEquals(lineItemCount - 1, shipment.getLineItems().size());
        assertEquals(1, Mailbox.get(tech.getEmail()).size());
        testEmail(tech.getEmail(), "has been updated",
                "was in the order, but has been removed as it is no longer available");

        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Tests make specimen in shipment destroyed.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testDestroyedSpecimenOnShippedShimpent() throws MessagingException, IOException {
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        Shipment shipment = getValidObjects()[0];
        getService().savePersistentObject(shipment);

        int lineItemCount = shipment.getLineItems().size();
        Iterator<SpecimenRequestLineItem> it = shipment.getLineItems().iterator();
        it.next();
        Specimen s = it.next().getSpecimen();

        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        getService().markOrderAsShipped(shipment);
        assertEquals(ShipmentStatus.SHIPPED, shipment.getStatus());

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        s = TissueLocatorRegistry.getServiceLocator().getSpecimenService().
            getPersistentObject(Specimen.class, s.getId());
        s.setStatus(SpecimenStatus.DESTROYED);
        TissueLocatorRegistry.getServiceLocator().getSpecimenService().savePersistentObject(s);

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        shipment = getService().getPersistentObject(Shipment.class, shipment.getId());
        assertEquals(lineItemCount, shipment.getLineItems().size());
    }

    /**
     * Tests make specimen in shipment destroyed.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testUnavailableSpecimenOnShippedShimpent() throws MessagingException, IOException {
        Shipment shipment = getValidObjects()[0];
        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.getSpecimen().setStatus(SpecimenStatus.SHIPPED);
        }
        getService().savePersistentObject(shipment);

        int lineItemCount = shipment.getLineItems().size();
        Iterator<SpecimenRequestLineItem> it = shipment.getLineItems().iterator();
        it.next();
        Specimen s = it.next().getSpecimen();

        ApplicationRole shipmentAdmin = new ApplicationRole();
        shipmentAdmin.setName(Role.SHIPMENT_ADMIN.getName());
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(shipmentAdmin);

        s.setStatus(SpecimenStatus.DESTROYED);
        TissueLocatorRegistry.getServiceLocator().getSpecimenService().savePersistentObject(s);

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        shipment = getService().getPersistentObject(Shipment.class, shipment.getId());
        assertEquals(lineItemCount, shipment.getLineItems().size());
    }


    /**
     * Tests marking as shipped.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testMarkAsShipped() throws MessagingException, IOException {
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        Shipment[] shipments = getValidObjects();
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        for (Shipment shipment : shipments) {
            getService().markOrderAsShipped(shipment);
            assertEquals(ShipmentStatus.SHIPPED, shipment.getStatus());
            assertTrue(shipment.isReadyForResearcherReview());
            assertEquals(1, Mailbox.get(shipment.getRequest().getRequestor().getEmail()).size());
            assertEquals(RequestStatus.FULLY_PROCESSED, shipment.getRequest().getStatus());
            for (SpecimenRequestLineItem specimen : shipment.getLineItems()) {
                assertEquals(SpecimenStatus.SHIPPED, specimen.getSpecimen().getStatus());
                assertFalse(specimen.getSpecimen().getStatusTransitionHistory().isEmpty());
                assertEquals(shipment.getShipmentDate(),
                        specimen.getSpecimen().getStatusTransitionHistory().get(0).getTransitionDate());
            }
            assertNull(shipment.getSignedSenderMta());
            assertNull(shipment.getSignedRecipientMta());
            testEmail(shipment.getRequest().getRequestor().getEmail(),
                    "Shipped", "specimen quality upon receipt");
            Mailbox.clearAll();
        }

        ApplicationRole shipmentAdmin = new ApplicationRole();
        shipmentAdmin.setName(Role.SHIPMENT_ADMIN.getName());
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(shipmentAdmin);

        shipments = getValidObjects();
        List<Shipment> newShipments = new ArrayList<Shipment>();
        for (int i = 0; i < shipments.length; i++) {
            TissueLocatorHibernateUtil.getCurrentSession().evict(shipments[i].getRequest());
            Shipment shipment = new Shipment();
            shipment.setRequest(shipments[i].getRequest());
            TissueLocatorUser u = createUserSkipRole();
            u.getRoles().add(shipmentAdmin);
            shipment.setSender(u);
            shipment.setSendingInstitution(u.getInstitution());
            populateConstantFields(shipment);
            shipment.setRecipient(shipments[i].getRequest().getShipment().getRecipient());
            shipment.setProcessingUser(getCurrentUser());
            populateContact(shipment);
            shipments[i].getRequest().getOrders().add(shipment);

            UsernameHolder.setUser(shipments[i].getRecipient().getEmail());
            getService().savePersistentObject(shipments[i]);
            UsernameHolder.setUser(shipment.getSender().getEmail());
            getService().savePersistentObject(shipment);
            UsernameHolder.setUser(shipments[i].getRequest().getRequestor().getUsername());
            TissueLocatorHibernateUtil.getCurrentSession().flush();
            TissueLocatorHibernateUtil.getCurrentSession().clear();
            SpecimenRequest request = EjbTestHelper.getGenericServiceBean(SpecimenRequest.class)
                .getPersistentObject(SpecimenRequest.class, shipments[i].getRequest().getId());
            for (Shipment saved : request.getOrders()) {
                saved.setRequest(request);
                UsernameHolder.setUser(saved.getSender().getEmail());
                getService().savePersistentObject(saved);
                if (ShippingMethod.DHL.equals(saved.getShippingMethod())) {
                    newShipments.add(saved);
                }
            }
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();

        for (int i = 0; i < newShipments.size(); i++) {
            UsernameHolder.setUser(newShipments.get(i).getSender().getEmail());
            getService().markOrderAsShipped(newShipments.get(i));
            assertEquals(ShipmentStatus.SHIPPED, newShipments.get(i).getStatus());
            assertEquals(1, Mailbox.get(newShipments.get(i).getRequest().getRequestor().getEmail()).size());
            assertEquals(RequestStatus.PARTIALLY_PROCESSED, newShipments.get(i).getRequest().getStatus());
            for (SpecimenRequestLineItem specimen : newShipments.get(i).getLineItems()) {
                assertEquals(SpecimenStatus.SHIPPED, specimen.getSpecimen().getStatus());
            }
            testEmail(newShipments.get(i).getRequest().getRequestor().getEmail(),
                    "Shipped", "is shipping", "Tracking #");
            assertEquals(1, Mailbox.get("mta_contact@example.com").size());
            testEmail("mta_contact@example.com", "Shipped", "has received the following notification",
                    "is shipping", "separate orders", "Tracking #");
            Mailbox.clearAll();
        }

        TissueLocatorUser u = createUser(false, getCurrentUser().getInstitution());
        u.getRoles().add(shipmentAdmin);
        shipments = getValidObjects();
        Shipment shipment = shipments[0];
        UsernameHolder.setUser(shipment.getSender().getEmail());
        Long id = getService().savePersistentObject(shipment);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = getService().getPersistentObject(Shipment.class, id);
        SignedMaterialTransferAgreement[] mtas = new SignedMtaPersistenceTest().getValidObjects();
        SignedMaterialTransferAgreement mta1 = mtas[0];
        SignedMaterialTransferAgreement mta2 = mtas[1];
        SignedMaterialTransferAgreementServiceLocal mtaService =
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        mta1.setSendingInstitution(shipment.getSendingInstitution());
        mta2.setSendingInstitution(null);
        mta2.setReceivingInstitution(shipment.getRecipient().getOrganization());

        TissueLocatorUser mtaAdmin = createUserInRole(Role.MTA_ADMINISTRATOR, getCurrentUser().getInstitution());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        UsernameHolder.setUser(mtaAdmin.getEmail());
        Session s = TissueLocatorHibernateUtil.getCurrentSession();
        mta1.setSendingInstitution((Institution) s.load(Institution.class, mta1.getSendingInstitution().getId()));
        mta2.setReceivingInstitution((Institution) s.load(Institution.class, mta2.getReceivingInstitution().getId()));
        Long mta1Id = mtaService.saveSignedMaterialTransferAgreement(mta1);
        Long mta2Id = mtaService.saveSignedMaterialTransferAgreement(mta2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        UsernameHolder.setUser(shipment.getSender().getEmail());
        shipment = getService().getPersistentObject(Shipment.class, id);
        getService().markOrderAsShipped(shipment);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = getService().getPersistentObject(Shipment.class, id);
        assertNull(shipment.getSignedRecipientMta());
        assertNull(shipment.getSignedSenderMta());

        mta1 = mtaService.getPersistentObject(SignedMaterialTransferAgreement.class, mta1Id);
        mta1.setStatus(SignedMaterialTransferAgreementStatus.APPROVED);
        mta2 = mtaService.getPersistentObject(SignedMaterialTransferAgreement.class, mta2Id);
        mta2.setStatus(SignedMaterialTransferAgreementStatus.APPROVED);
        UsernameHolder.setUser(mtaAdmin.getEmail());
        mtaService.saveSignedMaterialTransferAgreement(mta1);
        mtaService.saveSignedMaterialTransferAgreement(mta2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = shipments[1];
        UsernameHolder.setUser(shipment.getSender().getEmail());
        id = getService().savePersistentObject(shipment);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = getService().getPersistentObject(Shipment.class, id);
        getService().markOrderAsShipped(shipment);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = getService().getPersistentObject(Shipment.class, id);
        mta1 = mtaService.getPersistentObject(SignedMaterialTransferAgreement.class, mta1Id);
        mta2 = mtaService.getPersistentObject(SignedMaterialTransferAgreement.class, mta2Id);
        assertEquals(mta1, shipment.getSignedSenderMta());
        assertEquals(mta2, shipment.getSignedRecipientMta());

        MailUtils.setMailEnabled(false);
    }

    private void populateConstantFields(Shipment shipment) {
        shipment.setShippingMethod(ShippingMethod.DHL);
        shipment.setStatus(ShipmentStatus.PENDING);
        shipment.setTrackingNumber("1234");
        shipment.setShippingPrice(new BigDecimal("12.34"));
        shipment.setFees(new BigDecimal("12.34"));
        shipment.setUpdatedDate(new Date());
    }

    private void populateContact(Shipment shipment) {
        Institution receivingInstitution = shipment.getRequest().getRequestor().getInstitution();
        receivingInstitution.setMtaContact(new Person());
        receivingInstitution.getMtaContact().setFirstName("first name");
        receivingInstitution.getMtaContact().setLastName("last name");
        receivingInstitution.getMtaContact().setEmail("mta_contact@example.com");
        receivingInstitution.getMtaContact().setOrganization(receivingInstitution);
        receivingInstitution.getMtaContact().setAddress(getAddress());
    }

    /**
     * Test the retrieval of signed mtas for a shipment.
     */
    @Test
    public void testGetSignedMtas() {
        Shipment shipment = getValidObjects()[0];
        UsernameHolder.setUser(shipment.getSender().getEmail());

        Long id = getService().savePersistentObject(shipment);

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = getService().getPersistentObject(Shipment.class, id);
        assertNull(getService().getSignedRecipientMta(shipment));
        assertNull(getService().getSignedSenderMta(shipment));

        SignedMaterialTransferAgreement[] mtas = new SignedMtaPersistenceTest().getValidObjects();
        SignedMaterialTransferAgreement mta1 = mtas[0];
        SignedMaterialTransferAgreement mta2 = mtas[1];
        SignedMaterialTransferAgreementServiceLocal mtaService =
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        mta1.setSendingInstitution(shipment.getSendingInstitution());

        TissueLocatorUser mtaAdmin = createUserInRole(Role.MTA_ADMINISTRATOR, getCurrentUser().getInstitution());
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        UsernameHolder.setUser(mtaAdmin.getEmail());

        Session s = TissueLocatorHibernateUtil.getCurrentSession();
        mta1.setSendingInstitution((Institution) s.load(Institution.class, mta1.getSendingInstitution().getId()));
        Long mta1Id = mtaService.saveSignedMaterialTransferAgreement(mta1);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        UsernameHolder.setUser(shipment.getSender().getEmail());
        shipment = getService().getPersistentObject(Shipment.class, id);
        mta1 = mtaService.getPersistentObject(SignedMaterialTransferAgreement.class, mta1Id);
        assertEquals(mta1, getService().getSignedSenderMta(shipment));
        assertNull(getService().getSignedRecipientMta(shipment));

        mta1.setSendingInstitution(null);
        mta1.setReceivingInstitution(shipment.getRecipient().getOrganization());
        mtaService.saveSignedMaterialTransferAgreement(mta1);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = getService().getPersistentObject(Shipment.class, id);
        mta1 = mtaService.getPersistentObject(SignedMaterialTransferAgreement.class, mta1Id);
        assertEquals(mta1, getService().getSignedRecipientMta(shipment));

        mta2.setSendingInstitution(shipment.getSendingInstitution());
        Long mta2Id = mtaService.saveSignedMaterialTransferAgreement(mta2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        mta2 = mtaService.getPersistentObject(SignedMaterialTransferAgreement.class, mta2Id);
        shipment = getService().getPersistentObject(Shipment.class, id);
        assertEquals(mta2, getService().getSignedSenderMta(shipment));

        mta2.setSendingInstitution(null);
        mta2.setReceivingInstitution(shipment.getRecipient().getOrganization());
        mtaService.saveSignedMaterialTransferAgreement(mta2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        mta2 = mtaService.getPersistentObject(SignedMaterialTransferAgreement.class, mta2Id);
        shipment = getService().getPersistentObject(Shipment.class, id);
        assertEquals(mta2, getService().getSignedRecipientMta(shipment));

        shipment.setSignedRecipientMta(mta1);
        shipment.setSignedSenderMta(mta1);
        getService().savePersistentObject(shipment);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        shipment = getService().getPersistentObject(Shipment.class, id);
        mta1 = mtaService.getPersistentObject(SignedMaterialTransferAgreement.class, mta1Id);
        assertEquals(mta1, getService().getSignedRecipientMta(shipment));
        assertEquals(mta1, getService().getSignedSenderMta(shipment));

        assertNull(getService().getSignedRecipientMta(new Shipment()));
        assertNull(getService().getSignedSenderMta(new Shipment()));
    }

    /**
     * Tests canceling an order.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testCancel() throws MessagingException, IOException {
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        Shipment[] shipments = getValidObjects();
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        for (Shipment shipment : shipments) {
            getService().cancelOrder(shipment);
            assertEquals(ShipmentStatus.CANCELED, shipment.getStatus());
            assertFalse(shipment.isReadyForResearcherReview());
            assertEquals(RequestStatus.FULLY_PROCESSED, shipment.getRequest().getStatus());
            for (SpecimenRequestLineItem specimen : shipment.getLineItems()) {
                assertEquals(SpecimenStatus.AVAILABLE, specimen.getSpecimen().getStatus());
            }
            assertEquals(1, Mailbox.get(shipment.getRequest().getRequestor().getEmail()).size());
            testEmail(shipment.getRequest().getRequestor().getEmail(),
                    "Canceled", "has been canceled");
            Mailbox.clearAll();
        }

        ApplicationRole shipmentAdmin = new ApplicationRole();
        shipmentAdmin.setName(Role.SHIPMENT_ADMIN.getName());
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(shipmentAdmin);

        shipments = getValidObjects();
        List<Shipment> newShipments = new ArrayList<Shipment>();
        for (int i = 0; i < shipments.length; i++) {
            TissueLocatorHibernateUtil.getCurrentSession().evict(shipments[i].getRequest());
            Shipment shipment = new Shipment();
            shipment.setRequest(shipments[i].getRequest());
            TissueLocatorUser u = createUserSkipRole();
            u.getRoles().add(shipmentAdmin);
            shipment.setSender(u);
            shipment.setSendingInstitution(u.getInstitution());
            shipment.setShippingMethod(ShippingMethod.DHL);
            shipment.setStatus(ShipmentStatus.PENDING);
            shipment.setTrackingNumber("1234");
            shipment.setShippingPrice(new BigDecimal("12.34"));
            shipment.setFees(new BigDecimal("12.34"));
            shipment.setUpdatedDate(new Date());
            shipment.setRecipient(shipments[i].getRequest().getShipment().getRecipient());
            shipment.setProcessingUser(getCurrentUser());
            shipments[i].getRequest().getOrders().add(shipment);

            UsernameHolder.setUser(shipments[i].getRecipient().getEmail());
            getService().savePersistentObject(shipments[i]);
            UsernameHolder.setUser(shipment.getSender().getUsername());
            getService().savePersistentObject(shipment);
            UsernameHolder.setUser(shipments[i].getRequest().getRequestor().getUsername());
            SpecimenRequest request = EjbTestHelper.getGenericServiceBean(SpecimenRequest.class)
                .getPersistentObject(SpecimenRequest.class, shipments[i].getRequest().getId());
            for (Shipment saved : request.getOrders()) {
                saved.setRequest(request);
                UsernameHolder.setUser(saved.getSender().getUsername());
                getService().savePersistentObject(saved);
                if (ShippingMethod.DHL.equals(saved.getShippingMethod())) {
                    newShipments.add(saved);
                }
            }
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();

        for (int i = 0; i < newShipments.size(); i++) {
            UsernameHolder.setUser(newShipments.get(i).getSender().getUsername());
            getService().cancelOrder(newShipments.get(i));
            assertEquals(ShipmentStatus.CANCELED, newShipments.get(i).getStatus());
            assertEquals(RequestStatus.PARTIALLY_PROCESSED, newShipments.get(i).getRequest().getStatus());
            for (SpecimenRequestLineItem specimen : newShipments.get(i).getLineItems()) {
                assertEquals(SpecimenStatus.AVAILABLE, specimen.getSpecimen().getStatus());
            }
            assertEquals(1, Mailbox.get(newShipments.get(i).getRequest().getRequestor().getEmail()).size());
            testEmail(newShipments.get(i).getRequest().getRequestor().getEmail(),
                    "Canceled", "has been canceled");
            Mailbox.clearAll();
        }

        MailUtils.setMailEnabled(false);
    }

    /**
     * Tests the needs quality update method.
     * @throws MessagingException on error
     */
    @Test
    public void testNeedsQualityUpdate()  throws MessagingException {
        SpecimenReceiptQuality quality = SpecimenRequestPersistenceTestHelper.createReceiptQuality();
        Shipment shipment = getValidObjects()[0];
        TissueLocatorUser alternateUser = createUserSkipRole();
        assertFalse(shipment.needsQualityUpdate(getCurrentUser()));

        shipment.setStatus(ShipmentStatus.SHIPPED);
        assertTrue(shipment.needsQualityUpdate(getCurrentUser()));
        assertFalse(shipment.needsQualityUpdate(alternateUser));

        shipment.setStatus(ShipmentStatus.RECEIVED);
        assertTrue(shipment.needsQualityUpdate(getCurrentUser()));
        assertFalse(shipment.needsQualityUpdate(alternateUser));

        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.setDisposition(SpecimenDisposition.IN_USE);
        }
        assertTrue(shipment.needsQualityUpdate(getCurrentUser()));
        assertFalse(shipment.needsQualityUpdate(alternateUser));

        verifyQualityUpdateWithDisposition(shipment, SpecimenDisposition.IN_USE, alternateUser);
        verifyQualityUpdateWithReceipt(shipment, quality, alternateUser);
    }

    private void verifyQualityUpdateWithDisposition(Shipment shipment, SpecimenDisposition disposition,
            TissueLocatorUser alternateUser) {
        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.setDisposition(null);
        }
        for (AggregateSpecimenRequestLineItem li : shipment.getAggregateLineItems()) {
            li.setDisposition(disposition);
        }
        assertTrue(shipment.needsQualityUpdate(getCurrentUser()));
        assertFalse(shipment.needsQualityUpdate(alternateUser));

        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.setDisposition(disposition);
        }
        for (AggregateSpecimenRequestLineItem li : shipment.getAggregateLineItems()) {
            li.setDisposition(null);
        }
        assertTrue(shipment.needsQualityUpdate(getCurrentUser()));
        assertFalse(shipment.needsQualityUpdate(alternateUser));

        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.setDisposition(disposition);
        }
        for (AggregateSpecimenRequestLineItem li : shipment.getAggregateLineItems()) {
            li.setDisposition(disposition);
        }
        assertTrue(shipment.needsQualityUpdate(getCurrentUser()));
        assertFalse(shipment.needsQualityUpdate(alternateUser));
    }

    private void verifyQualityUpdateWithReceipt(Shipment shipment, SpecimenReceiptQuality quality,
            TissueLocatorUser alternateUser) {
        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.setReceiptQuality(null);
        }
        for (AggregateSpecimenRequestLineItem li : shipment.getAggregateLineItems()) {
            li.setReceiptQuality(quality);
        }
        assertTrue(shipment.needsQualityUpdate(getCurrentUser()));
        assertFalse(shipment.needsQualityUpdate(alternateUser));

        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.setReceiptQuality(quality);
        }
        for (AggregateSpecimenRequestLineItem li : shipment.getAggregateLineItems()) {
            li.setReceiptQuality(null);
        }
        assertTrue(shipment.needsQualityUpdate(getCurrentUser()));
        assertFalse(shipment.needsQualityUpdate(alternateUser));

        for (SpecimenRequestLineItem li : shipment.getLineItems()) {
            li.setReceiptQuality(quality);
        }
        for (AggregateSpecimenRequestLineItem li : shipment.getAggregateLineItems()) {
            li.setReceiptQuality(quality);
        }
        assertFalse(shipment.needsQualityUpdate(getCurrentUser()));
        assertFalse(shipment.needsQualityUpdate(alternateUser));
    }

    /**
     * Test getting the unreceived orders.
     * @throws Exception on error.
     */
    @Test
    public void testGetUnreceivedOrders() throws Exception {
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        Shipment[] shipments = getValidObjects();

        ApplicationRole shipmentAdmin = new ApplicationRole();
        shipmentAdmin.setName(Role.SHIPMENT_ADMIN.getName());
        TissueLocatorHibernateUtil.getCurrentSession().saveOrUpdate(shipmentAdmin);

        for (Shipment s : shipments) {
            TissueLocatorUser sender = s.getSender();
            sender.getRoles().add(shipmentAdmin);
            UsernameHolder.setUser(sender.getEmail());
            getService().savePersistentObject(s);
        }

        for (Shipment shipment : shipments) {
            getService().markOrderAsShipped(shipment);
            assertEquals(ShipmentStatus.SHIPPED, shipment.getStatus());
            assertEquals(RequestStatus.FULLY_PROCESSED, shipment.getRequest().getStatus());
            assertNull(shipment.getNextReminderDate());
            Thread.sleep(DateUtils.MILLIS_PER_SECOND);
            assertTrue(shipment.getShipmentDate().before(new Date()));
        }

        int i = NUM_SHIPMENTS;
        for (Shipment shipment : shipments) {
            i++;
            shipment.getRecipient().setEmail("test" + i + "@example.com");
            shipment.setShipmentDate(DateUtils.addDays(new Date(), -1 * 2));
            shipment.setShipmentDate(DateUtils.addHours(shipment.getShipmentDate(), -1 * 2));
            UsernameHolder.setUser(shipment.getSender().getEmail());
            getService().savePersistentObject(shipment);
        }

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        List<Shipment>  orders = getService().getAll(Shipment.class);
        assertEquals(2, orders.size());

        orders = getService().getUnreceivedShippedOrders(GRACE_PERIOD);
        assertEquals(0, orders.size());

        orders = getService().getUnreceivedShippedOrders(1);
        assertEquals(2, orders.size());

        for (Shipment s : orders) {
            s.setNextReminderDate(DateUtils.addHours(new Date(), -1));
            UsernameHolder.setUser(s.getSender().getEmail());
            getService().savePersistentObject(s);
        }

        orders = getService().getUnreceivedShippedOrders(1);
        assertEquals(2, orders.size());

        for (Shipment s : orders) {
            s.setNextReminderDate(DateUtils.addHours(new Date(), 1));
            UsernameHolder.setUser(s.getSender().getEmail());
            getService().savePersistentObject(s);
        }

        orders = getService().getUnreceivedShippedOrders(1);
        assertEquals(0, orders.size());

        orders = getService().getAll(Shipment.class);
        for (Shipment s : orders) {
            s.setNextReminderDate(DateUtils.addHours(new Date(), -1));
            UsernameHolder.setUser(s.getSender().getEmail());
            getService().savePersistentObject(s);
        }

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        orders = getService().getUnreceivedShippedOrders(1);
        assertEquals(2, orders.size());

        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        UsernameHolder.setUser(orders.get(1).getSender().getEmail());
        ScheduledReminderServiceBean reminderService = new ScheduledReminderServiceBean();
        reminderService.setShipmentService(new ShipmentServiceBean());
        reminderService.sendReminderEmails(1, 0, 0, 0);

        for (Shipment s : orders) {
            assertEquals(1, Mailbox.get(s.getRecipient().getEmail()).size());
            assertEquals(2, Mailbox.get(s.getProcessingUser().getEmail()).size());
        }

        orders = getService().getUnreceivedShippedOrders(1);
        assertEquals(0, orders.size());
    }

    private void verifyShippingMethod(ShippingMethod method, boolean hasUrl) {
        assertTrue(StringUtils.isBlank(method.getFormattedTrackingUrl(null)));
        assertTrue(StringUtils.isBlank(method.getFormattedTrackingUrl("")));
        assertEquals(hasUrl, StringUtils.isNotBlank(method.getFormattedTrackingUrl("1234")));
    }

    /**
     * Tests marking as shipped.
     * @throws MessagingException on error
     * @throws IOException on error
     */
    @Test
    public void testSaveOrder() throws MessagingException, IOException {
        Shipment[] shipments = getValidObjects();
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        for (Shipment shipment : shipments) {
            shipment.setProcessingUser(null);
            getService().saveOrder(shipment);
            assertNotNull(shipment.getProcessingUser());
            assertTrue(Mailbox.get(shipment.getRequest().getRequestor().getEmail()).isEmpty());

            shipment.setProcessingUser(null);
            shipment.setReadyForResearcherReview(false);
            getService().saveOrder(shipment);
            assertNotNull(shipment.getProcessingUser());
            assertTrue(Mailbox.get(shipment.getRequest().getRequestor().getEmail()).isEmpty());

            shipment.setProcessingUser(null);
            shipment.setPreviousReadyForResearcherReview(false);
            getService().saveOrder(shipment);
            assertNotNull(shipment.getProcessingUser());
            assertTrue(Mailbox.get(shipment.getRequest().getRequestor().getEmail()).isEmpty());

            shipment.setProcessingUser(null);
            shipment.setReadyForResearcherReview(true);
            getService().saveOrder(shipment);
            assertNotNull(shipment.getProcessingUser());
            assertFalse(Mailbox.get(shipment.getRequest().getRequestor().getEmail()).isEmpty());
            assertEquals(1, Mailbox.get(shipment.getRequest().getRequestor().getEmail()).size());
            testEmail(shipment.getRequest().getRequestor().getEmail(),
                    "has been updated", "updated");
            Mailbox.clearAll();
        }
        MailUtils.setMailEnabled(false);
    }

    /**
     * test the get specimen count method.
     */
    @Test
    public void testGetSpecimenCount() {
        Shipment s = new Shipment();
        s.getLineItems().add(new SpecimenRequestLineItem());
        s.getAggregateLineItems().add(new AggregateSpecimenRequestLineItem());
        s.getAggregateLineItems().iterator().next().setQuantity(2);
        assertEquals(2 + 1, s.getSpecimenCount());
    }


    /**
     * test the get shipped order count method.
     */
    @Test
    public void testGetShippedOrderCount() {
        Date today = new Date();
        Date tomorrow = DateUtils.addDays(new Date(), 1);
        Date future = DateUtils.addDays(new Date(), 2);
        assertEquals(0, getService().getShippedOrderCount(today, future));
        testSaveRetrieve();
        for (Shipment s : getService().getAll(Shipment.class)) {
            s.setShipmentDate(tomorrow);
            getService().savePersistentObject(s);
        }
        assertEquals(2, getService().getShippedOrderCount(today, future));
        assertEquals(2, getService().getShippedOrderCount(tomorrow, future));
        assertEquals(2, getService().getShippedOrderCount(today, tomorrow));
        assertEquals(0, getService().getShippedOrderCount(today, today));
        assertEquals(2, getService().getShippedOrderCount(tomorrow, tomorrow));
        assertEquals(0, getService().getShippedOrderCount(future, future));
    }
}
