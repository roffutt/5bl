/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.service.search;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Enclosed;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;

/**
 * Time unit range search condition unit tests.
 *
 * @author jstephens
 */
@RunWith(Enclosed.class)
public class TimeUnitRangeSearchConditionTest {

    private static final int MIN_AGE_MONTHS = 12;
    private static final int MAX_AGE_MONTHS = 24;
    private static final int MIN_AGE_DAYS = 4380;
    private static final int MAX_AGE_DAYS = 8760;
    private static final int MIN_AGE_HOURS = 105120;
    private static final int MAX_AGE_HOURS = 210240;

    /**
     * test time unite range search condition when the time unit is in years.
     */
    public static class WhenTimeUnitInYears {
        private Specimen specimen;
        private TimeUnitRangeSearchCondition condition;
        private Map<String, Object> conditionParameters;
        private Map<String, Object> bindParameters;
        private StringBuffer whereClause;

        /**
         * Set up year tests.
         */
        @Before
        public void setUp() {
            specimen = new Specimen();
            specimen.setPatientAgeAtCollectionUnits(TimeUnits.YEARS);
            condition = new TimeUnitRangeSearchCondition();
            condition.setPropertyName("patientAgeAtCollection");
            conditionParameters = new HashMap<String, Object>();
            bindParameters = new HashMap<String, Object>();
            whereClause = new StringBuffer();
        }

        /**
         * verify the condition is appended to the where clause for the min value.
         */
        @Test
        public void shouldAppendToWhereClauseForMinValue() {
            conditionParameters.put("min_patientAgeAtCollection", 1);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMinValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify the condition is appended to the where clause for the max value.
         */
        @Test
        public void shouldAppendToWhereClauseForMaxValue() {
            conditionParameters.put("max_patientAgeAtCollection", 2);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMaxValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify the condition is appended to the where clause for the min and max values.
         */
        @Test
        public void shouldAppendToWhereClauseForMinAndMaxValue() {
            conditionParameters.put("min_patientAgeAtCollection", 1);
            conditionParameters.put("max_patientAgeAtCollection", 2);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMinAndMaxValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify that no condition is appended for empty parameters.
         */
        @Test
        public void shouldNotAppendToWhereClauseForEmptyParameters() {
            conditionParameters.put("min_patientAgeAtCollection", StringUtils.EMPTY);
            conditionParameters.put("max_patientAgeAtCollection", StringUtils.EMPTY);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(StringUtils.EMPTY, whereClause.toString());
        }

        /**
         * verify that no condition is appended when no time units are specified.
         */
        @Test
        public void shouldNotAppendToWhereClauseWhenNoTimeUnit() {
            specimen.setPatientAgeAtCollectionUnits(null);
            conditionParameters.put("min_patientAgeAtCollection", 1);
            conditionParameters.put("max_patientAgeAtCollection", 2);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(StringUtils.EMPTY, whereClause.toString());
        }

        private String whereClauseForMinValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection >= 1.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection >= 12.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection >= 365.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection >= 8760.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

        private String whereClauseForMaxValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection <= 2.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection <= 24.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection <= 730.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection <= 17520.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

        private String whereClauseForMinAndMaxValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection >= 1.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection >= 12.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection >= 365.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection >= 8760.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(") AND (");
            sb.append("obj.patientAgeAtCollection <= 2.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection <= 24.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection <= 730.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection <= 17520.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

    }

    /**
     * test time unite range search condition when the time unit is in months.
     */
    public static class WhenTimeUnitInMonths {

        private Specimen specimen;
        private TimeUnitRangeSearchCondition condition;
        private Map<String, Object> conditionParameters;
        private Map<String, Object> bindParameters;
        private StringBuffer whereClause;

        /**
         * Set up month tests.
         */
        @Before
        public void setUp() {
            specimen = new Specimen();
            specimen.setPatientAgeAtCollectionUnits(TimeUnits.MONTHS);
            condition = new TimeUnitRangeSearchCondition();
            condition.setPropertyName("patientAgeAtCollection");
            conditionParameters = new HashMap<String, Object>();
            bindParameters = new HashMap<String, Object>();
            whereClause = new StringBuffer();
        }

        /**
         * verify the condition is appended to the where clause for the min value.
         */
        @Test
        public void shouldAppendToWhereClauseForMinValue() {
            conditionParameters.put("min_patientAgeAtCollection", MIN_AGE_MONTHS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMinValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify the condition is appended to the where clause for the max value.
         */
        @Test
        public void shouldAppendToWhereClauseForMaxValue() {
            conditionParameters.put("max_patientAgeAtCollection", MAX_AGE_MONTHS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMaxValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify the condition is appended to the where clause for the min and max values.
         */
        @Test
        public void shouldAppendToWhereClauseForMinAndMaxValue() {
            conditionParameters.put("min_patientAgeAtCollection", MIN_AGE_MONTHS);
            conditionParameters.put("max_patientAgeAtCollection", MAX_AGE_MONTHS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMinAndMaxValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify that no condition is appended for empty parameters.
         */
        @Test
        public void shouldNotAppendToWhereClauseForEmptyParameters() {
            conditionParameters.put("min_patientAgeAtCollection", StringUtils.EMPTY);
            conditionParameters.put("max_patientAgeAtCollection", StringUtils.EMPTY);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(StringUtils.EMPTY, whereClause.toString());
        }

        /**
         * verify that no condition is appended when no time units are specified.
         */
        @Test
        public void shouldNotAppendToWhereClauseWhenNoTimeUnit() {
            specimen.setPatientAgeAtCollectionUnits(null);
            conditionParameters.put("min_patientAgeAtCollection", MIN_AGE_MONTHS);
            conditionParameters.put("max_patientAgeAtCollection", MAX_AGE_MONTHS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(StringUtils.EMPTY, whereClause.toString());
        }

        private String whereClauseForMinValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection >= 1.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection >= 12.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection >= 360.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection >= 8640.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

        private String whereClauseForMaxValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection <= 2.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection <= 24.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection <= 720.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection <= 17280.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

        private String whereClauseForMinAndMaxValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection >= 1.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection >= 12.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection >= 360.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection >= 8640.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(") AND (");
            sb.append("obj.patientAgeAtCollection <= 2.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection <= 24.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection <= 720.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection <= 17280.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

    }

    /**
     * test time unite range search condition when the time unit is in daays.
     */
    public static class WhenTimeUnitInDays {

        private Specimen specimen;
        private TimeUnitRangeSearchCondition condition;
        private Map<String, Object> conditionParameters;
        private Map<String, Object> bindParameters;
        private StringBuffer whereClause;

        /**
         * Set up day tests.
         */
        @Before
        public void setUp() {
            specimen = new Specimen();
            specimen.setPatientAgeAtCollectionUnits(TimeUnits.DAYS);
            condition = new TimeUnitRangeSearchCondition();
            condition.setPropertyName("patientAgeAtCollection");
            conditionParameters = new HashMap<String, Object>();
            bindParameters = new HashMap<String, Object>();
            whereClause = new StringBuffer();
        }

        /**
         * verify the condition is appended to the where clause for the min value.
         */
        @Test
        public void shouldAppendToWhereClauseForMinValue() {
            conditionParameters.put("min_patientAgeAtCollection", MIN_AGE_DAYS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMinValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify the condition is appended to the where clause for the max value.
         */
        @Test
        public void shouldAppendToWhereClauseForMaxValue() {
            conditionParameters.put("max_patientAgeAtCollection", MAX_AGE_DAYS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMaxValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify the condition is appended to the where clause for the min and max values.
         */
        @Test
        public void shouldAppendToWhereClauseForMinAndMaxValue() {
            conditionParameters.put("min_patientAgeAtCollection", MIN_AGE_DAYS);
            conditionParameters.put("max_patientAgeAtCollection", MAX_AGE_DAYS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMinAndMaxValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify that no condition is appended for empty parameters.
         */
        @Test
        public void shouldNotAppendToWhereClauseForEmptyParameters() {
            conditionParameters.put("min_patientAgeAtCollection", StringUtils.EMPTY);
            conditionParameters.put("max_patientAgeAtCollection", StringUtils.EMPTY);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(StringUtils.EMPTY, whereClause.toString());
        }

        /**
         * verify that no condition is appended when no time units are specified.
         */
        @Test
        public void shouldNotAppendToWhereClauseWhenNoTimeUnit() {
            specimen.setPatientAgeAtCollectionUnits(null);
            conditionParameters.put("min_patientAgeAtCollection", MIN_AGE_DAYS);
            conditionParameters.put("max_patientAgeAtCollection", MAX_AGE_DAYS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(StringUtils.EMPTY, whereClause.toString());
        }

        private String whereClauseForMinValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection >= 12.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection >= 146.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection >= 4380.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection >= 105120.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

        private String whereClauseForMaxValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection <= 24.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection <= 292.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection <= 8760.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection <= 210240.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

        private String whereClauseForMinAndMaxValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection >= 12.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection >= 146.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection >= 4380.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection >= 105120.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(") AND (");
            sb.append("obj.patientAgeAtCollection <= 24.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection <= 292.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection <= 8760.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection <= 210240.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }
    }

    /**
     * test time unite range search condition when the time unit is in hours.
     */
    public static class WhenTimeUnitInHours {

        private Specimen specimen;
        private TimeUnitRangeSearchCondition condition;
        private Map<String, Object> conditionParameters;
        private Map<String, Object> bindParameters;
        private StringBuffer whereClause;

        /**
         * Set up hour tests.
         */
        @Before
        public void setUp() {
            specimen = new Specimen();
            specimen.setPatientAgeAtCollectionUnits(TimeUnits.HOURS);
            condition = new TimeUnitRangeSearchCondition();
            condition.setPropertyName("patientAgeAtCollection");
            conditionParameters = new HashMap<String, Object>();
            bindParameters = new HashMap<String, Object>();
            whereClause = new StringBuffer();
        }

        /**
         * verify the condition is appended to the where clause for the min value.
         */
        @Test
        public void shouldAppendToWhereClauseForMinValue() {
            conditionParameters.put("min_patientAgeAtCollection", MIN_AGE_HOURS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMinValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify the condition is appended to the where clause for the max value.
         */
        @Test
        public void shouldAppendToWhereClauseForMaxValue() {
            conditionParameters.put("max_patientAgeAtCollection", MAX_AGE_HOURS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMaxValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify the condition is appended to the where clause for the min and max values.
         */
        @Test
        public void shouldAppendToWhereClauseForMinAndMaxValue() {
            conditionParameters.put("min_patientAgeAtCollection", MIN_AGE_HOURS);
            conditionParameters.put("max_patientAgeAtCollection", MAX_AGE_HOURS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(whereClauseForMinAndMaxValue(), actualWhereClause(whereClause, bindParameters));
        }

        /**
         * verify that no condition is appended for empty parameters.
         */
        @Test
        public void shouldNotAppendToWhereClauseForEmptyParameters() {
            conditionParameters.put("min_patientAgeAtCollection", StringUtils.EMPTY);
            conditionParameters.put("max_patientAgeAtCollection", StringUtils.EMPTY);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(StringUtils.EMPTY, whereClause.toString());
        }

        /**
         * verify that no condition is appended when no time units are specified.
         */
        @Test
        public void shouldNotAppendToWhereClauseWhenNoTimeUnit() {
            specimen.setPatientAgeAtCollectionUnits(null);
            conditionParameters.put("min_patientAgeAtCollection", MIN_AGE_HOURS);
            conditionParameters.put("max_patientAgeAtCollection", MAX_AGE_HOURS);
            condition.setConditionParameters(conditionParameters);
            condition.appendCondition(specimen, whereClause, bindParameters);
            assertEquals(StringUtils.EMPTY, whereClause.toString());
        }

        private String whereClauseForMinValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection >= 12.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection >= 146.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection >= 4380.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection >= 105120.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

        private String whereClauseForMaxValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection <= 24.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection <= 292.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection <= 8760.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection <= 210240.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

        private String whereClauseForMinAndMaxValue() {
            StringBuilder sb = new StringBuilder("WHERE (");
            sb.append("obj.patientAgeAtCollection >= 12.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection >= 146.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection >= 4380.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection >= 105120.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(") AND (");
            sb.append("obj.patientAgeAtCollection <= 24.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = YEARS OR ");
            sb.append("obj.patientAgeAtCollection <= 292.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = MONTHS OR ");
            sb.append("obj.patientAgeAtCollection <= 8760.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = DAYS OR ");
            sb.append("obj.patientAgeAtCollection <= 210240.0 AND ");
            sb.append("obj.patientAgeAtCollectionUnits = HOURS");
            sb.append(")");
            return sb.toString();
        }

    }

    private static String actualWhereClause(StringBuffer whereClause, Map<String, Object> bindParameters) {
        String actualWhereClause = whereClause.toString().trim();
        for (Map.Entry<String, Object> entry : bindParameters.entrySet()) {
            String parameterName  = ":" + entry.getKey();
            String parameterValue = entry.getValue().toString();
            actualWhereClause = actualWhereClause.replaceAll(parameterName, parameterValue);
        }
        return actualWhereClause;
    }

}