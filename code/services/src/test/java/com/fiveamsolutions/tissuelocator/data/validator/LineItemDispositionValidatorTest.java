/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data.validator;

import junit.framework.Assert;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenDisposition;
import com.fiveamsolutions.tissuelocator.data.SpecimenDispositionAssignable;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.validation.LineItemDispositionValidator;

/**
 * @author smiller
 *
 */
public class LineItemDispositionValidatorTest {
    
    /**
     * test the validator.
     */
    @Test
    public void testValidator() {
       LineItemDispositionValidator validator = new LineItemDispositionValidator();
       Assert.assertFalse(validator.isValid(null));
       
       for (SpecimenDispositionAssignable li : new SpecimenDispositionAssignable[]{
               new SpecimenRequestLineItem(),
               new AggregateSpecimenRequestLineItem()
       }) {
           Assert.assertTrue(validator.isValid(li));
           
           li.setDisposition(SpecimenDisposition.IN_USE);
           Assert.assertTrue(validator.isValid(li));
           
           li.setDisposition(null);
           li.setPreviousDisposition(SpecimenDisposition.IN_USE);
           Assert.assertFalse(validator.isValid(li));
           
           li.setDisposition(SpecimenDisposition.IN_USE);
           li.setPreviousDisposition(SpecimenDisposition.IN_USE);
           Assert.assertTrue(validator.isValid(li));
           
           li.setDisposition(SpecimenDisposition.DAMAGED);
           li.setPreviousDisposition(SpecimenDisposition.IN_USE);
           Assert.assertTrue(validator.isValid(li));
           
           li.setDisposition(SpecimenDisposition.PARTIALLY_CONSUMED_DESTROYED);
           li.setPreviousDisposition(SpecimenDisposition.DAMAGED);
           Assert.assertTrue(validator.isValid(li));
           
           li.setDisposition(SpecimenDisposition.DAMAGED);
           li.setPreviousDisposition(SpecimenDisposition.DAMAGED);
           Assert.assertTrue(validator.isValid(li));
           
           li.setDisposition(SpecimenDisposition.IN_USE);
           li.setPreviousDisposition(SpecimenDisposition.DAMAGED);
           Assert.assertFalse(validator.isValid(li));
       }     
    }

}
