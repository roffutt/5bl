/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.mail.MessagingException;

import org.hibernate.validator.InvalidStateException;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.test.AbstractSpecimenRequestTest;
import com.fiveamsolutions.tissuelocator.test.EjbTestHelper;
import com.fiveamsolutions.tissuelocator.test.InstitutionPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * This class includes tests related to saving specimen requests.
 * @author ddasgupta
 *
 */

public class SaveSpecimenRequestTest extends AbstractSpecimenRequestTest {

    /**
     * test the save draft request method for an invalid RequestStatus of Draft.
     * @throws MessagingException on error sending email
     * @throws IOException on error
     */
    @Test(expected = InvalidStateException.class)
    public void testSaveDraftRequestInvalidState() throws MessagingException, IOException {
        SpecimenRequest request = new SpecimenRequest();
        request.setRequestor(getCurrentUser());
        request.setStatus(RequestStatus.PENDING);
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        service.saveDraftRequest(request);
    }

    /**
     * test the save draft request method.
     * @throws MessagingException on error sending email
     * @throws IOException on error
     */
    @Test
    public void testSaveDraftRequest() throws MessagingException, IOException {
        SpecimenRequestPersistenceTestHelper.createMtaCertificationSetting(false);
        SpecimenRequest request = new SpecimenRequest();
        request.setRequestor(getCurrentUser());
        request.setStatus(RequestStatus.DRAFT);
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        Long id = service.saveDraftRequest(request);
        assertNotNull(id);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(request);
        assertEquals(RequestStatus.DRAFT, request.getStatus());
    }

    /**
     * test the save request method for ABRC.
     * @throws MessagingException on error sending email
     * @throws IOException on error
     */
    // CHECKSTYLE:OFF � method too long
    @Test
    public void testSaveRequest() throws MessagingException, IOException {
    // CHECKSTYLE:ON
        SpecimenRequestPersistenceTestHelper.createProspectiveCollectionApplicationSetting(false);
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        TissueLocatorUser instReviewer = createReviewerAssigner(getCurrentUser().getInstitution());
        InstitutionPersistenceHelper instHelper = InstitutionPersistenceHelper.getInstance();
        Institution nonCons = instHelper.createInstitution(getCurrentUser().getInstitution().getType(),
                "test-non-consortium", false);
        TissueLocatorUser invalidReviewer = createReviewerAssigner(nonCons);
        for (Institution i : TissueLocatorRegistry.getServiceLocator().
                getInstitutionService().getAll(Institution.class)) {
            i.setLastReviewAssignment(null);
            TissueLocatorHibernateUtil.getCurrentSession().update(i);
        }
        getCurrentUser().getInstitution().setLastReviewAssignment(new Date());
        TissueLocatorHibernateUtil.getCurrentSession().update(getCurrentUser().getInstitution());
        TissueLocatorUser mtaAdmin = createUserInRole(Role.MTA_ADMINISTRATOR, getCurrentUser().getInstitution());

        Timestamp now = new Timestamp(Calendar.getInstance().getTimeInMillis());
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        TissueLocatorUser[] reviewers = getReviewCommittee();
        SpecimenRequest request = createRequest(true, SpecimenStatus.AVAILABLE);
        request.setConsortiumReviews(new HashSet<SpecimenRequestReviewVote>());
        request.getReviewers().clear();
        // Specimens are required to be available upon save
        setSpecimensAvailable(request.getLineItems());
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        UsernameHolder.setUser(request.getRequestor().getUsername());
        Long id = service.saveRequest(request, 2, 1, 1, 2);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(request);
        assertEquals(reviewers.length + 1, request.getConsortiumReviews().size());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            assertNull(vote.getUser());
            assertNull(vote.getVote());
        }
        assertEquals(1, request.getInstitutionalReviews().size());
        assertNull(request.getInstitutionalReviews().iterator().next().getVote());
        assertFalse(request.getReviewers().isEmpty());
        assertEquals(2, request.getReviewers().size());
        Set<Institution> previousReviewers = new HashSet<Institution>(request.getReviewers());
        for (Institution previousReviewer : previousReviewers) {
            assertTrue(request.getReviewers().contains(previousReviewer));
            assertFalse(previousReviewer.getLastReviewAssignment().before(now));
        }
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            assertEquals(lineItem.getSpecimen().getStatus(), SpecimenStatus.UNDER_REVIEW);
        }
        assertNotNull(request.getOrderLineItems());
        assertTrue(request.getOrderLineItems().isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[1].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        assertFalse(Mailbox.get(instReviewer.getEmail()).isEmpty());
        assertTrue(Mailbox.get(invalidReviewer.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(instReviewer.getEmail()).size());
        testEmail(instReviewer.getEmail(), "request", "request");
        assertTrue(Mailbox.get(mtaAdmin.getEmail()).isEmpty());
        Mailbox.clearAll();

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = service.getPersistentObject(SpecimenRequest.class, id);
        reviewers[0] = EjbTestHelper.getTissueLocatorUserServiceBean().getByUsername(reviewers[0].getUsername());
        TissueLocatorUser reviewerOneAssigner = createReviewerAssigner(reviewers[0].getInstitution());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            for (TissueLocatorUser reviewer : reviewers) {
                if (reviewer.getInstitution().equals(vote.getInstitution())) {
                    vote.setUser(reviewer);
                }
            }
        }

        UsernameHolder.setUser(request.getRequestor().getUsername());
        // Specimens are required to be available upon save
        setSpecimensAvailable(request.getLineItems());
        id = service.saveRequest(request, 2, 1, 1, 2);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(request);
        assertEquals(reviewers.length + 1, request.getConsortiumReviews().size());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            assertNull(vote.getVote());
        }
        assertEquals(1, request.getInstitutionalReviews().size());
        assertNull(request.getInstitutionalReviews().iterator().next().getVote());
        assertFalse(request.getReviewers().isEmpty());
        assertEquals(2, request.getReviewers().size());
        for (Institution previousReviewer : previousReviewers) {
            assertTrue(request.getReviewers().contains(previousReviewer));
            assertFalse(previousReviewer.getLastReviewAssignment().before(now));
        }
        for (SpecimenRequestLineItem lineItem : request.getLineItems()) {
            assertEquals(lineItem.getSpecimen().getStatus(), SpecimenStatus.UNDER_REVIEW);
        }
        assertNotNull(request.getOrderLineItems());
        assertTrue(request.getOrderLineItems().isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertFalse(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewers[0].getEmail()).size());
        testEmail(reviewers[0].getEmail(), "revised", "resubmitted with revisions", "submit your vote");
        assertFalse(Mailbox.get(reviewers[1].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewers[1].getEmail()).size());
        testEmail(reviewers[1].getEmail(), "revised", "resubmitted with revisions", "submit your vote");
        assertFalse(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewers[2].getEmail()).size());
        testEmail(reviewers[2].getEmail(), "revised", "resubmitted with revisions", "submit your vote");

        assertFalse(Mailbox.get(reviewerOneAssigner.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(reviewerOneAssigner.getEmail()).size());
        testEmail(reviewerOneAssigner.getEmail(), "revised", "review");
        assertFalse(Mailbox.get(instReviewer.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(instReviewer.getEmail()).size());
        testEmail(instReviewer.getEmail(), "Revised", "not yet been assigned");
        assertTrue(Mailbox.get(mtaAdmin.getEmail()).isEmpty());

        Mailbox.clearAll();
        request = createRequest(true, SpecimenStatus.AVAILABLE);
        request.getReviewers().clear();
        for (Institution i : TissueLocatorRegistry.getServiceLocator().getInstitutionService().
                getAll(Institution.class)) {
            i.setLastReviewAssignment(new Date());
            TissueLocatorHibernateUtil.getCurrentSession().update(i);
        }
        Institution specInst = request.getLineItems().iterator().next().getSpecimen().getExternalIdAssigner();
        specInst.setLastReviewAssignment(null);
        TissueLocatorHibernateUtil.getCurrentSession().update(specInst);
        instReviewer = (TissueLocatorUser) TissueLocatorHibernateUtil.getCurrentSession().load(TissueLocatorUser.class,
                instReviewer.getId());
        instReviewer.setInstitution(specInst);
        TissueLocatorHibernateUtil.getCurrentSession().update(instReviewer);
        id = service.saveRequest(request, 2, 1, 1, 2);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(request);
        assertEquals(reviewers.length + 1, request.getConsortiumReviews().size());
        for (SpecimenRequestReviewVote vote : request.getConsortiumReviews()) {
            assertNull(vote.getUser());
            assertNull(vote.getVote());
        }
        assertEquals(1, request.getInstitutionalReviews().size());
        assertNull(request.getInstitutionalReviews().iterator().next().getVote());
        assertFalse(request.getReviewers().isEmpty());
        assertEquals(2, request.getReviewers().size());
        assertTrue(request.getReviewers().contains(specInst));
        assertNotNull(request.getOrderLineItems());
        assertTrue(request.getOrderLineItems().isEmpty());
        assertTrue(Mailbox.get(request.getRequestor().getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[0].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[1].getEmail()).isEmpty());
        assertTrue(Mailbox.get(reviewers[2].getEmail()).isEmpty());
        assertFalse(Mailbox.get(instReviewer.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(instReviewer.getEmail()).size());
        testEmail(instReviewer.getEmail(), "request", "request");
        assertTrue(Mailbox.get(mtaAdmin.getEmail()).isEmpty());

        request.setFinalVote(RequestStatus.DENIED);
        service.savePersistentObject(request);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        // Specimens are required to be available upon save
        setSpecimensAvailable(request.getLineItems());
        id = service.saveRequest(request, 2, 1, 1, 2);
        request = service.getPersistentObject(SpecimenRequest.class, id);
        assertNotNull(request);
        assertNull(request.getFinalVote());
        assertNotNull(request.getOrderLineItems());
        assertTrue(request.getOrderLineItems().isEmpty());
        assertFalse(Mailbox.get(instReviewer.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(instReviewer.getEmail()).size());
        testEmail(instReviewer.getEmail(), "revised", "request");
        assertTrue(Mailbox.get(mtaAdmin.getEmail()).isEmpty());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * Test save when Exception should be thrown when specimens are not available.
     * @throws MessagingException on error sending email
     * @throws IOException on error
     */
    @Test
    public void testSaveRequestException() throws MessagingException, IOException {
        SpecimenRequestPersistenceTestHelper.createProspectiveCollectionApplicationSetting(false);
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        // create an institutional reviewer for existing line items.
        SpecimenRequest request = createRequest(true, SpecimenStatus.UNAVAILABLE);

        // create a new line item and associated institutional reviewer
        TissueLocatorHibernateUtil.getCurrentSession().flush();

        // save off all users so we are ready for the rest of the work
        TissueLocatorHibernateUtil.getCurrentSession().flush();

        UsernameHolder.setUser(request.getRequestor().getUsername());
        // save the specimen and verify votes are set up
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();

        UsernameHolder.setUser(request.getRequestor().getUsername());
        try {
            service.saveRequest(request, 2, 1, 1, 2);
            fail("Exception should be thrown if save occurs for specimens that have status other than available.");
        } catch (IllegalStateException ise) {
            // expected
        }
        assertTrue(request.getLineItems().isEmpty());
    }

    /**
     * Test save when Exception should be thrown when specimens or prospective collection notes are not specified.
     * @throws MessagingException on error sending email
     * @throws IOException on error
     */
    @Test
    public void testSaveRequestRequiredSpecimenOrNotesException() throws MessagingException, IOException {
        SpecimenRequestPersistenceTestHelper.createProspectiveCollectionApplicationSetting(true);
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        createReviewerAssigner(getCurrentUser().getInstitution());
        SpecimenRequest request = createRequest(true, SpecimenStatus.AVAILABLE);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        UsernameHolder.setUser(request.getRequestor().getUsername());
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();

        try {
            service.saveRequest(request, 2, 1, 1, 2);
        } catch (IllegalStateException ise) {
            fail("no exception - have line items, aggregate line items and prospective collection notes");
        }

        request.getLineItems().clear();
        try {
            service.saveRequest(request, 2, 1, 1, 2);
        } catch (IllegalStateException ise) {
            fail("no exception - have notes and aggregate line items, no line items");
        }

        request.getAggregateLineItems().clear();
        try {
            service.saveRequest(request, 2, 1, 1, 2);
        } catch (IllegalStateException ise) {
            fail("no exception - have notes, no line items or aggregate line items");
        }

        request.setProspectiveCollectionNotes(null);
        try {
            service.saveRequest(request, 2, 1, 1, 2);
            fail("Exception expected - have no line items, aggregate line items, or prospective collection notes");
        } catch (IllegalStateException ise) {
            //expected
        }
    }

    /**
     * Test save when Exception should be thrown when specimens are not specified.
     * @throws MessagingException on error sending email
     * @throws IOException on error
     */
    @Test
    public void testSaveRequestRequiredSpecimenException() throws MessagingException, IOException {
        SpecimenRequestPersistenceTestHelper.createProspectiveCollectionApplicationSetting(false);
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        createReviewerAssigner(getCurrentUser().getInstitution());
        SpecimenRequest request = createRequest(true, SpecimenStatus.AVAILABLE);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        UsernameHolder.setUser(request.getRequestor().getUsername());
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();

        try {
            service.saveRequest(request, 2, 1, 1, 2);
        } catch (IllegalStateException ise) {
            fail("Exception should not be thrown - have line items and aggregate line items");
        }

        request.getLineItems().clear();
        try {
            service.saveRequest(request, 2, 1, 1, 2);
        } catch (IllegalStateException ise) {
            fail("Exception should not be thrown - have aggregate line items, no line items");
        }

        request.getAggregateLineItems().clear();
        try {
            service.saveRequest(request, 2, 1, 1, 2);
            fail("Exception should be thrown - have no line items or aggregate line items");
        } catch (IllegalStateException ise) {
            //expected
        }
    }

    /**
     * test emails that get sent to mta admins on request submission.
     * @throws MessagingException on error sending email
     * @throws IOException on error
     */
    @Test
    public void testSaveRequestMtaAdminEmails() throws MessagingException, IOException {
        MailUtils.setMailEnabled(true);
        SpecimenRequestPersistenceTestHelper.createMtaCertificationSetting(true);
        TissueLocatorUser mtaAdmin = createUserInRole(Role.MTA_ADMINISTRATOR, null);
        SpecimenRequestPersistenceTestHelper.createProspectiveCollectionApplicationSetting(false);
        SpecimenRequestPersistenceTestHelper.createReviewProcessSetting(ReviewProcess.FULL_CONSORTIUM_REVIEW);
        createReviewerAssigner(getCurrentUser().getInstitution());
        SpecimenRequest request = createRequest(true, SpecimenStatus.AVAILABLE);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        UsernameHolder.setUser(request.getRequestor().getUsername());
        SpecimenRequestServiceLocal service = TissueLocatorRegistry.getServiceLocator().getSpecimenRequestService();
        Long id = service.saveRequest(request, 2, 1, 1, 2);
        assertFalse(Mailbox.get(mtaAdmin.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(mtaAdmin.getEmail()).size());
        testEmail(mtaAdmin.getEmail(), "submitted", "has been submitted", "Please review Request",
                "Researcher's institution to verify MTA", "Check Sending institution(s) to verify MTA");
        Mailbox.clearAll();

        request = service.getPersistentObject(SpecimenRequest.class, id);
        id = service.saveRequest(request, 2, 1, 1, 2);
        assertTrue(Mailbox.get(mtaAdmin.getEmail()).isEmpty());
        Mailbox.clearAll();

        MailUtils.setMailEnabled(false);
    }
}
