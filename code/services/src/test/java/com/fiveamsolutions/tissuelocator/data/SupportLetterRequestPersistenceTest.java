/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.code.FundingSource;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequest;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterRequestStatusTransition;
import com.fiveamsolutions.tissuelocator.data.support.SupportLetterType;
import com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.service.reminder.ScheduledReminderServiceBean;
import com.fiveamsolutions.tissuelocator.service.search.SupportLetterRequestSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.support.SupportLetterRequestServiceBean;
import com.fiveamsolutions.tissuelocator.service.support.SupportLetterRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceBean;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author ddasgupta
 *
 */
public class SupportLetterRequestPersistenceTest extends AbstractPersistenceTest<SupportLetterRequest> {

    /**
     * {@inheritDoc}
     */
    @Override
    public SupportLetterRequest[] getValidObjects() {
        return new SupportLetterRequest[] {createSupportLetterRequest(true, true),
                createSupportLetterRequest(true, false), createSupportLetterRequest(false, false)};
    }

    private SupportLetterRequest createSupportLetterRequest(boolean includeOptional, boolean respondedOffline) {
        SupportLetterRequest request = new SupportLetterRequest();
        TissueLocatorUser u = getCurrentUser();
        request.setRequestor(u);

        Person investigator = new Person();
        investigator.setFirstName("investigator");
        investigator.setLastName("investigator");
        investigator.setOrganization(u.getInstitution());
        investigator.setEmail(u.getEmail());
        investigator.setAddress(getAddress());
        new GenericServiceBean<Person>().savePersistentObject(investigator);
        request.setInvestigator(investigator);
        request.setResume(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));

        request.setType(SupportLetterType.IRB);
        request.setDraftProtocol(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));
        request.setStatus(SupportRequestStatus.PENDING);

        if (includeOptional) {
            request.setType(SupportLetterType.GRANT);
            request.setGrantType("grant type");
            FundingSource fs = SpecimenRequestPersistenceTestHelper.createFundingSource();
            request.setFundingSources(Collections.singletonList(fs));

            request.setStatus(SupportRequestStatus.RESPONDED);
            request.setResponse("online response notes");
            request.setLetter(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));
            request.setStatusTransitionDate(new Date());

            if (respondedOffline) {
                request.setType(SupportLetterType.OTHER);
                request.setOtherType("other type");
                request.setGrantType(null);
                request.setFundingSources(null);

                request.setStatus(SupportRequestStatus.RESPONDED_OFFLINE);
                request.setOfflineResponseNotes("offline response notes");
                request.setResponse(null);
                request.setLetter(null);

                request.setDraftLetter(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));
                request.setIntendedSubmissionDate(DateUtils.addYears(new Date(), 1));
                request.setComments("comments");
            }
        }
        return request;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(SupportLetterRequest expected, SupportLetterRequest actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
    }

    /**
     * Verify that a request can be saved by the requestor.
     */
    @Test
    public void testSaveByRequestor() {
        GenericServiceBean<SupportLetterRequest> service = new GenericServiceBean<SupportLetterRequest>();
        service.setUserService(new TissueLocatorUserServiceBean());
        SupportLetterRequest request = getValidObjects()[0];
        Long id = service.savePersistentObject(request);
        assertNotNull(id);
    }

    /**
     * Verify that a request can be saved by a support letter request administrator.
     */
    @Test
    public void testSaveByAdministrator() {
        GenericServiceBean<SupportLetterRequest> service = new GenericServiceBean<SupportLetterRequest>();
        service.setUserService(new TissueLocatorUserServiceBean());
        SupportLetterRequest request = getValidObjects()[1];
        TissueLocatorUser admin = createUserInRole(Role.SUPPORT_LETTER_REQUEST_REVIEWER,
                getCurrentUser().getInstitution());
        UsernameHolder.setUser(admin.getUsername());
        Long id = service.savePersistentObject(request);
        assertNotNull(id);
    }

    /**
     * Verify that a request can be saved by an inaccessible user (e.g., not the admin or the requestor).
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSaveByInccessibleUser() {
        GenericServiceBean<SupportLetterRequest> service = new GenericServiceBean<SupportLetterRequest>();
        service.setUserService(new TissueLocatorUserServiceBean());
        SupportLetterRequest request = getValidObjects()[1];
        TissueLocatorUser other = createUserSkipRole();
        UsernameHolder.setUser(other.getUsername());
        service.savePersistentObject(request);
    }

    /**
     * Test support letter request status transition handling.
     */
    @Test
    public void testStatusTransitionHistory() {
        SupportLetterRequest[] requests = getValidObjects();
        SupportLetterRequest request = requests[0];
        int statusCount = -1;
        request.setStatus(SupportRequestStatus.PENDING);
        Long id = getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SupportLetterRequest.class, id);
        assertEquals(++statusCount, request.getStatusTransitionHistory().size());

        request.setStatus(SupportRequestStatus.RESPONDED_OFFLINE);
        request.setOfflineResponseNotes("offline response notes");
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SupportLetterRequest.class, id);
        assertEquals(++statusCount, request.getStatusTransitionHistory().size());
        SupportLetterRequestStatusTransition t = request.getStatusTransitionHistory().get(statusCount - 1);
        t = request.getStatusTransitionHistory().get(statusCount - 1);
        assertEquals(SupportRequestStatus.RESPONDED_OFFLINE, t.getNewStatus());
        assertEquals(SupportRequestStatus.PENDING, t.getPreviousStatus());
        assertNotNull(t.getTransitionDate());
        assertNotNull(t.getSystemTransitionDate());

        request.setStatus(SupportRequestStatus.RESPONDED);
        request.setOfflineResponseNotes(null);
        request.setResponse("response");
        request.setLetter(new TissueLocatorFile(new byte[] {1}, "document.html", "text/html"));
        Date date = new Date();
        request.setStatusTransitionDate(date);
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SupportLetterRequest.class, id);
        assertEquals(++statusCount, request.getStatusTransitionHistory().size());
        t = request.getStatusTransitionHistory().get(statusCount - 1);
        assertEquals(SupportRequestStatus.RESPONDED, t.getNewStatus());
        assertEquals(SupportRequestStatus.RESPONDED_OFFLINE, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());
        assertNotNull(t.getSystemTransitionDate());
        assertNotSame(date, t.getSystemTransitionDate());

        // Edit a previously saved date
        date = new Date();
        t.setTransitionDate(date);
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SupportLetterRequest.class, id);
        assertEquals(statusCount, request.getStatusTransitionHistory().size());
        assertEquals(SupportRequestStatus.RESPONDED, t.getNewStatus());
        assertEquals(SupportRequestStatus.RESPONDED_OFFLINE, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());

        request.setComments("new comment");
        getService().savePersistentObject(request);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        request = getService().getPersistentObject(SupportLetterRequest.class, id);
        assertEquals(statusCount, request.getStatusTransitionHistory().size());
        assertEquals(SupportRequestStatus.RESPONDED, t.getNewStatus());
        assertEquals(SupportRequestStatus.RESPONDED_OFFLINE, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());
    }

    /**
     * Test saving a request and sending notification emails.
     * @throws Exception on error
     */
    @Test
    public void testSaveNotifications() throws Exception {
        MailUtils.setMailEnabled(true);
        SupportLetterRequest request = getValidObjects()[2];
        TissueLocatorUser admin = createUserInRole(Role.SUPPORT_LETTER_REQUEST_REVIEWER,
                getCurrentUser().getInstitution());
        Long id = getService().saveSupportLetterRequest(request);
        assertNotNull(id);
        assertFalse(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(getCurrentUser().getEmail()).size());
        testEmail(getCurrentUser().getEmail(), "has been submitted",
                "Your request for a Letter of Support has been submitted", id.toString(),
                getCurrentUser().getFirstName(), getCurrentUser().getLastName(), "support@");

        assertFalse(Mailbox.get(admin.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(admin.getEmail()).size());
        testEmail(admin.getEmail(), "is awaiting your action",
                "has been submitted. Please respond to the request at your earliest convenience",
                id.toString(), getCurrentUser().getFirstName(), getCurrentUser().getLastName());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }

    /**
     * test searching for signed mtas by status and requestor.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        SupportLetterRequest example = new SupportLetterRequest();
        SearchCriteria<SupportLetterRequest> sc =
            new TissueLocatorAnnotatedBeanSearchCriteria<SupportLetterRequest>(example);
        SupportLetterRequestServiceBean service = new SupportLetterRequestServiceBean();
        service.setUserService(new TissueLocatorUserServiceBean());
        int total = service.count(sc);

        example.setStatus(SupportRequestStatus.PENDING);
        List<SupportLetterRequest> results = service.search(sc);
        assertEquals(1, results.size());
        example.setStatus(SupportRequestStatus.RESPONDED);
        results = service.search(sc);
        assertEquals(1, results.size());
        example.setStatus(SupportRequestStatus.RESPONDED_OFFLINE);
        results = service.search(sc);
        assertEquals(1, results.size());

        example.setStatus(null);
        example.setRequestor(getCurrentUser());
        results = service.search(sc);
        assertEquals(total, results.size());
        example.setRequestor(createUserSkipRole());
        results = service.search(sc);
        assertEquals(0, results.size());

        example.setRequestor(null);
        SupportLetterRequestSortCriterion[] sorts = SupportLetterRequestSortCriterion.values();
        for (SupportLetterRequestSortCriterion sort : sorts) {
            PageSortParams<SupportLetterRequest> psp =
                new PageSortParams<SupportLetterRequest>(PAGE_SIZE, 0, sort, false);
            assertEquals(total, service.search(sc, psp).size());
        }
    }

    /**
     * Test getting the unreviewed requests.
     * @throws Exception on error.
     */
    @Test
    public void testGetUnreviewedRequests() throws Exception {
        SupportLetterRequest[] unsaved = getValidObjects();
        TissueLocatorUser admin = createUserInRole(Role.SUPPORT_LETTER_REQUEST_REVIEWER,
                getCurrentUser().getInstitution());

        int i = -1;
        for (SupportLetterRequest request : unsaved) {
            request.setStatus(SupportRequestStatus.PENDING);
            request.setResponse(null);
            request.setLetter(null);
            request.setOfflineResponseNotes(null);
            request.setCreatedDate(DateUtils.addDays(request.getCreatedDate(), i));
            getService().savePersistentObject(request);
            i -= 2;
        }

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        List<SupportLetterRequest>  requests = getService().getAll(SupportLetterRequest.class);
        assertEquals(unsaved.length, requests.size());

        i = i * -1 - 1;
        requests = getService().getUnreviewedRequests(i);
        assertEquals(0, requests.size());

        i -= 2;
        requests = getService().getUnreviewedRequests(i);
        assertEquals(1, requests.size());

        for (SupportLetterRequest request : requests) {
            request.setNextReminderDate(DateUtils.addHours(new Date(), -1));
            getService().savePersistentObject(request);
        }
        requests = getService().getUnreviewedRequests(i);
        assertEquals(1, requests.size());

        for (SupportLetterRequest request : requests) {
            request.setNextReminderDate(DateUtils.addHours(new Date(), 1));
            getService().savePersistentObject(request);
        }
        requests = getService().getUnreviewedRequests(i);
        assertEquals(0, requests.size());

        requests = getService().getAll(SupportLetterRequest.class);
        for (SupportLetterRequest request : requests) {
            request.setNextReminderDate(DateUtils.addHours(new Date(), -1));
            getService().savePersistentObject(request);
        }

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        requests = getService().getUnreviewedRequests(i);
        assertEquals(1, requests.size());
        String createdDate = new SimpleDateFormat("MM/dd/yyyy").format(requests.get(0).getCreatedDate());
        String idString = requests.get(0).getId().toString();

        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        ScheduledReminderServiceBean reminderService = new ScheduledReminderServiceBean();
        reminderService.setSupportLetterRequestService(getService());
        reminderService.setUserService(new TissueLocatorUserServiceBean());
        reminderService.sendReminderEmails(0, 0, i, 0);
        assertEquals(1, Mailbox.get(admin.getEmail()).size());
        testEmail(admin.getEmail(), "is awaiting your response",
                "This request for a Letter of Support has not yet been responded to.",
                idString, getCurrentUser().getFirstName(), getCurrentUser().getLastName(),
                createdDate, String.valueOf(i));

        requests = getService().getUnreviewedRequests(i);
        assertEquals(0, requests.size());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(true);
    }

    /**
     * @return the service
     */
    @Override
    public SupportLetterRequestServiceLocal getService() {
        SupportLetterRequestServiceBean service = new SupportLetterRequestServiceBean();
        service.setUserService(new TissueLocatorUserServiceBean());
        return service;
    }

    /**
     * Test reviewing a request and sending notification emails.
     * @throws Exception on error
     */
    @Test
    public void testReviewRequest() throws Exception {
        MailUtils.setMailEnabled(true);
        SupportLetterRequestServiceBean service = new SupportLetterRequestServiceBean();
        service.setUserService(new TissueLocatorUserServiceBean());

        SupportLetterRequest request = getValidObjects()[1];
        Long id = service.reviewSupportLetterRequest(request);
        assertNotNull(id);
        assertFalse(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(getCurrentUser().getEmail()).size());
        testEmail(getCurrentUser().getEmail(), "has been updated", "Administrator has responded to your Request #",
                "for a letter of support", id.toString(),
                getCurrentUser().getFirstName(), getCurrentUser().getLastName());
        Mailbox.clearAll();

        request = getValidObjects()[0];
        id = service.reviewSupportLetterRequest(request);
        assertNotNull(id);
        assertFalse(Mailbox.get(getCurrentUser().getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(getCurrentUser().getEmail()).size());
        testEmail(getCurrentUser().getEmail(), "has been updated", "Administrator has responded to your Request #",
                "for a letter of support", id.toString(),
                getCurrentUser().getFirstName(), getCurrentUser().getLastName());
        Mailbox.clearAll();

        MailUtils.setMailEnabled(false);
    }
}
