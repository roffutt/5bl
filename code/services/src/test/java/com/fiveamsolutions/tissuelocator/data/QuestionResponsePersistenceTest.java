/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.validator.InvalidStateException;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.support.Question;
import com.fiveamsolutions.tissuelocator.data.support.QuestionResponse;
import com.fiveamsolutions.tissuelocator.data.support.QuestionResponseStatusTransition;
import com.fiveamsolutions.tissuelocator.data.support.QuestionStatus;
import com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus;
import com.fiveamsolutions.tissuelocator.data.validation.ConsistentInstitutionsValidator;
import com.fiveamsolutions.tissuelocator.service.GenericServiceBean;
import com.fiveamsolutions.tissuelocator.service.reminder.ScheduledReminderServiceBean;
import com.fiveamsolutions.tissuelocator.service.search.QuestionResponseSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.support.QuestionResponseServiceBean;
import com.fiveamsolutions.tissuelocator.service.support.QuestionResponseServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.QuestionServiceBean;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceBean;
import com.fiveamsolutions.tissuelocator.test.InstitutionPersistenceHelper;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;

/**
 * @author ddasgupta
 *
 */
public class QuestionResponsePersistenceTest extends AbstractPersistenceTest<QuestionResponse> {

    /**
     * {@inheritDoc}
     */
    @Override
    public QuestionResponse[] getValidObjects() {
        return new QuestionResponse[] {createQuestionResponse(false, false),
                createQuestionResponse(true, false), createQuestionResponse(true, true)};
    }

    private QuestionResponse createQuestionResponse(boolean includeOptional, boolean respondedOffline) {
        QuestionResponse qr = new QuestionResponse();
        qr.setInstitution(getCurrentUser().getInstitution());
        qr.setStatus(SupportRequestStatus.PENDING);
        qr.setQuestion(new QuestionPersistenceTest().getValidObjects()[0]);

        if (includeOptional) {
            qr.setStatus(SupportRequestStatus.RESPONDED);
            qr.setResponder(getCurrentUser());
            qr.setResponse("online response notes");

            if (respondedOffline) {
                qr.setStatus(SupportRequestStatus.RESPONDED_OFFLINE);
                qr.setResponse(null);
            }
        }
        return qr;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verifyObjectRetrieval(QuestionResponse expected, QuestionResponse actual) {
        assertTrue(EqualsBuilder.reflectionEquals(expected, actual));
    }

    /**
     * Verify that a question response can be saved by the requestor.
     */
    @Test
    public void testSaveByRequestor() {
        Question question = new QuestionPersistenceTest().getValidObjects()[1];
        GenericServiceBean<Question> questionService = new GenericServiceBean<Question>();
        questionService.setUserService(new TissueLocatorUserServiceBean());
        Long questionId = questionService.savePersistentObject(question);

        GenericServiceBean<QuestionResponse> service = new GenericServiceBean<QuestionResponse>();
        service.setUserService(new TissueLocatorUserServiceBean());
        question = questionService.getPersistentObject(Question.class, questionId);
        assertFalse(question.getResponses().isEmpty());
        QuestionResponse response = question.getResponses().iterator().next();
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        response = service.getPersistentObject(QuestionResponse.class, response.getId());
        assertNotNull(response.getQuestion());
        Long id = service.savePersistentObject(response);
        assertNotNull(id);
    }

    /**
     * Verify that a question response can be saved by a question responder.
     */
    @Test
    public void testSaveByReviewer() {
        Question question = new QuestionPersistenceTest().getValidObjects()[1];
        GenericServiceBean<Question> questionService = new GenericServiceBean<Question>();
        questionService.setUserService(new TissueLocatorUserServiceBean());
        Long questionId = questionService.savePersistentObject(question);

        GenericServiceBean<QuestionResponse> service = new GenericServiceBean<QuestionResponse>();
        service.setUserService(new TissueLocatorUserServiceBean());
        question = questionService.getPersistentObject(Question.class, questionId);
        assertFalse(question.getResponses().isEmpty());
        QuestionResponse response = question.getResponses().iterator().next();
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        response = service.getPersistentObject(QuestionResponse.class, response.getId());
        assertNotNull(response.getQuestion());

        TissueLocatorUser responder = createUserInRole(Role.QUESTION_RESPONDER, getCurrentUser()
                .getInstitution());
        UsernameHolder.setUser(responder.getUsername());
        Long id = service.savePersistentObject(response);
        assertNotNull(id);
    }

    /**
     * Verify that a question response can be saved by a question administrator.
     */
    @Test
    public void testSaveByAdministrator() {
        Question question = new QuestionPersistenceTest().getValidObjects()[1];
        GenericServiceBean<Question> questionService = new GenericServiceBean<Question>();
        questionService.setUserService(new TissueLocatorUserServiceBean());
        Long questionId = questionService.savePersistentObject(question);

        GenericServiceBean<QuestionResponse> service = new GenericServiceBean<QuestionResponse>();
        service.setUserService(new TissueLocatorUserServiceBean());
        question = questionService.getPersistentObject(Question.class, questionId);
        assertFalse(question.getResponses().isEmpty());
        QuestionResponse response = question.getResponses().iterator().next();
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        response = service.getPersistentObject(QuestionResponse.class, response.getId());
        assertNotNull(response.getQuestion());

        TissueLocatorUser admin = createUserInRole(Role.QUESTION_ADMINISTRATOR, getCurrentUser()
                .getInstitution());
        UsernameHolder.setUser(admin.getUsername());
        Long id = service.savePersistentObject(response);
        assertNotNull(id);
    }

    /**
     * Verify that a question response cannot be saved by an inaccessible user
     * (e.g., not the responder, admin or requestor).
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSaveByInccessibleUser() {
        Question question = new QuestionPersistenceTest().getValidObjects()[1];
        GenericServiceBean<Question> questionService = new GenericServiceBean<Question>();
        questionService.setUserService(new TissueLocatorUserServiceBean());
        Long questionId = questionService.savePersistentObject(question);

        GenericServiceBean<QuestionResponse> service = new GenericServiceBean<QuestionResponse>();
        service.setUserService(new TissueLocatorUserServiceBean());
        question = questionService.getPersistentObject(Question.class, questionId);
        assertFalse(question.getResponses().isEmpty());
        QuestionResponse response = question.getResponses().iterator().next();
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        response = service.getPersistentObject(QuestionResponse.class, response.getId());
        assertNotNull(response.getQuestion());

        TissueLocatorUser other = createUserSkipRole();
        UsernameHolder.setUser(other.getUsername());
        service.savePersistentObject(response);
    }

    /**
     * Verify that a question response cannot be saved by a user from an unrelated institution.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSaveByUnrelatedInstitution() {
        Question question = new QuestionPersistenceTest().getValidObjects()[1];
        GenericServiceBean<Question> questionService = new GenericServiceBean<Question>();
        questionService.setUserService(new TissueLocatorUserServiceBean());
        Long questionId = questionService.savePersistentObject(question);

        GenericServiceBean<QuestionResponse> service = new GenericServiceBean<QuestionResponse>();
        service.setUserService(new TissueLocatorUserServiceBean());
        question = questionService.getPersistentObject(Question.class, questionId);
        assertFalse(question.getResponses().isEmpty());
        QuestionResponse response = question.getResponses().iterator().next();
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        response = service.getPersistentObject(QuestionResponse.class, response.getId());
        assertNotNull(response.getQuestion());

        Institution i = InstitutionPersistenceHelper.getInstance().createInstitution(
                getCurrentUser().getInstitution().getType(), "invalid inst", false);
        TissueLocatorHibernateUtil.getCurrentSession().save(i);
        TissueLocatorUser admin = createUserInRole(Role.QUESTION_ADMINISTRATOR, i);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        UsernameHolder.setUser(admin.getUsername());
        service.savePersistentObject(response);
    }

    /**
     * Test question response status transition handling.
     */
    @Test
    public void testStatusTransitionHistory() {
        QuestionResponse[] questionResponses = getValidObjects();
        QuestionResponse response = questionResponses[0];
        int statusCount = -1;
        response.setStatus(SupportRequestStatus.PENDING);
        Long id = getService().savePersistentObject(response);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        response = getService().getPersistentObject(QuestionResponse.class, id);
        assertEquals(++statusCount, response.getStatusTransitionHistory().size());

        response.setStatus(SupportRequestStatus.RESPONDED_OFFLINE);
        response.setResponse("response");
        getService().savePersistentObject(response);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        response = getService().getPersistentObject(QuestionResponse.class, id);
        assertEquals(++statusCount, response.getStatusTransitionHistory().size());
        QuestionResponseStatusTransition t = response.getStatusTransitionHistory().get(statusCount - 1);
        t = response.getStatusTransitionHistory().get(statusCount - 1);
        assertEquals(SupportRequestStatus.RESPONDED_OFFLINE, t.getNewStatus());
        assertEquals(SupportRequestStatus.PENDING, t.getPreviousStatus());
        assertNotNull(t.getTransitionDate());
        assertNotNull(t.getSystemTransitionDate());

        response.setStatus(SupportRequestStatus.RESPONDED);
        Date date = new Date();
        response.setStatusTransitionDate(date);
        getService().savePersistentObject(response);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        response = getService().getPersistentObject(QuestionResponse.class, id);
        assertEquals(++statusCount, response.getStatusTransitionHistory().size());
        t = response.getStatusTransitionHistory().get(statusCount - 1);
        assertEquals(SupportRequestStatus.RESPONDED, t.getNewStatus());
        assertEquals(SupportRequestStatus.RESPONDED_OFFLINE, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());
        assertNotNull(t.getSystemTransitionDate());
        assertNotSame(date, t.getSystemTransitionDate());

        // Edit a previously saved date
        date = new Date();
        t.setTransitionDate(date);
        getService().savePersistentObject(response);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        response = getService().getPersistentObject(QuestionResponse.class, id);
        assertEquals(statusCount, response.getStatusTransitionHistory().size());
        assertEquals(SupportRequestStatus.RESPONDED, t.getNewStatus());
        assertEquals(SupportRequestStatus.RESPONDED_OFFLINE, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());

        getService().savePersistentObject(response);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        response = getService().getPersistentObject(QuestionResponse.class, id);
        assertEquals(statusCount, response.getStatusTransitionHistory().size());
        assertEquals(SupportRequestStatus.RESPONDED, t.getNewStatus());
        assertEquals(SupportRequestStatus.RESPONDED_OFFLINE, t.getPreviousStatus());
        assertEquals(date, t.getTransitionDate());
    }

    /**
     * Tests question response status validator.
     */
    @Test
    public void testResponseValid() {
        QuestionResponse qr = new QuestionResponse();
        qr.setStatus(SupportRequestStatus.PENDING);
        assertTrue(qr.isResponseValid());
        qr.setStatus(SupportRequestStatus.RESPONDED);
        assertFalse(qr.isResponseValid());
        qr.setStatus(SupportRequestStatus.RESPONDED_OFFLINE);
        assertTrue(qr.isResponseValid());
        qr.setResponse("response");
        assertTrue(qr.isResponseValid());
        qr.setStatus(SupportRequestStatus.RESPONDED);
        assertTrue(qr.isResponseValid());
        qr.setStatus(SupportRequestStatus.PENDING);
        assertTrue(qr.isResponseValid());
    }

    /**
     * Tests consistent institution validator for question responses.
     */
    @Test
    public void testConsistentInstitutionsValidator() {
        ConsistentInstitutionsValidator validator = new ConsistentInstitutionsValidator();
        validator.initialize(null);
        validator.apply(null);

        Institution i = getCurrentUser().getInstitution();
        QuestionResponse qr = new QuestionResponse();
        assertTrue(validator.isValid(qr));
        qr.setInstitution(i);
        assertTrue(validator.isValid(qr));
        qr.setResponder(getCurrentUser());
        assertTrue(validator.isValid(qr));

        Institution i2 = createUserSkipRole().getInstitution();
        qr.setInstitution(i2);
        assertFalse(validator.isValid(qr));
        assertFalse(validator.isValid(new Institution()));
    }

    /**
     * Tests the unique constraint for question responses.
     */
    @Test(expected = InvalidStateException.class)
    public void testUniqueConstraint() {
        testSaveRetrieve();
        QuestionResponse existing = getService().getAll(QuestionResponse.class).iterator().next();
        QuestionResponse duplicate = new QuestionResponse();
        duplicate.setStatus(SupportRequestStatus.PENDING);
        duplicate.setInstitution(existing.getInstitution());
        duplicate.setQuestion(existing.getQuestion());
        getService().savePersistentObject(duplicate);
    }

    /**
     * test searching for question responses by status and institution.
     */
    @Test
    public void testSearch() {
        testSaveRetrieve();
        QuestionResponse example = new QuestionResponse();
        SearchCriteria<QuestionResponse> sc =
            new TissueLocatorAnnotatedBeanSearchCriteria<QuestionResponse>(example);
        QuestionResponseServiceBean service = new QuestionResponseServiceBean();
        service.setUserService(new TissueLocatorUserServiceBean());
        int total = service.getAll(QuestionResponse.class).size();

        example.setStatus(SupportRequestStatus.PENDING);
        List<QuestionResponse> results = service.search(sc);
        assertEquals(1, results.size());
        example.setStatus(SupportRequestStatus.RESPONDED);
        results = service.search(sc);
        assertEquals(1, results.size());
        example.setStatus(SupportRequestStatus.RESPONDED_OFFLINE);
        results = service.search(sc);
        assertEquals(1, results.size());

        example.setStatus(null);
        example.setInstitution(getCurrentUser().getInstitution());
        results = service.search(sc);
        assertEquals(total, results.size());
        example.setInstitution(createUserSkipRole().getInstitution());
        results = service.search(sc);
        assertEquals(0, results.size());

        example.setInstitution(getCurrentUser().getInstitution());
        QuestionResponseSortCriterion[] sorts = QuestionResponseSortCriterion.values();
        for (QuestionResponseSortCriterion sort : sorts) {
            PageSortParams<QuestionResponse> psp =
                new PageSortParams<QuestionResponse>(PAGE_SIZE, 0, sort, false);
            assertEquals(total, service.search(sc, psp).size());
        }
    }

    /**
     * Test getting the unresponded questions.
     * @throws Exception on error.
     */
    @Test
    public void testGetUnrespondedQuestions() throws Exception {
        QuestionResponse[] unsaved = getValidObjects();
        int i = -1;
        for (QuestionResponse response : unsaved) {
            response.setStatus(SupportRequestStatus.PENDING);
            response.setResponse(null);
            response.setCreatedDate(DateUtils.addDays(response.getCreatedDate(), i));
            getService().savePersistentObject(response);
            i -= 2;
        }

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        List<QuestionResponse> responses = getService().getAll(QuestionResponse.class);
        assertEquals(unsaved.length, responses.size());

        i = i * -1 - 1;
        responses = getService().getUnrespondedQuestions(i);
        assertEquals(0, responses.size());

        i -= 2;
        responses = getService().getUnrespondedQuestions(i);
        assertEquals(1, responses.size());

        for (QuestionResponse response : responses) {
            response.setNextReminderDate(DateUtils.addHours(new Date(), -1));
            getService().savePersistentObject(response);
        }
        responses = getService().getUnrespondedQuestions(i);
        assertEquals(1, responses.size());

        for (QuestionResponse response : responses) {
            response.setNextReminderDate(DateUtils.addHours(new Date(), 1));
            getService().savePersistentObject(response);
        }
        responses = getService().getUnrespondedQuestions(i);
        assertEquals(0, responses.size());

        responses = getService().getAll(QuestionResponse.class);
        for (QuestionResponse response : responses) {
            response.setNextReminderDate(DateUtils.addHours(new Date(), -1));
            getService().savePersistentObject(response);
        }

        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        responses = getService().getUnrespondedQuestions(i);
        assertEquals(1, responses.size());
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String responseDate = sdf.format(responses.get(0).getCreatedDate());
        String questionDate = sdf.format(responses.get(0).getQuestion().getCreatedDate());
        String responseIdString = responses.get(0).getId().toString();
        String questionIdString = responses.get(0).getQuestion().getId().toString();

        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        TissueLocatorUser responder = createUserInRole(Role.QUESTION_RESPONDER, getCurrentUser().getInstitution());
        TissueLocatorUser altResponder = createUserInRole(Role.QUESTION_RESPONDER, getCurrentUser().getInstitution());
        TissueLocatorUser altInstResponder = createUserInRole(Role.QUESTION_RESPONDER, null);
        TissueLocatorUser admin = createUserInRole(Role.QUESTION_ADMINISTRATOR, null);
        UsernameHolder.setUser(getCurrentUser().getUsername());
        ScheduledReminderServiceBean reminderService = new ScheduledReminderServiceBean();
        reminderService.setQuestionResponseService(getService());
        reminderService.setUserService(new TissueLocatorUserServiceBean());
        reminderService.sendReminderEmails(0, 0, 0, i);
        assertEquals(1, Mailbox.get(responder.getEmail()).size());
        testEmail(responder.getEmail(), "is awaiting your response", "has not yet been responded to",
                questionIdString, getCurrentUser().getFirstName(), getCurrentUser().getLastName(),
                responseIdString, responseDate, String.valueOf(i));
        assertEquals(1, Mailbox.get(altResponder.getEmail()).size());
        testEmail(altResponder.getEmail(), "is awaiting your response", "has not yet been responded to",
                questionIdString, getCurrentUser().getFirstName(), getCurrentUser().getLastName(),
                responseIdString, responseDate, String.valueOf(i));
        assertTrue(Mailbox.get(altInstResponder.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(admin.getEmail()).size());
        testEmail(admin.getEmail(), "is awaiting response", "has not yet been responded to by all requested",
                questionIdString, getCurrentUser().getFirstName(), getCurrentUser().getLastName(),
                questionDate, String.valueOf(i), responses.get(0).getInstitution().getName());

        responses = getService().getUnrespondedQuestions(i);
        assertEquals(0, responses.size());
        Mailbox.clearAll();
        MailUtils.setMailEnabled(true);
    }

    /**
     * Test getting the unresponded questions.
     * @throws Exception on error.
     */
    @Test
    public void testUnrespondedQuestionReminderEmail() throws Exception {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();

        TissueLocatorUser responder = createUserInRole(Role.QUESTION_RESPONDER, getCurrentUser().getInstitution());
        TissueLocatorUser altResponder = createUserInRole(Role.QUESTION_RESPONDER, null);
        TissueLocatorUser admin = createUserInRole(Role.QUESTION_ADMINISTRATOR, null);
        UsernameHolder.setUser(getCurrentUser().getUsername());

        QuestionResponse[] unsaved = getValidObjects();
        //every question has one outstanding response
        for (QuestionResponse response : unsaved) {
            response.setStatus(SupportRequestStatus.PENDING);
            response.setResponse(null);
            response.setCreatedDate(DateUtils.addMonths(response.getCreatedDate(), -1));
            getService().savePersistentObject(response);
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        //add a second outstanding response to every question
        for (Question question : getService().getAll(Question.class)) {
            QuestionResponse response = new QuestionResponse();
            response.setInstitution(altResponder.getInstitution());
            response.setStatus(SupportRequestStatus.PENDING);
            response.setResponse(null);
            response.setCreatedDate(DateUtils.addMonths(response.getCreatedDate(), -1));
            response.setQuestion(question);
            question.getResponses().add(response);
            getService().savePersistentObject(response);
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        //add a completed response to every question
        for (Question question : getService().getAll(Question.class)) {
            QuestionResponse response = new QuestionResponse();
            response.setResponder(admin);
            response.setInstitution(admin.getInstitution());
            response.setStatus(SupportRequestStatus.RESPONDED);
            response.setResponse("online response notes");
            response.setCreatedDate(DateUtils.addMonths(response.getCreatedDate(), -1));
            response.setQuestion(question);
            question.getResponses().add(response);
            getService().savePersistentObject(response);
        }
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();

        //each responder and administrator should get an email for each question
        ScheduledReminderServiceBean reminderService = new ScheduledReminderServiceBean();
        reminderService.setQuestionResponseService(getService());
        reminderService.setUserService(new TissueLocatorUserServiceBean());
        reminderService.sendReminderEmails(0, 0, 0, 2);
        assertEquals(unsaved.length, Mailbox.get(responder.getEmail()).size());
        testEmail(responder.getEmail(), "is awaiting your response", "has not yet been responded to");
        assertEquals(unsaved.length, Mailbox.get(altResponder.getEmail()).size());
        testEmail(altResponder.getEmail(), "is awaiting your response", "has not yet been responded to");
        assertEquals(unsaved.length, Mailbox.get(admin.getEmail()).size());
        testEmail(admin.getEmail(), "is awaiting response", "has not yet been responded to by all requested",
                responder.getInstitution().getName(), altResponder.getInstitution().getName());

        Mailbox.clearAll();
        MailUtils.setMailEnabled(true);
    }

    /**
     * @return the service
     */
    @Override
    public QuestionResponseServiceLocal getService() {
        QuestionResponseServiceBean service = new QuestionResponseServiceBean();
        service.setUserService(new TissueLocatorUserServiceBean());
        return service;
    }

    /**
     * Test submitting a response to a question, updating the overal question status,
     * and sending notification emails.
     * @throws Exception on error
     */
    @Test
    public void testSubmitResponse() throws Exception {
        MailUtils.setMailEnabled(true);
        QuestionResponse[] responses = getValidObjects();
        QuestionResponse response = responses[2];
        Date responseUpdatedDate = response.getLastUpdatedDate();
        Date questionUpdatedDate = response.getQuestion().getLastUpdatedDate();
        response.setResponder(null);
        String requestorEmail = response.getQuestion().getRequestor().getEmail();
        TissueLocatorUser admin = createUserInRole(Role.QUESTION_ADMINISTRATOR, getCurrentUser()
                .getInstitution());
        Long id = getService().submitQuestionResponse(response);
        assertNotNull(id);

        response = getService().getPersistentObject(QuestionResponse.class, id);
        assertEquals(SupportRequestStatus.RESPONDED_OFFLINE, response.getStatus());
        assertEquals(QuestionStatus.FULLY_RESPONDED, response.getQuestion().getStatus());
        assertNotNull(response.getResponder());
        assertEquals(admin.getId(), response.getResponder().getId());
        assertNotSame(responseUpdatedDate, response.getLastUpdatedDate());
        assertNotSame(questionUpdatedDate, response.getQuestion().getLastUpdatedDate());

        assertFalse(Mailbox.get(requestorEmail).isEmpty());
        assertEquals(1, Mailbox.get(requestorEmail).size());
        testEmail(requestorEmail, "has been updated", "Administrator has responded to your Question Request",
                "To review your question and responses", response.getQuestion().getId().toString());
        assertFalse(Mailbox.get(admin.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(admin.getEmail()).size());
        testEmail(admin.getEmail(), "has been updated", "Administrator has responded to Question Request",
                "To review the question and responses", response.getQuestion().getId().toString());
        Mailbox.clearAll();

        QuestionServiceBean qsb = new QuestionServiceBean();
        qsb.setUserService(new TissueLocatorUserServiceBean());
        Question q = responses[1].getQuestion();
        q.getResponses().add(responses[0]);
        responses[0].setQuestion(q);
        q.getResponses().add(responses[1]);
        Institution inst = InstitutionPersistenceHelper.getInstance().createInstitution(
                getCurrentUser().getInstitution().getType(), "altInst", true);
        Long instId = (Long) TissueLocatorHibernateUtil.getCurrentSession().save(inst);
        inst = (Institution) TissueLocatorHibernateUtil.getCurrentSession().get(Institution.class, instId);
        responses[0].setInstitution(inst);
        Long qId = qsb.savePersistentObject(q);
        TissueLocatorHibernateUtil.getCurrentSession().flush();
        TissueLocatorHibernateUtil.getCurrentSession().clear();
        q = qsb.getPersistentObject(Question.class, qId);
        assertEquals(2, q.getResponses().size());

        id = getService().submitQuestionResponse(q.getResponses().get(1));
        assertNotNull(id);
        response = getService().getPersistentObject(QuestionResponse.class, id);
        assertEquals(2, response.getQuestion().getResponses().size());
        assertEquals(QuestionStatus.PARTIALLY_RESPONDED, response.getQuestion().getStatus());

        assertFalse(Mailbox.get(requestorEmail).isEmpty());
        assertEquals(1, Mailbox.get(requestorEmail).size());
        testEmail(requestorEmail, "has been updated", "Administrator has responded to your Question Request",
                "To review your question and responses", response.getQuestion().getId().toString(),
                "online response notes");
        assertFalse(Mailbox.get(admin.getEmail()).isEmpty());
        assertEquals(1, Mailbox.get(admin.getEmail()).size());
        testEmail(admin.getEmail(), "has been updated", "Administrator has responded to Question Request",
                "To review the question and responses", response.getQuestion().getId().toString(),
                "online response notes");

        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
    }
}
