/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.data.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenRequestUniqueReviewerValidator;

/**
 * @author aevansel
 */
public class SpecimenRequestUniqueReviewerValidatorTest {
    
    /**
     * Tests the validator.
     */
    @Test
    public void testValidator() {
        SpecimenRequestUniqueReviewerValidator validator = new SpecimenRequestUniqueReviewerValidator();
        validator.initialize(null);
        
        assertFalse(validator.isValid(null));
        assertFalse(validator.isValid(new Specimen()));
        
        SpecimenRequest request = new SpecimenRequest();
        assertTrue(validator.isValid(request));
        
        TissueLocatorUser user1 = new TissueLocatorUser();
        user1.setId(1L);
        SpecimenRequestReviewVote vote1 = new SpecimenRequestReviewVote();
        vote1.setUser(user1);
         
        Set<SpecimenRequestReviewVote> votes = new HashSet<SpecimenRequestReviewVote>();
        votes.add(vote1);
        request.setConsortiumReviews(votes);
        assertTrue(validator.isValid(request));
        
        TissueLocatorUser user2 = new TissueLocatorUser();
        user2.setId(2L);
        SpecimenRequestReviewVote vote2 = new SpecimenRequestReviewVote();
        vote2.setUser(user2);
        votes.add(vote2);
        request.setConsortiumReviews(votes);
        assertTrue(validator.isValid(request));
        
        TissueLocatorUser user3 = new TissueLocatorUser();
        user3.setId(1L);
        SpecimenRequestReviewVote vote3 = new SpecimenRequestReviewVote();
        vote3.setUser(user3);
        votes.add(vote3);
        request.setConsortiumReviews(votes);
        assertFalse(validator.isValid(request));   
    }

}
