How To Use This Guide
=====================

This guide is intended to help a user prepare an environment to host the Biolocator.  It is not
intended to help with setting up a development environment or describe how to deploy to the created
environment.

A Note On Deploying on 5AM Solutions Virt Farm
==============================================
The 5AM IT staff has created a template for building a new server that will host
all Biolocator dependencies.  This template is compatible with the directions below for
manual installation.  So, if you need a new tier / server for a Biolocator install at 5AM
just ask for the 5AM IT staff to create a new instance of the template.

Manually Installing the Biolocator Runtime Environment
======================================================
The following instructions allow you to configure an environment by hand so that it is ready for a
Biolocator installation.

1.  Download and Install Java 6.

2.  Download and install JBoss 5.1.0 for JDK 6 from
    http://sourceforge.net/projects/jboss/files/JBoss/JBoss-5.1.0.GA/
    Set JBOSS_HOME env variable to your JBoss installation directory

    You will probably want to configure JBoss to run as a service.

3.  Update the java options in JBoss:
    Open JBOSS_HOME/bin/run.conf for editing.  The end of the file should look something like this:

    #
    # Specify options to pass to the Java VM.
    #
    if [ "x$JAVA_OPTS" = "x" ]; then
       JAVA_OPTS="-server -Xms128m -Xmx1024m -XX:PermSize=64m -XX:MaxPermSize=256m -Dsun.rmi.dgc.client.gcInterval=3600000 -Dsun.rmi.dgc.server.gcInterval=3600000"
    fi

    JAVA_OPTS="-Djava.awt.headless=true $JAVA_OPTS"
    JAVA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=9797,server=y,suspend=n $JAVA_OPTS"

4.  Disable login credential caching in JBoss:
     Open JBOSS_HOME/server/default/conf/jboss-service.xml
     Change the following line:
         <attribute name="DefaultCacheTimeout">1800</attribute>
     To this:
         <attribute name="DefaultCacheTimeout">0</attribute>

5.  Update the definition of the topContextComparator bean in
  $JBOSS_HOME/server/default/conf/bootstrap/deployers.xml  to ensure that the core app is
  deployed before the integration app.

      <!-- use legacy ordering -->
       <bean name="topContextComparator">
         <constructor factoryClass="org.jboss.system.deployers.LegacyDeploymentContextComparator" factoryMethod="getInstance"/>
         <property name="suffixOrder">
           <map keyClass="java.lang.String" valueClass="java.lang.Integer">
             <entry>
               <key>tissuelocator.ear</key>
               <value>610</value>
             </entry>
             <entry>
               <key>tissuelocator-integrator.ear</key>
               <value>620</value>
             </entry>
           </map>
         </property>
       </bean>

6. (Optional) Set up jboss to send email.
    a.  Select the appropriate example configuration file.
        1.  If configuring jboss locally on a laptop, outside of our data centers, use mail-service.xml.localExample
        2.  If configuring jboss on a tier in one of our data centers, use mail-service.xml.tierExample
    b.  Update $JBOSS_HOME/server/default/deploy/mail-service.xml with usernames, passwords, and other options
        so that it lines up with the selected example.
    c.  To get the application to actually send email, build the app with mailEnabled set to true in settings.xml

7.  Download and install Postgres 8.4.x


Setting up CBM
==============
The Biolocator can export its data to a CBM service.  This feature is optional.  If you expect to
host a CBM instance, follow the folloing instructions to prepare the environment for CBM.


******** Note **********
While this feature is optional, if you plan to execute the tests for the integration application
against this environment, you must at least set up the CBM database.

1.  Install mysql 5.x

2.  Create the cbm database and version following this:
  https://svn.5amsolutions.com/opensource/5bl/trunk/cbm/docs/create%20user.txt

3.  Initialize the cbm database by executing this script as the user created in step 2:
  https://svn.5amsolutions.com/opensource/5bl/trunk/cbm/sql/CBMpopulated.sql

4.  Download and Install Java 5.

5,  Download and install JBoss 4.0.5.GA and configure it to run as a service using Java 5.

6.  Follow the instructions found in sections 4.4 and 4.5 in this document:
  https://svn.5amsolutions.com/opensource/5bl/branches/java6-take2/cbm/docs/CBM1.0Beta_Description_GridNodeDeploymentInstructions_042110.doc

7.  Modify JBoss 4.0.5.GA's port configuration as needed if ports conflict with the JBoss 5.1.0 installation.


Configuring Maven Build for the new environment
===============================================
The settings/ directory contains settings.xml files for different tiers.  These files should
serve as good examples for what sort of items must be configured in order to build and deploy
to the newly created environment.  However, any property in the build can be defined in the
settings.xml file if needed.  At a minimum, you will need to provide the location of JBoss on
the server, the database connection information, and the server url.