/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;

import com.fiveamsolutions.tissuelocator.test.TestProperties;
import com.thoughtworks.selenium.SeleneseTestCase;

/**
 * @author smiller
 */
public abstract class AbstractSeleniumTest extends SeleneseTestCase {

    /**
     * The very first time setUp is called, change test to act like teardown was called.
     */
    private static boolean firstClean = true;
    private static final int MILLIS_PER_SECOND = 1000;
    private static final int PAGE_TIMEOUT_SECONDS = 180;
    private static final Logger LOG = Logger.getLogger(AbstractSeleniumTest.class);

    /**
     * set up.
     * {@inheritdoc}
     * @throws Exception on error.
     */
    @Override
    public void setUp() throws Exception {
        System.setProperty("selenium.port", "" + TestProperties.getSeleniumServerPort());
        final String hostname = TestProperties.getServerHostname();
        final int port = TestProperties.getServerPort();
        final String browser = TestProperties.getSeleniumBrowser();
        if (port == 0) {
            super.setUp("http://" + hostname, browser);
        } else {
            super.setUp("http://" + hostname + ":" + port, browser);

        }
        selenium.setTimeout(toMillisecondsString(PAGE_TIMEOUT_SECONDS));

        if (firstClean) {
            runSqlScript("cleanTables.sql");
            firstClean = false;
        }

        if (StringUtils.isNotEmpty(getScriptName())) {
            runSqlScript(getScriptName());
        }
    }

    /**
     * verify if an element is present by evaluating the xpath using javascript.
     * @param xpathExpression xpath to the element.
     * @return whether the element is present
     */
    protected boolean isElementPresentByJSXPath(final String xpathExpression) {
        final String cond = "var doc = selenium.browserbot.getCurrentWindow().document; " + "doc.evaluate("
                + toJSString(xpathExpression) + ", doc, null, XPathResult.BOOLEAN_TYPE, null).booleanValue;";
        LOG.trace(cond);
        return Boolean.valueOf(selenium.getEval(cond)).booleanValue();
    }

    /**
     * converts seconds to milliseconds.
     * @param seconds num of seconds.
     * @return num of millis
     */
    protected static String toMillisecondsString(final long seconds) {
        return String.valueOf(seconds * MILLIS_PER_SECOND);
    }

    /**
     * wait for the page to load.
     */
    protected void waitForPageToLoad() {
        selenium.waitForPageToLoad(toMillisecondsString(PAGE_TIMEOUT_SECONDS));
    }

    /**
     * wait for an element to appear.
     * @param id the id of the element.
     * @param timeoutSeconds timeout before giving up
     */
    protected void waitForElementById(final String id, final int timeoutSeconds) {
        selenium.waitForCondition("selenium.browserbot.getCurrentWindow().document.getElementById('" + id + "');",
                toMillisecondsString(timeoutSeconds));
    }

    /**
     * wait for an element to appear.
     * @param id the id of the element.
     */
    protected void waitForElementById(final String id) {
        waitForElementById(id, PAGE_TIMEOUT_SECONDS);
    }

    /**
     * wait for an element to appear.
     * @param xpathExpression xpath to the element.
     * @param timeoutSeconds timeout.
     */
    protected void waitForElementByXPath(final String xpathExpression, final int timeoutSeconds) {
        final String cond = "var doc = selenium.browserbot.getCurrentWindow().document; " + "doc.evaluate("
                + toJSString(xpathExpression) + ", doc, null, XPathResult.BOOLEAN_TYPE, null).booleanValue;";
        LOG.trace(cond);
        selenium.waitForCondition(cond, toMillisecondsString(timeoutSeconds));
    }

    /**
     * convert to JS string.
     * @param str the string
     * @return the js string
     */
    protected static String toJSString(final String str) {
        if (str == null) {
            return "null";
        }
        final StringBuffer sb = new StringBuffer().append('\'');
        final char[] cs = str.toCharArray();
        for (final char c : cs) {
            if (c == '\'' || c == '\\') {
                sb.append('\\');
            }
            sb.append(c);
        }

        return sb.append('\'').toString();
    }

    /**
     * click and wait for a page to load.
     * @param linkOrButton the link or button.
     */
    protected void clickAndWait(final String linkOrButton) {
        selenium.click(linkOrButton);
        waitForPageToLoad();
    }


    /**
     * Click on a link in a table and wait for the page to load.
     * @param tableId The ID of the table to traverse
     * @param rowNum the row number the link is in, index starts at 1
     * @param colNum the column number the link is in, index starts at 1
     */
    protected void clickTableCellLink(final String tableId, final int rowNum, final int colNum) {
        clickAndWait("xpath=//table[@id='" + tableId + "']/tbody/tr[" + Integer.toString(rowNum)
                + "]/td[" + Integer.toString(colNum) + "]/a");
    }

    /**
     * click and wait for the popup to appear.
     * @param linkOrButton the link or button to click
     * @param windowId window id of the popup
     * @param xpath xpath to an element
     */
    protected void clickAndWaitForPopup(final String linkOrButton, final String windowId, final String xpath) {
        selenium.click(linkOrButton);
        selenium.openWindow("", windowId);
        selenium.selectWindow(windowId);
        selenium.windowFocus();
        waitForElementByXPath(xpath, PAGE_TIMEOUT_SECONDS);
    }

    /**
     * run a sql script into the database.
     * @param scriptName the name of the script to run
     * @throws Exception on error
     */
    protected void runSqlScript(final String scriptName) throws Exception {
        final String driver = TestProperties.getProperties().getProperty("jdbc.driver-class");
        final String url = TestProperties.getProperties().getProperty("jdbc.connection-url");
        final String username = TestProperties.getProperties().getProperty("jdbc.user-name");
        final String password = TestProperties.getProperties().getProperty("jdbc.password");
        Class.forName(driver).newInstance();
        final Connection conn = DriverManager.getConnection(url, username, password);
        final Statement s = conn.createStatement();

        final InputStream stream = ClassLoader.getSystemClassLoader().getResourceAsStream(scriptName);
        final BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        String statement = br.readLine();
        while (null != statement) {
            if (!StringUtils.isEmpty(statement)) {
                try {
                    if (statement.startsWith("select")) {
                        s.executeQuery(statement);
                    } else {
                        s.executeUpdate(statement);
                    }
                } catch (Exception e) {
                    LOG.error(statement, e);
                    Assert.fail(e.toString() + ":" + statement);
                }
            }
            statement = br.readLine();
        }
        conn.close();
    }

    /**
     * @return the name of the sql script to run to seed the db with data.
     */
    protected String getScriptName() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void tearDown() throws Exception {
        super.tearDown();
        runSqlScript("cleanTables.sql");
    }

}
