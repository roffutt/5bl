/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import com.fiveamsolutions.tissuelocator.test.selenium.AbstractReportTest;

/**
 * @author ddasgupta
 *
 */
public class AbrcRequestReviewReportTest extends AbstractReportTest {

    private static final String TITLE_XPATH =
        "xpath=//div[@id='requestform']/div[%d]/div/h3";
    private static final String SUBTITLE_XPATH =
        "xpath=//div[@id='requestform']/div[%d]/div/table/tbody/tr[1]/th/div";
    private static final String METRIC_XPATH =
        "xpath=//div[@id='requestform']/div[%d]/div/table/tbody/tr[%d]/td[%d]";
    private static final String REJECTION_COMMENTS_XPATH = 
        "xpath=//div[@id='requestform']/div[%d]/div/table/tbody/tr[%d]/td[%d]/a";
    private static final String REQUEST_LINK_XPATH = "xpath=//body/p[%d]/strong/a";
    private static final String COMMENT_XPATH = "xpath=//body/p[%d]/em";

    /**
     * test the request review report page.
     */
    public void testUsageReport() {
        goToReportPage("Request Review Report");
        changeDates("2010");
        verifyData();
        changeDates("2009");
        verifyNoData();
    }

    private void verifyData() {
        //CHECKSTYLE:OFF - magic numbers
        assertEquals("2", selenium.getText(String.format(METRIC_XPATH, 1, 2, 2)));
        assertEquals("3", selenium.getText(String.format(METRIC_XPATH, 1, 3, 2)));
        assertEquals("1 (reasons)", selenium.getText(String.format(METRIC_XPATH, 1, 4, 2)));
        assertEquals("4", selenium.getText(String.format(METRIC_XPATH, 1, 5, 2)));
        assertEquals("6", selenium.getText(String.format(METRIC_XPATH, 1, 6, 2)));
        assertEquals("6", selenium.getText(String.format(METRIC_XPATH, 1, 7, 2)));
        verifyRejectionComments(String.format(REJECTION_COMMENTS_XPATH, 1, 4, 2), 1);

        assertEquals("3", selenium.getText(String.format(METRIC_XPATH, 2, 2, 2)));
        assertEquals("1", selenium.getText(String.format(METRIC_XPATH, 2, 3, 2)));
        assertEquals("2 (reasons)", selenium.getText(String.format(METRIC_XPATH, 2, 4, 2)));
        assertEquals("6", selenium.getText(String.format(METRIC_XPATH, 2, 5, 2)));
        assertEquals("6", selenium.getText(String.format(METRIC_XPATH, 2, 6, 2)));
        assertEquals("6", selenium.getText(String.format(METRIC_XPATH, 2, 7, 2)));
        verifyRejectionComments(String.format(REJECTION_COMMENTS_XPATH, 2, 4, 2), 2);

        assertEquals("1", selenium.getText(String.format(METRIC_XPATH, 3, 2, 2)));
        assertEquals("2", selenium.getText(String.format(METRIC_XPATH, 3, 3, 2)));
        assertEquals("3 (reasons)", selenium.getText(String.format(METRIC_XPATH, 3, 4, 2)));
        assertEquals("5", selenium.getText(String.format(METRIC_XPATH, 3, 5, 2)));
        assertEquals("6", selenium.getText(String.format(METRIC_XPATH, 3, 6, 2)));
        assertEquals("6", selenium.getText(String.format(METRIC_XPATH, 3, 7, 2)));
        verifyRejectionComments(String.format(REJECTION_COMMENTS_XPATH, 3, 4, 2), 3);
        //CHECKSTYLE:ON
    }
    
    private void verifyRejectionComments(String link, int count) {
        clickAndWait(link);
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent("Reasons for Rejection"));
        for (int i = 1; i <= count; i++) {
            assertTrue(selenium.getText(String.format(REQUEST_LINK_XPATH, i)).contains("Request"));
            assertEquals("Denied", selenium.getText(String.format(COMMENT_XPATH, i)));
        }
        selenium.selectWindow(null);
        clickAndWait("xpath=//div[@id='popupControls']/a");
    }

    private void verifyNoData() {
        //CHECKSTYLE:OFF - magic numbers
        String[] consortiumMembers = {"Maricopa Integrated Health System", "Phoenix Children's Hospital", "St. Joseph's Hospital and Medical Center"};
        for (int i = 0; i < consortiumMembers.length; i++) {
            assertEquals("Scientific Reviews: " + consortiumMembers[i],
                    selenium.getText(String.format(TITLE_XPATH, i + 1)));
            assertEquals("Reviews", selenium.getText(String.format(SUBTITLE_XPATH, i + 1)));
            assertEquals("Approved", selenium.getText(String.format(METRIC_XPATH, i + 1, 2, 1)));
            assertEquals("0", selenium.getText(String.format(METRIC_XPATH, i + 1, 2, 2)));
            assertEquals("Revisions needed", selenium.getText(String.format(METRIC_XPATH, i + 1, 3, 1)));
            assertEquals("0", selenium.getText(String.format(METRIC_XPATH, i + 1, 3, 2)));
            assertEquals("Rejected", selenium.getText(String.format(METRIC_XPATH, i + 1, 4, 1)));
            assertEquals("0", selenium.getText(String.format(METRIC_XPATH, i + 1, 4, 2)));
            assertEquals("Institutional veto", selenium.getText(String.format(METRIC_XPATH, i + 1, 5, 1)));
            assertEquals("0", selenium.getText(String.format(METRIC_XPATH, i + 1, 5, 2)));
            assertEquals("Scientific review longer than 15 days",
                    selenium.getText(String.format(METRIC_XPATH, i + 1, 6, 1)));
            assertEquals("0", selenium.getText(String.format(METRIC_XPATH, i + 1, 6, 2)));
            assertEquals("Institutional review longer than 15 days",
                    selenium.getText(String.format(METRIC_XPATH, i + 1, 7, 1)));
            assertEquals("0", selenium.getText(String.format(METRIC_XPATH, i + 1, 7, 2)));
        }
        //CHECKSTYLE:ON
    }
}
