/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import java.util.Arrays;

import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author ddasgupta
 *
 */
public class AbrcSpecimensByConditionReportTest extends AbstractTissueLocatorSeleniumTest {

    private static final String DATA_XPATH =
        "xpath=//div[@id='requestform']/div[@id='reportcontent']/table/tbody/tr[%d]/td[%d]/span";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "UsageReportTest.sql";
    }

    /**
     * test the specimens by condition report page.
     */
    public void testSpecimensByConditionReport() {
        goToReportPage();
        verifyData();
    }

    private void goToReportPage() {
        loginAsAdmin();
        mouseOverAndPause("link=Reports");
        clickAndWait("link=Specimens By Condition Report");
        assertTrue(selenium.isTextPresent("Specimens By Condition Report"));
    }

    private void verifyData() {
        //CHECKSTYLE:OFF - magic numbers
        String[] conditions = new String[] {"\"Dermoid\" tumor (morphologic abnormality)",
                "16q partial trisomy syndrome (disorder)", "2p partial trisomy syndrome (disorder)"};
        assertEquals("Institution: Maricopa Integrated Health System", selenium.getText(String.format(DATA_XPATH, 5, 2)));
        for (int i = 6; i <= 8; i ++) {
            assertTrue(Arrays.asList(conditions).contains(selenium.getText(String.format(DATA_XPATH, i, 2))));
            assertEquals("1", selenium.getText(String.format(DATA_XPATH, i, 3)));
        }
        assertEquals("Institution Subtotal:", selenium.getText(String.format(DATA_XPATH, 10, 2)));
        assertEquals("3", selenium.getText(String.format(DATA_XPATH, 10, 3)));

        assertEquals("Institution: Phoenix Children's Hospital", selenium.getText(String.format(DATA_XPATH, 15, 2)));
        for (int i = 16; i <= 18; i ++) {
            String conditionName = selenium.getText(String.format(DATA_XPATH, i, 2));
            assertTrue(Arrays.asList(conditions).contains(conditionName));
            assertEquals(conditionName.contains("Dermoid") ? "3" : "2", selenium.getText(String.format(DATA_XPATH, i, 3)));
        }
        assertEquals("Institution Subtotal:", selenium.getText(String.format(DATA_XPATH, 20, 2)));
        assertEquals("7", selenium.getText(String.format(DATA_XPATH, 20, 3)));

        assertEquals("Total:", selenium.getText(String.format(DATA_XPATH, 26, 2)));
        assertEquals("10", selenium.getText(String.format(DATA_XPATH, 26, 3)));

        String imgXpath = "xpath=//div[@id='requestform']/div[@id='reportcontent']/table/tbody/tr[27]/td[2]/img";
        assertTrue(selenium.isElementPresent(imgXpath));
        //CHECKSTYLE:ON
    }
}
