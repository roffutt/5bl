/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractExtendedFundingStatusTest;

/**
 * @author ddasgupta
 *
 */
public class AbrcSpecimenRequestAndReviewTest extends AbstractExtendedFundingStatusTest {
    private static final int CONSORTIUM_SIZE = 5;
    private static final int LINK_COL = 6;
    /**
     * test the request submission and review workflow.
     * @throws Exception on error
     */
    @Test
    public void testSpecimenRequestAndReview() throws Exception {
        String orderCount = getOrderCount();
        // login and make first request
        pendingRequestMessage(0);
        addToCart();
        cartValidation();
        studyDetails();
        confirm(ClientProperties.isMtaActive() ? null : new String[]{"Draft"});
        save();
        pendingRequestMessage(1);

        // test validation and make second request
        validation();
        copyFromUser("investigator");
        copyFromUser("recipient");
        if (ClientProperties.isDisplayShipmentBillingRecipient()) {
            selenium.click("includeBillingRecipient");
            copyFromUser("billingRecipient");
        }
        navigation();
        pendingRequestMessage(2);

        unassignedReviewer();
        assignReviewer();
        consortiumReviewerMessages();

        // sign in as scientific reviewer verify functionality
        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        checkReviewFunctionalityAvailable(true, true, false, true);
        verifyViewRequestPageFromReview();
        verifyGuidelineLinks();
        performScientificReview();
        clickAndWait("link=Sign Out");

        // sign in as inst admin and verify functionality
        login("test-user-inst@example.com", "tissueLocator1");
        checkReviewFunctionalityAvailable(true, false, true, true);
        performInstitutionalReview();
        clickAndWait("link=Sign Out");

        // sign in as consortium admin and verify admin functionality
        loginAsAdmin();
        checkReviewFunctionalityAvailable(true, false, false, true);
        checkReadOnlyVotes();
        clickAndWait("link=Sign Out");

        // sign in as super admin and verify functionality
        login("superadmin@example.com", "tissueLocator1");
        checkReviewFunctionalityAvailable(true, false, false, true);
        checkReadOnlyVotes();
        clickAndWait("link=Sign Out");

        verifySpecimenUnavailable();
        pendingRevisionMessage();
        clickAndWait("link=Sign Out");
        loginAsUser1();
        clickAndWait("link=My Requests");
        selenium.select("status", "Revision Requested");
        waitForPageToLoad();
        pause(LONG_DELAY);
        viewRequest(false, true, false, new String[]{"Draft", "Pending"});
        editRequest(LINK_COL);
        viewRequest(true, false, false, new String[]{"Draft", "Pending", "Pending"});
        reviewRevisedRequest();
        addComment();

        assignPendingFinalDecisionRequest();
        finalComment();
        verifyRequiredSpecimenValidation();
        verifyOrderCount(orderCount);

        verifyDraftValidation();
        verifyDeleteDraft();
        verifyEditErrorRequest();
        verifyDraftFlow();
        if (ClientProperties.isMtaActive()) {
            verifyMtaStatus();
        }
    }

    private void pendingRequestMessage(int requestCount) {
        login("test-user-inst@example.com", "tissueLocator1");
        assertFalse(selenium.isTextPresent("Consortium Review"));
        assertFalse(selenium.isTextPresent("Lead Review"));
        if (requestCount == 0) {
            assertFalse(selenium.isTextPresent("Reviewer Assignment"));
            assertFalse(selenium.isTextPresent("Institutional Review"));
        } else if (requestCount == 1) {
            assertTrue(selenium.isTextPresent("1 Reviewer Assignment"));
            clickAndWait("link=1 Reviewer Assignment");
            assertTrue(selenium.isTextPresent("Requests"));
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertTrue(selenium.isChecked("actionNeededOnly"));
            clickAndWait("link=Home");
            assertTrue(selenium.isTextPresent("1 Institutional Review"));
            clickAndWait("link=1 Institutional Review");
            assertTrue(selenium.isTextPresent("Requests"));
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            assertTrue(selenium.isChecked("actionNeededOnly"));
        } else {
            assertTrue(selenium.isTextPresent(requestCount + " Reviewer Assignments"));
            clickAndWait("link=" + requestCount + " Reviewer Assignments");
            assertTrue(selenium.isTextPresent("Requests"));
            assertTrue(selenium.isTextPresent("1-" + requestCount + " of " + requestCount + " Results"));
            assertTrue(selenium.isChecked("actionNeededOnly"));
            clickAndWait("link=Home");
            assertTrue(selenium.isTextPresent(requestCount + " Institutional Reviews"));
            clickAndWait("link=" + requestCount + " Institutional Reviews");
            assertTrue(selenium.isTextPresent("Requests"));
            assertTrue(selenium.isTextPresent("1-" + requestCount + " of " + requestCount + " Results"));
            assertTrue(selenium.isChecked("actionNeededOnly"));
        }
        clickAndWait("link=Sign Out");
    }

    private void verifyToDoList(boolean assignReviewer, boolean leadReview,
            boolean consortiumReview, boolean institutionalReview, boolean finalComment) {
        assertEquals(assignReviewer || leadReview || consortiumReview || institutionalReview || finalComment,
                selenium.isTextPresent("You must take the following action(s) on this request:"));
        assertEquals(assignReviewer, selenium.isTextPresent("Assign a Scientific Reviewer"));
        assertEquals(leadReview,
                selenium.isTextPresent("Submit your Lead Review comments in the Internal Comments section"));
        assertEquals(consortiumReview, selenium.isTextPresent("Submit your Scientific Review Vote"));
        assertEquals(institutionalReview, selenium.isTextPresent("Submit your Institutional Approval"));
        assertEquals(finalComment, selenium.isTextPresent("Submit your Final Comment"));
        for (int i = 0; i < CONSORTIUM_SIZE; i++) {
            String xpath = "xpath=//table[@id='consortiumReviewTable']/tbody/tr[" + (i + 2) + "]/td[1]/img";
            assertEquals(i == 0, selenium.isElementPresent(xpath));
        }
    }

    private void unassignedReviewer() {
        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        assertFalse(selenium.isTextPresent("Consortium Review"));
        assertFalse(selenium.isTextPresent("Lead Review"));
        assertFalse(selenium.isTextPresent("Institutional Review"));
        assertFalse(selenium.isTextPresent("Reviewer Assignments"));
        checkReviewFunctionalityAvailable(true, false, false, true);
        assertTrue(selenium.isTextPresent("Scientific Review Voting"));
        assertFalse(selenium.isElementPresent("consortiumReviewUser"));
        assertFalse(selenium.isElementPresent("consortiumReviewVote"));
        assertFalse(selenium.isElementPresent("consortiumReviewComment"));
        assertFalse(selenium.isElementPresent("consortiumReviewForm_btn_save"));
        verifyToDoList(false, false, false, false, false);
        clickAndWait("link=Sign Out");
    }

    private void assignReviewer() {
        login("test-user-inst@example.com", "tissueLocator1");
        checkReviewFunctionalityAvailable(true, false, true, true);
        verifyToDoList(true, false, false, true, false);
        clickAndWait("consortiumReviewForm_btn_save");
        assertTrue(selenium.isTextPresent("Scientific Review Voting"));
        assertTrue(selenium.isTextPresent("User is required."));
        assertTrue(selenium.isElementPresent("consortiumReviewUser"));
        String[] userOptions = selenium.getSelectOptions("consortiumReviewUser");
        assertTrue(userOptions.length >= 2);
        assertEquals("- Select -", userOptions[0]);
        assertEquals(ClientProperties.getScientificReviewerName(), userOptions[1]);
        assertFalse(selenium.isElementPresent("consortiumReviewVote"));
        assertFalse(selenium.isElementPresent("consortiumReviewComment"));
        selenium.select("consortiumReviewUser", "label=" + ClientProperties.getScientificReviewerName());
        clickAndWait("consortiumReviewForm_btn_save");
        assertTrue(selenium.isTextPresent("Scientific Review Voting"));
        assertTrue(selenium.isTextPresent("Scientific reviewer successfully assigned."));
        verifyToDoList(false, false, false, true, false);
        clickAndWait("link=Sign Out");
    }

    private void consortiumReviewerMessages() {
        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        assertFalse(selenium.isTextPresent("Institutional Review"));
        assertFalse(selenium.isTextPresent("Reviewer Assignments"));
        assertTrue(selenium.isTextPresent("1 Lead Review"));
        clickAndWait("link=1 Lead Review");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isChecked("actionNeededOnly"));
        clickAndWait("link=Home");
        assertTrue(selenium.isTextPresent("1 Consortium Review"));
        clickAndWait("link=1 Consortium Review");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isChecked("actionNeededOnly"));
        clickAndWait("link=Sign Out");
    }

    private void verifyViewRequestPageFromReview() {
        clickAndWait("link=View Request Details");
        collapseMainSections();
        verifyCart(false);
        confirmText(false, false, false, "/protected/downloadFile", new String[]{"Draft", "Pending"});
        clickAndWait("link=Go Back To Request Review");
    }

    private void performScientificReview() {
        verifyToDoList(false, true, true, false, false);
        assertFalse(selenium.isTextPresent("Previous Review Decision of the Consortium"));
        assertFalse(selenium.isElementPresent("consortiumReviewUser"));
        clickAndWait("consortiumReviewForm_btn_save");
        assertTrue(selenium.isTextPresent("Vote is required"));

        selenium.select("consortiumReviewVote", "label=Deny");
        clickAndWait("consortiumReviewForm_btn_save");
        assertTrue(selenium.isTextPresent("If you do not approve the request a comment is required."));

        selenium.type("consortiumReviewComment", "this is my denial explanation");
        clickAndWait("consortiumReviewForm_btn_save");
        assertTrue(selenium.isTextPresent("Scientific Review Voting"));
        assertTrue(selenium.isTextPresent("Scientific review successfully saved."));
        verifyToDoList(false, true, false, false, false);
    }

    private void performInstitutionalReview() {
        verifyToDoList(false, false, false, true, false);
        clickAndWait("instReviewForm_btn_save");
        assertTrue(selenium.isTextPresent("Vote is required"));

        selenium.select("institutionalReviewVote", "label=Deny");
        clickAndWait("instReviewForm_btn_save");
        assertTrue(selenium.isTextPresent("If you do not approve the request a comment is required."));

        selenium.select("institutionalReviewVote", "label=Approve");
        selenium.type("institutionalReviewComment", "this is my approval explanation");
        clickAndWait("instReviewForm_btn_save");
        assertTrue(selenium.isTextPresent("Scientific Review Voting"));
        assertTrue(selenium.isTextPresent("Your vote has been saved."));
        verifyToDoList(false, false, false, false, false);
    }

    private void checkReadOnlyVotes() {
        assertEquals("Deny", selenium.getTable("consortiumReviewTable.1.4"));
        assertEquals("this is my denial explanation", selenium.getTable("consortiumReviewTable.1.5"));

        assertEquals("Approve", selenium.getTable("instReviewTable.1.4"));
        assertEquals("this is my approval explanation", selenium.getTable("instReviewTable.1.5"));
    }

    private void checkReviewFunctionalityAvailable(boolean canGetToRequest,
            boolean isScientificReviewer, boolean isInstitutionalReviewer, boolean isCommentor) {
        if (canGetToRequest) {
            String linkText = "View";
            if (isScientificReviewer || isInstitutionalReviewer) {
                linkText = "Review";
            }

            goToRequestAdminPage();
            assertTrue(selenium.isTextPresent("1-2 of 2 Results"));

            clickAndWait("link=" + linkText);
            assertTrue(selenium.isTextPresent("Scientific Review Voting"));

            // we need to make sure that the final vote date shows up, but we don't know what it is, so some creative
            // string matching is in order.
            assertFalse(selenium.isTextPresent("The voting period for this request ends on ."));
            assertTrue(selenium.isTextPresent("The voting period for this request ends on"));

            clickAndWait("link=Back To List");
            assertTrue(selenium.isTextPresent("1-2 of 2 Results"));

            clickAndWait("link=" + linkText);
            assertTrue(selenium.isTextPresent("Internal Comments"));
            assertEquals(isCommentor, selenium.isElementPresent("btn_addComment"));

            assertEquals(isScientificReviewer || isInstitutionalReviewer,
                    selenium.isElementPresent("consortiumReviewForm_btn_save"));
            assertEquals(isInstitutionalReviewer, selenium.isElementPresent("instReviewForm_btn_save"));
            assertTrue(selenium.isElementPresent("specimenPopupLink"));

            clickAndWait("specimenPopupLink");
            selenium.selectFrame("popupFrame");
            assertTrue(selenium.isTextPresent("Biospecimens in Request"));
            selenium.selectWindow(null);
            clickAndWait("xpath=//div[@id='popupControls']/a");
        } else {
            mouseOverAndPause("link=Administration");
            assertFalse(selenium.isTextPresent("Request Administration"));
        }
    }

    private void reviewRevisedRequest() {
        clickAndWait("link=Sign Out");
        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        goToRequestAdminPage();
        clickAndWait("link=Review");
        assertEquals("Deny", selenium.getSelectedLabel("consortiumReviewVote"));
        assertEquals("this is my denial explanation", selenium.getValue("consortiumReviewComment"));
        verifyReviewExternalCommentDisplay();
    }

    private void goToRequestAdminPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Request Administration");
    }

    private void addComment() {
        verifyToDoList(false, true, false, false, false);
        clickAndWait("btn_addComment");
        selenium.selectFrame("popupFrame");
        selenium.type("comment", "This is an internal comment.");
        clickAndWait("btn_saveComment");
        waitForElementById("btn_addComment");
        assertTrue(selenium.isTextPresent("Scientific Review Voting"));
        assertTrue(selenium.isTextPresent("This is an internal comment."));
        assertTrue(selenium.isTextPresent("Internal comment successfully saved."));
        verifyToDoList(false, false, false, false, false);
        clickAndWait("link=Sign Out");
    }

    private void assignPendingFinalDecisionRequest() {
        if (StringUtils.isNotBlank(ClientProperties.getAlternateInstitutionalAdminEmail())) {
            login(ClientProperties.getAlternateInstitutionalAdminEmail(), "tissueLocator1");
            assertTrue(selenium.isTextPresent("2 Reviewer Assignments"));
            goToRequestAdminPage();
            assertEquals("Assign Reviewer", selenium.getTable("specimenRequest.1.6"));
            assertEquals("Assign Reviewer", selenium.getTable("specimenRequest.2.6"));
            assertTrue(StringUtils.isBlank(selenium.getTable("specimenRequest.3.6")));
            selenium.select("status", "label=Pending Final Decision");
            waitForPageToLoad();
            clickAndWait("link=View");
            verifyToDoList(false, false, false, false, false);
            assertFalse(selenium.isElementPresent("consortiumReviewUser"));
            verifyReviewReadOnly();
            clickAndWait("link=Sign Out");
        }
        login(ClientProperties.getInstitutionalAdminEmail(), "tissueLocator1");
        assertTrue(selenium.isTextPresent("2 Reviewer Assignments"));
        goToRequestAdminPage();
        assertTrue(StringUtils.isBlank(selenium.getTable("specimenRequest.1.6")));
        assertTrue(selenium.getTable("specimenRequest.2.6").contains("Assign Reviewer"));
        assertTrue(selenium.getTable("specimenRequest.2.6").contains("Institutional Review"));
        assertEquals("Assign Reviewer", selenium.getTable("specimenRequest.3.6"));
        selenium.select("status", "label=Pending Final Decision");
        waitForPageToLoad();
        clickAndWait("link=Review");
        verifyToDoList(true, false, false, false, false);
        assertTrue(selenium.isElementPresent("consortiumReviewUser"));
        verifyReviewReadOnly();
        selenium.select("consortiumReviewUser", "label=" + ClientProperties.getScientificReviewerName());
        clickAndWait("consortiumReviewForm_btn_save");
        assertTrue(selenium.isTextPresent("Scientific Review Voting"));
        assertTrue(selenium.isTextPresent("Scientific reviewer successfully assigned."));
        verifyToDoList(false, false, false, false, false);
        assertTrue(selenium.isElementPresent("consortiumReviewUser"));
        verifyReviewReadOnly();
        clickAndWait("link=Sign Out");
    }

    private void verifyReviewReadOnly() {
        assertFalse(selenium.isElementPresent("consortiumReviewVote"));
        assertFalse(selenium.isElementPresent("consortiumReviewComment"));
        assertFalse(selenium.isElementPresent("institutionalReviewVote"));
        assertFalse(selenium.isElementPresent("institutionalReviewComment"));
        assertFalse(selenium.isElementPresent("btn_addComment"));
    }

    private void finalComment() throws Exception {
        login(ClientProperties.getScientificReviewerEmail(), "tissueLocator1");
        assertTrue(selenium.isElementPresent("link=1 Final Comment"));
        clickAndWait("link=1 Final Comment");
        assertEquals("Pending Final Decision", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isChecked("actionNeededOnly"));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("Final Comment"));
        String requestId = selenium.getTable("specimenRequest.1.0");
        clickAndWait("link=Review");
        verifyToDoList(false, false, false, false, true);
        assertFalse(selenium.isElementPresent("consortiumReviewUser"));
        verifyReviewReadOnly();
        assertFalse(selenium.isElementPresent("btn_addComment"));
        assertTrue(selenium.isElementPresent("externalComment"));
        assertTrue(selenium.isElementPresent("btn_save"));
        assertTrue(selenium.isTextPresent("Consortium's Review Decision"));
        assertTrue(selenium.isTextPresent(ClientProperties.getFinalVoteLabel() + ": Approved"));
        assertTrue(selenium.isTextPresent("Lead Reviewer"));
        assertTrue(selenium.isTextPresent("Please provide a summary of the voting comments"));
        assertTrue(selenium.isTextPresent(ClientProperties.getFinalCommentLabel()));
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("A Lead Reviewer Comment must be set."));
        assertTrue(selenium.isElementPresent("externalComment"));
        assertTrue(selenium.isElementPresent("btn_save"));
        assertTrue(selenium.isTextPresent("Consortium's Review Decision"));
        selenium.type("externalComment", "final comment");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Lead Reviewer Comments successfully sent."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("0 Results found."));
        assertTrue(selenium.isTextPresent("Nothing found to display."));
        selenium.select("status", "label=Approved");
        waitForPageToLoad();
        assertEquals("Approved", selenium.getSelectedLabel("status"));
        selenium.click("actionNeededOnly");
        waitForPageToLoad();
        assertFalse(selenium.isChecked("actionNeededOnly"));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertEquals(requestId, selenium.getTable("specimenRequest.1.0"));
        clickAndWait("link=View");
        assertFalse(selenium.isElementPresent("consortiumReviewUser"));
        verifyReviewReadOnly();
        assertTrue(selenium.isTextPresent("Consortium's Review Decision"));
        assertTrue(selenium.isTextPresent(ClientProperties.getFinalVoteLabel() + ": Approved"));
        assertTrue(selenium.isTextPresent(ClientProperties.getFinalCommentLabel()));
        assertTrue(selenium.isTextPresent("final comment"));
        assertFalse(selenium.isTextPresent("Previous Review Decision of the Consortium"));
        clickAndWait("link=Sign Out");
        login(ClientProperties.getInstitutionalAdminEmail(), "tissueLocator1");
        goToRequestAdminPage();
        selenium.select("status", "label=Approved");
        waitForPageToLoad();
        clickAndWait("link=View");
        assertFalse(selenium.isElementPresent("consortiumReviewUser"));
        verifyReviewReadOnly();
    }

    private void verifyGuidelineLinks() {
        assertTrue(selenium.isElementPresent("consortiumReviewGuidelines"));
        String linkTarget = selenium.getAttribute("consortiumReviewGuidelines@href");
        assertTrue(linkTarget.contains("documents/ABL_ScientificPeerReviewForm.pdf"));
        assertTrue(selenium.isElementPresent("institutionalReviewGuidelines"));
        linkTarget = selenium.getAttribute("institutionalReviewGuidelines@href");
        assertTrue(linkTarget.contains("documents/ABL_InstitutionalAdministratorReviewForm.pdf"));
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyNotFundedMessage() {
        String message = "Most institutions will not approve a request for biospecimens for research that is not "
            + "funded absent special exception.";
        assertTrue(selenium.isTextPresent(message));
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected String getNoIrbApprovalNote() {
        return "IRB letter of approval or exemption will be required before the biospecimens are provided";
    }
}
