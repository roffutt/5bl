/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.test.selenium.nfcr;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.selenium.AbstractSpecimenSearchTest;

/**
 * @author ddasgupta
 *
 */
public class NfcrSpecimenSearchTest extends AbstractSpecimenSearchTest {

    private List<String> specimenTableColumnHeaders;

    private static final String BIOSPECIMEN_CHARACTERISTICS_TAB_LOCATOR = "id=tab_1";
    private static final String PATIENT_DEMOGRAPHICS_TAB_LOCATOR = "id=tab_2";
    private static final String PATHOLOGY_TAB_LOCATOR = "id=tab_3";
    private static final String TREATMENT_TAB_LOCATOR = "id=tab_4";
    private static final String FOLLOW_UP_TAB_LOCATOR = "id=tab_5";
    private static final String TUMOR_MARKERS_TAB_LOCATOR = "id=tab_6";
    private static final String ETIOLOGY_TAB_LOCATOR = "id=tab_7";
    private static final String PATIENT_DEMOGRAPHICS_CATEGORY = "Patient Demographics";
    private static final String PATIENT_DEMOGRAPHICS_GASTRIC_CANCER_CATEGORY = "Patient Demographics (Gastric Cancer)";
    private static final String PATIENT_DEMOGRAPHICS_LUNG_CANCER_CATEGORY = "Patient Demographics (Lung Cancer)";
    private static final String PATIENT_DEMOGRAPHICS_LIVER_CANCER_CATEGORY = "Patient Demographics (Liver Cancer)";
    private static final String PATIENT_DEMOGRAPHICS_COLON_CANCER_CATEGORY = "Patient Demographics (Colon Cancer)";
    private static final String PATIENT_DEMOGRAPHICS_PANCREATIC_CANCER_CATEGORY =
        "Patient Demographics (Pancreatic Cancer)";
    private static final String PATIENT_DEMOGRAPHICS_GBM_CANCER_CATEGORY =
        "Patient Demographics (Glioblastoma Multiforme Cancer)";
    private static final String PATHOLOGY_CATEGORY = "Pathology";
    private static final String PATHOLOGY_GASTRIC_CANCER_CATEGORY = "Pathology (Gastric Cancer)";
    private static final String PATHOLOGY_LUNG_CANCER_CATEGORY = "Pathology (Lung Cancer)";
    private static final String PATHOLOGY_LIVER_CANCER_CATEGORY = "Pathology (Liver Cancer)";
    private static final String PATHOLOGY_COLON_CANCER_CATEGORY = "Pathology (Colon Cancer)";
    private static final String PATHOLOGY_PANCREATIC_CANCER_CATEGORY = "Pathology (Pancreatic Cancer)";
    private static final String PATHOLOGY_GBM_CANCER_CATEGORY = "Pathology (Glioblastoma Multiforme Cancer)";
    private static final String ETIOLOGY_LIVER_CANCER_CATEGORY = "Etiology (Liver Cancer)";
    private static final String TREATMENT_CATEGORY = "Treatment";
    private static final String TREATMENT_LIVER_CANCER_CATEGORY = "Treatment (Liver Cancer)";
    private static final String FOLLOW_UP_CATEGORY = "Follow Up";
    private static final String TUMOR_MARKERS_GASTRIC_CANCER_CATEGORY = "Tumor Markers (Gastric Cancer)";
    private static final String TUMOR_MARKERS_LUNG_CANCER_CATEGORY = "Tumor Markers (Lung Cancer)";
    private static final String TUMOR_MARKERS_LIVER_CANCER_CATEGORY = "Tumor Markers (Liver Cancer)";
    private static final String TUMOR_MARKERS_COLON_CANCER_CATEGORY = "Tumor Markers (Colon Cancer)";
    private static final String TUMOR_MARKERS_PANCREATIC_CANCER_CATEGORY = "Tumor Markers (Pancreatic Cancer)";
    private static final String TUMOR_MARKERS_GBM_CANCER_CATEGORY = "Tumor Markers (Glioblastoma Multiforme Cancer)";
    private static final String EMPTY_STRING = "";
    private static final String ALL_STRING = "- All -";

    /**
     * Tests searching based upon dynamic extension-based fields.
     */
    @Test
    //CHECKSTYLE:OFF
    public void testSearchOnDynamicExtensionFields() {
        loginAsAdmin();
        goToSearchPage(false);
        verifyDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "dateOfDiagnosis", "January 1, 2000", 128);
        verifyDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "survivalInMonths", "2", 129);
        verifyDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "diseaseFreeSurvivalInMonths", "2", 130);
        verifyDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "tumorSizeCm", "2.2", 131);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "smokingStatus", "Ex-Smoker", 132);
        verifyDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "dateOfSurgery", "January 1, 2001", 5);
        verifyDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "ageAtDiagnosisInYears", "30", 6);
        verifyDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "weightInKg", "55", 7);
        verifyDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "heightInCm", "177", 8);
        verifyDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "packYearsOfSmoking", "3.3", 9);
        verifyDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "yearsSinceSmokingCessation", "3.3", 10);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "alcoholUseStatus", "Ex-Drinker", 11);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "relativeAlcoholUse", "Moderate", 12);
        verifyDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "yearsSinceAlcoholCessation", "2.2", 13);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "familyHistoryOfCancer", "Mother, Father, Brother, and Sister", 14);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "familyHistoryOfGastricCancer", "Mother, Father, Brother, and Sister", 15);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "familyHistoryOfLungCancer", "Mother, Father, Brother, and Sister", 16);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "familyHistoryOfLiverCancer", "Mother, Father, Brother, and Sister", 17);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "familyHistoryOfColonCancer", "Mother, Father, Brother, and Sister", 18);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "familyHistoryOfPancreaticCancer", "Mother, Father, Brother, and Sister", 19);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "familyHistoryOfGbmCancer", "Mother, Father, Brother, and Sister", 20);
        verifySelectDynamicExtensionField(PATIENT_DEMOGRAPHICS_TAB_LOCATOR, "occupation", "Farming", 21);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "childsGrade", "B", 22);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "edmondsonsGrade", "Moderately Differentiated", 23);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "tnmStageT", "3", 24);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "tnmStageN", "3", 25);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "tnmStageM", "1", 26);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "ecogPerformanceStatus", "5", 27);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "hPyloriStatus", "Positive", 28);
        verifyDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "hPyloriStrain", "foobar", 29);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "bclccStage", "C", 30);
        verifyDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "percentageOfTumorCells", "55.5", 31);
        verifyDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "percentageOfNecrosis", "44.4", 32);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "etiology", "HBV+HCV", 33);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "colonCancerHistology", "Mucinous Carcinoma", 34);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "gastricCancerHistology", "Lauren Intestinal", 35);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "nonTumerousLiverHistology", "Chronic Hepatitis", 36);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "veinousInfiltration", "Present", 37);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "portalVeinThrombosis", "Yes", 38);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "numberOfNodules", "Multiple", 39);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "hcvabStatus", "Positive", 40);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "hbsAgStatus", "Positive", 41);
        verifyDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "alanineTransaminase", "2.2", 42);
        verifyDynamicExtensionField(ETIOLOGY_TAB_LOCATOR, "serumAlbumin", "3.3", 43);
        verifyDynamicExtensionField(ETIOLOGY_TAB_LOCATOR, "aspartateAminotransferase", "4.4", 44);
        verifyDynamicExtensionField(ETIOLOGY_TAB_LOCATOR, "totalBilirubin", "5.5", 45);
        verifyDynamicExtensionField(ETIOLOGY_TAB_LOCATOR, "serumGlutamateOxaloaceticTransaminase", "6.6", 46);
        verifyDynamicExtensionField(ETIOLOGY_TAB_LOCATOR, "serumGlutamatePyruvateTransaminase", "1.1", 47);
        verifyDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "albumin", "0.1", 48);
        verifySelectDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "anatomicalLocationPancreaticCancer", "Tail Of Pancreas", 49);
        verifyDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "proportionOfBronchioloalveolarCarcinoma", "0.44", 50);
        verifyDynamicExtensionField(PATHOLOGY_TAB_LOCATOR, "anatomicalLocationGlioblastomaMultiformecancer", "foobar", 51);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "surgeryType", "foobar", 52);
        verifySelectDynamicExtensionField(TREATMENT_TAB_LOCATOR, "completeResectionAchieved", "Yes", 53);
        verifySelectDynamicExtensionField(TREATMENT_TAB_LOCATOR, "completeResectionNotPossibleBecause", "Positive Margin", 54);
        verifySelectDynamicExtensionField(TREATMENT_TAB_LOCATOR, "preoperativeChemotherapy", "Yes", 55);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "preOperativeChemotherapyDrugOrRegimen", "foobar", 56);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "preOperativeChemotherapyDosage", "foobar", 57);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "preOperativeChemotherapyCycles", "3", 58);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "preOperativeChemotherapyStartDate", "January 1, 2000", 59);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "preOperativeChemotherapyEndDate", "January 1, 2001", 60);
        verifySelectDynamicExtensionField(TREATMENT_TAB_LOCATOR, "treatmentSinceSurgery", "Yes", 61);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "treatmentDrugOrRegimenSinceSurgery", "foobar", 62);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "treatmentDosageSinceSurgery", "foobar", 63);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "treatmentCyclesSinceSurgery", "3", 64);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "treatmentSinceSurgeryStartDate", "January 1, 2002", 65);
        verifyDynamicExtensionField(TREATMENT_TAB_LOCATOR, "treatmentSinceSurgeryEndDate", "January 1, 2003", 66);
        verifySelectDynamicExtensionField(TREATMENT_TAB_LOCATOR, "radiation", "Yes", 67);
        verifySelectDynamicExtensionField(TREATMENT_TAB_LOCATOR, "liverCancerTreatment", "Radiofrequency Ablation (RFA)", 127);
        verifySelectDynamicExtensionField(TREATMENT_TAB_LOCATOR, "hasRegularFollowUp", "Yes", 68);
        verifyDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "monthsBetweenFollowUp", "4", 69);
        verifyDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "followUpStartDate", "January 1, 2003", 70);
        verifyDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "followUpEndDate", "January 1, 2004", 71);
        verifySelectDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "recurranceStatus", "Yes", 72);
        verifySelectDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "recurranceLocation", "Distant Organ Site", 73);
        verifyDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "distantRecurranceOrganSite", "foobar", 74);
        verifySelectDynamicExtensionField(TREATMENT_TAB_LOCATOR, "recurranceFoundByInterveningSymptoms", "Yes", 75);
        verifyDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "noRecurranceAndDateLastKnownDiseaseFree", "January 1, 2005", 76);
        verifySelectDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "progressionStatus", "Progressing", 77);
        verifyDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "progressingDateOfFirstKnownProgression", "January 1, 2005", 78);
        verifySelectDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "progressionSymptomatic", "Symptomatic", 79);
        verifySelectDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "progressionLocation", "Distant Organ Site", 80);
        verifyDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "distantProgressionOrganSite", "foobar", 81);
        verifyDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "noProgressionLastDateKnownWithoutProgression", "January 1, 2005", 82);
        verifySelectDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "patientsVitalStatus", "Deceased", 83);
        verifyDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "dateOfDeath", "January 1, 2005", 84);
        verifySelectDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "causeOfDeath", "Other Organ Failure", 85);
        verifyDynamicExtensionField(FOLLOW_UP_TAB_LOCATOR, "notDeadLastKnownDateAlive", "January 1, 2005", 86);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "her2MutationStatus", "Mutant", 87);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "her2MutationType", "foobar", 88);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "her2MutationSite", "foobar", 89);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "lungCancerKrasMutationStatus", "Mutant", 90);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "lungCancerKrasMutationType", "foobar", 91);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "lungCancerKrasMutationSite", "foobar", 92);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "lungCancerEgfrMutationStatus", "Mutant", 93);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "lungCancerEgfrMutationType", "foobar", 94);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "lungCancerEgfrMutationSite", "foobar", 95);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "alphaFetoprotein", "1.1", 96);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "colonCancerKrasMutationStatus", "Mutant", 97);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "colonCancerKrasMutationType", "foobar", 98);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "colonCancerKrasMutationSite", "foobar", 99);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "apcStatus", "foobar", 100);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "brafMutationStatus", "Mutant", 101);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "brafMutationType", "foobar", 102);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "brafMutationSite", "foobar", 103);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pi3kMutationStatus", "Mutant", 104);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pi3kMutationType", "foobar", 105);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pi3kMutationSite", "foobar", 106);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "betaCateninMutationStatus", "Mutant", 107);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "betaCateninExpressionLevel", "foobar", 108);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "msiMutationStatus", "MSS (Microsatellite Stable)", 109);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pancreaticCancerKrasMutationStatus", "Mutant", 110);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pancreaticCancerKrasMutationType", "foobar", 111);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pancreaticCancerKrasMutationSite", "foobar", 112);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pancreaticCancerEgfrMutationStatus", "Mutant", 113);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pancreaticCancerEgfrMutationType", "foobar", 114);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pancreaticCancerEgfrMutationSite", "foobar", 115);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pancreaticCancerP53MutationStatus", "Mutant", 116);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pancreaticCancerP53MutationType", "foobar", 117);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "pancreaticCancerP53MutationSite", "foobar", 118);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "gbmCancerKrasMutationStatus", "Mutant", 119);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "gbmCancerKrasMutationType", "foobar", 120);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "gbmCancerKrasMutationSite", "foobar", 121);
        verifySelectDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "gbmCancerEgfrMutationStatus", "Mutant", 122);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "gbmCancerEgfrMutationType", "foobar", 123);
        verifyDynamicExtensionField(TUMOR_MARKERS_TAB_LOCATOR, "gbmCancerEgfrMutationSite", "foobar", 124);
        verifySelectDynamicExtensionField(TREATMENT_TAB_LOCATOR, "gbmCancerEgfrv3MutationStatus", "Yes", 125);
        verifySelectDynamicExtensionField(TREATMENT_TAB_LOCATOR, "gbmPtenExpression", "Yes", 126);
    }
    //CHECKSTYLE:ON

    private void verifySelectDynamicExtensionField(String locator, String fieldName, String fieldSearchTargetValue,
            int targetSpecimenExternalId) {
        selenium.click(locator);
        selenium.select("object.customProperties['" + fieldName + "']", "label=" + fieldSearchTargetValue);
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("extension " + targetSpecimenExternalId));
        selenium.click(locator);
        selenium.select("object.customProperties['" + fieldName + "']", "label=" + ALL_STRING);
    }

    private void verifyDynamicExtensionField(String locator, String fieldName, String fieldSearchTargetValue,
            int targetSpecimenExternalId) {
        selenium.click(locator);
        selenium.type("customProperties['" + fieldName + "']", fieldSearchTargetValue);
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("extension " + targetSpecimenExternalId));
        selenium.click(locator);
        selenium.type("customProperties['" + fieldName + "']", EMPTY_STRING);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifySearchDisplayOptions() {
        goToSearchPage(false);
        clickAndWait("btn_search");
        assertEquals("Configure Displayed Columns", selenium.getText("link=Configure Displayed Columns"));
        clickAndWait("link=Configure Displayed Columns");
        selenium.selectFrame("popupFrame");
        assertTrue(selenium.isTextPresent("Specimen Results Display Options"));
        selenium.click("link=Patient Demographics");
        pause(SHORT_DELAY);
        selenium.click("link=Biospecimen Characteristics");
        pause(SHORT_DELAY);
        assertEquals("Check all", selenium.getText("link=Check all"));
        assertEquals("Uncheck all", selenium.getText("link=Uncheck all"));
        assertTrue(selenium.isElementPresent("Biospecimen_Characteristics_externalId"));
        assertEquals("on", selenium.getValue("Biospecimen_Characteristics_externalId"));
        assertTrue(selenium.isElementPresent("Biospecimen_Characteristics_preservationType"));
        assertEquals("on", selenium.getValue("Biospecimen_Characteristics_preservationType"));
        // check un-select all functionality
        selenium.click("link=Uncheck all");
        assertFalse(selenium.isChecked("css=input[id^='Biospecimen_Characteristics_']"));
        selenium.click("link=Check all");
        assertTrue(selenium.isChecked("css=input[id^='Biospecimen_Characteristics_']"));
        selenium.click("Biospecimen_Characteristics_externalId");
        assertEquals("off", selenium.getValue("Biospecimen_Characteristics_externalId"));
        selenium.click("Biospecimen_Characteristics_preservationType");
        assertEquals("off", selenium.getValue("Biospecimen_Characteristics_preservationType"));
        // cancel selection
        selenium.click("link=Cancel");
        selenium.selectFrame("relative=up");
        // go back and check the unselected values haven't actually changed value because of cancel
        clickAndWait("link=Configure Displayed Columns");
        selenium.selectFrame("popupFrame");
        assertEquals("on", selenium.getValue("Biospecimen_Characteristics_externalId"));
        assertEquals("on", selenium.getValue("Biospecimen_Characteristics_preservationType"));
        selenium.click("Biospecimen_Characteristics_externalId");
        assertEquals("off", selenium.getValue("Biospecimen_Characteristics_externalId"));
        selenium.click("Biospecimen_Characteristics_preservationType");
        assertEquals("off", selenium.getValue("Biospecimen_Characteristics_preservationType"));
        selenium.click("makeSettingsPersistendId"); // make changes persistent
        // save the selection
        selenium.click("btn_savedisplaypreferences");
        waitForPageToLoad();
        selenium.selectWindow(null);
        pause(LONG_DELAY + DELAY * 2);
        assertFalse(selenium.isTextPresent("External ID"));
        assertFalse(selenium.isTextPresent("Preservation Type"));
        // leave the page
        clickAndWait("link=Home");
        // go to specimen administration and check that nothing has changed there
        clickAndWait("link=Biospecimen Administration");
        assertTrue(selenium.isTextPresent("External ID"));
        // go back to search
        goToSearchPage(false);
        assertFalse(selenium.isTextPresent("External ID"));
        assertFalse(selenium.isTextPresent("Preservation Type"));
        // logout
        clickAndWait("link=Sign Out");
        // login as user1
        loginAsUser1();
        // go to the search page
        goToSearchPage(false);
        // and click on search because this is the first time on page after a login
        clickAndWait("btn_search");
        clickAndWait("link=Configure Displayed Columns");
        selenium.selectFrame("popupFrame");
        verifyFieldsPresence("Biospecimen Characteristics", false, new String[] {"externalId" });
        // cancel selection
        selenium.click("link=Cancel");
        selenium.selectFrame("relative=up");
        // logout
        clickAndWait("link=Sign Out");
        // log back in as admin
        loginAsAdmin();
        //test enhanced extensions fields
        checkEnhancedExtensionsFieldColumns();
        setDefaultDisplayedColumns();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void verifyCategoryTabs() {
        goToSearchPage(false);
        clickShowHideFilters(true);
        assertEquals("Biospecimen Characteristics", selenium.getText(BIOSPECIMEN_CHARACTERISTICS_TAB_LOCATOR).trim());
        assertEquals("Patient Demographics", selenium.getText(PATIENT_DEMOGRAPHICS_TAB_LOCATOR).trim());
        assertEquals("Pathology", selenium.getText(PATHOLOGY_TAB_LOCATOR).trim());
        assertEquals("Treatment", selenium.getText(TREATMENT_TAB_LOCATOR).trim());
        assertEquals("Follow-Up", selenium.getText(FOLLOW_UP_TAB_LOCATOR).trim());
        assertEquals("Tumor Markers", selenium.getText(TUMOR_MARKERS_TAB_LOCATOR).trim());
        assertEquals("Etiology", selenium.getText(ETIOLOGY_TAB_LOCATOR).trim());
        verifyPatientDemographicsFieldsArePresent(true);
        verifyPathologyFieldsArePresent(true);
        verifyTreatmentFieldsArePresent(true);
        verifyFollowUpFieldsArePresent(true);
        verifyTumorMarkersFieldsArePresent(true);
        verifyEtiologyFieldsArePresent(true);
        clickAndWait("link=Sign Out");
        loginAsUser1();
        goToSearchPage(false);
        verifyPatientDemographicsFieldsArePresent(false);
        verifyPathologyFieldsArePresent(false);
        verifyTreatmentFieldsArePresent(false);
        verifyFollowUpFieldsArePresent(false);
        verifyTumorMarkersFieldsArePresent(false);
        verifyEtiologyFieldsArePresent(false);
        goToSearchPage(false);
        clickAndWait("link=Sign Out");
        loginAsAdmin();
        goToSearchPage(false);
    }

    //CHECKSTYLE:OFF
    private String getXPathForCategoryName(String categoryName) {
        return "xpath=//th/div[text()=\"" + categoryName + "\"]";
    }
    
    private void checkConditionalCategoryVisibility(boolean userIsAdmin, String categoryName) {
        String xPath = getXPathForCategoryName(categoryName);
        assertTrue(userIsAdmin ? selenium.isVisible(xPath) : !selenium.isElementPresent(xPath));
    }
    
    private void verifyPatientDemographicsFieldsArePresent(boolean userIsSpecimenAdmin) {
        selenium.click(PATIENT_DEMOGRAPHICS_TAB_LOCATOR);
        assertTrue(selenium.isVisible(getXPathForCategoryName("Patient Demographics")));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['dateOfDiagnosis']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['survivalInMonths']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['diseaseFreeSurvivalInMonths']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['smokingStatus']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['dateOfSurgery']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['ageAtDiagnosisInYears']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['weightInKg']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['heightInCm']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['packYearsOfSmoking']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['yearsSinceSmokingCessation']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['alcoholUseStatus']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['relativeAlcoholUse']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['yearsSinceAlcoholCessation']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['familyHistoryOfCancer']\"]"));
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Patient Demographics (Gastric Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['familyHistoryOfGastricCancer']\"]");
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Patient Demographics (Lung Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['familyHistoryOfLungCancer']\"]");
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Patient Demographics (Liver Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['familyHistoryOfLiverCancer']\"]");
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Patient Demographics (Colon Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['familyHistoryOfColonCancer']\"]");
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Patient Demographics (Pancreatic Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['familyHistoryOfPancreaticCancer']\"]");
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Patient Demographics (Glioblastoma Multiforme Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['familyHistoryOfGbmCancer']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['occupation']\"]");
    }

    private void verifyPathologyFieldsArePresent(boolean userIsSpecimenAdmin) {
        selenium.click(PATHOLOGY_TAB_LOCATOR);
        assertTrue(selenium.isVisible(getXPathForCategoryName("Pathology")));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['tumorSizeCm']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['childsGrade']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['edmondsonsGrade']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['tnmStageT']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['tnmStageN']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['tnmStageM']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['ecogPerformanceStatus']\"]"));
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Pathology (Gastric Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['hPyloriStatus']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['hPyloriStrain']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['gastricCancerHistology']\"]");
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Pathology (Lung Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['proportionOfBronchioloalveolarCarcinoma']\"]");
        assertTrue(selenium.isVisible(getXPathForCategoryName("Pathology (Liver Cancer)")));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['bclccStage']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['percentageOfTumorCells']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['percentageOfNecrosis']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['numberOfNodules']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['alanineTransaminase']\"]");
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Pathology (Colon Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['colonCancerHistology']\"]");
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Pathology (Pancreatic Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['albumin']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['anatomicalLocationPancreaticCancer']\"]");
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Pathology (Glioblastoma Multiforme Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['anatomicalLocationGlioblastomaMultiformecancer']\"]");
    }

    private void verifyTreatmentFieldsArePresent(boolean userIsSpecimenAdmin) {
        selenium.click(TREATMENT_TAB_LOCATOR);
        assertTrue(selenium.isVisible(getXPathForCategoryName("Treatment")));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['surgeryType']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['completeResectionAchieved']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['completeResectionNotPossibleBecause']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['preoperativeChemotherapy']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['preOperativeChemotherapyDrugOrRegimen']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['preOperativeChemotherapyDosage']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['preOperativeChemotherapyCycles']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['preOperativeChemotherapyStartDate']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['preOperativeChemotherapyEndDate']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['treatmentDrugOrRegimenSinceSurgery']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['treatmentDosageSinceSurgery']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['treatmentCyclesSinceSurgery']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['treatmentSinceSurgeryStartDate']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['treatmentSinceSurgeryEndDate']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['radiation']\"]"));
        checkConditionalCategoryVisibility(userIsSpecimenAdmin, "Treatment (Liver Cancer)");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['liverCancerTreatment']\"]");
    }

    private void verifyFollowUpFieldsArePresent(boolean userIsSpecimenAdmin) {
        selenium.click(FOLLOW_UP_TAB_LOCATOR);
        assertTrue(selenium.isVisible(getXPathForCategoryName("Follow Up")));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['hasRegularFollowUp']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['monthsBetweenFollowUp']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['followUpStartDate']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['followUpEndDate']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['recurranceStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['recurranceLocation']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['distantRecurranceOrganSite']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['recurranceFoundByInterveningSymptoms']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['noRecurranceAndDateLastKnownDiseaseFree']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['progressionStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['progressingDateOfFirstKnownProgression']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['progressionSymptomatic']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['progressionLocation']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['distantProgressionOrganSite']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['noProgressionLastDateKnownWithoutProgression']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['patientsVitalStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['dateOfDeath']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['causeOfDeath']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['notDeadLastKnownDateAlive']\"]");
    }

    private void verifyTumorMarkersFieldsArePresent(boolean userIsSpecimenAdmin) {
        selenium.click(TUMOR_MARKERS_TAB_LOCATOR);
        assertTrue(selenium.isVisible(getXPathForCategoryName("Tumor Markers (Gastric Cancer)")));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['her2MutationStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['her2MutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['her2MutationSite']\"]");
        assertTrue(selenium.isVisible(getXPathForCategoryName("Tumor Markers (Lung Cancer)")));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['lungCancerKrasMutationStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['lungCancerKrasMutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['lungCancerKrasMutationSite']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['lungCancerEgfrMutationStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['lungCancerEgfrMutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['lungCancerEgfrMutationSite']\"]");
        assertTrue(selenium.isVisible(getXPathForCategoryName("Tumor Markers (Liver Cancer)")));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['alphaFetoprotein']\"]"));
        assertTrue(selenium.isVisible(getXPathForCategoryName("Tumor Markers (Colon Cancer)")));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['colonCancerKrasMutationStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['colonCancerKrasMutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['apcStatus']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['brafMutationStatus']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['brafMutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['brafMutationSite']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['pi3kMutationStatus']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['pi3kMutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['pi3kMutationSite']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['betaCateninMutationStatus']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['betaCateninExpressionLevel']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['msiMutationStatus']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['pancreaticCancerKrasMutationStatus']\"]"));
        assertTrue(selenium.isVisible(getXPathForCategoryName("Tumor Markers (Pancreatic Cancer)")));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['pancreaticCancerKrasMutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['pancreaticCancerKrasMutationSite']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['pancreaticCancerEgfrMutationStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['pancreaticCancerEgfrMutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['pancreaticCancerEgfrMutationSite']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['pancreaticCancerP53MutationStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['pancreaticCancerP53MutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['pancreaticCancerP53MutationSite']\"]");
        assertTrue(selenium.isVisible(getXPathForCategoryName("Tumor Markers (Glioblastoma Multiforme Cancer)")));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['gbmCancerKrasMutationStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['gbmCancerKrasMutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['gbmCancerKrasMutationSite']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['gbmCancerEgfrMutationStatus']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['gbmCancerEgfrMutationType']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['gbmCancerEgfrMutationSite']\"]");
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['gbmCancerEgfrv3MutationStatus']\"]"));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['gbmPtenExpression']\"]"));
    }

    private void verifyEtiologyFieldsArePresent(boolean userIsSpecimenAdmin) {
        selenium.click(ETIOLOGY_TAB_LOCATOR);
        assertTrue(selenium.isVisible(getXPathForCategoryName("Etiology (Liver Cancer)")));
        assertTrue(selenium.isVisible("xpath=//label[@for=\"object.customProperties['etiology']\"]"));
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['nonTumerousLiverHistology']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['veinousInfiltration']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['portalVeinThrombosis']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['hcvabStatus']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['hbsAgStatus']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['serumAlbumin']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['aspartateAminotransferase']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['totalBilirubin']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['serumGlutamateOxaloaceticTransaminase']\"]");
        checkConditionalVisibility(userIsSpecimenAdmin, "xpath=//label[@for=\"object.customProperties['serumGlutamatePyruvateTransaminase']\"]");
    }
    //CHECKSTYLE:ON

    private void checkConditionalVisibility(boolean userIsSpecimenAdmin, String xPath) {
        assertTrue(userIsSpecimenAdmin ? selenium.isVisible(xPath) : !selenium.isElementPresent(xPath));
    }

    private List<String> getSpecimenTableColumnHeaders() {
        if (null == specimenTableColumnHeaders) {
            specimenTableColumnHeaders = new ArrayList<String>();
            Number tableColumnHeaderCount = selenium.getXpathCount("//table[@id=\"specimen\"]//th");
            for (int i = 1; i <= tableColumnHeaderCount.intValue(); i++) {
                specimenTableColumnHeaders.add(selenium.getText("//table[@id=\"specimen\"]//th[" + i + "]"));
            }
        }
        return specimenTableColumnHeaders;
    }

    private boolean checkSpecimenTableColumnHeadersForString(boolean shouldBePresent, String stringToSearchFor) {
        return shouldBePresent && getSpecimenTableColumnHeaders().contains(stringToSearchFor);
    }

    private void checkEnhancedExtensionsFieldColumns() {
        // start in correct location
        goToSearchPage(false);
        clickAndWait("btn_search");

        // make sure field columns not already visible
        FieldColumnConfigInfoWrapper[] wrappers = getFieldColumnConfigInfoWrappers();
        for (FieldColumnConfigInfoWrapper wrapper : wrappers) {
            assertFalse(checkSpecimenTableColumnHeadersForString(false, wrapper.getFieldDisplayName()));
        }

        // configure field columns for display
        gotoConfigureDisplayedColumns();
        for (FieldColumnConfigInfoWrapper wrapper : wrappers) {
            String categoryReferenceName = wrapper.getCategoryName().replaceAll(" ", "_") + "_";
            selenium.click("link=" + wrapper.getCategoryName());
            pause(SHORT_DELAY);
            selenium.click(categoryReferenceName + wrapper.getFieldName());
            assertEquals("on", selenium.getValue(categoryReferenceName + wrapper.getFieldName()));
        }
        selenium.click("btn_savedisplaypreferences");
        waitForPageToLoad();
        selenium.selectWindow(null);
        pause(LONG_DELAY * 2);
        for (FieldColumnConfigInfoWrapper wrapper : wrappers) {
            assertTrue(checkSpecimenTableColumnHeadersForString(true, wrapper.getFieldDisplayName()));
        }

        // configure field columns for no display
        gotoConfigureDisplayedColumns();
        for (FieldColumnConfigInfoWrapper wrapper : wrappers) {
            String categoryReferenceName = wrapper.getCategoryName().replaceAll(" ", "_") + "_";
            selenium.click("link=" + wrapper.getCategoryName());
            pause(SHORT_DELAY);
            selenium.click(categoryReferenceName + wrapper.getFieldName());
            assertEquals("off", selenium.getValue(categoryReferenceName + wrapper.getFieldName()));
        }
        selenium.click("btn_savedisplaypreferences");
        waitForPageToLoad();
        selenium.selectWindow(null);
        pause(LONG_DELAY);
        for (FieldColumnConfigInfoWrapper wrapper : wrappers) {
            assertFalse(checkSpecimenTableColumnHeadersForString(false, wrapper.getFieldDisplayName()));
        }
    }

    private void setDefaultDisplayedColumns() {
        gotoConfigureDisplayedColumns();
        selenium.click("link=Biospecimen Characteristics");
        selenium.click("link=Uncheck all");
        selenium.click("Biospecimen_Characteristics_pathologicalCharacteristic");
        selenium.click("Biospecimen_Characteristics_anatomicSource");
        selenium.click("Biospecimen_Characteristics_specimenType");
        selenium.click("Biospecimen_Characteristics_preservationType");
        selenium.click("Biospecimen_Characteristics_availableQuantity");
        selenium.click("Biospecimen_Characteristics_minimumPrice");
        selenium.click("Biospecimen_Characteristics_externalId");
        selenium.click("btn_savedisplaypreferences");
    }

    private void gotoConfigureDisplayedColumns() {
            goToSearchPage(false);
            clickAndWait("btn_search");
            clickAndWait("link=Configure Displayed Columns");
            selenium.selectFrame("popupFrame");

    }

    //CHECKSTYLE:OFF
    private FieldColumnConfigInfoWrapper[] getFieldColumnConfigInfoWrappers() {
        return new FieldColumnConfigInfoWrapper[] {
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Date of Diagnosis", "dateOfDiagnosis"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Survival in Months", "survivalInMonths"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Disease Free Survival in Months", "diseaseFreeSurvivalInMonths"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Smoking Status", "smokingStatus"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Date of Surgery", "dateOfSurgery"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Age At Diagnosis In Years", "ageAtDiagnosisInYears"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Weight (kg)", "weightInKg"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Height (cm)", "heightInCm"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Pack Years Of Smoking", "packYearsOfSmoking"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Years Since Smoking Cessation", "yearsSinceSmokingCessation"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Alcohol Use Status", "alcoholUseStatus"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Relative Alcohol Use", "relativeAlcoholUse"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Years Since Alcohol Cessation", "yearsSinceAlcoholCessation"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_CATEGORY, "Family History Of Cancer", "familyHistoryOfCancer"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_GASTRIC_CANCER_CATEGORY, "Family History Of Gastric Cancer", "familyHistoryOfGastricCancer"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_LUNG_CANCER_CATEGORY, "Family History Of Lung Cancer", "familyHistoryOfLungCancer"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_LIVER_CANCER_CATEGORY, "Family History Of Liver Cancer", "familyHistoryOfLiverCancer"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_COLON_CANCER_CATEGORY, "Family History Of Colon Cancer", "familyHistoryOfColonCancer"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_PANCREATIC_CANCER_CATEGORY, "Family History Of Pancreatic Cancer", "familyHistoryOfPancreaticCancer"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_GBM_CANCER_CATEGORY, "Family History Of Glioblastoma Multiforme Cancer", "familyHistoryOfGbmCancer"),
            new FieldColumnConfigInfoWrapper(PATIENT_DEMOGRAPHICS_GBM_CANCER_CATEGORY, "Occupation", "occupation"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_CATEGORY, "Tumor Size (cm)", "tumorSizeCm"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_CATEGORY, "Child's Grade", "childsGrade"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_CATEGORY, "Edmondson's Grade", "edmondsonsGrade"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_CATEGORY, "TNM Stage (T)", "tnmStageT"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_CATEGORY, "TNM Stage (N)", "tnmStageN"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_CATEGORY, "TNM Stage (M)", "tnmStageM"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_CATEGORY, "ECOG Performance Status", "ecogPerformanceStatus"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_GASTRIC_CANCER_CATEGORY, "H. pylori Status", "hPyloriStatus"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_GASTRIC_CANCER_CATEGORY, "H. pylori Strain", "hPyloriStrain"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_LIVER_CANCER_CATEGORY, "Barcelona Clinic Liver Cancer Classification Stage", "bclccStage"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_LIVER_CANCER_CATEGORY, "Percentage Of Tumor Cells", "percentageOfTumorCells"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_LIVER_CANCER_CATEGORY, "Percentage Of Necrosis", "percentageOfNecrosis"),
            new FieldColumnConfigInfoWrapper(ETIOLOGY_LIVER_CANCER_CATEGORY, "Etiology", "etiology"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_COLON_CANCER_CATEGORY, "Histology", "colonCancerHistology"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_GASTRIC_CANCER_CATEGORY, "Histology", "gastricCancerHistology"),
            new FieldColumnConfigInfoWrapper(ETIOLOGY_LIVER_CANCER_CATEGORY, "Nontumorous Liver Histology", "nonTumerousLiverHistology"),
            new FieldColumnConfigInfoWrapper(ETIOLOGY_LIVER_CANCER_CATEGORY, "Veinous Infiltration", "veinousInfiltration"),
            new FieldColumnConfigInfoWrapper(ETIOLOGY_LIVER_CANCER_CATEGORY, "Portal Vein Thrombosis", "portalVeinThrombosis"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_LIVER_CANCER_CATEGORY, "Number Of Nodules", "numberOfNodules"),
            new FieldColumnConfigInfoWrapper(ETIOLOGY_LIVER_CANCER_CATEGORY, "HCVAb Status", "hcvabStatus"),
            new FieldColumnConfigInfoWrapper(ETIOLOGY_LIVER_CANCER_CATEGORY, "HbsAg Status", "hbsAgStatus"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_LIVER_CANCER_CATEGORY, "Alanine Transaminase (IU/L)", "alanineTransaminase"),
            new FieldColumnConfigInfoWrapper(ETIOLOGY_LIVER_CANCER_CATEGORY, "Serum Albumin (g/dL)", "serumAlbumin"),
            new FieldColumnConfigInfoWrapper(ETIOLOGY_LIVER_CANCER_CATEGORY, "Aspartate Aminotransferase (IU/L)", "aspartateAminotransferase"),
            new FieldColumnConfigInfoWrapper(ETIOLOGY_LIVER_CANCER_CATEGORY, "Total Bilirubin (mg/dL)", "totalBilirubin"),
            new FieldColumnConfigInfoWrapper(ETIOLOGY_LIVER_CANCER_CATEGORY, "Serum GPT (IU/L)", "serumGlutamatePyruvateTransaminase"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_PANCREATIC_CANCER_CATEGORY, "Albumin (g/dL)", "albumin"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_PANCREATIC_CANCER_CATEGORY, "Anatomical Location", "anatomicalLocationPancreaticCancer"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_LUNG_CANCER_CATEGORY, "Proportion Of Tumor That Is Bronchioloalveolar Carcinoma", "proportionOfBronchioloalveolarCarcinoma"),
            new FieldColumnConfigInfoWrapper(PATHOLOGY_GBM_CANCER_CATEGORY, "Anatomical Location", "anatomicalLocationGlioblastomaMultiformecancer"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Surgery Type", "surgeryType"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Complete Resection Achieved", "completeResectionAchieved"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Complete Resection Not Possible Because", "completeResectionNotPossibleBecause"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Preoperative Chemotherapy", "preoperativeChemotherapy"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Preoperative Chemotherapy Drug or Regimen", "preOperativeChemotherapyDrugOrRegimen"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Preoperative Chemotherapy Dosage", "preOperativeChemotherapyDosage"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Preoperative Chemotherapy Cycles", "preOperativeChemotherapyCycles"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Preoperative Chemotherapy Start Date", "preOperativeChemotherapyStartDate"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Preoperative Chemotherapy End Date", "preOperativeChemotherapyEndDate"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Treatment Drug or Regimen Since Surgery", "treatmentDrugOrRegimenSinceSurgery"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Treatment Dosage Since Surgery", "treatmentDosageSinceSurgery"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Treatment Cycles Since Surgery", "treatmentCyclesSinceSurgery"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Treatment Since Surgery", "treatmentSinceSurgery"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Treatment Since Surgery Start Date", "treatmentSinceSurgeryStartDate"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Treatment Since Surgery End Date", "treatmentSinceSurgeryEndDate"),
            new FieldColumnConfigInfoWrapper(TREATMENT_CATEGORY, "Radiation", "radiation"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Patient Has Regular Follow Up", "hasRegularFollowUp"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Months Between Follow Up", "monthsBetweenFollowUp"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Follow Up Start Date", "followUpStartDate"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Follow Up End Date", "followUpEndDate"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Recurrance Status", "recurranceStatus"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Recurrance Location", "recurranceLocation"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Distant Recurrance Organ Site", "distantRecurranceOrganSite"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Recurrance Found By Intervening Symptoms", "recurranceFoundByInterveningSymptoms"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "No Recurrance And Date Last Known Disease Free", "noRecurranceAndDateLastKnownDiseaseFree"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Progression Status", "progressionStatus"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Progressing, Date Of First Known Progression", "progressingDateOfFirstKnownProgression"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Progression Symptomatic", "progressionSymptomatic"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Progression Location", "progressionLocation"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Distant Progression Organ Site", "distantProgressionOrganSite"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "No Progression, Last Date Known Without Progression", "noProgressionLastDateKnownWithoutProgression"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Patient's Vital Status", "patientsVitalStatus"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Date Of Death", "dateOfDeath"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Cause Of Death", "causeOfDeath"),
            new FieldColumnConfigInfoWrapper(FOLLOW_UP_CATEGORY, "Not Dead, Last Known Date Alive", "notDeadLastKnownDateAlive"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GASTRIC_CANCER_CATEGORY, "Human Epidermal Growth Factor Receptor 2 (HER2) Mutation Status", "her2MutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GASTRIC_CANCER_CATEGORY, "Human Epidermal Growth Factor Receptor 2 (HER2) Mutation Type", "her2MutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GASTRIC_CANCER_CATEGORY, "Human Epidermal Growth Factor Receptor 2 (HER2) Mutation Site", "her2MutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_LUNG_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Status", "lungCancerKrasMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_LUNG_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Type", "lungCancerKrasMutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_LUNG_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Site", "lungCancerKrasMutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_LUNG_CANCER_CATEGORY, "Epidermal Growth Factor (EGFR) Mutation Status", "lungCancerEgfrMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_LUNG_CANCER_CATEGORY, "Epidermal Growth Factor (EGFR) Mutation Type", "lungCancerEgfrMutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_LUNG_CANCER_CATEGORY, "Epidermal Growth Factor (EGFR) Mutation Site", "lungCancerEgfrMutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_LIVER_CANCER_CATEGORY, "Alpha Fetoprotein (ng/mL)", "alphaFetoprotein"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Status", "colonCancerKrasMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Type", "colonCancerKrasMutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Site", "colonCancerKrasMutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "Adenomatous Polyposis coli (APC) Status", "apcStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "BRAF Mutation Status", "brafMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "BRAF Mutation Type", "brafMutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "BRAF Mutation Site", "brafMutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "PI3K Mutation Status", "pi3kMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "PI3K Mutation Type", "pi3kMutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "PI3K Mutation Site", "pi3kMutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "Beta Catenin Mutation Status", "betaCateninMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "Beta Catenin Expression Level", "betaCateninExpressionLevel"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_COLON_CANCER_CATEGORY, "Microsatellite Instability (MSI) Mutation Status", "msiMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_PANCREATIC_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Status", "pancreaticCancerKrasMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_PANCREATIC_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Type", "pancreaticCancerKrasMutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_PANCREATIC_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Site", "pancreaticCancerKrasMutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_PANCREATIC_CANCER_CATEGORY, "Epidermal Growth Factor (EGFR) Mutation Status", "pancreaticCancerEgfrMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_PANCREATIC_CANCER_CATEGORY, "Epidermal Growth Factor (EGFR) Mutation Type", "pancreaticCancerEgfrMutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_PANCREATIC_CANCER_CATEGORY, "Epidermal Growth Factor (EGFR) Mutation Site", "pancreaticCancerEgfrMutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_PANCREATIC_CANCER_CATEGORY, "Protein 53 (p53) Mutation Status", "pancreaticCancerP53MutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_PANCREATIC_CANCER_CATEGORY, "Protein 53 (p53) Mutation Type", "pancreaticCancerP53MutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_PANCREATIC_CANCER_CATEGORY, "Protein 53 (p53) Mutation Site", "pancreaticCancerP53MutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GBM_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Status", "gbmCancerKrasMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GBM_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Type", "gbmCancerKrasMutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GBM_CANCER_CATEGORY, "GTPase Kras (KRAS) Mutation Site", "gbmCancerKrasMutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GBM_CANCER_CATEGORY, "Epidermal Growth Factor (EGFR) Mutation Status", "gbmCancerEgfrMutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GBM_CANCER_CATEGORY, "Epidermal Growth Factor (EGFR) Mutation Type", "gbmCancerEgfrMutationType"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GBM_CANCER_CATEGORY, "Epidermal Growth Factor (EGFR) Mutation Site", "gbmCancerEgfrMutationSite"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GBM_CANCER_CATEGORY, "EGFRv3 Status", "gbmCancerEgfrv3MutationStatus"),
            new FieldColumnConfigInfoWrapper(TUMOR_MARKERS_GBM_CANCER_CATEGORY, "Phosphatase And Tensin Homolog (PTEN) Expression", "gbmPtenExpression"),
            new FieldColumnConfigInfoWrapper(TREATMENT_LIVER_CANCER_CATEGORY, "Treatment", "liverCancerTreatment")

        };
    }
    //CHECKSTYLE:ON

    /**
     * private inner class storing field column config information.
     * @author dharley
     */
    private static class FieldColumnConfigInfoWrapper {
        private final String categoryName;
        private final String fieldDisplayName;
        private final String fieldName;

        FieldColumnConfigInfoWrapper(String categoryName, String fieldDisplayName, String fieldName) {
            this.categoryName = categoryName;
            this.fieldDisplayName = fieldDisplayName;
            this.fieldName = fieldName;
        }

        String getCategoryName() {
            return categoryName;
        }

        String getFieldDisplayName() {
            return fieldDisplayName;
        }

        String getFieldName() {
            return fieldName;
        }
    }

}
