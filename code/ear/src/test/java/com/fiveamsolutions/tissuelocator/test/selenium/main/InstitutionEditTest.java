/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.xwork.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author smiller
 *
 */
public class InstitutionEditTest extends AbstractTissueLocatorSeleniumTest {

    private static final int DELAY = 1500;

    private static final int NUM_INSTITUTIONS_THREE = 3;
    private static final int NUM_INSTITUTIONS_FOUR = 4;
    private static final int NUM_INSTITUTIONS_FIVE = 5;
    private static final int NUM_INSTITUTIONS_SIX = 6;
    private static final int RESTRICTIONS_LENGTH = 3999;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "InstitutionEditTest.sql";
    }

    /**
     * Tests specimen management.
     * @throws Exception on error
     */
    @Test
    public void testInstitutionCreateAndEdit() throws Exception {
        goToInstitutionPage();
        requiredValidation();
        formatValidation();
        create();
        createNonConsortiumMember();
        duplicateValidation();
        edit();
        verifyDefaultProtocol();

        verifyCreatePage();
        verifyConsortiumAdminEditPage();
        verifyInstitutionalAdminEditPage();

        if (ClientProperties.isMtaActive()) {
            createWithMtaContact();
            editConsortiumMemberWithoutMtas();
            editNonConsortiumMemberWithContact();
            editConsortiumMemberWithMtas();
        }
    }

    private void goToInstitutionPage() {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");

        // go to list page
        clickAndWait("link=Institution Administration");
        assertTrue(selenium.isTextPresent("Institution Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));

        // go to edit page
        clickAndWait("link=Add New Institution");
        assertTrue(selenium.isTextPresent("Institution Administration"));
        assertFalse(selenium.isTextPresent("Search By Name"));

        // use cancel link
        clickAndWait("link=Cancel");
        assertTrue(selenium.isTextPresent("Institution Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));

        // go to edit page
        clickAndWait("link=Add New Institution");
        assertTrue(selenium.isTextPresent("Institution Administration"));
        assertFalse(selenium.isTextPresent("Search By Name"));

        // use back to list link
        clickAndWait("link=Back To List");
        assertTrue(selenium.isTextPresent("Institution Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));
        int count = ClientProperties.getDefaultInstitutionCount() + NUM_INSTITUTIONS_THREE;
        assertTrue(selenium.isTextPresent("1-" + count + " of " + count + " Result"));

        // go to edit page
        clickAndWait("link=Add New Institution");
        assertTrue(selenium.isTextPresent("Institution Administration"));
        assertFalse(selenium.isTextPresent("Search By Name"));
    }

    private void requiredValidation() {
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Name must be set"));
        assertTrue(selenium.isTextPresent("Type must be set"));
    }

    private void formatValidation() {
        selenium.type("homepage", "invalid");
        if (ClientProperties.isInstitutionProfileUrlActive()) {
            selenium.type("institutionProfileUrl", "invalid");
        }
        selenium.type("email", "invalid");
        selenium.check("consortiumMember");
        selenium.type("dunsEINNumber", "abcdefghi");
        selenium.type("restrictions", StringUtils.leftPad("*", RESTRICTIONS_LENGTH + 1, "*"));
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Name must be set"));
        assertTrue(selenium.isTextPresent("Type must be set"));
        assertTrue(selenium.isElementPresent("xpath=//div[@id='wwerr_homepage']/div"));
        assertEquals("URL is not well formed", selenium.getText("xpath=//div[@id='wwerr_homepage']/div"));
        if (ClientProperties.isInstitutionProfileUrlActive()) {
            assertTrue(selenium.isElementPresent("xpath=//div[@id='wwerr_institutionProfileUrl']/div"));
            assertEquals("URL is not well formed",
                    selenium.getText("xpath=//div[@id='wwerr_institutionProfileUrl']/div"));
        }
        assertTrue(selenium.isTextPresent(ClientProperties.getConsortiumMemberContactInfoRequiredMessage()));
        assertTrue(selenium.isTextPresent("Email is not a well-formed email address"));
        assertTrue(selenium.isTextPresent("If setting DUNS/EIN, both Type and Number must be set."));
        assertTrue(selenium.isTextPresent("DUNS/EIN number must be 9 digits."));
        assertTrue(selenium.isTextPresent("Restrictions length must be between 0 and 3999"));
    }

    private void create() throws Exception {
        enterData("test-123456789", true, true);
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Institution Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));
        int count = ClientProperties.getDefaultInstitutionCount() + NUM_INSTITUTIONS_FOUR;
        assertTrue(selenium.isTextPresent("1-" + count + " of " + count + " Result"));
        assertTrue(selenium.isTextPresent("test-123456789"));
    }

    private void createWithMtaContact() throws Exception {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Institution Administration");
        clickAndWait("link=Add New Institution");
        enterMtaContact();
        String filePath = new File(ClassLoader.getSystemResource("InstitutionEditTest.sql").toURI()).toString();
        selenium.type("document", filePath);
        enterData("test-1337", false, false);
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Institution Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));
        int count = ClientProperties.getDefaultInstitutionCount() + NUM_INSTITUTIONS_SIX;
        assertTrue(selenium.isTextPresent("1-" + count + " of " + count + " Result"));
        assertTrue(selenium.isTextPresent("test-1337"));
        clickAndWait("link=Sign Out");
    }

    private void createNonConsortiumMember() {
        clickAndWait("link=Add New Institution");
        enterData("test-1234567890", false, false);
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Institution Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));
        int count = ClientProperties.getDefaultInstitutionCount() + NUM_INSTITUTIONS_FIVE;
        assertTrue(selenium.isTextPresent("1-" + count + " of " + count + " Result"));
        assertTrue(selenium.isTextPresent("test-1234567890"));
    }

    private void enterData(String name, boolean consortiumMember, boolean includeContact) {
        selenium.type("name", name);
        selenium.select("institutionForm_object_type", "label=Academic");
        selenium.type("homepage", "http://www.example.com");
        if (ClientProperties.isInstitutionProfileUrlActive()) {
            selenium.type("institutionProfileUrl", "http://www.profile.com");
        }

        if (includeContact) {
            selenium.type("firstName", "fname");
            selenium.type("lastName", "lname");
            selenium.type("email", "fname.lname@example.com");
            selenium.type("phone", "1234567890");
            selenium.type("phoneExtension", "1234");
        }

        if (consortiumMember) {
            if (!selenium.isChecked("consortiumMember")) {
                selenium.click("consortiumMember");
                pause(DELAY);
            }
            selenium.type("restrictions", "test restrictions");
            selenium.type("billingAddressLine1", "testAddress1");
            selenium.type("billingAddressLine2", "testAddress2");
            selenium.type("billingAddressCity", "City");
            selenium.select("billingAddressState", "label=Virginia");
            selenium.type("billingAddressZip", "Zip");
            assertEquals("United States", selenium.getSelectedLabel("billingAddressCountry"));
            selenium.select("billingAddressCountry", "label=Togo");
            selenium.type("billingAddressPhone", "123456789");
            selenium.type("billingAddressPhoneExtension", "1234");
            selenium.type("paymentInstructions", "payment instructions");
            selenium.select("dunsEINType", "label=DUNS");
            selenium.type("dunsEINNumber", "123456789");
        }
        visibility(consortiumMember);
        clickAndWait("btn_save");
    }

    private void duplicateValidation() {
        clickAndWait("link=Add New Institution");
        enterData("test-123456789", true, true);
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("An institution with this name already exists."));
    }

    private void edit() {
        clickAndWait("link=Cancel");
        assertTrue(selenium.isTextPresent("Institution Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));
        int count = ClientProperties.getDefaultInstitutionCount() + NUM_INSTITUTIONS_FIVE;
        assertTrue(selenium.isTextPresent("1-" + count + " of " + count + " Result"));
        clickAndWait("link=test-123456789");
        assertEquals("test-123456789", selenium.getValue("name"));
        assertEquals("1", selenium.getValue("institutionForm_object_type"));
        assertEquals("http://www.example.com", selenium.getValue("homepage"));
        if (ClientProperties.isInstitutionProfileUrlActive()) {
            assertEquals("http://www.profile.com", selenium.getValue("institutionProfileUrl"));
        }
        assertEquals("test restrictions", selenium.getValue("restrictions"));
        assertEquals("fname", selenium.getValue("firstName"));
        assertEquals("lname", selenium.getValue("lastName"));
        assertEquals("fname.lname@example.com", selenium.getValue("email"));
        assertEquals("1234567890", selenium.getValue("phone"));
        assertEquals("1234", selenium.getValue("phoneExtension"));
        selenium.type("homepage", "http://www.example.com/updated");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertTrue(selenium.isTextPresent("Search By Name"));
        assertTrue(selenium.isTextPresent("1-" + count + " of " + count + " Result"));
        assertTrue(selenium.isTextPresent("test-123456789"));
        assertTrue(selenium.isTextPresent("http://www.example.com/updated"));
        clickAndWait("link=Sign Out");
    }

    private void verifyDefaultProtocol() {
        if (ClientProperties.isProtocolAdministrationActive()) {
            loginAsAdmin();
            mouseOverAndPause("link=Administration");
            clickAndWait("link=Protocol Administration");
            assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
            resultsCount(ClientProperties.getCollectionProtocolCount() + 1, PAGE_SIZE);
            selenium.type("name", "Biolocator Collection Protocol");
            clickAndWait("btn_search");
            resultsCount(1, PAGE_SIZE);
            assertEquals("Biolocator Collection Protocol", selenium.getTable("protocol.1.0"));
            assertEquals("test-123456789", selenium.getTable("protocol.1.1"));
            clickAndWait("link=Sign Out");
        }
    }

    private void visibility(boolean visible) {
        assertTrue(selenium.isVisible("name"));
        assertTrue(selenium.isVisible("institutionForm_object_type"));
        assertTrue(selenium.isVisible("homepage"));
        if (ClientProperties.isInstitutionProfileUrlActive()) {
            assertTrue(selenium.isVisible("institutionProfileUrl"));
        }
        assertTrue(selenium.isVisible("consortiumMember"));
        assertTrue(selenium.isVisible("firstName"));
        assertTrue(selenium.isVisible("lastName"));
        assertTrue(selenium.isVisible("email"));
        assertTrue(selenium.isVisible("phone"));
        assertTrue(selenium.isVisible("phoneExtension"));

        assertEquals(visible, selenium.isVisible("restrictions"));
        assertEquals(visible, selenium.isVisible("billingAddressLine1"));
        assertEquals(visible, selenium.isVisible("billingAddressLine2"));
        assertEquals(visible, selenium.isVisible("billingAddressCity"));
        assertEquals(visible, selenium.isVisible("billingAddressState"));
        assertEquals(visible, selenium.isVisible("billingAddressZip"));
        assertEquals(visible, selenium.isVisible("billingAddressCountry"));
        assertEquals(visible, selenium.isVisible("billingAddressPhone"));
        assertEquals(visible, selenium.isVisible("billingAddressPhoneExtension"));
        assertEquals(visible, selenium.isVisible("paymentInstructions"));
        assertEquals(visible, selenium.isVisible("dunsEINType"));
        assertEquals(visible, selenium.isVisible("dunsEINNumber"));
    }

    private void verifyCreatePage() {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Institution Administration");
        clickAndWait("link=Add New Institution");
        boolean mtaActive = ClientProperties.isMtaActive();
        assertEquals(mtaActive, selenium.isElementPresent("mta_admin"));
        assertEquals(mtaActive, selenium.isTextPresent("Upload Signed MTA"));
        assertEquals(mtaActive, selenium.isTextPresent("Download Signed MTA"));
        assertEquals(mtaActive, selenium.isTextPresent("Contact Information for the Individual Responsible for the "
                    + "Receiving Institution's MTA"));
        assertEquals(ClientProperties.isInstitutionProfileUrlActive(),
                selenium.isElementPresent("institutionProfileUrl"));
        clickAndWait("link=Sign Out");
    }

    private void verifyConsortiumAdminEditPage() {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Institution Administration");
        clickAndWait("link=Edit");
        boolean mtaActive = ClientProperties.isMtaActive();
        assertEquals(mtaActive, selenium.isElementPresent("mta_admin"));
        assertEquals(mtaActive, selenium.isTextPresent("Upload Signed MTA"));
        assertEquals(mtaActive, selenium.isElementPresent("document"));
        assertEquals(mtaActive, selenium.isTextPresent("Download Signed MTA"));
        assertEquals(mtaActive, selenium.isTextPresent("Uploaded"));
        assertEquals(mtaActive, selenium.isTextPresent("Status"));
        assertEquals(mtaActive, selenium.isTextPresent("Action"));
        assertEquals(mtaActive, selenium.isTextPresent("Contact Information for the Individual Responsible for the "
                + "Receiving Institution's MTA"));
        assertEquals(mtaActive, selenium.isTextPresent("MTA Contact Information is only saved"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactFirstName"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactLastName"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactLine1"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactLine2"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactCity"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactState"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactZip"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactCountry"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactEmail"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactPhone"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactPhoneExtension"));
        assertEquals(mtaActive, selenium.isElementPresent("mtaContactFax"));
        assertFalse(selenium.isTextPresent("Not Yet Specified"));
        clickAndWait("link=Sign Out");
    }

    private void verifyInstitutionalAdminEditPage() {
        login(ClientProperties.getInstitutionalAdminEmail(), PASSWORD);
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Institution Administration");
        clickAndWait("link=Edit");
        boolean mtaActive = ClientProperties.isMtaActive();
        assertEquals(mtaActive, selenium.isElementPresent("mta_admin"));
        assertFalse(selenium.isTextPresent("Upload Signed MTA"));
        assertFalse(selenium.isElementPresent("document"));
        assertEquals(mtaActive, selenium.isTextPresent("Download Signed MTA"));
        assertEquals(mtaActive, selenium.isTextPresent("Uploaded"));
        assertEquals(mtaActive, selenium.isTextPresent("Status"));
        assertEquals(mtaActive, selenium.isTextPresent("Action"));
        assertEquals(mtaActive, selenium.isTextPresent("Contact Information for the Individual Responsible for the "
                + "Receiving Institution's MTA"));
        assertFalse(selenium.isTextPresent("MTA Contact Information is only saved"));
        assertFalse(selenium.isElementPresent("mtaContactFirstName"));
        assertFalse(selenium.isElementPresent("mtaContactLastName"));
        assertFalse(selenium.isElementPresent("mtaContactLine1"));
        assertFalse(selenium.isElementPresent("mtaContactLine2"));
        assertFalse(selenium.isElementPresent("mtaContactCity"));
        assertFalse(selenium.isElementPresent("mtaContactState"));
        assertFalse(selenium.isElementPresent("mtaContactZip"));
        assertFalse(selenium.isElementPresent("mtaContactCountry"));
        assertFalse(selenium.isElementPresent("mtaContactEmail"));
        assertFalse(selenium.isElementPresent("mtaContactPhone"));
        assertFalse(selenium.isElementPresent("mtaContactPhoneExtension"));
        assertFalse(selenium.isElementPresent("mtaContactFax"));
        assertEquals(mtaActive, selenium.isTextPresent("Not Yet Specified"));

        assertFalse(selenium.isElementPresent("institutionProfileUrl"));
        boolean profileActive = ClientProperties.isInstitutionProfileUrlActive();
        assertEquals(profileActive, selenium.isTextPresent(ClientProperties.getInstitutionTerm() + " Profile URL"));
        assertEquals(profileActive, selenium.isElementPresent("institutionProfileLink"));

        clickAndWait("link=Sign Out");
    }

    private void editConsortiumMemberWithoutMtas() throws Exception {
        goToInstAdminPage(ClientProperties.getConsortiumAdminEmail(), "Test Consortium Member 1");
        assertFalse(selenium.isChecked("mta_type1"));
        assertTrue(selenium.isChecked("mta_type2"));

        // Check that the mta contact page is hidden when the sending MTA is clicked
        selenium.click("mta_type1");
        pause(DELAY);
        assertFalse(selenium.isVisible("mtaContactFirstName"));
        assertFalse(selenium.isVisible("mtaContactLastName"));
        assertFalse(selenium.isVisible("mtaContactLine1"));
        assertFalse(selenium.isVisible("mtaContactLine2"));
        assertFalse(selenium.isVisible("mtaContactCity"));
        assertFalse(selenium.isVisible("//div[@id='wwgrp_mtaContactState']/button"));
        assertFalse(selenium.isVisible("mtaContactZip"));
        assertFalse(selenium.isVisible("//div[@id='wwgrp_mtaContactCountry']/button"));
        assertFalse(selenium.isVisible("mtaContactEmail"));
        assertFalse(selenium.isVisible("mtaContactPhone"));
        assertFalse(selenium.isVisible("mtaContactPhoneExtension"));
        assertFalse(selenium.isVisible("mtaContactFax"));

        // Check that the mta contact page is shown when the receiving MTA is clicked
        selenium.click("mta_type2");
        pause(DELAY);
        assertTrue(selenium.isVisible("mtaContactFirstName"));
        assertTrue(selenium.isVisible("mtaContactLastName"));
        assertTrue(selenium.isVisible("mtaContactLine1"));
        assertTrue(selenium.isVisible("mtaContactLine2"));
        assertTrue(selenium.isVisible("mtaContactCity"));
        assertTrue(selenium.isVisible("//div[@id='wwgrp_mtaContactState']/button"));
        assertTrue(selenium.isVisible("mtaContactZip"));
        assertTrue(selenium.isVisible("//div[@id='wwgrp_mtaContactCountry']/button"));
        assertTrue(selenium.isVisible("mtaContactEmail"));
        assertTrue(selenium.isVisible("mtaContactPhone"));
        assertTrue(selenium.isVisible("mtaContactPhoneExtension"));
        assertTrue(selenium.isVisible("mtaContactFax"));

        // Check that save is working
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertTrue(selenium.isTextPresent("Search By Name"));

        // Try to save receiving MTA without contacts (will fail)
        String filePath = new File(ClassLoader.getSystemResource("InstitutionEditTest.sql").toURI()).toString();
        clickAndWait("link=Test Consortium Member 1");
        selenium.check("mta_type2");
        selenium.type("document", filePath);
        clickAndWait("btn_save");
        mtaContactValidation();
        assertTrue(selenium.isChecked("mta_type2"));
        assertFalse(selenium.isChecked("mta_type1"));

        // Save sending MTA
        selenium.check("mta_type1");
        selenium.type("document", filePath);
        enterMtaContact();
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertTrue(selenium.isTextPresent("Search By Name"));
        clickAndWait("link=Test Consortium Member 1");
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]"));
        assertEquals("01/01/2011", selenium.getTable("signedMtaList.0.0"));
        assertEquals("Sending Institution", selenium.getTable("signedMtaList.0.1"));
        assertEquals(new SimpleDateFormat("MM/dd/yyyy").format(new Date()), selenium.getTable("signedMtaList.0.2"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]/td[4]/img"));
        assertEquals("Current", selenium.getTable("signedMtaList.0.3"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]/td[5]/a"));

        // Save receiving MTA
        selenium.check("mta_type2");
        enterMtaContact();
        selenium.type("document", filePath);
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertTrue(selenium.isTextPresent("Search By Name"));
        clickAndWait("link=Test Consortium Member 1");
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]"));
        assertEquals("01/01/2011", selenium.getTable("signedMtaList.0.0"));
        assertEquals("Receiving Institution", selenium.getTable("signedMtaList.0.1"));
        assertEquals(new SimpleDateFormat("MM/dd/yyyy").format(new Date()), selenium.getTable("signedMtaList.0.2"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]/td[4]/img"));
        assertEquals("Current", selenium.getTable("signedMtaList.0.3"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]/td[5]/a"));
        verifyMtaContact();

        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[2]"));
        assertEquals("01/01/2011", selenium.getTable("signedMtaList.1.0"));
        assertEquals("Sending Institution", selenium.getTable("signedMtaList.1.1"));
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_MONTH, -1);
        String mtaDate = selenium.getTable("signedMtaList.1.2");
        assertTrue(mtaDate.equals(new SimpleDateFormat("MM/dd/yyyy").format(new Date()))
                || mtaDate.equals(new SimpleDateFormat("MM/dd/yyyy").format(yesterday.getTime())));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[2]/td[4]/img"));
        assertEquals("Current", selenium.getTable("signedMtaList.1.3"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[2]/td[5]/a"));
        verifyMtaContact();

        selenium.type("mtaContactEmail", "mta_contact");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        selenium.isTextPresent("Email is not a well-formed email address");
        clickAndWait("link=Sign Out");

        goToInstAdminPage("test-consort1-admin@example.com", "Test Consortium Member 1");
        assertTrue(selenium.isTextPresent("test mta contact first test mta contact last"));
        assertTrue(selenium.isTextPresent("line 1"));
        assertTrue(selenium.isTextPresent("line 2"));
        assertTrue(selenium.isTextPresent("city Maryland, zip"));
        assertTrue(selenium.isTextPresent("United States"));
        assertTrue(selenium.isTextPresent("mta_contact@example.com"));
        assertTrue(selenium.isTextPresent("1234567890"));
        assertTrue(selenium.isTextPresent("0987654321"));
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertTrue(selenium.isTextPresent("Search By Name"));
        clickAndWait("link=Sign Out");
    }

    private void editNonConsortiumMemberWithContact() throws Exception {
        goToInstAdminPage(ClientProperties.getConsortiumAdminEmail(), "Test Non-Consortium Member");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertTrue(selenium.isTextPresent("Search By Name"));
        clickAndWait("link=Test Non-Consortium Member");
        assertFalse(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]"));
        assertTrue(StringUtils.isNotBlank(selenium.getValue("mtaContactFirstName")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue("mtaContactLastName")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue("mtaContactLine1")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue("mtaContactLine2")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue("mtaContactCity")));
        assertTrue(StringUtils.isNotBlank(selenium.getSelectedLabel("mtaContactState")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue("mtaContactZip")));
        assertTrue(StringUtils.isNotBlank(selenium.getSelectedLabel("mtaContactCountry")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue("mtaContactEmail")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue("mtaContactPhone")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue("mtaContactPhoneExtension")));

        selenium.type("mtaContactEmail", "mta_contact");
        clickAndWait("btn_save");

        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        selenium.isTextPresent("Email is not a well-formed email address");

        selenium.type("mtaContactEmail", "mta_contact@example.com");
        String filePath = new File(ClassLoader.getSystemResource("InstitutionEditTest.sql").toURI()).toString();
        selenium.type("document", filePath);
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertTrue(selenium.isTextPresent("Search By Name"));
        clickAndWait("link=Test Non-Consortium Member");
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]"));
        assertEquals("01/01/2011", selenium.getTable("signedMtaList.0.0"));
        assertEquals(new SimpleDateFormat("MM/dd/yyyy").format(new Date()), selenium.getTable("signedMtaList.0.1"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]/td[3]/img"));
        assertEquals("Current", selenium.getTable("signedMtaList.0.2"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]/td[4]/a"));
        clickAndWait("link=Sign Out");
    }

    private void editConsortiumMemberWithMtas() {
        goToInstAdminPage(ClientProperties.getConsortiumAdminEmail(), "Test Consortium Member 2");
        clickAndWait("btn_save");
        mtaContactValidation();
        enterMtaContact();
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Institution successfully saved."));
        assertTrue(selenium.isTextPresent("Search By Name"));
        clickAndWait("link=Test Consortium Member 2");
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]"));
        assertEquals("odd current", selenium.getAttribute("xpath=//table[@id='signedMtaList']/tbody/tr[1]@class"));
        assertEquals("hilite current",
                selenium.getAttribute("xpath=//table[@id='signedMtaList']/tbody/tr[1]/td[1]@class"));
        assertEquals("01/01/2011", selenium.getTable("signedMtaList.0.0"));
        assertEquals("Sending Institution", selenium.getTable("signedMtaList.0.1"));
        assertEquals("12/16/2010", selenium.getTable("signedMtaList.0.2"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]/td[4]/img"));
        assertEquals("Current", selenium.getTable("signedMtaList.0.3"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[1]/td[5]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[2]"));
        assertTrue(selenium.getAttribute("xpath=//table[@id='signedMtaList']/tbody/tr[2]@class")
                .matches("\\s*current"));
        assertEquals("hilite current",
                selenium.getAttribute("xpath=//table[@id='signedMtaList']/tbody/tr[2]/td[1]@class"));
        assertEquals("01/01/2011", selenium.getTable("signedMtaList.1.0"));
        assertEquals("Receiving Institution", selenium.getTable("signedMtaList.1.1"));
        assertEquals("12/15/2010", selenium.getTable("signedMtaList.1.2"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[2]/td[4]/img"));
        assertEquals("Current", selenium.getTable("signedMtaList.1.3"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[2]/td[5]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[3]"));
        assertEquals("odd", selenium.getAttribute("xpath=//table[@id='signedMtaList']/tbody/tr[3]@class"));
        assertEquals("pending", selenium.getAttribute("xpath=//table[@id='signedMtaList']/tbody/tr[3]/td[1]@class"));
        assertEquals("01/01/2011", selenium.getTable("signedMtaList.2.0"));
        assertEquals("Receiving Institution", selenium.getTable("signedMtaList.2.1"));
        assertEquals("12/14/2010", selenium.getTable("signedMtaList.2.2"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[3]/td[4]/img"));
        assertEquals("Pending Review", selenium.getTable("signedMtaList.2.3"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[3]/td[5]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[4]"));
        assertEquals("none", selenium.getAttribute("xpath=//table[@id='signedMtaList']/tbody/tr[4]/td[1]@class"));
        assertEquals("01/01/2011", selenium.getTable("signedMtaList.3.0"));
        assertEquals("Receiving Institution", selenium.getTable("signedMtaList.3.1"));
        assertEquals("12/13/2010", selenium.getTable("signedMtaList.3.2"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[4]/td[4]/img"));
        assertEquals("Rejected", selenium.getTable("signedMtaList.3.3"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[4]/td[5]/a"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[5]"));
        assertEquals("odd", selenium.getAttribute("xpath=//table[@id='signedMtaList']/tbody/tr[3]@class"));
        assertEquals("outdated", selenium.getAttribute("xpath=//table[@id='signedMtaList']/tbody/tr[5]/td[1]@class"));
        assertEquals("01/01/2010", selenium.getTable("signedMtaList.4.0"));
        assertEquals("Receiving Institution", selenium.getTable("signedMtaList.4.1"));
        assertEquals("12/11/2010", selenium.getTable("signedMtaList.4.2"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[5]/td[4]/img"));
        assertEquals("Out-of-Date", selenium.getTable("signedMtaList.4.3"));
        assertTrue(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[5]/td[5]/a"));
        assertFalse(selenium.isElementPresent("xpath=//table[@id='signedMtaList']/tbody/tr[6]"));
        verifyMtaContact();
        clickAndWait("link=Sign Out");
    }

    private void goToInstAdminPage(String email, String instName) {
        login(email, PASSWORD);
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Institution Administration");
        clickAndWait("link=" + instName);
        assertEquals(instName, selenium.getValue("name"));
    }

    private void enterMtaContact() {
        selenium.type("mtaContactFirstName", "test mta contact first");
        selenium.type("mtaContactLastName", "test mta contact last");
        selenium.type("mtaContactLine1", "line 1");
        selenium.type("mtaContactLine2", "line 2");
        selenium.type("mtaContactCity", "city");
        selenium.select("mtaContactState", "label=Maryland");
        selenium.type("mtaContactZip", "zip");
        selenium.select("mtaContactCountry", "label=United States");
        selenium.type("mtaContactEmail", "mta_contact@example.com");
        selenium.type("mtaContactPhone", "1234567890");
        selenium.type("mtaContactPhoneExtension", "1234");
        selenium.type("mtaContactFax", "0987654321");
    }

    private void verifyMtaContact() {
        assertEquals("test mta contact first", selenium.getValue("mtaContactFirstName"));
        assertEquals("test mta contact last", selenium.getValue("mtaContactLastName"));
        assertEquals("line 1", selenium.getValue("mtaContactLine1"));
        assertEquals("line 2", selenium.getValue("mtaContactLine2"));
        assertEquals("city", selenium.getValue("mtaContactCity"));
        assertEquals("Maryland", selenium.getSelectedLabel("mtaContactState"));
        assertEquals("zip", selenium.getValue("mtaContactZip"));
        assertEquals("United States", selenium.getSelectedLabel("mtaContactCountry"));
        assertEquals("mta_contact@example.com", selenium.getValue("mtaContactEmail"));
        assertEquals("1234567890", selenium.getValue("mtaContactPhone"));
        assertEquals("1234", selenium.getValue("mtaContactPhoneExtension"));
        assertEquals("0987654321", selenium.getValue("mtaContactFax"));
    }

    private void mtaContactValidation() {
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("First Name must be set"));
        assertTrue(selenium.isTextPresent("Last Name must be set"));
        assertTrue(selenium.isTextPresent("Address must be set"));
        assertTrue(selenium.isTextPresent("City must be set"));
        assertTrue(selenium.isTextPresent("State must be set"));
        assertTrue(selenium.isTextPresent("Zip/Postal Code must be set"));
        assertTrue(selenium.isTextPresent("Email must be set"));
        assertTrue(selenium.isTextPresent("Phone must be set"));
    }
}
