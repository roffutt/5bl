/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;


import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author ddasgupta
 *
 */
public class CartTest extends AbstractTissueLocatorSeleniumTest {

    /**
     * Row XPath.
     */
    private static final String ROW_XPATH = "xpath=//table[@id='lineItem']/tbody/tr[%d]/td[%d]/a";
    private static final String PROSPECTIVE_ROW_XPATH =
        "xpath=//table[@id='prospectiveCollection']/tbody/tr[%d]/td[%d]/a";
    private static final int BUTTON_COL_NO_PRICE_INDEX = 5;
    private static final int BUTTON_COL_WITH_PRICE_INDEX = 6;
    private static final String PROSPECTIVE_NOTES = "50 Breast Cancer specimens";
    private static final String PROSPECTIVE_NOTES_UPDATED = "100 Breast Cancer specimens";
    private String specimenId;

    /**
     * ID One.
     */
    private static final String ID_ONE = "test 10";

    /**
     * ID Two.
     */
    private static final String ID_TWO = "test 15";

    /**
     * ID Three.
     */
    private static final String ID_THREE = "test 20";

    /**
     * Cart size.
     */
    private static final int CART_SIZE = 3;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "CartTest.sql";
    }

    /**
     * test the cart feature.
     */
    @Test
    public void testCart() {
        if (ClientProperties.isAggregateSearchResultsActive()) {
            return;
        }

        loginAsUser1();
        homePageMessage(0);
        addFromView();
        homePageMessage(1);
        addFromSearch();
        homePageMessage(CART_SIZE);
        addDuplicate();
        remove();
        empty();
        sessionStorage();
        if (ClientProperties.getDisplayProspectiveCollection()) {
            addProspectiveCollection();
            editProspectiveCollection();
            removeProspectiveCollection();
            emptyProspectiveCollection();
        }

        verifySinglePrice();
    }

    /**
     * Home page messages.
     * @param cartSize the cart size
     */
    private void homePageMessage(int cartSize) {
        clickAndWait("link=Home");
        if (cartSize == 1) {
            assertTrue(selenium.isElementPresent("link=(1 Item)"));
        } else {
            assertTrue(selenium.isElementPresent("link=(" + cartSize + " Items)"));
        }
    }

    /**
     * Verify cart contents after adding specimens from a search.
     */
    private void addFromSearch() {
        selectSpecimensFromSearch();
        verifyCart("$75.00", "$175.00", ID_ONE, ID_TWO, ID_THREE);
    }

    /**
     * Add specimens from a search to the cart.
     */
    private void selectSpecimensFromSearch() {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        selenium.type("min_collectionYear", "1905");
        selenium.type("max_collectionYear", "1915");
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        selenium.click("selectall");
        clickAndWait("btn_addtocart");
        assertTrue(selenium.isTextPresent("Added 2 " + ClientProperties.getSpecimenTermLowerCase()
                + "(s) to your " + getCartLabel() + "."));
    }

    /**
     * Add from view.
     */
    private void addFromView() {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        selenium.type("min_collectionYear", "1920");
        selenium.type("max_collectionYear", "1930");
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        specimenId = selenium.getText("xpath=//table[@id='specimen']/tbody/tr[1]/td[2]/a");
        String expectedHeader = selenium.getTable("specimen.1.2").replaceAll("\\s+", "");
        clickAndWait("xpath=//table[@id='specimen']/tbody/tr[1]/td[2]/a");
        String actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
        assertFalse(selenium.isTextPresent("test 20"));
        clickAndWait("add_to_cart");
        assertTrue(selenium.isTextPresent("Added 1 " + ClientProperties.getSpecimenTermLowerCase()
                + "(s) to your " + getCartLabel() + "."));
        verifyCart("$25.00", "$75.00", ID_THREE);
    }

    /**
     * Add duplicate.
     */
    private void addDuplicate() {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        selenium.type("min_collectionYear", "");
        selenium.type("max_collectionYear", "");
        selenium.type("id", specimenId);
        clickAndWait("btn_search");
        selenium.click("selectall");
        clickAndWait("btn_addtocart");
        assertTrue(selenium.isTextPresent("Added 0 " + ClientProperties.getSpecimenTermLowerCase()
                + "(s) to your " + getCartLabel() + "."));
        verifyCart("$75.00", "$175.00", ID_ONE, ID_TWO, ID_THREE);
        String[] diseases = ClientProperties.getDiseases();
        assertTrue(selenium.isTextPresent(diseases[0]));
        assertTrue(selenium.isTextPresent(diseases[1]));
        assertTrue(selenium.isTextPresent(diseases[2]));
    }

    /**
     * Remove from cart.
     */
    private void remove() {
        clickAndWait(String.format(ROW_XPATH, 2, getButtonColIndex()));
        assertTrue(selenium.isTextPresent("Removed 1 " + ClientProperties.getSpecimenTermLowerCase()
                + " from your " + getCartLabel() + "."));
        verifyCart("$25.00", "$75.00", ID_ONE, ID_THREE);
    }

    /**
     * Empty cart.
     */
    private void empty() {
        clickAndWait("link=Empty " + ClientProperties.getCartLink());
        assertTrue(selenium.isTextPresent("Removed 2 " + ClientProperties.getSpecimenTermLowerCase()
                + "(s) from your " + getCartLabel() + "."));
        verifyCart("$0.00", "$0.00");
        assertTrue(selenium.isTextPresent("No " + ClientProperties.getBiospecimenTerm() + "s Selected."));
        clickAndWait("link=Empty " + ClientProperties.getCartLink());
        assertTrue(selenium.isTextPresent("Removed 0 " + ClientProperties.getSpecimenTermLowerCase()
                + "(s) from your " + getCartLabel() + "."));
        verifyCart("$0.00", "$0.00");
        assertTrue(selenium.isTextPresent("No " + ClientProperties.getBiospecimenTerm() + "s Selected."));
    }

    /**
     * Session storage.
     */
    private void sessionStorage() {
        addFromView();
        clickAndWait("link=Home");
        clickAndWait("link=" + ClientProperties.getCartLink());
        verifyCart("$25.00", "$75.00", ID_THREE);
        clickAndWait("link=Sign Out");
        loginAsUser1();
        clickAndWait("link=" + ClientProperties.getCartLink());
        verifyCart("$0.00", "$0.00");
    }

    /**
     * Verify prospective collection  and dual collection/lineItem cart functionality.
     */
    private void addProspectiveCollection() {
        clickAndWait("link=Sign Out");
        loginAsUser1();
        clickAndWait("link=" + ClientProperties.getProspectiveCollectionLink());
        assertTrue(selenium.isTextPresent(ClientProperties.getProspectiveCollectionNotes()));
        selenium.type("prospectiveCollectionNotes", PROSPECTIVE_NOTES);
        clickAndWait("btn_addtocart");
        assertTrue(selenium.isTextPresent(ClientProperties.getProspectiveCollectionNotes()));
        assertTrue(selenium.isTextPresent(PROSPECTIVE_NOTES));
        selectSpecimensFromSearch();
        assertTrue(selenium.isTextPresent(ClientProperties.getProspectiveCollectionNotes()));
        assertTrue(selenium.isTextPresent(PROSPECTIVE_NOTES));
    }

    private void editProspectiveCollection() {
        if (ClientProperties.getDisplayProspectiveCollectionTable()) {
            clickAndWait(String.format(PROSPECTIVE_ROW_XPATH, 1, 2));
            assertTrue(selenium.isTextPresent(ClientProperties.getProspectiveCollectionNotes()));
            assertTrue(selenium.isTextPresent(PROSPECTIVE_NOTES));
            selenium.type("prospectiveCollectionNotes", PROSPECTIVE_NOTES_UPDATED);
            clickAndWait("btn_addtocart");
            assertTrue(selenium.isTextPresent(PROSPECTIVE_NOTES_UPDATED));
        } else {
            selenium.type("prospectiveCollectionNotes", PROSPECTIVE_NOTES_UPDATED);
            clickAndWait("btn_update");
            assertTrue(selenium.isTextPresent(PROSPECTIVE_NOTES_UPDATED));
        }
    }

    private void removeProspectiveCollection() {
        if (ClientProperties.getDisplayProspectiveCollectionTable()) {
            // CHECKSTYLE:OFF - magic number
            clickAndWait(String.format(PROSPECTIVE_ROW_XPATH, 1, 3));
            // CHECKSTYLE:ON
            assertTrue(selenium.isTextPresent("Removed the "
                    + ClientProperties.getProspectiveCollectionTerm().toLowerCase() + " from your "
                    + getCartLabel()));
            assertFalse(selenium.isElementPresent("prospectiveCollection"));
        }
    }

    private void emptyProspectiveCollection() {
        if (ClientProperties.getDisplayProspectiveCollectionTable()) {
            addProspectiveCollection();
            clickAndWait("link=Empty " + ClientProperties.getCartLink());
            assertTrue(selenium.isTextPresent("Removed 2 " + ClientProperties.getSpecimenTermLowerCase()
                    + "(s) from your " + getCartLabel() + "."));
            verifyCart("$0.00", "$0.00");
            assertTrue(selenium.isTextPresent("No " + ClientProperties.getBiospecimenTerm() + "s Selected."));
            assertTrue(selenium.isTextPresent("Removed the "
                    + ClientProperties.getProspectiveCollectionTerm().toLowerCase() + " from your "
                    + getCartLabel()));
            assertFalse(selenium.isElementPresent("prospectiveCollection"));
        }
    }

    /**
     * Verify the cart.
     * @param minSubtotal the minSubtotal
     * @param maxSubtotal the maxSubtotal
     * @param ids the ids
     */
    private void verifyCart(String minSubtotal, String maxSubtotal, String... ids) {
        assertTrue(selenium.isTextPresent(ClientProperties.getCartLink()));
        for (String id : ids) {
            assertFalse(selenium.isTextPresent(id));
        }
        for (int i = 1; i <= ids.length; i++) {
            assertTrue(selenium.isElementPresent(String.format(ROW_XPATH, i, getButtonColIndex())));
        }
        assertFalse(selenium.isElementPresent(String.format(ROW_XPATH, ids.length + 1, getButtonColIndex())));
        int footerIndex = ids.length + 1;
        if (ids.length == 0) {
            footerIndex++;
        }
        String subtotalText = selenium.getTable("lineItem." + footerIndex + ".2");
        assertEquals(ClientProperties.isDisplayPrices(), subtotalText.contains(minSubtotal));
        if (!minSubtotal.equals(maxSubtotal)) {
            assertEquals(ClientProperties.isDisplayPrices(), subtotalText.contains(maxSubtotal));
        }
    }

    /**
     * Verifies display of single specimen price (when minimum and maximum price are equal).
     */
    protected void verifySinglePrice() {
        String priceRange = "$20.00 - $20.00";
        String listPrice  = "$20.00";

        if (ClientProperties.isDisplayPrices()) {
            clickAndWait("link=Sign Out");
            loginAsAdmin();
            mouseOverAndPause("link=Administration");
            clickAndWait("link=" + ClientProperties.getBiospecimenAdministrationLink());
            selenium.type("externalId", "");
            clickAndWait("btn_search");

            String biospecimenId = StringUtils.EMPTY;
            int rowCount = selenium.getXpathCount("//table[@id='specimen']/tbody/tr").intValue();
            for (int i = 1; i <= rowCount && StringUtils.isBlank(biospecimenId); i++) {
                String specimenStatus = selenium.getTable("specimen." + i + ".8");
                if (specimenStatus.equalsIgnoreCase("Available")) {
                    biospecimenId = selenium.getTable("specimen." + i + ".0");
                    clickAndWait("//table[@id='specimen']/tbody/tr[" + i + "]/td[10]/a");
                }
            }

            if (selenium.isChecked("id=priceNegotiable")) {
                selenium.uncheck("//input[@id='priceNegotiable']");
            }

            selenium.type("minimumPrice", "20.00");
            selenium.type("maximumPrice", "20.00");
            clickAndWait("btn_save");

            selectSearchMenu();
            clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
            selenium.type("id", biospecimenId);
            clickAndWait("btn_search");
            selenium.click("selectall");
            clickAndWait("btn_addtocart");

            String fee = selenium.getText("//table[@id='lineItem']/tbody/tr[1]/td[4]");
            assertFalse(fee.equals(priceRange));
            assertEquals(listPrice, fee);
            String subtotal = selenium.getText("//table[@id='lineItem']/tfoot/tr[1]/td[3]");
            assertFalse(subtotal.equals(priceRange));
            assertEquals(listPrice, subtotal);
            clickAndWait("link=Empty " + ClientProperties.getCartLink());
        }
    }

    private String getCartLabel() {
        String[] cartLabelParts = StringUtils.split(ClientProperties.getCartLink());
        return cartLabelParts[cartLabelParts.length - 1].toLowerCase();
    }

    private int getButtonColIndex() {
        if (ClientProperties.isDisplayPrices()) {
            return BUTTON_COL_WITH_PRICE_INDEX;
        }
        return BUTTON_COL_NO_PRICE_INDEX;
    }
}
