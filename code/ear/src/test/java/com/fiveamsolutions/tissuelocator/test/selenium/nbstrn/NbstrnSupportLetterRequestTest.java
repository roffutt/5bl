/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author ddasgupta
 *
 */
public class NbstrnSupportLetterRequestTest extends AbstractTissueLocatorSeleniumTest {

    private static final String REVIEW_BUTTON_XPATH = "xpath=//table[@id='supportLetterRequest']/tbody/tr[1]/td[8]/a";
    private static final String REVIEW_PAGE_TITLE = "Review Request for Letter of Support";
    private static final String VIEW_LINK_XPATH = "xpath=//table[@id='supportLetterRequest']/tbody/tr[%d]/td[1]/a";
    private static final int GRANT_REQUEST_INDEX = 1;
    private static final int IRB_REQUEST_INDEX = 2;
    private static final int OTHER_REQUEST_INDEX = 3;
    private static final String BACK_BUTTON_XPATH = "xpath=//div[@class='topbtns']/a";

    /**
     * Tests submission of requests for letters of support.
     * @throws Exception on error
     */
    @Test
    public void testSupportLetterRequest() throws Exception {
        goToSupportLetterRequestPage();
        requiredValidation();
        submissionDateValidation();
        typeTabs();
        typeValidation();
        copyRequestor();
        createGrantRequest();
        createIRBRequest();
        createOtherRequest();

        goToReviewPage();
        reviewValidation();
        reviewOnline();
        reviewOffline();
        investigatorView();
        createFromDefault();
    }

    private void goToSupportLetterRequestPage() {
        loginAsUser1();
        mouseOverAndPause("link=Research Support");
        clickAndWait("link=Request VRDBS Letter of Support");
        assertTrue(selenium.isTextPresent("Request VRDBS Letter of Support"));
        assertTrue(selenium.isTextPresent("Why request a letter of support?"));
        assertTrue(selenium.isTextPresent("How long will it take to get my letter of support?"));
        clickAndWait("xpath=//div[@id='secnav']/div/ul/li[3]/a");
        assertTrue(selenium.isTextPresent("Request VRDBS Letter of Support"));
        assertTrue(selenium.isTextPresent("Why request a letter of support?"));
        assertTrue(selenium.isTextPresent("How long will it take to get my letter of support?"));
        String helpText = "We encourage you to provide a draft letter of support to assist the NBSTRN in "
            + "this process. ";
        verifyHelpText("aid.draftLetter", helpText, null, null);
    }

    private void requiredValidation() {
        clearInvestigatorFields();
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("First Name must be set"));
        assertTrue(selenium.isTextPresent("Last Name must be set"));
        assertTrue(selenium.isTextPresent("You must select a valid organization."));
        assertTrue(selenium.isTextPresent("Address must be set"));
        assertTrue(selenium.isTextPresent("City must be set"));
        assertTrue(selenium.isTextPresent("State must be set"));
        assertTrue(selenium.isTextPresent("Zip/Postal Code must be set"));
        assertTrue(selenium.isTextPresent("Email must be set"));
        assertTrue(selenium.isTextPresent("Phone must be set"));
        assertTrue(selenium.isTextPresent("Letter of Support Type must be set"));
        assertTrue(selenium.isTextPresent("Draft Protocol must be set"));
    }

    private void submissionDateValidation() {
        selenium.type("intendedSubmissionDate", "10/10/2009");
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Intended Submission Date must be a future date"));
        String helpText = "Date letter is needed for institution or for submission to funding source.";
        verifyHelpText("aid.intendedSubmissionDate", helpText, null, null);
    }

    private void typeTabs() {
        verifyTypeFieldVisibility(false, false);

        selenium.click("letterType_GRANT");
        verifyTypeFieldVisibility(true, false);
        enterGrantTypeFields();
        selenium.click("letterType_IRB");
        verifyTypeFieldVisibility(false, false);
        selenium.click("letterType_GRANT");
        verifyTypeFieldVisibility(true, false);
        verifyGrantTypeFieldsCleared();
        enterGrantTypeFields();
        selenium.click("letterType_OTHER");
        verifyTypeFieldVisibility(false, true);
        selenium.click("letterType_GRANT");
        verifyTypeFieldVisibility(true, false);
        verifyGrantTypeFieldsCleared();

        selenium.click("letterType_OTHER");
        verifyTypeFieldVisibility(false, true);
        enterOtherTypeFields();
        selenium.click("letterType_IRB");
        verifyTypeFieldVisibility(false, false);
        selenium.click("letterType_OTHER");
        verifyTypeFieldVisibility(false, true);
        verifyOtherTypeFieldsCleared();
        enterOtherTypeFields();
        selenium.click("letterType_GRANT");
        verifyTypeFieldVisibility(true, false);
        selenium.click("letterType_OTHER");
        verifyTypeFieldVisibility(false, true);
        verifyOtherTypeFieldsCleared();
    }

    private void verifyTypeFieldVisibility(boolean grantFieldsShown, boolean otherFieldsShown) {
        pause(SHORT_DELAY);
        assertEquals(grantFieldsShown, selenium.isVisible("grantType"));
        assertEquals(grantFieldsShown, selenium.isVisible("//div[@id='wwgrp_fundingSource']/button"));
        assertEquals(otherFieldsShown, selenium.isVisible("otherType"));
    }

    private void enterGrantTypeFields() {
        selenium.type("grantType", "grant type");
        selenium.select("object.fundingSources", "label=Foundation (not peer reviewed)");
        selenium.addSelection("object.fundingSources", "label=Other");
    }

    private void verifyGrantTypeFieldsCleared() {
        assertTrue(StringUtils.isBlank(selenium.getValue("grantType")));
        assertTrue(StringUtils.isBlank(selenium.getValue("object.fundingSources")));
        assertEquals("- Select -", selenium.getText("//div[@id='wwgrp_fundingSource']/button"));
    }

    private void enterOtherTypeFields() {
        selenium.type("otherType", "other type");
    }

    private void verifyOtherTypeFieldsCleared() {
        assertTrue(StringUtils.isBlank(selenium.getValue("otherType")));
    }

    private void typeValidation() {
        selenium.click("letterType_GRANT");
        pause(SHORT_DELAY);
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertFalse(selenium.isTextPresent("Letter of Support Type must be set"));
        assertTrue(selenium.isTextPresent("Type of Grant must be set."));
        assertTrue(selenium.isTextPresent("Funding Source must be set."));
        assertFalse(selenium.isTextPresent("Other Value must be set."));
        verifyTypeFieldVisibility(true, false);

        selenium.click("letterType_IRB");
        pause(SHORT_DELAY);
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertFalse(selenium.isTextPresent("Letter of Support Type must be set"));
        assertFalse(selenium.isTextPresent("Type of Grant must be set."));
        assertFalse(selenium.isTextPresent("Funding Source must be set."));
        assertFalse(selenium.isTextPresent("Other Value must be set."));
        verifyTypeFieldVisibility(false, false);

        selenium.click("letterType_OTHER");
        pause(SHORT_DELAY);
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertFalse(selenium.isTextPresent("Letter of Support Type must be set"));
        assertFalse(selenium.isTextPresent("Type of Grant must be set."));
        assertFalse(selenium.isTextPresent("Funding Source must be set."));
        assertTrue(selenium.isTextPresent("Other Value must be set."));
        verifyTypeFieldVisibility(false, true);
    }

    private void copyRequestor() {
        clearInvestigatorFields();
        selenium.click("changeresume");
        assertTrue(selenium.isVisible("newresume"));
        assertFalse(selenium.isVisible("existingresume"));
        selenium.type("resume", "invalid file name");
        selenium.click("copyinvestigator");
        pause(DELAY);
        selenium.click("copyinvestigator");
        pause(DELAY);
        String[] nameParts = StringUtils.split(ClientProperties.getResearcherName());
        assertEquals(nameParts[0], selenium.getValue("investigatorFirstName"));
        assertEquals(nameParts[1], selenium.getValue("investigatorLastName"));
        assertEquals("123 Main Street", selenium.getValue("investigatorLine1"));
        assertEquals("", selenium.getValue("investigatorLine2"));
        assertEquals("Anytown", selenium.getValue("investigatorCity"));
        assertEquals("Maryland", selenium.getSelectedLabel("investigatorState"));
        assertEquals("Maryland", selenium.getText("//div[@id='wwgrp_investigatorState']/button"));
        assertEquals("12345", selenium.getValue("investigatorZip"));
        assertEquals("United States", selenium.getSelectedLabel("investigatorCountry"));
        assertEquals("United States", selenium.getText("//div[@id='wwgrp_investigatorCountry']/button"));
        assertEquals(ClientProperties.getResearcherEmail(), selenium.getValue("investigatorEmail"));
        assertEquals("1234567890", selenium.getValue("investigatorPhone"));
        assertEquals(ClientProperties.getResearcherInstitution(), selenium.getValue("investigatorOrganization"));
        assertFalse(selenium.isVisible("newresume"));
        assertTrue(selenium.isVisible("existingresume"));
        assertTrue(StringUtils.isBlank(selenium.getValue("resume")));
    }

    private void createGrantRequest() throws Exception {
        clickAndWait("xpath=//div[@id='secnav']/div/ul/li[3]/a");
        enterGeneralData();
        selenium.click("letterType_GRANT");
        pause(SHORT_DELAY);
        enterGrantTypeFields();
        String helpText = "Please specify the type of grant that has been awarded. Some examples include: "
            + "Research Grants (R series), Career Development Awards (K series), Research Training and "
            + "Fellowships (T & F series), Program Project/Center Grants (P series), Resource Grants "
            + "(various series), and Trans-NIH Programs";
        String[] links = new String[] {"http://grants.nih.gov/grants/funding/ac_search_results.htm"};
        verifyHelpText("aid.grantType", helpText, links, links);
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent("Request for Letter of Support successfully submitted."));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        verifyViewPage(GRANT_REQUEST_INDEX, "Pending", "Grant", "Type of Grant", "grant type",
                "Funding Source", "Foundation (not peer reviewed), Other");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
    }

    private void createIRBRequest() throws Exception {
        clickAndWait("link=Request Letter of Support");
        enterGeneralData();
        selenium.click("letterType_IRB");
        pause(SHORT_DELAY);
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent("Request for Letter of Support successfully submitted."));
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        verifyViewPage(IRB_REQUEST_INDEX, "Pending", "IRB");
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
    }

    private void createOtherRequest() throws Exception {
        clickAndWait("link=Request Letter of Support");
        enterGeneralData();
        selenium.click("letterType_OTHER");
        pause(SHORT_DELAY);
        enterOtherTypeFields();
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent("Request for Letter of Support successfully submitted."));
        assertTrue(selenium.isTextPresent("1-3 of 3 Results"));
        verifyViewPage(OTHER_REQUEST_INDEX, "Pending", "Other", "Other Type", "other type");
        assertTrue(selenium.isTextPresent("1-3 of 3 Results"));
        clickAndWait("link=Sign Out");
    }

    private void enterGeneralData() throws Exception {
        clearInvestigatorFields();
        selenium.type("investigatorFirstName", "test-first-name-investigator");
        selenium.type("investigatorLastName", "test-last-name-investigator");
        selenium.type("investigatorLine1", "test-line1-investigator");
        selenium.type("investigatorLine2", "test-line2-investigator");
        selenium.type("investigatorCity", "test-city-investigator");
        selenium.select("investigatorState", "label=Virginia");
        selenium.type("investigatorZip", "13579");
        selenium.select("investigatorCountry", "label=Canada");
        selenium.type("investigatorEmail", "test-email-investigator@email.com");
        selenium.type("investigatorPhone", "1234567890");
        selenium.type("investigatorPhoneExtension", "24680");
        String name = ClientProperties.getDefaultConsortiumMemberName();
        selectFromAutocomplete("investigatorOrganization", StringUtils.substringBefore(name, "'").toLowerCase(), name);

        String filePath = new File(ClassLoader.getSystemResource("test.properties").toURI()).toString();
        selenium.type("resume", filePath);
        filePath = new File(ClassLoader.getSystemResource("testui.properties.example").toURI()).toString();
        selenium.type("draftProtocol", filePath);
        filePath = new File(ClassLoader.getSystemResource("approval.txt").toURI()).toString();
        selenium.type("draftLetter", filePath);
        selenium.type("comments", "test support letter request comments");

        String future = new SimpleDateFormat("MM/dd/yyyy").format(DateUtils.addYears(new Date(), 1));
        selenium.type("intendedSubmissionDate", future);
    }

    private void verifyViewPage(int linkIndex, String... additionalText) {
        clickAndWait(String.format(VIEW_LINK_XPATH, linkIndex));

        assertTrue(selenium.isTextPresent("Letter of Support Request"));
        assertTrue(selenium.isTextPresent("Letter Request"));
        assertTrue(selenium.isTextPresent("From Investigator"));
        assertTrue(selenium.isTextPresent("test-first-name-investigator test-last-name-investigator"));
        assertTrue(selenium.isTextPresent("California"));
        assertTrue(selenium.isTextPresent("test-email-investigator@email.com"));
        assertTrue(selenium.isTextPresent("1234567890"));
        assertTrue(selenium.isTextPresent("ext."));
        assertTrue(selenium.isTextPresent("24680"));
        assertTrue(selenium.isTextPresent("test-line1-investigator"));
        assertTrue(selenium.isTextPresent("test-line2-investigator"));
        assertTrue(selenium.isTextPresent("test-city-investigator"));
        assertTrue(selenium.isTextPresent("Virginia,"));
        assertTrue(selenium.isTextPresent("13579"));
        assertTrue(selenium.isTextPresent("Canada"));
        assertTrue(selenium.isTextPresent("Resume/CV/Biosketch"));
        assertTrue(selenium.isTextPresent("test.properties"));
        assertTrue(selenium.isElementPresent("link=test.properties"));

        assertTrue(selenium.isTextPresent("Letter of Support Details"));
        assertTrue(selenium.isTextPresent("Letter of Support Type"));
        for (String text : additionalText) {
            assertTrue(selenium.isTextPresent(text));
        }

        assertTrue(selenium.isTextPresent("Draft Protocol"));
        assertTrue(selenium.isTextPresent("testui.properties.example"));
        assertTrue(selenium.isElementPresent("link=testui.properties.example"));
        assertTrue(selenium.isTextPresent("Draft Letter"));
        assertTrue(selenium.isTextPresent("approval.txt"));
        assertTrue(selenium.isElementPresent("link=approval.txt"));
        String future = new SimpleDateFormat("MM/dd/yyyy").format(DateUtils.addYears(new Date(), 1));
        assertTrue(selenium.isTextPresent("Intended Submission Date"));
        assertTrue(selenium.isTextPresent(future));
        assertTrue(selenium.isTextPresent("Comments"));
        assertTrue(selenium.isTextPresent("test support letter request comments"));

        assertTrue(selenium.isTextPresent("Response"));
        assertTrue(selenium.isTextPresent("Response Details"));
        assertTrue(selenium.isTextPresent("Status"));

        clickAndWait(BACK_BUTTON_XPATH);
    }

    private void goToReviewPage() {
        loginAsAdmin();
        assertTrue(selenium.isTextPresent("3 Letter of Support Requests"));
        clickAndWait("link=3 Letter of Support Requests");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Letters of Support"));
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-3 of 3 Results"));
        clickAndWait(REVIEW_BUTTON_XPATH);
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        clickAndWait(BACK_BUTTON_XPATH);
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Letters of Support"));
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-3 of 3 Results"));
        verifyViewPage(1, "Pending", "Grant", "Type of Grant", "grant type",
                "Funding Source", "Foundation (not peer reviewed), Other");
        clickAndWait(REVIEW_BUTTON_XPATH);
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
    }

    private void reviewValidation() {
        clickAndWait("btn_submit_online");
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Note to Investigator must be set"));
        assertTrue(selenium.isTextPresent("Letter of Support must be set"));
        assertFalse(selenium.isTextPresent("Comments must be set"));
        clickAndWait("btn_submit_offline");
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertFalse(selenium.isTextPresent("Note to Investigator must be set"));
        assertFalse(selenium.isTextPresent("Letter of Support must be set"));
        assertTrue(selenium.isTextPresent("Comments must be set"));
    }

    private void reviewOnline() throws Exception {
        selenium.type("response", "online response");
        String filePath = new File(ClassLoader.getSystemResource("exemption.txt").toURI()).toString();
        selenium.type("letter", filePath);
        clickAndWait("btn_submit_online");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Letters of Support"));
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        assertTrue(selenium.isTextPresent("Request for Letter of Support successfully reviewed."));
        selenium.select("status", "Responded");
        waitForPageToLoad();
        verifyViewPage(1, "Responded", "Grant", "Type of Grant", "grant type", "Funding Source",
                "Foundation (not peer reviewed), Other", "Letter of Support", "exemption.txt",
                "Note to Investigator", "online response");
    }

    private void reviewOffline() {
        selenium.select("status", "Pending");
        waitForPageToLoad();
        verifyViewPage(1, "IRB");
        clickAndWait(REVIEW_BUTTON_XPATH);
        assertTrue(selenium.isTextPresent(REVIEW_PAGE_TITLE));
        selenium.type("offlineResponseNotes", "offline response");
        clickAndWait("btn_submit_offline");
        assertTrue(selenium.isTextPresent("Requests"));
        assertTrue(selenium.isTextPresent("Letters of Support"));
        assertEquals("Pending", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        assertTrue(selenium.isTextPresent("Request for Letter of Support successfully reviewed."));
        selenium.select("status", "Responded Off-line");
        waitForPageToLoad();
        verifyViewPage(1, "Responded Off-line", "IRB", "Comments", "offline response");
        clickAndWait("link=Sign Out");
    }

    private void investigatorView() {
        loginAsUser1();
        clickAndWait("link=My Requests");
        assertTrue(selenium.isTextPresent("My Requests"));
        assertTrue(selenium.isTextPresent("DBS Specimens"));
        clickAndWait("link=Letters of Support");
        assertTrue(selenium.isTextPresent("Letters of Support"));
        assertTrue(selenium.isTextPresent("1-3 of 3 Results"));
        verifyViewPage(GRANT_REQUEST_INDEX, "Received", "Grant", "Type of Grant", "grant type", "Funding Source",
                "Foundation (not peer reviewed), Other", "Letter of Support", "test.properties",
                "Note to Investigator", "online response");
        verifyViewPage(IRB_REQUEST_INDEX, "Received Off-line", "IRB", "Comments", "offline response");
        verifyViewPage(OTHER_REQUEST_INDEX, "Pending", "Other", "Other Type", "other type");
    }

    private void clearInvestigatorFields() {
        selenium.type("investigatorFirstName", "");
        selenium.type("investigatorLastName", "");
        selenium.type("investigatorLine1", "");
        selenium.type("investigatorLine2", "");
        selenium.type("investigatorCity", "");
        selenium.select("investigatorState", "label=- Select -");
        selenium.type("investigatorZip", "");
        selenium.select("investigatorCountry", "label=United States");
        selenium.type("investigatorEmail", "");
        selenium.type("investigatorPhone", "");
        selenium.type("investigatorPhoneExtension", "");
        selenium.type("investigatorOrganization", "");
        selenium.type("resume", "");
    }

    private void createFromDefault() throws Exception {
        mouseOverAndPause("link=Research Support");
        clickAndWait("link=Request VRDBS Letter of Support");
        assertTrue(selenium.isTextPresent("Request VRDBS Letter of Support"));
        selenium.type("investigatorLastName", "test-last-name-investigator");
        String filePath = new File(ClassLoader.getSystemResource("testui.properties.example").toURI()).toString();
        selenium.type("draftProtocol", filePath);
        filePath = new File(ClassLoader.getSystemResource("approval.txt").toURI()).toString();
        selenium.type("draftLetter", filePath);
        selenium.type("comments", "test support letter request comments");
        selenium.click("letterType_IRB");
        pause(SHORT_DELAY);
        clickAndWait("btn_submit");
        assertTrue(selenium.isTextPresent("Request for Letter of Support successfully submitted."));
        assertTrue(selenium.isTextPresent("1-4 of 4 Results"));
    }
}
