package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractSpecimenLocationTest;

/**
 * @author aswift
 *
 */
public class AbrcSpecimenLocationTest extends AbstractSpecimenLocationTest {

    /**
     * test the abrc specimen location.
     * @throws Exception on error
     */
    public void testAbrcSpecimenLocation() throws Exception {

        /*
         * Log in as a standard researcher / user Find Specimen through My Requests page Test 1: Specimen in a Pending
         * Request Test 2: Specimen in a Declined Request Test 3: Specimen in an Approved Request
         */
        loginAsUser1();
        goToMyRequestsPage();
        clickTableCellLink("specimenRequest", 1, 1);
        clickTableCellLink("request", 1, 1);
        checkDiv(false);
        goToMyRequestsPage();
        clickTableCellLink("specimenRequest", THREE, 1);
        clickTableCellLink("request", 1, 1);
        checkDiv(false);
        goToMyRequestsPage();
        clickTableCellLink("specimenRequest", FOUR, 1);
        clickTableCellLink("request", 1, 1);
        checkDiv(true);
        clickAndWait("link=Sign Out");

        /*
         * Log in as a standard tissue technician Find Specimen through Biospecimen Administration page
         */
        login(ClientProperties.getTissueTechEmail(), PASSWORD);
        validateBioSpecimenAdminRow(1, true, true);
        validateBioSpecimenAdminRow(THREE, true, true);
        validateBioSpecimenAdminRow(FOUR, true, true);
        clickAndWait("link=Sign Out");

        /*
         * Log in as an admin Find Specimen through Biospecimen Administration page
         */
        loginAsAdmin();
        validateBioSpecimenAdminRow(1, true, true);
        validateBioSpecimenAdminRow(THREE, true, true);
        validateBioSpecimenAdminRow(FOUR, true, true);
        clickAndWait("link=Sign Out");
    }
}
