/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;

/**
 * @author ddasgupta
 *
 */
public abstract class AbstractUserRegistrationTest extends AbstractTissueLocatorSeleniumTest {

    private static final String REGISTER_LINK = "link=Register";
    private static final String REGISTER_BUTTON = "btn_register";
    private static final int PAGE_SIZE = 20;
    private static final int NEW_USER_COUNT = 4;
    private static int userPrefix = -1;

    private static final int RESEARCHER_GROUP_ID = 1;
    private static final int TECHNICIAN_GROUP_ID = 2;
    private static final int SCIENTIFIC_REVIEW_GROUP_ID = 3;
    private static final int INST_ADMIN_GROUP_ID = 4;
    private static final int CONSORT_ADMIN_GROUP_ID = 5;
    private static final int SYS_ADMIN_GROUP_ID = 6;

    /**
     * Tests user registration.
     * @throws Exception on error
     */
    @Test
    public void testUserRegistration() throws Exception {
        goToRegisterPage();
        requiredValidation();
        matchingValidation();
        createWithNewInstitution(++userPrefix, "testInstitution", "Academic");
        clickAndWait(REGISTER_LINK);
        createWithNewInstitution(++userPrefix, "testInstitution1", "Non-profit");
        createWithExistingInstitution();
        duplicateValidation();
        cancelLink();
        updateUser();
        updatePassword();
        updateInstitution();
        administratableGroups();
        addNewUser();
        verifyNewUser();
        consortiumAdminCreate();
        deactivateUserAccount();
    }

    /**
     * Test concurrent institution auto-creation.
     * @throws Exception on error.
     */
    @Test
    public void testClashingInstitutions() throws Exception {
        goToRegisterPage();
        enterData(0);
        selenium.typeKeys("institution", "clashing institutions test");
        selenium.select("institutionType", "label=" + "Academic");
        runSqlScript("ClashingInstitutionsTest.sql");
        clickRegister(true);
        validateThankYouText();
    }

    /**
     * Test concurrent institution auto-creation with different institution types.
     * @throws Exception on error.
     */
    @Test
    public void testClashingInstitutionsDifferentTypes() throws Exception {
        goToRegisterPage();
        enterData(0);
        selenium.typeKeys("institution", "clashing institutions test");
        selenium.select("institutionType", "label=" + "Non-profit");
        runSqlScript("ClashingInstitutionsTest.sql");
        clickRegister(true);
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("An institution with this name already exists."));
    }

    /**
     * Test the case where an institution is created after the user
     * loads the registration page but before the user selects the institution.
     * @throws Exception on error.
     */
    @Test
    public void testNewInstitutionSelection() throws Exception {
        goToRegisterPage();
        enterData(0);
        runSqlScript("ClashingInstitutionsTest.sql");
        selectFromAutocomplete("institution", "test", "clashing institutions test");
        clickRegister(true);
        validateThankYouText();
    }

    private void goToRegisterPage() {
        selenium.open(CONTEXT_ROOT);
        assertTrue(selenium.isElementPresent(REGISTER_LINK));
        clickAndWait(REGISTER_LINK);
        validateText(true, false);
    }

    private void requiredValidation() {
        clickRegister(true);
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("First Name must be set"));
        assertTrue(selenium.isTextPresent("Last Name must be set"));
        assertTrue(selenium.isTextPresent("Address must be set"));
        assertTrue(selenium.isTextPresent("City must be set"));
        assertTrue(selenium.isTextPresent("State must be set"));
        assertTrue(selenium.isTextPresent("Zip/Postal Code must be set"));
        assertTrue(selenium.isTextPresent("Phone must be set"));
        assertTrue(selenium.isTextPresent("Job Title must be set"));
        assertTrue(selenium.isTextPresent("Academic Degree(s) must be set"));
        assertTrue(selenium.isTextPresent("Institution Name must be set"));
        assertTrue(selenium.isTextPresent("Institution Type must be set"));
        assertTrue(selenium.isTextPresent("Department must be set"));
        assertTrue(selenium.isTextPresent("Research Area/Specialty must be set"));
        assertTrue(selenium.isTextPresent("Email Address must be set"));
        assertTrue(selenium.isTextPresent("Email Address Confirmation must be set"));
        assertTrue(selenium.isTextPresent("Password is required"));
        assertTrue(selenium.isTextPresent("Password confirmation is required."));
        assertTrue(selenium.isTextPresent("Password must be at least 6 characters and contain one lowercase, "
                + "one uppercase, and one number or symbol."));
        requiredResumeValidation();
        requiredExtensionValidation();
    }

    /**
     * test that validation messages for the resume field do or do not appear.
     */
    protected void requiredResumeValidation() {
        assertFalse(selenium.isTextPresent("Resume/C.V./Biosketch must be set"));
    }

    /**
     * test that validation messages for required extensions appear.
     */
    protected void requiredExtensionValidation() {
        //do nothing
    }

    private void matchingValidation() {
        selenium.type("email1", "test1");
        selenium.type("email2", "test2");
        selenium.type("password", "test1");
        selenium.type("confirmPassword", "test2");
        clickRegister(true);
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Email addresses do not match"));
        assertTrue(selenium.isTextPresent("Passwords do not match."));
        assertTrue(selenium.isTextPresent("Email Address is not a well-formed email address"));
    }

    private void enterData(int prefix) throws Exception {
        selenium.type("firstName", prefix + "testFirst");
        selenium.type("lastName", prefix + "testLast");
        selenium.type("address1", "testAddress1");
        selenium.type("address2", "testAddress2");
        selenium.type("city", "City");
        selenium.select("state", "label=Virginia");
        selenium.type("zip", "Zip");
        assertEquals("United States", selenium.getSelectedLabel("country"));
        selenium.select("country", "label=Togo");
        selenium.type("phone", "1234567890");
        selenium.type("phoneExtension", "1234");
        selenium.type("fax", "0987654321");
        selenium.type("title", "testJobTitle");
        selenium.type("degree", "testDegree");
        selenium.type("department", "testDepartment");
        selenium.type("researchArea", "testResearchArea");
        selenium.type("email1", prefix + "test@example.com");
        selenium.type("email2", prefix + "test@example.com");
        if (prefix <= 2) {
            selenium.type("password", "Password" + prefix);
            selenium.type("confirmPassword", "Password" + prefix);
        } else {
            assertFalse(selenium.isElementPresent("password"));
            assertFalse(selenium.isElementPresent("confirmPassword"));
        }
        enterResume();
        enterExtensionData();
    }

    /**
     * enter a value for the resume field if it is present.
     * @throws Exception on error
     */
    protected void enterResume() throws Exception {
        assertFalse(selenium.isTextPresent("Resume/C.V./Biosketch"));
        assertFalse(selenium.isElementPresent("resume"));
    }

    /**
     * enter the extension fields.
     * @throws Exception on error
     */
    protected void enterExtensionData() throws Exception {
        assertFalse(selenium.isTextPresent("Additional Information"));
    }

    private void createWithNewInstitution(int prefix, String institutionName, String institutionType) throws Exception {
        enterData(prefix);
        assertFalse(selenium.isElementPresent("status"));
        selenium.typeKeys("institution", institutionName + " ");
        selenium.select("institutionType", "label=" + institutionType);
        clickRegister(true);
        validateThankYouText();
        clickAndWait("link=Sign In");
        if (ClientProperties.isAccountApprovalActive()) {
            login("2test@example.com", "Password2", false);
            loginAsAdmin();
            if (prefix == 0) {
                assertTrue(selenium.isElementPresent("link=1 Registration Review"));
                clickAndWait("link=1 Registration Review");
                assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
            } else {
                assertTrue(selenium.isElementPresent("link=2 Registration Reviews"));
                clickAndWait("link=2 Registration Reviews");
                assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
            }
            assertTrue(selenium.isTextPresent("Users"));
            selenium.select("status", "label=Pending");
            clickAndWait("link=Sign Out");
        } else {
            login(prefix + "test@example.com", "Password" + prefix);
            clickAndWait("link=Sign Out");
        }
    }

    private void createWithExistingInstitution() throws Exception {
        goToRegisterPage();
        enterData(++userPrefix);
        assertFalse(selenium.isElementPresent("status"));
        selectFromAutocomplete("institution", "test", "testInstitution");
        clickRegister(true);
        validateThankYouText();
        clickAndWait("link=Sign In");
        if (ClientProperties.isAccountApprovalActive()) {
            login("2test@example.com", "Password2", false);
            loginAsAdmin();
            assertTrue(selenium.isElementPresent("link=3 Registration Reviews"));
            clickAndWait("link=3 Registration Reviews");
            assertTrue(selenium.isTextPresent("Users"));
            selenium.select("status", "label=Pending");
            assertTrue(selenium.isTextPresent("1-3 of 3 Results"));
            clickAndWait("link=Sign Out");
        } else {
            login("2test@example.com", "Password2");
            clickAndWait("link=Sign Out");
        }
    }

    private void validateThankYouText() {
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Thank You for Registering"));
        assertTrue(selenium.isTextPresent("Registration information successfully submitted."));
        assertTrue(selenium.isTextPresent("Thank you for your interest"));
        assertTrue(selenium.isElementPresent("link=Continue to*Homepage >>"));
        if (ClientProperties.isAccountApprovalActive()) {
            assertTrue(selenium.isTextPresent("make a decision on account approval"));
            assertTrue(selenium.isTextPresent("The review process may take up to one week"));
            assertFalse(selenium.isElementPresent("link=Search for "
                    + ClientProperties.getBiospecimenTerm() + "s >>"));
        } else {
            assertTrue(selenium.isTextPresent("Your account has been successfully created."));
            assertTrue(selenium.isElementPresent("link=Search for "
                    + ClientProperties.getBiospecimenTerm() + "s >>"));
        }
    }

    private void duplicateValidation() throws Exception {
        goToRegisterPage();
        enterData(userPrefix);
        assertFalse(selenium.isElementPresent("status"));
        selenium.typeKeys("institution", "testInstitution");
        clickRegister(true);
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("A user with that email address already exists."));
    }

    private void cancelLink() {
        assertTrue(selenium.isElementPresent("link=cancel"));
        clickAndWait("link=cancel");
        assertTrue(selenium.isTextPresent("Sign In"));
        assertTrue(selenium.isTextPresent("Email Address"));
        assertTrue(selenium.isTextPresent("Password"));
        assertTrue(selenium.isElementPresent(REGISTER_LINK));
    }

    private void updateUser() {
        loginAsAdmin();
        goToUserListPage();
        assertTrue(selenium.isTextPresent("Users"));
        assertTrue(selenium.isElementPresent("link=2testLast, 2testFirst"));
        clickAndWait("link=2testLast, 2testFirst");
        validateText(false, true);
        viewResume();
        viewExtensions();
        selenium.type("firstName", "testFirstUpdated");
        selenium.type("lastName", "testLastUpdated");
        selenium.select("status", "label=Active");
        clickRegister(false);
        assertTrue(selenium.isTextPresent("User successfully saved."));
        assertTrue(selenium.isTextPresent("Users"));
        selenium.select("status", "label=Active");
        waitForPageToLoad();
        int count = ClientProperties.getDefaultUserCount() + 2 + 1;
        if (ClientProperties.isAccountApprovalActive()) {
            count -= 2;
        }
        assertTrue(selenium.isTextPresent("1-" + Math.min(PAGE_SIZE, count) + " of " + count + " Results"));
        clickAndWait("link=Last Updated (PDT)");
        clickAndWait("link=Last Updated (PDT)");
        assertTrue(selenium.isTextPresent("testFirstUpdated"));
        assertTrue(selenium.isTextPresent("testLastUpdated"));
        clickAndWait("link=Sign Out");
        login("2test@example.com", "Password2");
        clickAndWait("link=Sign Out");
    }

    /**
     * validate display of the resume field.
     */
    protected void viewResume() {
        assertFalse(selenium.isTextPresent("Resume/C.V./Biosketch"));
        assertFalse(selenium.isElementPresent("resume"));
    }

    /**
     * validate display of the extensions.
     */
    protected void viewExtensions() {
        //do nothing
    }

    private void validateText(boolean isRegister, boolean isAdmin) {
        assertEquals(isRegister && !isAdmin, selenium.isTextPresent("Register for an Account"));
        assertEquals(isRegister && isAdmin, selenium.isTextPresent("Create an Account"));
        assertEquals(!isRegister, selenium.isTextPresent("User Details:"));

        assertEquals(isRegister && !isAdmin, selenium.isTextPresent("Why Register?"));
        assertEquals(isRegister && !isAdmin, selenium.isTextPresent("Register to browse our "
                + ClientProperties.getBiospecimenTermLowerCase() + " inventory"));
        assertEquals(isRegister && !isAdmin, selenium.isTextPresent("Get Started Here"));
        assertEquals(!isRegister || isAdmin, selenium.isTextPresent("Personal Information"));
        assertTrue(selenium.isTextPresent("(* indicates required field.)"));

        assertEquals(isRegister && !isAdmin, selenium.isTextPresent("Choose Your Sign In Information"));
        assertEquals(!isRegister || !isAdmin,  selenium.isTextPresent("Password"));
        assertEquals(isRegister && !isAdmin, selenium.isTextPresent("Create Password"));
        assertEquals(!isRegister, selenium.isTextPresent("Enter New Password"));
        assertEquals(isRegister && !isAdmin, selenium.isTextPresent("Re-type Password"));
        assertEquals(!isRegister, selenium.isTextPresent("Re-enter New Password"));

        assertEquals(isRegister || isAdmin, selenium.isElementPresent("email1"));
        assertEquals(isRegister || isAdmin, selenium.isElementPresent("email2"));

        assertEquals(isAdmin, selenium.isTextPresent("Access Privileges"));
        assertEquals(isAdmin, selenium.isTextPresent("User Role"));
        assertEquals(isAdmin, selenium.isTextPresent("Status"));
        assertEquals(isAdmin, selenium.isElementPresent("status"));

        assertEquals(isRegister && !isAdmin,
                selenium.isElementPresent("xpath=//input[@value='Submit Registration Now']"));
        assertEquals(isRegister && isAdmin, selenium.isElementPresent("xpath=//input[@value='Create Account']"));
        assertEquals(!isRegister, selenium.isElementPresent("xpath=//input[@value='Save']"));

        if (isRegister) {
            if (isAdmin && !selenium.isElementPresent("institutionType")) {
                assertEquals("123 Main Street", selenium.getValue("address1"));
                assertTrue(StringUtils.isBlank(selenium.getValue("address2")));
                assertEquals("Anytown", selenium.getValue("city"));
                assertEquals("Maryland", selenium.getSelectedLabel("state"));
                assertEquals("12345", selenium.getValue("zip"));
                assertEquals("United States", selenium.getSelectedLabel("country"));
            } else {
                assertTrue(StringUtils.isBlank(selenium.getValue("address1")));
                assertTrue(StringUtils.isBlank(selenium.getValue("address2")));
                assertTrue(StringUtils.isBlank(selenium.getValue("city")));
                assertEquals("- Select -", selenium.getSelectedLabel("state"));
                assertTrue(StringUtils.isBlank(selenium.getValue("zip")));
                assertEquals("United States", selenium.getSelectedLabel("country"));
            }
         } else {
             assertEquals("testAddress1", selenium.getValue("address1"));
             assertEquals("testAddress2", selenium.getValue("address2"));
             assertEquals("City", selenium.getValue("city"));
             assertEquals("Virginia", selenium.getSelectedLabel("state"));
             assertEquals("Zip", selenium.getValue("zip"));
             assertEquals("Togo", selenium.getSelectedLabel("country"));
         }
    }

    private void updatePassword() throws Exception {
        loginAsAdmin();
        goToUserListPage();
        clickAndWait("link=Last Updated (PDT)");
        clickAndWait("link=Last Updated (PDT)");
        assertTrue(selenium.isTextPresent("Users"));
        selenium.select("status", "label=Active");
        waitForPageToLoad();
        assertTrue(selenium.isTextPresent("Users"));
        clickAndWait("link=testLastUpdated, testFirstUpdated");
        assertTrue(selenium.isTextPresent("User Details:"));
        selenium.type("password", "Password2");
        selenium.type("confirmPassword", "Password2");
        clickRegister(false);
        assertTrue(selenium.isTextPresent("User successfully saved."));
        assertTrue(selenium.isTextPresent("Users"));
        clickAndWait("link=Sign Out");
        login("2test@example.com", "Password2");
        clickAndWait("link=Sign Out");
    }

    private void updateInstitution() {
        loginAsAdmin();
        goToUserListPage();
        assertTrue(selenium.isTextPresent("Users"));
        assertTrue(selenium.isElementPresent("link=1testLast, 1testFirst"));
        assertTrue(selenium.isTextPresent("testInstitution"));
        clickAndWait("link=1testLast, 1testFirst");
        selenium.type("institution", "");
        String instName = ClientProperties.getDefaultConsortiumMemberName();
        String substring = StringUtils.substringBefore(instName, "'").toLowerCase();
        selectFromAutocomplete("institution", substring, instName);
        clickRegister(false);
        assertTrue(selenium.isTextPresent("User successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Users"));
        assertTrue(selenium.isElementPresent("link=1testLast, 1testFirst"));
        int rowIndex = 0;
        while (true) {
            if (selenium.getTable("user." + rowIndex + ".1").contains("1test@example.com")) {
                break;
            }
            rowIndex++;
        }
        String institution = selenium.getTable("user." + rowIndex + ".2");
        assertFalse(institution.contains("testInstitution"));
        assertTrue(institution.contains(ClientProperties.getDefaultConsortiumMemberName()));
        clickAndWait("link=Sign Out");
    }

    private void administratableGroups() {
        //verify only the right groups are available as options
        verifyAvailableGroups(ClientProperties.getInstitutionalAdminEmail(), "tissueLocator1", false, false);
        verifyAvailableGroups(ClientProperties.getConsortiumAdminEmail(), "tissueLocator1", false, true);
        verifyAvailableGroups("superadmin@example.com", "tissueLocator1", true, true);

        //verify roles the user has are not removed
        //first run adds an accessible group, leaving one accessible and one inaccessible group
        verifyGroupRetained(true);
        //second run removes the accesssible group, leaving only one inaccessible group
        verifyGroupRetained(false);
    }

    private void verifyAvailableGroups(String username, String password, boolean sysAdmin, boolean consortAdmin) {
        login(username, password);
        goToUserListPage();
        clickAndWait("link=" + ClientProperties.getInstitutionalAdminName());
        assertTrue(selenium.isElementPresent("object.groups-" + RESEARCHER_GROUP_ID));
        assertTrue(selenium.isElementPresent("object.groups-" + TECHNICIAN_GROUP_ID));
        assertTrue(selenium.isElementPresent("object.groups-" + INST_ADMIN_GROUP_ID));
        assertTrue(selenium.isElementPresent("object.groups-" + SCIENTIFIC_REVIEW_GROUP_ID));
        assertEquals(consortAdmin, selenium.isElementPresent("object.groups-" + CONSORT_ADMIN_GROUP_ID));
        assertEquals(sysAdmin, selenium.isElementPresent("object.groups-" + SYS_ADMIN_GROUP_ID));
        clickAndWait("link=Sign Out");
    }

    private void verifyGroupRetained(boolean add) {
        loginAsAdmin();
        goToUserListPage();
        selenium.select("pageSize", "label=100");
        waitForPageToLoad();
        clickAndWait("link=" + ClientProperties.getSuperadminName());
        if (add) {
            selenium.check("object.groups-" + RESEARCHER_GROUP_ID);
        } else {
            selenium.uncheck("object.groups-" + RESEARCHER_GROUP_ID);
        }
        clickRegister(false);
        assertTrue(selenium.isTextPresent("User successfully saved."));
        assertTrue(selenium.isTextPresent("Users"));
        assertTrue(selenium.isTextPresent("Super Administrator"));
        clickAndWait("link=Sign Out");
        login("superadmin@example.com", "tissueLocator1");
        goToUserListPage();
        selenium.select("pageSize", "label=100");
        waitForPageToLoad();
        clickAndWait("link=" + ClientProperties.getSuperadminName());
        assertEquals(add, selenium.isChecked("object.groups-" + RESEARCHER_GROUP_ID));
        assertTrue(selenium.isElementPresent("object.groups-" + SYS_ADMIN_GROUP_ID));
        assertTrue(selenium.isChecked("object.groups-" + SYS_ADMIN_GROUP_ID));
        clickAndWait("link=Sign Out");
    }

    private void goToUserListPage() {
        mouseOverAndPause("link=Administration");
        clickAndWait("link=User Administration");
        waitForPageToLoad();
    }

    private void addNewUser() throws Exception {
        login(ClientProperties.getInstitutionalAdminEmail(), "tissueLocator1");
        goToUserListPage();
        clickAndWait("link=Add New User");
        validateText(true, true);
        enterData(++userPrefix);
        assertTrue(selenium.isElementPresent("status"));
        assertEquals("Active", selenium.getSelectedLabel("status"));
        assertTrue(selenium.isElementPresent("object.groups-" + RESEARCHER_GROUP_ID));
        selenium.check("object.groups-" + RESEARCHER_GROUP_ID);
        clickRegister(false);
        assertTrue(selenium.isTextPresent("User successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
    }

    private void verifyNewUser() {
        assertTrue(selenium.isTextPresent("Users"));
        selenium.select("status", "label=Active");
        waitForPageToLoad();
        int count = ClientProperties.getDefaultUserCount() + NEW_USER_COUNT;
        if (ClientProperties.isAccountApprovalActive()) {
            count -= 2;
        }
        if (count <= PAGE_SIZE) {
            assertTrue(selenium.isTextPresent("1-" + count + " of " + count + " Results"));
        } else {
            assertTrue(selenium.isTextPresent("1-20 of " + count + " Results"));
        }
        assertTrue(selenium.isTextPresent("3testLast"));
        assertTrue(selenium.isTextPresent("3testFirst"));
        clickAndWait("link=Sign Out");
        login("3test@example.com", "changeMe!");
        clickAndWait("link=Account");
        assertEquals("3testFirst", selenium.getValue("firstName"));
        assertEquals("3testLast", selenium.getValue("lastName"));
        assertTrue(selenium.isElementPresent("email1"));
        assertTrue(selenium.isElementPresent("email2"));
        assertEquals("3test@example.com", selenium.getValue("email1"));
        clickAndWait("link=Sign Out");
    }

    /**
     * Test deactivation of an active user account.
     */
    private void deactivateUserAccount() {
        loginAsAdmin();
        goToUserListPage();
        selenium.select("status", "label=Active");
        waitForPageToLoad();
        clickAndWait("link=3testLast, 3testFirst");
        selenium.select("id=status", "label=Inactive");
        waitForElementById("statusTransitionComment");
        clickAndWait("xpath=//input[@type='submit' and @value='Save']");
        assertTrue(selenium.isTextPresent("A comment is required for inactivating an existing user of the system."));
        assertTrue(selenium.isElementPresent("id=statusTransitionComment"));
        selenium.type("statusTransitionComment", "deactivating user");
        clickAndWait("xpath=//input[@type='submit' and @value='Save']");
        assertTrue(selenium.isTextPresent("User successfully saved."));
        clickAndWait("link=Sign Out");
    }

    private void consortiumAdminCreate() {
        loginAsAdmin();
        goToUserListPage();
        clickAndWait("link=Add New User");
        validateText(true, true);
    }

    /**
     * Click the register button.
     * @param isRegister TODO
     */
    protected void clickRegister(boolean isRegister) {
        clickAndWait(REGISTER_BUTTON);
    }
}
