/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author smiller
 *
 */
public class MyAccountTest extends AbstractTissueLocatorSeleniumTest {

    private static final String MY_ACCOUNT_LINK = "link=Account";
    private static final String REGISTER_BUTTON = "btn_register";

    /**
     * Tests user registration.
     * @throws Exception on error
     */
    @Test
    public void testUserAccountEdit() throws Exception {
        loginAsUser1();
        goToMyAccount(false);
        cancelLink();
        updateUser(ClientProperties.getResearcherEmail());
        clickAndWait("link=Sign Out");

        login("superadmin@example.com", "tissueLocator1");
        goToMyAccount(true);
        cancelLink();
        updateUser("superadmin@example.com");
        clickAndWait("link=Sign Out");
    }

    private void cancelLink() {
        assertTrue(selenium.isElementPresent("link=cancel"));
        clickAndWait("link=cancel");
        assertTrue(selenium.isTextPresent("Welcome back"));
        clickAndWait(MY_ACCOUNT_LINK);
    }

    private void updateUser(String email) {
        assertTrue(selenium.isElementPresent("email1"));
        assertTrue(selenium.isElementPresent("email2"));
        assertTrue(selenium.isElementPresent("title"));
        String firstName = selenium.getValue("firstName");
        String suffix = " updated";
        selenium.type("firstName", firstName + suffix);
        selenium.type("address2", "new address 2");
        clickAndWait(REGISTER_BUTTON);
        assertTrue(selenium.isTextPresent("Account updated successfully."));
        assertTrue(selenium.isTextPresent("Welcome, " + firstName + suffix));
        selenium.type("firstName", firstName);
        selenium.type("address2", "");
        clickAndWait(REGISTER_BUTTON);
        assertTrue(selenium.isTextPresent("Account updated successfully."));
        assertTrue(selenium.isTextPresent("Welcome, " + firstName));
        selenium.type("title", "newTitle");
        clickAndWait(REGISTER_BUTTON);
        assertTrue(selenium.isTextPresent("Account updated successfully."));
        assertTrue(selenium.isTextPresent("Welcome, " + firstName));
    }

    private void goToMyAccount(boolean isAdmin) {
        assertTrue(selenium.isElementPresent(MY_ACCOUNT_LINK));
        clickAndWait(MY_ACCOUNT_LINK);
        validateText(isAdmin);
    }

    private void validateText(boolean isAdmin) {
        assertTrue(selenium.isTextPresent("User Details:"));
        assertFalse(selenium.isTextPresent("Register for an Account"));
        assertFalse(selenium.isTextPresent("Create an Account"));
        assertTrue(selenium.isTextPresent("Personal Information"));
        assertTrue(selenium.isTextPresent("(* indicates required field.)"));
        assertFalse(selenium.isTextPresent("Why Register?"));
        assertFalse(selenium.isTextPresent("Register to browse our biospecimen inventory"));
        assertFalse(selenium.isTextPresent("Get Started Here"));
        assertFalse(selenium.isTextPresent("Choose Your Sign In Information"));
        assertFalse(selenium.isTextPresent("Create Password"));
        assertTrue(selenium.isTextPresent("Enter New Password"));
        assertFalse(selenium.isTextPresent("Re-type Password"));
        assertTrue(selenium.isTextPresent("Re-enter New Password"));

        assertEquals(isAdmin, selenium.isTextPresent("Access Privileges"));
        assertEquals(isAdmin, selenium.isTextPresent("User Role"));
        assertEquals(isAdmin, selenium.isTextPresent("Status"));
        assertEquals(isAdmin, selenium.isElementPresent("status"));

        assertFalse(selenium.isElementPresent("xpath=//input[@value='Submit Registration Now']"));
        assertFalse(selenium.isElementPresent("xpath=//input[@value='Create Account']"));
        assertTrue(selenium.isElementPresent("xpath=//input[@value='Save']"));

        assertEquals("123 Main Street", selenium.getValue("address1"));
        assertTrue(StringUtils.isBlank(selenium.getValue("address2")));
        assertEquals("Anytown", selenium.getValue("city"));
        assertEquals("Maryland", selenium.getSelectedLabel("state"));
        assertEquals("12345", selenium.getValue("zip"));
        assertEquals("United States", selenium.getSelectedLabel("country"));
    }
}
