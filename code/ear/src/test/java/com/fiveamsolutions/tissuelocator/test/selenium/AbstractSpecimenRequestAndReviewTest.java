/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.TestProperties;

/**
 * @author ddasgupta
 *
 */
public abstract class AbstractSpecimenRequestAndReviewTest extends AbstractTissueLocatorSeleniumTest {

    /**
     * Cart Size.
     */
    protected static final int CART_SIZE = 3;

    /**
     * ID One.
     */
    protected static final String ID_ONE = "test 10";

    /**
     * ID Two.
     */
    protected static final String ID_TWO = "test 15";

    /**
     * ID Three.
     */
    protected static final String ID_THREE = "test 20";

    /**
     * String used to refer to portions of cart-related elements.
     */
    protected static final String CART_LABEL = "updateCartSession";

    /**
     * String used for db cart updates.
     */
    protected static final String DRAFT_CART_LABEL = "updateCartDb";

    private static final String PROSPECTIVE_NOTES = "50 Breast Cancer specimens";
    private static final String ORDER_COUNT_XPATH = "xpath=//form[@id='filterForm']/div[2]/div/div[1]";
    private static final String RECIPIENT_MTA_STATUS_VALID = "valid";
    private static final String RECIPIENT_MTA_STATUS_PENDING = "pending";
    private static final String RECIPIENT_MTA_STATUS_INVALID = "invalid";
    private static final String DELETE_DRAFT_CONFIRM = "Once you delete a draft request, "
        + "you cannot reopen it. Are you sure"
        + " you want to delete the request?"
        + " Click OK to delete the request and Cancel to remain on the current page.";
    private static final String DELETE_DRAFT_SUCCESS = "Draft Request * was successfully deleted";

    private static final int BILLING_ADDRESS_SECTION = 5;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "CartTest.sql";
    }

    /**
     * add specimens to the cart.
     */
    protected void addToCart() {
        loginAsUser1();
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        verifyRequestProcessStepDisplay("search");
        addSearchCriteria();
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-6 of 6 Results"));
        assertFalse(StringUtils.isBlank(selenium.getTable("specimen.1.6")));
        assertFalse(StringUtils.isBlank(selenium.getTable("specimen.2.6")));
        assertTrue(StringUtils.isBlank(selenium.getTable("specimen.3.6")));
        verifyRequestProcessStepDisplay("select");
        selenium.click("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/input[1]");
        selenium.click("xpath=//table[@id='specimen']/tbody/tr[2]/td[1]/input[1]");
        selenium.click("xpath=//table[@id='specimen']/tbody/tr[3]/td[1]/input[1]");
        clickAndWait("btn_addtocart");
        verifyRequestProcessStepDisplay("request");
        assertTrue(selenium.isTextPresent(ClientProperties.getCartLink()));
        assertFalse(selenium.isTextPresent(ID_ONE));
        assertFalse(selenium.isTextPresent(ID_TWO));
        assertFalse(selenium.isTextPresent(ID_THREE));
        assertFalse(StringUtils.isBlank(selenium.getTable("lineItem.1.1")));
        assertFalse(StringUtils.isBlank(selenium.getTable("lineItem.2.1")));
        assertTrue(StringUtils.isBlank(selenium.getTable("lineItem.3.1")));
        if (ClientProperties.getDisplayProspectiveCollection()) {
            addProspectiveCollectionToCart();
        }
        verifyExternalCommentDisplay(false);
        verifyProspectiveCollection();
    }

    /**
     * Add prospective collection data to the cart.
     */
    protected void addProspectiveCollectionToCart() {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getProspectiveCollectionLink());
        assertTrue(selenium.isTextPresent(ClientProperties.getProspectiveCollectionNotes()));
        selenium.type("prospectiveCollectionNotes", PROSPECTIVE_NOTES);
        clickAndWait("btn_addtocart");
    }

    /**
     * Subclasses should override this method to add add any necessary search criteria.
     */
    protected void addSearchCriteria() {
        //do nothing
    }

    /**
     * verify the proper display of the prospective collection fields and links.
     */
    protected void verifyProspectiveCollection() {
        boolean displayProspectiveCollection = ClientProperties.getDisplayProspectiveCollection();
        assertEquals(displayProspectiveCollection,
                selenium.isElementPresent("link=" + ClientProperties.getProspectiveCollectionLink()));
        assertEquals(displayProspectiveCollection,
                selenium.isTextPresent(ClientProperties.getProspectiveCollectionNotes()));
        assertEquals(displayProspectiveCollection,
                selenium.isTextPresent(PROSPECTIVE_NOTES));
    }

    /**
     * validate the cart.
     */
    protected void cartValidation() {
        for (int i = 0; i < CART_SIZE; i++) {
            assertEquals(StringUtils.isBlank(selenium.getValue(CART_LABEL + "_lineItems_" + i + "__quantity")),
                    selenium.isElementPresent(CART_LABEL + "_lineItems_" + i + "__quantityUnits"));
        }
        clickAndWait("btn_update");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Quantity must be set"));
        for (int i = 0; i < CART_SIZE; i++) {
            selenium.type(CART_LABEL + "_lineItems_" + i + "__quantity", "1000000");
            selenium.type(CART_LABEL + "_lineItems_" + i + "__note", "note");
            assertFalse(selenium.isElementPresent(CART_LABEL + "_lineItems_" + i + "__quantityUnits"));
        }
        clickAndWait("btn_update");
        assertTrue(selenium.isTextPresent(ClientProperties.getCartLink()));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        for (int i = 0; i < CART_SIZE; i++) {
            selenium.type(CART_LABEL + "_lineItems_" + i + "__quantity", "invalid");
            String message = "The quantity must be less than or equal to the available quanity.";
            assertEquals(StringUtils.isNotBlank(selenium.getTable("lineItem." + (i + 1) + ".1")),
                    selenium.getTable("lineItem." + (i + 1) + ".2").contains(message));
            assertFalse(selenium.isElementPresent(CART_LABEL + "_lineItems_" + i + "__quantityUnits"));
        }
        clickAndWait("btn_update");
        assertTrue(selenium.isTextPresent(ClientProperties.getCartLink()));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        for (int i = 0; i < CART_SIZE; i++) {
            selenium.type(CART_LABEL + "_lineItems_" + i + "__quantity", "10");
            assertTrue(selenium.getTable("lineItem." + (i + 1) + ".2").contains(
                    "Amount Requested must be a number greater than 0 and "
                            + "less than or equal to the amount available."));
        }
    }

    /**
     * enter study data.
     *
     * @throws Exception on error
     */
    protected void studyDetails() throws Exception {
        clickAndWait("btn_request");
        verifyRequestProcessStepDisplay("proposal");
        assertTrue(selenium.isTextPresent("Research Proposal"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        verifyCart(true);
        verifyPrepopulatedPersonData("investigator");
        verifyPrepopulatedPersonData("recipient");
        collapseSections();
        verifySaveDraft();
        verifyTabs();
        verifyFundingStatus();
        verifyBillingRecipient();
        enterData(false, true);
        if (ClientProperties.isMtaActive()) {
            verifySaveMtaCertificationDraft();
        }
        boolean displayProspectiveCollection = ClientProperties.getDisplayProspectiveCollection();
        assertEquals(displayProspectiveCollection,
                selenium.isTextPresent(ClientProperties.getProspectiveCollectionNotes()));
        assertEquals(displayProspectiveCollection, selenium.isTextPresent(PROSPECTIVE_NOTES));
        verifyExternalCommentDisplay(false);
    }

    private void verifySaveDraft() {
        assertTrue(selenium.isElementPresent("btn_saveDraft"));
        assertFalse(selenium.isElementPresent("revisionComment"));
        assertFalse(selenium.isElementPresent("link=Revise " + ClientProperties.getCartLink()));
        clickAndWait("btn_saveDraft");
        assertTrue(selenium.isTextPresent("Your request has been saved. View My Requests (Request *) to load your"
                + " saved request."));
        assertFalse(selenium.isElementPresent("revisionComment"));
        assertTrue(selenium.isElementPresent("link=Revise " + ClientProperties.getCartLink()));
    }

    private void verifyBillingRecipient() {
        boolean showAddress = ClientProperties.isDisplayShipmentBillingRecipient();
        assertEquals(showAddress, selenium.isTextPresent("Billing Address"));
        assertEquals(showAddress, selenium.isElementPresent("billing_address"));
        if (showAddress) {
            assertTrue(selenium.isChecked("noBillingRecipient"));
            assertFalse(selenium.isVisible("billingRecipient"));
            selenium.click("includeBillingRecipient");
            assertTrue(selenium.isVisible("billingRecipient"));
            copyFromUser("billingRecipient");
            selenium.click("noBillingRecipient");
            selenium.click("includeBillingRecipient");
            verifyBillingRecipientFieldsCleared();
            selenium.click("copybillingRecipient");
            selenium.click("noBillingRecipient");
        }
    }

    private void verifyBillingRecipientFieldsCleared() {
        String[] textFields = new String[]{"FirstName", "LastName", "Line1", "Line2",
                "City", "Zip", "Organization", "Phone", "PhoneExtension"};
        for (String field : textFields) {
            assertTrue(StringUtils.isBlank(selenium.getValue("billingRecipient" + field)));
        }
        assertEquals("- Select -", selenium.getText("//div[@id='wwgrp_billingRecipientCountry']/button"));
        assertEquals("- Select -", selenium.getText("//div[@id='wwgrp_billingRecipientState']/button"));
    }

    private void verifySaveMtaCertificationDraft() {
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Research Proposal - Continued"));
        clickAndWait("btn_saveDraft");
        assertTrue(selenium.isTextPresent("Your request has been saved. View My Requests (Request *) to load your"
                + " saved request."));
        assertFalse(selenium.isElementPresent("revisionComment"));
        assertTrue(selenium.isElementPresent("link=Revise " + ClientProperties.getCartLink()));
    }

    /**
     * Verify study validators are executed when submitting
     * a request previously saved as a draft.
     * @throws Exception on error.
     */
    protected void verifyDraftValidation() throws Exception {
        createDraft();
        clickAndWait("link=Sign Out");
        loginAsUser1();
        clickAndWait("link=My Requests");
        selenium.select("status", "label=Draft");
        waitForPageToLoad();
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[8]/a");
        selenium.type("approved_irbApprovalExpirationDate", "");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("An IRB approval expiration date, and an IRB approval number"
                + " or IRB approval letter must be provided."));
        selenium.chooseOkOnNextConfirmation();
        clickAndWait("link=My Requests");
        selenium.select("status", "label=Draft");
        waitForPageToLoad();
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[8]/a[2]");
        assertEquals(DELETE_DRAFT_CONFIRM, selenium.getConfirmation());
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(DELETE_DRAFT_SUCCESS));
        clickAndWait("link=Sign Out");
    }
    
    /**
     * Verify a draft request can be saved and submitted, and
     * verify two requests cannot be submitted for the same specimen.
     * @throws Exception from enterData()
     */
    protected void verifyDraftFlow() throws Exception {
        // Create a draft to be submitted
        createDraft();
        // Create a draft for the same specimen
        clickAndWait("link=Sign Out");
        createDraft();
        verifyUpdateDraftCart();
        // Verify the first draft is successfully submitted
        clickAndWait("link=Sign Out");
        verifySubmitDraft();
        // Verify the same specimen cannot be submitted twice
        clickAndWait("link=Sign Out");
        if (!ClientProperties.isAggregateSearchResultsActive()) {
            verifyDraftSpecimenUnavailable();
        }
    }

    private void createDraft() throws Exception {
        loginAsUser1();
        simpleAddToCart(CART_LABEL, true);
        clickAndWait("btn_request");
        enterData(false, true);
        // omit billing address
        if (ClientProperties.isDisplayShipmentBillingRecipient()) {
            selenium.click("noBillingRecipient");
        }
        clickAndWait("btn_saveDraft");
    }

    private void verifySubmitDraft() throws Exception {
        loginAsUser1();
        clickAndWait("link=My Requests");
        selenium.select("status", "label=Draft");
        waitForPageToLoad();
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[8]/a");
        clickAndWait("btn_save");
        if (ClientProperties.isMtaActive()) {
            certifyMta(RECIPIENT_MTA_STATUS_PENDING, true);
        }
        clickAndWait("btn_save_request");
        assertTrue(selenium.isTextPresent("Thank You"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Thank you. Your request has been submitted and a unique "
                + "identifier has been generated."));
    }

    private void verifyUpdateDraftCart() {
        clickAndWait("link=My Requests");
        selenium.select("status", "label=Draft");
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[8]/a");
        simpleAddToCart(DRAFT_CART_LABEL, true);
        clickAndWait("btn_request");
        clickAndWait("btn_saveDraft");
    }

    private void verifyDraftSpecimenUnavailable() throws Exception {
        loginAsUser1();
        clickAndWait("link=My Requests");
        selenium.select("status", "label=Draft");
        waitForPageToLoad();
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[8]/a");
        clickAndWait("btn_save");
        if (ClientProperties.isMtaActive()) {
            certifyMta(RECIPIENT_MTA_STATUS_PENDING, true);
        }
        clickAndWait("btn_save_request");
        assertFalse(selenium.isTextPresent("Thank You"));
        assertTrue(selenium.isTextPresent("At least one of the " + ClientProperties.getSpecimenTermLowerCase()
                + "s you have requested is no longer available"));
    }

    /**
     * Test the deletion of draft requests.
     * @throws Exception from enterData().
     */
    protected void verifyDeleteDraft() throws Exception {
        // immediately delete
        createDraft();
        selenium.chooseCancelOnNextConfirmation();
        selenium.click("link=Delete Draft");
        assertEquals(DELETE_DRAFT_CONFIRM, selenium.getConfirmation());
        assertFalse(selenium.isTextPresent(DELETE_DRAFT_SUCCESS));
        selenium.chooseOkOnNextConfirmation();
        selenium.click("link=Delete Draft");
        waitForPageToLoad();
        assertEquals(DELETE_DRAFT_CONFIRM, selenium.getConfirmation());
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(DELETE_DRAFT_SUCCESS));

        // return to edit and delete
        clickAndWait("link=Sign Out");
        createDraft();
        clickAndWait("link=Sign Out");
        loginAsUser1();
        clickAndWait("link=My Requests");
        selenium.select("status", "label=Draft");
        waitForPageToLoad();
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[8]/a[1]");
        assertTrue(selenium.isTextPresent("Research Proposal"));
        selenium.chooseOkOnNextConfirmation();
        selenium.click("link=Delete Draft");
        assertEquals(DELETE_DRAFT_CONFIRM, selenium.getConfirmation());
        waitForPageToLoad();
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(DELETE_DRAFT_SUCCESS));

        // revise cart and delete
        selenium.chooseOkOnNextConfirmation();
        clickAndWait("link=Sign Out");
        createDraft();
        clickAndWait("link=Revise " + ClientProperties.getCartLink());
        selenium.click("link=Delete Draft");
        waitForPageToLoad();
        assertEquals(DELETE_DRAFT_CONFIRM, selenium.getConfirmation());
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(DELETE_DRAFT_SUCCESS));

        // delete from request table
        selenium.chooseOkOnNextConfirmation();
        clickAndWait("link=Sign Out");
        createDraft();
        clickAndWait("link=Sign Out");
        loginAsUser1();
        clickAndWait("link=My Requests");
        selenium.select("status", "label=Draft");
        waitForPageToLoad();
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[8]/a[2]");
        assertEquals(DELETE_DRAFT_CONFIRM, selenium.getConfirmation());
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(DELETE_DRAFT_SUCCESS));

    }

    private void verifyTabs() {
        boolean irbApprovalRequired = ClientProperties.isIrbApprovalRequired();
        selenium.click("irbApprovalStatusValue_EXEMPT");
        assertFalse(selenium.isVisible("approved_irbName"));
        assertFalse(selenium.isVisible("approved_irbRegistrationNumber"));
        assertFalse(selenium.isVisible("approved_irbApprovalExpirationDate"));
        assertFalse(selenium.isVisible("approved_irbApprovalLetter"));
        assertFalse(selenium.isVisible("approved_irbApprovalNumber"));
        assertTrue(selenium.isVisible("exempt_irbName"));
        assertTrue(selenium.isVisible("exempt_irbRegistrationNumber"));
        assertTrue(selenium.isVisible("exempt_irbExemptionLetter"));
        assertTrue(selenium.isEditable("btn_save"));
        String humanSubjectNote = getNoIrbApprovalNote();
        selenium.click("irbApprovalStatusValue_NOT_APPROVED");
        assertFalse(selenium.isVisible("approved_irbName"));
        assertFalse(selenium.isVisible("approved_irbRegistrationNumber"));
        assertFalse(selenium.isVisible("approved_irbApprovalExpirationDate"));
        assertFalse(selenium.isVisible("approved_irbApprovalLetter"));
        assertFalse(selenium.isVisible("approved_irbApprovalNumber"));
        assertFalse(selenium.isVisible("exempt_irbName"));
        assertFalse(selenium.isVisible("exempt_irbRegistrationNumber"));
        assertFalse(selenium.isVisible("exempt_irbExemptionLetter"));
        assertTrue(selenium.isTextPresent(humanSubjectNote));
        assertEquals(!irbApprovalRequired, selenium.isEditable("btn_save"));
        selenium.click("irbApprovalStatusValue_APPROVED");
        assertTrue(selenium.isVisible("approved_irbName"));
        assertTrue(selenium.isVisible("approved_irbRegistrationNumber"));
        assertTrue(selenium.isVisible("approved_irbApprovalExpirationDate"));
        assertTrue(selenium.isVisible("approved_irbApprovalLetter"));
        assertTrue(selenium.isVisible("approved_irbApprovalNumber"));
        assertFalse(selenium.isVisible("exempt_irbExemptionLetter"));
        assertTrue(selenium.isEditable("btn_save"));
        boolean showPILegalFields = ClientProperties.getDisplayPILegalFields();
        if (showPILegalFields) {
            selenium.click("investigated_NO");
            assertFalse(selenium.isVisible("investigationComment"));
            selenium.click("investigated_YES");
            assertTrue(selenium.isVisible("investigationComment"));
            selenium.click("investigated_NO");
            assertFalse(selenium.isVisible("investigationComment"));
        }
    }

    private void verifyFundingStatus() {
        String minFundingStatus = ClientProperties.getMinimumRequiredFundingStatus();
        boolean displayPending = ClientProperties.isDisplayFundingStatusPending();
        verifyFundedFieldVisibility(false);
        if (minFundingStatus.equals("NOT_FUNDED")) {
            if (displayPending) {
                selenium.click("fundingStatus_PENDING");
                assertTrue(selenium.isEditable("btn_save"));
                assertFalse(selenium.isVisible("fundingStatusLink_FUNDED"));
                assertTrue(selenium.isVisible("fundingStatusLink_PENDING"));
                assertFalse(selenium.isVisible("fundingStatusLink_NOT_FUNDED"));
                verifyFundedFieldVisibility(false);
            }
            selenium.click("fundingStatus_NOT_FUNDED");
            assertTrue(selenium.isEditable("btn_save"));
            assertFalse(selenium.isVisible("fundingStatusLink_FUNDED"));
            assertFalse(displayPending ? selenium.isVisible("fundingStatusLink_PENDING")
                    : selenium.isElementPresent("fundingStatusLink_PENDING"));
            assertTrue(selenium.isVisible("fundingStatusLink_NOT_FUNDED"));
            verifyFundedFieldVisibility(false);

        } else if (minFundingStatus.equals("PENDING")) {

            selenium.click("fundingStatus_PENDING");
            assertTrue(selenium.isEditable("btn_save"));
            assertFalse(selenium.isVisible("fundingStatusLink_FUNDED"));
            assertTrue(selenium.isVisible("fundingStatusLink_PENDING"));
            assertFalse(selenium.isVisible("fundingStatusLink_NOT_FUNDED"));
            verifyFundedFieldVisibility(false);
            selenium.click("fundingStatus_NOT_FUNDED");
            assertFalse(selenium.isEditable("btn_save"));
            assertFalse(selenium.isVisible("fundingStatusLink_FUNDED"));
            assertFalse(selenium.isVisible("fundingStatusLink_PENDING"));
            assertTrue(selenium.isVisible("fundingStatusLink_NOT_FUNDED"));
            verifyFundedFieldVisibility(false);

        } else if (minFundingStatus.equals("FUNDED")) {
            if (displayPending) {
                selenium.click("fundingStatus_PENDING");
                assertFalse(selenium.isVisible("fundingStatusLink_FUNDED"));
                assertTrue(selenium.isVisible("fundingStatusLink_PENDING"));
                assertFalse(selenium.isVisible("fundingStatusLink_NOT_FUNDED"));
                assertFalse(selenium.isEditable("btn_save"));
                verifyFundedFieldVisibility(false);
            }
            selenium.click("fundingStatus_NOT_FUNDED");
            assertFalse(selenium.isVisible("fundingStatusLink_FUNDED"));
            assertFalse(displayPending ? selenium.isVisible("fundingStatusLink_PENDING")
                    : selenium.isElementPresent("fundingStatusLink_PENDING"));
            assertTrue(selenium.isVisible("fundingStatusLink_NOT_FUNDED"));
            assertFalse(selenium.isEditable("btn_save"));
            verifyFundedFieldVisibility(false);

        } else {
            fail("Not appropriate status value");
        }
        selenium.click("fundingStatus_FUNDED");
        assertTrue(selenium.isEditable("btn_save"));
        assertTrue(selenium.isVisible("fundingStatusLink_FUNDED"));
        assertFalse(displayPending ? selenium.isVisible("fundingStatusLink_PENDING")
                : selenium.isElementPresent("fundingStatusLink_PENDING"));
        assertFalse(selenium.isVisible("fundingStatusLink_NOT_FUNDED"));
        verifyFundedFieldVisibility(true);
    }

    /**
     * Subclasses should override to add verification of the visibility of dynamic extensions associated with the
     * funding field.
     *
     * @param visible whether the funded fields should be visible.
     */
    protected void verifyFundedFieldVisibility(boolean visible) {
        assertEquals(visible, selenium.isVisible("//div[@id='wwgrp_fundingSource']/button"));
    }

    /**
     * verify the contents of the cart.
     *
     * @param isResearcher if the user is a research and should not see external id's
     */
    protected void verifyCart(boolean isResearcher) {
        assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + "s in Request"));
        if (isResearcher) {
            assertFalse(selenium.isTextPresent(ID_ONE));
            assertFalse(selenium.isTextPresent(ID_TWO));
            assertFalse(selenium.isTextPresent(ID_THREE));
            assertFalse(StringUtils.isBlank(selenium.getTable("request.1.1")));
            assertFalse(StringUtils.isBlank(selenium.getTable("request.2.1")));
            assertTrue(StringUtils.isBlank(selenium.getTable("request.3.1")));

        } else {
            assertTrue(selenium.isTextPresent(ID_ONE));
            assertTrue(selenium.isTextPresent(ID_TWO));
            assertTrue(selenium.isTextPresent(ID_THREE));
            assertFalse(StringUtils.isBlank(selenium.getTable("request.1.2")));
            assertFalse(StringUtils.isBlank(selenium.getTable("request.2.2")));
            assertTrue(StringUtils.isBlank(selenium.getTable("request.3.2")));
        }
        String subtotalText = selenium.getTable("request.4.2");
        assertTrue(subtotalText.contains("$75.00"));
        assertTrue(subtotalText.contains("$175.00"));
    }

    /**
     * Enters request data.
     *
     * @param includeExemptionLetter whether or not to include the exemption letter
     * @param includeGeneralComment whether or not to include the general comment
     * @throws Exception on error
     */
    protected void enterData(boolean includeExemptionLetter, boolean includeGeneralComment) throws Exception {
        selenium.type("protocolTitle", "test-protocol-title");
        selenium.click("preliminary_YES");
        String pdPath = new File(ClassLoader.getSystemResource("testui.properties.example").toURI()).toString();
        selenium.type("protocolDocument", pdPath);
        selenium.type("abstract", "test-study-abstract");

        if (includeExemptionLetter) {
            selenium.click("irbApprovalStatusValue_EXEMPT");
            selenium.type("exempt_irbName", "test-irb-name");
            selenium.type("exempt_irbRegistrationNumber", "test-irb-registration-number");
            String exemptionPath = new File(ClassLoader.getSystemResource("exemption.txt").toURI()).toString();
            selenium.type("exempt_irbExemptionLetter", exemptionPath);
        } else {
            selenium.click("irbApprovalStatusValue_APPROVED");
            selenium.type("approved_irbName", "test-irb-name");
            selenium.type("approved_irbRegistrationNumber", "test-irb-registration-number");
            selenium.type("approved_irbApprovalNumber", "test-irb-approval-number");
            selenium.type("approved_irbApprovalExpirationDate", "01/01/2020");
            String approvalLetterPath = new File(ClassLoader.getSystemResource("approval.txt").toURI()).toString();
            selenium.type("approved_irbApprovalLetter", approvalLetterPath);
        }

        String resumePath = new File(ClassLoader.getSystemResource("test.properties").toURI()).toString();
        if (selenium.isElementPresent("changeresume") && selenium.isVisible("changeresume")) {
            selenium.click("changeresume");
        }
        selenium.type("resume", resumePath);

        int autocompleteIndex = 1;
        clearPersonData("investigator");
        enterPersonData("investigator", "Maryland", "Canada",
                ClientProperties.getDefaultConsortiumMemberName(), autocompleteIndex++);
        boolean showPILegalFields = ClientProperties.getDisplayPILegalFields();
        assertEquals(showPILegalFields, selenium.isElementPresent("certified_YES"));
        assertEquals(showPILegalFields, selenium.isElementPresent("certified_NO"));
        assertEquals(showPILegalFields, selenium.isElementPresent("investigated_YES"));
        assertEquals(showPILegalFields, selenium.isElementPresent("investigated_NO"));
        assertEquals(showPILegalFields, selenium.isElementPresent("investigationComment"));
        if (showPILegalFields) {
            selenium.check("certified_YES");
            selenium.check("investigated_YES");
            selenium.type("investigationComment", "investigation comment");
        }

        selenium.type("shippingInstructions", "test-instructions");

        clearPersonData("recipient");
        enterPersonData("recipient", "Virginia", "Mexico",
                ClientProperties.getShipmentRecipientInstitution(), autocompleteIndex++);
        if (ClientProperties.isDisplayShipmentBillingRecipient()) {
            selenium.click("includeBillingRecipient");
            enterPersonData("billingRecipient", "Georgia", "France",
                    ClientProperties.getShipmentRecipientInstitution(), autocompleteIndex++);
        }
        verifyStudyHelpText();
        enterDynamicExtensionFields();
        enterFundingStatusFields();

        assertTrue(selenium.isTextPresent("Additional Criteria Not Listed Above"));
        assertTrue(selenium.isElementPresent("generalComment"));
        if (includeGeneralComment) {
            selenium.type("generalComment", "test general comment");
        }
    }

    /**
     * subclasses should override to enter their own dynamic extensions.
     */
    protected void enterDynamicExtensionFields() {
        int section = ClientProperties.isDisplayShipmentBillingRecipient()
            ? BILLING_ADDRESS_SECTION + 1 : BILLING_ADDRESS_SECTION;
        assertTrue(selenium.isTextPresent(section + ". General Information"));
    }

    private void enterFundingStatusFields() {
        selenium.click("fundingStatus_FUNDED");
        enterFundedFields();
        selenium.click("fundingStatus_NOT_FUNDED");
        selenium.click("fundingStatus_FUNDED");
        verifyFundedFieldsCleared();
        enterFundedFields();
        if (ClientProperties.isDisplayFundingStatusPending()) {
            selenium.click("fundingStatus_PENDING");
            selenium.click("fundingStatus_FUNDED");
            verifyFundedFieldsCleared();
        }
        enterFundedFields();
    }

    /**
     * enter the fields associated with the funded status subclasses should override this method to enter any dynamic
     * extensions associated with the funded status.
     */
    protected void enterFundedFields() {
        selenium.select("object.study.fundingSources", "label=Foundation (not peer reviewed)");
        selenium.addSelection("object.study.fundingSources", "label=Other");
    }

    /**
     * verify the fields associated with the funded status subclasses should override this method to verify dynamic
     * extensions associated with the funded status have been cleared.
     */
    protected void verifyFundedFieldsCleared() {
        assertTrue(StringUtils.isBlank(selenium.getValue("object.study.fundingSources")));
        assertEquals("- Select -", selenium.getText("//div[@id='wwgrp_fundingSource']/button"));
    }

    /**
     * Subclasses should override to verify any expected help text links.
     */
    protected void verifyStudyHelpText() {

    }

    /**
     * validate the contents of the confirm page.
     *
     * @param statusHistory Expected status history.
     * @throws Exception on error.
     */
    protected void confirm(String[] statusHistory) throws Exception {
        clickAndWait("btn_save");
        verifyRequestProcessStepDisplay("confirm");
        if (ClientProperties.isMtaActive()) {
            certifyMta(RECIPIENT_MTA_STATUS_PENDING, true);
        }
        assertTrue(selenium.isTextPresent("Confirm Your Request"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Please confirm the proposal details."));
        verifyCart(true);
        collapseSections();
        confirmText(false, false, false, "/protected/request/download.*Db.action", statusHistory);
        verifyExternalCommentDisplay(false);
    }

    /**
     * validate the contents of the mta certification page.
     *
     * @param recipientMtaStatus status of the signed recipient mta
     * @param needsVerification whether the user must initial before proceeding.
     * @throws Exception on error.
     */
    protected void certifyMta(String recipientMtaStatus, boolean needsVerification) throws Exception {
        assertTrue(selenium.isTextPresent("Research Proposal - Continued"));
        assertTrue(selenium.isTextPresent("your institution needs to have signed the latest"));
        assertEquals(recipientMtaStatus.equals(RECIPIENT_MTA_STATUS_VALID),
                selenium.isTextPresent("Your organization has a valid MTA"));
        assertEquals(recipientMtaStatus.equals(RECIPIENT_MTA_STATUS_PENDING),
                selenium.isTextPresent("The MTA for your organization is pending review"));
        assertEquals(recipientMtaStatus.equals(RECIPIENT_MTA_STATUS_INVALID),
                selenium.isTextPresent("Your organization does not have a valid *MTA*. Please submit one below"));
        assertEquals(recipientMtaStatus.equals(RECIPIENT_MTA_STATUS_INVALID),
                selenium.isTextPresent("A valid *MTA* must be in place within 4 weeks of your request submission"
                        + " or it may be canceled. | See MTA FAQ"));
        assertTrue(selenium.isElementPresent("link=MTA FAQ"));
        assertEquals(recipientMtaStatus.equals(RECIPIENT_MTA_STATUS_INVALID),
                selenium.isTextPresent("Upload Signed *MTA"));
        assertEquals(recipientMtaStatus.equals(RECIPIENT_MTA_STATUS_INVALID),
                selenium.isElementPresent("submittedMtaDocument"));
        assertTrue(selenium.isTextPresent("Please certify that you have read and understand the requirements of the"
                + " Material Transfer Agreement by entering your initials in the following box:"));
        assertTrue(selenium.isElementPresent("mtaCertification"));
        assertTrue(selenium.isElementPresent("link=Download ABC MTA"));
        String fileTarget = selenium.getAttribute("link=Download ABC MTA@href");
        assertTrue(Pattern.matches(".*" + "/protected/downloadFile[^/]*mta-1.0.txt[^/]*", fileTarget));
        clickAndWait("btn_save");
        assertEquals(needsVerification, selenium.isTextPresent(ERROR_MESSAGE));
        if (needsVerification) {
            assertTrue(selenium.isTextPresent("You must certify you have read and understand the MTA"));

            if (recipientMtaStatus.equals(RECIPIENT_MTA_STATUS_INVALID)) {
                String mtaPath = new File(ClassLoader.getSystemResource("testui.properties.example")
                        .toURI()).toString();
                selenium.type("submittedMtaDocument", mtaPath);
                clickAndWait("btn_upload_mta");
                assertTrue(selenium.isTextPresent("The MTA was successfully uploaded"));
            }
            selenium.type("mtaCertification", "ABC");
            clickAndWait("btn_save");
        }
    }

    /**
     * confirm the text of the request page.
     *
     * @param showComment whether to show the comments
     * @param showVotes whether to show the votes
     * @param exemptFromApproval whether the request is exempt from approval.
     * @param docUrlBase the base url for the file downloads
     * @param statusHistory Expected statuses in the status history.
     */
    protected void confirmText(boolean showComment, boolean showVotes, boolean exemptFromApproval, String docUrlBase,
            String[] statusHistory) {
        assertTrue(selenium.isTextPresent("test-protocol-title"));
        confirmFundedFieldText();
        assertTrue(selenium.isTextPresent("No"));
        assertTrue(selenium.isTextPresent("test-irb-name"));
        assertTrue(selenium.isTextPresent("test-irb-registration-number"));
        testFileDownload("testui.properties.example", docUrlBase);
        assertTrue(selenium.isTextPresent("test-study-abstract"));
        boolean displayProspectiveCollection = ClientProperties.getDisplayProspectiveCollection();
        assertEquals(displayProspectiveCollection,
                selenium.isTextPresent(ClientProperties.getProspectiveCollectionNotes()));
        assertEquals(displayProspectiveCollection, selenium.isTextPresent(PROSPECTIVE_NOTES));

        if (exemptFromApproval) {
            testFileDownload("exemption.txt", docUrlBase);
        } else {
            assertTrue(selenium.isTextPresent("test-irb-approval-number"));
            assertTrue(selenium.isTextPresent("01/01/2020"));
            testFileDownload("approval.txt", docUrlBase);
        }

        testFileDownload("test.properties", docUrlBase);

        confirmPerson("investigator", "Maryland", "Canada", ClientProperties.getDefaultConsortiumMemberName());
        boolean showPILegalFields = ClientProperties.getDisplayPILegalFields();
        assertEquals(showPILegalFields, selenium.isTextPresent("I certify that the Principal investigator"));
        assertEquals(showPILegalFields, selenium.isElementPresent("investigatorCertifiedLabel"));
        assertEquals(showPILegalFields, selenium.isTextPresent("Has the Principal Investigator been subject"));
        assertEquals(showPILegalFields, selenium.isElementPresent("investigatorInvestigatedLabel"));
        assertEquals(showPILegalFields, selenium.isTextPresent("Please explain"));
        assertEquals(showPILegalFields, selenium.isTextPresent("investigation comment"));

        assertTrue(selenium.isTextPresent("test-instructions"));

        confirmPerson("recipient", "Virginia", "Mexico", ClientProperties.getShipmentRecipientInstitution());
        boolean showBillingRecipient = ClientProperties.isDisplayShipmentBillingRecipient();
        if (showBillingRecipient) {
            confirmPerson("billingRecipient", "Georgia", "France", ClientProperties.getShipmentRecipientInstitution());
        }
        assertEquals(showBillingRecipient, selenium.isTextPresent("Billing Address"));

        assertEquals(showComment, selenium.isTextPresent("Revision Comments"));
        assertEquals(showVotes, selenium.isTextPresent("Consortium's Review Decision"));

        if (statusHistory != null) {
            assertTrue(selenium.isTextPresent(". Status History"));
            assertTrue(selenium.isElementPresent("status_details"));
            String history = selenium.getText("status_details");
            String[] histories = StringUtils.stripAll(StringUtils.split(history, '\n'));
            assertEquals(statusHistory.length, histories.length);
            String dateString = new SimpleDateFormat("(MM/dd/yyyy)").format(new Date());
            for (int i = 0; i < statusHistory.length; i++) {
                assertTrue(histories[i].startsWith(statusHistory[i]));
                assertTrue(histories[i].endsWith(dateString));
            }
        } else {
            assertFalse(selenium.isTextPresent("Status History"));
            assertFalse(selenium.isElementPresent("status_details"));
        }

        assertTrue(selenium.isTextPresent("Additional Criteria Not Listed Above"));
        assertTrue(selenium.isTextPresent("test general comment"));

        confirmDynamicExtensionFields();
    }

    /**
     * verify display of the funded fields on the confirmation page.
     */
    protected void confirmFundedFieldText() {
        assertTrue(selenium.isTextPresent("Currently Funded"));
        assertTrue(selenium.isTextPresent("Yes"));
        assertTrue(selenium.isTextPresent("Foundation (not peer reviewed), Other"));
    }

    private void testFileDownload(String fileName, String docUrlBase) {
        assertTrue(selenium.isElementPresent("link=" + fileName));
        String fileTarget = selenium.getAttribute("link=" + fileName + "@href");
        assertTrue(Pattern.matches(".*" + docUrlBase + "[^/]*", fileTarget));
    }

    /**
     * subclasses should override to confirm their own dynamic extensions.
     */
    protected void confirmDynamicExtensionFields() {
        assertTrue(selenium.isTextPresent("General Information"));
    }

    /**
     * save the request.
     */
    protected void save() {
        clickAndWait("btn_save_request");
        verifyRequestProcessStepDisplay("receipt");
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Thank you. Your request has been submitted and a unique "
                + "identifier has been generated."));
        assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + " Request ID"));
        clickAndWait("link=Sign Out");
    }

    /**
     * test validation.
     *
     * @throws Exception on error
     */
    protected void validation() throws Exception {
        loginAsUser1();
        clickAndWait("link=" + ClientProperties.getCartLink());
        clickAndWait("btn_request");
        boolean showBillingRecipient = ClientProperties.isDisplayShipmentBillingRecipient();
        clearPersonData("investigator");
        clearPersonData("recipient");
        if (showBillingRecipient) {
            selenium.click("includeBillingRecipient");
        }
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Research Proposal"));
        assertFalse(selenium.isTextPresent("Confirm"));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));

        // required
        assertTrue(selenium.isTextPresent("Protocol Title must be set"));
        assertEquals(ClientProperties.isProtocolDocumentRequired(),
                selenium.isTextPresent("Protocol Document must be set"));
        assertFalse(selenium.isElementPresent("newprotocolDocument"));
        assertFalse(selenium.isElementPresent("existingprotocolDocument"));
        assertTrue(selenium.isTextPresent("IRB approval information must be set"));
        assertTrue(selenium.isTextPresent("Abstract must be set"));
        if (!ClientProperties.isUserResumeRequired()) {
            assertTrue(selenium.isTextPresent(ClientProperties.getInvestigatorResumeTerm() + " must be set."));
            assertFalse(selenium.isElementPresent("newresume"));
            assertFalse(selenium.isElementPresent("existingresume"));
        } else {
            assertTrue(selenium.isElementPresent("newresume"));
            assertFalse(selenium.isVisible("newresume"));
            assertTrue(selenium.isElementPresent("existingresume"));
            assertTrue(selenium.isVisible("existingresume"));
        }
        validatePerson("investigator");
        boolean showPILegalFields = ClientProperties.getDisplayPILegalFields();
        assertEquals(showPILegalFields, selenium.isTextPresent("PI Certified Response must be set"));
        assertEquals(showPILegalFields, selenium.isTextPresent("PI Investigated Response must be set"));
        validatePerson("recipient");
        if (showBillingRecipient) {
            validatePerson("billingRecipient");
        }
        validateRequiredDynamicExtensionFields();

        // email format
        selenium.type("investigatorEmail", "test-email-investigator");
        selenium.type("recipientEmail", "test-email-recipient");
        if (showPILegalFields) {
            selenium.check("investigated_YES");
            selenium.type("investigationComment", "");
        }

        if (showBillingRecipient) {
            selenium.click("includeBillingRecipient");
        }
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Research Proposal"));
        assertFalse(selenium.isTextPresent("Confirm"));
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isElementPresent("wwerr_investigatorEmail"));
        assertEquals("Email is not a well-formed email address", selenium.getText("wwerr_investigatorEmail"));
        assertTrue(selenium.isElementPresent("wwerr_recipientEmail"));
        assertEquals("Email is not a well-formed email address", selenium.getText("wwerr_recipientEmail"));
        assertEquals(showPILegalFields,
                selenium.isTextPresent("If the PI has been investigated, a comment must be provided."));
        assertFalse(selenium.isTextPresent("Billing Address First Name must be set"));
        assertFalse(selenium.isTextPresent("Billing Address Last Name must be set"));

        selenium.click("irbApprovalStatusValue_APPROVED");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("IRB Name is required"));
        assertTrue(selenium.isTextPresent("An IRB approval expiration date, and an IRB approval number"
                + " or IRB approval letter must be provided."));
        assertFalse(selenium.isElementPresent("newapproved_irbApprovalLetter"));
        assertFalse(selenium.isElementPresent("existingapproved_irbApprovalLetter"));
        String approvalLetterPath = new File(ClassLoader.getSystemResource("approval.txt").toURI()).toString();
        selenium.type("approved_irbApprovalLetter", approvalLetterPath);
        clickAndWait("btn_save");
        testFileDownload("approval.txt", "/protected/request/downloadIrbApprovalLetterSession.action");
        exerciseFileField("approved_irbApprovalLetter", "approval.txt");

        validateFundingStatus();

        // Future date validation
        selenium.type("approved_irbApprovalNumber", "test-irb-approval-number");
        selenium.type("approved_irbApprovalExpirationDate", "01/01/2009");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("IRB Approval Expiration Date must be a future date"));
        selenium.click("irbApprovalStatusValue_EXEMPT");
        clickAndWait("btn_save");

        assertTrue(selenium.isTextPresent("IRB Name is required"));
        assertTrue(selenium.isTextPresent("A letter of exemption must be provided"));
        assertFalse(selenium.isElementPresent("newexempt_irbExemptionLetter"));
        assertFalse(selenium.isElementPresent("existingexempt_irbExemptionLetter"));

        String exemptionPath = new File(ClassLoader.getSystemResource("exemption.txt").toURI()).toString();
        selenium.type("exempt_irbExemptionLetter", exemptionPath);
        clickAndWait("btn_save");
        testFileDownload("exemption.txt", "/protected/request/downloadIrbExemptionLetterSession.action");
        exerciseFileField("exempt_irbExemptionLetter", "exemption.txt");

        String pdPath = new File(ClassLoader.getSystemResource("testui.properties.example").toURI()).toString();
        selenium.type("protocolDocument", pdPath);
        String resumePath = new File(ClassLoader.getSystemResource("test.properties").toURI()).toString();
        selenium.type("resume", resumePath);
        clickAndWait("btn_save");
        testFileDownload("testui.properties.example", "/protected/request/downloadProtocolDocumentSession.action");
        exerciseFileField("protocolDocument", "testui.properties.example");
        testFileDownload("test.properties", "/protected/request/downloadResumeSession.action");
        exerciseFileField("resume", "test.properties");
    }

    private void validateFundingStatus() {
        boolean displayPending = ClientProperties.isDisplayFundingStatusPending();
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Currently Funded must be set"));

        assertEquals(displayPending, selenium.isElementPresent("fundingStatus_PENDING"));

        String minFundingStatus = ClientProperties.getMinimumRequiredFundingStatus();
        if (minFundingStatus.equals("NOT_FUNDED")) {
            selenium.click("fundingStatus_NOT_FUNDED");
            clickAndWait("btn_save");
            assertTrue(selenium.isChecked("fundingStatus_NOT_FUNDED"));
            validateFundedFields(false);

            if (displayPending) {
                selenium.click("fundingStatus_PENDING");
                clickAndWait("btn_save");
                assertTrue(selenium.isChecked("fundingStatus_PENDING"));
            }
            validateFundedFields(false);
        } else if (minFundingStatus.equals("PENDING")) {
            selenium.click("fundingStatus_PENDING");
            clickAndWait("btn_save");
            assertTrue(selenium.isChecked("fundingStatus_PENDING"));
            validateFundedFields(false);
        }

        selenium.click("fundingStatus_NOT_FUNDED");
        pause(SHORT_DELAY);
        verifyNotFundedMessage();

        selenium.click("fundingStatus_FUNDED");
        clickAndWait("btn_save");
        validateFundedFields(true);
        validateFundedFieldsFormat();
    }

    private void exerciseFileField(String fieldId, String fileName) {
        String fieldDivName = "new" + fieldId;
        String linkDivName = "existing" + fieldId;
        String link = "link=" + fileName;
        String changeLink = "change" + fieldId;
        String cancelLink = "cancel" + fieldId;
        assertTrue(selenium.isElementPresent(fieldDivName));
        assertTrue(selenium.isElementPresent(fieldId));
        assertFalse(selenium.isVisible(fieldDivName));
        assertFalse(selenium.isVisible(fieldId));
        assertFalse(selenium.isVisible(cancelLink));

        assertTrue(selenium.isElementPresent(linkDivName));
        assertTrue(selenium.isElementPresent(link));
        assertTrue(selenium.isVisible(linkDivName));
        assertTrue(selenium.isVisible(link));
        assertTrue(selenium.isVisible(changeLink));

        selenium.click(changeLink);
        assertTrue(selenium.isVisible(fieldDivName));
        assertTrue(selenium.isVisible(fieldId));
        assertTrue(selenium.isVisible(cancelLink));
        assertFalse(selenium.isVisible(linkDivName));
        assertFalse(selenium.isVisible(link));
        assertFalse(selenium.isVisible(changeLink));
        selenium.type(fieldId, fileName);

        selenium.click(cancelLink);
        assertFalse(selenium.isVisible(fieldDivName));
        assertFalse(selenium.isVisible(fieldId));
        assertFalse(selenium.isVisible(cancelLink));
        assertTrue(selenium.isVisible(linkDivName));
        assertTrue(selenium.isVisible(link));
        assertTrue(selenium.isVisible(changeLink));

        selenium.click(changeLink);
        assertTrue(StringUtils.isBlank(selenium.getValue(fieldId)));
    }

    /**
     * Verify the expected message is shown when a status of
     * 'not funded' is selected.
     */
    protected void verifyNotFundedMessage() {
        // by default, no message
        assertFalse(selenium.isElementPresent("xpath=//div[@id='fundingStatusLink_NOT_FUNDED']/p"));
    }

    /**
     * verify validation of the fields that appear when the funding status is set to funded.
     *
     * @param required whether the fields are required.
     */
    protected void validateFundedFields(boolean required) {
        assertEquals(required, selenium.isChecked("fundingStatus_FUNDED"));
        verifyFundedFieldVisibility(required);
        assertEquals(required, selenium.isTextPresent("Funding Source must be set"));
    }

    /**
     * verify format validation of the fields that appear when the funding status is set to funded.
     */
    protected void validateFundedFieldsFormat() {
        //do nothing
    }

    private void validatePerson(String idPrefix) {
        assertTrue(selenium.isElementPresent("wwerr_" + idPrefix + "FirstName"));
        assertEquals("First Name must be set", selenium.getText("wwerr_" + idPrefix + "FirstName"));
        assertTrue(selenium.isElementPresent("wwerr_" + idPrefix + "LastName"));
        assertEquals("Last Name must be set", selenium.getText("wwerr_" + idPrefix + "LastName"));
        assertTrue(selenium.isTextPresent("You must select a valid organization"));
        assertTrue(selenium.isElementPresent("wwerr_" + idPrefix + "Line1"));
        assertEquals("Address must be set", selenium.getText("wwerr_" + idPrefix + "Line1"));
        assertTrue(selenium.isElementPresent("wwerr_" + idPrefix + "City"));
        assertEquals("City must be set", selenium.getText("wwerr_" + idPrefix + "City"));
        assertTrue(selenium.isElementPresent("wwerr_" + idPrefix + "State"));
        assertEquals("State must be set", selenium.getText("wwerr_" + idPrefix + "State"));
        assertTrue(selenium.isElementPresent("wwerr_" + idPrefix + "Zip"));
        assertEquals("Zip/Postal Code must be set", selenium.getText("wwerr_" + idPrefix + "Zip"));
        assertTrue(selenium.isElementPresent("wwerr_" + idPrefix + "Email"));
        assertEquals("Email must be set", selenium.getText("wwerr_" + idPrefix + "Email"));
        assertTrue(selenium.isElementPresent("wwerr_" + idPrefix + "Phone"));
        assertEquals("Phone must be set", selenium.getText("wwerr_" + idPrefix + "Phone"));
    }

    /**
     * subclasses should override to validate their own required dynamic extensions.
     */
    protected void validateRequiredDynamicExtensionFields() {
        int section = ClientProperties.isDisplayShipmentBillingRecipient()
            ? BILLING_ADDRESS_SECTION + 1 : BILLING_ADDRESS_SECTION;
        assertTrue(selenium.isTextPresent(section + ". General Information"));
    }

    /**
     * copy the data from the current user.
     *
     * @param idPrefix the element id prefix
     */
    protected void copyFromUser(String idPrefix) {
        clearPersonData(idPrefix);
        if (ClientProperties.isUserResumeRequired() && "investigator".equals(idPrefix)) {
            selenium.click("changeresume");
            assertTrue(selenium.isVisible("newresume"));
            assertFalse(selenium.isVisible("existingresume"));
            selenium.type("resume", "invalid file name");
        }
        selenium.click("copy" + idPrefix);
        pause(DELAY);
        selenium.click("copy" + idPrefix);
        pause(DELAY);
        String[] nameParts = StringUtils.split(ClientProperties.getResearcherName());
        assertEquals(nameParts[0], selenium.getValue(idPrefix + "FirstName"));
        assertEquals(nameParts[1], selenium.getValue(idPrefix + "LastName"));
        assertEquals("123 Main Street", selenium.getValue(idPrefix + "Line1"));
        assertEquals("", selenium.getValue(idPrefix + "Line2"));
        assertEquals("Anytown", selenium.getValue(idPrefix + "City"));
        assertEquals("Maryland", selenium.getSelectedLabel(idPrefix + "State"));
        assertEquals("12345", selenium.getValue(idPrefix + "Zip"));
        assertEquals("United States", selenium.getSelectedLabel(idPrefix + "Country"));
        assertEquals(ClientProperties.getResearcherEmail(), selenium.getValue(idPrefix + "Email"));
        assertEquals("1234567890", selenium.getValue(idPrefix + "Phone"));
        assertEquals(ClientProperties.getResearcherInstitution(), selenium.getValue(idPrefix + "Organization"));
        if (ClientProperties.isUserResumeRequired() && "investigator".equals(idPrefix)) {
            assertFalse(selenium.isVisible("newresume"));
            assertTrue(selenium.isVisible("existingresume"));
            assertTrue(StringUtils.isBlank(selenium.getValue("resume")));
        }
    }

    /**
     * test navigation between the various request submission pages.
     *
     * @throws Exception on error
     */
    protected void navigation() throws Exception {
        simpleAddToCart(CART_LABEL, true);
        clickAndWait("btn_request");
        verifyRequestProcessStepDisplay("proposal");
        enterData(true, false);
        clickAndWait("btn_save");
        if (ClientProperties.isMtaActive()) {
            assertTrue(selenium.isElementPresent("link=Back"));
            clickAndWait("link=Back");
            assertTrue(selenium.getTitle().contains("Research Proposal"));
            clickAndWait("btn_save");
            certifyMta(RECIPIENT_MTA_STATUS_PENDING, true);
        }
        assertTrue(selenium.getTitle().contains("Confirm Your Request"));
        verifyRequestProcessStepDisplay("confirm");
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        verifyNoGeneralInformation();
        clickAndWait("link=Go Back To Edit");
        assertTrue(selenium.getTitle().contains("Research Proposal"));
        verifyRequestProcessStepDisplay("proposal");
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        verifyFieldValues(true);

        assertFalse(selenium.isElementPresent("link=Revise " + ClientProperties.getCartLink()));
        assertTrue(selenium.isElementPresent("link=Back"));
        clickAndWait("link=Back");
        assertTrue(selenium.getTitle().contains(ClientProperties.getCartLink()));
        verifyRequestProcessStepDisplay("request");
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        clickAndWait("btn_request");
        verifyRequestProcessStepDisplay("proposal");
        verifyFieldValues(true);
        clickAndWait("btn_save");
        if (ClientProperties.isMtaActive()) {
            assertTrue(selenium.isTextPresent("Research Proposal - Continued"));
            clickAndWait("btn_save");
        }
        assertTrue(selenium.getTitle().contains("Confirm Your Request"));
        verifyRequestProcessStepDisplay("confirm");
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        verifyNoGeneralInformation();
        clickAndWait("btn_save_request");
        verifyRequestProcessStepDisplay("receipt");
        assertTrue(selenium.getTitle().contains("Thank You"));
        assertTrue(selenium.isTextPresent("Thank you. Your request has been submitted and a unique "
                + "identifier has been generated."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        clickAndWait("link=Sign Out");
    }

    private void verifyNoGeneralInformation() {
        assertFalse(selenium.isTextPresent("5. General Information"));
        assertFalse(selenium.isTextPresent("6. General Information"));
        assertFalse(selenium.isElementPresent("general_info"));
        assertFalse(selenium.isTextPresent("Additional Criteria Not Listed Above"));
        assertFalse(selenium.isTextPresent("test general comment"));
    }

    /**
     * add to the cart without a lot of verification.
     *
     * @param cartLabel cart label.
     * @param setQuantity whether a quantity needs to be set for the specimen.
     */
    protected void simpleAddToCart(String cartLabel, boolean setQuantity) {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        verifyRequestProcessStepDisplay("search");
        addSearchCriteria();
        clickAndWait("btn_search");
        verifyRequestProcessStepDisplay("select");
        selenium.click("xpath=//table[@id='specimen']/tbody/tr[1]/td[1]/input[1]");
        clickAndWait("btn_addtocart");
        verifyRequestProcessStepDisplay("request");
        if (setQuantity) {
            selenium.type(cartLabel + "_lineItems_0__quantity", "100");
        }
    }

    /**
     * Verifies field values.
     *
     * @param exemptFromApproval whether the request is exempt from approval
     */
    protected void verifyFieldValues(boolean exemptFromApproval) {
        assertEquals("test-protocol-title", selenium.getValue("protocolTitle"));
        verifyFundedFieldValues();
        assertTrue(selenium.isChecked("preliminary_YES"));
        if (exemptFromApproval) {
            assertEquals("test-irb-name", selenium.getValue("exempt_irbName"));
            assertEquals("test-irb-registration-number", selenium.getValue("exempt_irbRegistrationNumber"));
        } else {
            assertEquals("test-irb-name", selenium.getValue("approved_irbName"));
            assertEquals("test-irb-registration-number", selenium.getValue("approved_irbRegistrationNumber"));
        }

        if (exemptFromApproval) {
            assertTrue(selenium.isElementPresent("link=exemption.txt"));
        } else {
            assertEquals("01/01/10", selenium.getValue("approved_irbApprovalExpirationDate"));
            assertEquals("test-irb-approval-number", selenium.getValue("approved_irbApprovalNumber"));
        }
        assertTrue(selenium.isElementPresent("link=testui.properties.example"));
        assertEquals("test-study-abstract", selenium.getValue("abstract"));

        assertTrue(selenium.isElementPresent("link=test.properties"));
        verifyPersonFieldValues("investigator", "Maryland", "Canada",
                ClientProperties.getDefaultConsortiumMemberName());
        if (ClientProperties.getDisplayPILegalFields()) {
            assertTrue(selenium.isChecked("certified_YES"));
            assertTrue(selenium.isChecked("investigated_YES"));
            assertEquals("investigation comment", selenium.getValue("investigationComment"));
        }

        assertEquals("test-instructions", selenium.getValue("shippingInstructions"));

        verifyPersonFieldValues("recipient", "Virginia", "Mexico", ClientProperties.getShipmentRecipientInstitution());
    }

    private void verifyPersonFieldValues(String idPrefix, String state, String country, String institutionName) {
        assertEquals("test-first-name-" + idPrefix, selenium.getValue(idPrefix + "FirstName"));
        assertEquals("test-last-name-" + idPrefix, selenium.getValue(idPrefix + "LastName"));
        assertEquals("test-line1-" + idPrefix, selenium.getValue(idPrefix + "Line1"));
        assertEquals("test-line2-" + idPrefix, selenium.getValue(idPrefix + "Line2"));
        assertEquals("test-city-" + idPrefix, selenium.getValue(idPrefix + "City"));
        assertEquals(state, selenium.getSelectedLabel(idPrefix + "State"));
        assertEquals(StringUtils.left(idPrefix, ZIP_LENGTH), selenium.getValue(idPrefix + "Zip"));
        assertEquals(country, selenium.getSelectedLabel(idPrefix + "Country"));
        assertEquals("test-email-" + idPrefix + "@email.com", selenium.getValue(idPrefix + "Email"));
        assertEquals(StringUtils.left(idPrefix, PHONE_LENGTH), selenium.getValue(idPrefix + "Phone"));
        assertEquals(StringUtils.left("ex" + idPrefix, EXTENSION_LENGTH),
                selenium.getValue(idPrefix + "PhoneExtension"));
        assertEquals(institutionName, selenium.getValue(idPrefix + "Organization"));
    }

    /**
     * Verify the values of the fields associated with the funded status. Subclasses should overrie to handle dynamic
     * extensions associated with the funded status
     */
    protected void verifyFundedFieldValues() {
        List<String> selections = Arrays.asList(selenium.getSelectedLabels("object.study.fundingSources"));
        assertTrue(selections.contains("Foundation (not peer reviewed)"));
        assertTrue(selections.contains("Other"));
    }

    /**
     * Collapses and uncollapses all sections.
     */
    protected void collapseSections() {
        collapseSection("1. Study & Protocol Details", "study_details");
        collapseSection("2. Principal Investigator", "principal_investigator");
        collapseSection("3. Shipping Information", "shipping_details");
        collapseSection("4. Shipment Recipient", "shipping_address");
        int sectionCount = BILLING_ADDRESS_SECTION;
        if (ClientProperties.isDisplayShipmentBillingRecipient()
                && selenium.isElementPresent("billing_address")) {
            collapseSection(sectionCount + ". Billing Address", "billing_address");
            sectionCount++;
        }
        if (selenium.isElementPresent("status_details")) {
            collapseSection(sectionCount + ". Status History", "status_details");
            sectionCount++;
        }
        collapseSection(sectionCount + ". General Information", "general_info");

    }

    /**
     * collapse the main sections of the page.
     */
    protected void collapseMainSections() {
        collapseSection(ClientProperties.getBiospecimenTerm() + "s in Request*", "cart");
        collapseSection("Research Proposal", "requestform");
    }

    /**
     * verify that requested specimens are unavailable.
     */
    protected void verifySpecimenUnavailable() {
        loginAsUser1();
        clickAndWait("link=My Requests");
        String linkXpath = "xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a";
        if (!selenium.isElementPresent(linkXpath)) {
            selenium.select("status", "label=Pending");
            waitForPageToLoad();
        }
        clickAndWait(linkXpath);
        String[] specimenCellContent = selenium.getTable("request.1.0").split("\\s+");
        String expectedHeader = StringUtils.join(specimenCellContent, "", 1, specimenCellContent.length);
        clickAndWait("xpath=//table[@id='request']/tbody/tr[1]/td[1]/a");
        String actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
        assertFalse(selenium.isElementPresent("add_to_cart"));
        assertTrue(selenium.isElementPresent("add_to_cart_disabled"));
        clickAndWait("link=Sign Out");
    }

    /**
     * Verify a request that has been submitted in error can be edited.
     *
     * @throws Exception on error
     */
    protected void verifyEditErrorRequest() throws Exception {
        loginAsUser1();
        clickAndWait("link=" + ClientProperties.getCartLink());
        clickAndWait("btn_request");
        enterData(false, true);
        clickAndWait("btn_save");
        if (ClientProperties.isMtaActive()) {
            certifyMta(RECIPIENT_MTA_STATUS_PENDING, true);
        }
        clickAndWait("btn_save_request");
        assertFalse(selenium.isTextPresent("Thank you"));
        assertTrue(selenium.isTextPresent("At least one " + ClientProperties.getSpecimenTermLowerCase()
                + " must be selected"));
        simpleAddToCart(CART_LABEL, true);
        clickAndWait("btn_request");
        if (ClientProperties.isAggregateSearchResultsActive()) {
            assertTrue(selenium.isTextPresent(ClientProperties.getInstitutionTerm()));
        } else {
            assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + "s in Request"));
        }
        assertTrue(selenium.isTextPresent("Research Proposal"));
        clickAndWait("link=Sign Out");
    }

    /**
     * verify request pending revision home page message.
     *
     * @throws Exception on error
     */
    protected void pendingRevisionMessage() throws Exception {
        runSqlScript("SpecimenRequestAndReviewTest-update.sql");
        loginAsUser1();
        assertTrue(selenium.isTextPresent("2 " + ClientProperties.getBiospecimenTerm() + " Requests Requiring Action"));
        clickAndWait("link=2 " + ClientProperties.getBiospecimenTerm() + " Requests Requiring Action");
        assertTrue(selenium.isTextPresent("My Requests"));
        selenium.select("status", "label=Revision Requested");
        waitForPageToLoad();
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        String urlHackPath = "http://" + TestProperties.getServerHostname() + ":" + TestProperties.getServerPort()
                + selenium.getAttribute("xpath=//table[@id='specimenRequest']/tbody/tr[2]/td[8]/a@href");
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[2]/td[8]/a");

        clickAndWait("btn_save");
        if (ClientProperties.isMtaActive()) {
            certifyMta(RECIPIENT_MTA_STATUS_PENDING, false);
        }
        assertTrue(selenium.getTitle().contains("Confirm Your Request"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        clickAndWait("btn_save_request");
        assertTrue(selenium.getTitle().contains("Thank You"));
        assertTrue(selenium.isTextPresent("Thank you. Your revision has been successfully submitted."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        clickAndWait("link=Home");
        assertTrue(selenium.isTextPresent("1 " + ClientProperties.getBiospecimenTerm() + " Request Requiring Action"));

        testNonPendingRequestUrlHack(urlHackPath);
    }

    private void testNonPendingRequestUrlHack(String editRequestUrl) {
        clickAndWait("link=Sign Out");
        loginAsUser1();
        clickAndWait("link=My Requests");
        assertTrue(selenium.isTextPresent("My Requests"));

        String nonPendingRequestId = selenium.getTable("specimenRequest.3.0");
        selenium.open(editRequestUrl.substring(0, editRequestUrl.indexOf("=") + 1) + nonPendingRequestId);
        assertTrue(selenium.isTextPresent("Selected " + ClientProperties.getSpecimenTermLowerCase()
                + " request is not editable"));
    }

    /**
     * view a submitted request.
     *
     * @param showComment whether the comment should be shown
     * @param showVote whether the final vote should be shown
     * @param showSpecimen whether the specimen should be shown
     * @param statusHistory Expected status history.
     */
    protected void viewRequest(boolean showComment, boolean showVote, boolean showSpecimen, String[] statusHistory) {
        clickAndWait("link=My Requests");
        assertTrue(selenium.isTextPresent("My Requests"));
        assertEquals("Revision Requested", selenium.getSelectedLabel("status"));
        assertFalse(selenium.isTextPresent("Delete Draft"));
        String xpath = "xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[1]/a";
        if (!selenium.isElementPresent(xpath)) {
            if (selenium.isChecked("actionNeededOnly")) {
                selenium.click("actionNeededOnly");
                waitForPageToLoad();
            }
            selenium.select("status", "label=Pending");
            waitForPageToLoad();
        }
        clickAndWait(xpath);
        if (ClientProperties.isAggregateSearchResultsActive()) {
            assertTrue(selenium.isTextPresent(ClientProperties.getInstitutionTerm()));
        } else {
            assertTrue(selenium.isTextPresent(ClientProperties.getBiospecimenTerm() + "s in Request"));
        }
        assertTrue("Expected text 'Research Proposal' not found, body text: "
                + selenium.getBodyText(), selenium.isTextPresent("Research Proposal"));
        collapseMainSections();

        confirmText(showComment, showVote, false, "/protected/downloadFile", statusHistory);
        assertEquals(showComment, selenium.isTextPresent("Revision Comments"));
        assertEquals(showComment, selenium.isTextPresent("revision comment text"));

        assertEquals(showVote, selenium.isTextPresent("Consortium's Review Decision"));
        assertEquals(showVote, selenium.isTextPresent(ClientProperties.getFinalVoteLabel() + ": Revision Requested"));
        assertEquals(showVote, selenium.isTextPresent(ClientProperties.getFinalCommentLabel()));
        assertEquals(showVote, selenium.isTextPresent("this is the external comment"));

        assertEquals(showSpecimen, selenium.isTextPresent(ID_ONE));
        assertEquals(showSpecimen, selenium.isTextPresent(ID_TWO));
        assertEquals(showSpecimen, selenium.isTextPresent(ID_THREE));
    }

    /**
     * edit the request.
     *
     * @param linkCol the link column
     * @throws Exception on error.
     */
    protected void editRequest(int linkCol) throws Exception {
        clickAndWait("link=My Requests");
        clickAndWait("xpath=//table[@id='specimenRequest']/tbody/tr[1]/td[8]/a");
        assertTrue(selenium.isTextPresent("Your Revision Comments"));
        assertTrue(selenium.isTextPresent("You may add comments on the revisions"));
        assertTrue(selenium.isTextPresent("you are making to this proposal here"));
        assertTrue(selenium.isElementPresent("revisionComment"));
        clickAndWait("link=" + ClientProperties.getCartLink());
        clickAndWait("xpath=//table[@id='lineItem']/tbody/tr[1]/td[" + linkCol + "]/a");
        clickAndWait("btn_request");
        verifyExternalCommentDisplay(true);
        assertTrue(selenium.isElementPresent("link=Revise " + ClientProperties.getCartLink()));
        assertFalse(selenium.isElementPresent("link=Back"));
        assertTrue(selenium.isTextPresent("To revise proposal details, edit the form below and click Continue"));
        assertTrue(selenium.isTextPresent("Note, you will be able to confirm all edits in the next step."));
        assertFalse(selenium.isTextPresent("Delete Draft"));
        clickAndWait("link=Revise " + ClientProperties.getCartLink());
        assertTrue(selenium.getTitle().contains(ClientProperties.getCartLink()));
        verifyExternalCommentDisplay(true);
        clickAndWait("btn_request");
        assertTrue(selenium.getTitle().contains("Research Proposal"));
        verifyExternalCommentDisplay(true);
        selenium.type("revisionComment", "revision comment text");
        clickAndWait("btn_save");
        if (ClientProperties.isMtaActive()) {
            certifyMta(RECIPIENT_MTA_STATUS_PENDING, false);
        }
        assertTrue(selenium.getTitle().contains("Confirm Your Request"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Revision Comments"));
        assertTrue(selenium.isTextPresent("revision comment text"));
        verifyExternalCommentDisplay(true);
        String history = selenium.getText("status_details");
        String[] histories = StringUtils.stripAll(StringUtils.split(history, '\n'));
        String[] statusHistory = new String[]{"Draft", "Pending"};
        assertEquals(statusHistory.length, histories.length);
        String dateString = new SimpleDateFormat("(MM/dd/yyyy)").format(new Date());
        for (int i = 0; i < statusHistory.length; i++) {
            assertTrue(histories[i].startsWith(statusHistory[i]));
            assertTrue(histories[i].endsWith(dateString));
        }
        clickAndWait("btn_save_request");
        assertTrue(selenium.getTitle().contains("Thank You"));
        assertTrue(selenium.isTextPresent("Thank you. Your revision has been successfully submitted."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
    }

    /**
     * Verify the external comments.
     *
     * @param present whether or not the comment is present
     */
    protected void verifyExternalCommentDisplay(boolean present) {
        assertEquals(present, selenium.isTextPresent("Review Decision of the Consortium"));
        assertEquals(present, selenium.isTextPresent("Your proposal has been given a status of Revision Requested."));
        assertEquals(present, selenium.isTextPresent("Comments from the consortium are as follows:"));
        assertEquals(present, selenium.isTextPresent("this is the external comment"));
    }

    /**
     * verify teh display of the external comment on the review page.
     */
    protected void verifyReviewExternalCommentDisplay() {
        assertTrue(selenium.isTextPresent("Previous Review Decision of the Consortium"));
        assertTrue(selenium.isTextPresent("This proposal previously was given a status of Revision Requested."));
        assertTrue(selenium.isTextPresent("Comments from the consortium are as follows:"));
        assertTrue(selenium.isTextPresent("this is the external comment"));
        assertTrue(selenium.isTextPresent("Comments from the researcher on revision are as follows:"));
        assertTrue(selenium.isTextPresent("revision comment text"));
        assertFalse(selenium.isTextPresent("Consortium's Review Decision"));
    }

    /**
     * verify the requirement that at least one specimen or prospective collection notes be selected.
     *
     * @throws Exception on error
     */
    protected void verifyRequiredSpecimenValidation() throws Exception {
        clickAndWait("link=Sign Out");
        loginAsUser1();
        clickAndWait("link=" + ClientProperties.getCartLink());
        assertTrue(selenium.getTitle().contains(ClientProperties.getCartLink()));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        clickAndWait("btn_request");
        assertTrue(selenium.getTitle().contains("Research Proposal"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        enterData(true, true);
        clickAndWait("btn_save");
        if (ClientProperties.isMtaActive()) {
            certifyMta(RECIPIENT_MTA_STATUS_PENDING, true);
        }
        assertTrue(selenium.getTitle().contains("Confirm Your Request"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        clickAndWait("btn_save_request");
        assertTrue(selenium.getTitle().contains(ClientProperties.getCartLink()));
        if (ClientProperties.getDisplayProspectiveCollection()) {
            assertTrue(selenium.isTextPresent(ClientProperties.getProspectiveCollectionRequired()));
        } else {
            assertTrue(selenium.isTextPresent("At least one " + ClientProperties.getSpecimenTermLowerCase()
                    + " must be selected. Please update and resubmit your request."));
        }
        clickAndWait("link=Sign Out");
    }

    /**
     * get the number of orders in the system.
     *
     * @return a string listing the number of orders in the system
     */
    protected String getOrderCount() {
        if (!ClientProperties.isOrderAdministrationActive()) {
            return null;
        }
        login(ClientProperties.getTissueTechEmail(), PASSWORD);
        goToOrderList();
        selenium.select("status", "label=All");
        waitForPageToLoad();
        String orderCount = selenium.getText(ORDER_COUNT_XPATH);
        clickAndWait("link=Sign Out");
        return orderCount;
    }

    /**
     * verify that the order count depending on whether or not it is expected to have been updated.
     *
     * @param orderCount the order count
     */
    protected void verifyOrderCount(String orderCount) {
        if (!ClientProperties.isOrderAdministrationActive()) {
            return;
        }
        login(ClientProperties.getTissueTechEmail(), PASSWORD);
        goToOrderList();
        selenium.select("status", "label=All");
        waitForPageToLoad();
        assertNotSame(orderCount, selenium.getText(ORDER_COUNT_XPATH));
        clickAndWait("link=Sign Out");
    }

    /**
     * Verify the mta certification page for recipient mtas with various statuses.
     *
     * @throws Exception on error.
     */
    protected void verifyMtaStatus() throws Exception {
        clickAndWait("link=Sign Out");
        login("test-user-inst@example.com", "tissueLocator1");
        simpleAddToCart(CART_LABEL, false);
        clickAndWait("btn_request");
        enterData(false, true);
        clickAndWait("btn_save");
        certifyMta(RECIPIENT_MTA_STATUS_VALID, true);
        assertTrue(selenium.isTextPresent("Confirm Your Request"));
        clickAndWait("link=Sign Out");
        login("test-reviewer@example.com", "tissueLocator1");
        simpleAddToCart(CART_LABEL, false);
        clickAndWait("btn_request");
        enterData(false, true);
        clickAndWait("btn_save");
        certifyMta(RECIPIENT_MTA_STATUS_INVALID, true);
        assertTrue(selenium.isTextPresent("Confirm Your Request"));
        clickAndWait("link=Sign Out");
    }

    /**
     * Verify the 'additional request' button functionality.
     */
    protected void verifyAdditionalRequests() {
        clickAndWait("link=Sign Out");
        login("test-user-inst@example.com", "tissueLocator1");
        simpleAddToCart(CART_LABEL, false);
        clickAndWait("link=" + ClientProperties.getCartSearchMore());
        assertTrue(selenium.isVisible("filters"));
        clickAndWait("link=Sign Out");
    }

    /**
     * @return The warning shown for unapproved requests.
     */
    protected String getNoIrbApprovalNote() {
        return "If the research involves human subjects, it may require IRB review before the "
                + ClientProperties.getBiospecimenTermLowerCase() + "s may be provided.";
    }

    /**
     * Verify the request process step display.
     * @param step the current step
     */
    protected void verifyRequestProcessStepDisplay(String step) {
        boolean displayProgress = ClientProperties.isDisplayRequestProcessSteps();
        assertEquals(displayProgress, selenium.isElementPresent("process_steps"));
        if (displayProgress) {
            String leftcapId = "process_steps_leftcap";
            String searchId = "process_steps_search";
            String searchArrowId = "process_steps_search_arrow";
            String selectId = "process_steps_select";
            String selectArrowId = "process_steps_select_arrow";
            String requestId = "process_steps_request";
            String requestArrowId = "process_steps_request_arrow";
            String proposalId = "process_steps_proposal";
            String proposalArrowId = "process_steps_proposal_arrow";
            String confirmId = "process_steps_confirm";
            String confirmArrowId = "process_steps_confirm_arrow";
            String receiptId = "process_steps_receipt";
            String rightcapId = "process_steps_rightcap";
            if ("search".equals(step)) {
                leftcapId += "_selected";
                searchId += "_selected";
                searchArrowId += "_selected_left";
            } else if ("select".equals(step)) {
                searchArrowId += "_selected_right";
                selectId += "_selected";
                selectArrowId += "_selected_left";
            } else if ("request".equals(step)) {
                selectArrowId += "_selected_right";
                requestId += "_selected";
                requestArrowId += "_selected_left";
            } else if ("proposal".equals(step)) {
                requestArrowId += "_selected_right";
                proposalId += "_selected";
                proposalArrowId += "_selected_left";
            } else if ("confirm".equals(step)) {
                proposalArrowId += "_selected_right";
                confirmId += "_selected";
                confirmArrowId += "_selected_left";
            } else if ("receipt".equals(step)) {
                confirmArrowId += "_selected_right";
                receiptId += "_selected";
                rightcapId += "_left_selected";
            }
            String[] processStepIds = new String[] {leftcapId, searchId, searchArrowId, selectId, selectArrowId,
                    requestId, requestArrowId, proposalId, proposalArrowId, confirmId, confirmArrowId,
                    receiptId, rightcapId};
            for (String processStepId : processStepIds) {
                assertTrue(selenium.isElementPresent(processStepId));
            }
        }
    }

    private void clearPersonData(String idPrefix) {
        selenium.type(idPrefix + "FirstName", "");
        selenium.type(idPrefix + "LastName", "");
        selenium.type(idPrefix + "Line1", "");
        selenium.type(idPrefix + "Line2", "");
        selenium.type(idPrefix + "City", "");
        selenium.select(idPrefix + "State", "label=- Select -");
        selenium.type(idPrefix + "Zip", "");
        selenium.select(idPrefix + "Country", "label=Canada");
        selenium.type(idPrefix + "Email", "");
        selenium.type(idPrefix + "Phone", "");
        selenium.type(idPrefix + "PhoneExtension", "");
        selenium.type(idPrefix + "Organization", "");
    }

    private void verifyPrepopulatedPersonData(String idPrefix) {
        assertTrue(StringUtils.isNotBlank(selenium.getValue(idPrefix + "FirstName")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue(idPrefix + "LastName")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue(idPrefix + "Line1")));
        assertTrue(StringUtils.isBlank(selenium.getValue(idPrefix + "Line2")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue(idPrefix + "City")));
        assertNotSame("- Select -", selenium.getSelectedLabel(idPrefix + "State"));
        assertTrue(StringUtils.isNotBlank(selenium.getValue(idPrefix + "Zip")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue(idPrefix + "Email")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue(idPrefix + "Phone")));
        assertTrue(StringUtils.isBlank(selenium.getValue(idPrefix + "PhoneExtension")));
        assertTrue(StringUtils.isNotBlank(selenium.getValue(idPrefix + "Organization")));
    }
}
