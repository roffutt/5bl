/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author ddasgupta
 *
 */
public class AbrcSpecimensByInstitutionReportTest extends AbstractTissueLocatorSeleniumTest {

    private static final String DATA_XPATH =
        "//div[@id='requestform']/div[@id='reportcontent']/table/tbody/tr[%d]/td[%d]/span";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "UsageReportTest.sql";
    }

    /**
     * test the specimens by institution report page.
     */
    public void testSpecimensByInstitutionReport() {
        goToReportPage();
        verifyData();
    }

    private void goToReportPage() {
        loginAsAdmin();
        mouseOverAndPause("link=Reports");
        clickAndWait("link=Specimens By Institution Report");
        assertTrue(selenium.isTextPresent("Specimens By Institution Report"));
    }

    private void verifyData() {
        //CHECKSTYLE:OFF - magic numbers
        assertEquals("Maricopa Integrated Health System:", selenium.getText(String.format(DATA_XPATH, 3, 2)));
        assertEquals("3", selenium.getText(String.format(DATA_XPATH, 3, 3)));
        assertEquals("Gender Breakdown", selenium.getText(String.format(DATA_XPATH, 3, 4)));
        assertEquals("female", selenium.getText(String.format(DATA_XPATH, 4, 2)));
        assertEquals("1", selenium.getText(String.format(DATA_XPATH, 4, 3)));
        assertEquals("male", selenium.getText(String.format(DATA_XPATH, 5, 2)));
        assertEquals("1", selenium.getText(String.format(DATA_XPATH, 5, 3)));
        assertEquals("unspecified", selenium.getText(String.format(DATA_XPATH, 6, 2)));
        assertEquals("1", selenium.getText(String.format(DATA_XPATH, 6, 3)));

        assertEquals("Phoenix Children's Hospital:", selenium.getText(String.format(DATA_XPATH, 7, 2)));
        assertEquals("7", selenium.getText(String.format(DATA_XPATH, 7, 3)));
        assertEquals("Gender Breakdown", selenium.getText(String.format(DATA_XPATH, 7, 4)));
        assertEquals("female", selenium.getText(String.format(DATA_XPATH, 8, 2)));
        assertEquals("2", selenium.getText(String.format(DATA_XPATH, 8, 3)));
        assertEquals("male", selenium.getText(String.format(DATA_XPATH, 9, 2)));
        assertEquals("3", selenium.getText(String.format(DATA_XPATH, 9, 3)));
        assertEquals("unspecified", selenium.getText(String.format(DATA_XPATH, 10, 2)));
        assertEquals("2", selenium.getText(String.format(DATA_XPATH, 10, 3)));

        assertEquals("Total:", selenium.getText(String.format(DATA_XPATH, 13, 2)));
        assertEquals("10", selenium.getText(String.format(DATA_XPATH, 13, 3)));

        String imgXpath = "xpath=//div[@id='requestform']/div[@id='reportcontent']/table/tbody/tr[14]/td[2]/img";
        assertTrue(selenium.isElementPresent(imgXpath));
        //CHECKSTYLE:ON
    }
}
