/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.main;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author smiller
 */
public class CollectionProtocolEditTest extends AbstractTissueLocatorSeleniumTest {

    /**
     * Tests specimen management.
     */
    @Test
    public void testProtocolCreateAndEdit() {
        if (ClientProperties.isProtocolAdministrationActive()) {
            int expectedResults = ClientProperties.getCollectionProtocolCount();
            goToProtocolPage(expectedResults);
            requiredValidation();
            formatValidation();
            create(expectedResults + 1);
            duplicateValidation();
            edit(expectedResults + 1);
            createAsTech(expectedResults + 2);
        }
    }

    /**
     * Go to protocol page.
     * @param resultCount the number of results
     */
    private void goToProtocolPage(int resultCount) {
        loginAsAdmin();
        mouseOverAndPause("link=Administration");

        // go to list page
        clickAndWait("link=Protocol Administration");
        assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));

        // go to edit page
        clickAndWait("link=Add New Collection Protocol");
        assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
        assertFalse(selenium.isTextPresent("Search By Name"));

        // use cancel link
        clickAndWait("link=Cancel");
        assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));

        // go to edit page
        clickAndWait("link=Add New Collection Protocol");
        assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
        assertFalse(selenium.isTextPresent("Search By Name"));

        // use back to list link
        clickAndWait("link=Back To List");
        assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));

        resultsCount(resultCount, PAGE_SIZE);

        // go to edit page
        clickAndWait("link=Add New Collection Protocol");
        assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
        assertFalse(selenium.isTextPresent("Search By Name"));
    }

    /**
     * Test required validation.
     */
    private void requiredValidation() {
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Name must be set"));
        assertTrue(selenium.isTextPresent("You must select a valid "
                + ClientProperties.getInstitutionTerm().toLowerCase() + "."));
    }

    /**
     * test format validation.
     */
    private void formatValidation() {
        selenium.type("email", "invalid");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Name must be set"));
        assertTrue(selenium.isTextPresent("You must select a valid "
                + ClientProperties.getInstitutionTerm().toLowerCase() + "."));
        assertTrue(selenium.isTextPresent("Email is not a well-formed email address"));
    }

    /**
     * Create a collection protocol.
     * @param resultCount the number of results after creation
     */
    private void create(int resultCount) {
        enterData();
        assertTrue(selenium.isTextPresent("Collection Protocol successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));
        resultsCount(resultCount, PAGE_SIZE);
        assertTrue(selenium.isTextPresent("test-123456789"));
    }


    private void enterData() {
        selenium.type("name", "test-123456789");
        String instName = StringUtils.split(ClientProperties.getDefaultConsortiumMemberName())[0].toLowerCase();
        selectFromAutocomplete("institution", instName, ClientProperties.getDefaultConsortiumMemberName());
        selenium.type("startDate", "10/10/2009");
        selenium.type("endDate", "10/12/2009");
        selenium.type("firstName", "fname");
        selenium.type("lastName", "lname");
        selenium.type("email", "fname.lname@example.com");
        selenium.type("phone", "1234567890");
        selenium.type("phoneExtension", "1234");
        clickAndWait("btn_save");
    }

    /**
     * Try to create a duplicate.
     */
    private void duplicateValidation() {
        clickAndWait("link=Add New Collection Protocol");
        enterData();
        assertTrue(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("A collection protocol exists with this name for this institution."));
    }

    /**
     * Edit a collection protocol.
     * @param resultCount The number of results after edit
     */
    private void edit(int resultCount) {
        clickAndWait("link=Cancel");
        assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
        assertTrue(selenium.isTextPresent("Search By Name"));

        resultsCount(resultCount, PAGE_SIZE);

        clickAndWait("link=test-123456789");
        assertEquals("test-123456789", selenium.getValue("name"));
        assertEquals("fname", selenium.getValue("firstName"));
        assertEquals("lname", selenium.getValue("lastName"));
        assertEquals("fname.lname@example.com", selenium.getValue("email"));
        assertEquals("1234567890", selenium.getValue("phone"));
        assertEquals("1234", selenium.getValue("phoneExtension"));
        selenium.type("name", "test-123456789-updated");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Collection Protocol successfully saved."));
        assertTrue(selenium.isTextPresent("Search By Name"));

        resultsCount(resultCount, PAGE_SIZE);
        assertTrue(selenium.isTextPresent("test-123456789-updated"));
        clickAndWait("link=Sign Out");
    }

    /**
     * Create at tech.
     * @param resultCount the number of results after creation.
     */
    private void createAsTech(int resultCount) {
        login(ClientProperties.getTissueTechEmail(), "tissueLocator1");
        mouseOverAndPause("link=Administration");
        clickAndWait("link=Protocol Administration");
        clickAndWait("link=Add New Collection Protocol");
        selenium.type("name", "test-tech");
        clickAndWait("btn_save");
        assertTrue(selenium.isTextPresent("Collection Protocol successfully saved."));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent("Collection Protocol Administration"));
        resultsCount(resultCount, PAGE_SIZE);
        assertTrue(selenium.isTextPresent("test-tech"));
    }

}
