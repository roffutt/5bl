/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;


import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractTissueLocatorSeleniumTest;

/**
 * @author ddasgupta
 *
 */
public class NbstrnCartTest extends AbstractTissueLocatorSeleniumTest {

    private static final String GENERAL_ROW_XPATH = "xpath=//tr[@id='general_data%d']/td[%d]/a";
    private static final String SPECIFIC_ROW_XPATH = "xpath=//tr[@id='specific_data%d']/td[%d]/a";
    private static final int BUTTON_COL_INDEX = 5;
    private static final int ROW_1_TOTAL = 5;
    private static final String MAX_REQUEST_ERROR = "The amount requested, %d"
        + ", was greater than the amount available, %d.";
    private static final String INVALID_REQUEST_ERROR = "Please enter a positive whole number.";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "CartTest.sql";
    }

    /**
     * test the cart feature.
     */
    @Test
    public void testCart() {
        loginAsUser1();
        homePageMessage(0);
        addFromSearch(2);
        homePageMessage(2);
        addFromSearch(0);
        homePageMessage(2);
        requestValidation();
        update();
        remove();
        empty();
        sessionStorage();
        verifyCartSeparation();
    }

    private void homePageMessage(int cartSize) {
        clickAndWait("link=Home");
        assertTrue(selenium.isElementPresent("link=(" + cartSize + " Items)"));
    }

    private void addFromSearch(int numAdded) {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        selenium.type("min_collectionYear", "1950");
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        String textFieldXPath = "xpath=//table[@id='specimen']/tbody/tr[%d]/td[3]/input[3]";
        selenium.type(String.format(textFieldXPath, 1), Integer.toString(1));
        selenium.keyUp(String.format(textFieldXPath, 1), Integer.toString(1));
        selenium.type(String.format(textFieldXPath, 2), Integer.toString(1));
        selenium.keyUp(String.format(textFieldXPath, 2), Integer.toString(1));
        clickAndWait("btn_addtocart");
        assertTrue(selenium.isTextPresent("Added " + numAdded + " new request(s) to your selections."));
        verifyCart(false, getCaliforniaLineItem(), getNewYorkItem());
    }

    private void verifyCartSeparation() {
        clickAndWait("link=Sign Out");
        loginAsUser1();
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        selenium.type("min_collectionYear", "1950");
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-2 of 2 Results"));
        String textFieldXPath = "xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]";
        selenium.type(textFieldXPath, Integer.toString(1));
        selenium.keyUp(textFieldXPath, Integer.toString(1));
        clickAndWait("btn_addtocart");
        assertTrue(selenium.isElementPresent(String.format(GENERAL_ROW_XPATH, 0, BUTTON_COL_INDEX)));
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        selenium.select("multiSelectParamMap['externalIdAssigner.id']", "label=California");
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        selenium.type(textFieldXPath, Integer.toString(1));
        selenium.keyUp(textFieldXPath, Integer.toString(1));
        clickAndWait("btn_addtocart");
        assertTrue(selenium.isElementPresent(String.format(GENERAL_ROW_XPATH, 1, BUTTON_COL_INDEX)));
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        selenium.select("multiSelectParamMap['externalIdAssigner.id']", "label=California");
        selenium.select("multiSelectParamMap['participant.gender']", "label=Male");
        clickAndWait("btn_search");
        assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        selenium.type(textFieldXPath, Integer.toString(1));
        selenium.keyUp(textFieldXPath, Integer.toString(1));
        clickAndWait("btn_addtocart");
        assertTrue(selenium.isElementPresent(String.format(GENERAL_ROW_XPATH, 0, BUTTON_COL_INDEX)));
        assertTrue(selenium.isElementPresent(String.format(GENERAL_ROW_XPATH, 1, BUTTON_COL_INDEX)));
        assertTrue(selenium.isElementPresent(String.format(SPECIFIC_ROW_XPATH, 0, BUTTON_COL_INDEX)));
    }

    private void requestValidation() {
        selectSearchMenu();
        clickAndWait("link=" + ClientProperties.getBiospecimenSearchLink());
        clickAndWait("btn_search");

        String textFieldXPath = "xpath=//table[@id='specimen']/tbody/tr[1]/td[3]/input[3]";
        selenium.type(textFieldXPath, Integer.toString(ROW_1_TOTAL + 1));
        selenium.keyUp(textFieldXPath, Integer.toString(ROW_1_TOTAL + 1));
        assertEquals(String.format(MAX_REQUEST_ERROR, ROW_1_TOTAL + 1, ROW_1_TOTAL), selenium.getAlert());
        assertEquals(Integer.toString(ROW_1_TOTAL), selenium.getValue(textFieldXPath));

        selenium.type(textFieldXPath, "-1");
        selenium.keyUp(textFieldXPath, "1");
        assertEquals(INVALID_REQUEST_ERROR, selenium.getAlert());
        assertEquals("", selenium.getValue(textFieldXPath));
        selenium.type(textFieldXPath, "2invalid");
        selenium.keyUp(textFieldXPath, "d");
        assertEquals(INVALID_REQUEST_ERROR, selenium.getAlert());
        assertEquals("", selenium.getValue(textFieldXPath));
    }

    private void update() {
        clickAndWait("link=" + ClientProperties.getCartLink());
        selenium.type("generalLineItemsArray[0].note", getCaliforniaLineItem().getNote());
        selenium.type("generalLineItemsArray[1].note", getNewYorkItem().getNote());
        clickAndWait("btn_update");
        assertTrue(selenium.isTextPresent("Selections successfully updated."));
        verifyCart(true, getCaliforniaLineItem(), getNewYorkItem());
    }

    private void remove() {
        clickAndWait("link=" + ClientProperties.getCartLink());
        clickAndWait(String.format(GENERAL_ROW_XPATH, 1, BUTTON_COL_INDEX));
        assertTrue(selenium.isTextPresent("Removed 1 request from your selections."));
        verifyCart(false, getCaliforniaLineItem());
    }

    private void empty() {
        clickAndWait("link=" + ClientProperties.getCartLink());
        clickAndWait("link=Empty My Selections");
        assertTrue(selenium.isTextPresent("Removed 1 request(s) from your selections."));
        verifyCart(false);
        assertTrue(selenium.isTextPresent("No " + ClientProperties.getBiospecimenTerm() + "s Selected."));
        clickAndWait("link=Empty " + ClientProperties.getCartLink());
        assertTrue(selenium.isTextPresent("Removed 0 request(s) from your selections."));
        verifyCart(false);
        assertTrue(selenium.isTextPresent("No " + ClientProperties.getBiospecimenTerm() + "s Selected."));
    }

    private void sessionStorage() {
        addFromSearch(2);
        clickAndWait("link=Home");
        clickAndWait("link=" + ClientProperties.getCartLink());
        verifyCart(false, getCaliforniaLineItem(), getNewYorkItem());
        clickAndWait("link=Sign Out");
        loginAsUser1();
        clickAndWait("link=" + ClientProperties.getCartLink());
        verifyCart(false);
    }

    private void verifyCart(boolean testNotes, AggregateSpecimenRequestLineItem... lineItems) {
        assertTrue(selenium.isTextPresent(ClientProperties.getCartLink()));
        if (lineItems.length == 0) {
            assertTrue(selenium.isTextPresent("No " + ClientProperties.getBiospecimenTerm() + "s Selected."));
        }
        String xpath = "xpath=//tr[@id='general_data%d']/td[%d]";
        for (int i = 0; i < lineItems.length; i++) {
            assertEquals(lineItems[i].getInstitution().getName(), selenium.getText(String.format(xpath, i, 1)));
            assertEquals(lineItems[i].getQuantity(), selenium.getText(String.format(xpath, i, 2)));
            assertEquals(lineItems[i].getCriteria(), selenium.getText(String.format(xpath, i, 2 + 1)));
            assertTrue(selenium.isElementPresent("generalLineItemsArray[" + i + "].note"));
            if (testNotes) {
                assertEquals(lineItems[i].getNote(), selenium.getValue("generalLineItemsArray[" + i + "].note"));
            }
            assertTrue(selenium.isElementPresent(String.format(GENERAL_ROW_XPATH, i, BUTTON_COL_INDEX)));
        }
        assertFalse(selenium.isElementPresent(String.format(GENERAL_ROW_XPATH, lineItems.length, BUTTON_COL_INDEX)));
    }

    private AggregateSpecimenRequestLineItem getCaliforniaLineItem() {
        AggregateSpecimenRequestLineItem lineItem = new AggregateSpecimenRequestLineItem();
        lineItem.setInstitution(new Institution());
        lineItem.getInstitution().setName("California");
        lineItem.setCriteria("Birth Year (yyyy): greater than or equal to 1950");
        lineItem.setNote("california note");
        lineItem.setQuantity(2);
        return lineItem;
    }

    private AggregateSpecimenRequestLineItem getNewYorkItem() {
        AggregateSpecimenRequestLineItem lineItem = new AggregateSpecimenRequestLineItem();
        lineItem.setInstitution(new Institution());
        lineItem.getInstitution().setName("New York");
        lineItem.setCriteria("Birth Year (yyyy): greater than or equal to 1950");
        lineItem.setQuantity(2 + 1);
        lineItem.setNote("ny note");
        return lineItem;
    }
}
