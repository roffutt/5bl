package com.fiveamsolutions.tissuelocator.test.selenium.nbstrn;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractSpecimenLocationTest;

/**
 *
 * @author aswift
 *
 */
public class NbstrnSpecimenLocationTest extends AbstractSpecimenLocationTest {

    /**
     * test the nbstrn specimen location.
     * @throws Exception on error
     */
    public void testNbstrnSpecimenLocation() throws Exception {

        /*
         * Log in as a standard tissue technician Find Specimen through Biospecimen Administration page
         */
        login(ClientProperties.getInstitutionalAdminEmail(), PASSWORD);
        validateBioSpecimenAdminRow(1, true, true);
        validateBioSpecimenAdminRow(THREE, true, true);
        validateBioSpecimenAdminRow(FOUR, true, true);
        clickAndWait("link=Sign Out");

        /*
         * Log in as an admin Find Specimen through Biospecimen Administration page
         */
        loginAsAdmin();
        validateBioSpecimenAdminRow(1, true, true);
        validateBioSpecimenAdminRow(THREE, true, true);
        validateBioSpecimenAdminRow(FOUR, true, true);
        clickAndWait("link=Sign Out");
    }
}
