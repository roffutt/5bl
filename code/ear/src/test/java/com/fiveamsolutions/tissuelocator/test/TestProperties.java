/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author smiller
 */
public class TestProperties {
    /**
     * @author smiller
     */
    enum PropertiesType {
        CommandLine("test.properties"), UI("testui.properties");

        private String propertyFileName;

        private PropertiesType(String propertyFileName) {
            this.propertyFileName = propertyFileName;
        }

        public String getPropertyFileName() {
            return propertyFileName;
        }

    }

    /**
     * the server hostname key.
     */
    public static final String SERVER_HOSTNAME_KEY = "server.hostname";

    /**
     * the server port key.
     */
    public static final String SERVER_PORT_KEY = "server.port";

    /**
     * the server jndi port key.
     */
    public static final String SERVER_JNDI_PORT_KEY = "server.jndi.port";

    /**
     * the server hostname default.
     */
    public static final String SERVER_HOSTNAME_DEFAULT = "localhost";

    /**
     * the server port default.
     */
    public static final String SERVER_PORT_DEFAULT = "8080";

    /**
     * the server jndi port default.
     */
    public static final String SERVER_JNDI_PORT_DEFAULT = "1099";

    /**
     * the selenium server port key.
     */
    public static final String SELENIUM_SERVER_PORT_KEY = "selenium.server.port";

    /**
     * the selenium server port default.
     */
    public static final String SELENIUM_SERVER_PORT_DEFAULT = "4444";

    /**
     * the selenium browser key.
     */
    public static final String SELENIUM_BROWSER_KEY = "selenium.browser";

    /**
     * the selenium browser default.
     */
    public static final String SELENIUM_BROWSER_DEFAULT = "*chrome";

    private static Properties properties = new Properties();
    static {
        try {
            InputStream stream = ClassLoader.getSystemClassLoader().getResourceAsStream(
                    PropertiesType.CommandLine.getPropertyFileName());
            properties.load(stream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return the hostname.
     */
    public static String getServerHostname() {
        return properties.getProperty(SERVER_HOSTNAME_KEY, SERVER_HOSTNAME_DEFAULT);
    }

    /**
     * @return the port.
     */
    public static int getServerPort() {
        return Integer.parseInt(properties.getProperty(SERVER_PORT_KEY, SERVER_PORT_DEFAULT));
    }

    /**
     * @return the jndi port
     */
    public static int getServerJndiPort() {
        return Integer.parseInt(properties.getProperty(SERVER_JNDI_PORT_KEY, SERVER_JNDI_PORT_DEFAULT));
    }

    /**
     * @return the selenium server port.
     */
    public static int getSeleniumServerPort() {
        return Integer.parseInt(properties.getProperty(SELENIUM_SERVER_PORT_KEY, SELENIUM_SERVER_PORT_DEFAULT));
    }

    /**
     * @return the selenium browser
     */
    public static String getSeleniumBrowser() {
        return properties.getProperty(SELENIUM_BROWSER_KEY, SELENIUM_BROWSER_DEFAULT);
    }

    /**
     * @return the properties object
     */
    public static Properties getProperties() {
        return properties;
    }
}
