/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.test.selenium.abrc;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;
import com.fiveamsolutions.tissuelocator.test.selenium.AbstractReportTest;

/**
 * @author ddasgupta
 *
 */
public class AbrcUsageReportTest extends AbstractReportTest {

    private static final String TITLE_XPATH =
        "xpath=//div[@id='requestform']/div[%d]/div[%d]/h3";
    private static final String SUBTITLE_XPATH =
        "xpath=//div[@id='requestform']/div[%d]/div[%d]/table/tbody/tr[1]/th/div";
    private static final String MULTI_TABLE_SUBTITLE_XPATH =
        "xpath=//div[@id='requestform']/div[%d]/div[%d]/table[%d]/tbody/tr[1]/th/div";
    private static final String METRIC_XPATH =
        "xpath=//div[@id='requestform']/div[%d]/div[%d]/table/tbody/tr[%d]/td[%d]";
    private static final String MULTI_TABLE_METRIC_XPATH = 
        "xpath=//div[@id='requestform']/div[%d]/div[%d]/table[%d]/tbody/tr[%d]/td[%d]";
    private static final String MULTI_TABLE_LINK_XPATH = 
        "xpath=//div[@id='requestform']/div[%d]/div[%d]/table[%d]/tbody/tr[%d]/td[%d]/a";
    
    /**
     * test the usage report page.
     */
    public void testUsageReport() {
        goToReportPage("Usage Report");
        changeDates("2010");
        verifyData();
        changeDates("2009");
        verifyNoData();
    }

    private void verifyData() {
        //CHECKSTYLE:OFF - magic numbers
        assertEquals("2", selenium.getText(String.format(METRIC_XPATH, 1, 1, 2, 2)));
        assertEquals("2", selenium.getText(String.format(METRIC_XPATH, 1, 1, 3, 2)));
        assertEquals("4", selenium.getText(String.format(METRIC_XPATH, 1, 1, 4, 2)));

        assertEquals("6", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 2, 2)));
        assertEquals("4", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 3, 2)));
        assertEquals("2", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 4, 2)));
        assertEquals("2", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 5, 2)));
        
        assertEquals("Biospecimen Return Requests by " + ClientProperties.getParticipantTerm(), 
                selenium.getText(String.format(MULTI_TABLE_SUBTITLE_XPATH, 1, 2, 2)));
        assertEquals("2", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 2, 2, 2)));
        assertEquals("test participant 1", selenium.getText(String.format(MULTI_TABLE_LINK_XPATH, 1, 2, 2, 2, 1)));
        assertEquals("1", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 2, 3, 2)));
        assertEquals("test participant 2", selenium.getText(String.format(MULTI_TABLE_LINK_XPATH, 1, 2, 2, 3, 1)));
        assertEquals("1", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 2, 4, 2)));
        assertEquals("test participant 3", selenium.getText(String.format(MULTI_TABLE_LINK_XPATH, 1, 2, 2, 4, 1)));

        assertEquals("Biospecimen Returns by " + ClientProperties.getParticipantTerm(), 
                selenium.getText(String.format(MULTI_TABLE_SUBTITLE_XPATH, 1, 2, 3)));
        assertEquals("1", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 3, 2, 2)));
        assertEquals("test participant 1", selenium.getText(String.format(MULTI_TABLE_LINK_XPATH, 1, 2, 3, 2, 1)));
        assertEquals("1", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 3, 3, 2)));
        assertEquals("test participant 3", selenium.getText(String.format(MULTI_TABLE_LINK_XPATH, 1, 2, 3, 3, 1)));
        
        assertEquals("Unfulfilled Biospecimen Return Requests by " + ClientProperties.getParticipantTerm(), 
                selenium.getText(String.format(MULTI_TABLE_SUBTITLE_XPATH, 1, 2, 4)));
        assertEquals("1", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 4, 2, 2)));
        assertEquals("test participant 1", selenium.getText(String.format(MULTI_TABLE_LINK_XPATH, 1, 2, 4, 2, 1)));
        assertEquals("1", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 4, 3, 2)));
        assertEquals("test participant 2", selenium.getText(String.format(MULTI_TABLE_LINK_XPATH, 1, 2, 4, 3, 1)));
        
        assertEquals("testType1", selenium.getText(String.format(METRIC_XPATH, 2, 1, 2, 1)));
        assertEquals("3", selenium.getText(String.format(METRIC_XPATH, 2, 1, 2, 2)));
        assertEquals("testType2", selenium.getText(String.format(METRIC_XPATH, 2, 1, 3, 1)));
        assertEquals("2", selenium.getText(String.format(METRIC_XPATH, 2, 1, 3, 2)));
        assertEquals("testType3", selenium.getText(String.format(METRIC_XPATH, 2, 1, 4, 1)));
        assertEquals("1", selenium.getText(String.format(METRIC_XPATH, 2, 1, 4, 2)));
        //CHECKSTYLE:ON
    }

    private void verifyNoData() {
        //CHECKSTYLE:OFF - magic numbers
        assertEquals("Usage Statistics", selenium.getText(String.format(TITLE_XPATH, 1, 1)));
        assertEquals("Registrations, and Requests", selenium.getText(String.format(SUBTITLE_XPATH, 1, 1)));
        assertEquals("Registered", selenium.getText(String.format(METRIC_XPATH, 1, 1, 2, 1)));
        assertEquals("0", selenium.getText(String.format(METRIC_XPATH, 1, 1, 2, 2)));
        assertTrue(selenium.getText(String.format(METRIC_XPATH, 1, 1, 3, 1)).contains("Requests for Biospecimens"));
        assertEquals("0", selenium.getText(String.format(METRIC_XPATH, 1, 1, 3, 2)));
        assertTrue(selenium.getText(String.format(METRIC_XPATH, 1, 1, 4, 1)).contains("Requests for Biospecimens"));
        assertEquals("0", selenium.getText(String.format(METRIC_XPATH, 1, 1, 4, 2)));

        assertEquals("Shipments and Returns", selenium.getText(String.format(TITLE_XPATH, 1, 2)));
        assertEquals("Shipments and Returns", selenium.getText(String.format(MULTI_TABLE_SUBTITLE_XPATH, 1, 2, 1)));
        assertEquals("Total Shipments", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 2, 1)));
        assertEquals("0", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 2, 2)));
        assertEquals("Total Biospecimen Returns Requested", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 3, 1)));
        assertEquals("0", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 3, 2)));
        assertEquals("Total Biospecimens Returned", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 4, 1)));
        assertEquals("0", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 4, 2)));
        assertEquals("Total Unfulfilled Biospecimen Return Requests", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 5, 1)));
        assertEquals("0", selenium.getText(String.format(MULTI_TABLE_METRIC_XPATH, 1, 2, 1, 5, 2)));
        

        assertFalse(selenium.isTextPresent("Biospecimen Return Requests by " + ClientProperties.getParticipantTerm()));
        assertFalse(selenium.isTextPresent("Biospecimen Returns by " + ClientProperties.getParticipantTerm()));
        assertFalse(selenium.isTextPresent("Unfulfilled Biospecimen Return Requests by " + ClientProperties.getParticipantTerm()));

        assertEquals("Top Tissue Types", selenium.getText(String.format(TITLE_XPATH, 2, 1)));
        assertEquals("Tissue Types", selenium.getText(String.format(SUBTITLE_XPATH, 2, 1)));
        assertFalse(selenium.isElementPresent(String.format(METRIC_XPATH, 2, 1, 2, 1)));
        //CHECKSTYLE:ON
    }
}
