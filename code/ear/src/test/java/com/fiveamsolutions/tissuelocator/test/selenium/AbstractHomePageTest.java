/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.test.selenium;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.test.ClientProperties;

/**
 * test the user home page.
 * note that this test focuses on the search functionality on the home page.  the individual status messages
 * that appear on the home page are verified in the tests of the features that generate those messages.
 *
 * @author ddasgupta
 *
 */
public abstract class AbstractHomePageTest extends AbstractTissueLocatorSeleniumTest {

    /**
     * Search title.
     */
    protected static final String SEARCH_TITLE = "Search for " + ClientProperties.getBiospecimenTerm() + "s";
    private static final int NORMAL_SAMPLE_COUNT = 3;

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "HomePageTest.sql";
    }

    /**
     * Update the cache and log in.
     */
    protected void goToHomePage() {
        selenium.open(CONTEXT_ROOT + "/updateHomePageCache.jsp");
        assertTrue(selenium.isTextPresent("Home Page Data Cache Updated"));
        loginAsAdmin();
    }

    /**
     * Browse by type.
     */
    protected void browseByType() {
        clickAndWait("link=Home");
        int cellsCount = ClientProperties.getCellsSpecimenCount() + 2;
        assertTrue(selenium.isElementPresent("link=Cells (" + cellsCount + ")"));
        int fluidCount = ClientProperties.getFluidSpecimenCount() + 2;
        assertTrue(selenium.isElementPresent("link=Fluid (" + fluidCount + ")"));
        int molecularCount = ClientProperties.getMolecularSpecimenCount() + 2;
        assertTrue(selenium.isElementPresent("link=Molecular (" + molecularCount + ")"));
        int tissueCount = ClientProperties.getTissueSpecimenCount() + 2;
        assertTrue(selenium.isElementPresent("link=Tissue (" + tissueCount + ")"));
        clickAndWait("link=Molecular (" + molecularCount + ")");
        assertTrue(selenium.isTextPresent(SEARCH_TITLE));
        assertEquals("Molecular", selenium.getSelectedLabel(getSpecimenClassSelectionName()));
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));
    }

    /**
     * @return the name of the specimen class control.
     */
    protected String getSpecimenClassSelectionName() {
        return "object.specimenType.specimenClass";
    }

    /**
     * Browse by disease.
     */
    protected void browseByDisease() {
        clickAndWait("link=Home");
        String[] diseases = ClientProperties.getDiseases();
        int[] counts = ClientProperties.getDiseaseCounts();
        String normalTerm = ClientProperties.getNormalTerm();
        assertTrue(selenium.isElementPresent(getDiseaseLink(normalTerm, NORMAL_SAMPLE_COUNT)));
        for (int i  = 0; i < diseases.length; i++) {
            String shortDisease = StringUtils.substringBefore(diseases[i], " - ");
            boolean shortPresent = selenium.isElementPresent(getDiseaseLink(shortDisease, counts[i]));
            boolean longPresent = selenium.isElementPresent(getDiseaseLink(diseases[i], counts[i]));
            assertTrue(shortPresent || longPresent);
        }

        int diseaseCount = selenium.getXpathCount("//div[@id='disease_scrollbox']/ul/li").intValue();
        String xpathBase = "xpath=//div[@id='disease_scrollbox']/ul/li[%d]";
        assertTrue(selenium.isElementPresent(String.format(xpathBase, 1)));
        assertTrue(selenium.getText(String.format(xpathBase, 1)).contains(normalTerm));
        for (int i = 2; i < diseaseCount; i++) {
            String firstLinkText = selenium.getText(String.format(xpathBase, i) + "/a");
            String secondLinkText = selenium.getText(String.format(xpathBase, i + 1) + "/a");
            assertTrue(firstLinkText.toLowerCase().compareTo(secondLinkText.toLowerCase()) <= 0
                    || firstLinkText.replaceAll("[^a-z,A-Z]", "").compareTo(
                            secondLinkText.replaceAll("[^a-z,A-Z]", "")) <= 0);
        }
        assertTrue(selenium.isElementPresent(String.format(xpathBase, diseaseCount)));
        assertFalse(selenium.isElementPresent(String.format(xpathBase, diseaseCount + 1)));
        clickAndWait(getDiseaseLink(diseases[1], counts[1]));
        assertTrue(selenium.isTextPresent(SEARCH_TITLE));
        assertEquals(diseases[1], selenium.getValue("pathologicalCharacteristic"));
        if (selenium.isElementPresent("object.specimenType.specimenClass")) {
            assertTrue(selenium.getSelectedLabel("object.specimenType.specimenClass").contains("All"));
        }
    }

    /**
     * Return the link locator for the browse by disease widget.
     * @param disease disease text.
     * @param count disease count.
     * @return the link locator for the browse by disease widget.
     */
    protected String getDiseaseLink(String disease, int count) {
        return "link=" + disease +  " (" + count + ")";
    }

    /**
     * Search.
     */
    protected void search() {
        String disease = ClientProperties.getDiseases()[2];
        clickAndWait("link=Home");
        selenium.click("specimenClass_TISSUE");
        selenium.type("pathologicalCharacteristic", disease);
        clickAndWait("btn_search");

        assertTrue(selenium.isTextPresent(SEARCH_TITLE));
        assertEquals("Tissue", selenium.getSelectedLabel(getSpecimenClassSelectionName()));
        assertEquals(disease, selenium.getValue("pathologicalCharacteristic"));

        clickAndWait("link=Home");

        assertFalse(selenium.isChecked("specimenClass_CELLS"));
        assertFalse(selenium.isChecked("specimenClass_FLUID"));
        assertFalse(selenium.isChecked("specimenClass_MOLECULAR"));
        assertFalse(selenium.isChecked("specimenClass_TISSUE"));
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));

        clickAndWait("link=Advanced Search >>");
        assertTrue(selenium.isTextPresent(SEARCH_TITLE));
        verifySpecimenClassSelection();
        assertTrue(StringUtils.isBlank(selenium.getValue("pathologicalCharacteristic")));
    }

    /**
     * Search dynamic extensions.
     */
    protected void searchDynamicExtensions() {
        clickAndWait("link=Home");
        String anatomicSourceFieldId = "object.customProperties['anatomicSource']";
        selenium.select(anatomicSourceFieldId, "label=" + ClientProperties.getAnatomicSource());
        clickAndWait("btn_search");
        assertFalse(selenium.isVisible("filters"));
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertTrue(selenium.isTextPresent(ClientProperties.getAnatomicSourceResults()));
        assertTrue(selenium.isTextPresent("1 of " + ClientProperties.getAnatomicSourcePages()));
        assertTrue(selenium.isTextPresent("Anatomic Source: " + ClientProperties.getAnatomicSource()));
        assertEquals(ClientProperties.getAnatomicSource(), selenium.getSelectedLabel(anatomicSourceFieldId));

        String defaultSelection = "- All -";
        clickAndWait("link=Home");
        assertEquals(defaultSelection, selenium.getSelectedLabel(anatomicSourceFieldId));
        String preservationTypeFieldId = "object.customProperties['preservationType']";
        selenium.select(preservationTypeFieldId, "label=Fixed Formalin");
        clickAndWait("btn_search");
        assertFalse(selenium.isTextPresent(ERROR_MESSAGE));
        assertEquals("Fixed Formalin", selenium.getSelectedLabel(preservationTypeFieldId));

        clickAndWait("link=Home");
        assertEquals(defaultSelection, selenium.getSelectedLabel(anatomicSourceFieldId));
        assertEquals(defaultSelection, selenium.getSelectedLabel(preservationTypeFieldId));
        clickAndWait("link=Advanced Search >>");
        assertTrue(selenium.isTextPresent(SEARCH_TITLE));
        assertEquals(defaultSelection, selenium.getSelectedLabel(anatomicSourceFieldId));
        assertEquals(defaultSelection, selenium.getSelectedLabel(preservationTypeFieldId));
    }

    /**
     * Verify whether any option has been selected from the specimenClass multiselect.
     */
    protected void verifySpecimenClassSelection() {
        assertEquals("- All -", selenium.getSelectedLabel(getSpecimenClassSelectionName()));
    }

}
