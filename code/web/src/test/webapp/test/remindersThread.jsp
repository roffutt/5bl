<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<%@page import="com.fiveamsolutions.tissuelocator.web.listener.ScheduledRemindersTask"%><html>
<%
int gracePeriod = 1;
String param = request.getParameter("gracePeriod");
if (param != null) {
    gracePeriod = Integer.parseInt(param);
}
ScheduledRemindersTask task = new ScheduledRemindersTask(gracePeriod);
task.run();
%>


<head>
    <title>Scheduled Reminders Thread Complete</title>
</head>
<body>
    Scheduled Reminders Thread Complete
</body>
</html>
