/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.util.SubsetIteratorFilter.Decider;
import org.displaytag.tags.TableTagParameters;
import org.junit.Test;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.BooleanDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.DecimalDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.IntegerDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.StringDynamicFieldDefinition;
import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.config.GenericFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.category.AbstractUiCategory;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField.FieldType;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorRequestHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractPersistentObjectAction.PaginatedSearchHelper;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.CodeServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.CollectionProtocolServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.ParticipantServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.SpecimenSearchServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.SpecimenServiceStub;
import com.fiveamsolutions.tissuelocator.web.util.SelectOption;
import com.opensymphony.xwork2.Action;

/**
 * @author smiller
 */
public class SpecimenActionTest extends AbstractTissueLocatorWebTest {

    private static final int THREE = 3;
    private static final int THREE_STATUSES = 3;
    private static final int FOUR_STATUSES = 4;
    private static final int ONE_THOUSAND = 1000;
    private static final int TWO_THOUSAND = 2000;
    private static final int THREE_THOUSAND = 3000;
    private static final int MAX_DISEASE_COUNT = 100;
    private static final int MAX_PARTICIPANT_COUNT = 100;
    private static final int MAX_PROTOCOL_COUNT = 100;

    /**
     * Tests the load and view methods.
     */
    @Test
    public void testLoadView() {
        assertEquals(Action.INPUT, getSpecimenAction().input());
        assertEquals("view", getSpecimenAction().view());
    }

    /**
     * Test properties related to dynamic categories.
     */
    @Test
    public void testDynamicCategoriesProperty() {
        SpecimenAction sa = getSpecimenAction();
        sa.prepare();
        assertNotNull(sa.getCategoryNames());
        assertTrue(sa.getCategoryNames().size() > 0);
        String testCategory = "Biospecimen Category";
        testCategory = sa.getNormalizedName(testCategory);
        assertTrue(testCategory.indexOf(' ') == -1);
        assertNotNull(sa.getDynamicFieldDefinitions());
        assertTrue(sa.getDynamicFieldDefinitions().size() > 0);
    }

    /**
     * Tests the list method.
     */
    @Test
    public void testList() {
        SpecimenSearchServiceStub stub = new SpecimenSearchServiceStub();
        SpecimenAction action = getSpecimenAction(stub);
        assertEquals(Action.SUCCESS, action.list());
        assertEquals(1, action.getObjects().getList().size());
        assertTrue(stub.isSearchCalled());
        String name = action.getClass().getSimpleName() + "_base_params";
        Boolean instFilter = (Boolean) getSession().getAttribute(name);
        assertNotNull(instFilter);
        assertFalse(instFilter.booleanValue());
        assertNull(action.getObject().getExternalIdAssigner());

        action = getSpecimenAction(stub);
        action.setPage(2);
        getRequest().addParameter("page", "2");
        assertEquals(Action.SUCCESS, action.list());
        assertEquals(1, action.getObjects().getList().size());
        assertEquals(1, action.getPage());
        assertTrue(stub.isSearchCalled());

        action = getSpecimenAction(stub);
        action.setFilterByInstitution(true);
        assertEquals(Action.SUCCESS, action.filter());
        assertEquals(1, action.getObjects().getList().size());
        assertTrue(stub.isSearchCalled());
        assertNotNull(action.getObject().getExternalIdAssigner());
    }

    /**
     * Tests the prepare method.
     */
    @Test
    public void testPrepare() {
        SpecimenAction action = getSpecimenAction();
        assertFalse(action.isDisplayCollectionProtocol());
        assertFalse(action.isNormalSample());
        action.prepare();
        assertNull(action.getObject().getId());
        assertTrue(action.isDisplayCollectionProtocol());
        assertFalse(action.isNormalSample());
        assertFalse(action.isAggregateResults());

        getTestAppSettingService().setSpecimenGrouping("institution");
        action.setObject(new Specimen());
        action.getObject().setId(1L);
        action.prepare();
        assertNotNull(action.getObject());
        assertFalse(action.isNormalSample());
        assertTrue(action.isAggregateResults());

        getTestAppSettingService().setSpecimenGrouping(null);
        action.setObject(new Specimen());
        action.getObject().setId(-1L);
        action.prepare();
        assertFalse(action.isNormalSample());
        assertFalse(action.isAggregateResults());

        action.setObject(new Specimen());
        action.getObject().setPathologicalCharacteristic(new AdditionalPathologicFinding());
        action.prepare();
        assertFalse(action.isNormalSample());

        action.setObject(new Specimen());
        action.getObject().setPathologicalCharacteristic(new AdditionalPathologicFinding());
        action.getObject().getPathologicalCharacteristic().setId(Long.valueOf(action.getNormalSampleId()) + 1);
        action.prepare();
        assertTrue(action.isNormalSample());

        assertNotNull(action.getInstitutions());
        assertEquals(1, action.getInstitutions().size());

        assertNotNull(action.getSpecimenTypes());
        assertEquals(1, action.getSpecimenTypes().size());
        assertTrue(action.isDisplayCollectionProtocol());
    }

    /**
     * Tests the save method.
     */
    @Test
    public void testSave() {
        SpecimenAction action = getSpecimenAction();
        Specimen specimen = new Specimen();
        specimen.setExternalId("test");
        action.setObject(specimen);
        action.save();
        SpecimenServiceStub stub = (SpecimenServiceStub)
            TissueLocatorRegistry.getServiceLocator().getSpecimenService();
        assertEquals(action.getObject(), stub.getObject());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * Test invalid constructor.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testInvalidConstructor() {
        SpecimenAction sa = new SpecimenAction() {

            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
        assertNull("UnsuportedOperationException should have been thrown", sa);
    }

    /**
     * Tests the input method.
     */
    @Test
    public void testInput() {
        SpecimenAction action = getSpecimenAction();
        action.setObject(null);
        action.input();
        assertEquals(null, action.getDiseaseSelection());
        assertEquals(null, action.getParticipantSelection());
        assertNotNull(action.getDefaultProtocol());

        action.setObject(new Specimen());
        action.input();
        assertEquals(null, action.getDiseaseSelection());
        assertEquals(null, action.getParticipantSelection());
        assertEquals(null, action.getProtocolSelection());
        assertNotNull(action.getDefaultProtocol());
        assertNotNull(action.getObject().getProtocol());

        action.getObject().setPathologicalCharacteristic(new AdditionalPathologicFinding());
        action.getObject().setParticipant(new Participant());
        action.getObject().setProtocol(new CollectionProtocol());
        action.input();
        assertEquals(null, action.getDiseaseSelection());
        assertEquals(null, action.getParticipantSelection());
        assertEquals(null, action.getProtocolSelection());

        action.getObject().getPathologicalCharacteristic().setName("test");
        action.getObject().getParticipant().setExternalId("participant");
        action.getObject().getProtocol().setName("protocol");
        action.input();
        assertEquals("test", action.getDiseaseSelection());
        assertEquals("participant", action.getParticipantSelection());
        assertEquals("protocol", action.getProtocolSelection());
    }

    /**
     * Tests the disease autocomplete.
     */
    @Test
    public void testDiseaseAutocomplete() {
        SpecimenAction action = getSpecimenAction();
        assertEquals(Action.SUCCESS, action.autocompleteDisease());
        action.setTerm(null);
        assertEquals(0, action.getDiseaseMap().length);
        action.setTerm("test");
        assertEquals(1, action.getDiseaseMap().length);
        SelectOption result = action.getDiseaseMap()[0];
        assertNotNull(result.getId());
        assertNotNull(result.getLabel());
        assertNotNull(result.getValue());

        action = getSpecimenAction();
        action.setTerm("test");
        CodeServiceStub stub = (CodeServiceStub) TissueLocatorRegistry.getServiceLocator().getCodeService();
        stub.setSearchResultsSize(MAX_DISEASE_COUNT);
        SelectOption[] results = action.getDiseaseMap();
        assertEquals(MAX_DISEASE_COUNT + 1, results.length);
        assertEquals("-1", results[0].getId());
    }

    /**
     * Tests the in-use disease autocomplete.
     */
    @Test
    public void testInUseDiseaseAutocomplete() {
        SpecimenAction action = getSpecimenAction();
        String normalTerm = action.getText("specimen.search.normal.name");
        assertEquals(Action.SUCCESS, action.autocompleteInUseDisease());
        action.setTerm(null);
        action.prepare();
        assertEquals(1, action.getInUseDiseaseMap().length);
        SelectOption result = action.getInUseDiseaseMap()[0];
        assertEquals(normalTerm, result.getValue());
        action.setTerm("test");
        action.prepare();
        assertEquals(1, action.getInUseDiseaseMap().length);
        result = action.getInUseDiseaseMap()[0];
        assertNotNull(result.getId());
        assertNotNull(result.getLabel());
        assertNotNull(result.getValue());


        action = getSpecimenAction();
        action.setTerm("test");
        CodeServiceStub stub = (CodeServiceStub) TissueLocatorRegistry.getServiceLocator().getCodeService();
        stub.setSearchResultsSize(MAX_DISEASE_COUNT);
        SelectOption[] results = action.getInUseDiseaseMap();
        assertEquals(MAX_DISEASE_COUNT + 1, results.length);
        assertEquals("-1", results[0].getId());

        action = getSpecimenAction();
        action.setTerm(normalTerm);
        stub = (CodeServiceStub) TissueLocatorRegistry.getServiceLocator().getCodeService();
        stub.setSearchResultsSize(MAX_DISEASE_COUNT);
        results = action.getInUseDiseaseMap();
        assertEquals(MAX_DISEASE_COUNT + 2, results.length);
        assertEquals("-1", results[0].getId());

        stub.setSearchResultsSize(0);
        action.setTerm(normalTerm);
        action.prepare();
        assertEquals(1, action.getInUseDiseaseMap().length);
        result = action.getInUseDiseaseMap()[0];
        assertEquals(normalTerm, result.getValue());
    }

    /**
     * Tests the participant autocomplete.
     */
    @Test
    public void testParticipantAutocomplete() {
        SpecimenAction action = getSpecimenAction();
        assertEquals(Action.SUCCESS, action.autocompleteParticipant());
        action.setTerm(null);
        action.setInstitutionSelection("test institution");
        assertEquals(0, action.getParticipantMap().length);
        action.setTerm("test");
        assertEquals(1, action.getParticipantMap().length);
        SelectOption result = action.getProtocolMap()[0];
        assertNotNull(result.getId());
        assertNotNull(result.getLabel());
        assertNotNull(result.getValue());

        action = getSpecimenAction();
        action.setTerm("test");
        ParticipantServiceStub stub = (ParticipantServiceStub)
            TissueLocatorRegistry.getServiceLocator().getParticipantService();
        stub.setSearchResultsSize(MAX_PARTICIPANT_COUNT);
        SelectOption[] results = action.getParticipantMap();
        assertEquals(MAX_PARTICIPANT_COUNT + 1, results.length);
        assertEquals("-1", results[0].getId());
    }

    /**
     * Tests the protocol autocomplete.
     */
    @Test
    public void testProtocolAutocomplete() {
        SpecimenAction action = getSpecimenAction();
        assertEquals(Action.SUCCESS, action.autocompleteProtocol());
        action.setTerm(null);
        action.setInstitutionSelection("test institution");
        assertEquals(0, action.getProtocolMap().length);
        action.setTerm("test");
        assertEquals(1, action.getProtocolMap().length);
        SelectOption result = action.getProtocolMap()[0];
        assertNotNull(result.getId());
        assertNotNull(result.getLabel());
        assertNotNull(result.getValue());

        action = getSpecimenAction();
        action.setTerm("test");
        CollectionProtocolServiceStub stub = (CollectionProtocolServiceStub)
            TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();
        stub.setSearchResultsSize(MAX_PROTOCOL_COUNT);
        SelectOption[] results = action.getProtocolMap();
        assertEquals(MAX_PROTOCOL_COUNT + 1, results.length);
        assertEquals("-1", results[0].getId());
    }

    /**
     * Tests the moreDetails method.
     */
    @Test
    public void testMoreDetails() {
        SpecimenAction action = getSpecimenAction();
        Specimen specimen = new Specimen();
        specimen.setExternalId("test");
        action.setObject(specimen);
        action.prepare();
        assertEquals("request", action.moreDetails());
        assertNotNull(action.getRedirectId());

        action.getObject().setId(1L);
        action.setRedirectId(null);
        action.prepare();
        assertEquals("shipment", action.moreDetails());
        assertNotNull(action.getRedirectId());
    }

    /**
     * test legal statuses.
     */
    @Test
    public void testLegalStatuses() {
        SpecimenAction action = getSpecimenAction();
        action.setObject(null);
        assertEquals(1, action.getLegalStatuses().size());

        action.setObject(new Specimen());
        action.getObject().setStatus(null);
        assertEquals(1, action.getLegalStatuses().size());

        action.getObject().setStatus(SpecimenStatus.AVAILABLE);
        assertEquals(FOUR_STATUSES, action.getLegalStatuses().size());

        action.getObject().setStatus(SpecimenStatus.UNDER_REVIEW);
        assertEquals(FOUR_STATUSES, action.getLegalStatuses().size());

        action.getObject().setStatus(SpecimenStatus.PENDING_SHIPMENT);
        assertEquals(FOUR_STATUSES, action.getLegalStatuses().size());

        action.getObject().setStatus(SpecimenStatus.UNAVAILABLE);
        assertEquals(FOUR_STATUSES, action.getLegalStatuses().size());

        action.getObject().setStatus(SpecimenStatus.SHIPPED);
        assertEquals(THREE_STATUSES, action.getLegalStatuses().size());

        action.getObject().setStatus(SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION);
        assertEquals(THREE_STATUSES, action.getLegalStatuses().size());

        action.getObject().setStatus(SpecimenStatus.DESTROYED);
        assertEquals(1, action.getLegalStatuses().size());
    }

    /**
     * Tests the withdraw consent method.
     */
    @Test
    public void testWithdrawConsent() {
        SpecimenAction action = getSpecimenAction();
        action.setObject(new Specimen());
        action.getObject().setId(1L);
        assertEquals("success", action.withdrawConsent());
        assertTrue(action.getObject().isConsentWithdrawn());

        action.setObject(new Specimen());
        action.getObject().setId(SpecimenServiceStub.CONSENT_NOT_WITHDRAWABLE_ID);
        assertEquals("success", action.withdrawConsent());
        assertFalse(action.getObject().isConsentWithdrawn());
        assertEquals(1, action.getActionErrors().size());
    }

    /**
     * Tests the return to source method.
     */
    @Test
    public void testReturnToSourceInstitution() {
        SpecimenAction action = getSpecimenAction();
        action.setObject(new Specimen());
        action.getObject().setId(1L);
        assertEquals("success", action.returnToSourceInstitution());

        action.setObject(new Specimen());
        action.getObject().setId(SpecimenServiceStub.SPECIMEN_NOT_RETURNABLE_ID);
        assertEquals("success", action.returnToSourceInstitution());
        assertEquals(1, action.getActionErrors().size());
    }

    /**
     * Tests a request-scoped PaginatedSearchHelper.
     */
    @Test
    @SuppressWarnings("rawtypes")
    public void testPaginatedSearchHelper() {
        SpecimenSearchServiceStub stub = new SpecimenSearchServiceStub();
        SpecimenAction action = getSpecimenAction(stub);
        action.setObject(new Specimen());
        action.getObject().setId(1L);
        getRequest().addParameter(TableTagParameters.PARAMETER_EXPORTING, "true");

        stub.setPaginatedResultsAvailable(ONE_THOUSAND);
        assertEquals("success", action.filter());
        PaginatedSearchHelper helper = TissueLocatorRequestHelper.getPaginatedSearchHelper(getRequest());
        assertNotNull(helper);
        assertFalse(helper.hasMoreResults());
        assertEquals(0, helper.getNextResults().size());

        stub.setPaginatedResultsAvailable(TWO_THOUSAND);
        assertEquals("success", action.filter());
        helper = TissueLocatorRequestHelper.getPaginatedSearchHelper(getRequest());
        assertNotNull(helper);
        assertFalse(helper.hasMoreResults());
        assertEquals(0, helper.getNextResults().size());

        stub.setPaginatedResultsAvailable(THREE_THOUSAND);
        assertEquals("success", action.filter());
        helper = TissueLocatorRequestHelper.getPaginatedSearchHelper(getRequest());
        assertNotNull(helper);
        assertTrue(helper.hasMoreResults());
        assertEquals(1, helper.getNextResults().size());
        assertFalse(helper.hasMoreResults());
        assertEquals(0, helper.getNextResults().size());
    }

    /**
     * Test the dynamic field user interface categories.
     */
    @Test
    public void testUiDynamicFieldCategories() {
        SpecimenAction action = getSpecimenAction();
        action.setObject(new Specimen());
        action.prepare();

        // CHECKSTYLE:OFF
        Map<String, UiDynamicFieldCategory> categories = action.getUiDynamicFieldCategories();
        assertFalse(categories.isEmpty());
        UiDynamicFieldCategory category = categories.values().iterator().next();
        List<UiDynamicCategoryField> categoryFields = category.getUiDynamicCategoryFields();
        assertEquals("fieldName1", categoryFields.get(0).getFieldConfig().getFieldName());
        assertEquals("fieldName2", categoryFields.get(1).getFieldConfig().getFieldName());
        assertEquals("fieldName3", categoryFields.get(2).getFieldConfig().getFieldName());
        assertEquals("fieldName4", categoryFields.get(3).getFieldConfig().getFieldName());

        UiDynamicFieldCategory directCategory = action.getUiDynamicFieldCategory(category.getCategoryName());
        assertNotNull(directCategory);
        assertEquals(category.getCategoryName(), directCategory.getCategoryName());
        // CHECKSTYLE:ON
    }

    /**
     * Test dynamic field user interface category map.
     */
    @Test
    public void testUiDynamicFieldCategoryMap() {
        SpecimenAction action = getSpecimenAction();
        action.setObject(new Specimen());
        action.prepare();
        verifyMappedCategory(SpecimenAction.SpecimenHeaderKey.BIOSPECIMEN_CHARACTERISTICS.getResourceKey(), action);
        verifyMappedCategory(SpecimenAction.SpecimenHeaderKey.PARTICIPANT.getResourceKey(), action);
        verifyMappedCategory(SpecimenAction.SpecimenHeaderKey.COLLECTION_PROTOCOL.getResourceKey(), action);
    }

    /**
     * Test the dynamic field user interface category decider.
     *
     * @throws Exception if error occurs
     */
    @Test
    public void testUiDynamicFieldCategoryDecider() throws Exception {
        SpecimenAction action = getSpecimenAction();
        action.setObject(new Specimen());
        action.prepare();
        Decider decider = action.getUiDynamicFieldCategoryDecider();
        Iterator<UiDynamicFieldCategory> it = action.getUiDynamicFieldCategories().values().iterator();
        assertFalse(decider.decide(it.next()));
        assertFalse(decider.decide(it.next()));
        assertFalse(decider.decide(it.next()));
        assertTrue(decider.decide(it.next()));
    }

    /**
     * Test getObjectProperty method.
     *
     * @throws Exception rethrows any exception raised by getObjectProperty
     */
    @Test
    public void testGetObjectProperty() throws Exception {
        SpecimenAction action = getSpecimenAction();
        Specimen testSpecimen = new Specimen();
        Institution testInstition = new Institution();
        testInstition.setId(1L);
        testSpecimen.setExternalIdAssigner(testInstition);
        final String testCustomPropertyName = "test";
        final String testCustomPropertyValue = "this is the test field value";
        testSpecimen.getCustomProperties().put(testCustomPropertyName, testCustomPropertyValue);
        action.setObject(testSpecimen);
        assertNotNull(action.getObjectProperty("externalIdAssigner"));
        assertEquals(testInstition.getId(), action.getObjectProperty("externalIdAssigner.id"));
        Object retrievedCustomPropertyValue = action.getObjectProperty("test");
        assertNotNull(retrievedCustomPropertyValue);
        assertEquals(retrievedCustomPropertyValue, testCustomPropertyValue);
        testSpecimen.setExternalIdAssigner(null);
        assertNull(action.getObjectProperty("externalIdAssigner"));
        assertNull(action.getObjectProperty("externalIdAssigner.id"));
        assertNull(action.getObjectProperty("externalIdAssigner.jd"));
    }

    /**
     * Test dynamic field defintion retrieval by category.
     */
    @Test
    public void testGetDynamicFieldDefinitionsByCategory() {
        SpecimenAction action = getSpecimenAction();
        action.prepare();
        UiDynamicFieldCategory category = new UiDynamicFieldCategory();
        assertTrue(action.getDynamicFieldDefinitionsByCategory(category).isEmpty());
        
        action = getSpecimenAction();
        action.prepare();
        assertTrue(action.getDynamicFieldDefinitionsByCategory(null).isEmpty());

        List<UiDynamicCategoryField> fields = new ArrayList<UiDynamicCategoryField>();
        for (int i = 0; i < THREE; i++) {
            category = new UiDynamicFieldCategory();
            UiDynamicCategoryField field = new UiDynamicCategoryField();
            field.setFieldConfig(new GenericFieldConfig());
            field.getFieldConfig().setFieldName("fieldName" + (i + 1));
            field.setFieldType(FieldType.CUSTOM_PROPERTY);
            fields.add(field);
            category.setUiDynamicCategoryFields(fields);
            assertEquals(i + 1, action.getDynamicFieldDefinitionsByCategory(category).size());
            for (int j = 0; j < i; j++) {
                assertEquals(fields.get(j).getFieldConfig().getFieldName(),
                        action.getDynamicFieldDefinitionsByCategory(category).get(j).getFieldName());
            }
        }

        category = new UiDynamicFieldCategory();
        UiDynamicCategoryField field = new UiDynamicCategoryField();
        field.setFieldConfig(new GenericFieldConfig());
        field.getFieldConfig().setFieldName("fieldName");
        field.setFieldType(FieldType.TEXT);
        fields.add(field);
        category.setUiDynamicCategoryFields(fields);
        assertEquals(THREE, action.getDynamicFieldDefinitionsByCategory(category).size());
    }

    private void verifyMappedCategory(String header, SpecimenAction action) {
        AbstractUiCategory category = action.getUiDynamicFieldCategories().get(header);
        assertEquals(header, category.getCategoryName());
    }

    private SpecimenAction getSpecimenAction() {
        return getSpecimenAction(new SpecimenSearchServiceStub());
    }

    private SpecimenAction getSpecimenAction(SpecimenSearchServiceStub stub) {
        return new SpecimenAction(stub, getTestUserService(), getTestAppSettingService(), 
                getTestUiDynamicFieldCategoryService(), getTestUiSectionService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                List<AbstractDynamicFieldDefinition> definitions = new ArrayList<AbstractDynamicFieldDefinition>();

                BooleanDynamicFieldDefinition bfd = new BooleanDynamicFieldDefinition();
                bfd.setEntityClassName(Object.class.getName());
                bfd.setFieldName("fieldName1");
                bfd.setFieldDisplayName("Boolean Dynamic Field");
                definitions.add(bfd);

                DecimalDynamicFieldDefinition dfd = new DecimalDynamicFieldDefinition();
                dfd.setEntityClassName(Object.class.getName());
                dfd.setFieldName("fieldName2");
                dfd.setFieldDisplayName("Decimal Dynamic Field");
                definitions.add(dfd);

                IntegerDynamicFieldDefinition ifd = new IntegerDynamicFieldDefinition();
                ifd.setEntityClassName(Object.class.getName());
                ifd.setFieldName("fieldName3");
                ifd.setFieldDisplayName("Integer Dynamic Field");
                definitions.add(ifd);

                StringDynamicFieldDefinition sfd = new StringDynamicFieldDefinition();
                sfd.setEntityClassName(Object.class.getName());
                sfd.setFieldName("");
                sfd.setFieldDisplayName("String Dynamic Field");
                definitions.add(sfd);

                return definitions;
            }
        };
    }

}
