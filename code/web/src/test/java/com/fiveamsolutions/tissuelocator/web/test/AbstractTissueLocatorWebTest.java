/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.apache.struts2.ServletActionContext;
import org.junit.After;
import org.junit.Before;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockServletContext;

import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.config.Configuration;
import com.opensymphony.xwork2.config.ConfigurationManager;
import com.opensymphony.xwork2.config.providers.XWorkConfigurationProvider;
import com.opensymphony.xwork2.inject.Container;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.util.ValueStackFactory;

/**
 * @author smiller
 */
public abstract class AbstractTissueLocatorWebTest {
    private ValueStack theValueStack;
    private ValueStackFactory theValueStackFactory;

    /**
     * Test user service.
     */
    private ApplicationSettingServiceStub testAppSettingService;
    private SpecimenSearchServiceStub testSpecimenSearchService;
    private TissueLocatorUserServiceStub testUserService;
    private SearchFieldConfigServiceStub testSearchFieldConfigService;
    private SearchResultDisplaySettingsServiceStub testSearchResultDisplaySettingService;
    private UiDynamicFieldCategoryServiceStub testUiDynamicFieldCategoryService;
    private UiSearchFieldCategoryServiceStub testUiSearchFieldCategoryService;
    private UiSectionServiceStub testUiSectionService;

    /**
     * initialize the web tests.
     */
    @Before
    public void initializeWebTests() {
        createTestServices();
        TissueLocatorRegistry.getInstance().setServiceLocator(new MockServiceLocator());
        Guice.createInjector(new AbstractModule() {

            /**
             * {@inheritDoc}
             */
            @Override
            protected void configure() {
                bind(ApplicationSettingServiceLocal.class).toInstance(testAppSettingService);
                bind(TissueLocatorUserServiceLocal.class).toInstance(testUserService);
            }

        });
    }

    /**
     * Initialize the mock request.
     */
    @Before
    public void initMockrequest() {
        ConfigurationManager configurationManager = new ConfigurationManager();
        configurationManager.addContainerProvider(new XWorkConfigurationProvider());
        Configuration config = configurationManager.getConfiguration();
        Container container = config.getContainer();

        theValueStackFactory = container.getInstance(ValueStackFactory.class);
        theValueStack = theValueStackFactory.createValueStack();
        theValueStack.getContext().put(ActionContext.CONTAINER, container);
        ActionContext.setContext(new ActionContext(theValueStack.getContext()));

        MockServletContext context = new MockServletContext();
        context.addInitParameter("defaultPageSize", "20");
        ServletActionContext.setServletContext(context);

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setSession(new MockHttpSession());
        ServletActionContext.setRequest(request);

        TissueLocatorUser u = new TissueLocatorUser();
        u.setInstitution(new Institution());
        TissueLocatorSessionHelper.setLoggedInUser(request.getSession(), u);
    }

    /**
     * Create an instance of each of the services for testing.
     */
    protected void createTestServices() {
        setTestAppSettingService(new ApplicationSettingServiceStub());
        setTestSpecimenSearchService(new SpecimenSearchServiceStub());
        setTestUserService(new TissueLocatorUserServiceStub());
        setTestSearchFieldConfigService(new SearchFieldConfigServiceStub());
        setTestUiDynamicFieldCategoryService(new UiDynamicFieldCategoryServiceStub());
        setTestUiSearchFieldCategoryService(new UiSearchFieldCategoryServiceStub());
        setTestUiSectionService(new UiSectionServiceStub());
        setTestSearchResultDisplaySettingService(new SearchResultDisplaySettingsServiceStub());
    }

    /**
     * @return An array of specific line items.
     */
    protected AggregateSpecimenRequestLineItem[] createSpecificLineItems() {
        return new AggregateSpecimenRequestLineItem[]{
                createUniqueLineItem("inst2", "Condition: Cancer", 1),
                createUniqueLineItem("inst1", "Birth Year: 1908; disease", 1),
                createUniqueLineItem("inst2", "Condition: Cancer; Birth Year: 1908", 1),
                createUniqueLineItem("inst2", "Condition: Cancer; Birth Year: 1908; disease", 1)
        };
    }

    /**
     * @return An array of general line items.
     */
    protected AggregateSpecimenRequestLineItem[] createGeneralLineItems() {
        return new AggregateSpecimenRequestLineItem[]{
                createUniqueLineItem("inst1", "No Criteria Specified", 1),
                createUniqueLineItem("inst2", "Birth Year: 1908", 1)
            };
    }

    /**
     * Creates a unique aggregate line item.
     * @param institutionSuffix institution name suffix.
     * @param criteria Search criteria.
     * @param quantity line item quantity.
     * @return a unique aggregate line item.
     */
    protected AggregateSpecimenRequestLineItem createUniqueLineItem(
            String institutionSuffix, String criteria, int quantity) {
        AggregateSpecimenRequestLineItem lineItem = new AggregateSpecimenRequestLineItem();
        lineItem.setInstitution(new Institution());
        lineItem.getInstitution().setName("institution name " + institutionSuffix);
        lineItem.setCriteria(criteria);
        lineItem.setQuantity(quantity);
        lineItem.setNote("note");
        return lineItem;
    }

    /**
     * Verifies arrays of line items are equal.
     * @param expectedSpecific expected specific values.
     * @param confirmSpecific actual specific values.
     * @param expectedGeneral expected general values.
     * @param confirmGeneral actual general values.
     */
    protected void verifyLineItems(AggregateSpecimenRequestLineItem[] expectedSpecific,
            AggregateSpecimenRequestLineItem[] confirmSpecific,
            AggregateSpecimenRequestLineItem[] expectedGeneral, AggregateSpecimenRequestLineItem[] confirmGeneral) {
        assertEquals(expectedGeneral.length, confirmGeneral.length);
        assertTrue(Arrays.asList(expectedSpecific).containsAll(Arrays.asList(confirmSpecific)));
        assertTrue(Arrays.asList(confirmSpecific).containsAll(Arrays.asList(expectedSpecific)));
        assertEquals(expectedSpecific.length, confirmSpecific.length);
        assertTrue(Arrays.asList(expectedGeneral).containsAll(Arrays.asList(confirmGeneral)));
        assertTrue(Arrays.asList(confirmGeneral).containsAll(Arrays.asList(expectedGeneral)));
    }

    /**
     * Verifies collections of line items contain all and only the expected values.
     * @param expectedSpecific expected specific values.
     * @param confirmSpecific actual specific values.
     * @param expectedGeneral expected general values.
     * @param confirmGeneral actual general values.
     */
    protected void verifyLineItems(AggregateSpecimenRequestLineItem[] expectedSpecific,
            Collection<AggregateSpecimenRequestLineItem> confirmSpecific,
            AggregateSpecimenRequestLineItem[] expectedGeneral,
            Collection<AggregateSpecimenRequestLineItem> confirmGeneral) {
        assertEquals(expectedSpecific.length, confirmSpecific.size());
        assertEquals(expectedGeneral.length, confirmGeneral.size());
        assertTrue(confirmGeneral.containsAll(Arrays.asList(expectedGeneral)));
        assertTrue(confirmSpecific.containsAll(Arrays.asList(expectedSpecific)));
    }

    /**
     * Clean out the action context to ensure one test does not impact another.
     */
    @After
    public void cleanUpActionContext() {
        ActionContext.setContext(null);
    }

    /**
     * @return MockHttpServletRequest
     */
    protected MockHttpServletRequest getRequest() {
        return (MockHttpServletRequest) ServletActionContext.getRequest();
    }

    /**
     * @return MockHttpSession
     */
    protected MockHttpSession getSession() {
        return (MockHttpSession) ServletActionContext.getRequest().getSession();
    }

    /**
     * @return the testUserService
     */
    protected TissueLocatorUserServiceStub getTestUserService() {
        return testUserService;
    }

    /**
     * @param testUserService the testUserService to set
     */
    protected void setTestUserService(TissueLocatorUserServiceStub testUserService) {
        this.testUserService = testUserService;
    }

    /**
     * @return the testAppSettingService
     */
    protected ApplicationSettingServiceStub getTestAppSettingService() {
        return testAppSettingService;
    }

    /**
     * @param testAppSettingService the testAppSettingService to set
     */
    protected void setTestAppSettingService(ApplicationSettingServiceStub testAppSettingService) {
        this.testAppSettingService = testAppSettingService;
    }

    /**
     * @param testSpecimenSearchService the testSpecimenSearchService to set
     */
    protected void setTestSpecimenSearchService(SpecimenSearchServiceStub testSpecimenSearchService) {
       this.testSpecimenSearchService = testSpecimenSearchService;
    }

    /**
     * @return the testSpecimenSearchService
     */
    protected SpecimenSearchServiceStub getTestSpecimenSearchService() {
        return testSpecimenSearchService;
    }

    /**
     * @return the testSearchFieldConfigService
     */
    public SearchFieldConfigServiceStub getTestSearchFieldConfigService() {
        return testSearchFieldConfigService;
    }

    /**
     * @param testSearchFieldConfigService the testSearchFieldConfigService to set
     */
    public void setTestSearchFieldConfigService(
            SearchFieldConfigServiceStub testSearchFieldConfigService) {
        this.testSearchFieldConfigService = testSearchFieldConfigService;
    }

    /**
     * @return the testSearchResultDisplaySettingService
     */
    public SearchResultDisplaySettingsServiceStub getTestSearchResultDisplaySettingService() {
        return testSearchResultDisplaySettingService;
    }

    /**
     * @param testSearchResultDisplaySettingService the testSearchResultDisplaySettingService to set
     */
    public void setTestSearchResultDisplaySettingService(
            SearchResultDisplaySettingsServiceStub testSearchResultDisplaySettingService) {
        this.testSearchResultDisplaySettingService = testSearchResultDisplaySettingService;
    }

    /**
     * @return the testUiDynamicFieldCategoryService
     */
    public UiDynamicFieldCategoryServiceStub getTestUiDynamicFieldCategoryService() {
        return testUiDynamicFieldCategoryService;
    }

    /**
     * @param testUiDynamicFieldCategoryService the testUiDynamicFieldCategoryService to set
     */
    public void setTestUiDynamicFieldCategoryService(
            UiDynamicFieldCategoryServiceStub testUiDynamicFieldCategoryService) {
        this.testUiDynamicFieldCategoryService = testUiDynamicFieldCategoryService;
    }

    /**
     * @return the testUiSearchFieldCategoryService
     */
    public UiSearchFieldCategoryServiceStub getTestUiSearchFieldCategoryService() {
        return testUiSearchFieldCategoryService;
    }

    /**
     * @param testUiSearchFieldCategoryService the testUiSearchFieldCategoryService to set
     */
    public void setTestUiSearchFieldCategoryService(
            UiSearchFieldCategoryServiceStub testUiSearchFieldCategoryService) {
        this.testUiSearchFieldCategoryService = testUiSearchFieldCategoryService;
    }

    /**
     * @return the testUiSectionService
     */
    public UiSectionServiceStub getTestUiSectionService() {
        return testUiSectionService;
    }

    /**
     * @param testUiSectionService the testUiSectionService to set
     */
    public void setTestUiSectionService(UiSectionServiceStub testUiSectionService) {
        this.testUiSectionService = testUiSectionService;
    }

}
