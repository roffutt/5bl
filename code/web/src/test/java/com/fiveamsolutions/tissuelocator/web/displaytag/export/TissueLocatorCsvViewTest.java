/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.displaytag.export;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.StringUtils;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.HeaderCell;
import org.displaytag.model.Row;
import org.displaytag.model.TableModel;
import org.displaytag.properties.TableProperties;
import org.junit.Test;
import org.springframework.mock.web.MockPageContext;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchCriteria;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSortCriterion;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorRequestHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractPersistentObjectAction.PaginatedSearchHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.SpecimenAction;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.SpecimenSearchServiceStub;

/**
 * @author gvaughn
 *
 */
public class TissueLocatorCsvViewTest extends AbstractTissueLocatorWebTest {

    private static final int PAGE_SIZE = 2000;
    private static final int MAX_EXPORT_ROWS = 1048576;
    private static final int INITIAL_RESULTS_COUNT = 10;
    private static final String EXTERNAL_ID_PREFIX = "x";


    /**
     * Test csv exports.
     * @throws JspException on error
     * @throws IOException on error
     */
    @Test
    @SuppressWarnings("rawtypes")
    public void testExport() throws IOException, JspException {

        // Test writes without a helper in request
        TissueLocatorCsvView view = getCsvView();
        view.setParameters(getModel(), true, true, true);
        StringWriter writer = new StringWriter();
        view.doExport(writer);
        String[][] splitResults = getSplitResults(writer.toString());
        verifyHeaders(splitResults);
        verifyInitialResults(splitResults);
        assertEquals(INITIAL_RESULTS_COUNT + 1, splitResults.length);
        assertFalse(view.outputPage());
        assertEquals("text/csv", view.getMimeType());

        // Test writes with a helper, with no additional results available
        Specimen example = new Specimen();
        example.setId(1L);
        example.setExternalId("exId");
        SpecimenSearchCriteria crit = new SpecimenSearchCriteria(example, null, null, null, null);
        PageSortParams<Specimen> params = new PageSortParams<Specimen>(PAGE_SIZE, 0,
                new ArrayList<SpecimenSortCriterion>(), false);
        SpecimenSearchServiceStub stub = new SpecimenSearchServiceStub();
        SpecimenAction action = new SpecimenAction(stub, getTestUserService(), getTestAppSettingService(), 
                getTestUiDynamicFieldCategoryService(), getTestUiSectionService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
        stub.setPaginatedResultsAvailable(PAGE_SIZE);
        PaginatedSearchHelper helper = action.new PaginatedSearchHelper(params, crit, PAGE_SIZE);
        TissueLocatorRequestHelper.setPaginatedSearchHelper(getRequest(), helper);
        view = getCsvView();
        view.setParameters(getModel(), true, true, true);
        writer = new StringWriter();
        view.doExport(writer);
        splitResults = getSplitResults(writer.toString());
        verifyHeaders(splitResults);
        verifyInitialResults(splitResults);
        assertEquals(INITIAL_RESULTS_COUNT + 1, splitResults.length);

        // Test writes with addtional results available
        stub.setPaginatedResultsAvailable(PAGE_SIZE * 2 + 1);
        helper = action.new PaginatedSearchHelper(params, crit, PAGE_SIZE * 2 + 1);
        TissueLocatorRequestHelper.setPaginatedSearchHelper(getRequest(), helper);
        view = getCsvView();
        view.setParameters(getModel(), true, true, true);
        writer = new StringWriter();
        view.doExport(writer);
        splitResults = getSplitResults(writer.toString());
        verifyHeaders(splitResults);
        verifyInitialResults(splitResults);
        verifyPaginatedResults(splitResults, PAGE_SIZE + 1);

        // Test writes with results > max allowed
        stub.setPaginatedResultsAvailable(MAX_EXPORT_ROWS * (PAGE_SIZE + 2));
        params = new PageSortParams<Specimen>(PAGE_SIZE, 0,
                new ArrayList<SpecimenSortCriterion>(), false);
        helper = action.new PaginatedSearchHelper(params, crit, MAX_EXPORT_ROWS * (PAGE_SIZE + 2));
        TissueLocatorRequestHelper.setPaginatedSearchHelper(getRequest(), helper);
        view = getCsvView();
        view.setParameters(getModel(), true, true, true);
        writer = new StringWriter();
        view.doExport(writer);
        String[] rows = StringUtils.split(writer.toString(), '\n');
        assertEquals("ID,External ID", rows[0]);
        assertEquals("1,x1", rows[1]);
        assertEquals("1,exId", rows[MAX_EXPORT_ROWS - 1]);
        assertEquals(MAX_EXPORT_ROWS, rows.length);
        rows = null;

        // Test writes of a single page, with more results available but not written
        stub.setPaginatedResultsAvailable(MAX_EXPORT_ROWS * PAGE_SIZE + 1);
        params = new PageSortParams<Specimen>(PAGE_SIZE, 0,
                new ArrayList<SpecimenSortCriterion>(), false);
        helper = action.new PaginatedSearchHelper(params, crit, MAX_EXPORT_ROWS * PAGE_SIZE + 1);
        TissueLocatorRequestHelper.setPaginatedSearchHelper(getRequest(), helper);
        view = getCsvView();
        view.setParameters(getModel(), false, true, true);
        writer = new StringWriter();
        view.doExport(writer);
        splitResults = getSplitResults(writer.toString());
        verifyHeaders(splitResults);
        verifyInitialResults(splitResults);
        assertEquals(INITIAL_RESULTS_COUNT + 1, splitResults.length);

        // Test no column headers
        TissueLocatorRequestHelper.setPaginatedSearchHelper(getRequest(), null);
        view = getCsvView();
        view.setParameters(getModel(), true, false, true);
        writer = new StringWriter();
        view.doExport(writer);
        splitResults = getSplitResults(writer.toString());
        assertEquals(INITIAL_RESULTS_COUNT, splitResults.length);

        // Test character escaping, decoration, default column header
        view = getCsvView();
        TableModel model = getModel();
        HeaderCell header = new HeaderCell();
        header.setColumnNumber(2);
        header.setBeanPropertyName("collectionYear");
        model.addColumnHeader(header);
        model.getRowListFull().clear();
        Specimen spec = new Specimen();
        spec.setExternalId("comma,test");
        spec.setCollectionYear(1);
        model.addRow(new Row(spec, INITIAL_RESULTS_COUNT + 1));
        model.setTableDecorator(new TableDecorator() {

            /**
             * {@inheritDoc}
             */
            @Override
            public String startRow() {
                return "startrow";
            }

        });
        view.setParameters(model, true, true, false);
        writer = new StringWriter();
        view.doExport(writer);
        String result = writer.toString();
        rows = StringUtils.split(result, '\n');
        assertEquals("ID,External ID,CollectionYear", rows[0]);
        assertEquals("startrow,\"comma,test\",1", rows[1]);
    }

    private TableModel getModel() {
        TableModel model = new TableModel(TableProperties.getInstance(getRequest()), "UTF-8",
                new MockPageContext());
        HeaderCell header = new HeaderCell();
        header.setColumnNumber(0);
        header.setBeanPropertyName("id");
        header.setTitle("ID");
        model.addColumnHeader(header);
        header = new HeaderCell();
        header.setColumnNumber(1);
        header.setBeanPropertyName("externalId");
        header.setTitle("External ID");
        model.addColumnHeader(header);

        List<Specimen> initialResults = getInitialSearchResults();
        int rowNum = 1;
        for (Specimen s : initialResults) {
            model.addRow(new Row(s, rowNum++));
        }

        model.setRowListPage(model.getRowListFull());
        return model;
    }

    private List<Specimen> getInitialSearchResults() {
        List<Specimen> results = new ArrayList<Specimen>();
        for (int i = 1; i <= INITIAL_RESULTS_COUNT; i++) {
            Specimen s = new Specimen();
            s.setId(new Long(i));
            s.setExternalId(EXTERNAL_ID_PREFIX + Integer.toString(i));
            results.add(s);
        }
        return results;
    }

    private TissueLocatorCsvView getCsvView() {
        return  new TissueLocatorCsvView() {

            /**
             * {@inheritDoc}
             */
            @Override
            protected void clearResults() {
                // do nothing
            }

        };
    }

    private String[][] getSplitResults(String results) {
        String[] rows = StringUtils.split(results, '\n');
        String[][] splitResults = new String[rows.length][];
        for (int i = 0; i < rows.length; i++) {
            String[] splitRow = StringUtils.split(rows[i], ',');
            splitResults[i] = splitRow;
        }
        return splitResults;
    }

    private void verifyHeaders(String[][] splitResults) {
        assertEquals(2, splitResults[0].length);
        assertEquals("ID", splitResults[0][0]);
        assertEquals("External ID", splitResults[0][1]);
    }

    private void verifyInitialResults(String[][] splitResults) {
        for (int i = 1; i <= INITIAL_RESULTS_COUNT; i++) {
            assertEquals(2, splitResults[i].length);
            assertEquals(Integer.toString(i), splitResults[i][0]);
            assertEquals(EXTERNAL_ID_PREFIX + Integer.toString(i), splitResults[i][1]);
        }
    }

    private void verifyPaginatedResults(String[][] splitResults, int additionalResults) {
        int additionalResultCount = additionalResults / PAGE_SIZE + (additionalResults % PAGE_SIZE == 0 ? 0 : 1);
        additionalResultCount = Math.min(additionalResultCount, MAX_EXPORT_ROWS - INITIAL_RESULTS_COUNT);
        assertEquals(INITIAL_RESULTS_COUNT + 1 + additionalResultCount, splitResults.length);
        for (int i = 0; i < additionalResultCount; i++) {
            int index = INITIAL_RESULTS_COUNT + 1 + i;
            assertEquals(Integer.toString(1), splitResults[index][0]);
            assertEquals("exId", splitResults[index][1]);
        }
    }
}
