/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

import org.apache.struts2.ServletActionContext;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.service.support.QuestionResponseServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.SupportLetterRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.ShipmentServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.SignedMaterialTransferAgreementServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.SpecimenRequestServiceStub;

/**
 * @author ddasgupta
 *
 */
public class HomeActionTest extends AbstractTissueLocatorWebTest {

    /**
     * test prep of user home page.
     */
    @Test
    public void testPrepare() {
        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        verifyPrepare(false, -1, -1, -1, -1, false, -1, -1, -1, -1, false, -1, false, -1, -1, -1, -1, "welcome");
        request.setRemoteUser("testuser");
        verifyPrepare(true, -1, -1, -1, -1, false, -1, -1, -1, 0, false, -1, false, -1, -1, -1, -1, "simpleSearch");
        request.addUserRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName());
        request.addUserRole(Role.CONSORTIUM_REVIEW_VOTER.getName());
        request.addUserRole(Role.LEAD_REVIEWER.getName());
        request.addUserRole(Role.INSTITUTIONAL_REVIEW_VOTER.getName());
        request.addUserRole(Role.LINE_ITEM_REVIEW_VOTER.getName());
        request.addUserRole(Role.SHIPMENT_ADMIN.getName());
        request.addUserRole(Role.REVIEW_DECISION_NOTIFIER.getName());
        request.addUserRole(Role.USER_ACCOUNT_APPROVER.getName());
        request.addUserRole(Role.MTA_ADMINISTRATOR.getName());
        request.addUserRole(Role.SUPPORT_LETTER_REQUEST_REVIEWER.getName());
        request.addUserRole(Role.QUESTION_RESPONDER.getName());
        assertTrue(request.isUserInRole(Role.SCIENTIFIC_REVIEWER_ASSIGNER.getName()));
        assertTrue(request.isUserInRole(Role.CONSORTIUM_REVIEW_VOTER.getName()));
        assertTrue(request.isUserInRole(Role.INSTITUTIONAL_REVIEW_VOTER.getName()));
        assertTrue(request.isUserInRole(Role.LINE_ITEM_REVIEW_VOTER.getName()));
        assertTrue(request.isUserInRole(Role.SHIPMENT_ADMIN.getName()));
        assertTrue(request.isUserInRole(Role.REVIEW_DECISION_NOTIFIER.getName()));
        assertTrue(request.isUserInRole(Role.USER_ACCOUNT_APPROVER.getName()));
        assertTrue(request.isUserInRole(Role.MTA_ADMINISTRATOR.getName()));
        verifyPrepare(true, 0, 0, 0, 0, true, 0, 0, 0, 0, true, 0, true, 0, 0, 0, 0, "simpleSearch");
    }

    // CHECKSTYLE:OFF � too many parameters
    private void verifyPrepare(boolean searchCalled, int assignReviewerCount, int leadReviewCount,
            int consortiumReviewCount, int institutionalReviewCount, boolean shipmentSearchCalled, int shipmentCount,
            int finalCommentCount, int reviewerDecisionNotificationCount, int requestorActionRequiredCount,
            boolean userSearchCalled, int registrationReviewCount, boolean mtaSearchCalled, int mtaReviewCount,
            int lineItemReviewCount, int supportLetterRequestCount, int questionResponseCount,
            String rightWidgetName) {
    // CHECKSTYLE:ON
        SpecimenRequestServiceStub stub = (SpecimenRequestServiceStub) TissueLocatorRegistry
            .getServiceLocator().getSpecimenRequestService();
        ShipmentServiceStub shipmentStub = (ShipmentServiceStub) TissueLocatorRegistry
            .getServiceLocator().getShipmentService();
        SignedMaterialTransferAgreementServiceStub mtaStub =
            (SignedMaterialTransferAgreementServiceStub) TissueLocatorRegistry.getServiceLocator().
            getSignedMaterialTransferAgreementService();
        HomeAction ha = new HomeAction(getTestUserService(), getTestAppSettingService(),
                getTestSupportLetterRequestService(), getTestQuestionResponseService());
        ha.prepare();
        assertEquals(searchCalled, stub.isSearchCalled());
        assertEquals(assignReviewerCount, ha.getAssignReviewerCount());
        assertEquals(leadReviewCount, ha.getLeadReviewerCount());
        assertEquals(consortiumReviewCount, ha.getConsortiumReviewCount());
        assertEquals(institutionalReviewCount, ha.getInstitutionalReviewCount());
        assertEquals(shipmentSearchCalled, shipmentStub.isSearchCalled());
        assertEquals(shipmentCount, ha.getApprovedRequestCount());
        assertEquals(finalCommentCount, ha.getFinalCommentCount());
        assertEquals(reviewerDecisionNotificationCount, ha.getResearcherDecisionNotificationCount());
        assertEquals(requestorActionRequiredCount, ha.getRequestorActionRequiredCount());
        assertEquals(userSearchCalled, getTestUserService().isSearchCalled());
        assertEquals(registrationReviewCount, ha.getRegistrationReviewCount());
        assertEquals(mtaSearchCalled, mtaStub.isSearchCalled());
        assertEquals(mtaReviewCount, ha.getMtaReviewCount());
        assertEquals(lineItemReviewCount, ha.getLineItemReviewCount());
        assertEquals(supportLetterRequestCount, ha.getSupportLetterRequestCount());
        assertEquals(questionResponseCount, ha.getQuestionResponseCount());
        assertEquals("type", ha.getLeftBrowseWidget());
        assertEquals("diseaseWithCount", ha.getCenterBrowseWidget());
        assertEquals(rightWidgetName, ha.getRightBrowseWidget());
    }

    @SuppressWarnings("unchecked")
    private SupportLetterRequestServiceLocal getTestSupportLetterRequestService() {
        SupportLetterRequestServiceLocal mock = mock(SupportLetterRequestServiceLocal.class);
        stub(mock.count(any(SearchCriteria.class))).toReturn(0);
        return mock;
    }

    @SuppressWarnings("unchecked")
    private QuestionResponseServiceLocal getTestQuestionResponseService() {
        QuestionResponseServiceLocal mock = mock(QuestionResponseServiceLocal.class);
        stub(mock.count(any(SearchCriteria.class))).toReturn(0);
        return mock;
    }
}
