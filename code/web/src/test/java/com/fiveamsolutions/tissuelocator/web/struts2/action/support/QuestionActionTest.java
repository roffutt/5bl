/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.support;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.LobHolder;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.support.Question;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.QuestionServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.support.SupportLetterRequestActionTest.LobHolderProxy;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.InstitutionServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 *
 */
public class QuestionActionTest extends AbstractTissueLocatorWebTest {


    /**
     * Tests preparing the file properties.
     * @throws URISyntaxException on error
     */
    @Test
    public void testPrepareFiles() throws URISyntaxException {
        QuestionAction action = getAction();
        Question oldQuestion = action.getObject();
        action.prepare();
        assertEquals(oldQuestion, action.getObject());
        assertNull(action.getObject().getResume());
        assertNull(action.getObject().getProtocolDocument());

        action.setResume(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setResumeFileName("log4j.properties");
        action.setResumeContentType("text/plain");
        action.setProtocolDocument(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        action.setProtocolDocumentFileName("log4j.properties");
        action.setProtocolDocumentContentType("text/plain");
        action.prepare();
        assertEquals(oldQuestion, action.getObject());
        assertNotNull(action.getObject().getResume());
        assertTrue(action.getObject().getResume().getLob().getData().length > 0);
        assertNotNull(action.getObject().getResume().getName());
        assertNotNull(action.getObject().getResume().getContentType());
        assertNotNull(action.getObject().getProtocolDocument());
        assertTrue(action.getObject().getProtocolDocument().getLob().getData().length > 0);
        assertNotNull(action.getObject().getProtocolDocument().getName());
        assertNotNull(action.getObject().getProtocolDocument().getContentType());
    }

    /**
     * Tests preparing the resume properties.
     * @throws URISyntaxException on error
     */
    @Test
    public void testPrepareResume() throws URISyntaxException {
        TissueLocatorUser u = TissueLocatorSessionHelper.getLoggedInUser(getSession());
        u.setResume(new TissueLocatorFile(new byte[] {1}, "resume.txt", "text/plain"));
        QuestionAction action = getAction();
        assertNotNull(action.getObject().getResume());
        assertNotNull(action.getObject().getResume().getLob());
        assertNull(action.getObject().getResume().getLob().getData());
        TissueLocatorFile oldFile = action.getObject().getResume();
        action.prepare();
        assertEquals(oldFile, action.getObject().getResume());
        assertTrue(action.isCopyinvestigator());

        action.getObject().getResume().setLob(new LobHolderProxy());
        oldFile = action.getObject().getResume();
        action.setCopyinvestigator(false);
        action.prepare();
        assertEquals(oldFile, action.getObject().getResume());
        assertFalse(action.isCopyinvestigator());

        action.getObject().getResume().setLob(new LobHolder());
        action.getObject().getResume().getLob().setId(1L);
        oldFile = action.getObject().getResume();
        action.prepare();
        assertNotSame(oldFile, action.getObject().getResume());
        assertNotNull(action.getObject().getResume().getLob().getData());
    }

    /**
     * tests preparing the institutions properties.
     */
    @Test
    public void testPrepareInstitution() {
        InstitutionServiceStub institutionStub = (InstitutionServiceStub)
            TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        List<Institution> consortiumMembers = new ArrayList<Institution>();
        consortiumMembers.add(new Institution());
        consortiumMembers.get(0).setId(1L);
        consortiumMembers.get(0).setName("consortium member");
        institutionStub.setConsortiumMembers(consortiumMembers);

        QuestionAction action = getAction();
        Question oldQuestion = action.getObject();
        action.prepare();
        assertEquals(oldQuestion, action.getObject());
        assertNotNull(action.getInstitutions());
        assertEquals(1, action.getInstitutions().size());
        assertNotNull(action.getInstitutions().get(0));
        assertEquals(1, action.getInstitutions().get(0).getId().intValue());
        action.setSelectedInstitutions(action.getInstitutions());
        assertNotNull(action.getSelectedInstitutionIds());
        assertEquals(1, action.getSelectedInstitutionIds().size());
        assertNotNull(action.getSelectedInstitutionIds().get(0));
        assertEquals(1, action.getSelectedInstitutionIds().get(0).intValue());
        institutionStub.setConsortiumMembers(new ArrayList<Institution>());
    }

    /**
     * tests preparing the investigator organization properties.
     */
    @Test
    public void testPrepareOrganization() {
        QuestionAction action = getAction();
        Question oldQuestion = action.getObject();
        oldQuestion.getInvestigator().setOrganization(null);
        action.prepare();
        assertEquals(oldQuestion, action.getObject());
        assertNull(action.getInvestigatorOrganizationName());

        oldQuestion = action.getObject();
        Institution i = new Institution();
        i.setName("test");
        oldQuestion.getInvestigator().setOrganization(i);
        action.prepare();
        assertEquals(oldQuestion, action.getObject());
        assertEquals("test", action.getInvestigatorOrganizationName());
    }

    /**
     * Test the save method.
     */
    @Test
    public void testSave() {
        QuestionAction action = getAction();
        assertEquals(Action.SUCCESS, action.save());
        assertEquals(1, action.getActionMessages().size());
        assertEquals("view", action.view());
    }

    /**
     * Tests the list method.
     */
    @Test
    public void testList() {
        QuestionAction action = getAction();
        assertNull(action.getObjects().getList());
        String result = action.list();
        assertEquals(Action.SUCCESS, result);
        assertNotNull(action.getObjects().getList());
        assertEquals(1, action.getObjects().getList().size());
    }

    @SuppressWarnings("unchecked")
    private QuestionAction getAction() {
        QuestionServiceLocal mock = mock(QuestionServiceLocal.class);
        stub(mock.count(any(SearchCriteria.class))).toReturn(1);
        List<Question> results = Collections.singletonList(new Question());
        stub(mock.search(any(SearchCriteria.class))).toReturn(results);
        stub(mock.search(any(SearchCriteria.class), any(PageSortParams.class))).toReturn(results);

        LobHolderServiceLocal lobServiceMock = mock(LobHolderServiceLocal.class);
        LobHolder lob = new LobHolder(new byte[] {1});
        lob.setId(1L);
        stub(lobServiceMock.getLob(anyLong())).toReturn(lob);

        return new QuestionAction(mock, lobServiceMock);
    }

    /**
     * Test the unsupported no-arg constructor.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedConstructor() {
        new QuestionAction();
        fail();
    }
}
