/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.CodeSortCriterion;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;

/**
 * @author ddasgupta
 *
 */
public class CodeServiceStub extends GenericServiceStub<Code> implements CodeServiceLocal {

    private Long id = 0L;
    private int searchResultsSize = 1;

    /**
     * {@inheritDoc}
     */
    public <T extends Code> List<T> getActiveCodes(Class<T> codeType) {
        return getActiveCodes(codeType, CodeSortCriterion.NAME);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends Code> List<T> getActiveCodes(Class<T> codeType, CodeSortCriterion order) {
        try {
            T code = codeType.newInstance();
            code.setId(1L);
            code.setName(codeType.getSimpleName());
            return Collections.singletonList(code);
        } catch (IllegalAccessException e)  {
            return null;
        } catch (InstantiationException e) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int count(SearchCriteria<Code> criteria) {
        return search(criteria).size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Code> search(SearchCriteria<Code> criteria, PageSortParams<Code> params) {
        TissueLocatorAnnotatedBeanSearchCriteria<Code> crit =
            (TissueLocatorAnnotatedBeanSearchCriteria<Code>) criteria;
        List<Code> results = new ArrayList<Code>();
        if (StringUtils.isNotBlank(crit.getCriteria().getName())) {
            crit.getCriteria().setId(1L);
            for (int i = 0; i < searchResultsSize; i++) {
                results.add(crit.getCriteria());
            }
        }
        return results;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Code> search(SearchCriteria<Code> criteria) {
        return search(criteria, null);
    }

    /**
     * {@inheritDoc}
     */
    public <T extends Code> T getCode(String name, Class<T> codeType) {
        try {
            T code = codeType.newInstance();
            code.setName(name);
            code.setValue(name);
            code.setId(id++);
            return code;
        } catch (IllegalAccessException e)  {
            return null;
        } catch (InstantiationException e) {
            return null;
        }
    }

    /**
     * @param searchResultsSize the searchResultsSize to set
     */
    public void setSearchResultsSize(int searchResultsSize) {
        this.searchResultsSize = searchResultsSize;
    }
}
