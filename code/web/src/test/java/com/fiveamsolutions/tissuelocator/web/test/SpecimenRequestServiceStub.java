/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.mail.MessagingException;

import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceBean;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.util.RequestProcessingConfiguration;

/**
 * @author ddasgupta
 */
public class SpecimenRequestServiceStub extends GenericServiceStub<SpecimenRequest>
    implements SpecimenRequestServiceLocal {

    /**
     * ID of request that should throw IllegalStateException because of unavailable specimens.
     */
    public static final Long UNAVAILABLE_SPECIMENS_ID = 987654L;

    /**
     * ID of request that should throw IllegalStateException because of no specimens and prospective collection notes.
     */
    public static final Long NO_SPECIMENS_OR_NOTES_ID = 456789L;

    /**
     * ID of request that should throw IllegalStateException because of no specimens and prospective collection notes.
     */
    public static final Long NO_SPECIMENS_ID = 555555L;

    /**
     * ID of request that should throw IllegalStateException because it can not be deleted.
     */
    public static final Long NOT_DELETABLE_ID = 444444L;

    /**
     * ID of request that should throw other EJBException without a cause.
     */
    public static final Long EJB_NO_CAUSE_ID = 123456L;

    /**
     * ID of request that should throw other EJBException with a cause.
     */
    public static final Long EJB_WITH_CAUSE_ID = 654321L;

    private RequestProcessingConfiguration config;
    private int numReviewers;
    private int votingPeriod;
    private int reviewPeriod;
    private int reviewerAssignmentPeriod;
    private boolean createShipments;

    /**
     * {@inheritDoc}
     */
    public void processPendingRequest(SpecimenRequest request, RequestProcessingConfiguration configuration) {
        savePersistentObject(request);
        config = configuration;
    }

    /**
     * {@inheritDoc}
     */
    public void processPendingDecisionRequest(SpecimenRequest request, RequestProcessingConfiguration configuration) {
        savePersistentObject(request);
        config = configuration;
    }

    /**
     * {@inheritDoc}
     */
    public Long saveDraftRequest(SpecimenRequest request) {
        savePersistentObject(request);
        final Long id = 1234L;
        request.getRequestor().setId(id);
        request.setId(id);
        return id;
    }

    /**
     * {@inheritDoc}
     */
    public Long saveRequest(SpecimenRequest request, int votingPeriodArg, int reviewPeriodArg,
            int reviewerAssignmentPeriodArg, int numReviewersArg) {
        // If we are testing case where IllegalStateException should be thrown, throw Exception
        if (request.getId() == UNAVAILABLE_SPECIMENS_ID) {
            throw new EJBException(
                    new IllegalStateException(SpecimenRequestServiceBean.SPECIMEN_NOT_AVAILABLE));
        } else if (request.getId() == NO_SPECIMENS_OR_NOTES_ID) {
            throw new EJBException(
                    new IllegalStateException(SpecimenRequestServiceBean.SPECIMEN_OR_NOTES_NOT_SELECTED));
        } else if (request.getId() == NO_SPECIMENS_ID) {
            throw new EJBException(
                    new IllegalStateException(SpecimenRequestServiceBean.SPECIMEN_NOT_SELECTED));
        } else if (request.getId() == EJB_WITH_CAUSE_ID) {
            throw new EJBException(new IllegalStateException("other error"));
        } else if (request.getId() == EJB_NO_CAUSE_ID) {
            throw new EJBException("other error");
        }
        votingPeriod = votingPeriodArg;
        reviewPeriod = reviewPeriodArg;
        reviewerAssignmentPeriod = reviewerAssignmentPeriodArg;
        numReviewers = numReviewersArg;
        return savePersistentObject(request);
    }

    /**
     * {@inheritDoc}
     */
    public void deleteRequest(SpecimenRequest request) {
        if (request.getId().equals(NOT_DELETABLE_ID)) {
            throw new EJBException(new IllegalArgumentException(SpecimenRequestServiceBean.CURRENTLY_NOT_DELETABLE));
        }
        if (request.getId().equals(EJB_NO_CAUSE_ID)) {
            throw new EJBException("other error");
        }
        deletePersistentObject(request);
    }

    /**
     * {@inheritDoc}
     */
    public void finalizeVote(SpecimenRequest request, boolean createShipmentsArg)
        throws MessagingException {
        createShipments = createShipmentsArg;
        request.setStatus(request.getFinalVote());
        request.setExternalComment(null);
        savePersistentObject(request);
    }

    /**
     * {@inheritDoc}
     */
    public SpecimenRequest getRequestWithSpecimen(Specimen specimen) {
        SpecimenRequestLineItem li = new SpecimenRequestLineItem();
        li.setSpecimen(specimen);

        SpecimenRequest request = new SpecimenRequest();
        request.setId(1L);
        request.setStatus(RequestStatus.APPROVED);
        request.getLineItems().add(li);
        return specimen.getExternalId() == null ? null : request;
    }

    /**
     * {@inheritDoc}
     */
    public void saveFinalVote(SpecimenRequest request) {
        savePersistentObject(request);
    }

    /**
     * @return the config
     */
    public RequestProcessingConfiguration getConfig() {
        return config;
    }

    /**
     * @return the numReviewers
     */
    public int getNumReviewers() {
        return numReviewers;
    }

    /**
     * @return the votingPeriod
     */
    public int getVotingPeriod() {
        return votingPeriod;
    }

    /**
     * @param votingPeriod the votingPeriod to set
     */
    public void setVotingPeriod(int votingPeriod) {
        this.votingPeriod = votingPeriod;
    }

    /**
     * @return the reviewPeriod
     */
    public int getReviewPeriod() {
        return reviewPeriod;
    }

    /**
     * @param reviewPeriod the reviewPeriod to set
     */
    public void setReviewPeriod(int reviewPeriod) {
        this.reviewPeriod = reviewPeriod;
    }

    /**
     * @return the reviewerAssignmentPeriod
     */
    public int getReviewerAssignmentPeriod() {
        return reviewerAssignmentPeriod;
    }

    /**
     * @param reviewerAssignmentPeriod the reviewerAssignmentPeriod to set
     */
    public void setReviewerAssignmentPeriod(int reviewerAssignmentPeriod) {
        this.reviewerAssignmentPeriod = reviewerAssignmentPeriod;
    }

    /**
     * @return the createShipments
     */
    public boolean isCreateShipments() {
        return createShipments;
    }

    /**
     * {@inheritDoc}
     */
    public List<SpecimenRequest> getUnreviewedRequests(int lineItemReviewGracePeriod) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public int getNewRequestCount(Date startDate, Date endDate, String additionalCondition) {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, Integer> getLateScientificReviewCounts(Date startDate, Date endDate, int votingPeriodArg) {
        return new HashMap<String, Integer>();
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, Integer> getLateInstitutionalReviewCounts(Date startDate, Date endDate, int votingPeriodArg) {
        return new HashMap<String, Integer>();
    }
}
