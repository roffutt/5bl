/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.InstitutionServiceStub;
import com.opensymphony.xwork2.ActionContext;

/**
 * @author gvaughn
 *
 */
public class InstitutionAutoCreateConverterTest extends AbstractTissueLocatorWebTest {

    /**
     * Test conversion of an autocreated institution.
     */
    @Test
    @SuppressWarnings({"rawtypes", "unchecked" })
    public void testConvertFromString() {
        InstitutionAutoCreateConverter converter = new InstitutionAutoCreateConverter();
        
        // confirm expected functionality for non-relevant cases
        assertNull(converter.convertFromString(null, new String[]{"test"}, Institution.class));
        assertNotNull(converter.convertFromString(null, new String[]{"1"}, Institution.class));
        
        // no action if name is not included
        Map<String, String[]> parameterMap = new HashMap<String, String[]>();
        ActionContext.getContext().setParameters((Map) parameterMap);
        assertNull(converter.convertFromString(null, new String[]{null}, Institution.class));
        
        // existing institution with matching name is returned
        parameterMap.put("object.institution.name", new String[]{"institution name"});
        Institution inst = (Institution) converter.convertFromString(null, new String[]{null}, Institution.class);
        assertNotNull(inst);
        assertEquals("institution name", inst.getName());
        assertEquals(InstitutionServiceStub.TYPE_ID, inst.getType().getId());
        
        // existing inst is returned if type matches or is blank
        parameterMap.put("object.institution.type", new String[]{InstitutionServiceStub.TYPE_ID.toString()});
        inst = (Institution) converter.convertFromString(null, new String[]{null}, Institution.class);
        assertNotNull(inst);
        assertEquals("institution name", inst.getName());
        assertEquals(InstitutionServiceStub.TYPE_ID, inst.getType().getId());
        
        parameterMap.put("object.institution.type", new String[]{""});
        inst = (Institution) converter.convertFromString(null, new String[]{null}, Institution.class);
        assertNotNull(inst);
        assertEquals("institution name", inst.getName());
        assertEquals(InstitutionServiceStub.TYPE_ID, inst.getType().getId());
        
        // null returned for non-matching types
        parameterMap.put("object.institution.type", new String[]{"1"});
        assertNull(converter.convertFromString(null, new String[]{null}, Institution.class));
        
        parameterMap.put("object.institution.name", new String[]{InstitutionServiceStub.INVALID_NAME});
        assertNull(converter.convertFromString(null, new String[]{null}, Institution.class));
    }
}
