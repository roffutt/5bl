/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.service.CodeServiceLocal;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceLocal;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceRemote;
import com.fiveamsolutions.tissuelocator.service.FixedConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.FullConsortiumReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.GenericServiceLocal;
import com.fiveamsolutions.tissuelocator.service.HelpConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceRemote;
import com.fiveamsolutions.tissuelocator.service.LineItemReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceRemote;
import com.fiveamsolutions.tissuelocator.service.ReportServiceLocal;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SignedMaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestProcessingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;
import com.fiveamsolutions.tissuelocator.service.locator.ServiceLocator;

/**
 * @author smiller
 */
public class MockServiceLocator implements ServiceLocator {

    private final GenericServiceStub<PersistentObject> genericService = new GenericServiceStub<PersistentObject>();
    private final InstitutionServiceStub institutionService = new InstitutionServiceStub();
    private final CodeServiceStub codeService = new CodeServiceStub();
    private final SpecimenServiceStub specimenService = new SpecimenServiceStub();
    private final CollectionProtocolServiceStub collectionProtocolService = new CollectionProtocolServiceStub();
    private final ParticipantServiceStub participantService = new ParticipantServiceStub();
    private final SpecimenRequestServiceStub specimenRequestService = new SpecimenRequestServiceStub();
    private final ShipmentServiceStub shipmentService = new ShipmentServiceStub();
    private final SpecimenRequestProcessingServiceStub specimenRequestProcessingService =
        new SpecimenRequestProcessingServiceStub();
    private final FixedConsortiumReviewServiceStub fixedConsortiumReviewServiceStub =
        new FixedConsortiumReviewServiceStub();
    private final FullConsortiumReviewServiceStub fullConsortiumReviewServiceStub =
        new FullConsortiumReviewServiceStub();
    private final LineItemReviewServiceStub lineItemReviewServiceStub = new LineItemReviewServiceStub();
    private final MaterialTransferAgreementServiceStub materialTransferAgreementServiceStub =
        new MaterialTransferAgreementServiceStub();
    private final SignedMaterialTransferAgreementServiceStub signedMaterialTransferAgreementServiceStub =
        new SignedMaterialTransferAgreementServiceStub();
    private final ReportServiceStub reportServiceStub = new ReportServiceStub();
    private final HelpConfigServiceStub helpConfigServiceStub = new HelpConfigServiceStub();

    /**
     * {@inheritDoc}
     */
    @Override
    public GenericServiceLocal<PersistentObject> getGenericService() {
        return genericService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InstitutionServiceLocal getInstitutionService() {
        return institutionService;
    }

    /**
     * {@inheritDoc}
     */
    public InstitutionServiceRemote getInstitutionServiceRemote() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CodeServiceLocal getCodeService() {
        return codeService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenServiceLocal getSpecimenService() {
        return specimenService;
    }

    /**
     * {@inheritDoc}
     */
    public SpecimenServiceRemote getSpecimenServiceRemote() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CollectionProtocolServiceLocal getCollectionProtocolService() {
        return collectionProtocolService;
    }

    /**
     * {@inheritDoc}
     */
    public CollectionProtocolServiceRemote getCollectionProtocolServiceRemote() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ParticipantServiceLocal getParticipantService() {
        return participantService;
    }

    /**
     * {@inheritDoc}
     */
    public ParticipantServiceRemote getParticipantServiceRemote() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenRequestServiceLocal getSpecimenRequestService() {
        return specimenRequestService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShipmentServiceLocal getShipmentService() {
        return shipmentService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpecimenRequestProcessingServiceLocal getSpecimenRequestProcessingService() {
        return specimenRequestProcessingService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FixedConsortiumReviewServiceLocal getFixedConsortiumReviewService() {
        return fixedConsortiumReviewServiceStub;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FullConsortiumReviewServiceLocal getFullConsortiumReviewService() {
        return fullConsortiumReviewServiceStub;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LineItemReviewServiceLocal getLineItemReviewService() {
        return lineItemReviewServiceStub;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MaterialTransferAgreementServiceLocal getMaterialTransferAgreementService() {
        return materialTransferAgreementServiceStub;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SignedMaterialTransferAgreementServiceLocal getSignedMaterialTransferAgreementService() {
        return signedMaterialTransferAgreementServiceStub;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReportServiceLocal getReportService() {
        return reportServiceStub;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HelpConfigServiceLocal getHelpConfigService() {
        return helpConfigServiceStub;
    }
}
