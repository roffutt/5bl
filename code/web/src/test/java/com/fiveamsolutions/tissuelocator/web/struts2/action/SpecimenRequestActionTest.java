/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import javax.ejb.EJBException;
import javax.mail.MessagingException;

import org.apache.struts2.ServletActionContext;
import org.junit.Test;
import org.springframework.mock.web.MockServletContext;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.IrbApprovalStatus;
import com.fiveamsolutions.tissuelocator.data.LobHolder;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicFieldCategory;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.data.validation.DisableableUtil;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractSpecimenRequestAction.SpecimenRequestHeaderKey;
import com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails.AbstractCartAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails.AbstractCartSpecimenAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails.AbstractSpecimenRequestAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails.DbCartAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails.DbSpecimenRequestAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails.SessionCartAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails.SessionSpecimenRequestAction;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.SpecimenRequestServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 *
 */
public class SpecimenRequestActionTest extends AbstractTissueLocatorWebTest {

    private static final int VOTING_PERIOD = 15;
    private static final int REVIEW_PERIOD = 10;
    private static final int REVIEWER_ASSIGNMENT_PERIOD = 1;
    private static final String PROSPECTIVE_COLLECTION_NOTES = "prospectve collection notes";


    /**
     * tests the add to cart method.
     * @throws IllegalAccessException on constructor error.
     * @throws InstantiationException on construction error.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testAddToCart() throws IllegalAccessException, InstantiationException {
        List<Class<? extends AbstractCartAction>> actionTypes =
            Arrays.asList(SessionCartAction.class, DbCartAction.class);
        for (Class<? extends AbstractCartAction> clazz : actionTypes) {
            AbstractCartAction ca = getCartAction(clazz);
            String result = addToCart(ca);
            assertEquals("cart", result);
            assertEquals(2, getSessionCart().getLineItems().size());
            assertTrue(ca.getSpecimenIdsInCart().contains(1L));
            assertTrue(ca.getSpecimenIdsInCart().contains(2L));
            assertFalse(ca.getActionMessages().isEmpty());
            assertEquals(1, ca.getActionMessages().size());
        }
    }

    private String addToCart(AbstractCartAction ca) {
        ca.prepare();
        ca.getSelection().add(1L);
        ca.getSelection().add(2L);
        return ca.addToCart();
    }

    private String addProspectiveCollectionToCart(AbstractCartAction ca) {
        ca.prepare();
        ca.getObject().setProspectiveCollectionNotes(PROSPECTIVE_COLLECTION_NOTES);
        return ca.addProspectiveCollectionToCart();
    }

    /**
     * tests the remove from cart method.
     * @throws IllegalAccessException on constructor error.
     * @throws InstantiationException on construction error.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testRemoveFromCart() throws IllegalAccessException, InstantiationException {
        List<Class<? extends AbstractCartAction>> actionTypes =
            Arrays.asList(SessionCartAction.class, DbCartAction.class);
        for (Class<? extends AbstractCartAction> clazz : actionTypes) {
            AbstractCartAction ca = getCartAction(clazz);
            addToCart(ca);
            Iterator<SpecimenRequestLineItem> it = getSessionCart().getLineItems().iterator();
            SpecimenRequestLineItem s1 = it.next();
            SpecimenRequestLineItem s2 = it.next();
            ca = getCartAction(clazz);
            ca.getSelection().add(s1.getSpecimen().getId());
            ca.prepare();
            String result = ca.removeFromCart();
            assertEquals("cart", result);
            assertEquals(1, getSessionCart().getLineItems().size());
            assertTrue(ca.getSpecimenIdsInCart().contains(s2.getSpecimen().getId()));
            assertFalse(ca.getActionMessages().isEmpty());
            assertEquals(1, ca.getActionMessages().size());
        }
    }

    /**
     * tests the empty cart method.
     * @throws IllegalAccessException on constructor error.
     * @throws InstantiationException on construction error.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testEmptyCart() throws IllegalAccessException, InstantiationException {
        List<Class<? extends AbstractCartAction>> actionTypes =
            Arrays.asList(SessionCartAction.class, DbCartAction.class);
        for (Class<? extends AbstractCartAction> clazz : actionTypes) {
            AbstractCartAction ca = getCartAction(clazz);
            addToCart(ca);
            addProspectiveCollectionToCart(ca);
            ca = getCartAction(clazz);
            ca.prepare();
            String result = ca.emptyCart();
            assertEquals("cart", result);
            assertTrue(getSessionCart().getLineItems().isEmpty());
            assertTrue(getSessionCart().getProspectiveCollectionNotes() == null);
            assertFalse(ca.getActionMessages().isEmpty());
            assertEquals(2, ca.getActionMessages().size());

            ca = getCartAction(clazz);
            addToCart(ca);
            ca = getCartAction(clazz);
            ca.prepare();
            result = ca.emptyCart();
            assertTrue(getSessionCart().getLineItems().isEmpty());
            assertFalse(ca.getActionMessages().isEmpty());
            assertEquals(1, ca.getActionMessages().size());
        }
    }

    /**
     * Test the addition of prospective collection data to the cart.
     */
    @Test
    public void testAddProspectiveCollectionToCart() {
        AbstractCartAction ca = getSessionCartAction();
        String result = addProspectiveCollectionToCart(ca);
        assertEquals("cart", result);
        assertFalse(getSessionCart().getProspectiveCollectionNotes() == null);
        assertFalse(ca.getActionMessages().isEmpty());
    }

    /**
     * Tests the removal of prospective collection data from the cart.
     */
    @Test
    public void testRemoveProspectiveCollectionFromCart() {
        AbstractCartAction ca = getSessionCartAction();
        addProspectiveCollectionToCart(ca);
        ca = getSessionCartAction();
        ca.prepare();
        String result = ca.removeProspectiveCollectionFromCart();
        assertEquals("cart", result);
        assertTrue(getSessionCart().getProspectiveCollectionNotes() == null);
        assertFalse(ca.getActionMessages().isEmpty());
        ca.clearErrorsAndMessages();
        result = ca.removeProspectiveCollectionFromCart();
        assertEquals("cart", result);
        assertNull(getSessionCart().getProspectiveCollectionNotes());
        assertFalse(ca.getActionMessages().isEmpty());
    }

    /**
     * Tests the editProspectiveCollection method.
     */
    @Test
    public void testEditProspectiveCollection() {
        AbstractCartAction ca = getSessionCartAction();
        ca.prepare();
        String result = ca.editProspectiveCollection();
        assertEquals("prospectiveCollection", result);
        assertTrue(ca.getActionMessages().isEmpty());
    }

    /**
     * tests the view cart method.
     */
    @Test
    public void testViewCart() {
        AbstractCartAction ca = getSessionCartAction();
        String result = ca.viewCart();
        assertEquals("cart", result);
        assertEquals(0, ca.getActionMessages().size());

        ca = getDbCartAction();
        result = ca.viewCart();
        assertEquals("cart", result);
        assertEquals(0, ca.getActionMessages().size());
    }

    @SuppressWarnings("unchecked")
    private SpecimenRequest getSessionCart() {
        Enumeration<String> names = getSession().getAttributeNames();
        names.nextElement();
        return (SpecimenRequest) getSession().getAttribute(names.nextElement());
    }

    /**
     * tests the save draft method.
     *
     * @throws MessagingException on error
     */
    @Test
    public void testSaveDraft() throws MessagingException {
        SpecimenRequestServiceStub stub = (SpecimenRequestServiceStub) TissueLocatorRegistry.getServiceLocator()
                .getSpecimenRequestService();
        AbstractCartSpecimenAction sra = getSessionSpecimenRequestAction();
        sra.setSubmit(sra.getText("specimenRequest.edit.saveDraft"));
        sra.prepare();
        TissueLocatorUser requestor = new TissueLocatorUser();
        requestor.setEmail("moooo");
        sra.getObject().setRequestor(requestor);
        assertEquals("input", sra.saveDraftOrConfirm());

        assertNotNull(stub.getObject());
        assertEquals(stub.getObject(), sra.getObject());
        assertEquals(1, sra.getActionMessages().size());
    }

    /**
     * tests the save draft method and then an actual save.
     *
     * @throws MessagingException on error
     */
    @Test
    public void testSaveDraftThenSave() throws MessagingException {
        SpecimenRequestServiceStub stub = (SpecimenRequestServiceStub) TissueLocatorRegistry.getServiceLocator()
                .getSpecimenRequestService();
        AbstractSpecimenRequestAction sra = getSessionSpecimenRequestAction();
        sra.setSubmit(sra.getText("specimenRequest.edit.saveDraft"));
        sra.prepare();
        TissueLocatorUser requestor = new TissueLocatorUser();
        requestor.setEmail("moooo");
        sra.getObject().setRequestor(requestor);
        assertEquals("input", sra.saveDraftOrConfirm());

        assertNotNull(stub.getObject());
        assertEquals(stub.getObject(), sra.getObject());
        assertEquals(1, sra.getActionMessages().size());

        MockServletContext context = (MockServletContext) ServletActionContext.getServletContext();
        context.addInitParameter("numberRequestReviewers", "1");
        context.addInitParameter("reviewPeriod", String.valueOf(REVIEW_PERIOD));
        context.addInitParameter("votingPeriod", String.valueOf(VOTING_PERIOD));
        context.addInitParameter("reviewerAssignmentPeriod", String.valueOf(REVIEWER_ASSIGNMENT_PERIOD));
        SpecimenRequest originalRequest = getSessionCart();
        assertEquals("view", sra.view());
        assertEquals(originalRequest, getSessionCart());
        assertEquals("thankYou", sra.save());
        assertNotNull(stub.getObject());
        assertEquals(stub.getObject(), sra.getObject());
        assertEquals(VOTING_PERIOD, stub.getVotingPeriod());
        assertEquals(REVIEW_PERIOD, stub.getReviewPeriod());
        assertEquals(REVIEWER_ASSIGNMENT_PERIOD, stub.getReviewerAssignmentPeriod());
        assertEquals(1, stub.getNumReviewers());
    }

    /**
     * Tests the delete method.
     */
    @Test
    public void testDelete() {
        AbstractCartSpecimenAction sra = getDbSpecimenRequestAction();
        sra.prepare();
        sra.getObject().setId(1L);
        SpecimenRequest sessionRequest = TissueLocatorSessionHelper.getCart(getSession());
        assertEquals("deleteSuccess", sra.delete());
        assertNull(sra.getObject());
        assertNotSame(sessionRequest, TissueLocatorSessionHelper.getCart(getSession()));

        sra = getDbSpecimenRequestAction();
        sra.prepare();
        sra.getObject().setId(SpecimenRequestServiceStub.NOT_DELETABLE_ID);
        assertEquals("deleteSuccess", sra.delete());
        assertEquals("specimenRequest.error.delete", sra.getSpecimenRequestError());

        sra = getDbSpecimenRequestAction();
        sra.prepare();
        sra.getObject().setId(SpecimenRequestServiceStub.EJB_NO_CAUSE_ID);
        try {
            sra.delete();
            fail("Exception expected.");
        } catch (EJBException e) {
            // expected
        }
        sra = getDbSpecimenRequestAction();
        sra.getObject().setLineItems(null);
        sra.getObject().setAggregateLineItems(null);
        sra.prepare();
    }

    /**
     * tests the save and confirm methods.
     *
     * @throws MessagingException on error
     * @throws IllegalAccessException on constructor error.
     * @throws InstantiationException on constructor error.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testSave() throws MessagingException, InstantiationException, IllegalAccessException {
        MockServletContext context = (MockServletContext) ServletActionContext.getServletContext();
        context.addInitParameter("numberRequestReviewers", "1");
        context.addInitParameter("reviewPeriod", String.valueOf(REVIEW_PERIOD));
        context.addInitParameter("votingPeriod", String.valueOf(VOTING_PERIOD));
        context.addInitParameter("reviewerAssignmentPeriod", String.valueOf(REVIEWER_ASSIGNMENT_PERIOD));
        SpecimenRequestServiceStub stub = (SpecimenRequestServiceStub) TissueLocatorRegistry.getServiceLocator()
                .getSpecimenRequestService();

        List<Class<? extends AbstractSpecimenRequestAction>> actionTypes =
            Arrays.asList(SessionSpecimenRequestAction.class, DbSpecimenRequestAction.class);
        for (Class<? extends AbstractSpecimenRequestAction> clazz : actionTypes) {
            AbstractSpecimenRequestAction sra = getSpecimenRequestAction(clazz);
            sra.setSubmit("specimenRequest.edit.continue");
            sra.prepare();
            assertEquals("confirm", sra.saveDraftOrConfirm());
            SpecimenRequest originalRequest = getSessionCart();
            assertEquals("view", sra.view());
            assertEquals(originalRequest, getSessionCart());
            assertEquals("thankYou", sra.save());
            assertNotNull(stub.getObject());
            assertEquals(stub.getObject(), sra.getObject());
            assertEquals(VOTING_PERIOD, stub.getVotingPeriod());
            assertEquals(REVIEW_PERIOD, stub.getReviewPeriod());
            assertEquals(REVIEWER_ASSIGNMENT_PERIOD, stub.getReviewerAssignmentPeriod());
            assertEquals(1, stub.getNumReviewers());

            sra.setSubmit("specimenRequest.edit.continue");
            sra.getObject().setId(1L);
            sra.prepare();
            originalRequest = getSessionCart();
            sra.getObject().setId(2L);
            sra.prepare();
            assertEquals("confirm", sra.saveDraftOrConfirm());
            assertEquals("view", sra.view());
            assertEquals(originalRequest, getSessionCart());
            assertEquals("thankYou", sra.save());

            sra.setSubmit("specimenRequest.edit.continue");
            sra.prepare();
            sra.getObject().getStudy().setIrbApprovalStatus(IrbApprovalStatus.EXEMPT);
            assertEquals("confirm", sra.saveDraftOrConfirm());

            sra.setSubmit("specimenRequest.edit.continue");
            sra.prepare();
            sra.getObject().getStudy().setIrbApprovalStatus(IrbApprovalStatus.APPROVED);
            assertEquals("confirm", sra.saveDraftOrConfirm());
            assertNull(sra.getObject().getStudy().getIrbExemptionLetter());

            sra.setSubmit("specimenRequest.edit.continue");
            sra.prepare();
            sra.getObject().getShipment().setBillingRecipient(new Person());
            sra.setIncludeBillingRecipient(true);
            assertEquals("confirm", sra.saveDraftOrConfirm());
            assertNotNull(sra.getObject().getShipment().getBillingRecipient());

            sra.setSubmit("specimenRequest.edit.continue");
            sra.prepare();
            sra.getObject().getShipment().setBillingRecipient(new Person());
            sra.setIncludeBillingRecipient(false);
            assertEquals("confirm", sra.saveDraftOrConfirm());
            assertNull(sra.getObject().getShipment().getBillingRecipient());

            sra.setSubmit("specimenRequest.edit.continue");
            sra.prepare();
            sra.getObject().getStudy().setIrbApprovalStatus(IrbApprovalStatus.APPROVED);
            assertEquals("confirm", sra.saveDraftOrConfirm());
            assertNull(sra.getObject().getStudy().getIrbApprovalExpirationDate());
            assertNull(sra.getObject().getStudy().getIrbApprovalLetter());
            assertNull(sra.getObject().getStudy().getIrbApprovalNumber());

            getTestAppSettingService().setMtaCertificationRequired(true);
            sra.setSubmit("specimenRequest.edit.continue");
            sra.prepare();
            assertEquals("certifyMta", sra.saveDraftOrConfirm());
            getTestAppSettingService().setMtaCertificationRequired(false);
        }
    }

    /**
     * tests the save in cases where specimen is not available or no specimens were selected.
     *
     * @throws MessagingException on error
     */
    @Test
    public void testSaveIllegalStateException() throws MessagingException {
        MockServletContext context = (MockServletContext) ServletActionContext.getServletContext();
        context.addInitParameter("numberRequestReviewers", "1");
        context.addInitParameter("reviewPeriod", String.valueOf(REVIEW_PERIOD));
        context.addInitParameter("votingPeriod", String.valueOf(VOTING_PERIOD));
        context.addInitParameter("reviewerAssignmentPeriod", String.valueOf(REVIEWER_ASSIGNMENT_PERIOD));

        AbstractSpecimenRequestAction sra = getSessionSpecimenRequestAction();

        sra.prepare();
        sra.getObject().setId(SpecimenRequestServiceStub.UNAVAILABLE_SPECIMENS_ID);
        assertEquals("cart", sra.save());
        assertEquals("specimenRequest.error.unavailable", sra.getSpecimenRequestError());

        sra.getObject().setId(SpecimenRequestServiceStub.NO_SPECIMENS_OR_NOTES_ID);
        assertEquals("cart", sra.save());
        assertEquals("specimenRequest.error.noSpecimensOrNotes", sra.getSpecimenRequestError());

        sra.getObject().setId(SpecimenRequestServiceStub.NO_SPECIMENS_ID);
        assertEquals("cart", sra.save());
        assertEquals("specimenRequest.error.noSpecimens", sra.getSpecimenRequestError());

        sra.getObject().setId(SpecimenRequestServiceStub.EJB_WITH_CAUSE_ID);
        try {
            sra.save();
            fail("Exception expected");
        } catch (EJBException ejb) {
            // expected
        }
        sra.getObject().setId(SpecimenRequestServiceStub.EJB_NO_CAUSE_ID);
        try {
            sra.save();
            fail("Exception expected");
        } catch (EJBException ejb) {
            // expected
        }
    }

    /**
     * Test certifyMta method.
     * @throws Exception on error.
     */
    @Test
    public void testCertifyMta() throws Exception {
        AbstractSpecimenRequestAction sra = getSessionSpecimenRequestAction();
        sra.setSubmittedMtaDocument(new File(
                ClassLoader.getSystemResource("log4j.properties").toURI()));
        sra.setSubmittedMtaDocumentFileName("log4j.properties");
        sra.setSubmittedMtaDocumentContentType("text/plain");
        sra.setSignedRecipientMta(new SignedMaterialTransferAgreement());
        sra.getSignedRecipientMta().setStatus(
                SignedMaterialTransferAgreementStatus.PENDING_REVIEW);
        assertTrue(sra.isSignedRecipientMtaPending());
        assertFalse(sra.isSignedRecipientMtaValid());
        assertEquals("confirm", sra.certifyMta());
        assertNotNull(sra.getSignedRecipientMta());

        sra = getSessionSpecimenRequestAction();
        sra.setSignedRecipientMta(new SignedMaterialTransferAgreement());
        sra.getSignedRecipientMta().setStatus(
                SignedMaterialTransferAgreementStatus.APPROVED);
        assertEquals("confirm", sra.certifyMta());
        assertFalse(sra.isSignedRecipientMtaPending());
        assertTrue(sra.isSignedRecipientMtaValid());
    }

    /**
     * Test mta submission.
     * @throws Exception on error.
     */
    @Test
    public void testUploadMta() throws Exception {
        AbstractSpecimenRequestAction sra = getSessionSpecimenRequestAction();
        sra.setSubmittedMtaDocument(new File(
                ClassLoader.getSystemResource("log4j.properties").toURI()));
        sra.setSubmittedMtaDocumentFileName("log4j.properties");
        sra.setSubmittedMtaDocumentContentType("text/plain");
        assertEquals("input", sra.uploadMta());
        assertNotNull(sra.getSignedRecipientMta());
        assertEquals(1, sra.getActionMessages().size());
    }

    /**
     * tests the prepare method.
     *
     * @throws Exception on error
     */
    //CHECKSTYLE:OFF - method length
    @Test
    public void testPrepare() throws Exception {
    //CHECKSTYLE:ON
        AbstractSpecimenRequestAction sra = getSessionSpecimenRequestAction();
        SpecimenRequest oldSr = sra.getObject();
        Date oldDate = oldSr.getUpdatedDate();
        sra.setLineItems(null);
        sra.setGeneralLineItemsArray(null);
        sra.setSpecificLineItemsArray(null);
        sra.setSpecificLineSet(new LinkedHashSet<AggregateSpecimenRequestLineItem>());
        sra.setGeneralLineSet(new LinkedHashSet<AggregateSpecimenRequestLineItem>());
        sra.prepare();
        assertNotNull(sra.getObject());
        assertNotSame(oldSr, sra.getObject());
        assertNotNull(sra.getObject().getUpdatedDate());
        assertNotSame(oldDate, sra.getObject().getUpdatedDate());
        assertNotNull(sra.getPaginatedList());
        assertNull(sra.getObject().getInvestigator().getResume());
        assertNull(sra.getObject().getStudy().getProtocolDocument());
        assertNull(sra.getObject().getStudy().getIrbExemptionLetter());
        assertNull(sra.getObject().getStudy().getIrbApprovalLetter());
        assertNull(sra.getInvestigatorOrganizationName());
        assertNull(sra.getRecipientOrganizationName());
        assertNull(sra.getBillingRecipientOrganizationName());
        assertNotNull(sra.getCurrentMta());
        assertNull(sra.getSignedRecipientMta());
        assertTrue(sra.isDisplayPILegalField());
        assertTrue(sra.isDisplayProspectiveCollection());
        assertFalse(sra.isDisplayAggregateSearchResults());
        assertTrue(sra.isDisplayRequestReviewVotes());
        assertFalse(sra.isMtaCertificationRequired());
        assertFalse(sra.isIrbApprovalRequired());
        assertFalse(sra.isSignedRecipientMtaPending());
        assertFalse(sra.isSignedRecipientMtaValid());
        assertNotNull(sra.getReviewProcess());
        assertEquals(ReviewProcess.FULL_CONSORTIUM_REVIEW, sra.getReviewProcess());
        assertNotNull(sra.getLineItems());
        assertNotNull(sra.getHelpConfig());
        assertNotNull(sra.getMinimumFundingRequired());
        assertTrue(sra.isProtocolDocumentRequired());
        assertTrue(sra.isDisplayRequestProcessSteps());
        assertTrue(sra.isDisplayFundingStatusPending());
        assertTrue(sra.isDisplayShipmentBillingRecipient());
        assertFalse(sra.isIncludeBillingRecipient());

        oldSr = sra.getObject();
        oldDate = oldSr.getUpdatedDate();
        Institution i = new Institution();
        i.setName("test");
        oldSr.getInvestigator().setOrganization(i);
        oldSr.getShipment().getRecipient().setOrganization(i);
        oldSr.getShipment().setBillingRecipient(new Person());
        oldSr.getShipment().getBillingRecipient().setOrganization(i);
        getTestAppSettingService().setMtaCertificationRequired(true);
        sra.setResume(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        sra.setResumeFileName("log4j.properties");
        sra.setResumeContentType("text/plain");
        sra.setProtocolDocument(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        sra.setProtocolDocumentFileName("log4j.properties");
        sra.setProtocolDocumentContentType("text/plain");
        sra.setIrbExemptionLetter(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        sra.setIrbExemptionLetterFileName("log4j.properties");
        sra.setIrbExemptionLetterContentType("text/plain");
        sra.setIrbApprovalLetter(new File(ClassLoader.getSystemResource("log4j.properties").toURI()));
        sra.setIrbApprovalLetterFileName("log4j.properties");
        sra.setIrbApprovalLetterContentType("text/plain");

        sra.prepare();
        assertEquals(oldSr, sra.getObject());
        assertNotSame(oldDate, sra.getObject().getUpdatedDate());
        assertNotNull(sra.getObject().getInvestigator().getResume());
        assertTrue(sra.getObject().getInvestigator().getResume().getLob().getData().length > 0);
        assertNotNull(sra.getObject().getInvestigator().getResume().getName());
        assertNotNull(sra.getObject().getInvestigator().getResume().getContentType());
        assertNotNull(sra.getObject().getStudy().getProtocolDocument());
        assertTrue(sra.getObject().getStudy().getProtocolDocument().getLob().getData().length > 0);
        assertNotNull(sra.getObject().getStudy().getProtocolDocument().getName());
        assertNotNull(sra.getObject().getStudy().getProtocolDocument().getContentType());
        assertNotNull(sra.getObject().getStudy().getIrbExemptionLetter());
        assertTrue(sra.getObject().getStudy().getIrbExemptionLetter().getLob().getData().length > 0);
        assertNotNull(sra.getObject().getStudy().getIrbExemptionLetter().getName());
        assertNotNull(sra.getObject().getStudy().getIrbExemptionLetter().getContentType());
        assertNotNull(sra.getObject().getStudy().getIrbApprovalLetter());
        assertTrue(sra.getObject().getStudy().getIrbApprovalLetter().getLob().getData().length > 0);
        assertNotNull(sra.getObject().getStudy().getIrbApprovalLetter().getName());
        assertNotNull(sra.getObject().getStudy().getIrbApprovalLetter().getContentType());
        assertEquals("test", sra.getInvestigatorOrganizationName());
        assertEquals("test", sra.getRecipientOrganizationName());
        assertEquals("test", sra.getBillingRecipientOrganizationName());
        assertTrue(sra.isDisplayPILegalField());
        assertTrue(sra.isDisplayProspectiveCollection());
        assertTrue(sra.isMtaCertificationRequired());
        assertTrue(sra.isIncludeBillingRecipient());

        sra = getDbSpecimenRequestAction();
        oldSr = sra.getObject();
        SpecimenRequest sessionSr = TissueLocatorSessionHelper.getCart(getSession());
        assertNotSame(oldSr, sessionSr);
        sra.getObject().setId(1L);
        sra.prepare();
        assertNotSame(oldSr, sra.getObject());
        assertNotSame(sessionSr, sra.getObject());
        assertNotSame(oldSr, TissueLocatorSessionHelper.getCart(getSession()));
        assertTrue(sra.isDisplayPILegalField());
        assertTrue(sra.isDisplayProspectiveCollection());

        sra = getDbSpecimenRequestAction();
        oldSr = sra.getObject();
        sessionSr = TissueLocatorSessionHelper.getCart(getSession());
        assertNotSame(oldSr, sessionSr);
        sra.getObject().setId(2L);
        sra.prepare();
        assertNotSame(oldSr, sra.getObject());
        assertNotSame(sessionSr, sra.getObject());
        assertNotSame(oldSr, TissueLocatorSessionHelper.getCart(getSession()));
        assertTrue(sra.isDisplayPILegalField());
        assertTrue(sra.isDisplayProspectiveCollection());

        sra = getSessionSpecimenRequestAction();
        oldSr = sra.getObject();
        sessionSr = TissueLocatorSessionHelper.getCart(getSession());
        assertNotSame(oldSr, sessionSr);
        sra.getObject().setId(1L);
        sra.prepare();
        assertNotSame(oldSr, sra.getObject());
        assertNotSame(sessionSr, sra.getObject());
        assertNotSame(oldSr, TissueLocatorSessionHelper.getCart(getSession()));
        assertTrue(sra.isDisplayPILegalField());
        assertTrue(sra.isDisplayProspectiveCollection());

        sra = getSessionSpecimenRequestAction();
        oldSr = sra.getObject();
        sessionSr = TissueLocatorSessionHelper.getCart(getSession());
        assertNotSame(oldSr, sessionSr);
        sra.getObject().setId(2L);
        sra.prepare();
        assertNotSame(oldSr, sra.getObject());
        assertNotSame(sessionSr, sra.getObject());
        assertNotSame(oldSr, TissueLocatorSessionHelper.getCart(getSession()));
        assertTrue(sra.isDisplayPILegalField());
        assertTrue(sra.isDisplayProspectiveCollection());
        sra = getSessionSpecimenRequestAction();
        sra.setSubmit("specimenRequest.edit.saveDraft");
        sra.prepare();
        assertEquals(RequestStatus.DRAFT, sra.getObject().getStatus());
        sra = getDbSpecimenRequestAction();
        sra.prepare();
        sra.getObject().setStatus(RequestStatus.PENDING_REVISION);
        sra.setSubmit("specimenRequest.edit.saveDraft");
        sra.prepare();
        assertEquals(RequestStatus.PENDING_REVISION, sra.getObject().getStatus());
        sra = getSessionSpecimenRequestAction();
        sra.setSubmit("specimenRequest.edit.continue");
        sra.prepare();
    }

    /**
     * Test validation disabling when recovering a request from the session.
     */
    @Test
    public void testValidationDisablingWithSessionCart() {
        DisableableUtil.resetThreadLocal();
        AbstractSpecimenRequestAction sra = getSessionSpecimenRequestAction();
        sra.getObject().setId(1L);
        sra.getObject().setStatus(RequestStatus.DRAFT);
        SpecimenRequest sessionRequest = new SpecimenRequest();
        sessionRequest.setId(1L);
        sessionRequest.setStatus(RequestStatus.DRAFT);
        TissueLocatorSessionHelper.setCart(getSession(), sessionRequest);
        sra.prepare();
        assertEquals(sra.getObject(), sessionRequest);
        assertTrue(DisableableUtil.isValidationDisabled());
        DisableableUtil.resetThreadLocal();

        sessionRequest.setStatus(null);
        sra = getSessionSpecimenRequestAction();
        sra.getObject().setId(1L);
        sra.getObject().setStatus(RequestStatus.DRAFT);
        sra.prepare();
        assertEquals(sra.getObject(), sessionRequest);
        assertFalse(DisableableUtil.isValidationDisabled());
        DisableableUtil.resetThreadLocal();

        sessionRequest.setStatus(RequestStatus.DRAFT);
        sra = getSessionSpecimenRequestAction();
        sra.getObject().setId(1L);
        sra.getObject().setStatus(null);
        sra.prepare();
        assertEquals(sra.getObject(), sessionRequest);
        assertTrue(DisableableUtil.isValidationDisabled());
        DisableableUtil.resetThreadLocal();

        sessionRequest.setStatus(null);
        sra = getSessionSpecimenRequestAction();
        sra.getObject().setId(1L);
        sra.getObject().setStatus(null);
        sra.setSubmit(sra.getText("specimenRequest.edit.saveDraft"));
        sra.prepare();
        assertEquals(sra.getObject(), sessionRequest);
        assertTrue(DisableableUtil.isValidationDisabled());
        DisableableUtil.resetThreadLocal();

        sessionRequest.setStatus(RequestStatus.DRAFT);
        sra = getSessionSpecimenRequestAction();
        sra.getObject().setId(1L);
        sra.getObject().setStatus(RequestStatus.DRAFT);
        sra.setSubmit(sra.getText("specimenRequest.edit.continue"));
        sra.prepare();
        assertEquals(sra.getObject(), sessionRequest);
        assertFalse(DisableableUtil.isValidationDisabled());
        DisableableUtil.resetThreadLocal();
    }

    /**
     * tests preparing the funding source properties.
     */
    @Test
    public void testPrepareFundingSource() {
        AbstractSpecimenRequestAction sra = getSessionSpecimenRequestAction();
        sra.prepare();
        assertNotNull(sra.getFundingSources());
        assertEquals(1, sra.getFundingSources().size());
        assertNotNull(sra.getFundingSources().get(0));
        assertEquals(1, sra.getFundingSources().get(0).getId().intValue());
        sra.getObject().getStudy().setFundingSources(sra.getFundingSources());
        assertNotNull(sra.getFundingSourceIds());
        assertEquals(1, sra.getFundingSourceIds().size());
        assertNotNull(sra.getFundingSourceIds().get(0));
        assertEquals(1, sra.getFundingSourceIds().get(0).intValue());
    }

    /**
     * tests the downloads methods.
     */
    @Test
    public void testDownloads() {
        AbstractSpecimenRequestAction sra = getSessionSpecimenRequestAction();
        sra.getObject().getInvestigator().setResume(new TissueLocatorFile(new byte[0], null, null));
        assertEquals("downloadResume", sra.downloadResume());
        assertNotNull(sra.getResumeStream());
        sra.getObject().getStudy().setProtocolDocument(new TissueLocatorFile(new byte[0], null, null));
        assertEquals("downloadProtocolDocument", sra.downloadProtocolDocument());
        assertNotNull(sra.getProtocolDocumentStream());
        sra.getObject().getStudy().setIrbExemptionLetter(new TissueLocatorFile(new byte[0], null, null));
        assertEquals("downloadIrbExemptionLetter", sra.downloadIrbExemptionLetter());
        assertNotNull(sra.getIrbExemptionLetterStream());
        sra.getObject().getStudy().setIrbApprovalLetter(new TissueLocatorFile(new byte[0], null, null));
        assertEquals("downloadIrbApprovalLetter", sra.downloadIrbApprovalLetter());
        assertNotNull(sra.getIrbApprovalLetterStream());

    }

    /**
     * tests calling the update cart method.
     */
    @Test
    public void testUpdateCart() {
        AbstractCartAction ca = getSessionCartAction();
        assertEquals("request", ca.updateCart());
        assertTrue(ca.getActionMessages().isEmpty());
        ca.setSubmit("cart.update");
        assertEquals(Action.SUCCESS, ca.updateCart());
        assertFalse(ca.getActionMessages().isEmpty());
        assertEquals(1, ca.getActionMessages().size());

        ca = getSessionCartAction();
        try {
            ca.getObject().setStatus(RequestStatus.DENIED);
            ca.updateCart();
        } catch (Exception e) {
            assertEquals("Selected specimen request is not editable", e.getMessage());
        }

        ca = getSessionCartAction();
        ca.getObject().setStatus(RequestStatus.PENDING_REVISION);
        assertEquals("request", ca.updateCart());
        assertTrue(ca.getActionMessages().isEmpty());
        assertEquals(RequestStatus.PENDING_REVISION, ca.getObject().getStatus());

        ca = getSessionCartAction();
        ca.getObject().setStatus(RequestStatus.DRAFT);
        assertEquals("request", ca.updateCart());
        assertTrue(ca.getActionMessages().isEmpty());
        assertEquals(RequestStatus.DRAFT, ca.getObject().getStatus());

        ca = getSessionCartAction();
        try {
            ca.getObject().setStatus(RequestStatus.PENDING);
            ca.updateCart();
        } catch (Exception e) {
            assertEquals("Selected specimen request is not editable", e.getMessage());
        }

        ca = getSessionCartAction();
        ca.getObject().setStatus(RequestStatus.DRAFT);
        ca.setSubmit("specimenRequest.edit.saveDraft");
        TissueLocatorUser requestor = new TissueLocatorUser();
        requestor.setEmail("moooo");
        ca.getObject().setRequestor(requestor);
        assertEquals("success", ca.updateCart());
    }

    /**
     * test adding to aggregate cart.
     * @throws Exception on error
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testAddToAggregateCart() throws Exception {
        getTestAppSettingService().setSpecimenGrouping("EXTERNAL_ID_ASSIGNER");
        List<Class<? extends AbstractCartAction>> actionTypes =
            Arrays.asList(SessionCartAction.class, DbCartAction.class);
        for (Class<? extends AbstractCartAction> clazz : actionTypes) {
            AbstractCartAction ca = getCartAction(clazz);
            ca.prepare();
            ca.setAggregateSelections(new AggregateSpecimenRequestLineItem[2 + 2 + 1]);
            ca.getAggregateSelections()[0] = createUniqueLineItem("inst1", "No Criteria Specified", 1);
            ca.getAggregateSelections()[1] = createUniqueLineItem("inst1", "Birth Year: 1908", 1);
            ca.getAggregateSelections()[2] = createUniqueLineItem("inst1", "Birth Year: 1908, disease", 1);
            ca.getAggregateSelections()[2 + 1] = createUniqueLineItem("inst1", "Birth Year: 1908, disease", 1);
            ca.getAggregateSelections()[2 + 2] = createUniqueLineItem("inst1", "disease", 1);
            String result = ca.addToAggregateCart();
            assertEquals("aggregateCart", result);
            assertEquals(2 + 2, getSessionCart().getAggregateLineItems().size());
            assertFalse(ca.getActionMessages().isEmpty());
            assertEquals(1, ca.getActionMessages().size());
            ca.clearErrorsAndMessages();
            result = ca.addToAggregateCart();
            assertEquals("aggregateCart", result);
            assertEquals(2 + 2, getSessionCart().getAggregateLineItems().size());
            assertFalse(ca.getActionMessages().isEmpty());
            assertEquals(1, ca.getActionMessages().size());
            ca.getAggregateSelections()[0].setQuantity(0);
            result = ca.addToAggregateCart();
            assertEquals("aggregateCart", result);
        }
        getTestAppSettingService().setSpecimenGrouping(null);
    }

    /**
     * test removing from aggregate cart.
     * @throws Exception on error
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testRemoveFromAggregateCart() throws Exception {
        getTestAppSettingService().setSpecimenGrouping("EXTERNAL_ID_ASSIGNER");
        List<Class<? extends AbstractCartAction>> actionTypes =
            Arrays.asList(SessionCartAction.class, DbCartAction.class);
        for (Class<? extends AbstractCartAction> clazz : actionTypes) {
            AbstractCartAction ca = getCartAction(clazz);
            ca.prepare();
            ca.getObject().getAggregateLineItems().add(createUniqueLineItem("", "", 2));
            ca.getObject().getAggregateLineItems().add(createUniqueLineItem("", "No Criteria Specified", 2));
            ca.prepare();
            ca.setSelection(new HashSet<Long>(Arrays.asList(new Long[] {0L})));
            //check this

            String result = ca.removeFromSpecificAggregateCart();
            String result2 = ca.removeFromGeneralAggregateCart();
            assertEquals("aggregateCart", result);
            assertEquals("aggregateCart", result2);
            assertTrue(getSessionCart().getLineItems().isEmpty());
            assertFalse(ca.getActionMessages().isEmpty());
            assertEquals(2, ca.getActionMessages().size());

        }
        getTestAppSettingService().setSpecimenGrouping(null);
    }

    /**
     * test emptying the aggregate cart.
     * @throws Exception on error
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testEmptyAggregateCart() throws Exception {
        getTestAppSettingService().setSpecimenGrouping("EXTERNAL_ID_ASSIGNER");
        List<Class<? extends AbstractCartAction>> actionTypes =
            Arrays.asList(SessionCartAction.class, DbCartAction.class);
        for (Class<? extends AbstractCartAction> clazz : actionTypes) {
            AbstractCartAction ca = getCartAction(clazz);
            ca.prepare();
            ca.getObject().getAggregateLineItems().add(createUniqueLineItem("", "", 2));
            ca.prepare();
            String result = ca.emptyAggregateCart();
            assertEquals("aggregateCart", result);
            assertTrue(getSessionCart().getLineItems().isEmpty());
            assertFalse(ca.getActionMessages().isEmpty());
            assertEquals(1, ca.getActionMessages().size());
        }
        getTestAppSettingService().setSpecimenGrouping(null);
    }

    /**
     * test updating the aggregate cart.
     * @throws Exception on error
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testUpdateAggregateCart() throws Exception {
        getTestAppSettingService().setSpecimenGrouping("EXTERNAL_ID_ASSIGNER");
        List<Class<? extends AbstractCartAction>> actionTypes =
            Arrays.asList(SessionCartAction.class, DbCartAction.class);
        for (Class<? extends AbstractCartAction> clazz : actionTypes) {
            AbstractCartAction ca = getCartAction(clazz);
            ca.setSubmit("cart.update");
            String result = ca.updateAggregateCart();
            assertEquals(Action.SUCCESS, result);
            assertFalse(ca.getActionMessages().isEmpty());
            assertEquals(1, ca.getActionMessages().size());
        }
        getTestAppSettingService().setSpecimenGrouping(null);
    }

    /**
     * test preparing the aggregate selections for population with request params.
     * @throws Exception on error
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testPrepareAggregateSelections() throws Exception {
        getTestAppSettingService().setSpecimenGrouping("EXTERNAL_ID_ASSIGNER");
        List<Class<? extends AbstractCartAction>> actionTypes =
            Arrays.asList(SessionCartAction.class, DbCartAction.class);
        for (Class<? extends AbstractCartAction> clazz : actionTypes) {
            getRequest().addParameter("otherParam", "1");
            getRequest().addParameter("aggregateSelections[1].institution", "1");
            getRequest().addParameter("aggregateSelections[3].institution", "1");
            getRequest().addParameter("aggregateSelections[0].institution", "1");
            getRequest().addParameter("aggregateSelections[2].institution", "1");
            AbstractCartAction ca = getCartAction(clazz);
            ca.setAggregateSelections(null);
            ca.prepare();
            assertNotNull(ca.getAggregateSelections());
            assertEquals(2 + 2, ca.getAggregateSelections().length);
            for (int i = 0; i < ca.getAggregateSelections().length; i++) {
                assertNotNull(ca.getAggregateSelections()[i]);
            }
        }
        getTestAppSettingService().setSpecimenGrouping(null);
    }

    /**
     * Tests splitting up the aggregate cart into separate arrays.
     */
    @Test
    public void testGenerateSeparateArrays() {
        AbstractCartAction ca = getSessionCartAction();
        ca.prepare();
        AggregateSpecimenRequestLineItem[] generalLineItems =
            createGeneralLineItems();
        AggregateSpecimenRequestLineItem[] specificLineItems =
            createSpecificLineItems();

        AggregateSpecimenRequestLineItem[] allLineItems =
            new AggregateSpecimenRequestLineItem[specificLineItems.length + generalLineItems.length];
        System.arraycopy(specificLineItems, 0, allLineItems, 0, specificLineItems.length);
        System.arraycopy(generalLineItems, 0, allLineItems, specificLineItems.length, generalLineItems.length);
        ca.setAggregateSelections(allLineItems);
        ca.addToAggregateCart();
        verifyLineItems(specificLineItems, ca.getSpecificLineItemsArray(), generalLineItems,
                ca.getGeneralLineItemsArray());
        verifyLineItems(specificLineItems, ca.getSpecificLineItems(), generalLineItems, ca.getGeneralLineItems());
    }

    /**
     * test dynamic extension categorization decider for the user action.
     * @throws Exception on error
     */
    @Test
    public void testDecider() throws Exception  {
        AbstractCartAction ca = getSessionCartAction();
        UiDynamicFieldCategory category = new UiDynamicFieldCategory();
        category.setCategoryName(SpecimenRequestHeaderKey.CURRENTLY_FUNDED.getResourceKey());
        assertFalse(ca.getUiDynamicFieldCategoryDecider().decide(category));
        assertTrue(ca.getUiDynamicFieldCategoryDecider().decide(new UiDynamicFieldCategory()));
    }

    private DbCartAction getDbCartAction() {
        return new DbCartAction(getTestAppSettingService(), getTestUiDynamicFieldCategoryService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
    }

    private SessionCartAction getSessionCartAction() {
        return new SessionCartAction(getTestAppSettingService(), getTestUiDynamicFieldCategoryService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
    }

    private <T extends AbstractCartAction> AbstractCartAction getCartAction(Class<T> clazz) {
        if (DbCartAction.class.equals(clazz)) {
            return getDbCartAction();
        } else if (SessionCartAction.class.equals(clazz)) {
            return getSessionCartAction();
        }
        return null;
    }

    private DbSpecimenRequestAction getDbSpecimenRequestAction() {
        return new DbSpecimenRequestAction(getTestUserService(), getTestAppSettingService(),
                getTestUiDynamicFieldCategoryService(), getTestLobHolderService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
    }

    private SessionSpecimenRequestAction getSessionSpecimenRequestAction() {
        return new SessionSpecimenRequestAction(getTestUserService(), getTestAppSettingService(),
                getTestUiDynamicFieldCategoryService(), getTestLobHolderService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
    }

    private LobHolderServiceLocal getTestLobHolderService() {
        LobHolderServiceLocal lobServiceMock = mock(LobHolderServiceLocal.class);
        LobHolder lob = new LobHolder(new byte[] {1});
        lob.setId(1L);
        stub(lobServiceMock.getLob(anyLong())).toReturn(lob);
        return lobServiceMock;
    }

    private <T extends AbstractSpecimenRequestAction> AbstractSpecimenRequestAction
        getSpecimenRequestAction(Class<T> clazz) {
        if (DbSpecimenRequestAction.class.equals(clazz)) {
            return getDbSpecimenRequestAction();
        } else if (SessionSpecimenRequestAction.class.equals(clazz)) {
            return getSessionSpecimenRequestAction();
        }
        return null;
    }
}
