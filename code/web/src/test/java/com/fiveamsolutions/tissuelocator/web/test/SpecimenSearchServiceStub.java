/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fiveamsolutions.nci.commons.data.search.GroupByCriteria;
import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.GroupableSearchCriteria;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.service.search.SpecimenSearchServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.TissueLocatorAnnotatedBeanSearchCriteria;

/**
 * @author bhumphrey
 *
 */
public class SpecimenSearchServiceStub implements SpecimenSearchServiceLocal {

    private boolean searchCalled;
    private int paginatedResultsAvailable = 0;
    private long aggregateResultsCount = 1L;

    /**
     * {@inheritDoc}
     */
    public Map<Institution, Long> aggregate(GroupableSearchCriteria<Institution, Specimen> criteria,
            PageSortParams<Specimen> pageSortParams, List<? extends GroupByCriteria<Specimen>> groupByCriterias) {
        search(criteria);
        Map<Institution, Long> results =  new HashMap<Institution, Long>();
        Institution b = new Institution();
        b.setName("b");
        results.put(b, aggregateResultsCount);
        Institution a = new Institution();
        a.setName("a");
        results.put(a, aggregateResultsCount);
        searchCalled = true;
        return results;
    }

    /**
     * {@inheritDoc}
     */
    public List<Specimen> search(SearchCriteria<Specimen> criteria, PageSortParams<Specimen> params) {
        TissueLocatorAnnotatedBeanSearchCriteria<Specimen> crit =
            (TissueLocatorAnnotatedBeanSearchCriteria<Specimen>) criteria;
        List<Specimen> results = new ArrayList<Specimen>();
        // if pagination is being tested
        if (getPaginatedResultsAvailable() > 0) {
            if (getPaginatedResultsAvailable() > params.getIndex()) {
                crit.getCriteria().setId(1L);
                results.add(crit.getCriteria());
            }
        } else {
            crit.getCriteria().setId(1L);
            results.add(crit.getCriteria());
        }
        searchCalled = true;
        return results;
    }

    /**
     * {@inheritDoc}
     */
    public List<Specimen> search(SearchCriteria<Specimen> criteria, PageSortParams<Specimen> pageSortParams,
            List<? extends GroupByCriteria<Specimen>> groupByCriterias) {
        TissueLocatorAnnotatedBeanSearchCriteria<Specimen> crit =
            (TissueLocatorAnnotatedBeanSearchCriteria<Specimen>) criteria;
        List<Specimen> results = new ArrayList<Specimen>();
        crit.getCriteria().setId(1L);
        results.add(crit.getCriteria());
        searchCalled = true;
        return results;

    }

    /**
     * @return whether search has been called
     */
    public boolean isSearchCalled() {
        boolean oldSearchCalled = searchCalled;
        searchCalled = false;
        return oldSearchCalled;
    }

    /**
     * {@inheritDoc}
     */
    public int count(SearchCriteria<Specimen> criteria) {
        searchCalled = true;
        return paginatedResultsAvailable;
    }

    /**
     * {@inheritDoc}
     */
    public List<Specimen> search(SearchCriteria<Specimen> criteria) {
        return search(criteria, null);
    }

    /**
     * @return the paginatedResultsAvailable
     */
    public int getPaginatedResultsAvailable() {
        return paginatedResultsAvailable;
    }

    /**
     * @param paginatedResultsAvailable the paginatedResultsAvailable to set
     */
    public void setPaginatedResultsAvailable(int paginatedResultsAvailable) {
        this.paginatedResultsAvailable = paginatedResultsAvailable;
    }

    /**
     * @param aggregateResultsCount The aggregate result count to be returned from searches.
     */
    public void setAggregateResultsCount(long aggregateResultsCount) {
        this.aggregateResultsCount = aggregateResultsCount;
    }

}
