/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.struts2.ServletActionContext;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.CheckboxAutocompleteCompositeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.RangeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.RangeSelectCompositeConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectDynamicExtensionConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.TextConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.TextSelectCompositeConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorRequestHelper;
import com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield.AbstractSearchFieldWidgetAction;
//CHECKSTYLE:OFF - line length
import com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield.CheckboxAutocompleteCompositeSearchFieldWidgetAction;
//CHECKSTYLE:ON
import com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield.RangeSearchFieldWidgetAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield.RangeSelectCompositeSearchFieldWidgetAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield.SelectDynamicExtensionSearchFieldWidgetAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield.SelectSearchFieldWidgetAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield.TextSearchFieldWidgetAction;
import com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield.TextSelectCompositeSearchFieldWidgetAction;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.InstitutionServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author gvaughn
 *
 */
public class SearchFieldWidgetActionTest extends AbstractTissueLocatorWebTest {

 // CHECKSTYLE:OFF - method length
    /**
     * Test the prepare method.
     */
    @Test
    public void testPrepare() {
        SearchFieldConfigServiceLocal service = getTestSearchFieldConfigService();
        Collection<AbstractSearchFieldConfig> configs = service.getSearchFieldConfigs(SearchSetType.ADVANCED);
        TextSearchFieldWidgetAction tAction = new TextSearchFieldWidgetAction(getTestSearchFieldConfigService());
        TextConfig tConfig = null;
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            if (searchFieldConfig.getClass().equals(TextConfig.class)) {
                tConfig = (TextConfig) searchFieldConfig;
                break;
            }
        }
        tAction.setSearchFieldConfig(tConfig);
        tAction.prepare();
        assertTrue(EqualsBuilder.reflectionEquals(tAction.getSearchFieldConfig(), 
                tConfig, new String[]{"fieldConfig"}));
        assertTrue(EqualsBuilder.reflectionEquals(tAction.getSearchFieldConfig().getFieldConfig(), 
                tConfig.getFieldConfig()));
        tAction.setSearchFieldConfig(null);
        tAction.prepare();
        assertNull(tAction.getSearchFieldConfig());
        tAction.setSearchFieldConfig(new TextConfig());
        tAction.prepare();
        assertNull(tAction.getSearchFieldConfig().getId());
        tAction = new TextSearchFieldWidgetAction(getTestSearchFieldConfigService());
        tAction.setSearchFieldConfig(tConfig);
        tAction.setSearchSetType(null);
        tAction.prepare();
        assertTrue(EqualsBuilder.reflectionEquals(tAction.getSearchFieldConfig(), 
                tConfig, new String[]{"fieldConfig"}));
        assertTrue(EqualsBuilder.reflectionEquals(tAction.getSearchFieldConfig().getFieldConfig(), 
                tConfig.getFieldConfig()));
        tAction = new TextSearchFieldWidgetAction(getTestSearchFieldConfigService());
        tAction.setSearchFieldConfig(tConfig);
        tAction.setSearchSetType(SearchSetType.SIMPLE);
        tAction.prepare();
        assertTrue(EqualsBuilder.reflectionEquals(tAction.getSearchFieldConfig(), tConfig, 
                new String[]{"fieldConfig"}));
        assertTrue(EqualsBuilder.reflectionEquals(tAction.getSearchFieldConfig().getFieldConfig(), tConfig.getFieldConfig()));

        RangeSearchFieldWidgetAction rAction = new RangeSearchFieldWidgetAction(getTestSearchFieldConfigService());
        RangeConfig rConfig = null;
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            if (searchFieldConfig.getClass().equals(RangeConfig.class)) {
                rConfig = (RangeConfig) searchFieldConfig;
                break;
            }
        }
        rAction.setSearchFieldConfig(rConfig);
        rAction.prepare();
        assertTrue(EqualsBuilder.reflectionEquals(rAction.getSearchFieldConfig(), rConfig, 
                new String[]{"fieldConfig"}));
        assertTrue(EqualsBuilder.reflectionEquals(rAction.getSearchFieldConfig().getFieldConfig(), rConfig.getFieldConfig()));

        TissueLocatorRequestHelper.setSearchConfigValueMap(ServletActionContext.getRequest(),
                new HashMap<AbstractSearchFieldConfig, Object>());
        SelectSearchFieldWidgetAction sAction = new SelectSearchFieldWidgetAction(getTestSearchFieldConfigService());
        SelectConfig sConfig = null;
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            if (searchFieldConfig.getClass().equals(SelectConfig.class) && searchFieldConfig.getSearchFieldDisplayName().equals("Institution")) {
                sConfig = (SelectConfig) searchFieldConfig;
                break;
            }
        }
        InstitutionServiceStub institutionStub = (InstitutionServiceStub) 
            TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        List<Institution> consortiumMembers = new ArrayList<Institution>();
        consortiumMembers.add(new Institution());
        consortiumMembers.get(0).setId(1L);
        institutionStub.setConsortiumMembers(consortiumMembers);
        sAction.setSearchFieldConfig(sConfig);
        sAction.prepare();
        assertTrue(EqualsBuilder.reflectionEquals(sAction.getSearchFieldConfig(), sConfig, 
                new String[]{"fieldConfig"}));
        assertTrue(EqualsBuilder.reflectionEquals(sAction.getSearchFieldConfig().getFieldConfig(), sConfig.getFieldConfig()));
        if (sConfig.isShowEmptyOption() && !sConfig.isMultiSelect()) {
            assertEquals(sAction.getSelectOptions().size(), 
                    TissueLocatorRegistry.getServiceLocator().getInstitutionService().getConsortiumMembers().size() + 1);
        } else {
            assertEquals(sAction.getSelectOptions().size(), 1);
        }
        assertNull(sAction.getValue());
        
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            if (searchFieldConfig.getClass().equals(SelectConfig.class) && searchFieldConfig.getSearchFieldDisplayName().equals("Gender")) {
                sConfig = (SelectConfig) searchFieldConfig;
                break;
            }
        }
        sAction.setSearchFieldConfig(sConfig);
        sAction.prepare();
        assertTrue(EqualsBuilder.reflectionEquals(sAction.getSearchFieldConfig(), sConfig, 
                new String[]{"fieldConfig"}));
        assertTrue(EqualsBuilder.reflectionEquals(sAction.getSearchFieldConfig().getFieldConfig(), sConfig.getFieldConfig()));
        assertEquals(sAction.getSelectOptions().size(), Gender.values().length);
        
        assertNull(sAction.getValue());
        
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            if (searchFieldConfig.getClass().equals(SelectConfig.class) && searchFieldConfig.getSearchFieldDisplayName().equals("Specimen Type")) {
                sConfig = (SelectConfig) searchFieldConfig;
                break;
            }
        }
        sAction.setSearchFieldConfig(sConfig);
        sAction.prepare();
        assertTrue(EqualsBuilder.reflectionEquals(sAction.getSearchFieldConfig(), sConfig, 
                new String[]{"fieldConfig"}));
        assertTrue(EqualsBuilder.reflectionEquals(sAction.getSearchFieldConfig().getFieldConfig(), sConfig.getFieldConfig()));
        if (sConfig.isShowEmptyOption() && !sConfig.isMultiSelect()) {
            assertEquals(sAction.getSelectOptions().size(), 2);
        } else {
            assertEquals(sAction.getSelectOptions().size(), 1);
        }
        assertNull(sAction.getValue());

        TextSelectCompositeSearchFieldWidgetAction tscAction = new TextSelectCompositeSearchFieldWidgetAction(getTestSearchFieldConfigService());
        TextSelectCompositeConfig tscConfig = null;
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            if (searchFieldConfig.getClass().equals(TextSelectCompositeConfig.class)) {
                tscConfig = (TextSelectCompositeConfig) searchFieldConfig;
                break;
            }
        }
        tscAction.setSearchFieldConfig(tscConfig);
        tscAction.prepare();
        assertEquals(tscAction.getSearchFieldConfig().getId(), tscConfig.getId());
        assertEquals(tscAction.getSelectOptions().size(), 
               QuantityUnits.values().length  + 1);
        assertNull(tscAction.getValue());

        RangeSelectCompositeSearchFieldWidgetAction rscAction = new RangeSelectCompositeSearchFieldWidgetAction(getTestSearchFieldConfigService());
        RangeSelectCompositeConfig rscConfig = null;
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            if (searchFieldConfig.getClass().equals(RangeSelectCompositeConfig.class)) {
                rscConfig = (RangeSelectCompositeConfig) searchFieldConfig;
                break;
            }
        }
        rscConfig.setId(null);
        rscAction.setSearchFieldConfig(rscConfig);
        rscAction.prepare();
        assertNull(rscAction.getValue());
        rscConfig.getSelectConfig().setMultiSelect(false);
        rscConfig.getSelectConfig().setShowEmptyOption(true);
        assertEquals(TimeUnits.values().length + 1, rscAction.getSelectOptions().size());
        rscConfig.getSelectConfig().setMultiSelect(true);
        rscConfig.getSelectConfig().setShowEmptyOption(true);
        rscAction.setSearchFieldConfig(rscConfig);
        rscAction.prepare();
        assertEquals(TimeUnits.values().length, rscAction.getSelectOptions().size());
        rscConfig.getSelectConfig().setMultiSelect(true);
        rscConfig.getSelectConfig().setShowEmptyOption(false);
        rscAction.prepare();
        assertEquals(TimeUnits.values().length, rscAction.getSelectOptions().size());
        rscConfig.getSelectConfig().setMultiSelect(false);
        rscConfig.getSelectConfig().setShowEmptyOption(false);
        rscAction.prepare();
        assertEquals(TimeUnits.values().length, rscAction.getSelectOptions().size());

        CheckboxAutocompleteCompositeSearchFieldWidgetAction cacAction
            = new CheckboxAutocompleteCompositeSearchFieldWidgetAction(getTestSearchFieldConfigService());
        CheckboxAutocompleteCompositeConfig cacConfig = null;
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            if (searchFieldConfig.getClass().equals(CheckboxAutocompleteCompositeConfig.class)) {
                cacConfig = (CheckboxAutocompleteCompositeConfig) searchFieldConfig;
                break;
            }
        }
        cacAction.setSearchFieldConfig(cacConfig);
        cacAction.prepare();
        assertEquals(cacConfig.getId(), cacAction.getSearchFieldConfig().getId());
        assertTrue(CheckboxAutocompleteCompositeSearchFieldWidgetAction.getMaxResultsCount() > 0);

        SelectDynamicExtensionSearchFieldWidgetAction sdeAction
            = new SelectDynamicExtensionSearchFieldWidgetAction(getTestSearchFieldConfigService());
        SelectDynamicExtensionConfig sdeConfig = null;
        for (AbstractSearchFieldConfig searchFieldConfig : configs) {
            if (searchFieldConfig.getClass().equals(SelectDynamicExtensionConfig.class)) {
                sdeConfig = (SelectDynamicExtensionConfig) searchFieldConfig;
                break;
            }
        }
        sdeConfig.getControlledVocabConfig().getOptions().add("testOption");
        sdeAction.setSearchFieldConfig(sdeConfig);
        sdeAction.prepare();
        assertEquals(sdeAction.getSearchFieldConfig().getId(), sdeConfig.getId());
        assertEquals(sdeAction.getSelectOptions().size(), 1);
        assertEquals("search.emptyOption", sdeAction.getSelectOptions().get(0).getLabel());
        assertEquals(" ", sdeAction.getSelectOptions().get(0).getValue());
        assertNull(sdeAction.getValue());

        sdeConfig.setShowEmptyOption(false);
        sdeAction.setSearchFieldConfig(sdeConfig);
        assertEquals(sdeAction.getSelectOptions().size(), 1);

        sdeConfig.setMultiSelect(true);
        assertEquals(sdeAction.getSelectOptions().size(), 1);

        sdeConfig.setShowEmptyOption(true);
        assertEquals(sdeAction.getSelectOptions().size(), 1);
    }
 // CHECKSTYLE:ON
    /**
     * Test the getWidget() and input() methods.
     * @throws IllegalAccessException on error.
     * @throws InstantiationException on error.
     * @throws NoSuchMethodException on error.
     * @throws InvocationTargetException on error.
     */
    @Test
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void testCommonMethods() throws InstantiationException, IllegalAccessException, 
        InvocationTargetException, NoSuchMethodException {
        List<Class<? extends AbstractSearchFieldWidgetAction<? extends AbstractSearchFieldConfig>>> widgetActions = 
            Arrays.asList(TextSearchFieldWidgetAction.class, SelectSearchFieldWidgetAction.class,
                RangeSearchFieldWidgetAction.class, TextSelectCompositeSearchFieldWidgetAction.class,
                RangeSelectCompositeSearchFieldWidgetAction.class,
                CheckboxAutocompleteCompositeSearchFieldWidgetAction.class,
                SelectDynamicExtensionSearchFieldWidgetAction.class);
        for (Class actionClass : widgetActions) {
            AbstractSearchFieldWidgetAction<AbstractSearchFieldConfig> action =
                    (AbstractSearchFieldWidgetAction<AbstractSearchFieldConfig>) 
                    actionClass.getConstructor(SearchFieldConfigServiceLocal.class)
                        .newInstance(getTestSearchFieldConfigService());
            assertEquals(action.getWidget(), Action.SUCCESS);
            assertEquals(action.input(), Action.INPUT);
        }
        for (Class actionClass : widgetActions) {
            try {
                actionClass.newInstance();
                fail();
            } catch (UnsupportedOperationException e) {
                // expected
            }
            
        }
    }

    /**
     * Test the getValue() method.
     */
    @Test
    public void testGetValue() {
        SelectSearchFieldWidgetAction action = new SelectSearchFieldWidgetAction(getTestSearchFieldConfigService());
        Specimen specimen = new Specimen();
        specimen.setId(1L);
        assertEquals(action.getValue("id", specimen), 1L);
    }

    /**
     * Test the convert value method.
     */
    @Test
    public void testConvertValue() {
        SelectSearchFieldWidgetAction action = new SelectSearchFieldWidgetAction(getTestSearchFieldConfigService());

        assertEquals("test", action.convertValueIfNeeded("test"));
        HashSet<Gender> genders = new HashSet<Gender>();
        genders.add(Gender.MALE);
        genders.add(Gender.FEMALE);
        Object val = action.convertValueIfNeeded(genders);
        assertTrue(val instanceof List);
        @SuppressWarnings("unchecked")
        List<String> vals = (List<String>) val;
        assertEquals(2, vals.size());
        assertTrue(vals.contains(Gender.MALE.toString()));
        assertTrue(vals.contains(Gender.FEMALE.toString()));
        SpecimenType type = new SpecimenType();
        type.setId(1L);
        assertEquals(1L, action.convertValueIfNeeded(type));
    }
}
