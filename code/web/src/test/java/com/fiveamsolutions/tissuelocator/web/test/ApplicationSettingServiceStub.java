/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.test;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import com.fiveamsolutions.tissuelocator.data.ApplicationSetting;
import com.fiveamsolutions.tissuelocator.data.FundingStatus;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.web.util.HomePageWidget;

/**
 * @author ddasgupta
 *
 */
public class ApplicationSettingServiceStub extends GenericServiceStub<ApplicationSetting>
    implements ApplicationSettingServiceLocal {

    private boolean displayConsortiumRegistration = true;
    private boolean votingTaskEnabled = true;
    private String specimenGrouping = "";
    private static final String GENERAL_POPULATION_SORT_REGEX = "(No Criteria Specified)|(Birth Year)[^;]*";
    private boolean mtaCertificationRequired;
    private int minimumAggregateResultsDisplayed = 0;
    private ReviewProcess reviewProcess = ReviewProcess.FULL_CONSORTIUM_REVIEW;

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNumberOfReviewers() {
        return 2;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date getScheduledReminderThreadStartTime() {
        return DateUtils.addHours(new Date(), 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getShipmentReceiptGracePeriod() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getLineItemReviewGracePeriod() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSupportLetterRequestReviewGracePeriod() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getQuestionResponseGracePeriod() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayPiLegalFields() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayProspectiveCollection() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayConsortiumRegistration() {
        return displayConsortiumRegistration;
    }

    /**
     * Set whether consortium members should be displayed
     * for user registration.
     * @param displayConsortiumRegistration are consortium members displayed.
     */
    public void setDisplayConsortiumRegistration(
        boolean displayConsortiumRegistration) {
        this.displayConsortiumRegistration = displayConsortiumRegistration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSimpleSearchEnabled() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTabbedSearchAndSectionedCategoriesViewsEnabled() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLeftBrowseWidget() {
        return HomePageWidget.TYPE.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCenterBrowseWidget() {
        return HomePageWidget.DISEASE_WITH_COUNT.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRightBrowseWidget() {
        return HomePageWidget.WELCOME.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLeftHomeWidget() {
        return HomePageWidget.TYPE.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCenterHomeWidget() {
        return HomePageWidget.DISEASE_WITH_COUNT.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRightHomeWidget() {
        return HomePageWidget.SIMPLE_SEARCH.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVotingTaskEnabled() {
        return votingTaskEnabled;
    }

    /**
     * Sets whether the voting task is enabled.
     * @param votingTaskEnabled whether the voting task is enabled.
     */
    public void setVotingTaskEnabled(boolean votingTaskEnabled) {
        this.votingTaskEnabled = votingTaskEnabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenGrouping() {
        return specimenGrouping;
    }

    /**
     * used for testing.
     * @param str the grouping
     */
    public void setSpecimenGrouping(String str) {
        specimenGrouping = str;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getGeneralPopulationSortRegex() {
        return GENERAL_POPULATION_SORT_REGEX;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayRequestReviewVotes() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReviewProcess getReviewProcess() {
        return reviewProcess;
    }

    /**
     * @param reviewProcess the reviewProcess to set
     */
    public void setReviewProcess(ReviewProcess reviewProcess) {
        this.reviewProcess = reviewProcess;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAccountApprovalActive() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMtaCertificationRequired() {
        return mtaCertificationRequired;
    }

    /**
     * @param mtaCertificationRequired the mtaCertificationRequired to set
     */
    public void setMtaCertificationRequired(boolean mtaCertificationRequired) {
        this.mtaCertificationRequired = mtaCertificationRequired;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getNewRequestCountCondition() {
        return "1 = 1";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FundingStatus getMinimunRequiredFundingStatus() {
        return FundingStatus.NOT_FUNDED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isIrbApprovalRequired() {
        return false;
    }

    /**
     * @param minimumAggregateResultsDisplayed the minimum aggregate results displayed.
     */
    public void setMinimumAggregateResultsDisplayed(int minimumAggregateResultsDisplayed) {
        this.minimumAggregateResultsDisplayed = minimumAggregateResultsDisplayed;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getMinimumAggregateResultsDisplayed() {
        return minimumAggregateResultsDisplayed;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn1Label() {
        return "Anatomic Source";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn1Value() {
        return "customProperty(anatomicSource)";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn2Label() {
        return "Type";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn2Value() {
        return "specimenType";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn3Label() {
        return "Preservation Type";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn3Value() {
        return "customProperty(preservationType)";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn4Label() {
        return "Available Quantity";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecimenDetailsPanelColumn4Value() {
        return "availableQuantity";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayCollectionProtocol() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayUsageRestrictions() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isProtocolDocumentRequired() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getHomePageDataCacheRefreshInterval() {
        return 2;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayRequestProcessSteps() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayFundingStatusPending() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDisplayShipmentBillingRecipient() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUserResumeRequired() {
        return true;
    }
}
