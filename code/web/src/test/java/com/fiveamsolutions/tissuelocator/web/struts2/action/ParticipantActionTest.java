/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.apache.struts2.ServletActionContext;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Role;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.ParticipantServiceStub;
import com.fiveamsolutions.tissuelocator.web.util.SelectOption;
import com.opensymphony.xwork2.Action;

/**
 * @author smiller
 *
 */
public class ParticipantActionTest extends AbstractTissueLocatorWebTest {

    /**
     * Tests the save method.
     */
    @Test
    public void testSave() {
        ParticipantAction action = new ParticipantAction();
        Participant participant = new Participant();
        participant.setExternalId("test");
        action.setObject(participant);
        action.prepare();
        action.save();
        ParticipantServiceStub stub = (ParticipantServiceStub)
            TissueLocatorRegistry.getServiceLocator().getParticipantService();
        assertEquals(action.getObject(), stub.getObject());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
    }

    /**
     * Tests the list method.
     */
    @Test
    public void testlist() {
        ParticipantAction action = new ParticipantAction();
        action.prepare();
        action.setInstitutionSelection(null);
        assertNull(action.getInstitutionSelection());
        assertEquals(Action.SUCCESS, action.list());
        assertEquals(0, action.getObjects().getList().size());
    }

    /**
     * Tests the input method.
     */
    @Test
    public void testInput() {
        ParticipantAction action = new ParticipantAction();
        action.prepare();
        action.input();
        assertEquals(null, action.getInstitutionSelection());

        action.setObject(new Participant());
        action.prepare();
        action.input();
        assertEquals(null, action.getInstitutionSelection());

        action.getObject().setExternalIdAssigner(new Institution());
        action.prepare();
        action.input();
        assertEquals(null, action.getInstitutionSelection());

        action.getObject().getExternalIdAssigner().setName("test");
        action.prepare();
        action.input();
        assertEquals("test", action.getInstitutionSelection());

        action.getObject().setId(1L);
        action.prepare();
        action.input();
        assertFalse(action.getSpecimens().getList().isEmpty());
        for (Specimen specimen : action.getSpecimens().getList()) {
            assertEquals(specimen.getParticipant().getId(), action.getObject().getId());
        }

        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        request.addParameter("specimenReturned", "true");
        action.prepare();
        action.input();
        assertEquals(1, action.getActionMessages().size());

    }

    /**
     * Tests the list method.
     */
    @Test
    public void testInstitutions() {
        ParticipantAction action = new ParticipantAction();
        action.prepare();
        assertEquals(Action.SUCCESS, action.autocompleteInstitution());
        action.setTerm(null);
        assertEquals(0, action.getInstitutionsMap().length);
        action.setTerm("test");
        assertEquals(1, action.getInstitutionsMap().length);
        SelectOption result = action.getInstitutionsMap()[0];
        assertNotNull(result.getId());
        assertNotNull(result.getLabel());
        assertNotNull(result.getValue());
    }

    /**
     * Tests the hasMoreDetails method.
     */
    @Test
    public void testsHasMoreDetails() {
        MockHttpServletRequest request = (MockHttpServletRequest) ServletActionContext.getRequest();
        request.setRemoteUser("testadmin");
        ParticipantAction action = new ParticipantAction();
        Specimen specimen = new Specimen();
        assertFalse(action.hasMoreDetails(specimen));

        request.addUserRole(Role.TISSUE_REQUEST_ADMIN.getName());
        assertFalse(action.hasMoreDetails(specimen));
        specimen.setExternalId("test");
        assertTrue(action.hasMoreDetails(specimen));
        specimen.setExternalId(null);

        request.addUserRole(Role.SHIPMENT_ADMIN.getName());
        assertFalse(action.hasMoreDetails(specimen));
        specimen.setId(1L);
        assertTrue(action.hasMoreDetails(specimen));
        specimen.setId(null);
    }

    /**
     * Tests the withdraw consent method.
     */
    @Test
    public void testWithdrawConsent() {
        ParticipantAction action = new ParticipantAction();
        action.setObject(new Participant());
        action.getObject().setId(1L);
        action.prepare();
        action.input();
        assertNotNull(action.getSpecimens());
        PaginatedList<Specimen> specimens = action.getSpecimens();
        assertEquals("input", action.withdrawConsent());
        assertNotSame(specimens, action.getSpecimens());
    }
}
