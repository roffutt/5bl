/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.interceptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;

/**
 * @author gvaughn
 *
 */
public class TissueLocatorCheckboxInterceptorTest {

    private static final String UNCHECKED_VALUE = Boolean.FALSE.toString();
    private static final String CHECKBOX_PREFIX = "__checkbox_";
    
    /**
     * Test the intercept method with no params excluded.
     * @throws Exception on error.
     */
    @Test
    public void testInterceptNoExclusions() throws Exception {
        TissueLocatorCheckboxInterceptor interceptor = new TissueLocatorCheckboxInterceptor();
        ActionInvocation mockInvocation = mock(ActionInvocation.class);
        ActionContext mockContext = mock(ActionContext.class);
        Map<String, Object> parameters = new HashMap<String, Object>();
        Map<String, Object> expectedParameters = new HashMap<String, Object>();
        when(mockInvocation.getInvocationContext()).thenReturn(mockContext);
        when(mockInvocation.invoke()).thenReturn("invoke");
        when(mockContext.getParameters()).thenReturn(parameters);
        
        // empty parameters
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
        
        // non-checkbox parameter
        parameters.put("field", new String[]{"value"});
        expectedParameters.put("field", new String[]{"value"});
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
        
        // null chekcbox parameter, should be removed & ignored
        parameters.put(CHECKBOX_PREFIX + "field", null);
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
        
        // unexpected value type, should be removed & ignored
        parameters.put(CHECKBOX_PREFIX + "field", new Object());
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
        
        // multiple values, should be removed and ignored
        parameters.put(CHECKBOX_PREFIX + "field", new String[]{"1", "2"});
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
        
        // a corresponding value is submitted, should be ignored
        parameters.put(CHECKBOX_PREFIX + "cb", new String[]{"true"});
        parameters.put("cb", new String[]{"true"});
        expectedParameters.put("cb", new String[]{"true"});
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
        
        // no corresponding value submitted, should be set to false
        parameters.put(CHECKBOX_PREFIX + "cb", new String[]{"true"});
        parameters.put(CHECKBOX_PREFIX + "cb2", new String[]{"true"});
        expectedParameters.put("cb2", new String[]{UNCHECKED_VALUE});
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
        
        // excluded param submitted, should be ignored
        parameters.put(CHECKBOX_PREFIX + "cb", new String[]{"true"});
        parameters.put(CHECKBOX_PREFIX + "cb2", new String[]{"true"});
        parameters.put(CHECKBOX_PREFIX + "cb3", new String[]{"1"});
        interceptor.setExcludeParams("cb3");
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
        
        // test multiple exclusions
        parameters.put(CHECKBOX_PREFIX + "cb", new String[]{"true"});
        parameters.put(CHECKBOX_PREFIX + "cb2", new String[]{"true"});
        parameters.put(CHECKBOX_PREFIX + "cb3", new String[]{"1"});
        parameters.put(CHECKBOX_PREFIX + "cb4", new String[]{"2"});
        interceptor.setExcludeParams(" cb3 , cb4 ");
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
        
        // test null exclusions
        interceptor.setExcludeParams(null);
        parameters.put(CHECKBOX_PREFIX + "cb", new String[]{"true"});
        parameters.put(CHECKBOX_PREFIX + "cb2", new String[]{"true"});
        parameters.put(CHECKBOX_PREFIX + "cb3", new String[]{"1"});
        parameters.put(CHECKBOX_PREFIX + "cb4", new String[]{"2"});
        expectedParameters.put("cb3", new String[]{UNCHECKED_VALUE});
        expectedParameters.put("cb4", new String[]{UNCHECKED_VALUE});
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
        
        // test empty exclusions
        interceptor.setExcludeParams(" ");
        parameters.put(CHECKBOX_PREFIX + "cb", new String[]{"true"});
        parameters.put(CHECKBOX_PREFIX + "cb2", new String[]{"true"});
        parameters.put(CHECKBOX_PREFIX + "cb3", new String[]{"1"});
        parameters.put(CHECKBOX_PREFIX + "cb4", new String[]{"2"});
        assertEquals("invoke", interceptor.intercept(mockInvocation));
        verifyExpectedParameters(parameters, expectedParameters);
    }
    
    private void verifyExpectedParameters(Map<String, Object> params, 
            Map<String, Object> expectedParams) {
        assertEquals(expectedParams.size(), params.size());
        for (Map.Entry<String, Object> expected : expectedParams.entrySet()) {
            assertTrue(EqualsBuilder.reflectionEquals(params.get(expected.getKey()), expected.getValue()));
        }
    }
}
