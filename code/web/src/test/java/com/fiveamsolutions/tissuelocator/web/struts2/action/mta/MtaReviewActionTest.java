/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.mta;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.test.AbstractTissueLocatorWebTest;
import com.fiveamsolutions.tissuelocator.web.test.InstitutionServiceStub;
import com.fiveamsolutions.tissuelocator.web.test.SignedMaterialTransferAgreementServiceStub;
import com.opensymphony.xwork2.Action;

/**
 * @author ddasgupta
 *
 */
public class MtaReviewActionTest extends AbstractTissueLocatorWebTest {

    /**
     * tests the prepare method.
     * @throws Exception on error
     */
    @Test
    public void testPrepare() throws Exception {
        InstitutionServiceStub stub = (InstitutionServiceStub)
            TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        MtaReviewAction action = new MtaReviewAction(getTestUserService());
        assertNull(action.getInstitutions());
        assertNotNull(action.getSignedMta());
        assertNull(action.getSignedMta().getId());
        Institution previous = action.getInstitution();
        assertNotNull(previous);
        action.prepare();
        assertNotNull(action.getInstitutions());
        assertTrue(stub.isSearchCalled());
        assertNotNull(action.getSignedMta());
        assertNull(action.getSignedMta().getId());
        assertNotNull(action.getInstitution());
        assertNotSame(previous, action.getInstitution());
        assertNull(action.getInstitution().getId());

        action = new MtaReviewAction(getTestUserService());
        assertNotNull(action.getSignedMta());
        assertNull(action.getSignedMta().getId());
        previous = action.getInstitution();
        assertNotNull(previous);
        action.getInstitution().setId(1L);
        action.getSignedMta().setId(1L);
        SignedMaterialTransferAgreement previousMta = action.getSignedMta();
        action.prepare();
        assertNotNull(action.getSignedMta());
        assertNotNull(action.getSignedMta().getId());
        assertNotSame(previousMta, action.getSignedMta());
        assertNotNull(action.getInstitution());
        assertNotSame(previous, action.getInstitution());
        assertNotNull(action.getInstitution().getId());
    }


    /**
     * tests the review method.
     */
    @Test
    public void testReview() {
        MtaReviewAction action = new MtaReviewAction(getTestUserService());
        SignedMaterialTransferAgreement mta = new SignedMaterialTransferAgreement();
        action.setSignedMta(mta);
        Institution i = new Institution();
        i.setMtaContact(new Person());
        i.getMtaContact().setId(1L);
        action.setInstitution(i);
        action.setCorrectInstitution(Boolean.TRUE);
        action.setReplaceExisting(Boolean.FALSE);
        action.setValid(Boolean.TRUE);
        assertEquals("reviewSuccess", action.review());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        SignedMaterialTransferAgreementServiceStub stub = (SignedMaterialTransferAgreementServiceStub)
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();
        assertEquals(mta, stub.getObject());
        assertEquals(i, stub.getInstitution());
        assertNotNull(i.getMtaContact());
        assertFalse(stub.isReplaceExisting());
        assertTrue(stub.isValid());
        assertTrue(action.getCorrectInstitution());
        action.clearErrorsAndMessages();

        action.setValid(Boolean.FALSE);
        assertEquals("reviewSuccess", action.review());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        assertEquals(mta, stub.getObject());
        assertEquals(i, stub.getInstitution());
        assertNotNull(i.getMtaContact());
        assertFalse(stub.isReplaceExisting());
        assertFalse(stub.isValid());
        assertTrue(action.getCorrectInstitution());
        action.clearErrorsAndMessages();

        action.setReplaceExisting(Boolean.TRUE);
        assertEquals("reviewSuccess", action.review());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        assertEquals(mta, stub.getObject());
        assertEquals(i, stub.getInstitution());
        assertNotNull(i.getMtaContact());
        assertTrue(stub.isReplaceExisting());
        assertFalse(stub.isValid());
        assertTrue(action.getCorrectInstitution());
        action.clearErrorsAndMessages();

        i.getMtaContact().setId(null);
        assertEquals("reviewSuccess", action.review());
        assertFalse(action.getActionMessages().isEmpty());
        assertEquals(1, action.getActionMessages().size());
        assertEquals(mta, stub.getObject());
        assertEquals(i, stub.getInstitution());
        assertNull(i.getMtaContact());
        assertTrue(stub.isReplaceExisting());
        assertFalse(stub.isValid());
        assertTrue(action.getCorrectInstitution());
        action.clearErrorsAndMessages();
    }

    /**
     * Tests the list method.
     */
    @Test
    public void testList() {
        MtaReviewAction action = new MtaReviewAction(getTestUserService());
        assertEquals(Action.SUCCESS, action.list());
        assertEquals(0, action.getObjects().getList().size());
        InstitutionServiceStub stub = (InstitutionServiceStub)
            TissueLocatorRegistry.getServiceLocator().getInstitutionService();
        assertTrue(stub.isSearchCalled());
    }
}
