/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.junit.Test;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.test.AbstractSpecimenRequestReviewActionTest;
import com.fiveamsolutions.tissuelocator.web.test.LineItemReviewServiceStub;

/**
 * @author gvaughn
 *
 */
public class LineItemReviewActionTest extends AbstractSpecimenRequestReviewActionTest {

    /**
     * Test the prepare method.
     */
    @Test
    public void testPrepare() {
        LineItemReviewAction action = getLineItemReviewAction();
        action.setObject(getTestRequest());
        Institution inst = new Institution();
        AggregateSpecimenRequestLineItem li = createUniqueLineItem("test", "criteria", 1);
        li.setInstitution(inst);
        Shipment s = new Shipment();
        s.getAggregateLineItems().add(li);
        action.getObject().getOrders().add(s);
        action.prepare();
        assertEquals(action.getComment().getUser().getUsername(), UsernameHolder.getUser());
        assertNull(action.getComment().getComment());
        assertEquals(1, action.getOrderSpecificLineItems().size());
        assertEquals(0, action.getOrderGeneralLineItems().size());

        action = getLineItemReviewAction();
        action.setObject(getTestRequest());
        action.getObject().getAggregateLineItems().clear();
        AggregateSpecimenRequestLineItem[] specificLineItems = createSpecificLineItems();
        AggregateSpecimenRequestLineItem[] generalLineItems = createGeneralLineItems();
        List<AggregateSpecimenRequestLineItem> lineItems = new ArrayList<AggregateSpecimenRequestLineItem>();
        lineItems.addAll(Arrays.asList(specificLineItems));
        lineItems.addAll(Arrays.asList(generalLineItems));
        for (AggregateSpecimenRequestLineItem item : lineItems) {
            item.setInstitution(inst);
        }
        s = new Shipment();
        s.getAggregateLineItems().addAll(lineItems);
        action.getObject().getOrders().add(s);
        action.prepare();
        verifyLineItems(specificLineItems, action.getOrderSpecificLineItems(),
                generalLineItems, action.getOrderGeneralLineItems());
        verifyLineItems(specificLineItems, action.getCombinedSpecificLineItems(),
                generalLineItems, action.getCombinedGeneralLineItems());
        verifyLineItems(specificLineItems, Arrays.asList(action.getCombinedSpecificLineItemsArray()),
                generalLineItems, Arrays.asList(action.getCombinedGeneralLineItemsArray()));
    }

    /**
     * Test separation of reviewed and un-reviewed line items in the prepare method.
     */
    @Test
    public void testPrepareLineItemSeparation() {
        LineItemReviewAction action = getLineItemReviewAction();
        action.setObject(getTestRequest());
        action.getObject().getAggregateLineItems().clear();
        AggregateSpecimenRequestLineItem[] specificLineItems = createSpecificLineItems();
        AggregateSpecimenRequestLineItem[] generalLineItems = createGeneralLineItems();
        List<AggregateSpecimenRequestLineItem> lineItems = new ArrayList<AggregateSpecimenRequestLineItem>();
        lineItems.addAll(Arrays.asList(specificLineItems));
        lineItems.addAll(Arrays.asList(generalLineItems));
        action.getObject().getAggregateLineItems().addAll(lineItems);
        action.prepare();
        verifyLineItems(specificLineItems, action.getUnreviewedSpecificLineItems(),
                generalLineItems, action.getUnreviewedGeneralLineItems());
        assertTrue(action.getReviewedSpecificLineItems().isEmpty());
        assertTrue(action.getReviewedGeneralLineItems().isEmpty());
        for (AggregateSpecimenRequestLineItem item : lineItems) {
            SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
            vote.setVote(Vote.APPROVE);
            item.setVote(vote);
        }
        action = getLineItemReviewAction();
        action.setObject(getTestRequest());
        action.getObject().getAggregateLineItems().clear();
        action.getObject().getAggregateLineItems().addAll(lineItems);
        action.prepare();
        verifyLineItems(specificLineItems, action.getReviewedSpecificLineItems(),
                generalLineItems, action.getReviewedGeneralLineItems());
        assertTrue(action.getUnreviewedSpecificLineItems().isEmpty());
        assertTrue(action.getUnreviewedGeneralLineItems().isEmpty());
    }

    /**
     * Test line item review voting.
     * @throws MessagingException on error.
     */
    @Test
    public void testSaveLineItemReview() throws MessagingException {
        LineItemReviewAction action = getLineItemReviewAction();
        action.setObject(getTestRequest());
        action.prepare();
        assertEquals("input", action.saveLineItemReview());
        assertEquals(1, action.getActionMessages().size());
        LineItemReviewServiceStub stub =
            (LineItemReviewServiceStub) TissueLocatorRegistry.getServiceLocator().getLineItemReviewService();
        assertEquals(action.getObject(), stub.getObject());
    }

    /**
     * Test comment saving.
     * @throws MessagingException on error.
     */
    @Test
    public void testAddComment() throws MessagingException {
        Institution userInstitution = new Institution();
        userInstitution.setId(1L);
        userInstitution.setName("inst");
        setUserInstitution(userInstitution);
        LineItemReviewAction action = getLineItemReviewAction();
        action.prepare();
        assertEquals("success", action.addComment());
        assertNotNull(action.getComment().getDate());
        assertEquals(1, action.getObject().getComments().size());
    }

    /**
     * Test validation.
     */
    @Test
    public void testValidate() {
        Institution userInstitution = new Institution();
        userInstitution.setId(1L);
        userInstitution.setName("inst");
        setUserInstitution(userInstitution);

        LineItemReviewAction action = getLineItemReviewAction();
        AggregateSpecimenRequestLineItem[] specificLineItems = createSpecificLineItems();
        AggregateSpecimenRequestLineItem[] generalLineItems = createGeneralLineItems();
        List<AggregateSpecimenRequestLineItem> lineItems = new ArrayList<AggregateSpecimenRequestLineItem>();
        lineItems.addAll(Arrays.asList(specificLineItems));
        lineItems.addAll(Arrays.asList(generalLineItems));
        for (AggregateSpecimenRequestLineItem item : lineItems) {
            item.setInstitution(userInstitution);
            item.setVote(new SpecimenRequestReviewVote());
            item.getVote().setInstitution(userInstitution);
            item.getVote().setDate(new Date());
        }
        action.getObject().getAggregateLineItems().addAll(lineItems);
        action.prepare();
        action.validate();
        assertTrue(action.getFieldErrors().isEmpty());
        action.clearErrorsAndMessages();
        action.setValidateRequest(true);
        action.validate();
        assertEquals(specificLineItems.length + generalLineItems.length, action.getFieldErrors().size());
        for (int i = 0; i < specificLineItems.length; i++) {
            String fieldName = "combinedSpecificLineItemsArray" + "[" + i + "].vote.vote";
            assertTrue(action.getFieldErrors().keySet().contains(fieldName));
        }
        for (int i = 0; i < generalLineItems.length; i++) {
            String fieldName = "combinedGeneralLineItemsArray" + "[" + i + "].vote.vote";
            assertTrue(action.getFieldErrors().keySet().contains(fieldName));
        }
        action.clearErrorsAndMessages();
        Institution alternate = new Institution();
        alternate.setId(2L);
        for (AggregateSpecimenRequestLineItem item : lineItems) {
            item.setInstitution(alternate);
            item.getVote().setInstitution(alternate);
        }
        action.validate();
        assertTrue(action.getFieldErrors().isEmpty());
        action.clearErrorsAndMessages();
        for (AggregateSpecimenRequestLineItem item : lineItems) {
            item.setInstitution(userInstitution);
            item.getVote().setInstitution(userInstitution);
            item.getVote().setVote(Vote.APPROVE);
        }
        action.validate();
        assertTrue(action.getFieldErrors().isEmpty());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected SpecimenRequest getTestRequest() {
        SpecimenRequest request = super.getTestRequest();

        request.getConsortiumReviews().clear();
        request.getLineItems().clear();
        request.getOrders().clear();

        for (SpecimenRequestReviewVote review : request.getInstitutionalReviews()) {
            AggregateSpecimenRequestLineItem lineItem = new AggregateSpecimenRequestLineItem();
            lineItem.setInstitution(review.getInstitution());
            lineItem.setQuantity(1);
            lineItem.setCriteria("criteria");
            request.getAggregateLineItems().add(lineItem);
        }

        return request;
    }

    private LineItemReviewAction getLineItemReviewAction() {
        return new LineItemReviewAction(getTestUserService(), getTestAppSettingService(), 
                getTestUiDynamicFieldCategoryService()) {
            private static final long serialVersionUID = 1L;

            /** {@inheritDoc} */
            @Override
            public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
                return new ArrayList<AbstractDynamicFieldDefinition>();
            }
        };
    }
}
