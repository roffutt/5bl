/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails;

import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.google.inject.Inject;

/**
 * @author vsemenov
 *
 */
public class DbCartAction extends AbstractCartAction {

    private static final long serialVersionUID = -8734625028768744475L;

    /**
     * Default constructor.
     */
    public DbCartAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     */
    @Inject
    public DbCartAction(ApplicationSettingServiceLocal appSettingService, 
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
        super(appSettingService, uiDynamicFieldCategoryService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String emptyCart() {
        String retval = super.emptyCart();
        getService().savePersistentObject(getObject());
        return retval;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String removeFromCart() {
        String retval = super.removeFromCart();
        getService().savePersistentObject(getObject());
        return retval;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String emptyAggregateCart() {
        String retval = super.emptyAggregateCart();
        getService().savePersistentObject(getObject());
        return retval;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String removeFromGeneralAggregateCart() {
        String retval = super.removeFromGeneralAggregateCart();
        getService().savePersistentObject(getObject());
        return retval;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String removeFromSpecificAggregateCart() {
        String retval = super.removeFromSpecificAggregateCart();
        getService().savePersistentObject(getObject());
        return retval;
}
}
