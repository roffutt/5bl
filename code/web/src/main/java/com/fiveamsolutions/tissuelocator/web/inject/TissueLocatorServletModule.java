/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.inject;

import org.apache.struts2.dispatcher.ng.filter.StrutsExecuteFilter;
import org.apache.struts2.dispatcher.ng.filter.StrutsPrepareFilter;

import com.fiveamsolutions.nci.commons.web.filter.SessionFixationProtectionFilter;
import com.fiveamsolutions.nci.commons.web.filter.UsernameFilter;
import com.fiveamsolutions.tissuelocator.web.filter.TissueLocatorOpenSessionInViewFilter;
import com.fiveamsolutions.tissuelocator.web.filter.TissueLocatorUserFilter;
import com.google.inject.servlet.ServletModule;
import com.opensymphony.sitemesh.webapp.SiteMeshFilter;

/**
 * @author smiller
 *
 */
public class TissueLocatorServletModule extends ServletModule {
    private static final String ALL_ACTIONS = "*.action";
    private static final String ALL_REQUESTS = "/*";

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configureServlets() {
        super.configureServlets();
        filter(ALL_ACTIONS).through(SessionFixationProtectionFilter.class);
        filter(ALL_ACTIONS).through(UsernameFilter.class);
        filter(ALL_ACTIONS).through(TissueLocatorOpenSessionInViewFilter.class);
        filter(ALL_ACTIONS).through(TissueLocatorUserFilter.class);
        filter(ALL_REQUESTS).through(StrutsPrepareFilter.class);
        filter(ALL_REQUESTS).through(SiteMeshFilter.class);
        filter(ALL_ACTIONS, "/struts/*").through(StrutsExecuteFilter.class);
    }
}
