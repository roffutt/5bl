/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts2.util.SubsetIteratorFilter.Decider;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.ExtendableEntity;
import com.fiveamsolutions.dynamicextensions.hibernate.DynamicExtensionsConfigurator;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.data.search.SortCriterion;
import com.fiveamsolutions.tissuelocator.data.LobHolder;
import com.fiveamsolutions.tissuelocator.data.config.category.AbstractUiCategory;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField;
import com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicFieldCategory;
import com.fiveamsolutions.tissuelocator.data.extension.FileDynamicFieldDefinition;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorFileHelper;
import com.fiveamsolutions.tissuelocator.web.util.ExtendableEntitySection;
import com.google.inject.Inject;

/**
 * Abstract action class for extendable entities.
 * @author ddasgupta
 * @param <T> the type of the object to manage
 * @param <E> the type of the sort criterion
 */
public abstract class AbstractDynamicExtensionAction<T extends PersistentObject, E extends Enum<E> & SortCriterion<T>>
        extends AbstractPersistentObjectAction<T, E> {

    private static final long serialVersionUID = 7539633885964380364L;

    private UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService;
    private Map<String, UiDynamicFieldCategory> uiDynamicFieldCategories;
    private final Map<String, AbstractDynamicFieldDefinition> dynamicFieldDefinitionsByFieldName 
        = new HashMap<String, AbstractDynamicFieldDefinition>();
    private final Map<String, FileUpload> files = new HashMap<String, FileUpload>();

    /**
     * Default constructor.
     */
    public AbstractDynamicExtensionAction() {
        throw new UnsupportedOperationException();
    }
    /**
     * Default constructor.
     * @param uiDynamicFieldCategoryService dynamic field category service.
     */
    @Inject
    @SuppressWarnings("PMD.ConstructorCallsOverridableMethod")
    public AbstractDynamicExtensionAction(UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
        super();
        this.uiDynamicFieldCategoryService = uiDynamicFieldCategoryService;
        for (AbstractDynamicFieldDefinition definition : getDynamicFieldDefinitions()) {
            if (FileDynamicFieldDefinition.class.isAssignableFrom(definition.getClass())) {
                files.put(definition.getFieldName(), new FileUpload());
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        for (AbstractDynamicFieldDefinition definition : getDynamicFieldDefinitions()) {
            dynamicFieldDefinitionsByFieldName.put(definition.getFieldName(), definition);
            if (FileDynamicFieldDefinition.class.isAssignableFrom(definition.getClass())) {
                FileDynamicFieldDefinition fileDef = (FileDynamicFieldDefinition) definition;
                FileUpload upload = files.get(fileDef.getFieldName());
                if (upload.getFile() != null) {
                    ExtendableEntity ee = (ExtendableEntity) getObject();
                    byte[] fileContents = TissueLocatorFileHelper.getFileContents(upload.getFile());
                    ee.setCustomProperty(fileDef.getFieldName(), new LobHolder(fileContents));
                    ee.setCustomProperty(fileDef.getFileNameFieldName(), upload.getFileFileName());
                    ee.setCustomProperty(fileDef.getContentTypeFieldName(), upload.getFileContentType());
                }
            }
        }

        initializeUiDynamicFieldCategories();
    }

    /**
     * @return the files
     */
    public Map<String, FileUpload> getFiles() {
        return this.files;
    }

    /**
     * Retrieve a collection of category names.
     *
     * @return the set of category names
     */
    public Set<String> getCategoryNames() {
        return uiDynamicFieldCategories.keySet();
    }

    /**
     * @return get the dynamic extension field definitions for this class
     */
    public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions() {
        DynamicExtensionsConfigurator configurator = TissueLocatorHibernateUtil.getDynamicExtensionConfigurator();
        return configurator.getDynamicFieldDefinitionsForClass(getObjectType().getName());
    }
    
    /**
     * Returns the field definition for the given field.
     * @param fieldName Field name of the dynamic field definition.
     * @return The field definition for the given field name.
     */
    public AbstractDynamicFieldDefinition getDynamicFieldDefinition(String fieldName) {
        return dynamicFieldDefinitionsByFieldName.get(fieldName);
    }

    /**
     * Return the user interface dynamic field categories for the specimen detail pages.
     *
     * @return the specimen detail user interface dynamic field categories
     */
    public Map<String, UiDynamicFieldCategory> getUiDynamicFieldCategories() {
        return uiDynamicFieldCategories;
    }
    
    /**
     * Returns the dynamic field definitions for the given category.
     * @param category Category for which definitions will be returned.
     * @return the dynamic field definitions for the given category.
     */
    public List<AbstractDynamicFieldDefinition> getDynamicFieldDefinitionsByCategory(UiDynamicFieldCategory category) {
        List<AbstractDynamicFieldDefinition> definitions = new ArrayList<AbstractDynamicFieldDefinition>();
        if (category != null) {
            for (UiDynamicCategoryField customField : category.getCustomPropertyFields()) {
                definitions.add(getDynamicFieldDefinition(customField.getFieldConfig().getFieldName()));
            }
        }
        return definitions;
    }
    
    /**
     * Normalize name by replacing all white spaces with '_'.
     *
     * @param name string for which white spaces need to be replaced.
     * @return name string with all white spaces replaced with '_'
     */
    public String getNormalizedName(String name) {
        return name.replaceAll("\\s", "_");
    }

    private void initializeUiDynamicFieldCategories() {
        uiDynamicFieldCategories = uiDynamicFieldCategoryService.getUiDynamicFieldCategories(getObjectType());
    }

    /**
     * Returns the UiDynamicFieldCategory for a given section.
     *
     * @param sectionHeaderKey the resource key for the section header
     * @return a map of dynamic field user interface categories
     */
    public UiDynamicFieldCategory getUiDynamicFieldCategory(String sectionHeaderKey) {
        return getUiDynamicFieldCategories().get(getText(sectionHeaderKey));
    }

    /**
     * @return a collection of all sections
     */
    protected abstract Collection<? extends ExtendableEntitySection> getAllSections();

    /**
     * Determines if a given user interface category should be included in the resulting subset iterator.
     *
     * @return the user interface category decider
     */
    public Decider getUiDynamicFieldCategoryDecider() {
        return new Decider() {
            @Override
            public boolean decide(Object element) {
                AbstractUiCategory uiCategory = (AbstractUiCategory) element;
                for (ExtendableEntitySection headerKey : getAllSections()) {
                    String headerValue = getText(headerKey.getResourceKey());
                    if (headerValue.equalsIgnoreCase(uiCategory.getCategoryName())) {
                        return false;
                    }
                }
                return true;
            }
        };
    }

    /**
     * A file upload, including the file and its name and content type.
     * @author ddasgupta
     */
    public class FileUpload {
        private File file;
        private String fileFileName;
        private String fileContentType;

        /**
         * @return the file
         */
        public File getFile() {
            return this.file;
        }

        /**
         * @param file the file to set
         */
        public void setFile(File file) {
            this.file = file;
        }

        /**
         * @return the fileFileName
         */
        public String getFileFileName() {
            return this.fileFileName;
        }

        /**
         * @param fileFileName the fileFileName to set
         */
        public void setFileFileName(String fileFileName) {
            this.fileFileName = fileFileName;
        }

        /**
         * @return the fileContentType
         */
        public String getFileContentType() {
            return this.fileContentType;
        }

        /**
         * @param fileContentType the fileContentType to set
         */
        public void setFileContentType(String fileContentType) {
            this.fileContentType = fileContentType;
        }
    }

}
