/**
 * NOTICE: This software  source code and any of  its derivatives are the
 * confidential  and  proprietary   information  of  5AM Solutions, Inc.,
 * Inc. (such source  and its derivatives are hereinafter  referred to as
 * "Confidential Information"). The  Confidential Information is intended
 * to be  used exclusively by  individuals or entities that  have entered
 * into either  a non-disclosure agreement or license  agreement (or both
 * of   these  agreements,   if  applicable)  with  5AM  Solutions,  Inc.
 * ("5AM")   regarding   the  use   of  the   Confidential   Information.
 * Furthermore,  the  Confidential  Information  shall be  used  only  in
 * accordance  with   the  terms   of  such  license   or  non-disclosure
 * agreements.   All  parties using  the  Confidential Information  shall
 * verify that their  intended use of the Confidential  Information is in
 * compliance  with and  not in  violation of  any applicable  license or
 * non-disclosure  agreements.  Unless expressly  authorized  by  5AM  in
 * writing, the Confidential Information  shall not be printed, retained,
 * copied, or  otherwise disseminated,  in part or  whole.  Additionally,
 * any party using the Confidential  Information shall be held liable for
 * any and  all damages incurred  by  5AM  due  to any disclosure  of the
 * Confidential  Information (including  accidental disclosure).   In the
 * event that  the applicable  non-disclosure or license  agreements with
 * 5AM  have  expired, or  if  none  currently  exists,  all  copies   of
 * Confidential Information in your  possession, whether in electronic or
 * printed  form, shall be  destroyed  or  returned to  5AM  immediately.
 * 5AM  makes  no  representations  or warranties  hereby regarding   the
 * suitability  of  the   Confidential  Information,  either  express  or
 * implied,  including  but not  limited  to  the  implied warranties  of
 * merchantability,    fitness    for    a   particular    purpose,    or
 * non-infringement. 5AM  shall not be liable for  any  damages  suffered
 * by  licensee as  a result  of  using, modifying  or distributing  this
 * Confidential Information.  Please email [info@5amsolutions.com]   with
 * any questions regarding the use of the Confidential Information.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.interceptor;

import com.fiveamsolutions.nci.commons.data.security.AbstractUser;
import com.fiveamsolutions.nci.commons.web.struts2.interceptor.AbstractExceptionMailerInterceptor;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @author bpickeral
 *
 */
public class TissueLocatorExceptionMailingInterceptor extends AbstractExceptionMailerInterceptor {
    private static final long  serialVersionUID    = 5743557429753540L;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getHelpdeskEmail(ActionInvocation invocation) {
        return getText(invocation, "email.unhandledError.helpDesk");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUnhandledErrorTemplateString(ActionInvocation invocation) {
        return getText(invocation, "email.unhandledError.body");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEmailTitle(ActionInvocation invocation) {
        return getText(invocation, "email.unhandledError.title");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getErrorSendingEmail(ActionInvocation invocation) {
        return getText(invocation, "email.error.notSent");
    }

    private String getText(ActionInvocation invocation, String text) {
        return ((ActionSupport) invocation.getAction()).getText(text);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractUser getUser() {
        return new TissueLocatorUser();
    }
}
