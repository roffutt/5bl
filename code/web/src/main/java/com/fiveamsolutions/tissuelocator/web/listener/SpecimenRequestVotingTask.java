/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.listener;

import java.util.Date;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.service.SpecimenRequestProcessingServiceLocal;
import com.fiveamsolutions.tissuelocator.util.RequestProcessingConfiguration;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;

/**
 * @author ddasgupta
 *
 */
public final class SpecimenRequestVotingTask extends TimerTask {

    private final int reviewPeriod;
    private final int votingPeriod;
    private final int minPercentAvailableVotes;
    private final int minPercentWinningVotes;
    private final int reviewerAssignmentPeriod;

    private static final Logger LOG = Logger.getLogger(SpecimenRequestVotingTask.class);

    /**
     * @param reviewPeriod the reviewPeriod
     * @param votingPeriod the votingPeriod
     * @param minPercentAvailableVotes the minPercentAvailableVotes
     * @param minPercentWinningVotes the minPercentWinningVotes
     * @param reviewerAssignmentPeriod the reviewerAssignmentPeriod
     */
    @SuppressWarnings("PMD.ExcessiveParameterList")
    public SpecimenRequestVotingTask(int reviewPeriod, int votingPeriod, int minPercentAvailableVotes,
            int minPercentWinningVotes, int reviewerAssignmentPeriod) {
        this.reviewPeriod = reviewPeriod;
        this.votingPeriod = votingPeriod;
        this.minPercentAvailableVotes = minPercentAvailableVotes;
        this.minPercentWinningVotes = minPercentWinningVotes;
        this.reviewerAssignmentPeriod = reviewerAssignmentPeriod;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        LOG.info("starting specimen request voting task at " + new Date().toString());
        SpecimenRequestProcessingServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getSpecimenRequestProcessingService();
        RequestProcessingConfiguration config = new RequestProcessingConfiguration();
        config.setReviewPeriod(reviewPeriod);
        config.setVotingPeriod(votingPeriod);
        config.setMinPercentAvailableVotes(minPercentAvailableVotes);
        config.setMinPercentWinningVotes(minPercentWinningVotes);
        config.setReviewerAssignmentPeriod(reviewerAssignmentPeriod);
        service.processPendingRequests(config);
    }
}
