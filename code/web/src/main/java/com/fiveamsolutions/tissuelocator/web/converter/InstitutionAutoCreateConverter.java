/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.converter;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.opensymphony.xwork2.ActionContext;

/**
 * Handles conversion of an autocreated institution to an existing institution
 * if one with the same name and type exists.
 * @author gvaughn
 *
 */
public class InstitutionAutoCreateConverter extends PersistentObjectTypeConverter {

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("rawtypes")
    public Object convertFromString(Map context, String[] values, Class toClass) {
        Object converted = super.convertFromString(context, values, toClass);
        if (converted == null) {
            converted = getInstitutionByName();
        }
        return converted;
    }

    private Institution getInstitutionByName() {
        String name = (String) getParameter("object.institution.name");
        if (name == null) {
            return null;
        }
        Institution inst = getService().getInstitutionByName(name);
        if (inst == null) {
            return null;           
        }
        String type = (String) getParameter("object.institution.type");
        if (StringUtils.isNotBlank(type) && !type.equals(inst.getType().getId().toString())) {
            return null;
        }
        return inst;
    }
    
    private Object getParameter(String expression) {
        Map<String, Object> parameterMap = ActionContext.getContext().getParameters();
        if (parameterMap != null) {
            String[] param = (String[]) parameterMap.get(expression);
            if (param != null) {
                return param[0];
            }
        }
        return null;
    }
    
    private InstitutionServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getInstitutionService();
    }
}
