/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.ReviewProcess;
import com.fiveamsolutions.tissuelocator.data.Shipment;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequestReviewVote;
import com.fiveamsolutions.tissuelocator.data.Vote;
import com.fiveamsolutions.tissuelocator.service.ShipmentServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.google.inject.Inject;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * @author smiller
 *
 */
public class AddToOrderAction extends ActionSupport implements Preparable {
    private static final long serialVersionUID = 1L;

    private Set<Long> selection = new HashSet<Long>();
    private AggregateSpecimenRequestLineItem[] aggregateSelections = new AggregateSpecimenRequestLineItem[0];
    private Shipment order = new Shipment();
    private final TissueLocatorUserServiceLocal userService;
    private final ApplicationSettingServiceLocal appSettingService;

    /**
     * Default constructor.
     */
    public AddToOrderAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     *
     * @param userService user service
     * @param appSettingService the application setting service
     */
    @Inject
    public AddToOrderAction(TissueLocatorUserServiceLocal userService,
            ApplicationSettingServiceLocal appSettingService) {
        this.userService = userService;
        this.appSettingService = appSettingService;
    }

    /**
     * Adds the selected specimens to the order.
     * @return the struts forward.
     */
    public String addToOrder() {
        Set<Specimen> specimensToAdd = new HashSet<Specimen>();
        for (Long currentId : selection) {
            Specimen s = TissueLocatorRegistry.getServiceLocator().getSpecimenService().
                getPersistentObject(Specimen.class, currentId);
            specimensToAdd.add(s);
        }
        getService().addSpecimenToOrder(specimensToAdd, getOrder());
        return SUCCESS;
    }

    /**
     * action for adding aggregate line items to an order.
     * @return a struts forward
     */
    @SkipValidation
    public String addAggregatesToOrder() {
        for (AggregateSpecimenRequestLineItem aggregateSelection : getAggregateSelections()) {
            if (aggregateSelection.getQuantity() > 0) {
                boolean quantityUpdated = updateQuantity(aggregateSelection);
                if (!quantityUpdated) {
                    addVote(aggregateSelection);
                    getOrder().getAggregateLineItems().add(aggregateSelection);
                }
            }
        }
        getService().savePersistentObject(getOrder());
        return SUCCESS;
    }

    private boolean updateQuantity(AggregateSpecimenRequestLineItem aggregateSelection) {
        for (AggregateSpecimenRequestLineItem lineItem : getOrder().getAggregateLineItems()) {
            if (StringUtils.equals(aggregateSelection.getCriteria(), lineItem.getCriteria())
                    && ObjectUtils.equals(aggregateSelection.getInstitution(), lineItem.getInstitution())) {
                lineItem.setQuantity(aggregateSelection.getQuantity());
                return true;
            }
        }
        return false;
    }

    private void addVote(AggregateSpecimenRequestLineItem aggregateSelection) {
        if (ReviewProcess.LINE_ITEM_REVIEW.equals(appSettingService.getReviewProcess())) {
            SpecimenRequestReviewVote vote = new SpecimenRequestReviewVote();
            vote.setVote(Vote.APPROVE);
            vote.setInstitution(aggregateSelection.getInstitution());
            vote.setUser(userService.getByUsername(UsernameHolder.getUser()));
            vote.setComment(getText("shipment.cart.aggregate.approved.comment"));
            aggregateSelection.setVote(vote);
        }
    }

    private ShipmentServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getShipmentService();
    }

    /**
     * @return the selection
     */
    public Set<Long> getSelection() {
        return selection;
    }

    /**
     * @param selection the selection to set
     */
    public void setSelection(Set<Long> selection) {
        this.selection = selection;
    }

    /**
     * @return the aggregateSelections
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public AggregateSpecimenRequestLineItem[] getAggregateSelections() {
        return aggregateSelections;
    }

    /**
     * @param aggregateSelections the aggregateSelections to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setAggregateSelections(
            AggregateSpecimenRequestLineItem[] aggregateSelections) {
        this.aggregateSelections = aggregateSelections;
    }

    /**
     * @return the order
     */
    public Shipment getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Shipment order) {
        this.order = order;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public void prepare() {
        int selectionCount = 0;
        Enumeration<String> paramNames = ServletActionContext.getRequest().getParameterNames();
        while (paramNames.hasMoreElements()) {
            String name = paramNames.nextElement();
            if (name.startsWith("aggregateSelections")) {
                String indexString = StringUtils.substringBefore(StringUtils.substringAfter(name, "["), "]");
                int index = Integer.valueOf(indexString);
                if (index > selectionCount) {
                    selectionCount = index;
                }
            }
        }
        setAggregateSelections(new AggregateSpecimenRequestLineItem[selectionCount + 1]);
        for (int i = 0; i < aggregateSelections.length; i++) {
            aggregateSelections[i] = new AggregateSpecimenRequestLineItem();
        }
    }
}
