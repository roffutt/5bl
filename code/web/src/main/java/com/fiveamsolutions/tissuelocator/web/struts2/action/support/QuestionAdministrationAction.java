/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.support;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.fiveamsolutions.tissuelocator.data.support.QuestionResponse;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.service.support.QuestionServiceLocal;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
@Namespace("/admin/support/question")
@Results(value = {
        @Result(name = "success", location = "/WEB-INF/jsp/support/question/adminList.jsp"),
        @Result(name = "input", location = "/WEB-INF/jsp/support/question/review.jsp"),
        @Result(name = "view", location = "/WEB-INF/jsp/support/question/adminView.jsp")
})
public class QuestionAdministrationAction extends QuestionAction {

    private static final long serialVersionUID = 573364190751203955L;
    private QuestionResponse adminResponse = new QuestionResponse();

    /**
     * Default constructor.
     */
    public QuestionAdministrationAction() {
        throw new UnsupportedOperationException();
    }

    /**
     * @param questionService support letter request service
     * @param lobHolderService the lob holder service
     */
    @Inject
    public QuestionAdministrationAction(QuestionServiceLocal questionService, LobHolderServiceLocal lobHolderService) {
        super(questionService, lobHolderService);
    }

    /**
     * action to submit a response to a question.
     * @return the forward to go to.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "question")
                }),
                @CustomValidator(type = "hibernate", fieldName = "adminResponse",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "questionResponse")
                })
            }
        )
    @Action(value = "review")
    public String review() {
        getService().addAdminResponse(getObject(), getAdminResponse());
        addActionMessage(getText("question.adminReview.success"));
        return list();
    }

    /**
     * @return the adminResponse
     */
    public QuestionResponse getAdminResponse() {
        return adminResponse;
    }

    /**
     * @param adminResponse the adminResponse to set
     */
    public void setAdminResponse(QuestionResponse adminResponse) {
        this.adminResponse = adminResponse;
    }
}
