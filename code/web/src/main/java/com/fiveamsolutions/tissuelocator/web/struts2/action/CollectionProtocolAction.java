/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.CollectionProtocolSortCriterion;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author smiller
 *
 */
public class CollectionProtocolAction
    extends AbstractPersistentObjectAction<CollectionProtocol, CollectionProtocolSortCriterion> {

    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public CollectionProtocolAction() {
        setObject(new CollectionProtocol());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String input() {
        if (getObject() != null && getObject().getInstitution() != null) {
            setInstitutionSelection(getObject().getInstitution().getName());
        }
        return super.input();
    }

    /**
     * action to save an institution.
     * @return the forward to go to.
     */
    @Validations(
            customValidators = { @CustomValidator(type = "hibernate", fieldName = "object" ,
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "protocol") })
            },
            fieldExpressions = {
                @FieldExpressionValidator(fieldName = "institutionSelection", key = "protocol.institution.required",
                        expression = "institutionSelection == object.institution.name")
            },
            expressions = {
                @ExpressionValidator(key = "protocol.dateOrder", message = "",
                        expression = "object.startDate == null or object.endDate == null "
                            + "or object.startDate <= object.endDate")
            }
    )
    public String save() {
        CollectionProtocolServiceLocal service = getService();
        service.savePersistentObject(getObject());
        addActionMessage(getText("protocol.success"));

        return list();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaginatedList<CollectionProtocol> getPaginatedList() {
        return new SortablePaginatedList<CollectionProtocol, CollectionProtocolSortCriterion>(1,
                CollectionProtocolSortCriterion.NAME.name(), CollectionProtocolSortCriterion.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CollectionProtocolServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getCollectionProtocolService();
    }
}
