/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.tissuelocator.web.util.HomePageWidget;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * @author bhumphrey
 *
 */
public class BrowseDiseaseWidgetAction extends ActionSupport implements Preparable {

    /**
     * Serial Version Id.
     */
    private static final long serialVersionUID = 2953254985915508834L;

    private Map<String, Long[]> countsByPathologicalFinding;
    private Map<String, Long> pathologicalCharacteristics;
    private boolean displayDiseaseCount;

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public void prepare() {
        String name = HomePageWidget.DISEASE_WITH_COUNT.getAttributeName();
        countsByPathologicalFinding =
            (Map<String, Long[]>) ServletActionContext.getServletContext().getAttribute(name);
        name = HomePageWidget.DISEASE_NO_COUNT.getAttributeName();
        pathologicalCharacteristics = (Map<String, Long>) ServletActionContext.getServletContext().getAttribute(name);
        setDisplayDiseaseCount(countsByPathologicalFinding != null);
    }

    /**
     * @return the countsByPathologicalFinding
     */
    public Map<String, Long[]> getCountsByPathologicalFinding() {
        return countsByPathologicalFinding;
    }

    /**
     * @return the pathologicalCharacteristics
     */
    public Map<String, Long> getPathologicalCharacteristics() {
        return pathologicalCharacteristics;
    }

    /**
     * @return the displayDiseaseCount
     */
    public boolean isDisplayDiseaseCount() {
        return displayDiseaseCount;
    }

    /**
     * @param displayDiseaseCount the displayDiseaseCount to set
     */
    public void setDisplayDiseaseCount(boolean displayDiseaseCount) {
        this.displayDiseaseCount = displayDiseaseCount;
    }

}
