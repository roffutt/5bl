/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.io.File;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.struts2.convention.annotation.Action;

import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.nci.commons.web.displaytag.PaginatedList;
import com.fiveamsolutions.nci.commons.web.displaytag.SortablePaginatedList;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.InstitutionType;
import com.fiveamsolutions.tissuelocator.data.Person;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.service.InstitutionServiceLocal;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.search.InstitutionSortCriterion;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorFileHelper;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * Action for managing institutions.
 * @author smiller
 */
@SuppressWarnings("PMD.TooManyMethods")
public class InstitutionAction extends AbstractPersistentObjectAction<Institution, InstitutionSortCriterion> {

    private static final long serialVersionUID = 1L;
    private static final String HIBERNATE = "hibernate";
    private static final String RESOURCE_KEY_BASE = "resourceKeyBase";
    private static final String CONDITIONAL_EXPRESSION = "conditionalExpression";


    private boolean consortiumMembersOnly;
    private List<InstitutionType> types;

    private MaterialTransferAgreement currentMta;
    private SignedMaterialTransferAgreement signedMta = new SignedMaterialTransferAgreement();
    private File document;
    private String documentFileName;
    private String documentContentType;
    private boolean sendingMta;
    private Person origMtaContact;
    private Boolean wasConsortiumMember;
    private final TissueLocatorUserServiceLocal userService;

    /**
     * Default constructor.
     */
    public InstitutionAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     * @param userService the user service
     */
    @Inject
    public InstitutionAction(TissueLocatorUserServiceLocal userService) {
        super();
        this.userService = userService;
        setObject(new Institution());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        types =
            TissueLocatorRegistry.getServiceLocator().getGenericService().getAll(InstitutionType.class);

        MaterialTransferAgreementServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getMaterialTransferAgreementService();
        setCurrentMta(service.getCurrentMta());

        TissueLocatorUser user = userService.getByUsername(UsernameHolder.getUser());
        getSignedMta().setUploader(user);
        getSignedMta().setUploadTime(new Date());
        if (isSendingMta()) {
            getSignedMta().setSendingInstitution(getObject());
        } else {
            getSignedMta().setReceivingInstitution(getObject());
        }
        getSignedMta().setStatus(SignedMaterialTransferAgreementStatus.APPROVED);
        getSignedMta().setOriginalMta(getCurrentMta());

        if (getDocument() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getDocument());
            getSignedMta().setDocument(new TissueLocatorFile(fileContents, getDocumentFileName(),
                    getDocumentContentType()));
        }

        origMtaContact = null;
        if (getObject().getMtaContact() != null && getObject().getMtaContact().getId() != null) {
            origMtaContact = new Person();
            origMtaContact.copyValues(getObject().getMtaContact());
        }
        wasConsortiumMember = getObject().getConsortiumMember();
    }

    /**
     * action to save an institution.
     * @return the forward to go to.
     */
    @Validations(
            customValidators = {
                    @CustomValidator(type = HIBERNATE, fieldName = "object" ,
                            parameters = { @ValidationParameter(name = RESOURCE_KEY_BASE, value = "institution"),
                                           @ValidationParameter(name = "excludes",
                                                   value = "billingAddress.line1, billingAddress.city, "
                                                         + "billingAddress.state, billingAddress.zip, "
                                                         + "billingAddress.country, billingAddress.phone, "
                                                         + "mtaContact.firstName, mtaContact.lastName, "
                                                         + "mtaContact.email, mtaContact.organization, "
                                                         + "mtaContact.address.line1, mtaContact.address.city, "
                                                         + "mtaContact.address.state, mtaContact.address.zip, "
                                                         + "mtaContact.address.country, mtaContact.address.phone"),
                                           @ValidationParameter(name = CONDITIONAL_EXPRESSION,
                                                                value = "object.consortiumMember == false") }),
                    @CustomValidator(type = HIBERNATE, fieldName = "object" ,
                            parameters = { @ValidationParameter(name = RESOURCE_KEY_BASE, value = "institution"),
                                           @ValidationParameter(name = "excludes",
                                                   value = "mtaContact.firstName, mtaContact.lastName, "
                                                         + "mtaContact.email, mtaContact.organization, "
                                                         + "mtaContact.address.line1, mtaContact.address.city, "
                                                         + "mtaContact.address.state, mtaContact.address.zip, "
                                                         + "mtaContact.address.country, mtaContact.address.phone"),
                                           @ValidationParameter(name = CONDITIONAL_EXPRESSION,
                                                                value = "object.consortiumMember == true") }),
                    @CustomValidator(type = HIBERNATE, fieldName = "signedMta" ,
                            parameters = { @ValidationParameter(name = RESOURCE_KEY_BASE, value = "signedMta"),
                                           @ValidationParameter(name = CONDITIONAL_EXPRESSION,
                                                   value = "document != null") }),
                    @CustomValidator(type = HIBERNATE, fieldName = "object.mtaContact" ,
                            parameters = { @ValidationParameter(name = RESOURCE_KEY_BASE,
                                                   value = "institution.mtaContact"),
                                           @ValidationParameter(name = "excludes",
                                                   value = "organization"),
                                           @ValidationParameter(name = CONDITIONAL_EXPRESSION,
                                                   value = "!sendingMta && object.mtaContact != null && "
                                                           +  "(document != null || !signedMtas.empty"
                                                           + "|| object.mtaContact.id != null)") })
            }
    )
    public String save() {
        resetBillingAddress();
        resetMtaContact();
        // ignore MTA contact information if uploading a sending MTA
        if (isSendingMta()) {
            ignoreSendMtaContact();
        }

        // If institution was removed as a consortium member, deprecate the current sender MTA
        if (wasConsortiumMember != null) {
            checkConsortiumMembershipChange();
        }

        // Save the Institution
        if (getObject().getId() == null) {
            saveNewInstitution();
        } else {
            if (getDocument() != null) {
                TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService()
                    .updateSignedMta(getSignedMta(), getCurrentSignedMta());
            } else {
                getService().savePersistentObject(getObject());
            }
        }

        addActionMessage(getText("institution.success"));
        return list();
    }

    private void saveNewInstitution() {
        // Set this institution as the mta contact's organization
        Person mtaContact = getObject().getMtaContact();
        if (mtaContact != null) {
            mtaContact.setOrganization(getObject());
        }

        // Save the institution
        if (getDocument() != null) {
            if (isSendingMta()) {
                getSignedMta().setSendingInstitution(getObject());
            } else {
                getSignedMta().setReceivingInstitution(getObject());
            }
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService()
               .savePersistentObject(getSignedMta());
        }

        getService().saveNewInstitution(getObject());
    }

    private void checkConsortiumMembershipChange() {
        if (wasConsortiumMember && !getObject().getConsortiumMember()) {
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService()
                .deprecateSignedMta(getObject().getCurrentSignedSenderMta());
        }
    }

    private void ignoreSendMtaContact() {
        if (origMtaContact != null) {
            getSignedMta().getSendingInstitution().getMtaContact().copyValues(origMtaContact);
        } else {
            getSignedMta().getSendingInstitution().setMtaContact(null);
        }
    }

    private SignedMaterialTransferAgreement getCurrentSignedMta() {
        if (isSendingMta()) {
            return getSignedMta().getSendingInstitution().getCurrentSignedSenderMta();
        } else {
            return getSignedMta().getReceivingInstitution().getCurrentSignedRecipientMta();
        }
    }

    private void resetBillingAddress() {
        if (getObject().getConsortiumMember() == null || !getObject().getConsortiumMember()) {
            getObject().setBillingAddress(null);
        }
    }

    private void resetMtaContact() {
        if (getDocument() == null && getSignedMtas().isEmpty() && getObject().getMtaContact() != null
                && getObject().getMtaContact().getId() == null) {
            getObject().setMtaContact(null);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Action("filter")
    public String filter() {
        if (isConsortiumMembersOnly()) {
            getObject().setConsortiumMember(true);
        } else {
            getObject().setConsortiumMember(null);
        }
        return super.filter();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PaginatedList<Institution> getPaginatedList() {
        return new SortablePaginatedList<Institution, InstitutionSortCriterion>(1,
                InstitutionSortCriterion.NAME.name(), InstitutionSortCriterion.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected InstitutionServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getInstitutionService();
    }

    /**
     * @return the types
     */
    public List<InstitutionType> getTypes() {
        return types;
    }

    /**
     * @return the consortiumMembersOnly
     */
    public boolean isConsortiumMembersOnly() {
        return consortiumMembersOnly;
    }

    /**
     * @param consortiumMembersOnly the consortiumMembersOnly to set
     */
    public void setConsortiumMembersOnly(boolean consortiumMembersOnly) {
        this.consortiumMembersOnly = consortiumMembersOnly;
    }


    /**
     * @return the currentMta
     */
    public MaterialTransferAgreement getCurrentMta() {
        return currentMta;
    }

    /**
     * @param currentMta the currentMta to set
     */
    public void setCurrentMta(MaterialTransferAgreement currentMta) {
        this.currentMta = currentMta;
    }

    /**
     * @return the signedMta
     */
    public SignedMaterialTransferAgreement getSignedMta() {
        return signedMta;
    }

    /**
     * @param signedMta the signedMta to set
     */
    public void setSignedMta(SignedMaterialTransferAgreement signedMta) {
        this.signedMta = signedMta;
    }

    /**
     * @return the document
     */
    public File getDocument() {
        return document;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(File document) {
        this.document = document;
    }

    /**
     * @return the documentFileName
     */
    public String getDocumentFileName() {
        return documentFileName;
    }

    /**
     * @param documentFileName the documentFileName to set
     */
    public void setDocumentFileName(String documentFileName) {
        this.documentFileName = documentFileName;
    }

    /**
     * @return the documentContentType
     */
    public String getDocumentContentType() {
        return documentContentType;
    }

    /**
     * @param documentContentType the documentContentType to set
     */
    public void setDocumentContentType(String documentContentType) {
        this.documentContentType = documentContentType;
    }

    /**
     * @return the sendingMta
     */
    public boolean isSendingMta() {
        return sendingMta;
    }

    /**
     * @param sendingMta the sendingMta to set
     */
    public void setSendingMta(boolean sendingMta) {
        this.sendingMta = sendingMta;
    }

    /**
     * @return a single set of signed MTAs for this institution ordered by upload time
     */
    public Set<SignedMaterialTransferAgreement> getSignedMtas() {
        Comparator<SignedMaterialTransferAgreement> c = new Comparator<SignedMaterialTransferAgreement>() {
            @Override
            public int compare(SignedMaterialTransferAgreement o1, SignedMaterialTransferAgreement o2) {
                return o2.getUploadTime().compareTo(o1.getUploadTime());
            }
        };
        Set<SignedMaterialTransferAgreement> signedMtas = new TreeSet<SignedMaterialTransferAgreement>(c);
        signedMtas.addAll(getObject().getSignedSenderMtas());
        signedMtas.addAll(getObject().getSignedRecipientMtas());
        return signedMtas;
    }
}
