/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web;

import javax.servlet.ServletContext;

/**
 * @author ddasgupta
 *
 */
public class TissueLocatorContextParameterHelper {

    private static final String VOTING_TIME_CONTEXT_PARAM = "votingThreadStartTime";
    private static final String VOTING_PERIOD_CONTEXT_PARAM = "votingPeriod";
    private static final String REVIEW_PERIOD_CONTEXT_PARAM = "reviewPeriod";
    private static final String MIN_PERCENT_AVAILABLE_VOTES = "minPercentAvailableVotes";
    private static final String MIN_PERCENT_WINNING_VOTES = "minPercentWinningVotes";
    private static final String NUM_REVIEWERS_CONTEXT_PARAM = "numberRequestReviewers";
    private static final String REVIWER_ASSIGNMENT_PERIOD = "reviewerAssignmentPeriod";

    /**
     * @return a string representation of the time of daily run of the request processing task.
     * @param context the servlet context
     */
    public static String getVotingTime(ServletContext context) {
        return context.getInitParameter(VOTING_TIME_CONTEXT_PARAM);
    }

    /**
     * @return the length of the voting period.
     * @param context the servlet context
     */
    public static int getVotingPeriod(ServletContext context) {
        return Integer.valueOf(context.getInitParameter(VOTING_PERIOD_CONTEXT_PARAM));
    }

    /**
     * @return the length of the review period.
     * @param context the servlet context
     */
    public static int getReviewPeriod(ServletContext context) {
        return Integer.valueOf(context.getInitParameter(REVIEW_PERIOD_CONTEXT_PARAM));
    }

    /**
     * @return the minimum percent of available votes.
     * @param context the servlet context
     */
    public static int getMinPercentAvailableVotes(ServletContext context) {
        return Integer.valueOf(context.getInitParameter(MIN_PERCENT_AVAILABLE_VOTES));
    }

    /**
     * @return the minimum percent of winning votes.
     * @param context the servlet context
     */
    public static int getMinPercentWinningVotes(ServletContext context) {
        return Integer.valueOf(context.getInitParameter(MIN_PERCENT_WINNING_VOTES));
    }

    /**
     * @return the number of request reviewers.
     * @param context the servlet context
     */
    public static int getNumberRequestReviewers(ServletContext context) {
        return Integer.valueOf(context.getInitParameter(NUM_REVIEWERS_CONTEXT_PARAM));
    }

    /**
     * @return the length of the reviewer assignment period
     * @param context the servlet context
     */
    public static int getReviewerAssignmentPeriod(ServletContext context) {
        return Integer.valueOf(context.getInitParameter(REVIWER_ASSIGNMENT_PERIOD));
    }
}
