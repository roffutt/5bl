/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action.mta;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorFileHelper;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
public class MaterialTransferAgreementAction extends ActionSupport implements Preparable {

    private static final long serialVersionUID = -6688004712958707712L;

    private MaterialTransferAgreement mta = new MaterialTransferAgreement();
    private List<MaterialTransferAgreement> mtas;

    private File document;
    private String documentFileName;
    private String documentContentType;

    /**
     * Default constructor.
     */
    public MaterialTransferAgreementAction() {
        getMta().setUploadTime(new Date());
    }

    /**
     * action to save an institution.
     * @return the forward to go to.
     * @throws IOException on error
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "mta",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "mta")
                })
            },
            fieldExpressions = {
                @FieldExpressionValidator(fieldName = "document",
                    key = "mta.document.required", expression = "document != null || mta.document != null")
            }
        )
    public String save() throws IOException {
        getService().saveMaterialTransferAgreement(getMta());
        addActionMessage(getText("mta.success"));
        setMta(new MaterialTransferAgreement());
        setMtas(getService().getAll());
        return Action.SUCCESS;
    }

    /**
     * {@inheritDoc}
     */
    public void prepare() {
        setMtas(getService().getAll());
        if (getDocument() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getDocument());
            getMta().setDocument(new TissueLocatorFile(fileContents, getDocumentFileName(), getDocumentContentType()));
        }
    }

    /**
     * {@inheritDoc}
     */
    protected MaterialTransferAgreementServiceLocal getService() {
        return TissueLocatorRegistry.getServiceLocator().getMaterialTransferAgreementService();
    }

    /**
     * @return the mta
     */
    public MaterialTransferAgreement getMta() {
        return mta;
    }

    /**
     * @param mta the mta to set
     */
    public void setMta(MaterialTransferAgreement mta) {
        this.mta = mta;
    }

    /**
     * @return the mtas
     */
    public List<MaterialTransferAgreement> getMtas() {
        return mtas;
    }

    /**
     * @param mtas the mtas to set
     */
    public void setMtas(List<MaterialTransferAgreement> mtas) {
        this.mtas = mtas;
    }

    /**
     * @return the document
     */
    public File getDocument() {
        return document;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(File document) {
        this.document = document;
    }

    /**
     * @return the documentFileName
     */
    public String getDocumentFileName() {
        return documentFileName;
    }

    /**
     * @param documentFileName the documentFileName to set
     */
    public void setDocumentFileName(String documentFileName) {
        this.documentFileName = documentFileName;
    }

    /**
     * @return the documentContentType
     */
    public String getDocumentContentType() {
        return documentContentType;
    }

    /**
     * @param documentContentType the documentContentType to set
     */
    public void setDocumentContentType(String documentContentType) {
        this.documentContentType = documentContentType;
    }
}
