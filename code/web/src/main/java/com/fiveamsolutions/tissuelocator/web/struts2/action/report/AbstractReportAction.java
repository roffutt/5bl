/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action.report;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

import org.apache.commons.lang.time.DateUtils;
import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.tissuelocator.service.ReportServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @author ddasgupta
 *
 */
public abstract class AbstractReportAction extends ActionSupport {

    private static final long serialVersionUID = -3209214081701037097L;
    private static final String REPORT_DIR = "WEB-INF/classes/";
    private static final String REPORT_EXTENSION = ".jasper";
    private static final String CONTENT_TYPE = "text/html";
    private static final String IMAGES_URI = "/jasper/image?image=";
    private static final int BLOCK_SIZE = 1024;
    private static final int MAX_SIZE = 3;

    private Date startDate;
    private Date endDate;
    private String reportContent;
    private int page = 0;
    private int lastPage;

    /**
     * set the initial date range.
     */
    public AbstractReportAction() {
        super();
        startDate = DateUtils.truncate(DateUtils.addMonths(new Date(), -1), Calendar.DATE);
        endDate = DateUtils.truncate(new Date(), Calendar.DATE);
    }

    /**
     * Run a jasper report.
     * @param fileName the jasper report file to run
     * @return a struts forward
     * @throws JRException on error
     * @throws IOException on error
     */
    public String runJasperReport(String fileName) throws JRException, IOException {
        ServletActionContext.getResponse().setContentType(CONTENT_TYPE);
        Map<String, Object> params = getParameters();
        JasperPrint report = runReport(fileName, params);

        JRHtmlExporter exporter = new JRHtmlExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, report);
        StringBuffer reportContentBuffer = new StringBuffer();
        exporter.setParameter(JRExporterParameter.OUTPUT_STRING_BUFFER, reportContentBuffer);
        ServletActionContext.getRequest().getSession()
            .setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, report);
        String context = ServletActionContext.getRequest().getContextPath();
        exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, context + IMAGES_URI);
        exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, false);
        exporter.setParameter(JRHtmlExporterParameter.IS_WRAP_BREAK_WORD, true);

        lastPage = report.getPages().size();
        setPage(Math.max(0, Math.min(page, lastPage - 1)));
        if (lastPage > 0) {
            exporter.setParameter(JRExporterParameter.PAGE_INDEX, Integer.valueOf(getPage()));
        }
        exporter.setParameter(JRHtmlExporterParameter.HTML_HEADER, "");
        exporter.setParameter(JRHtmlExporterParameter.BETWEEN_PAGES_HTML, "");
        exporter.setParameter(JRHtmlExporterParameter.HTML_FOOTER, "");

        exporter.exportReport();
        setReportContent(reportContentBuffer.toString());
        ((JRSwapFileVirtualizer) params.get(JRParameter.REPORT_VIRTUALIZER)).cleanup();
        return SUCCESS;
    }

    /**
     * Get the full path to the report file.
     * @param fileName the name of the report file
     * @return the full path to the report file
     */
    protected String getReportPath(String fileName) {
        String fullFileName = REPORT_DIR + fileName + REPORT_EXTENSION;
        return ServletActionContext.getServletContext().getRealPath(fullFileName);
    }

    /**
     * run the report.
     * @param fileName the name of the report file
     * @param parameters the report parameters
     * @return the filled report
     * @throws JRException on error
     */
    protected JasperPrint runReport(String fileName, Map<String, Object> parameters) throws JRException {
        ReportServiceLocal service = TissueLocatorRegistry.getServiceLocator().getReportService();
        return service.runJasperReport(getReportPath(fileName), parameters);
    }

    /**
     * Get the report parameters.  Subclasses should override to specify additional parameters.
     * @return a Map of report parameters
     */
    protected Map<String, Object> getParameters() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", DateUtils.truncate(getStartDate(), Calendar.DATE));
        params.put("endDate", DateUtils.truncate(DateUtils.addDays(getEndDate(), 1), Calendar.DATE));
        params.put("SUBREPORT_DIR", ServletActionContext.getServletContext().getRealPath(REPORT_DIR) + "/");

        JRSwapFile file = new JRSwapFile(System.getProperty("java.io.tmpdir"), BLOCK_SIZE, BLOCK_SIZE);
        JRSwapFileVirtualizer virtualizer = new JRSwapFileVirtualizer(MAX_SIZE, file, false);
        params.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

        return params;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the reportContent
     */
    public String getReportContent() {
        return reportContent;
    }

    /**
     * @param reportContent the reportContent to set
     */
    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }

    /**
     * @return the page
     */
    public int getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * @return the lastPage
     */
    public int getLastPage() {
        return lastPage;
    }
}
