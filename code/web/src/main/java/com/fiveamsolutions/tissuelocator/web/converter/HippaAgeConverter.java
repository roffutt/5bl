/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.converter;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenPatientAgeValidator;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;

/**
 * Conforms the passed in Patient Age At Collection parameters to HIPPA standards
 * by enforcing an upper limit of 90 years.
 *
 * @author smiller
 *
 */
public class HippaAgeConverter extends StrutsTypeConverter {

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("rawtypes")
    public Object convertFromString(Map context, String[] values, Class toClass) {
        // If no time unit was in the action parameters, then just parse the number
        TimeUnits timeUnit = retrieveTimeUnits();
        if (timeUnit == null) {
            return super.performFallbackConversion(context, values[0], toClass);
        }

        // Retrieves the max value user friendly string based on the time unit
        Integer maxAgeValue = SpecimenPatientAgeValidator.getHippaMaxAge(timeUnit);
        String maxAgeValueString = maxAgeValue + "+";

        // Return max age if the passed in string matches the user friendly max string
        if (maxAgeValueString.equals(values[0])) {
            return maxAgeValue;
        }

        // Return max age if the value exceeds allowed value
        Integer result = (Integer) super.performFallbackConversion(context, values[0], toClass);
        if (result >= maxAgeValue) {
            return maxAgeValue;
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("rawtypes")
    public String convertToString(Map context, Object o) {
        // This block will check whether the passed in value is an integer that has a value
        // greater than the allowed maximum.  If it is, then it returns a user friendly
        // string noting the upper HIPPA age limit.
        if (o instanceof Integer) {
            int ageValue = (Integer) o;

            TimeUnits timeUnit = retrieveTimeUnits();
            if (timeUnit == null) {
                return (String) super.convertValue(o, String.class);
            }

            Integer maxAgeValue = SpecimenPatientAgeValidator.getHippaMaxAge(timeUnit);
            if (ageValue >= maxAgeValue) {
                return maxAgeValue + "+";
            }
        }

        // Returns the value as a string
        return (String) super.convertValue(o, String.class);
    }

    /**
     * Retrieves the time units of the patient age at collection of this action.
     *
     * @return TimeUnits The unit of time (years, months, etc.) this action is currently handling.  Can
     *                   be null.
     */
    public TimeUnits retrieveTimeUnits() {
        Map<String, Object> parameterMap = ActionContext.getContext().getParameters();
        if (parameterMap != null) {
            String[] unitParams = (String[]) parameterMap.get("object.patientAgeAtCollectionUnits");
            if (unitParams != null) {
                String timeUnitString = unitParams[0];
                return TimeUnits.valueOf(timeUnitString);
            }
            
            ValueStack vs = ActionContext.getContext().getValueStack();
            return (TimeUnits) vs.findValue("object.patientAgeAtCollectionUnits");            
        }
        
        return null;
    }
}