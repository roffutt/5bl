/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.config.category.DisplayOption;
import com.fiveamsolutions.tissuelocator.data.config.category.SearchResultFieldDisplaySetting;
import com.fiveamsolutions.tissuelocator.data.config.category.UiSearchFieldCategory;
import com.fiveamsolutions.tissuelocator.data.config.search.SearchSetType;
import com.fiveamsolutions.tissuelocator.service.config.category.SearchResultDisplaySettingsServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiSearchFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.google.inject.Inject;

/**
 * @author cgoina
 *
 */
@SuppressWarnings("PMD.TooManyMethods")
public class SearchResultDisplayConfigHelper {

    static final String SPECIMEN_DISPLAY_RESULT_OPTIONS = "SpecimenDisplayOptions";
    
    private Map<String, List<SearchResultFieldDisplaySetting>> categoryDisplayConfig;
    private Map<String, Object> session;
    private final TissueLocatorUserServiceLocal userService;
    private final UiSearchFieldCategoryServiceLocal uiSearchFieldCategoryService;
    private final SearchResultDisplaySettingsServiceLocal searchResultDisplaySettingService;

    /**
     * Class constructor.
     * @param userService the user service
     * @param uiSearchFieldCategoryService search field category service.
     * @param searchResultDisplaySettingService search result display setting service.
     */
    @Inject
    public SearchResultDisplayConfigHelper(TissueLocatorUserServiceLocal userService, 
            UiSearchFieldCategoryServiceLocal uiSearchFieldCategoryService, 
            SearchResultDisplaySettingsServiceLocal searchResultDisplaySettingService) {
        categoryDisplayConfig = new LinkedHashMap<String, List<SearchResultFieldDisplaySetting>>();
        this.userService = userService;
        this.uiSearchFieldCategoryService = uiSearchFieldCategoryService;
        this.searchResultDisplaySettingService = searchResultDisplaySettingService;
    }

    /**
     * @return the categoryDisplayConfig
     */
    public Map<String, List<SearchResultFieldDisplaySetting>> getCategoryDisplayConfig() {
        return categoryDisplayConfig;
    }

    /**
     * @param categoryDisplayConfig the categoryDisplayConfig to set
     */
    public void setCategoryDisplayConfig(Map<String, List<SearchResultFieldDisplaySetting>> categoryDisplayConfig) {
        this.categoryDisplayConfig = categoryDisplayConfig;
    }

    /**
     * Retrieve a collection of category names.
     *
     * @return the set of category names
     */
    public Set<String> getCategoryNames() {
        return categoryDisplayConfig.keySet();
    }

    /**
     * Return the dynamic fields for a specific category.
     *
     * @param category name of the category
     * @return specimen result fields for the specified category
     */
    public List<SearchResultFieldDisplaySetting> getSpecimenResultFieldsByCategory(String category) {
        return categoryDisplayConfig.get(category);
    }

    /**
     * @return the display settings for all specimen result fields ordered by id.
     */
    public List<SearchResultFieldDisplaySetting> getSearchResultFieldDisplaySettings() {
        List<SearchResultFieldDisplaySetting> allResultFieldDisplaySettings =
            getAllFieldDisplaySettings();
        Collections.sort(allResultFieldDisplaySettings, new SearchResultFieldDisplaySettingComparator());
        return allResultFieldDisplaySettings;
    }

    /**
     * Save the session attributes.
     *
     * @param s session to be set
     */
    public void setSession(Map<String, Object> s) {
        session = s;
    }

    /**
     * initialize categories.
     * @param searchSetType SEarch set type of the categories.
     */
    @SuppressWarnings("unchecked")
    public void initializeCategoriesList(SearchSetType searchSetType) {
        if (session != null) {
            // I only want this to be done for session aware actions
            Map<String, SearchResultFieldDisplaySetting> savedSpecimenResultDisplayConfig =
                (Map<String, SearchResultFieldDisplaySetting>) session
                    .get(SPECIMEN_DISPLAY_RESULT_OPTIONS);
            if (savedSpecimenResultDisplayConfig == null || savedSpecimenResultDisplayConfig.isEmpty()) {
                populateCategoriesList(searchSetType);
                saveSpecimenResultsDisplayOptions(false);
            } else {
                restoreSpecimenResultsDisplayOptions(savedSpecimenResultDisplayConfig);
            }
        }
    }

    /**
     * Save specimen result options.
     *
     * @param makeCurrentSettingsPersistent if true persists the settings for the current user not only in the session
     *            but in the service as well.
     */
    public void saveSpecimenResultsDisplayOptions(boolean makeCurrentSettingsPersistent) {
        Map<String, SearchResultFieldDisplaySetting> savedSpecimenResultDisplayConfig =
            new LinkedHashMap<String, SearchResultFieldDisplaySetting>();
        for (Map.Entry<String, List<SearchResultFieldDisplaySetting>> categoryFieldsSettings : categoryDisplayConfig
                .entrySet()) {
            for (SearchResultFieldDisplaySetting fieldSettings : categoryFieldsSettings.getValue()) {
                savedSpecimenResultDisplayConfig.put(fieldSettings.getFieldConfig().getFieldName(), fieldSettings);
            }
        }
        session.put(SPECIMEN_DISPLAY_RESULT_OPTIONS, savedSpecimenResultDisplayConfig);
        if (makeCurrentSettingsPersistent) {
            TissueLocatorUser currentUser = userService.getByUsername(UsernameHolder.getUser());
            searchResultDisplaySettingService.storeUserSearchResultDisplaySettings(currentUser, 
                    getAllFieldDisplaySettings());
        }
    }

    /**
     * Adds the fieldDisplaySetting to the list of valid display fields if the current role
     * is allowed to view the field.
     *
     * @param fieldDisplaySetting field to be added
     * @param validFieldDisplaySettings list of valid fields
     */
    private void addDisplaySettingWithRoleCheck(SearchResultFieldDisplaySetting fieldDisplaySetting,
            List<SearchResultFieldDisplaySetting> validFieldDisplaySettings) {
        boolean roleIsAllowed = true;
        if (fieldDisplaySetting.getFieldConfig().getRequiredRole() != null) {
            roleIsAllowed = ServletActionContext.getRequest().isUserInRole(
                    fieldDisplaySetting.getFieldConfig().getRequiredRole().getName());
        }
        if (roleIsAllowed) {
            validFieldDisplaySettings.add(fieldDisplaySetting);
        }
    }

    /**
     * Returns a list of display settings filtered by role and DisplayOption.
     *
     * @param configuredCategoryfieldDisplaySettings the list of display settings to filter.
     * @return A list of display settings filtered by role and DisplayOption.
     */
    private List<SearchResultFieldDisplaySetting> getFilteredDisplaySettings(
            List<SearchResultFieldDisplaySetting> configuredCategoryfieldDisplaySettings) {
        List<SearchResultFieldDisplaySetting> filteredDisplaySettings 
            = new ArrayList<SearchResultFieldDisplaySetting>();
        for (SearchResultFieldDisplaySetting fieldDisplaySetting : configuredCategoryfieldDisplaySettings) {
            if (fieldDisplaySetting.getDisplayFlag() != DisplayOption.NOT_APPLICABLE) {
                addDisplaySettingWithRoleCheck(fieldDisplaySetting, filteredDisplaySettings);
            }
        }
        return filteredDisplaySettings;
    }

    private List<SearchResultFieldDisplaySetting> getAllFieldDisplaySettings() {
        List<SearchResultFieldDisplaySetting> allFieldDisplaySettings =
            new ArrayList<SearchResultFieldDisplaySetting>();
        for (Map.Entry<String, List<SearchResultFieldDisplaySetting>> categoryDisplaySettings : categoryDisplayConfig
                .entrySet()) {
            allFieldDisplaySettings.addAll(categoryDisplaySettings.getValue());
        }
        return allFieldDisplaySettings;
    }

    private void populateCategoriesList(SearchSetType searchSetType) {
        categoryDisplayConfig.clear();
        populateDynamicCategoriesList(searchSetType);
    }

    private void populateDynamicCategoriesList(SearchSetType searchSetType) {
        Map<String, List<SearchResultFieldDisplaySetting>> categoryDisplaySettingMap =
            retrieveCategoryNameDisplaySettingsMap();
        // retrieve the search categories
        List<UiSearchFieldCategory> searchCategories = uiSearchFieldCategoryService
            .getUiSearchFieldCategories(Specimen.class, searchSetType);
        for (UiSearchFieldCategory searchCategory : searchCategories) {
            List<SearchResultFieldDisplaySetting> fieldDisplaySettings = categoryDisplaySettingMap.get(searchCategory
                    .getCategoryName());
            List<SearchResultFieldDisplaySetting> actualFieldDisplaySettings 
                = new ArrayList<SearchResultFieldDisplaySetting>();
            if (fieldDisplaySettings != null) {                
                actualFieldDisplaySettings.addAll(getFilteredDisplaySettings(fieldDisplaySettings));
                Collections.sort(actualFieldDisplaySettings, 
                        new SearchResultFieldDisplaySettingComparator());                
            }
            if (!actualFieldDisplaySettings.isEmpty()) {
                // if the list of category fields is not empty add this category to the configuration
                categoryDisplayConfig.put(searchCategory.getCategoryName(), actualFieldDisplaySettings);
            }
        }
    }

    /**
     * Save specimen result options.
     */
    private void restoreSpecimenResultsDisplayOptions(
            Map<String, SearchResultFieldDisplaySetting> savedSpecimenResultDisplayConfig) {
        categoryDisplayConfig.clear();
        for (Map.Entry<String, SearchResultFieldDisplaySetting> fieldDisplaySetting : savedSpecimenResultDisplayConfig
                .entrySet()) {
            String category = fieldDisplaySetting.getValue().getFieldCategoryName();
            List<SearchResultFieldDisplaySetting> fieldSettings = categoryDisplayConfig.get(category);
            if (fieldSettings == null) {
                fieldSettings = new ArrayList<SearchResultFieldDisplaySetting>();
                categoryDisplayConfig.put(category, fieldSettings);
            }
            fieldSettings.add(fieldDisplaySetting.getValue());
        }
        for (List<SearchResultFieldDisplaySetting> fieldDisplaySetting : categoryDisplayConfig
                .values()) {
            Collections.sort(fieldDisplaySetting, new SearchResultFieldDisplaySettingComparator());
        }
    }

    private Map<String, List<SearchResultFieldDisplaySetting>> retrieveCategoryNameDisplaySettingsMap() {
        List<SearchResultFieldDisplaySetting> fieldDisplaySettings = searchResultDisplaySettingService
                .getUserSearchResultDisplaySettings(UsernameHolder.getUser());
        Map<String, List<SearchResultFieldDisplaySetting>> categoryDisplaySettingMap =
            new LinkedHashMap<String, List<SearchResultFieldDisplaySetting>>();
        for (SearchResultFieldDisplaySetting fieldDisplaySetting : fieldDisplaySettings) {
            String fieldCategoryName = fieldDisplaySetting.getFieldCategoryName();
            List<SearchResultFieldDisplaySetting> categoryFieldSettings = categoryDisplaySettingMap
                    .get(fieldCategoryName);
            if (categoryFieldSettings == null) {
                categoryFieldSettings = new ArrayList<SearchResultFieldDisplaySetting>();
                categoryDisplaySettingMap.put(fieldCategoryName, categoryFieldSettings);
            }
            categoryFieldSettings.add(fieldDisplaySetting);
        }
        return categoryDisplaySettingMap;
    }
    
    /**
     * Comparator for sorting search result display settings.
     * @author gvaughn
     *
     */
    class SearchResultFieldDisplaySettingComparator implements Comparator<SearchResultFieldDisplaySetting> {
        
        @Override
        public int compare(SearchResultFieldDisplaySetting fs1,
                SearchResultFieldDisplaySetting fs2) {
            if (fs2.getId() == null) {
                return -1;
            }
            if (fs1.getId() == null) {
                return 1;
            }
            return fs1.getId().compareTo(fs2.getId());
        }
    }

}
