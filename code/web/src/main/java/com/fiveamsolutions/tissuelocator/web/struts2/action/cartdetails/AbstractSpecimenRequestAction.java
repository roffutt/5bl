/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.struts2.action.cartdetails;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJBException;
import javax.mail.MessagingException;
import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.RequestStatus;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorFile;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.data.mta.MaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreement;
import com.fiveamsolutions.tissuelocator.data.mta.SignedMaterialTransferAgreementStatus;
import com.fiveamsolutions.tissuelocator.service.MaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SignedMaterialTransferAgreementServiceLocal;
import com.fiveamsolutions.tissuelocator.service.SpecimenRequestServiceBean;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.lob.LobHolderServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorContextParameterHelper;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorFileHelper;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorSessionHelper;
import com.google.inject.Inject;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * @author ddasgupta
 *
 */
@SuppressWarnings({ "PMD.TooManyMethods", "PMD.ExcessiveClassLength", "PMD.TooManyFields",
    "PMD.AvoidDuplicateLiterals" })
public abstract class AbstractSpecimenRequestAction extends AbstractCartSpecimenAction {

    private static final long serialVersionUID = 8833842225985370223L;

    private Set<Long> selection = new HashSet<Long>();

    private File resume;
    private String resumeFileName;
    private String resumeContentType;

    private File protocolDocument;
    private String protocolDocumentFileName;
    private String protocolDocumentContentType;

    private File irbExemptionLetter;
    private String irbExemptionLetterFileName;
    private String irbExemptionLetterContentType;

    private File irbApprovalLetter;
    private String irbApprovalLetterFileName;
    private String irbApprovalLetterContentType;

    private File submittedMtaDocument;
    private String submittedMtaDocumentFileName;
    private String submittedMtaDocumentContentType;

    private SignedMaterialTransferAgreement signedRecipientMta;
    private MaterialTransferAgreement currentMta;

    private RequestStatus previousStatus;
    private final TissueLocatorUserServiceLocal userService;
    private final LobHolderServiceLocal lobHolderService;

    /**
     * Default constructor.
     */
    public AbstractSpecimenRequestAction() {
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     * @param userService the user service
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     * @param lobHolderService the lob holder service
     */
    @Inject
    public AbstractSpecimenRequestAction(TissueLocatorUserServiceLocal userService,
            ApplicationSettingServiceLocal appSettingService,
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService,
            LobHolderServiceLocal lobHolderService) {
        super(appSettingService, uiDynamicFieldCategoryService);
        this.userService = userService;
        this.lobHolderService = lobHolderService;
    }

    /**
     * action to save an specimen.
     * @return the forward to go to.
     * @throws MessagingException on error
     */
    @Validations(
        customValidators = {
            @CustomValidator(type = "hibernate", fieldName = "object",
                parameters = { @ValidationParameter(name = "resourceKeyBase", value = "specimenRequest"),
                               @ValidationParameter(name = "excludes", value = "status")
            })
        },
        fieldExpressions = {
            @FieldExpressionValidator(fieldName = "resume",
                key = "specimenRequest.investigator.resume.required",
                expression = "resume != null || object.investigator.resume != null"),
            @FieldExpressionValidator(fieldName = "investigatorOrganizationName",
                key = "specimenRequest.investigator.organization.required",
                expression = "investigatorOrganizationName == object.investigator.organization.name"),
            @FieldExpressionValidator(fieldName = "recipientOrganizationName",
                key = "specimenRequest.shipment.recipient.organization.required",
                expression = "recipientOrganizationName == object.shipment.recipient.organization.name")
        }
    )
    public String save() throws MessagingException {
        ServletContext sc = ServletActionContext.getServletContext();
        int votingPeriod = TissueLocatorContextParameterHelper.getVotingPeriod(sc);
        int reviewPeriod = TissueLocatorContextParameterHelper.getReviewPeriod(sc);
        int reviewerAssignmentPeriod = TissueLocatorContextParameterHelper.getReviewerAssignmentPeriod(sc);
        int numberReviewers = TissueLocatorContextParameterHelper.getNumberRequestReviewers(sc);
        try {
            getService().saveRequest(getObject(), votingPeriod, reviewPeriod, reviewerAssignmentPeriod,
                    numberReviewers);
        } catch (EJBException e) {
            if (e.getCause() != null) {
                if (e.getCause().getMessage().equals(SpecimenRequestServiceBean.SPECIMEN_NOT_AVAILABLE)) {
                    setSpecimenRequestError("specimenRequest.error.unavailable");
                    return viewCart();
                } else if (e.getCause().getMessage().equals(
                        SpecimenRequestServiceBean.SPECIMEN_OR_NOTES_NOT_SELECTED)) {
                    setSpecimenRequestError("specimenRequest.error.noSpecimensOrNotes");
                    return viewCart();
                } else if (e.getCause().getMessage().equals(SpecimenRequestServiceBean.SPECIMEN_NOT_SELECTED)) {
                    setSpecimenRequestError("specimenRequest.error.noSpecimens");
                    return viewCart();
                }
            }
            throw e;
        }

        TissueLocatorSessionHelper.setCart(ServletActionContext.getRequest().getSession(), null);
        return "thankYou";
    }

    /**
     * Certify understanding of the MTA.
     * @return struts forward.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "specimenRequest"),
                                   @ValidationParameter(name = "excludes", value = "status")
                })
            })
    public String certifyMta() {
        if (getSubmittedMtaDocument() != null) {
            submitSignedMta();
        }
        return "confirm";
    }

    /**
     * Upload a signed MTA.
     * @return struts forward.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "object",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "specimenRequest"),
                                   @ValidationParameter(name = "excludes", value = "status, mtaCertification")
                })
            },
            fieldExpressions = {
                @FieldExpressionValidator(fieldName = "submittedMtaDocument",
                    key = "specimenRequest.submittedMtaDocument.required",
                    expression = "submittedMtaDocument != null")
            }
        )
    public String uploadMta() {
        submitSignedMta();
        addActionMessage(getText("specimenRequest.uploadMta.success"));
        return Action.INPUT;
    }

    private void submitSignedMta() {
        TissueLocatorUser user = userService.getByUsername(
                        UsernameHolder.getUser());
        SignedMaterialTransferAgreement mta = new SignedMaterialTransferAgreement();
        mta.setUploader(user);
        mta.setUploadTime(new Date());
        mta.setReceivingInstitution(user.getInstitution());
        mta.setStatus(
                SignedMaterialTransferAgreementStatus.PENDING_REVIEW);
        mta.setOriginalMta(getCurrentMta());

        byte[] fileContents = TissueLocatorFileHelper
            .getFileContents(getSubmittedMtaDocument());
        mta.setDocument(new TissueLocatorFile(fileContents,
                getSubmittedMtaDocumentFileName(), getSubmittedMtaDocumentContentType()));
        SignedMaterialTransferAgreementServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getSignedMaterialTransferAgreementService();

        Long id = service.saveSignedMaterialTransferAgreement(mta);
        setSignedRecipientMta(service.getPersistentObject(SignedMaterialTransferAgreement.class, id));
        setSubmittedMtaDocument(null);
        setSubmittedMtaDocumentContentType(null);
        setSubmittedMtaDocumentFileName(null);
    }

    /**
     * Action for downloading the pi resume file.
     * @return a struts forward.
     */
    @SkipValidation
    public String downloadResume() {
        return "downloadResume";
    }

    /**
     * @return the input stream used for the pi resume file download.
     */
    public InputStream getResumeStream() {
        return new ByteArrayInputStream(getObject().getInvestigator().getResume().getLob().getData());
    }

    /**
     * Action for downloading the protocol document file.
     * @return a struts forward.
     */
    @SkipValidation
    public String downloadProtocolDocument() {
        return "downloadProtocolDocument";
    }

    /**
     * @return the input stream used for the protocol document file download.
     */
    public InputStream getProtocolDocumentStream() {
        return new ByteArrayInputStream(getObject().getStudy().getProtocolDocument().getLob().getData());
    }

    /**
     * Action for downloading the irb letter of exemption file.
     * @return a struts forward.
     */
    @SkipValidation
    public String downloadIrbExemptionLetter() {
        return "downloadIrbExemptionLetter";
    }

    /**
     * @return the input stream used for the irb exemption letter file download.
     */
    public InputStream getIrbExemptionLetterStream() {
        return new ByteArrayInputStream(getObject().getStudy().getIrbExemptionLetter().getLob().getData());
    }

    /**
     * Action for download the irb approval letter file.
     * @return the struts forward
     */
    public String downloadIrbApprovalLetter() {
        return "downloadIrbApprovalLetter";
    }

    /**
     * @return the input stream used for the irb approval letter download
     */
    public InputStream getIrbApprovalLetterStream() {
        return new ByteArrayInputStream(getObject().getStudy().getIrbApprovalLetter().getLob().getData());
    }

    /**
     * @return Whether the current user's institution has a valid signed mta on file.
     */
    public boolean isSignedRecipientMtaValid() {
        return getSignedRecipientMta() != null && getSignedRecipientMta().getStatus()
            .equals(SignedMaterialTransferAgreementStatus.APPROVED);
    }

    /**
     * @return Whether the current user's institution has a pending signed mta on file.
     */
    public boolean isSignedRecipientMtaPending() {
        return getSignedRecipientMta() != null && getSignedRecipientMta().getStatus()
            .equals(SignedMaterialTransferAgreementStatus.PENDING_REVIEW);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        convertFiles();
        MaterialTransferAgreementServiceLocal service =
            TissueLocatorRegistry.getServiceLocator().getMaterialTransferAgreementService();
        setCurrentMta(service.getCurrentMta());
        TissueLocatorUser user = userService.getByUsername(UsernameHolder.getUser());
        setSignedRecipientMta(user.getInstitution().getCurrentSignedRecipientMta());
    }

    private void convertFiles() {
        TissueLocatorFile resumeFile = TissueLocatorFileHelper.getFile(getResume(), getResumeFileName(),
                getResumeContentType(), getObject().getInvestigator().getResume(), lobHolderService);
        if (resumeFile != null) {
            getObject().getInvestigator().setResume(resumeFile);
        }

        if (getProtocolDocument() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getProtocolDocument());
            getObject().getStudy().setProtocolDocument(new TissueLocatorFile(fileContents,
                    getProtocolDocumentFileName(), getProtocolDocumentContentType()));
        }

        if (getIrbExemptionLetter() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getIrbExemptionLetter());
            getObject().getStudy().setIrbExemptionLetter(new TissueLocatorFile(fileContents,
                    getIrbExemptionLetterFileName(), getIrbExemptionLetterContentType()));
        }

        if (getIrbApprovalLetter() != null) {
            byte[] fileContents = TissueLocatorFileHelper.getFileContents(getIrbApprovalLetter());
            getObject().getStudy().setIrbApprovalLetter(new TissueLocatorFile(fileContents,
                    getIrbApprovalLetterFileName(), getIrbApprovalLetterContentType()));
        }
    }

    /**
     * @return the selection
     */
    public Set<Long> getSelection() {
        return selection;
    }

    /**
     * @param selection the selection to set
     */
    public void setSelection(Set<Long> selection) {
        this.selection = selection;
    }

    /**
     * @return the resume
     */
    public File getResume() {
        return resume;
    }

    /**
     * @param resume the resume to set
     */
    public void setResume(File resume) {
        this.resume = resume;
    }

    /**
     * @return the resumeFileName
     */
    public String getResumeFileName() {
        return resumeFileName;
    }

    /**
     * @param resumeFileName the resumeFileName to set
     */
    public void setResumeFileName(String resumeFileName) {
        this.resumeFileName = resumeFileName;
    }

    /**
     * @return the resumeContentType
     */
    public String getResumeContentType() {
        return resumeContentType;
    }

    /**
     * @param resumeContentType the resumeContentType to set
     */
    public void setResumeContentType(String resumeContentType) {
        this.resumeContentType = resumeContentType;
    }

    /**
     * @return the protocolDocument
     */
    public File getProtocolDocument() {
        return protocolDocument;
    }

    /**
     * @param protocolDocument the protocolDocument to set
     */
    public void setProtocolDocument(File protocolDocument) {
        this.protocolDocument = protocolDocument;
    }

    /**
     * @return the protocolDocumentFileName
     */
    public String getProtocolDocumentFileName() {
        return protocolDocumentFileName;
    }

    /**
     * @param protocolDocumentFileName the protocolDocumentFileName to set
     */
    public void setProtocolDocumentFileName(String protocolDocumentFileName) {
        this.protocolDocumentFileName = protocolDocumentFileName;
    }

    /**
     * @return the protocolDocumentContentType
     */
    public String getProtocolDocumentContentType() {
        return protocolDocumentContentType;
    }

    /**
     * @param protocolDocumentContentType the protocolDocumentContentType to set
     */
    public void setProtocolDocumentContentType(String protocolDocumentContentType) {
        this.protocolDocumentContentType = protocolDocumentContentType;
    }

    /**
     * @return the irb exemption letter
     */
    public File getIrbExemptionLetter() {
        return irbExemptionLetter;
    }

    /**
     * @param irbExemptionLetter the irb exemption letter to set
     */
    public void setIrbExemptionLetter(File irbExemptionLetter) {
        this.irbExemptionLetter = irbExemptionLetter;
    }

    /**
     * @return the irb exemption letter file name
     */
    public String getIrbExemptionLetterFileName() {
        return irbExemptionLetterFileName;
    }

    /**
     * @param irbExemptionLetterFileName the irb exemption letter filename to set
     */
    public void setIrbExemptionLetterFileName(String irbExemptionLetterFileName) {
        this.irbExemptionLetterFileName = irbExemptionLetterFileName;
    }

    /**
     * @return the irb exemption letter content type
     */
    public String getIrbExemptionLetterContentType() {
        return irbExemptionLetterContentType;
    }

    /**
     * @param irbExemptionLetterContentType the irb exemption letter content type to set
     */
    public void setIrbExemptionLetterContentType(String irbExemptionLetterContentType) {
        this.irbExemptionLetterContentType = irbExemptionLetterContentType;
    }

    /**
     * @return the irb approval letter
     */
    public File getIrbApprovalLetter() {
        return irbApprovalLetter;
    }

    /**
     * @param irbApprovalLetter the irb approval letter to set
     */
    public void setIrbApprovalLetter(File irbApprovalLetter) {
        this.irbApprovalLetter = irbApprovalLetter;
    }

    /**
     * @return the irb approval letter name
     */
    public String getIrbApprovalLetterFileName() {
        return irbApprovalLetterFileName;
    }

    /**
     * @param irbApprovalLetterFileName the irb approval letter name to set
     */
    public void setIrbApprovalLetterFileName(String irbApprovalLetterFileName) {
        this.irbApprovalLetterFileName = irbApprovalLetterFileName;
    }

    /**
     * @return the irb approval letter content type
     */
    public String getIrbApprovalLetterContentType() {
        return irbApprovalLetterContentType;
    }

    /**
     * @param irbApprovalLetterContentType the irb approval letter content type to set
     */
    public void setIrbApprovalLetterContentType(String irbApprovalLetterContentType) {
        this.irbApprovalLetterContentType = irbApprovalLetterContentType;
    }

    /**
     * @return the submittedMta
     */
    public File getSubmittedMtaDocument() {
        return submittedMtaDocument;
    }

    /**
     * @param submittedMta the submittedMta to set
     */
    public void setSubmittedMtaDocument(File submittedMta) {
        submittedMtaDocument = submittedMta;
    }

    /**
     * @return the submittedMtaFileName
     */
    public String getSubmittedMtaDocumentFileName() {
        return submittedMtaDocumentFileName;
    }

    /**
     * @param submittedMtaFileName the submittedMtaFileName to set
     */
    public void setSubmittedMtaDocumentFileName(String submittedMtaFileName) {
        submittedMtaDocumentFileName = submittedMtaFileName;
    }

    /**
     * @return the submittedMtaContentType
     */
    public String getSubmittedMtaDocumentContentType() {
        return submittedMtaDocumentContentType;
    }

    /**
     * @param submittedMtaContentType the submittedMtaContentType to set
     */
    public void setSubmittedMtaDocumentContentType(String submittedMtaContentType) {
        submittedMtaDocumentContentType = submittedMtaContentType;
    }

    /**
     * @return the currentSignedMta
     */
    public SignedMaterialTransferAgreement getSignedRecipientMta() {
        return signedRecipientMta;
    }

    /**
     * @param signedRecipientMta the currentSignedMta to set
     */
    public void setSignedRecipientMta(SignedMaterialTransferAgreement signedRecipientMta) {
        this.signedRecipientMta = signedRecipientMta;
    }

    /**
     * @return the currentMta
     */
    public MaterialTransferAgreement getCurrentMta() {
        return currentMta;
    }

    /**
     * @param currentMta the currentMta to set
     */
    public void setCurrentMta(MaterialTransferAgreement currentMta) {
        this.currentMta = currentMta;
    }

    /**
     * @return the previous status of the current request, null if this is a new request
     */
    public RequestStatus getPreviousStatus() {
        return previousStatus;
    }

    /**
     * @param previousStatus the previous status of the current request
     */
    public void setPreviousStatus(RequestStatus previousStatus) {
        this.previousStatus = previousStatus;
    }
}
