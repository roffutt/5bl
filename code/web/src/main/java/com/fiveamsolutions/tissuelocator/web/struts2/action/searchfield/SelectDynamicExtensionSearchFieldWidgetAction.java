/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web.struts2.action.searchfield;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.data.config.search.SelectDynamicExtensionConfig;
import com.fiveamsolutions.tissuelocator.service.config.search.SearchFieldConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.web.TissueLocatorRequestHelper;
import com.fiveamsolutions.tissuelocator.web.util.SelectOption;
import com.google.inject.Inject;

/**
 * @author ddasgupta
 */
public class SelectDynamicExtensionSearchFieldWidgetAction extends
        AbstractSearchFieldWidgetAction<SelectDynamicExtensionConfig> {

    private static final long serialVersionUID = 1L;

    /**
     * Default Constructor.
     */
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    public SelectDynamicExtensionSearchFieldWidgetAction() {
        throw new UnsupportedOperationException();
    }
    
    /**
     * 
     * @param searchFieldConfigService search field config service.
     */
    @Inject
    public SelectDynamicExtensionSearchFieldWidgetAction(SearchFieldConfigServiceLocal searchFieldConfigService) {
        super(searchFieldConfigService);
    }
    
    /**
     * Return the list of select options.
     * @return the list.
     */
    public List<SelectOption> getSelectOptions() {
        List<String> labels = getSearchFieldConfig().getControlledVocabConfig().getOptions();
        List<SelectOption> opts = new ArrayList<SelectOption>();
        if (getSearchFieldConfig().isShowEmptyOption() && !getSearchFieldConfig().isMultiSelect()) {
            opts.add(new SelectOption(null, getText("search.emptyOption"), " "));
        }
        for (String label : labels) {
            opts.add(new SelectOption(null, label, label));
        }
        return opts;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<SelectDynamicExtensionConfig> getConfigType() {
        return SelectDynamicExtensionConfig.class;
    }

    /**
     * @return The value of the select config.
     */
    public Object getValue() {
        Map<AbstractSearchFieldConfig, Object> configValueMap =
            TissueLocatorRequestHelper.getSearchConfigValueMap(ServletActionContext.getRequest());
        return configValueMap.get(getSearchFieldConfig());
    }
}
