/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.web.struts2.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import com.fiveamsolutions.nci.commons.util.UsernameHolder;
import com.fiveamsolutions.tissuelocator.data.AggregateSpecimenRequestLineItem;
import com.fiveamsolutions.tissuelocator.data.SpecimenRequest;
import com.fiveamsolutions.tissuelocator.data.TissueLocatorUser;
import com.fiveamsolutions.tissuelocator.service.LineItemReviewServiceLocal;
import com.fiveamsolutions.tissuelocator.service.config.category.UiDynamicFieldCategoryServiceLocal;
import com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal;
import com.fiveamsolutions.tissuelocator.service.user.TissueLocatorUserServiceLocal;
import com.fiveamsolutions.tissuelocator.util.AggregateLineItemSeparator;
import com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry;
import com.google.inject.Inject;
import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.ValidationParameter;
import com.opensymphony.xwork2.validator.annotations.Validations;

/**
 * Action to support the line item review process.
 *
 * @author gvaughn
 *
 */
public class LineItemReviewAction extends AbstractSpecimenRequestReviewAction {

    private static final long serialVersionUID = 6839196400145962798L;

    private AggregateSpecimenRequestLineItem[] combinedSpecificLineItemsArray = new AggregateSpecimenRequestLineItem[0];
    private AggregateSpecimenRequestLineItem[] combinedGeneralLineItemsArray = new AggregateSpecimenRequestLineItem[0];
    private final Collection<AggregateSpecimenRequestLineItem> combinedSpecificLineItems =
        new ArrayList<AggregateSpecimenRequestLineItem>();
    private final Collection<AggregateSpecimenRequestLineItem> combinedGeneralLineItems =
        new ArrayList<AggregateSpecimenRequestLineItem>();

    private boolean validateRequest;
    private Long instId;

    /**
     * Default constructor.
     */
    public LineItemReviewAction() {
        // Default constructor is unsupported, guice build using the other constructor.
        // this needs to be here so xml validation by struts passes.
        throw new UnsupportedOperationException();
    }

    /**
     * Default constructor.
     * @param userService the user service.
     * @param appSettingService the application setting service
     * @param uiDynamicFieldCategoryService category service.
     */
    @Inject
    public LineItemReviewAction(TissueLocatorUserServiceLocal userService,
            ApplicationSettingServiceLocal appSettingService, 
            UiDynamicFieldCategoryServiceLocal uiDynamicFieldCategoryService) {
        super(userService, appSettingService, uiDynamicFieldCategoryService);
        setObject(new SpecimenRequest());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void prepare() {
        super.prepare();
        instId = getUserService().getByUsername(UsernameHolder.getUser()).getInstitution().getId();

        List<AggregateSpecimenRequestLineItem> lineItems = new ArrayList<AggregateSpecimenRequestLineItem>();
        lineItems.addAll(getObject().getAggregateLineItems());
        lineItems.addAll(getObject().getOrderAggregateLineItems());
        AggregateLineItemSeparator separator = new AggregateLineItemSeparator(lineItems, getAppSettingService());
        getCombinedSpecificLineItems().clear();
        getCombinedSpecificLineItems().addAll(separator.getSpecificLineItems());
        getCombinedGeneralLineItems().clear();
        getCombinedGeneralLineItems().addAll(separator.getGeneralLineItems());

        setCombinedSpecificLineItemsArray(new AggregateSpecimenRequestLineItem[getCombinedSpecificLineItems().size()]);
        setCombinedGeneralLineItemsArray(new AggregateSpecimenRequestLineItem[getCombinedGeneralLineItems().size()]);
        getCombinedSpecificLineItems().toArray(combinedSpecificLineItemsArray);
        getCombinedGeneralLineItems().toArray(combinedGeneralLineItemsArray);
    }

    /**
     * add the vote to the set and save the object.
     *
     * @return the struts forward.
     * @throws MessagingException on error.
     */
    @Validations(
            customValidators = {
                @CustomValidator(type = "hibernate", fieldName = "combinedSpecificLineItems",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "lineItemReview") }),
                @CustomValidator(type = "hibernate", fieldName = "combinedGeneralLineItems",
                        parameters = { @ValidationParameter(name = "resourceKeyBase", value = "lineItemReview") })
            }
    )
    public String saveLineItemReview() throws MessagingException {
        getReviewService().addLineItemReviews(getObject());
        addActionMessage(getText("specimenRequest.review.lineItem.success"));
        return INPUT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate() {
        super.validate();
        if (isValidateRequest()) {
            validateLineItems(getCombinedSpecificLineItems(), "combinedSpecificLineItemsArray");
            validateLineItems(getCombinedGeneralLineItems(), "combinedGeneralLineItemsArray");
        }
    }

    private void validateLineItems(Collection<AggregateSpecimenRequestLineItem> lineItems, String propName) {
        int i = 0;
        for (AggregateSpecimenRequestLineItem lineItem : lineItems) {
            if (instId.equals(lineItem.getVote().getInstitution().getId()) && lineItem.getVote().getVote() == null) {
                addFieldError(propName + "[" + i + "].vote.vote", getText("lineItemReview.vote.required"));
            }
            i++;
        }
    }

    /**
     * Adds the comment.
     * @return string forward.
     * @throws MessagingException on error
     */
    @Validations(
            customValidators =
                @CustomValidator(type = "hibernate", fieldName = "comment",
                    parameters = { @ValidationParameter(name = "resourceKeyBase", value = "comment") })
    )
    public String addComment() throws MessagingException {
        TissueLocatorUser user = getUserService().getByUsername(UsernameHolder.getUser());
        getComment().setDate(new Date());
        getComment().setUser(user);
        getReviewService().addReviewComment(getObject(), getComment());
        return SUCCESS;
    }

    /**
     * @return the combinedSpecificLineItems
     */
    public Collection<AggregateSpecimenRequestLineItem> getCombinedSpecificLineItems() {
        return combinedSpecificLineItems;
    }

    /**
     * @return the combinedGeneralLineItems
     */
    public Collection<AggregateSpecimenRequestLineItem> getCombinedGeneralLineItems() {
        return combinedGeneralLineItems;
    }

    /**
     * @return the combinedGeneralLineItemsArray
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public AggregateSpecimenRequestLineItem[] getCombinedGeneralLineItemsArray() {
        return combinedGeneralLineItemsArray;
    }

    /**
     * @param combinedGeneralLineItemsArray the combinedGeneralLineItemsArray to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setCombinedGeneralLineItemsArray(AggregateSpecimenRequestLineItem[] combinedGeneralLineItemsArray) {
        this.combinedGeneralLineItemsArray = combinedGeneralLineItemsArray;
    }

    /**
     * @return the combinedSpecificLineItemsArray
     */
    @SuppressWarnings("PMD.MethodReturnsInternalArray")
    public AggregateSpecimenRequestLineItem[] getCombinedSpecificLineItemsArray() {
        return combinedSpecificLineItemsArray;
    }

    /**
     * @param combinedSpecificLineItemsArray the combinedSpecificLineItemsArray to set
     */
    @SuppressWarnings("PMD.ArrayIsStoredDirectly")
    public void setCombinedSpecificLineItemsArray(AggregateSpecimenRequestLineItem[] combinedSpecificLineItemsArray) {
        this.combinedSpecificLineItemsArray = combinedSpecificLineItemsArray;
    }

    private LineItemReviewServiceLocal getReviewService() {
        return TissueLocatorRegistry.getServiceLocator().getLineItemReviewService();
    }

    /**
     * @return the validateRequest
     */
    public boolean isValidateRequest() {
        return validateRequest;
    }

    /**
     * @param validateRequest the validateRequest to set
     */
    public void setValidateRequest(boolean validateRequest) {
        this.validateRequest = validateRequest;
    }
}
