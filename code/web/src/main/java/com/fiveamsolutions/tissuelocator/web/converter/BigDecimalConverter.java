/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.web.converter;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.util.StrutsTypeConverter;

import com.opensymphony.xwork2.conversion.TypeConversionException;

/**
 * Converter for BigDecimal objects.  This differs from the built in converter in that this converter
 * will not attempt to convert null or the empty string into a BigDecimal.  new BigDecimal(null) or
 * new BigDecimal("") throws NumberFormatException in the built in converter, which causes the param
 * setting process to exit for BigDecimal fields.  To avoid this, this converter will return null in
 * that case rather than attempting the conversion.
 *
 * In addition, this differs from the built in converter in that it properly handles conversions of
 * strings with grouping separators, like commas, by parsing the string using the default number format
 * for the current locale.
 *
 * @author ddasgupta
 *
 */
public class BigDecimalConverter extends StrutsTypeConverter {

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings({ "rawtypes" })
    public Object convertFromString(Map context, String[] values, Class toClass) {
        if (StringUtils.isBlank(values[0])) {
            return null;
        }

        NumberFormat numFormat = NumberFormat.getInstance();
        numFormat.setGroupingUsed(true);
        ParsePosition parsePos = new ParsePosition(0);
        Number number = numFormat.parse(values[0], parsePos);
        if (parsePos.getIndex() != values[0].length()) {
            throw new TypeConversionException("Unparseable number: \"" + values[0] + "\" at position "
                    + parsePos.getIndex());
        }

        return new BigDecimal(number.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("rawtypes")
    public String convertToString(Map context, Object o) {
        String retVal = "";
        if (o instanceof BigDecimal) {
            BigDecimal bd = (BigDecimal) o;
            retVal = bd.toString();
        }
        return retVal;
    }

}
