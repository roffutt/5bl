/**
    * Copyright (c) 2009, 5AM Solutions, Inc.
    * All rights reserved.
      *
    * Redistribution and use in source and binary forms, with or without
    * modification, are permitted provided that the following conditions are met:
      *
    * - Redistributions of source code must retain the above copyright notice,
    * this list of conditions and the following disclaimer.
    *
    * - Redistributions in binary form must reproduce the above copyright notice,
    * this list of conditions and the following disclaimer in the documentation
    * and/or other materials provided with the distribution.
    *
    * - Neither the name of the author nor the names of its contributors may be
    * used to endorse or promote products derived from this software without
    * specific prior written permission.
    *
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    * POSSIBILITY OF SUCH DAMAGE.
*/

package com.fiveamsolutions.tissuelocator.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.fiveamsolutions.tissuelocator.data.config.search.AbstractSearchFieldConfig;
import com.fiveamsolutions.tissuelocator.web.struts2.action.AbstractPersistentObjectAction.PaginatedSearchHelper;

/**
 * @author gvaughn
 *
 */
public class TissueLocatorRequestHelper {

    private static final String CONFIG_VALUE_MAP_NAME = "searchConfigValueMap";
    private static final String PAGINATED_SEARCH_HELPER_NAME = "paginatedSearchHelper";
    
    /**
     * Set the config value map in the request.
     * @param request The request.
     * @param configValueMap The map to be set.
     */
    public static final void setSearchConfigValueMap(HttpServletRequest request, 
            Map<AbstractSearchFieldConfig, Object> configValueMap) {
        request.setAttribute(CONFIG_VALUE_MAP_NAME, configValueMap);
    }
    
    /**
     * Get the config value map.
     * @param request The request.
     * @return The config value map.
     */
    @SuppressWarnings("unchecked")
    public static final Map<AbstractSearchFieldConfig, Object> getSearchConfigValueMap(HttpServletRequest request) {
        return (Map<AbstractSearchFieldConfig, Object>) request.getAttribute(CONFIG_VALUE_MAP_NAME);
    }
    
    /**
     * Set the paginated search helper in the request.
     * @param request The request.
     * @param helper The helper to be set.
     */
    @SuppressWarnings("rawtypes")
    public static final void setPaginatedSearchHelper(HttpServletRequest request, PaginatedSearchHelper helper) {
        request.setAttribute(PAGINATED_SEARCH_HELPER_NAME, helper);
    }
    
    /**
     * Get the paginated search helper.
     * @param request The request.
     * @return The search helper.
     */
    @SuppressWarnings("rawtypes")
    public static final PaginatedSearchHelper getPaginatedSearchHelper(HttpServletRequest request) {
        return (PaginatedSearchHelper) request.getAttribute(PAGINATED_SEARCH_HELPER_NAME);
    }
}
