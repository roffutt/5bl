<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.fiveamsolutions.tissuelocator.web.listener.HomePageDataCacheRefreshTask"%>
<%@page import="com.fiveamsolutions.tissuelocator.web.TissueLocatorContextParameterHelper"%>
<%@page import="com.fiveamsolutions.tissuelocator.web.listener.ScheduledRemindersTask"%>
<%@page import="com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceLocal"%>
<%@page import="com.fiveamsolutions.tissuelocator.service.setting.ApplicationSettingServiceBean"%>
<%@page import="com.fiveamsolutions.tissuelocator.util.TissueLocatorRegistry"%>
<%@page import="com.fiveamsolutions.tissuelocator.util.TissueLocatorHibernateUtil"%>
<%@page import="com.fiveamsolutions.tissuelocator.inject.GuiceInjectorHolder"%>

<%
try {
    TissueLocatorHibernateUtil.getHibernateHelper().openAndBindSession();

    ApplicationSettingServiceLocal settingService =
        GuiceInjectorHolder.getInjector().getInstance(ApplicationSettingServiceBean.class);
    ServletContext context = request.getSession().getServletContext();

    Set<String> widgets = new HashSet<String>();
    widgets.add(settingService.getLeftBrowseWidget());
    widgets.add(settingService.getCenterBrowseWidget());
    widgets.add(settingService.getRightBrowseWidget());
    widgets.add(settingService.getLeftHomeWidget());
    widgets.add(settingService.getCenterHomeWidget());
    widgets.add(settingService.getRightHomeWidget());
    HomePageDataCacheRefreshTask task = new HomePageDataCacheRefreshTask(context, widgets);
    task.run();
} finally {
    TissueLocatorHibernateUtil.getHibernateHelper().unbindAndCleanupSession();
}
%>

<html>
<head>
    <title>Home Page Data Cache Updated</title>
</head>
<body>
    Home Page Data Cache Updated
</body>
</html>
