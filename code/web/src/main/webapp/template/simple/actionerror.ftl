<#--
    updating actionerror in simple theme to not use ul's and li's
-->
<#if (actionErrors?? && actionErrors?size > 0)>
    <div>
        <#list actionErrors as error>
            <span
                <#if parameters.cssClass??>
                    class="${parameters.cssClass?html}"<#rt/>
                <#else>
                    class="errorMessage"<#rt/>
                </#if>
                <#if parameters.cssStyle??>
                    style="${parameters.cssStyle?html}"<#rt/>
                </#if>
            >${error!}</span><br/>
        </#list>
    </div>
</#if>
