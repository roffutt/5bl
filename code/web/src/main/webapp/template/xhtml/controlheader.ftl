<#--
    Simplifying header from xhtml theme to not include so many block level elements
-->
<#--
  Only show message if errors are available.
  This will be done if ActionSupport is used.
-->
<#assign hasFieldErrors = parameters.name?? && fieldErrors?? && fieldErrors[parameters.name]??/>
<#--
  if the label position is top,
  then give the label it's own row in the table
-->
<tr>
<#if parameters.labelposition?default("") == 'top'>
    <td align="left" valign="top" colspan="2"><#rt/>
<#else>
    <td class="tdLabel"><#rt/>
</#if>
<#if parameters.label??>
    <label <#t/>
<#if parameters.id??>
        for="${parameters.id?html}" <#t/>
</#if>
        class="label" <#t/>
        style="font-style:normal;" <#t>
    ><#t/>
<#if parameters.required?default(false) && parameters.requiredposition?default("right") != 'right'>
        <span class="reqd">*</span><#t/>
</#if>
${parameters.label?html}<#t/>
<#if parameters.required?default(false) && parameters.requiredposition?default("right") == 'right'>
 <span class="reqd">*</span><#t/>
</#if>
${parameters.labelseparator?default(":")?html}<#t/>
<#include "/${parameters.templateDir}/xhtml/tooltip.ftl" />
</label><#t/>
</#if>
    </td><#lt/>
<#-- add the extra row -->
<#if parameters.labelposition?default("") == 'top'>
</tr>
<tr>
</#if>

    <td
<#if parameters.align??>
    align="${parameters.align?html}"<#t/>
</#if>
><#t/>
