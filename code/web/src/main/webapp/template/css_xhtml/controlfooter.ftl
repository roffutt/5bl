<#--
    Simplifying footer from css_xhtml theme to not include so many block level elements
-->
${parameters.after?if_exists}<#t/>
    <#lt/>
</div>

<#if hasFieldErrors>
    <div <#rt/><#if parameters.id??>id="wwerr_${parameters.id}"<#rt/></#if> class="wwerr">
    <#list fieldErrors[parameters.name] as error>
        <div<#rt/>
        <#if parameters.id??>
            errorFor="${parameters.id}"<#rt/>
        </#if>
        class="fielderror">
             ${error?html}
        </div><#t/>
    </#list>
    </div><#t/>
</#if>
