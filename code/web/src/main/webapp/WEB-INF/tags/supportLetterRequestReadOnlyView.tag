<%@ tag body-content="empty" %>
<%@ attribute name="backUrl" required="true" %>
<%@ attribute name="statusResourceKey" required="true" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>
<html>
<head>
    <title><fmt:message key="supportLetterRequest.view.title" /></title>
</head>
<body>
    <c:url value="${backUrl}" var="listUrl"/>
    <div class="topbtns">
        <a href="${listUrl}" class="btn">
            &laquo; <fmt:message key="supportLetterRequest.review.back"/>
        </a>
    </div>

    <tissuelocator:supportLetterRequestDetails/>

    <s:set var="onlineEnum" value="%{@com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus@RESPONDED}"/>
    <s:set var="offlineEnum"
        value="%{@com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus@RESPONDED_OFFLINE}"/>

    <div class="box pad10">
        <h2 class="formtop noline nobg"><fmt:message key="supportLetterRequest.response.title"/></h2>

        <h4 class="green_bar"><fmt:message key="supportLetterRequest.view.response.title"/></h4>

        <div class="col250">
            <b><fmt:message key="supportLetterRequest.status"/></b><br />
            <fmt:message key="${statusResourceKey}"/>
        </div>

        <s:if test="%{object.status == #onlineEnum}">
            <div class="col250">
                <c:url value="/protected/downloadFile.action" var="downloadLetterUrl">
                    <c:param name="file.id" value="${object.letter.lob.id}" />
                    <c:param name="fileName" value="${object.letter.name}" />
                    <c:param name="contentType" value="${object.letter.contentType}" />
                </c:url>
                <b><fmt:message key="supportLetterRequest.letter"/></b>
                <a href="${downloadLetterUrl}" class="file">
                    ${object.letter.name}
                </a>
            </div>

            <div class="clear"></div>

            <div class="pad10">
                <b><fmt:message key="supportLetterRequest.response"/></b>
                <p style="width:100%; max-width:none;">
                    <s:property value="%{object.response}"/>
                </p>
            </div>
        </s:if>
        <s:elseif test="%{object.status == #offlineEnum}">
            <div class="clear"></div>

            <div class="pad10">
                <b><fmt:message key="supportLetterRequest.offlineResponseNotes"/></b>
                <p style="width:100%; max-width:none;">
                    <s:property value="%{object.offlineResponseNotes}"/>
                </p>
            </div>
        </s:elseif>

        <div class="clear"></div>

    </div>
</body>
</html>
