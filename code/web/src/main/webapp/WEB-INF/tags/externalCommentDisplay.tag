<%@ tag body-content="empty" %>
<%@ attribute name="specimenRequest" required="true" type="com.fiveamsolutions.tissuelocator.data.SpecimenRequest" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${!empty object.externalComment}">
    <div class="topcomment">
        <h2><fmt:message key="specimenRequest.edit.decision.header"/></h2>
        <p><fmt:message key="specimenRequest.edit.decision.externalComment.header"/></p>
        <p class="comment">${object.externalComment}</p>
    </div>
    <br/>
</c:if>

