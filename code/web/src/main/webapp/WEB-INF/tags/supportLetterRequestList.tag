<%@ tag body-content="empty" %>
<%@ attribute name="isAdmin" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<tissuelocator:messages/>

<s:set name="pendingStatus" value="%{@com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus@PENDING}" />
<s:set var="filterActionNamespace" value="/protected/support/letter"/>
<c:set var="tableURI" value="/protected/support/letter/list.action"/>
<c:if test="${isAdmin == 'true' }">
    <s:set var="filterActionNamespace" value="/admin/support/letter"/>
    <c:set var="tableURI" value="/admin/support/letter/list.action"/>
</c:if>

<s:form id="filterForm" namespace="%{#filterActionNamespace}" action="filter.action">
    <!--Filters-->
    <div class="roundbox_gray_wrapper">
        <div class="roundbox_gray">
            <div class="float_left">
            </div>

            <div class="float_right">
                <label class="inline" for="status">
                  <strong><fmt:message key="supportLetterRequest.list.filter.status"/></strong>
                </label>
                <c:choose>
                    <c:when test="${isAdmin == 'true' }">
                        <s:select name="object.status" id="status"
                            onchange="submit()" theme="simple"
                            list="%{@com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus@values()}"
                            listValue="%{getText(administratorResourceKey)}"
                            headerKey="" headerValue="%{getText('supportRequestStatus.all')}" />
                    </c:when>
                    <c:otherwise>
                        <s:select name="object.status" id="status"
                            onchange="submit()" theme="simple"
                            list="%{@com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus@values()}"
                            listValue="%{getText(researcherResourceKey)}"
                            headerKey="" headerValue="%{getText('supportRequestStatus.all')}" />
                    </c:otherwise>
                </c:choose>
                <s:if test="%{object.requestor != null}">
                    <s:hidden name="object.requestor" value="%{object.requestor.id}"/>
                </s:if>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
    <fmt:message key="default.date.format" var="dateFormat"/>
    <display:table name="objects" requestURI="${tableURI}"
            uid="supportLetterRequest" pagesize="${tablePageSize}" sort="external" export="true">
            <tissuelocator:displaytagProperties/>

        <display:column titleKey="supportLetterRequest.id" sortProperty="ID" sortable="true">
            ${supportLetterRequest.id}
            <c:set var="viewUrlBase" value="/protected/support/letter/view.action"/>
            <c:if test="${isAdmin == 'true' }">
                <c:set var="viewUrlBase" value="/admin/support/letter/view.action"/>
            </c:if>
            <c:url var="viewUrl" value="${viewUrlBase}">
                <c:param name="object.id" value="${supportLetterRequest.id}"/>
            </c:url>
            <a href="${viewUrl}"><fmt:message key="btn.view"/></a>
        </display:column>
        <display:column titleKey="supportLetterRequest.requestorName"
            sortProperty="REQUESTOR_LAST_NAME,REQUESTOR_FIRST_NAME" sortable="true">
            ${supportLetterRequest.requestor.displayName}
        </display:column>
        <display:column titleKey="supportLetterRequest.requestorInstitution" property="requestor.institution.name"
            sortProperty="REQUESTOR_INSTITUTION" sortable="true"/>
        <display:column titleKey="supportLetterRequest.createdDate" sortProperty="CREATED_DATE" sortable="true">
            <fmt:formatDate pattern="${dateFormat}" value="${supportLetterRequest.createdDate}"/>
        </display:column>
        <display:column titleKey="supportLetterRequest.lastUpdatedDate" sortProperty="UPDATED_DATE" sortable="true">
            <fmt:formatDate pattern="${dateFormat}" value="${supportLetterRequest.lastUpdatedDate}"/>
        </display:column>
        <display:column titleKey="supportLetterRequest.status" sortProperty="STATUS" sortable="true">
            <s:set var="statusKey" value="%{#attr.supportLetterRequest.status.researcherResourceKey}"/>
            <c:if test="${isAdmin == 'true' }">
                <s:set var="statusKey" value="%{#attr.supportLetterRequest.status.administratorResourceKey}"/>
            </c:if>
            <s:property value="%{getText(#statusKey)}"/>
        </display:column>

        <c:if test="${isAdmin == 'true' }">
            <req:isUserInRole role="supportLetterRequestReviewer">
                <display:column titleKey="column.actionRequired">
                    <s:if test="%{#attr.supportLetterRequest.status == #pendingStatus}">
                        <fmt:message key="supportLetterRequest.review"/>
                    </s:if>
                </display:column>
                <display:column titleKey="column.action" headerClass="action" class="action">
                    <s:if test="%{#attr.supportLetterRequest.status == #pendingStatus}">
                        <c:url var="reviewUrl" value="/admin/support/letter/input.action">
                            <c:param name="object.id" value="${supportLetterRequest.id}"/>
                        </c:url>
                        <a href="${reviewUrl}" class="btn_fw_action_col"><fmt:message key="btn.review" /></a>
                    </s:if>
                </display:column>
            </req:isUserInRole>
        </c:if>
    </display:table>
</s:form>
