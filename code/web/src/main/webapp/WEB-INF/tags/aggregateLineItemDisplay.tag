<%@ tag body-content="empty" %>
<%@ attribute name="displayVotingResults" rtexprvalue="true" required="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set var="hasAggregateOrderLineItems" value="%{!object.orderAggregateLineItems.isEmpty()}" scope="page"/>
<s:set var="hasReviewedLineItems" value="%{!object.reviewedAggregateLineItems.isEmpty()}" scope="page"/>
<s:set var="hasUnreviewedLineItems" value="%{!object.unreviewedAggregateLineItems.isEmpty()}" scope="page"/>
<c:if test="${((hasAggregateOrderLineItems || hasReviewedLineItems) && displayVotingResults == 'true') || hasUnreviewedLineItems}">
    <table class="data" id="aggregateLineItemTable">

        <c:if test="${hasUnreviewedLineItems}">
            <tr id="unreviewed_line_item_title">
                <td colspan="6">
                    <h2 class="noline" style="padding-top: 10px; padding-bottom: 0px">
                        <fmt:message key="specimenRequest.lineItems.unreviewed"/>
                    </h2>
                </td>
            </tr>

            <s:set var="specificLineItems" value="%{unreviewedSpecificLineItems}"/>
            <s:set var="generalLineItems" value="%{unreviewedGeneralLineItems}"/>
            <tissuelocator:aggregateCartTable specificLineItems="${specificLineItems}"
                generalLineItems="${generalLineItems}" showFinalPriceColumn="false"
                showOrderFulfillmentForms="false" cartPrefix="unreviewed"
                displayVotingResults="${displayVotingResults}" wrapped="false"/>
        </c:if>

        <c:if test="${hasAggregateOrderLineItems && displayVotingResults == 'true'}">
            <tr id="order_line_item_title">
                <td colspan="6">
                    <h2 class="noline" style="padding-top: 10px; padding-bottom: 0px">
                        <fmt:message key="specimenRequest.lineItems.approved"/>
                    </h2>
                </td>
            </tr>

            <s:set var="orderSpecificLineItems" value="%{orderSpecificLineItems}"/>
            <s:set var="orderGeneralLineItems" value="%{orderGeneralLineItems}"/>
            <tissuelocator:aggregateCartTable specificLineItems="${orderSpecificLineItems}"
                generalLineItems="${orderGeneralLineItems}" showFinalPriceColumn="false"
                showOrderFulfillmentForms="false" cartPrefix="order"
                displayVotingResults="true" wrapped="false"/>
        </c:if>

        <c:if test="${hasReviewedLineItems && displayVotingResults == 'true'}">
            <tr id="reviewed_line_item_title">
                <td colspan="6">
                    <h2 class="noline" style="padding-top: 10px; padding-bottom: 0px">
                        <fmt:message key="specimenRequest.lineItems.reviewed"/>
                    </h2>
                </td>
            </tr>
            <s:set var="specificLineItems" value="%{reviewedSpecificLineItems}"/>
            <s:set var="generalLineItems" value="%{reviewedGeneralLineItems}"/>
            <tissuelocator:aggregateCartTable specificLineItems="${specificLineItems}"
                generalLineItems="${generalLineItems}" showFinalPriceColumn="false"
                showOrderFulfillmentForms="false" cartPrefix="reviewed"
                displayVotingResults="true" wrapped="false"/>
        </c:if>
    </table>
</c:if>
