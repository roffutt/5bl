<%@ tag body-content="empty" %>
<%@ attribute name="selected" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<s:if test="%{displayRequestProcessSteps}">
    <c:set var="leftCapImageSuffix" value=""/>
    <c:set var="searchImageSuffix" value=""/>
    <c:set var="searchArrowImageSuffix" value=""/>
    <c:set var="selectImageSuffix" value=""/>
    <c:set var="selectArrowImageSuffix" value=""/>
    <c:set var="requestImageSuffix" value=""/>
    <c:set var="requestArrowImageSuffix" value=""/>
    <c:set var="proposalImageSuffix" value=""/>
    <c:set var="proposalArrowImageSuffix" value=""/>
    <c:set var="confirmImageSuffix" value=""/>
    <c:set var="confirmArrowImageSuffix" value=""/>
    <c:set var="receiptImageSuffix" value=""/>
    <c:set var="rightCapImageSuffix" value=""/>
    <c:choose>
        <c:when test="${selected == 'search'}">
            <c:set var="leftcapImageSuffix" value="_selected"/>
            <c:set var="searchImageSuffix" value="_selected"/>
            <c:set var="searchArrowImageSuffix" value="_selected_left"/>
        </c:when>
        <c:when test="${selected == 'select'}">
            <c:set var="searchArrowImageSuffix" value="_selected_right"/>
            <c:set var="selectImageSuffix" value="_selected"/>
            <c:set var="selectArrowImageSuffix" value="_selected_left"/>
        </c:when>
        <c:when test="${selected == 'request'}">
            <c:set var="selectArrowImageSuffix" value="_selected_right"/>
            <c:set var="requestImageSuffix" value="_selected"/>
            <c:set var="requestArrowImageSuffix" value="_selected_left"/>
        </c:when>
        <c:when test="${selected == 'proposal'}">
            <c:set var="requestArrowImageSuffix" value="_selected_right"/>
            <c:set var="proposalImageSuffix" value="_selected"/>
            <c:set var="proposalArrowImageSuffix" value="_selected_left"/>
        </c:when>
        <c:when test="${selected == 'confirm'}">
            <c:set var="proposalArrowImageSuffix" value="_selected_right"/>
            <c:set var="confirmImageSuffix" value="_selected"/>
            <c:set var="confirmArrowImageSuffix" value="_selected_left"/>
        </c:when>
        <c:when test="${selected == 'receipt'}">
            <c:set var="confirmArrowImageSuffix" value="_selected_right"/>
            <c:set var="receiptImageSuffix" value="_selected"/>
            <c:set var="rightcapImageSuffix" value="_left_selected"/>
        </c:when>
    </c:choose>

    <div id="process_steps">
        <c:url value="/images/process_steps_images/ps_leftcap${leftcapImageSuffix}.gif" var="leftcapArrowImageUrl"/>
        <div id="process_steps_leftcap${leftcapImageSuffix}" class="leftcap">
            <img src="${leftcapArrowImageUrl}" alt="" />
        </div>

        <c:url value="/images/process_steps_images/ps_search${searchImageSuffix}.gif" var="searchImageUrl"/>
        <fmt:message key="specimenRequest.processSteps.search" var="searchAltText"/>
        <div id="process_steps_search${searchImageSuffix}" class="process_step">
            <img src="${searchImageUrl}" alt="${searchAltText}" />
        </div>
        <c:url value="/images/process_steps_images/ps_arrow${searchArrowImageSuffix}.gif" var="searchArrowImageUrl"/>
        <div id="process_steps_search_arrow${searchArrowImageSuffix}" class="process_arrow">
            <img src="${searchArrowImageUrl}" alt="" />
        </div>

        <c:url value="/images/process_steps_images/ps_select${selectImageSuffix}.gif" var="selectImageUrl"/>
        <fmt:message key="specimenRequest.processSteps.select" var="selectAltText"/>
        <div id="process_steps_select${selectImageSuffix}" class="process_step">
            <img src="${selectImageUrl}" alt="${selectAltText}" />
        </div>
        <c:url value="/images/process_steps_images/ps_arrow${selectArrowImageSuffix}.gif" var="selectArrowImageUrl"/>
        <div id="process_steps_select_arrow${selectArrowImageSuffix}" class="process_arrow">
            <img src="${selectArrowImageUrl}" alt="" />
        </div>

        <c:url value="/images/process_steps_images/ps_request${requestImageSuffix}.gif" var="requestImageUrl"/>
        <fmt:message key="specimenRequest.processSteps.request" var="requestAltText"/>
        <div id="process_steps_request${requestImageSuffix}" class="process_step">
            <img src="${requestImageUrl}" alt="${requestAltText}" />
        </div>
        <c:url value="/images/process_steps_images/ps_arrow${requestArrowImageSuffix}.gif" var="requestArrowImageUrl"/>
        <div id="process_steps_request_arrow${requestArrowImageSuffix}" class="process_arrow">
            <img src="${requestArrowImageUrl}" alt="" />
        </div>

        <c:url value="/images/process_steps_images/ps_proposal${proposalImageSuffix}.gif" var="proposalImageUrl"/>
        <fmt:message key="specimenRequest.processSteps.proposal" var="proposalAltText"/>
        <div id="process_steps_proposal${proposalImageSuffix}" class="process_step">
            <img src="${proposalImageUrl}" alt="${proposalAltText}" />
        </div>
        <c:url value="/images/process_steps_images/ps_arrow${proposalArrowImageSuffix}.gif" var="proposalArrowImageUrl"/>
        <div id="process_steps_proposal_arrow${proposalArrowImageSuffix}" class="process_arrow">
            <img src="${proposalArrowImageUrl}" alt="" />
        </div>

        <c:url value="/images/process_steps_images/ps_confirm${confirmImageSuffix}.gif" var="confirmImageUrl"/>
        <fmt:message key="specimenRequest.processSteps.confirm" var="confirmAltText"/>
        <div id="process_steps_confirm${confirmImageSuffix}" class="process_step">
            <img src="${confirmImageUrl}" alt="${confirmAltText}" />
        </div>
        <c:url value="/images/process_steps_images/ps_arrow${confirmArrowImageSuffix}.gif" var="confirmArrowImageUrl"/>
        <div id="process_steps_confirm_arrow${confirmArrowImageSuffix}" class="process_arrow">
            <img src="${confirmArrowImageUrl}" alt="" />
        </div>

        <c:url value="/images/process_steps_images/ps_receipt${receiptImageSuffix}.gif" var="receiptImageUrl"/>
        <fmt:message key="specimenRequest.processSteps.receipt" var="receiptAltText"/>
        <div id="process_steps_receipt${receiptImageSuffix}" class="process_step">
            <img src="${receiptImageUrl}" alt="${receiptAltText}" />
        </div>

        <c:url value="/images/process_steps_images/ps_rightcap${rightcapImageSuffix}.gif" var="rightcapArrowImageUrl"/>
        <div id="process_steps_rightcap${rightcapImageSuffix}" class="rightcap">
            <img src="${rightcapArrowImageUrl}" alt="" />
        </div>
        <div class="clear"></div>
    </div>
</s:if>
