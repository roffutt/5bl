<%@ tag body-content="empty" %>
<%@ attribute name="isAdmin" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<tissuelocator:messages/>

<s:set var="filterActionNamespace" value="/protected/support/question"/>
<c:set var="tableURI" value="/protected/support/question/list.action"/>
<c:if test="${isAdmin == 'true' }">
    <s:set var="filterActionNamespace" value="/admin/support/question"/>
    <c:set var="tableURI" value="/admin/support/question/list.action"/>
</c:if>

<s:form id="filterForm" namespace="%{#filterActionNamespace}" action="filter.action">
    <!--Filters-->
    <div class="roundbox_gray_wrapper">
        <div class="roundbox_gray">
            <div class="float_left">
            </div>

            <div class="float_right">
                <label class="inline" for="status">
                  <strong><fmt:message key="supportLetterRequest.list.filter.status"/></strong>
                </label>
                <c:choose>
                    <c:when test="${isAdmin == 'true' }">
                        <s:select name="object.status" id="status"
                            onchange="submit()" theme="simple"
                            list="%{@com.fiveamsolutions.tissuelocator.data.support.QuestionStatus@values()}"
                            listValue="%{getText(administratorResourceKey)}"
                            headerKey="" headerValue="%{getText('questionStatus.all')}" />
                    </c:when>
                    <c:otherwise>
                        <s:select name="object.status" id="status"
                            onchange="submit()" theme="simple"
                            list="%{@com.fiveamsolutions.tissuelocator.data.support.QuestionStatus@values()}"
                            listValue="%{getText(researcherResourceKey)}"
                            headerKey="" headerValue="%{getText('questionStatus.all')}" />
                    </c:otherwise>
                </c:choose>
                <s:if test="%{object.requestor != null}">
                    <s:hidden name="object.requestor" value="%{object.requestor.id}"/>
                </s:if>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
    <fmt:message key="default.date.format" var="dateFormat"/>
    <display:table name="objects" requestURI="${tableURI}"
            uid="question" pagesize="${tablePageSize}" sort="external" export="true">
            <tissuelocator:displaytagProperties/>

        <display:column titleKey="question.id" sortProperty="ID" sortable="true">
            ${question.id}
            <c:set var="viewUrlBase" value="/protected/support/question/view.action"/>
            <c:if test="${isAdmin == 'true' }">
                <c:set var="viewUrlBase" value="/admin/support/question/view.action"/>
            </c:if>
            <c:url var="viewUrl" value="${viewUrlBase}">
                <c:param name="object.id" value="${question.id}"/>
            </c:url>
            <a href="${viewUrl}"><fmt:message key="btn.view"/></a>
        </display:column>
        <display:column titleKey="question.requestorName"
            sortProperty="REQUESTOR_LAST_NAME,REQUESTOR_FIRST_NAME" sortable="true">
            ${question.requestor.displayName}
        </display:column>
        <display:column titleKey="question.requestorInstitution" property="requestor.institution.name"
            sortProperty="REQUESTOR_INSTITUTION" sortable="true"/>
        <display:column titleKey="question.institutions" property="respondingInstitutionNames"/>
        <display:column titleKey="question.createdDate" sortProperty="CREATED_DATE" sortable="true">
            <fmt:formatDate pattern="${dateFormat}" value="${question.createdDate}"/>
        </display:column>
        <display:column titleKey="question.lastUpdatedDate" sortProperty="UPDATED_DATE" sortable="true">
            <fmt:formatDate pattern="${dateFormat}" value="${question.lastUpdatedDate}"/>
        </display:column>
        <display:column titleKey="question.status" sortProperty="STATUS" sortable="true">
            <c:choose>
                <c:when test="${isAdmin == 'true' }">
                    <fmt:message key="${question.status.administratorResourceKey}"/>
                </c:when>
                <c:otherwise>
                    <fmt:message key="${question.status.researcherResourceKey}"/>
                </c:otherwise>
            </c:choose>
        </display:column>

        <c:if test="${isAdmin == 'true' }">
            <req:isUserInRole role="questionAdministrator">
                <display:column titleKey="column.action" headerClass="action" class="action">
                    <c:url var="reviewUrl" value="/admin/support/question/input.action">
                        <c:param name="object.id" value="${question.id}"/>
                    </c:url>
                    <a href="${reviewUrl}" class="btn_fw_action_col"><fmt:message key="btn.review" /></a>
                </display:column>
            </req:isUserInRole>
        </c:if>
    </display:table>
</s:form>
