<%@ tag body-content="empty" %>
<%@ attribute name="orderLineItems" required="true" type="java.util.Collection" rtexprvalue="true"%>
<%@ attribute name="titleKey" required="true" %>
<%@ attribute name="prefix" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<c:set var="tableCellCount" value="${4}"/>
<req:isUserInRole role="priceViewer">
    <c:set var="tableCellCount" value="${tableCellCount + 1}"/>
</req:isUserInRole>

<tr id="${prefix}_title">
    <td colspan="${tableCellCount}">
        <h2 class="noline">
            <fmt:message key="${titleKey}"/>
        </h2>
    </td>
</tr>

<tr id="${prefix}_header">
    <th><div><fmt:message key="aggregateCart.institution"/></div></th>
    <th><div><fmt:message key="aggregateCart.quantity"/></div></th>
    <th><div><fmt:message key="aggregateCart.criteria"/></div></th>
    <th><div><fmt:message key="aggregateCart.note"/></div></th>

    <req:isUserInRole role="priceViewer">
        <th><div><fmt:message key="specimenRequest.finalFee"/></div></th>
    </req:isUserInRole>
</tr>

<c:forEach items="${orderLineItems}" var="lineItem" varStatus="lineItemStatus">
    <tr id="${prefix}_data${lineItemStatus.index}">
        <td>${lineItem.institution.name}</td>
        <td><fmt:formatNumber value="${lineItem.quantity}"/></td>
        <td>${lineItem.criteria}</td>
        <td>${lineItem.note}</td>

        <req:isUserInRole role="priceViewer">
            <td class="action" style="white-spqce:nowrap">
                <c:if test="${not empty lineItem.finalPrice}">
                    <fmt:formatNumber value="${lineItem.finalPrice}" type="currency" />
                    <c:set var="finalPriceTotal" value="${finalPriceTotal + lineItem.finalPrice}" scope="request"/>
                </c:if>
        </req:isUserInRole>
    </tr>
</c:forEach>
