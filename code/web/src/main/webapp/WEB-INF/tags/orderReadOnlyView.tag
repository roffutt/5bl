<%@ tag body-content="empty" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<c:set var="isShipmentAdmin" value="${false}"/>
<req:isUserInRole role="shipmentAdmin">
    <c:set var="isShipmentAdmin" value="${true}"/>
</req:isUserInRole>
<c:set var="isReceived" value="${false}"/>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@SHIPPED}" name="shipped"/>
<s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@RECEIVED}" name="received"/>
<s:if test="%{#shipped.equals(object.status) || #received.equals(object.status)}">
    <c:set var="isReceived" value="${true}"/>
</s:if>
<c:set var="showReceiptQuality" value="${isShipmentAdmin && isReceived}"/>

<s:if test="%{displayAggregateSearchResults}">
    <s:set var="specificLineItems" value="%{specificLineItemList}"/>
    <s:set var="generalLineItems" value="%{generalLineItemList}"/>
    <tissuelocator:aggregateCartTable specificLineItems="${specificLineItems}" generalLineItems="${generalLineItems}" showFinalPriceColumn="${object.readyForResearcherReview}" showOrderFulfillmentForms="false"
        order="${object}" showReceiptQuality="${showReceiptQuality}" cartPrefix="aggregate"/>
</s:if>
<s:else>
    <tissuelocator:cartTable showFinalPriceColumn="${object.readyForResearcherReview}" showOrderFulfillmentForms="false"
        requestLineItems="${object.lineItems}" showSpecimenLinks="true" showPriceRange="${!showReceiptQuality}" showReceiptQuality="${showReceiptQuality}" order="${object}"/>

</s:else>

<h2 class="noline">
    <a href="javascript:;" onclick="toggleVisibility('study_details');" class="toggle">
        <fmt:message key="shipment.studyDetails"/>
    </a>
</h2>

<div id="study_details">
<c:set var="specimenRequest" value="${object.request}"/>

        <s:set name="yesEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@YES}" />
        <s:set name="noEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@NO}" />
        <s:set name="naEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@NOT_APPLICABLE}" />
        <div class="formcol" style="width:300px">
            <label for="pi"><fmt:message key="specimenRequest.study.protocolTitle"/></label>
            ${specimenRequest.study.protocolTitle}
        </div>

        <div class="formcol" style="width:500px">
            <label for="file2"><fmt:message key="specimenRequest.study.protocolDocument"/></label>
            <c:choose>
                <c:when test="${!empty specimenRequest.study.protocolDocument}">
                    <c:choose>
                        <c:when test="${specimenRequest.study.protocolDocument.lob.id != null}">
                            <c:url value="/protected/downloadFile.action" var="downloadProtocolUrl">
                                <c:param name="file.id" value="${specimenRequest.study.protocolDocument.lob.id}" />
                                <c:param name="fileName" value="${specimenRequest.study.protocolDocument.name}" />
                                <c:param name="contentType" value="${specimenRequest.study.protocolDocument.contentType}" />
                            </c:url>
                        </c:when>
                        <c:otherwise>
                            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/downloadProtocolDocument" urlEnding=".action"/>
                            <c:url value="${outputUrl}" var="downloadProtocolUrl"/>
                        </c:otherwise>
                    </c:choose>
                    <a href="${downloadProtocolUrl}" class="file">
                        ${specimenRequest.study.protocolDocument.name}
                    </a>
                </c:when>
                <c:otherwise>
                    <fmt:message key="notSpecified.value"/>
                </c:otherwise>
            </c:choose>
        </div>

        <div class="clear"></div>
        <div class="line"></div>

        <label for="sa"><fmt:message key="specimenRequest.study.studyAbstract"/></label>
        <p>${specimenRequest.study.studyAbstract}</p>

        <div class="clear"></div>
        <div class="line"></div>

        <h3 class="noline">
            <fmt:message key="shipment.pi.title"/>
        </h3>

    <div id="principal_investigator">
        <tissuelocator:viewPerson person="${specimenRequest.investigator}" showInstitution="true"/>

        <div class="formcol" style="width:300px">
            <label for="file"><fmt:message key="specimenRequest.investigator.resume"/></label>
            <c:choose>
                <c:when test="${specimenRequest.investigator.resume.lob.id != null}">
                    <c:url value="/protected/downloadFile.action" var="downloadResumeUrl">
                        <c:param name="file.id" value="${specimenRequest.investigator.resume.lob.id}" />
                        <c:param name="fileName" value="${specimenRequest.investigator.resume.name}" />
                        <c:param name="contentType" value="${specimenRequest.investigator.resume.contentType}" />
                    </c:url>
                </c:when>
                <c:otherwise>
                    <tissuelocator:determinePersistenceType urlPrefix="/protected/request/downloadResume" urlEnding=".action"/>
                    <c:url value="${outputUrl}" var="downloadResumeUrl"/>
                </c:otherwise>
            </c:choose>
            <a href="${downloadResumeUrl}" class="file">
                ${specimenRequest.investigator.resume.name}
            </a>
        </div>

        <div class="clear"></div>
        <div class="line"></div>
    </div>
</div>

<h2 class="noline">
    <a href="javascript:;" onclick="toggleVisibility('shipping_details');" class="toggle">
        <fmt:message key="shipment.shippingDetails"/>
    </a>
</h2>

<div id="shipping_details">
    <div class="formcol" style="width:300px">
        <label for="sv"><fmt:message key="shipment.shippingMethod"/></label>
        <c:choose>
            <c:when test="${!empty object.shippingMethod.resourceKey}">
                <fmt:message key="${object.shippingMethod.resourceKey}"/>
            </c:when>
            <c:otherwise>
                -
            </c:otherwise>
        </c:choose>
        <tissuelocator:orderTrackingLink order="${object}" showLabel="true"/>
    </div>

    <div class="formcol" style="width:300px">
        <label for="si"><fmt:message key="specimenRequest.shipment.instructions"/></label>
        <c:choose>
            <c:when test="${!empty object.shippingInstructions}">
                <p>${object.shippingInstructions}</p>
            </c:when>
            <c:otherwise>
                <p>-</p>
            </c:otherwise>
        </c:choose>
    </div>

    <div class="clear"></div>
    <div class="line"></div>

    <div id="shipping_address">
        <tissuelocator:viewPerson person="${object.recipient}" showInstitution="true"/>
        <c:if test="${not empty object.billingRecipient}">
            <div class="clear"><br /></div>
            <label for="billingRecipient"><fmt:message key="shipment.billingRecipient"/></label>
            <tissuelocator:viewPerson person="${object.billingRecipient}" showInstitution="true"/>
        </c:if>
        <div class="clear"><br /></div>
        <div class="line"></div>
    </div>

    <c:if test="${object.readyForResearcherReview}">
    <div class="formcol" style="width:300px">
    <label for="notes"><fmt:message key="shipment.notes"/></label>
        <c:choose>
            <c:when test="${!empty object.notes}">
                <p>${object.notes}</p>
            </c:when>
            <c:otherwise>
                <p>-</p>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="clear"></div>
    <div class="line"></div>
    </c:if>

</div>

<req:isUserInRole role="signedMtaViewer">
    <c:set var="showMta" value="false" />
    <c:set var="mtaViewer" value="other"/>
  <c:if test="${object.readyForResearcherReview}">
    <c:set var="showMta" value="true" />
  </c:if>
  <req:isUserInRole role="shipmentAdmin">
    <c:set var="showMta" value="true" />
  </req:isUserInRole>
  <c:choose>
     <c:when test="${object.request.requestor.id == TissueLocatorUser.id}">
         <c:set var="mtaViewer" value="recipient"/>
     </c:when>
     <c:otherwise>
         <req:isUserInRole role="shipmentAdmin">
             <c:if test="${object.sendingInstitution.id == TissueLocatorUser.institution.id}">
                 <c:set var="mtaViewer" value="sender"/>
             </c:if>
         </req:isUserInRole>
     </c:otherwise>
  </c:choose>
  <c:if test="${showMta}">
     <h2 class="noline"><a href="javascript:;" onclick="toggleVisibility('documentation');" class="toggle">
         <fmt:message key="shipment.mta.title" />
     </a></h2>

     <div id="documentation">
         <tissuelocator:orderMta mode="view" viewer="${mtaViewer}"/>
       </div>

  </c:if>
</req:isUserInRole>
<div class="clear"><br /></div>