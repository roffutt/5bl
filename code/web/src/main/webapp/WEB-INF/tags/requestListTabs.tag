<%@ tag body-content="empty" %>
<%@ attribute name="isAdmin" required="true" %>
<%@ attribute name="selectedTab" required="true" %>
<%@ attribute name="subtitleKey" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<c:set var="showTabs" value="${false}"/>
<c:set var="showLettersTab" value="${false}"/>
<c:set var="showQuestionsTab" value="${false}"/>
<c:set var="showResponsesTab" value="${false}"/>
<c:choose>
    <c:when test="${isAdmin == 'true'}">
        <req:isUserInRole role="questionAdministrator">
            <c:set var="showTabs" value="${true}"/>
            <c:set var="showQuestionsTab" value="${true}"/>
        </req:isUserInRole>
        <req:isUserInRole role="questionResponder">
            <c:set var="showTabs" value="${true}"/>
            <c:set var="showResponsesTab" value="${true}"/>
        </req:isUserInRole>
        <req:isUserInRole role="supportLetterRequestReviewer">
            <c:set var="showTabs" value="${true}"/>
            <c:set var="showLettersTab" value="${true}"/>
        </req:isUserInRole>
    </c:when>
    <c:otherwise>
        <req:isUserInRole role="questionAsker">
            <c:set var="showTabs" value="${true}"/>
            <c:set var="showQuestionsTab" value="${true}"/>
        </req:isUserInRole>
        <req:isUserInRole role="supportLetterRequestor">
            <c:set var="showTabs" value="${true}"/>
            <c:set var="showLettersTab" value="${true}"/>
        </req:isUserInRole>
    </c:otherwise>
</c:choose>

<c:if test="${showTabs}">
    <div class="tabbox">

        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/list" urlEnding=".action"/>
        <c:url value="${outputUrl}" var="requestsUrl">
            <c:param name="object.requestor" value="${TissueLocatorUser.id}" />
        </c:url>
        <c:if test="${isAdmin == 'true'}">
            <c:url value="/admin/request/review/list.action" var="requestsUrl"/>
        </c:if>
        <c:set var="specTabClass" value=""/>
        <c:if test="${selectedTab == 'specimen'}">
            <c:set var="specTabClass" value="selected"/>
        </c:if>
        <a href="${requestsUrl}" class="${specTabClass}"><fmt:message key="specimenRequest.myrequests.subtitle"/></a>

        <c:if test="${showQuestionsTab}">
            <c:url value="/protected/support/question/list.action" var="questionsUrl">
                <c:param name="object.requestor" value="${TissueLocatorUser.id}" />
            </c:url>
            <c:if test="${isAdmin == 'true'}">
                <c:url value="/admin/support/question/list.action" var="questionsUrl"/>
            </c:if>
            <c:set var="questionTabClass" value=""/>
            <c:if test="${selectedTab == 'question'}">
                <c:set var="questionTabClass" value="selected"/>
            </c:if>
            <a href="${questionsUrl}" class="${questionTabClass}"><fmt:message key="question.list.title"/></a>
        </c:if>

        <c:if test="${showResponsesTab}">
            <c:url value="/admin/support/question/response/list.action" var="responsesUrl">
                <c:param name="object.institution" value="${TissueLocatorUser.institution.id}" />
            </c:url>
            <c:set var="responseTabClass" value=""/>
            <c:if test="${selectedTab == 'response'}">
                <c:set var="responseTabClass" value="selected"/>
            </c:if>
            <a href="${responsesUrl}" class="${responseTabClass}"><fmt:message key="questionResponse.list.title"/></a>
        </c:if>

        <c:if test="${showLettersTab}">
            <c:url value="/protected/support/letter/list.action" var="lettersUrl">
                <c:param name="object.requestor" value="${TissueLocatorUser.id}" />
            </c:url>
            <c:if test="${isAdmin == 'true'}">
                <c:url value="/admin/support/letter/list.action" var="lettersUrl"/>
            </c:if>
            <c:set var="letterTabClass" value=""/>
            <c:if test="${selectedTab == 'letter'}">
                <c:set var="letterTabClass" value="selected"/>
            </c:if>
            <a href="${lettersUrl}" class="${letterTabClass}"><fmt:message key="supportLetterRequest.list.title"/></a>
        </c:if>

        <div class="clear"></div>
    </div>

    <h2 class="formtop tabbedtop"><fmt:message key="${subtitleKey}"/></h2>
</c:if>
