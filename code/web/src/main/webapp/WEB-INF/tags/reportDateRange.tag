<%@ tag body-content="empty" %>
<%@ attribute name="formAction" required="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<fmt:message key="datepicker.format" var="datePickerFormat"/>

<div class="line"></div>

<tissuelocator:messages/>
<s:form id="reportDateRangeForm" action="%{#attr.formAction}">
    <div class="period_select">
        <h3><fmt:message key="reports.dateRange"/></h3>
        <div class="date" style=>
            <label for="startDate" style="display:none;">
                <fmt:message key="reports.dateRange.from"/>
            </label>
            <sj:datepicker id="startDate" name="startDate" displayFormat="%{#attr.datePickerFormat}"
                buttonImageOnly="true" changeMonth="true" changeYear="true"/>
            <div class="note"><fmt:message key="reports.dateRange.format"/></div>
        </div>
        <div class="thru">-</div>
        <div class="date">
            <label for="endDate" style="display:none;">
                <fmt:message key="reports.dateRange.to"/>
            </label>
            <sj:datepicker id="endDate" name="endDate" displayFormat="%{#attr.datePickerFormat}"
                buttonImageOnly="true" changeMonth="true" changeYear="true"/>
            <div class="note"><fmt:message key="reports.dateRange.format"/></div>
        </div>
        <div class="submitbtn">
            <s:submit value="%{getText('reports.dateRange.submit')}" name="btn_save" cssClass="btn tiny"
                theme="simple" title="%{getText('btn.search.button')}"/>
        </div>

        <div class="clear"></div>
    </div>
</s:form>

<div class="line"></div>

