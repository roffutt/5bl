<%@ tag body-content="empty" %>
<%@ attribute name="code" required="true" type="com.fiveamsolutions.tissuelocator.data.code.Code" %>
<%@ attribute name="displayUnknown" required="false" %>
<%@ attribute name="suffix" required="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:choose>
    <c:when test="${empty code}">
        <c:if test="${displayUnknown == 'true'}">
            <fmt:message key="unknown.value"/>
        </c:if>
    </c:when>
    <c:when test="${!empty code.value}">
        ${code.name} - ${code.value}${suffix}
    </c:when>
    <c:otherwise>
        ${code.name}${suffix}
    </c:otherwise>
</c:choose>
