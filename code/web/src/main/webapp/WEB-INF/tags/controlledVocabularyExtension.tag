<%@ tag body-content="empty" %>
<%@ attribute name="extendableEntity" required="true" type="com.fiveamsolutions.dynamicextensions.ExtendableEntity" %>
<%@ attribute name="fieldDefinition" required="true" type="com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition" %>
<%@ attribute name="layout" required="true" %>
<%@ attribute name="mode" required="true" %>
<%@ attribute name="propertyPath" %>
<%@ attribute name="wrapperClass" %>
<%@ attribute name="wrapperStyle" %>
<%@ attribute name="displayUnknown" %>
<%@ attribute name="helpUrlBase" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<s:set var="dynamicFieldName" value="%{#attr.propertyPath + '.customProperties[\\'' + #attr.fieldDefinition.fieldName + '\\']'}"/>
<s:set var="dynamicFieldValue" value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName)}"/>
<fmt:message key="unknown.value" var="unknownValue"/>

    <s:if test="%{#attr.layout == 'rowLayout'}">
        <tr>
            <td class="label" scope="row">
                <label>
                    <s:property value="%{#attr.fieldDefinition.fieldDisplayName}"/>
                    <s:if test="%{#attr.mode == 'edit'}">
                        <s:if test="%{!nullable || nullableExtensionDeciderClass != null}">
                            <span class="reqd">
                                <fmt:message key="required.indicator"/>
                            </span>
                        </s:if>                    
                    </s:if>
                </label>            
            </td>
            <td class="value">
    </s:if>
    <s:else>
        <div class="${wrapperClass}" style="${wrapperStyle}">
    </s:else>
    <s:if test="%{#attr.mode == 'view'}">
        <s:if test="%{#attr.layout == 'boxLayout'}">
            <label>${fieldDefinition.fieldDisplayName}</label>
            <div class="value">
        </s:if>
        <s:if test="%{(#attr.displayUnknown == 'true') && (#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName) == null)}">
            ${unknownValue}
        </s:if>
        <s:else>
            <s:property value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName)}"/>
        </s:else>
        <s:if test="%{#attr.layout == 'boxLayout'}">
            </div>
        </s:if>        
    </s:if>            
    <s:if test="%{#attr.mode == 'edit'}">
        <s:if test="%{#attr.layout == 'rowLayout'}">
            <s:if test="%{nullable && nullableExtensionDeciderClass == null}">
                <s:select id="%{#dynamicFieldName}"
                        name="%{#dynamicFieldName}"
                        value="%{#dynamicFieldValue}"
                        list="%{#attr.fieldDefinition.options}"
                        headerKey=""
                        headerValue="%{getText('select.emptyOption')}"/>
            </s:if>
            <s:else>
                <s:select id="%{#dynamicFieldName}"
                        name="%{#dynamicFieldName}" 
                        value="%{#dynamicFieldValue}"
                        list="%{#attr.fieldDefinition.options}"
                        required="true"
                        requiredposition="right"/>
            </s:else>
        </s:if>
        <s:if test="%{#attr.layout == 'boxLayout'}">
            <s:if test="%{nullable && nullableExtensionDeciderClass == null}">
                <s:select id="%{#dynamicFieldName}"
                        name="%{#dynamicFieldName}"
                        value="%{#dynamicFieldValue}"
                        list="%{#attr.fieldDefinition.options}"
                        label="%{#attr.fieldDefinition.fieldDisplayName}"
                        labelposition="top"
                        labelSeparator=""
                        headerKey=""
                        headerValue="%{getText('select.emptyOption')}"/>
            </s:if>
            <s:else>
                <s:select id="%{#dynamicFieldName}"
                        name="%{#dynamicFieldName}" 
                        value="%{#dynamicFieldValue}"
                        list="%{#attr.fieldDefinition.options}"
                        label="%{#attr.fieldDefinition.fieldDisplayName}"
                        labelposition="top"
                        labelSeparator=""
                        required="true"
                        requiredposition="right"/>
            </s:else>
        </s:if>
        <s:fielderror cssClass="fielderror">
            <s:param value="%{#attr.propertyPath + '.' + #attr.fieldDefinition.fieldDisplayName}"/>
        </s:fielderror>
        <s:fielderror cssClass="fielderror">
            <s:param value="%{#attr.fieldDefinition.fieldDisplayName}"/>
        </s:fielderror>        
    </s:if>
    <s:if test="%{#attr.layout == 'rowLayout'}">
            </td>
        </tr>
    </s:if>
    <s:else>
        </div>
    </s:else>    
