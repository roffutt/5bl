<%@ tag body-content="empty" %>
<%@ attribute name="person" required="true" type="com.fiveamsolutions.tissuelocator.data.Person" rtexprvalue="true" %>
<%@ attribute name="showInstitution" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<div class="formcol_wide" style="width:300px">
    <p>
        ${person.firstName} ${person.lastName}<br />
        <c:if test="${showInstitution == 'true'}">
            ${person.organization.name}<br />
        </c:if>
        ${person.address.line1}<br />
        <c:if test="${!empty person.address.line2}">
            ${person.address.line2}<br />
        </c:if>

        ${person.address.city}
        <fmt:message key="${person.address.state.resourceKey}"/>,
        ${person.address.zip}<br />
        <fmt:message key="country.${person.address.country}"/><br />
    </p>
</div>
<div class="formcol_wide" style="width:300px">
    <p>
        <a href="mailto:${person.email}">
            ${person.email}
        </a>
        <br />
        ${person.address.phone}
        <tissuelocator:phoneExtensionDisplay extension="${person.address.phoneExtension}"/>
        <c:if test="${!empty person.address.fax}">
            <br />${person.address.fax}
        </c:if>
    </p>
</div>
