<%@ tag body-content="empty" %>
<%@ attribute name="value" required="true" type="java.lang.Object"%>
<%@ attribute name="suffix" required="false" %>
<%@ attribute name="useResourceKey" required="false" %>
<%@ attribute name="unknownResourceKey" required="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:message key="unknown.value" var="unknownValue"/>
<c:if test="${!empty unknownResourceKey}">
    <fmt:message key="${unknownResourceKey}" var="unknownValue"/>
</c:if>

<div class="value">
    <c:choose>
        <c:when test="${not empty value && useResourceKey == 'true'}">
            <fmt:message key="${value.resourceKey}"/>${suffix}
        </c:when>
        <c:when test="${not empty value && useResourceKey != 'true'}">
            ${value}${suffix}
        </c:when>
        <c:otherwise>
            ${unknownValue}
        </c:otherwise>
    </c:choose>
</div>
