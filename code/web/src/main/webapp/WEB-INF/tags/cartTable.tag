<%@ tag body-content="empty" %>
<%@ attribute name="requestLineItems" required="true" type="java.util.Collection" %>
<%@ attribute name="showOrderFulfillmentForms" required="true" %>
<%@ attribute name="showFinalPriceColumn" required="true" %>
<%@ attribute name="showSpecimenLinks" required="true" %>
<%@ attribute name="showReceiptQuality" %>
<%@ attribute name="showPriceRange" %>
<%@ attribute name="order" type="com.fiveamsolutions.tissuelocator.data.Shipment" %>
<%@ attribute name="cartPrefix" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>
<c:set var="canSeeInstitutionInfo" value="false" />
<req:isUserInRole role="cartInstitutionVisible">
    <c:set var="canSeeInstitutionInfo" value="true" />
</req:isUserInRole>

<div id="${cartPrefix}cart">
    <c:set var="minPriceTotal" value="${0.0}"/>
    <c:set var="maxPriceTotal" value="${0.0}"/>

    <display:table name="${requestLineItems}" uid="lineItem" htmlId="${cartPrefix}request" defaultsort="1">
        <tissuelocator:displaytagProperties/>
        <display:setProperty name="basic.msg.empty_list_row">
            <tr class="empty"><td colspan="{0}"><fmt:message key="specimenRequest.noSpecimensSelected"/></td></tr>
        </display:setProperty>

        <fmt:message key="specimenRequest.lineItems" var="lineItemsTitle"/>
        <display:column title="<div>${lineItemsTitle}</div>" class="carttitle" sortProperty="specimen.id">
            <c:url var="viewUrl" value="/protected/specimen/view.action">
                <c:param name="object.id" value="${lineItem.specimen.id}"/>
            </c:url>
            <c:choose>
                <c:when test="${showSpecimenLinks}">
                    <a href="${viewUrl}">${lineItem.specimen.id} <req:isUserInRole role="externalIdViewer">(External Id: ${lineItem.specimen.externalId})</req:isUserInRole></a>
                </c:when>
                <c:otherwise>
                    <strong>${lineItem.specimen.id} <req:isUserInRole role="externalIdViewer">(External Id: ${lineItem.specimen.externalId})</req:isUserInRole></strong><br/>
                </c:otherwise>
            </c:choose>
            <tissuelocator:codeDisplay code="${lineItem.specimen.pathologicalCharacteristic}"/>
        </display:column>
        <c:if test="${canSeeInstitutionInfo and showOrderFulfillmentForms != 'true'}">
            <fmt:message key="specimenRequest.institution" var="institutionTitle"/>
            <display:column title="<div>${institutionTitle}</div>" class="carttitle">
                ${lineItem.specimen.externalIdAssigner.name}
            </display:column>
        </c:if>
        <fmt:message key="specimenRequest.amountAvailable" var="amountAvailableTitle"/>
        <display:column title="<div>${amountAvailableTitle}</div>">
            <c:if test="${!empty lineItem.specimen.availableQuantity}">
                ${lineItem.specimen.availableQuantity}
                <fmt:message key="${lineItem.specimen.availableQuantityUnits.resourceKey}"/>
            </c:if>
        </display:column>

        <fmt:message key="specimenRequest.amountRequested" var="amountRequestedTitle"/>
        <display:column title="<div>${amountRequestedTitle}</div>">
            ${lineItem.quantity}
            <fmt:message key="${lineItem.quantityUnits.resourceKey}"/>
        </display:column>

        <req:isUserInRole role="priceViewer">
            <fmt:message key="specimenRequest.costRecoveryFee" var="costRecoveryFeeTitle"/>
            <c:if test="${empty showPriceRange || showPriceRange == 'true'}">
                <display:column title="<div>${costRecoveryFeeTitle}</div>">
                    <tissuelocator:specimenPrice priceNegotiable="${lineItem.specimen.priceNegotiable}"
                        minimumPrice="${lineItem.specimen.minimumPrice}"
                        maximumPrice="${lineItem.specimen.maximumPrice}"/>
                    <c:if test="${!lineItem.specimen.priceNegotiable}">
                        <c:set var="minPriceTotal" value="${minPriceTotal + lineItem.specimen.minimumPrice}"/>
                        <c:set var="maxPriceTotal" value="${maxPriceTotal + lineItem.specimen.maximumPrice}"/>
                    </c:if>
                </display:column>
            </c:if>
            <c:if test="${showFinalPriceColumn == 'true'}">
                <fmt:message key="specimenRequest.finalFee" var="finalPriceTitle"/>
                <display:column title="<div>${finalPriceTitle}</div>" class="action" style="white-space: nowrap">
                    <c:choose>
                        <c:when test="${showOrderFulfillmentForms == 'true'}">
                            &#36;&nbsp;<s:textfield size="5" theme="simple" name="lineItems[%{#attr.lineItem_rowNum - 1}].finalPrice"
                                        value="%{#attr.lineItem.finalPrice}" title="%{getText('specimenRequest.finalFee.field')}"/>
                            <s:fielderror cssClass="fielderror">
                                <s:param>lineItems[${lineItem_rowNum - 1}].finalPrice</s:param>
                            </s:fielderror>
                            <c:if test="${not empty lineItem.finalPrice and showFinalPriceColumn == 'true'}">
                                <c:set var="finalPriceTotal" value="${finalPriceTotal + lineItem.finalPrice}"/>
                            </c:if>
                        </c:when>
                        <c:when test="${not empty lineItem.finalPrice}">
                            <fmt:formatNumber value="${lineItem.finalPrice}" type="currency" />
                            <c:set var="finalPriceTotal" value="${finalPriceTotal + lineItem.finalPrice}"/>
                        </c:when>
                        <c:otherwise>-</c:otherwise>
                    </c:choose>
                </display:column>
            </c:if>
        </req:isUserInRole>
        <fmt:message key="specimenRequest.notesForProcessing" var="notesForProcessingTitle"/>
        <display:column title="<div>${notesForProcessingTitle}</div>" class="carttitle">
            <c:choose>
                <c:when test="${lineItem.note != null}">
                    ${lineItem.note}
                </c:when>
                <c:otherwise>
                    -
                </c:otherwise>
            </c:choose>
        </display:column>

        <c:if test="${showOrderFulfillmentForms == 'true'}">
            <display:column title="<div>Action</div>" headerClass="action" class="action">
                <c:url var="removeUrl" value="/admin/shipment/removeLineItem.action">
                    <c:param name="lineItem" value="${lineItem.id}"/>
                    <c:param name="object.id" value="${object.id}"/>
                </c:url>
                <a href="${removeUrl}" class="btn_fw_action_col"><fmt:message key="shipment.remove.btn"/></a>
            </display:column>
        </c:if>

        <c:if test="${showReceiptQuality == 'true'}">
            <fmt:message key="specimenRequest.receiptQuality" var="receiptQualityTitle"/>
            <display:column title="<div>${receiptQualityTitle}</div>">
                <c:set var="receiptQualityString" value="${''}"/>
                <c:if test="${!empty lineItem.received}">
                    <c:set var="receiptQualityString">
                        ${receiptQualityString}<fmt:message key="specimenRequest.received.${lineItem.received}"/>
                    </c:set>
                </c:if>
                <c:if test="${!empty lineItem.receiptQuality}">
                    <c:if test="${!empty receiptQualityString}">
                        <c:set var="receiptQualityString">${receiptQualityString},&nbsp;</c:set>
                    </c:if>
                    <c:set var="receiptQualityString">
                        ${receiptQualityString}${lineItem.receiptQuality.name}
                    </c:set>
                </c:if>
                <c:if test="${!empty lineItem.disposition}">
                    <c:if test="${!empty receiptQualityString}">
                        <c:set var="receiptQualityString">${receiptQualityString},&nbsp;</c:set>
                    </c:if>
                    <c:set var="receiptQualityString">
                        ${receiptQualityString}<fmt:message key="${lineItem.disposition.resourceKey}"/>
                    </c:set>
                </c:if>
                <c:if test="${!empty lineItem.receiptNote}">
                    <c:if test="${!empty receiptQualityString}">
                        <c:set var="receiptQualityString">${receiptQualityString},&nbsp;</c:set>
                    </c:if>
                    <c:set var="receiptQualityString">
                        ${receiptQualityString}${lineItem.receiptNote}
                    </c:set>
                </c:if>
                ${receiptQualityString}
            </display:column>
        </c:if>

        <tissuelocator:cartTableFooter order="${order}" showOrderForms="${showOrderFulfillmentForms}" showInstitution="${canSeeInstitutionInfo}" showFinalPriceColumn="${showFinalPriceColumn}"
            showPriceRange="${showPriceRange}" finalPriceTotal="${finalPriceTotal}" minPriceTotal="${minPriceTotal}" maxPriceTotal="${maxPriceTotal}" showReceiptQuality="${showReceiptQuality}"/>

    </display:table>
</div>
