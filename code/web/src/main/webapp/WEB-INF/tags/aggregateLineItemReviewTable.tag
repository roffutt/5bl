<%@ tag body-content="empty" %>
<%@ attribute name="requestLineItems" required="true" type="java.util.Collection" rtexprvalue="true" %>
<%@ attribute name="cartPrefix" required="true" %>
<%@ attribute name="emptyKey" required="true" %>
<%@ attribute name="titleKey" required="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set value="%{hasErrors()}" var="hasErrors" scope="page" />
<fmt:message key="default.date.format" var="dateFormat"/>
<req:isUserInRole role="lineItemReviewVoter">
    <c:set var="isLineItemReviewer" value="true" />
</req:isUserInRole>

<tr id="${cartPrefix}_title">
    <td colspan="8">
        <h2 class="noline">
            <fmt:message key="${titleKey}"/>
        </h2>
    </td>
</tr>

<tr id="${cartPrefix}_header">
    <th style="padding: 8px;" colspan="2" scope="col"><fmt:message key="specimenRequest.review.columnHeader.institution" /></th>
    <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.biospecimens" /></th>
    <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.criteria" /></th>
    <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.reviewer" /></th>
    <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.date" /></th>
    <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.vote" /></th>
    <th style="padding: 8px;" scope="col"><fmt:message key="specimenRequest.review.columnHeader.comment" /></th>
</tr>

<c:if test="${empty requestLineItems}">
    <tr class="empty" id="${cartPrefix}_empty">
        <td colspan="8">
            <fmt:message key="${emptyKey}"/>
        </td>
    </tr>
</c:if>

<c:forEach items="${requestLineItems}" var="lineItem" varStatus="lineItemStatus">
    <c:set var="rowClass" value=""/>
    <c:if test="${lineItemStatus.count % 2 == 1}">
        <c:set var="rowClass" value="odd"/>
    </c:if>
    <tr class="${rowClass}" id="${cartPrefix}_data${lineItemStatus.index}">

    <c:choose>
        <c:when test="${isLineItemReviewer == true && lineItem.institution.id == TissueLocatorUser.institution.id && (lineItem.vote.vote == null || hasErrors)}">
            <td class="hilite"><img src="<c:url value="/images/arrow_right.gif" />" alt="arrow_right" /></td>
            <td class="hilite">${lineItem.vote.institution.name}</td>
            <td class="hilite">${lineItem.quantity}</td>
            <td class="hilite">${lineItem.criteria}</td>
            <td class="hilite">
                <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${true}"
                    value="${lineItem.vote.user.firstName} ${lineItem.vote.user.lastName}" maxWordLength="20"/>
            </td>
            <td class="hilite"><fmt:formatDate pattern="${dateFormat}" value="${lineItem.vote.date}"/></td>
            <td class="hilite">
                <s:select name="%{#attr.cartPrefix}LineItemsArray[%{#attr.lineItemStatus.index}].vote.vote"
                        id="%{#attr.cartPrefix}LineItem%{#attr.lineItemStatus.index}Vote"
                        value="%{#attr.lineItem.vote.vote}"
                        list="%{instiutionalReviewAllowableVotes}"
                        listValue="%{getText(resourceKey)}"
                        headerKey="" headerValue="%{getText('select.emptyOption')}"
                        theme="simple" title="%{getText('lineItemReview.vote.select')}"/>
                <s:fielderror cssClass="fielderror">
                    <s:param>${cartPrefix}LineItemsArray[${lineItemStatus.index}].vote.vote</s:param>
                </s:fielderror>
            </td>
            <td class="hilite">
                <s:textarea name="%{#attr.cartPrefix}LineItemsArray[%{#attr.lineItemStatus.index}].vote.comment"
                        id="%{#attr.cartPrefix}LineItem%{#attr.lineItemStatus.index}Comment" cssStyle="width:100%" rows="3"
                        required="false" theme="simple" title="%{getText('lineItemReview.comment.textarea')}"/>
                <s:fielderror cssClass="fielderror">
                    <s:param>${cartPrefix}LineItems[${lineItemStatus.index}].vote.comment</s:param>
                </s:fielderror>
                <s:fielderror cssClass="fielderror">
                    <s:param>${cartPrefix}LineItems[${lineItemStatus.index}].vote.commentValid</s:param>
                </s:fielderror>
            </td>
        </c:when>
        <c:otherwise>
            <td colspan="2">${lineItem.vote.institution.name}</td>
            <td>${lineItem.quantity}</td>
            <td>${lineItem.criteria}</td>
            <td>
                <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${true}" maxWordLength="20"
                    value="${lineItem.vote.user.firstName} ${lineItem.vote.user.lastName}"/>
            </td>
            <td><fmt:formatDate pattern="${dateFormat}" value="${lineItem.vote.date}"/></td>
            <td>
                <c:if test="${lineItem.vote.vote != null}">
                    <fmt:message key="${lineItem.vote.vote.resourceKey}"  />
                </c:if>
            </td>
            <td>${lineItem.vote.comment}</td>
        </c:otherwise>
    </c:choose>
</c:forEach>
