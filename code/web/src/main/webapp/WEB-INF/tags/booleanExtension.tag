<%@ tag body-content="empty" %>
<%@ attribute name="extendableEntity" required="true" type="com.fiveamsolutions.dynamicextensions.ExtendableEntity" %>
<%@ attribute name="fieldDefinition" required="true" type="com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition" %>
<%@ attribute name="layout" required="true" %>
<%@ attribute name="mode" required="true" %>
<%@ attribute name="propertyPath" %>
<%@ attribute name="wrapperClass" %>
<%@ attribute name="wrapperStyle" %>
<%@ attribute name="helpUrlBase" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<s:set var="dynamicFieldName" value="%{#attr.propertyPath + '.customProperties[\\'' + #attr.fieldDefinition.fieldName + '\\']'}"/>
<s:set var="yesEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@YES}" />
<s:set var="noEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@NO}" />
<s:set var="naEnum" value="%{@com.fiveamsolutions.tissuelocator.data.YesNoResponse@NOT_APPLICABLE}" />

    <s:if test="%{#attr.layout == 'rowLayout'}">
        <tr>
            <td class="label" scope="row">
            <s:if test="%{#attr.mode == 'view'}">
                <label>${fieldDefinition.fieldDisplayName}</label>
            </s:if>
            <s:if test="%{#attr.mode == 'edit'}">
                <label for="${dynamicFieldName}">
                    <s:property value="%{#attr.fieldDefinition.fieldDisplayName}"/>
                    <s:if test="%{!nullable || nullableExtensionDeciderClass != null}">
                        <span class="reqd"><fmt:message key="required.indicator"/></span>
                    </s:if>
                </label>            
            </s:if>
            </td>
            <td class="value">
    </s:if>
    <s:else>
        <div class="${wrapperClass}" style="${wrapperStyle}">
    </s:else>
    <s:if test="%{#attr.mode == 'view'}">
        <s:if test="%{#attr.layout == 'boxLayout'}">
            <label>${fieldDefinition.fieldDisplayName}</label>
            <div class="value">
        </s:if>
        <s:set var="customPropertyValue" value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName)}"/>
        <c:set var="checkClass" value=""/>
        <s:set var="customPropertyBooleanText" value="%{''}"/>
        <s:if test="%{#customPropertyValue == null}">
            <s:set var="customPropertyBooleanText" value="%{getText(#naEnum.resourceKey)}"/>
        </s:if>
        <s:elseif test="%{#customPropertyValue}">
            <c:set var="checkClass" value="class=\"checked\""/>
            <s:set var="customPropertyBooleanText" value="%{getText(#yesEnum.resourceKey)}"/>
        </s:elseif>
        <s:elseif test="%{!#customPropertyValue}">
            <c:set var="checkClass" value="class=\"x_icon\""/>
            <s:set var="customPropertyBooleanText" value="%{getText(#noEnum.resourceKey)}"/>
        </s:elseif>
        <span ${checkClass}>
            <s:property value="%{#customPropertyBooleanText}"/>
        </span>
        <s:if test="%{#attr.layout == 'boxLayout'}">
            </div>
        </s:if>                
    </s:if>
    <s:if test="%{#attr.mode == 'edit'}">
        <s:if test="%{#attr.layout == 'boxLayout'}">    
            <label for="${dynamicFieldName}">
                <s:property value="%{#attr.fieldDefinition.fieldDisplayName}"/>
                <s:if test="%{!nullable || nullableExtensionDeciderClass != null}">
                    <span class="reqd">
                        <fmt:message key="required.indicator"/>
                    </span>
                </s:if>
            </label>
        </s:if>
        <s:set var="customPropertyValue" value="%{#attr.extendableEntity.getCustomProperty(#attr.fieldDefinition.fieldName)}"/>
        <s:set var="booleanFieldOptionMap" value="%{#{#yesEnum : true, #noEnum : false}}"/>
        <s:if test="%{nullable && nullableExtensionDeciderClass == null}">
            <s:set var="booleanFieldOptionMap" value="%{#{#yesEnum : true, #noEnum : false, #naEnum : null}}"/>
        </s:if>
        <s:iterator value="%{#booleanFieldOptionMap}">
            <s:set value="%{key.name()}" var="optionName"/>
            <s:set value="%{value}" var="optionValue"/>        
            <c:set var="checked" value=""/>
            <s:if test="%{#optionValue == #customPropertyValue}">
                <c:set var="checked" value="checked"/>
            </s:if>        
            <input type="radio" name="${dynamicFieldName}" id="${dynamicFieldName}_${optionName}" value="${optionValue}" ${checked}/>        
            <label class="inline" for="${dynamicFieldName}_${optionName}">
                <s:property value="%{getText(key.resourceKey)}"/>
            </label>
        </s:iterator>
        <s:fielderror cssClass="fielderror">
            <s:param value="%{#attr.propertyPath + '.' + #attr.fieldDefinition.fieldDisplayName}"/>
        </s:fielderror>
        <s:fielderror cssClass="fielderror">
            <s:param value="%{#attr.fieldDefinition.fieldDisplayName}"/>
        </s:fielderror>                               
    </s:if>
    <s:if test="%{#attr.layout == 'rowLayout'}">
            </td>
        </tr>
    </s:if>
</div>