<%@ tag body-content="empty" %>
<%@ attribute name="institution" required="true" type="com.fiveamsolutions.tissuelocator.data.Institution" rtexprvalue="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
    <c:when test="${!empty institution.institutionProfileUrl}">
        <a href="${institution.institutionProfileUrl}" target="_blank">
            ${institution.name}
        </a>
    </c:when>
    <c:otherwise>
        ${institution.name}
    </c:otherwise>
</c:choose>
