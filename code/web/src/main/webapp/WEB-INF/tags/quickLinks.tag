<%@ tag body-content="empty" %>
<%@ attribute name="helpUrl" required="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

    <div id="quicklinks"><span>
        <c:url var="helpUrl" value="${helpUrl}"/>
        <c:if test="${empty helpUrl}">
            <c:url var="helpUrl" value="/notYetImplemented.jsp"/>
        </c:if>
    
        <!--Quick Links-->

        <!--If Logged In-->
        <c:url value="/protected/myAccount/input.action" var="myAccountUrl"/>
        <c:if test="${pageContext.request.remoteUser != null}">
            <fmt:message key="header.welcome"/>
            <a href="${myAccountUrl}" class="userfname" title="${TissueLocatorUser.firstName}">
                <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${false}" value="${TissueLocatorUser.firstName}"
                    maxWordLength="20" manual="true"/></a>
            <a href="${myAccountUrl}" class="account"><fmt:message key="header.account"/></a>
        </c:if>
        <!--/If Logged In-->

        <!--Omnipresent-->
        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/viewCart" urlEnding=".action" asUrl="true"/>
        <a href="${outputUrl}" class="cart"><fmt:message key="header.cart"/></a>
        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/list" urlEnding=".action"/>
        <c:url value="${outputUrl}" var="myRequestsUrl">
            <c:param name="object.requestor" value="${TissueLocatorUser.id}" />
        </c:url>
        <a href="${myRequestsUrl}" class="requests"><fmt:message key="header.requests"/></a>
        <a href="${helpUrl}" class="help"><fmt:message key="header.help"/></a>
        <!--/Omnipresent-->

        <c:choose>
            <c:when test="${pageContext.request.remoteUser == null}">
                <!--If Logged Out-->
                <c:url value="/protected/home.action" var="homeUrl"/>
                <a href="${homeUrl}" class="sign_in"><fmt:message key="header.login"/></a>
                <!--/If Logged Out-->
            </c:when>
            <c:otherwise>
                <!--If Logged In-->
                <c:url value="/login/logout.action" var="logoutUrl"/>
                <a href="${logoutUrl}" class="sign_out"><fmt:message key="header.logout"/></a>
                <!--/If Logged In-->
            </c:otherwise>
        </c:choose>

        <!--/Quick Links-->

    </span></div>
