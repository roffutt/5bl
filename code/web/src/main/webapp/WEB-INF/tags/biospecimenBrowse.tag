<%@ tag body-content="empty" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<s:action namespace="/widget" name="%{leftBrowseWidget}" executeResult="true"></s:action>
<s:action namespace="/widget" name="%{centerBrowseWidget}" executeResult="true"></s:action>
<s:action namespace="/widget" name="%{rightBrowseWidget}" executeResult="true"></s:action>
