<%@ tag body-content="empty" %>
<%@ attribute name="uiDynamicFieldCategory" required="true" rtexprvalue="true" type="com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicFieldCategory"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<fmt:message key="unknown.value" var="unknownValue"/>
<s:set var="textType" value="%{@com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField$FieldType@TEXT}" />
<s:set var="codeType" value="%{@com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField$FieldType@CODE}" />
<s:set var="quantityType" value="%{@com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField$FieldType@QUANTITY}" />
<s:set var="ageType" value="%{@com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField$FieldType@AGE}" />
<s:set var="customType" value="%{@com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField$FieldType@CUSTOM_PROPERTY}" />
<s:set var="enumType" value="%{@com.fiveamsolutions.tissuelocator.data.config.category.UiDynamicCategoryField$FieldType@ENUM}" />
<s:set var="specimen" value="%{object}"/>
<table class="data tabular-layout">
    <s:iterator value="%{#attr.uiDynamicFieldCategory.uiDynamicCategoryFields}" var="categoryField">
        <s:if test="%{#categoryField.isFieldDisplayed(#specimen, #attr.TissueLocatorUser)}">
            <s:set var="fieldValue" value="%{getObjectProperty(#categoryField.fieldConfig.fieldName)}"/>
            <s:if test="%{#categoryField.fieldType eq #textType}">
                <tr>
                    <td class="label" scope="row">
                        <label>${categoryField.fieldConfig.fieldDisplayName}</label>
                    </td>
                    <td class="value">
                        <tissuelocator:displayValueOrUnknown value="${fieldValue}"/>
                    </td>
                </tr>
            </s:if>
            <s:elseif test="%{#categoryField.fieldType eq #codeType}">
                <tr>
                    <td class="label" scope="row">
                        <label>${categoryField.fieldConfig.fieldDisplayName}</label>
                    </td>
                    <td class="value">
                        <tissuelocator:codeDisplay code="${fieldValue}" displayUnknown="true"/>
                    </td>
                </tr>
            </s:elseif>
            <s:elseif test="%{#categoryField.fieldType eq #quantityType}">
                <s:set var="unitsKeyName" value="%{#categoryField.fieldConfig.fieldName + 'Units.resourceKey'}"/>
                <s:set var="unitsKey" value="%{getObjectProperty(#unitsKeyName)}"/>
                <tr>
                    <td class="label" scope="row">
                        <label>${categoryField.fieldConfig.fieldDisplayName}</label>
                    </td>
                    <td class="value">
                        <c:choose>
                            <c:when test="${not empty fieldValue}">
                                <fmt:formatNumber value="${fieldValue}" maxFractionDigits="2"/>
                                <fmt:message key="${unitsKey}"/>
                            </c:when>
                            <c:otherwise>
                                ${unknownValue}
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </s:elseif>
            <s:elseif test="%{#categoryField.fieldType eq #ageType}">
                <s:set var="unitsName" value="%{#categoryField.fieldConfig.fieldName + 'Units'}"/>
                <s:set var="unitsValue" value="%{getObjectProperty(#unitsName)}"/>
                <s:set var="unitsKeyName" value="%{#categoryField.fieldConfig.fieldName + 'Units.resourceKey'}"/>
                <s:set var="unitsKey" value="%{getObjectProperty(#unitsKeyName)}"/>
                <tr>
                    <td class="label" scope="row">
                        <label>${categoryField.fieldConfig.fieldDisplayName}</label>
                    </td>
                    <td class="value">
                        <c:choose>
                            <c:when test="${not empty fieldValue}">
                                ${fieldValue}<c:if test="${object.patientAgeAtCollectionAtHippaMax}"><fmt:message key="hippaMaxAge.indicator"/></c:if>
                                <fmt:message key="${unitsKey}"/>
                            </c:when>
                            <c:otherwise>
                                ${unknownValue}
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </s:elseif>
            <s:elseif test="%{#categoryField.fieldType eq #customType}">
                <s:set var="dynamicFieldDefinition" value="%{getDynamicFieldDefinition(#categoryField.fieldConfig.fieldName)}"/>
                <s:set var="dynamicFieldDefinitionList" value="%{{#dynamicFieldDefinition}}"/>
                <tissuelocator:viewExtensions
                    dynamicFieldDefinitions="${dynamicFieldDefinitionList}"
                    layout="rowLayout"
                    extendableEntity="${object}"
                    displayUnknown="${true}"/>
            </s:elseif>
            <s:elseif test="%{#categoryField.fieldType eq #enumType}">
                <tr>
                    <td class="label" scope="row">
                        <label>${categoryField.fieldConfig.fieldDisplayName}</label>
                    </td>
                    <s:if test="%{#fieldValue instanceof java.util.List}">
                        <td class="value">
                            <c:choose>
                                <c:when test="${not empty fieldValue}">
                                    <c:forEach items="${fieldValue}" var="value">
                                        <fmt:message key="${value.resourceKey}"/>
                                        <br/>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    ${unknownValue}
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </s:if>
                    <s:else>
                        <td class="value">
                            <tissuelocator:displayValueOrUnknown value="${fieldValue}" useResourceKey="true"/>
                        </td>
                    </s:else>
                </tr>
            </s:elseif>
        </s:if>
    </s:iterator>
</table>