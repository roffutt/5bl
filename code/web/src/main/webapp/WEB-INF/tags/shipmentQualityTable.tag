<%@ tag body-content="empty" %>
<%@ attribute name="requestLineItems" required="true" type="java.util.Collection" rtexprvalue="true"%>
<%@ attribute name="lineItemName" required="true" rtexprvalue="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/request-1.0" prefix="req" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<tr id="${lineItemName}_header">
    <s:if test="%{displayAggregateSearchResults}">
        <th><div><fmt:message key="aggregateCart.institution"/></div></th>
        <th><div><fmt:message key="aggregateCart.criteria"/></div></th>
    </s:if>
    <s:else>
        <th><div><fmt:message key="specimenRequest.lineItems"/></div></th>
    </s:else>

    <th><div><fmt:message key="shipment.updateSpecimenQuality.amountRequested"/></div></th>
    <th><div><fmt:message key="shipment.updateSpecimenQuality.received"/></div></th>
    <th><div><fmt:message key="shipment.updateSpecimenQuality.quality"/></div></th>
    <th><div><fmt:message key="shipment.updateSpecimenQuality.disposition"/></div></th>
    <th><div><fmt:message key="shipment.updateSpecimenQuality.receiptNotes"/></div></th>
</tr>

<c:if test="${empty requestLineItems}">
    <tr class="empty" id="${lineItemName}_empty">
        <td colspan="${tableCellCount}">
            <fmt:message key="${emptyKey}"/>
        </td>
    </tr>
</c:if>

<c:forEach items="${requestLineItems}" var="lineItem" varStatus="lineItemStatus">
    <c:set var="rowClass" value=""/>
    <c:if test="${lineItemStatus.count % 2 == 1}">
        <c:set var="rowClass" value="odd"/>
    </c:if>

    <tr class="${rowClass}" id="${lineItemName}_data${lineItemStatus.index}">
        <s:if test="%{displayAggregateSearchResults}">
            <td><tissuelocator:institutionName institution="${lineItem.institution}"/></td>
            <td class="carttitle">${lineItem.criteria}</td>
        </s:if>
        <s:else>
            <td class="carttitle">
                <c:url var="viewUrl" value="/protected/specimen/view.action">
                    <c:param name="object.id" value="${lineItem.specimen.id}"/>
                </c:url>
                <a href="${viewUrl}">${lineItem.specimen.id} <req:isUserInRole role="externalIdViewer">(<fmt:message key="specimen.cartTable"/> ${lineItem.specimen.externalId})</req:isUserInRole></a>
                <tissuelocator:codeDisplay code="${lineItem.specimen.pathologicalCharacteristic}"/>
            </td>
        </s:else>

        <td>
            ${lineItem.quantity}
            <s:if test="%{!displayAggregateSearchResults}">
                <fmt:message key="${lineItem.quantityUnits.resourceKey}"/>
            </s:if>
        </td>

        <td class="inline_label" style="text-align: left;">
            <s:radio name="%{#attr.lineItemName}[%{#attr.lineItemStatus.index}].received" value="%{#attr.lineItem.received}" theme="5bl-custom"
                list="#{true: getText('shipment.updateSpecimenQuality.yes'), false: getText('shipment.updateSpecimenQuality.no')}"
                title="%{getText('shipment.updateSpecimenQuality.received.radio')}"/>
        </td>

        <td>
            <s:select name="%{#attr.lineItemName}[%{#attr.lineItemStatus.index}].receiptQuality"
                value="%{#attr.lineItem.receiptQuality.id}"
                headerKey="" headerValue="%{getText('select.emptyOption')}"
                list="%{receiptQualities}" listKey="id" listValue="name"
                title="%{getText('shipment.updateSpecimenQuality.quality.select')}"/>
        </td>

        <td>
            <c:choose>
                <c:when test="${empty lineItem.disposition.allowedTransitions}">
                    <s:select name="%{#attr.lineItemName}[%{#attr.lineItemStatus.index}].disposition"
                        value="%{#attr.lineItem.disposition}"
                        headerKey="" headerValue="%{getText('select.emptyOption')}"
                        list="%{@com.fiveamsolutions.tissuelocator.data.SpecimenDisposition@values()}"
                        listValue="%{getText(resourceKey)}" title="%{getText('shipment.updateSpecimenQuality.disposition.select')}"/>
                </c:when>
                <c:otherwise>
                    <s:select name="%{#attr.lineItemName}[%{#attr.lineItemStatus.index}].disposition"
                        value="%{#attr.lineItem.disposition}"
                        headerKey="" headerValue="%{getText('select.emptyOption')}"
                        list="%{#attr.lineItem.disposition.allowedTransitions}"
                        listValue="%{getText(resourceKey)}" title="%{getText('shipment.updateSpecimenQuality.disposition.select')}"/>
                </c:otherwise>
            </c:choose>

            <script>
            dispositionArray[<s:property value="%{#attr.lineItemStatus.index}"/>] = '<s:property value="%{#attr.lineItem.disposition}"/>';
            </script>
        </td>

        <td>
            <s:textarea name="%{#attr.lineItemName}[%{#attr.lineItemStatus.index}].receiptNote" value="%{#attr.lineItem.receiptNote}"
                cssStyle="width:100%" cols="25" rows="3" required="false" theme="simple" title="%{getText('shipment.updateSpecimenQuality.receiptNotes.field')}"/>
            <s:fielderror cssClass="fielderror">
                <s:param>${lineItemName}s[${lineItemStatus.index}].receipt_note</s:param>
            </s:fielderror>
        </td>
    </tr>
</c:forEach>

<s:if test="%{displayAggregateSearchResults}">
    <c:set var="btnColspan" value="${6}"/>
</s:if>
<s:else>
    <c:set var="btnColspan" value="${5}"/>
</s:else>
<tr class="graybar" id="${lineItemName}_footer">
    <td>
        <s:submit value="%{getText('btn.updateStatus')}" name="submit" id="btn_update" cssClass="btn" theme="simple" title="%{getText('btn.updateStatus.button')}"/>
    </td>
    <td colspan="${btnColspan}">&nbsp;</td>
</tr>
