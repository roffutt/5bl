<%@ tag body-content="empty" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tissuelocator" %>

<s:set var="grantTypeEnum" value="%{@com.fiveamsolutions.tissuelocator.data.support.SupportLetterType@GRANT}"/>
<s:set var="otherTypeEnum" value="%{@com.fiveamsolutions.tissuelocator.data.support.SupportLetterType@OTHER}"/>

<div class="box pad10">
    <h2 class="formtop noline nobg"><fmt:message key="supportLetterRequest.review.letterRequest.title"/></h2>

    <div id="principal_investigator">
        <h4 class="green_bar"><fmt:message key="supportLetterRequest.review.investigator.title"/></h4>

        <tissuelocator:viewPersonSingleColumn person="${object.investigator}" showInstitution="true"/>

        <div class="col250">
            <c:url value="/protected/downloadFile.action" var="downloadResumeUrl">
                <c:param name="file.id" value="${object.resume.lob.id}" />
                <c:param name="fileName" value="${object.resume.name}" />
                <c:param name="contentType" value="${object.resume.contentType}" />
            </c:url>
            <b><fmt:message key="supportLetterRequest.resume"/></b>
            <a href="${downloadResumeUrl}" class="file">
                ${object.resume.name}
            </a>
        </div>

        <div class="clear"></div>

        <h4 class="green_bar"><fmt:message key="supportLetterRequest.letter.title"/></h4>

        <div class="col250">
            <b><fmt:message key="supportLetterRequest.type"/></b>
            <br/>
            <s:property value="%{getText(object.type.resourceKey)}"/>

            <s:if test="%{object.type == #grantTypeEnum}">
                <br /><br />
                <b><fmt:message key="supportLetterRequest.grantType"/></b>
                <br/>
                <s:property value="%{object.grantType}"/>
                <br /><br />

                <b><fmt:message key="supportLetterRequest.fundingSources"/></b>
                <br/>
                <s:set var="fundingSourceString" value="%{''}" scope="request"/>
                <s:iterator value="%{object.fundingSources}">
                    <c:choose>
                        <c:when test="${empty fundingSourceString}">
                            <s:set var="fundingSourceString" value="%{name}" scope="request"/>
                        </c:when>
                        <c:otherwise>
                            <s:set var="fundingSourceString" value="%{#attr.fundingSourceString + ', ' + name}"
                                scope="request"/>
                        </c:otherwise>
                    </c:choose>
                </s:iterator>
                <s:property value="%{#attr.fundingSourceString}"/>
            </s:if>

            <s:elseif test="%{object.type == #otherTypeEnum}">
                <br /><br />
                <b><fmt:message key="supportLetterRequest.otherType"/></b>
                <br/>
                <s:property value="%{object.otherType}"/>
            </s:elseif>
        </div>

        <div class="col250">
            <c:url value="/protected/downloadFile.action" var="downloadDraftProtocolUrl">
                <c:param name="file.id" value="${object.draftProtocol.lob.id}" />
                <c:param name="fileName" value="${object.draftProtocol.name}" />
                <c:param name="contentType" value="${object.draftProtocol.contentType}" />
            </c:url>
            <b><fmt:message key="supportLetterRequest.draftProtocol"/></b>
            <a href="${downloadDraftProtocolUrl}" class="file">
                ${object.draftProtocol.name}
            </a>

            <s:if test="%{object.draftLetter != null}">
                <br /><br />
                <c:url value="/protected/downloadFile.action" var="downloadDraftLetterUrl">
                    <c:param name="file.id" value="${object.draftLetter.lob.id}" />
                    <c:param name="fileName" value="${object.draftLetter.name}" />
                    <c:param name="contentType" value="${object.draftLetter.contentType}" />
                </c:url>
                <b><fmt:message key="supportLetterRequest.draftLetter"/></b>
                <a href="${downloadDraftLetterUrl}" class="file">
                    ${object.draftLetter.name}
                </a>
            </s:if>
        </div>

        <s:if test="%{object.intendedSubmissionDate != null}">
            <div class="col250">
                <b><fmt:message key="supportLetterRequest.intendedSubmissionDate"/></b>
                <br/>
                <s:date name="%{object.intendedSubmissionDate}" format="%{getText('default.date.format')}"/>
            </div>
        </s:if>

        <div class="clear"></div>

        <div class="pad10">
            <b><fmt:message key="supportLetterRequest.comments"/></b>
            <p style="width:100%; max-width:none;">
                <s:property value="%{object.comments}"/>
            </p>
        </div>
    </div>
</div>

