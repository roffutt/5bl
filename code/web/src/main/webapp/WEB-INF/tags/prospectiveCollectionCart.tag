<%@ tag body-content="empty" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>


<label for="prospectiveCollectionNotes">
    <fmt:message key="specimenRequest.prospectiveCollectionNotes"/>
</label>
<s:textarea name="object.prospectiveCollectionNotes" id="prospectiveCollectionNotes"
    cols="20" rows="6" cssStyle="width:900px;" />

<div class="clear"><br /></div>