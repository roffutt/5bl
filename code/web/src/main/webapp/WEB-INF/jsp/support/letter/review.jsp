<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="supportLetterRequest.review.title" /></title>
</head>
<body>
    <c:url value="/admin/support/letter/list.action" var="listUrl"/>
    <div class="topbtns">
        <a href="${listUrl}" class="btn">
            &laquo; <fmt:message key="supportLetterRequest.review.back"/>
        </a>
    </div>

    <div class="important">
        <fmt:message key="supportLetterRequest.review.action.title"/>
        <ul style="margin-bottom:5px;">
            <li><b><fmt:message key="supportLetterRequest.review.action.respond"/></b></li>
        </ul>
    </div>

    <tissuelocator:messages/>

    <tissuelocator:supportLetterRequestDetails/>

    <div class="box pad10">
        <h2 class="formtop noline nobg"><fmt:message key="supportLetterRequest.response.title"/></h2>

        <div class="col50percent">
            <h4 class="green_bar"><fmt:message key="supportLetterRequest.response.online.title"/></h4>

            <s:form id="onlineLetterForm" namespace="/protected/support/letter" action="review.action"
                    enctype="multipart/form-data" method="POST">

                    <s:hidden key="object.id" />
                    <s:hidden name="object.status" value="RESPONDED"/>

                    <s:textarea name="object.response" id="response"
                    cols="20" rows="5" cssStyle="width:100%;margin-bottom:6px;"
                        required="true" requiredposition="right"
                        labelposition="top" label="%{getText('supportLetterRequest.response')}" labelSeparator=""/>

                <div class="attachments" style="width:445px; margin-bottom:10px;">
                    <label for="resume" class="label" style="font-style:normal;">
                        <fmt:message key="supportLetterRequest.letter"/>
                        <span class="reqd"><fmt:message key="required.indicator"/></span>
                    </label>

                    <div class="large_doc">
                        <s:file name="letter" id="letter" theme="simple"/>
                        <s:fielderror fieldName="object.letter" cssClass="fielderror"/>
                    </div>
                </div>

                    <s:submit value="%{getText('supportLetterRequest.review.online.submit')}" name="btn_submit"
                        id="btn_submit_online" cssClass="btn" theme="simple"
                        title="%{getText('supportLetterRequest.review.online.submit')}"/>
                </s:form>
            </div>

        <div class="col50percent" style="margin-left:15px; border-left:1px solid #ccc; padding-left:15px; height:25em;">
            <h4 class="green_bar"><fmt:message key="supportLetterRequest.response.offline.title"/></h4>

            <s:form id="offlineLetterForm" namespace="/protected/support/letter" action="review.action"
                    enctype="multipart/form-data" method="POST">

                    <s:hidden key="object.id" />
                    <s:hidden name="object.status" value="RESPONDED_OFFLINE"/>

                    <s:textarea name="object.offlineResponseNotes" id="offlineResponseNotes"
                    cols="20" rows="5" cssStyle="width:100%;margin-bottom:6px;"
                        required="true" requiredposition="right"
                        labelposition="top" labelSeparator=""
                        label="%{getText('supportLetterRequest.offlineResponseNotes')}"/>

                    <s:submit value="%{getText('supportLetterRequest.review.offline.submit')}" name="btn_submit"
                        id="btn_submit_offline" cssClass="btn" theme="simple"
                        title="%{getText('supportLetterRequest.review.offline.submit')}"/>
                </s:form>
            </div>

        <div class="clear"></div>

    </div>
</body>
</html>
