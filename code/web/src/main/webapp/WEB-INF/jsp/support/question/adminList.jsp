<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.review.list.title" /></title>
</head>
<body>
    <tissuelocator:requestListTabs isAdmin="true" selectedTab="question" subtitleKey="question.list.title"/>
    <tissuelocator:questionList isAdmin="true"/>
</body>
</html>
