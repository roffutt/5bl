<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="questionResponse.submit.title" /></title>
</head>
<body>
    <c:url value="/admin/support/question/list.action" var="listUrl"/>
    <div class="topbtns">
        <a href="${listUrl}" class="btn">
            &laquo; <fmt:message key="question.adminReview.back"/>
        </a>
    </div>

    <tissuelocator:messages/>

    <tissuelocator:supportQuestionDetails question="${object}"/>

    <s:set var="pendingStatus" value="%{@com.fiveamsolutions.tissuelocator.data.support.SupportRequestStatus@PENDING}"/>

    <div class="box pad10">
        <h2 class="formtop noline nobg"><fmt:message key="questionResponse.responses.title"/></h2>

        <s:form id="responseForm" namespace="/admin/support/question/" action="review.action"
            enctype="multipart/form-data" method="POST">
            <s:hidden key="object.id" />
            <s:hidden key="adminResponse.status" id="responseStatus"/>
            <s:hidden key="adminResponse.institution" value="%{#attr.TissueLocatorUser.institution.id}"/>
            <s:hidden key="adminResponse.question" value="%{object.id}"/>

            <table class="data">
                <thead>
                    <tr>
                        <th><div><fmt:message key="questionResponse.institution"/></div></th>
                        <th><div><fmt:message key="questionResponse.responder"/></div></th>
                        <th><div><fmt:message key="questionResponse.updated"/></div></th>
                        <th><div><fmt:message key="questionResponse.status"/></div></th>
                        <th><div><fmt:message key="questionResponse.response"/></div></th>
                    </tr>
                </thead>
                <tbody>
                    <s:iterator value="%{object.responses}" status="responseStatus">
                        <c:set var="rowCssClass" value=""/>
                        <c:set var="adminRowCssClass" value="odd"/>
                        <s:if test="%{#responseStatus.odd}">
                            <c:set var="rowCssClass" value="odd"/>
                            <c:set var="adminRowCssClass" value=""/>
                        </s:if>
                        <tr class="${rowCssClass}">
                            <td class="title"><s:property value="%{institution.name}"/></td>
                            <td><s:property value="%{responder.displayName}"/></td>
                            <td>
                                <s:date name="%{lastUpdatedDate}" format="%{getText('default.date.format')}"/>
                            </td>
                            <td>
                                <s:property value="%{getText(previousStatus.administratorResourceKey)}"/>
                            </td>
                            <td><s:property value="%{response}"/></td>
                        </tr>
                    </s:iterator>
                    <s:if test="%{!object.hasAdminResponse()}">
                        <tr class="${adminRowCssClass}">
                            <td class="title"><s:property value="%{#attr.TissueLocatorUser.institution.name}"/></td>
                            <td><s:property value="%{adminResponse.responder.displayName}"/></td>
                            <td>
                                <s:date name="%{adminResponse.lastUpdatedDate}"
                                    format="%{getText('default.date.format')}"/>
                            </td>
                            <td>
                                <s:property value="%{getText(#pendingStatus.administratorResourceKey)}"/>
                            </td>
                            <td>
                                <s:textarea name="adminResponse.response" id="response"
                                    value="%{adminResponse.response}" theme="simple"
                                    style="width:100%;margin-bottom:6px;" cols="20" rows="5"
                                    title="%{getText('questionResponse.response')}"/>
                                <s:fielderror fieldName="adminResponse.response" cssClass="fielderror"/>
                                <s:fielderror fieldName="adminResponse.responseValid" cssClass="fielderror"/>
                                <s:submit value="%{getText('questionResponse.submit.online')}"
                                    name="btn_save_online" cssClass="btn" theme="simple"
                                    onclick="$('#responseStatus').val('RESPONDED');"
                                    title="%{getText('questionResponse.submit.online')}"/>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <s:submit value="%{getText('questionResponse.submit.offline')}"
                                    name="btn_save_offline" cssClass="btn" theme="simple"
                                    onclick="$('#responseStatus').val('RESPONDED_OFFLINE');"
                                    title="%{getText('questionResponse.submit.offline')}"/>
                            </td>
                        </tr>
                    </s:if>
                </tbody>
            </table>
        </s:form>
    </div>
</body>
</html>
