<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>
<html>
<head>
    <title><fmt:message key="question.edit.title" /></title>
</head>
<body>

    <tissuelocator:supportMenu selectedPage="questions"/>

    <div id="maincontent" style="padding-bottom:0px;">
        <h1><fmt:message key="question.edit.title"/></h1>

        <div class="pad10 note">
            <c:url value="/images/arrow_right.gif" var="rightArrowUrl"/>
            <h4><fmt:message key="question.edit.why"/></h4>
            <div>
                <img src="${rightArrowUrl}" align="absmiddle" width="12" height="12" />
                <fmt:message key="question.edit.why.one"/>
            </div>
            <div>
                <img src="${rightArrowUrl}" align="absmiddle" width="12" height="12" />
                <fmt:message key="question.edit.why.two"/>
            </div>
            <div>
                <img src="${rightArrowUrl}" align="absmiddle" width="12" height="12" />
                <fmt:message key="question.edit.why.three"/>
            </div>
            <div>
                <img src="${rightArrowUrl}" align="absmiddle" width="12" height="12" />
                <fmt:message key="question.edit.why.four"/>
            </div>
            <div>
                <img src="${rightArrowUrl}" align="absmiddle" width="12" height="12" />
                <fmt:message key="question.edit.why.five"/>
            </div>
            <br />

            <h4><fmt:message key="question.edit.who"/></h4>
            <div>
                <img src="${rightArrowUrl}" align="absmiddle" width="12" height="12" />
                <fmt:message key="question.edit.who.one"/>
            </div>
            <br />

            <div>
                <fmt:message key="quesiton.edit.moreInfo">
                    <fmt:param><c:url value="/support/resources.action"/></fmt:param>
                    <fmt:param><fmt:message key="footer.support.email"/></fmt:param>
                </fmt:message>
            </div>

        </div>
        <br />

        <div class="box pad10">
            <s:form id="letterForm" namespace="/protected/support/question" action="save.action"
                enctype="multipart/form-data" method="POST">
                <tissuelocator:messages/>

                <s:hidden name="object.id" />
                <s:hidden name="object.requestor" value="%{#attr.TissueLocatorUser.id}"/>
                <s:hidden name="object.status" value="PENDING"/>

                <div id="investigator_edit">
                    <h4 class="green_bar"><fmt:message key="question.investigator.title"/></h4>

                    <div class="pad10 select_container">
                        <div class="note">
                            <strong><fmt:message key="question.investigator.note.title"/></strong>
                            <fmt:message key="question.investigator.note"/>
                        </div>
                        <br />

                        <c:set var="resumeFile" value="${object.resume}"/>
                        <s:if test="%{hasErrors()}">
                            <c:set var="resumeFile" value="${TissueLocatorUser.resume}"/>
                        </s:if>
                        <tissuelocator:editPerson copyEnabled="true" propertyPrefix="object.investigator"
                            resourcePrefix="question.investigator" idPrefix="investigator"
                            countryValue="${object.investigator.address.country}"
                            stateValue="${object.investigator.address.state}"
                            showInstitution="true" showFax="false"
                            showResume="true" resumeFile="${resumeFile}"/>

                        <div class="line"></div>

                        <div class="formcol" style="width:320px">
                            <s:file name="protocolDocument" id="protocolDocument" size="28"
                                label="%{getText('question.protocolDocument')}" labelposition="top" labelSeparator=""/>
                            <s:fielderror fieldName="object.protocolDocument" cssClass="fielderror"/>
                        </div>

                        <div class="clear" style="height:10px;"></div>
                        <div class="line"></div>
                    </div>

                    <div class="clear"></div>
                </div>

                <div class="line"></div>

                <div id="letter_of_support">
                    <h4 class="green_bar"><fmt:message key="question.question.title"/></h4>

                    <div class="pad10">

                        <div class="formcol select_container" style="width:300px; display:none"
                            id="selectedInstitutionsWrapper">
                            <s:select name="selectedInstitutions" id="selectedInstitutions"
                                    value="%{selectedInstitutionIds}"
                                    list="%{institutions}" listKey="id" listValue="name"
                                    multiple="true" required="true" requiredposition="right"
                                    labelSeparator="" labelposition="top"
                                    label="%{getText('question.question.to')}" />
                            <script type="text/javascript">
                                $(function(){
                                  $("#selectedInstitutions").multiselect({
                                        selectedList: 1,
                                        minWidth: 280,
                                        noneSelectedText: '<fmt:message key="select.emptyOption"/>',
                                        position: {
                                            my: 'left top',
                                            at: 'left bottom'
                                        }
                                    });
                                });
                                $(document).ready(function() {
                                    $('#selectedInstitutionsWrapper').show();
                                });
                            </script>
                        </div>

                        <div class="line"></div>

                        <s:textarea name="object.questionText" id="questionText"
                            cols="50" rows="3" cssStyle="width:100%; height:100px"
                            required="true" requiredposition="right"
                            labelposition="top" label="%{getText('question.questionText')}" labelSeparator=""/>

                        <div class="clear"><br /></div>
                        <div class="line"></div>

                        <div class="btn_bar">
                            <s:submit value="%{getText('question.submit')}" name="btn_submit"
                                id="btn_submit" cssClass="btn" theme="simple" title="%{getText('question.submit')}"/>
                        </div>

                    </div>
                </div>
            </s:form>
        </div>
    </div>

</body>
</html>
