<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.myrequests.title" /></title>
</head>
<body>
    <tissuelocator:requestListTabs isAdmin="false" selectedTab="question" subtitleKey="question.list.title"/>
    <div class="topbtns">
        <a href="<c:url value="/protected/support/question/input.action"/>" class="btn">
            <fmt:message key="question.list.add"/>
        </a>
    </div>
    <tissuelocator:questionList isAdmin="false"/>
</body>
</html>
