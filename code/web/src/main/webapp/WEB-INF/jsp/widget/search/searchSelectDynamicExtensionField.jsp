<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:set var="propName" value="searchFieldConfig.searchFieldName"/>
<s:set var="isObjectProperty" value="%{searchFieldConfig.objectProperty}"/>
<s:set var="helpBaseUrl" value="searchFieldConfig.helpBaseUrl"/>
<s:if test="%{isObjectProperty}">
 <s:set var="propName" value="%{'object.' + #propName}"/>
</s:if>

<s:set var="label">${searchFieldConfig.searchFieldDisplayName}</s:set>
    
<td scope="col" class="label" nowrap="nowrap">
    <tissuelocator:helpLabel helpUrlBase="${helpBaseUrl}" 
                             labelFor="${propName}" 
                             fieldDisplayName="${label}"
                             fieldName="${propName}"
                             required="false"/>
</td> 
<td scope="col" class="value">
    <tissuelocator:searchSelect name="${propName}" selectSearchConfig="${searchFieldConfig}" 
        optionList="${selectOptions}"/>
        
    <s:if test="%{searchFieldConfig.note != null}">
        ${searchFieldConfig.note}
    </s:if>
    <s:fielderror cssClass="fielderror">
        <s:param value="%{propName}"/>
    </s:fielderror>
    <s:if test="%{searchFieldConfig.errorProperty != null}">
        <s:fielderror cssClass="fielderror">
            <s:param value="%{searchFieldConfig.errorProperty}"/>
        </s:fielderror>
    </s:if>
</td> 