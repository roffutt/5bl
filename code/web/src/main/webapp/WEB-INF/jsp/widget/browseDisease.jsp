<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<div id="disease">
    <h2><fmt:message key="home.browse.disease"/></h2>
    <div id="disease_scrollbox">
        <ul>
            <s:set var="displayCount" value="%{displayDiseaseCount}"/>
            <s:if test="%{#displayCount}">
                <s:set var="pcEntrySet" value="%{countsByPathologicalFinding.entrySet()}"/>
            </s:if>
            <s:else>
                <s:set var="pcEntrySet" value="%{pathologicalCharacteristics.entrySet()}"/>
            </s:else>
            <s:iterator value="%{#pcEntrySet}" var="pcData">
                <s:url action="specimen/search/filter.action" namespace="/protected" var="diseaseUrl">
                    <s:param name="object.pathologicalCharacteristic.name">
                        <s:property value="%{#pcData.key}"/>
                    </s:param>
                    <s:param name="object.pathologicalCharacteristic.id">
                        <s:if test="%{#displayCount}">
                            <s:property value="%{#pcData.value[0]}"/>
                        </s:if>
                        <s:else>
                            <s:property value="%{#pcData.value}"/>
                        </s:else>
                    </s:param>
                </s:url>
                <li>
                    <a href="${diseaseUrl}">
                        <s:property value="%{#pcData.key}"/>
                        <s:if test="%{#displayCount}">
                            <span class="count">
                                (<s:property value="%{#pcData.value[1]}"/>)
                            </span>
                        </s:if>
                    </a>
                </li>
            </s:iterator>
        </ul>
    </div>
</div>
