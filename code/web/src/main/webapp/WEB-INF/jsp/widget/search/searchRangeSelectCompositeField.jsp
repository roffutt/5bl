<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<s:set var="minPropName" value="%{searchFieldConfig.rangeConfig.minSearchFieldName}"/>
<s:set var="maxPropName" value="%{searchFieldConfig.rangeConfig.maxSearchFieldName}"/>
<s:set var="selectPropName" value="selectConfig.searchFieldName"/>
<s:set var="isRangeObjectProperty" value="%{searchFieldConfig.rangeConfig.objectProperty}"/>
<s:set var="isSelectObjectProperty" value="%{selectConfig.objectProperty}"/>
<s:set var="helpBaseUrl" value="searchFieldConfig.helpBaseUrl"/>

<s:if test="%{isRangeObjectProperty}">
 <s:set var="minPropName" value="%{'object.' + #minPropName}"/>
 <s:set var="maxPropName" value="%{'object.' + #maxPropName}"/>
</s:if>
<s:if test="%{isSelectObjectProperty}">
 <s:set var="selectPropName" value="%{'object.' + #selectPropName}"/>
</s:if>

<s:set var="label">${searchFieldConfig.rangeConfig.searchFieldDisplayName}</s:set>

<td scope="col" class="label" nowrap="nowrap">
    <tissuelocator:helpLabel helpUrlBase="${helpBaseUrl}" 
                             labelFor="${searchFieldConfig.rangeConfig.searchFieldName}" 
                             fieldDisplayName="${label}"
                             fieldName="${searchFieldConfig.rangeConfig.searchFieldName}"
                             required="false"/>
</td> 
<td scope="col" class="value">
    <s:textfield name="%{'rangeSearchParameters[\\'' + #minPropName + '\\']'}"
        id="%{minPropName}"
        theme="simple"
        size="15"
        title="%{getText('specimen.search.min.field')} %{searchFieldConfig.rangeConfig.searchFieldDisplayName}"
        cssClass="%{searchFieldConfig.rangeConfig.cssClasses}"/>
    <span class="${searchFieldConfig.rangeConfig.cssClasses}">&ndash;</span>
    <s:textfield name="%{'rangeSearchParameters[\\'' + #maxPropName + '\\']'}"
        id="%{maxPropName}" theme="simple"
        size="15"
        title="%{getText('specimen.search.max.field')} %{searchFieldConfig.rangeConfig.searchFieldDisplayName}"
        cssClass="%{searchFieldConfig.rangeConfig.cssClasses}"/>
    <tissuelocator:searchSelect name="${selectPropName}" selectSearchConfig="${selectConfig}" 
        optionList="${selectOptions}"/>
        
    <s:fielderror cssClass="fielderror">
        <s:param value="%{minPropName}"/>
    </s:fielderror>
    <s:fielderror cssClass="fielderror">
        <s:param value="%{maxPropName}"/>
    </s:fielderror>
    <s:if test="%{searchFieldConfig.rangeConfig.errorProperty != null}">
        <s:fielderror cssClass="fielderror">
            <s:param value="%{searchFieldConfig.rangeConfig.errorProperty}"/>
        </s:fielderror>
    </s:if>
</td>