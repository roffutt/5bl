<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:url value="/static/overview.jsp" var="overviewUrl"/>
<c:url value="/protected/specimen/search/refreshList.action?simpleSearch=true" var="simpleSearchUrl"/>
<c:url value="/protected/specimen/search/refreshList.action" var="advancedSearchUrl"/>
<c:url var="imagesBase" value="/images"/>
<c:url value="/static/howToUse.jsp" var="howToUrl"/>

<div id="search_home">
    <div id="nosearch_infotabs" class="tabbed">
        <ul id="infotabs" class="infotabs">
            <li class="tab"><a href="#search" id="tab_search">Search</a></li>
            <li class="tab"><a href="#about_nbstrn" id="tab_about_nbstrn"><fmt:message key="login.tab.about"/></a></li>
            <li class="tab"><a href="#how_to_use_site" id="tab_how_to_use_site"><fmt:message key="login.tab.how_to_use_site"/></a></li>
        </ul>

        <div id="tabbox">

            <!--Search-->
            <div id="search">
                <p>
                    <fmt:message key="nosearch.tab.text">
                        <fmt:param value="${simpleSearchUrl}"/>
                        <fmt:param value="${advancedSearchUrl}"/>
                    </fmt:message>
                </p>
                <div class="home_search_btns">
                    <a id="btn_home_search_collection" href="${simpleSearchUrl}">
                       <img src="<c:url value="/images/1px_trans.gif" />"
                         alt="General Population Search" width="172" height="54" />
                    </a>
                    <a id="btn_home_search_advanced" href="${advancedSearchUrl}">
                        <img src="<c:url value="/images/1px_trans.gif" />"
                            alt="Search for DBS Specimens" width="172" height="54" />
                    </a>
                </div>
            </div>
            <!--/Search-->

            <!--About the NBSTRN tab-->
            <div id="about_nbstrn">
                <p>
                    <fmt:message key="login.tab.about.text"/>
                </p>
                <p>
                    <fmt:message key="login.tab.about.foot"/>
                </p>
            </div>
            <!--/About the NBSTRN tab-->
            
            <!--how to use this site tab-->
            <div id="how_to_use_site" style="padding: 0; text-align:center;">
                <a href="${howToUrl}">
                    <img src="${imagesBase}/home-how-to-use-this-site.png" alt="${login.tab.how_to_use_site.long}" id="how_to_use_site_img"/>
                </a>
            </div>
            <!--/how to use this site tab-->

            </div>

    </div>
</div>
<!--Tabs-->
<script type="text/javascript">
    $(document).ready(function () {
        $('#nosearch_infotabs').tabs();
        $('#nosearch_infotabs').removeClass('ui-widget ui-widget-content');
        $('#infotabs').removeClass('ui-tabs-nav ui-widget-header');
    });
</script>
<!--/Tabs-->
