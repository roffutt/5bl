<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="mta.title" /></title>
    <script type="text/javascript">        
        function submitMtaUploadForm() {            
           return confirm('<fmt:message key="mta.save.irreversible.warning" />') ;
        }
    </script>
</head>
<body onload="setFocusToFirstControl();">

    <c:url value="/admin/mta/input.action" var="addMtaUrl"/>
    <c:url value="/admin/signedMta/list.action" var="reviewMtaUrl"/>
    <div class="tabbox">
        <a href="${addMtaUrl}" class="selected"><fmt:message key="mta.subtitle"/></a>
        <a href="${reviewMtaUrl}"><fmt:message key="signedMta.subtitle"/></a>
        <div class="clear"></div>
    </div>

    <h2 class="formtop tabbedtop"><fmt:message key="mta.subtitle"/></h2>
    <p><fmt:message key="mta.title.note"/></p>
    <br />

    <tissuelocator:messages/>
    <fmt:message key="default.date.format" var="dateFormat"/>
            
    <div id="mta_consort_admin">
        <div class="box uploadbox">
            <h3><fmt:message key="mta.upload"/></h3>
            <div class="line"></div>
            <s:form id="mtaForm" action="/admin/mta/save.action" enctype="multipart/form-data" method="POST"
                    onsubmit="return submitMtaUploadForm();">

                <s:file name="document" id="document" size="30" cssStyle="width:20em"
                    required="true" requiredposition="right"
                    label="%{getText('mta.document')}" labelposition="top" labelSeparator=""/>
                <div class="note"><fmt:message key="mta.document.note"/></div>

                <sj:datepicker id="version" name="mta.version"
                    displayFormat="%{#dateFormat}" cssStyle="width:160px"
                    labelSeparator="" labelposition="top" label="%{getText('mta.version')}" 
                    buttonImageOnly="true" changeMonth="true" changeYear="true"
                    required="true" requiredposition="right"/>

                <div class="line"></div>

                <div class="btn_bar">
                    <s:submit value="%{getText('mta.save')}" name="btn_save"
                        cssClass="btn" theme="simple" title="%{getText('mta.save.title')}"/>
                </div>

            </s:form>
        </div>

        <div class="box downloadbox">

            <h3><fmt:message key="mta.download"/></h3>
            <fmt:message key="mta.link.title" var="linkTitle"/>
            <fmt:message key="mta.list.summary" var="mtaListSummary"/>
            <display:table name="mtas" uid="mta" style="width:560px;" class="data" summary="${mtaListSummary}">

                <fmt:message key="mta.list.version" var="versionTitle"/>
                <c:set var="versionTitle"><div class="nosort">${versionTitle}</div></c:set>
                <display:column title="${versionTitle}">
                    <fmt:formatDate pattern="${dateFormat}" value="${mta.version}"/>
                </display:column>

                <fmt:message key="mta.list.uploadTime" var="uploadTimeTitle"/>
                <c:set var="uploadTimeTitle"><div class="nosort">${uploadTimeTitle}</div></c:set>
                <display:column title="${uploadTimeTitle}">
                    <fmt:formatDate pattern="${dateFormat}" value="${mta.uploadTime}"/>
                </display:column>

                <c:set var="actionTitle"><div class="nosort"></div></c:set>
                <display:column title="${actionTitle}" headerClass="action" class="action">
                    <c:url value="/protected/downloadFile.action" var="downloadMtaUrl">
                        <c:param name="file.id" value="${mta.document.lob.id}" />
                        <c:param name="fileName" value="${mta.document.name}" />
                        <c:param name="contentType" value="${mta.document.contentType}" />
                    </c:url>
                    <a href="${downloadMtaUrl}" class="btn_fw_action_col" title="${linkTitle}">
                        <fmt:message key="mta.action.download"/>
                    </a>
                </display:column>

            </display:table>

        </div>

    </div>
    <div class="clear"><br /></div>

</body>
</html>
