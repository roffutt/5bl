<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="mta.title" /></title>
</head>
<body onload="setFocusToFirstControl();">

    <c:url value="/admin/mta/input.action" var="addMtaUrl"/>
    <c:url value="/admin/signedMta/list.action" var="reviewMtaUrl"/>
    <div class="tabbox">
        <a href="${addMtaUrl}"><fmt:message key="mta.subtitle"/></a>
        <a href="${reviewMtaUrl}" class="selected"><fmt:message key="signedMta.subtitle"/></a>
        <div class="clear"></div>
    </div>

    <h2 class="formtop tabbedtop"><fmt:message key="signedMta.subtitle"/></h2>

    <tissuelocator:messages/>

    <div class="box">

        <s:form id="filterForm" action="/admin/signedMta/filter.action">
            <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
            <fmt:message key="default.date.format" var="dateFormat"/>
            <display:table name="objects" requestURI="/admin/signedMta/list.action"
                uid="signedMta" pagesize="${tablePageSize}" sort="external">
                <tissuelocator:displaytagBoxProperties/>

                <display:column titleKey="signedMta.list.uploadTime" sortable="true" sortProperty="UPLOAD_TIME">
                    <fmt:formatDate pattern="${dateFormat}" value="${signedMta.uploadTime}"/>
                </display:column>
                <display:column titleKey="signedMta.list.institution" property="receivingInstitution.name"
                    sortProperty="RECEIVING_INSTITUTION" sortable="true"/>
                <display:column titleKey="signedMta.list.uploader.name"
                    sortable="true" sortProperty="UPLOADER_FIRST_NAME,UPLOADER_LAST_NAME">
                    ${signedMta.uploader.firstName} ${signedMta.uploader.lastName}
                </display:column>
                <display:column titleKey="signedMta.list.uploader.email" property="uploader.email"
                    sortProperty="UPLOADER_EMAIL" sortable="true"/>
                <display:column titleKey="signedMta.list.uploader.phone"
                    sortProperty="UPLOADER_PHONE" sortable="true">
                    ${signedMta.uploader.address.phone}
                    <tissuelocator:phoneExtensionDisplay extension="${signedMta.uploader.address.phoneExtension}"/>  
               </display:column>

                <display:column titleKey="column.action" headerClass="action" class="action">
                    <c:url var="reviewUrl" value="/admin/signedMta/review/list.action">
                        <c:param name="signedMta.id" value="${signedMta.id}"/>
                    </c:url>
                    <a href="${reviewUrl}" class="btn_fw_action_col"><fmt:message key="btn.review" /></a>
                </display:column>
            </display:table>
        </s:form>

    </div>

</body>
</html>
