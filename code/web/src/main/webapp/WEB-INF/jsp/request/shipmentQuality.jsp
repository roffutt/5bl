<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title>
        <fmt:message key="shipment.updateSpecimenQuality.title">
            <fmt:param value="${object.id}"/>
            <fmt:param>
                <fmt:message key="${object.status.resourceKey}"/>
            </fmt:param>
        </fmt:message>
    </title>
    <script type="text/javascript">
        var dispositionArray = new Array();
        function submitQualityForm() {
            var markedOneAsDeleted = false;
          for ( var i = 0; i < dispositionArray.length; ++i ) {
                var dispositionSelect = $('#qualityForm_lineItems_' + i + '__disposition').get(0);
                var dispositionSelectValue = dispositionSelect.options[dispositionSelect.options.selectedIndex].value;
            if (dispositionArray[i] != 'DAMAGED' && dispositionArray[i] != 'PARTIALLY_CONSUMED_DESTROYED'
                        && (dispositionSelectValue == 'DAMAGED' || dispositionSelectValue == 'PARTIALLY_CONSUMED_DESTROYED')){
              markedOneAsDeleted = true;
            }
        }

            if (markedOneAsDeleted) {
              if(!confirm('<fmt:message key="shipment.updateSpecimenQuality.irreversible.warning" />')) {
                    return false;
                }
            }
            return true;
        }
    </script>
</head>
<body>
    <div class="topbtns">
        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/view" urlEnding=".action"/>
        <c:url var="backUrl" value="${attr.request.outputUrl}">
            <c:param name="object.id" value="${object.request.id}"/>
        </c:url>
        <a href="${backUrl}" class="btn"><fmt:message key="btn.backToRequest" /></a>
    </div>

    <!--Instructions-->

    <p><fmt:message key="shipment.updateSpecimenQuality.instructions"></fmt:message></p>


    <ul>
        <li class="small"><em><strong><fmt:message key="shipment.updateSpecimenQuality.instructions.rq"/></strong> &ndash; <fmt:message key="shipment.updateSpecimenQuality.instructions.rq.message"/></em></li>
        <li class="small"><em><strong><fmt:message key="shipment.updateSpecimenQuality.instructions.fd"/></strong> &ndash; <fmt:message key="shipment.updateSpecimenQuality.instructions.fd.message"/></em></li>
    </ul>

    <br />

    <br />

    <!--/Instructions-->



    <tissuelocator:messages breakCount="2"/>

    <s:form action="/protected/request/updateShipmentQuality.action" id="qualityForm" onsubmit="if (!submitQualityForm()) { return false; }">
        <s:hidden name="object.id" />

        <c:set var="tableId" value="request"/>
        <s:if test="%{displayAggregateSearchResults}">
            <c:set var="tableId" value="aggregaterequest"/>
        </s:if>
        <table class="data request" id="${tableId}">
            <s:if test="%{displayAggregateSearchResults}">
                    <s:set var="requestLineItems" value="%{specificLineItemList}"/>
                    <c:set var="lineItemName" value="specificLineItems"/>
                    <c:if test="${not empty requestLineItems}">
                        <tissuelocator:shipmentQualityTable requestLineItems="${requestLineItems}"
                            lineItemName="${lineItemName}"/>
                    </c:if>

                    <s:set var="requestLineItems" value="%{generalLineItemList}"/>
                    <c:set var="lineItemName" value="generalLineItems"/>
                    <c:if test="${not empty requestLineItems}">
                        <tissuelocator:shipmentQualityTable requestLineItems="${requestLineItems}"
                            lineItemName="${lineItemName}"/>
                    </c:if>
                </s:if>
            <s:else>
                <c:set var="requestLineItems" value="${object.lineItems}"/>
                <c:set var="lineItemName" value="lineItems"/>

                <tissuelocator:shipmentQualityTable requestLineItems="${requestLineItems}" lineItemName="${lineItemName}"/>
            </s:else>
        </table>

    </s:form>
</body>
</html>
