<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimenRequest.lineItems" /></title>
</head>
<body>
   <s:set name="specificLineItems" value="%{getSpecificLineItemsByInstitution(#attr.institution)}"/>
   <s:set name="generalLineItems" value="%{getGeneralLineItemsByInstitution(#attr.institution)}"/>
   <tissuelocator:aggregateCartTable specificLineItems="${specificLineItems}" generalLineItems="${generalLineItems}" cartPrefix="aggregate" showOrderFulfillmentForms="false" showFinalPriceColumn="false"/>
</body>
