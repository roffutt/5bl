<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<html>
    <head>
        <title>
            <fmt:message key="prospectiveCollection.title"/>
        </title>
    </head>
    <body>
    
    
        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/addProspectiveCollectionToCart" urlEnding=".action"/>
        <s:form theme="simple" action="%{#attr.request.outputUrl}">
        
        <c:url var="searchUrl" value="/protected/specimen/search.action"/>
        <p><fmt:message key="prospectiveCollection.message"><fmt:param>${searchUrl}</fmt:param></fmt:message></p>
        
        <label for="prospectiveCollectionNotes">
            <fmt:message key="specimenRequest.prospectiveCollectionNotes"/>
        </label>
        <s:textarea name="object.prospectiveCollectionNotes" id="prospectiveCollectionNotes"
            cols="20" rows="6" cssStyle="width:900px;" />

        <div class="clear"><br /></div>
        
        <div class="btn_bar">
            <s:submit value="%{getText('prospectiveCollection.addToCart')}" title="%{getText('prospectiveCollection.addToCart.button')}"
                name="btn_addtocart" id="btn_addtocart" cssClass="btn" theme="simple"/>
        </div>
                
        </s:form>
    </body>
    
</html>