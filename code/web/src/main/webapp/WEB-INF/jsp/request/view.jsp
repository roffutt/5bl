<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title>
        <fmt:message key="specimenRequest.view.title">
            <fmt:param value="${object.id}"/>
        </fmt:message>
         (<fmt:message key="${object.status.resourceKey}"/>)
    </title>
</head>
<body onload="setFocusToFirstControl();">

    <div class="topbtns">
        <c:if test="${object.requestor.id != TissueLocatorUser.id}">
          <req:isUserInRole role="tissueRequestAdmin">
              <c:url var="reviewUrl" value="/admin/request/review/input.action">
                  <c:param name="object.id" value="${object.id}"/>
              </c:url>
              <a href="${reviewUrl}" class="btn"><fmt:message key="btn.addReview" /></a>
          </req:isUserInRole>
        </c:if>
        <a href="#" onclick="history.back();return false" class="btn">
            <fmt:message key="btn.back"/>
        </a>
    </div>

    <c:set var="needsQualityUpdate" value="${false}"/>
    <c:forEach items="${object.orders}" var="order">
        <s:if test="%{#attr.order.needsQualityUpdate(#attr.TissueLocatorUser)}">
            <c:set var="needsQualityUpdate" value="${true}"/>
        </s:if>
    </c:forEach>


    <c:if test="${needsQualityUpdate}">
        <div class="topimportant">
            <p><fmt:message key="specimenRequest.review.outstanding"/><br></p>
            <ul>
                <c:if test="${needsQualityUpdate}">
                    <li style="font-size:100%">
                        <fmt:message key="specimenRequest.review.outstanding.qualityUpdate"/>
                    </li>
                </c:if>
            </ul>
        </div>
    </c:if>

    <tissuelocator:votingResultsDisplay specimenRequest="${object}" />

    <s:if test="%{!object.orders.isEmpty()}">
        <h2 class="formtop">
            <a href="javascript:;" onclick="toggleVisibility('orders');" class="toggle">
               <fmt:message key="specimenRequest.view.orders"/>
            </a>
        </h2>

        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@SHIPPED}" name="shipped" scope="page"/>
        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@RECEIVED}" name="received" scope="page"/>

        <div id="orders">
            <display:table name="${object.orders}" uid="shipment" defaultsort="1">
                <tissuelocator:displaytagProperties/>
                <fmt:message key="shipment.id" var="idColumnTitle"/>
                <display:column title="<div>${idColumnTitle}</div>">
                    <c:url var="viewUrl" value="/protected/request/viewOrder.action">
                        <c:param name="object.id" value="${shipment.id}"/>
                    </c:url>
                    <a href="${viewUrl}">${shipment.id}</a>
                </display:column>
                <fmt:message key="shipment.sendingInstitution.name" var="institutionColumnTitle"/>
                <display:column title="<div>${institutionColumnTitle}</div>">
                    <tissuelocator:institutionName institution="${shipment.sendingInstitution}"/>
                </display:column>
                <fmt:message key="shipment.status" var="statusColumnTitle"/>
                <fmt:message key="date.format.display" var="dateFormat"/>
                <display:column title="<div>${statusColumnTitle}</div>">
                    <fmt:message key="${shipment.status.resourceKey}" />
                    <c:if test="${(shipment.status == shipped) && (!empty shipment.shipmentDate)}">
                        <fmt:formatDate pattern="${dateFormat}" value="${shipment.shipmentDate}"/>
                    </c:if>
                </display:column>
                <fmt:message key="shipment.trackingNumber" var="trackingNumberColumnTitle"/>
                <display:column title="<div>${trackingNumberColumnTitle}</div>">
                    <tissuelocator:orderTrackingLink order="${shipment}" showLabel="false"/>
                </display:column>
                <fmt:message key="shipment.specimenCount" var="countColumnTitle"/>
                <display:column title="<div>${countColumnTitle}</div>">
                    <c:url var="viewLineItemsUrl" value="/protected/request/popup/viewLineItems.action">
                        <c:param name="object.id" value="${shipment.id}"/>
                    </c:url>
                    <a href="javascript: showPopWin('${viewLineItemsUrl}', 700, 400, null);">
                        <s:property value="%{#attr.shipment.specimenCount}"/>
                    </a>
                </display:column>
                <display:column>
                <c:if test="${shipment.status == shipped || shipment.status == received}">
                  <c:url var="qualityUrl" value="/protected/request/viewShipmentQuality.action">
                      <c:param name="object.id" value="${shipment.id}"/>
                  </c:url>
                  <a href="${qualityUrl}" class="btn tiny">
                      <fmt:message key="shipment.updateSpecimenQuality"/>
                  </a>
                </c:if>
                </display:column>
            </display:table>
        </div>
    </s:if>

    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@DRAFT}" name="draft" scope="page"/>
    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING}" name="pending" scope="page"/>
    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_REVISION}"
        name="pendingRevision" scope="page"/>
    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_FINAL_DECISION}"
        name="pendingDecision" scope="page"/>
    <c:set var="displayVotingResults" value="false"/>
    <c:if test="${object.status != draft && object.status != pendingDecision}">
        <s:if test="%{displayRequestReviewVotes}">
            <c:set var="displayVotingResults" value="true"/>
        </s:if>
    </c:if>

    <tissuelocator:aggregateLineItemDisplay displayVotingResults="${displayVotingResults}"/>

    <s:set var="hasOrderLineItems" value="%{!object.orderLineItems.isEmpty()}" scope="page"/>
    <c:if test="${hasOrderLineItems && displayVotingResults == 'true'}">
        <h2 class="formtop">
            <a href="javascript:;" onclick="toggleVisibility('ordercart');" class="toggle">
               <fmt:message key="specimenRequest.lineItems"/>
            </a>
        </h2>

        <tissuelocator:cartTable showFinalPriceColumn="false" requestLineItems="${object.orderLineItems}"
            showOrderFulfillmentForms="false" showSpecimenLinks="true" cartPrefix="order"/>
        <br />
    </c:if>

    <s:set var="hasLineItems" value="%{!object.lineItems.isEmpty()}" scope="page"/>
    <c:if test="${hasLineItems}">
        <fmt:message key="specimenRequest.view.unavailable" var="lineItemsLabel"/>
        <c:if test="${object.status == pending || object.status == pendingRevision || object.status == pendingDecision}">
            <fmt:message key="specimenRequest.lineItems" var="lineItemsLabel"/>
        </c:if>
        <h2 class="formtop">
            <a href="javascript:;" onclick="toggleVisibility('cart');" class="toggle">
               ${lineItemsLabel}
            </a>
        </h2>
    </c:if>

    <tissuelocator:requestReadOnlyView specimenRequest="${object}" showCart="${hasLineItems}"
        displayVotingResults="${displayVotingResults}"/>

    <div class="clear"><br/></div>
    <div class="btn_bar">
        <c:if test="${object.requestor.id != TissueLocatorUser.id}">
          <req:isUserInRole role="tissueRequestAdmin">
              <c:url var="reviewUrl" value="/admin/request/review/input.action">
                  <c:param name="object.id" value="${object.id}"/>
              </c:url>
              <a href="${reviewUrl}" class="btn"><fmt:message key="btn.addReview" /></a>
          </req:isUserInRole>
        </c:if>
        <a href="#" onclick="history.back();return false" class="btn">
            <fmt:message key="btn.back"/>
        </a>
    </div>
    <div class="clear"></div>
</body>
</html>
