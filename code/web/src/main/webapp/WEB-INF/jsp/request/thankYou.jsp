<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>

<html>
    <head>
        <title><fmt:message key="specimenRequest.thankyou.title" /></title>
    </head>
    <body>

        <tissuelocator:requestProcessSteps selected="receipt"/>
        <h1><fmt:message key="specimenRequest.thankyou.title" /></h1>

        <tissuelocator:determinePersistenceType urlPrefix="/protected/request/list" urlEnding=".action" asUrl="false"/>
        <c:url value="${attr.request.outputUrl}" var="myRequestsUrl">
            <c:param name="object.requestor" value="${TissueLocatorUser.id}" />
        </c:url>
        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.RequestStatus@PENDING_REVISION}"
                name="pendingRevision" scope="page"/>
        <div class="topconfirm">
            <p>
                <c:choose>
                    <c:when test="${previousStatus == pendingRevision}">
                        <fmt:message key="specimenRequest.thankyou.topconfirm.revision"/>
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="specimenRequest.thankyou.topconfirm"/>
                    </c:otherwise>
                </c:choose>
            </p>
        </div>

        <div class="line"></div>

            <div id="requestform">

                <h2 class="noline_top">
                    <fmt:message key="specimenRequest.thankyou.id"/>:
                    <s:property value="%{object.id}"/>
                </h2>

                <p><fmt:message key="specimenRequest.thankyou.text1"/></p>

                <h3><fmt:message key="specimenRequest.thankyou.next"/></h3>
                <p>
                    <fmt:message key="specimenRequest.thankyou.text2"/>
                    <a href="${myRequestsUrl}">
                        <fmt:message key="header.requests"/>
                    </a>
                    <fmt:message key="specimenRequest.thankyou.text3"/>
                </p>

                <ul>
                    <li>
                        <a href="${myRequestsUrl}">
                            <fmt:message key="specimenRequest.thankyou.view"/>
                        </a>
                    </li>
                    <li>
                        <c:url value="/protected/specimen/search/list.action" var="browseUrl"/>
                        <a href="${browseUrl}">
                            <fmt:message key="specimenRequest.thankyou.browse"/>
                        </a>
                    </li>
                    <li>
                        <c:url value="/protected/home.action" var="homeUrl"/>
                        <a href="${homeUrl}">
                            <fmt:message key="specimenRequest.thankyou.home"/>
                        </a>
                    </li>
                </ul>

            </div>
    </body>
</html>
