<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title>
        <fmt:message key="shipment.order.title">
            <fmt:param value="${object.id}" />
            <fmt:param>
                <fmt:message key="${object.status.resourceKey}"/>
            </fmt:param>
            <fmt:param>
                <s:property value="%{object.sendingInstitution.name}"/>
            </fmt:param>
        </fmt:message>
    </title>
</head>
<body>
    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@SHIPPED}" name="shipped" scope="page"/>
    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@RECEIVED}" name="received" scope="page"/>
    <div class="topbtns">
        <a href="#" onclick="history.back();return false" class="btn">
            <fmt:message key="btn.back"/>
        </a>

        <c:if test="${object.status == shipped || object.status == received}">
            <c:url var="qualityUrl" value="/protected/request/viewShipmentQuality.action">
                <c:param name="object.id" value="${object.id}"/>
            </c:url>
            <a href="${qualityUrl}" class="btn"><fmt:message key="shipment.updateSpecimenQuality"/></a>
        </c:if>
    </div>

    <tissuelocator:orderReadOnlyView />

    <c:set var="hideInvoice" value="false"/>
    <req:isUserInRole role="signedMtaViewer">
        <c:set var="hideInvoice" value="true"/>
    </req:isUserInRole>
    <c:if test="${object.readyForResearcherReview && !hideInvoice}">
        <c:url value="/protected/shipment/print/invoice.action" var="invoiceUrl">
            <c:param name="object.id" value="${object.id}"/>
        </c:url>
        <a class="btn" onclick="Popup=window.open('${invoiceUrl}','Popup','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=800,height=540,left=430,top=23'); return false;"><fmt:message key="invoice.print"/></a>
    </c:if>
</body>
</html>
