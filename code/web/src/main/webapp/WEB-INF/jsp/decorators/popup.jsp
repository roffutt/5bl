<%@ page language="java" errorPage="/error.jsp" pageEncoding="UTF-8" contentType="text/html;charset=utf-8" %>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title><decorator:title default="Welcome" /></title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="X-UA-Compatible" content="IE=8" />
        <link href="<c:url value='/styles/subModal.css'/>" rel="stylesheet" type="text/css" media="all"/>
        <link href="<c:url value='/jquery-ui/css/custom-theme/jquery-ui-1.8.10.custom.css'/>" rel="stylesheet" type="text/css" media="all"/>
        <link href="<c:url value='/jquery-ui/css/jquery.multiselect.css'/>" rel="stylesheet" type="text/css" media="all"/>
        <script type="text/javascript" language="javascript" src="<c:url value="/struts/js/base/jquery-1.4.4.min.js"/>"></script>
        <script type="text/javascript" language="javascript" src="<c:url value="/struts/js/plugins/jquery.subscribe.min.js"/>"></script>
        <script type="text/javascript" language="javascript" src="<c:url value="/struts/js/struts2/jquery.struts2.min.js"/>"></script>
        <script type="text/javascript" language="javascript" src="<c:url value="/jquery-ui/js/jquery.text-overflow.js"/>"></script>
        <script type="text/javascript" language="javascript" src="<c:url value="/jquery-ui/js/jquery-ui-1.8.10.custom.min.js"/>"></script>
        <script type="text/javascript" language="javascript" src="<c:url value="/jquery-ui/js/jquery.multiselect.min.js"/>"></script>
        <script type="text/javascript" language="javascript">
            var contextPath = "<%=request.getContextPath()%>";

            $(document).ready(function () {
                $.scriptPath = '<c:url value="/struts/"/>';
                $.ajaxSettings.traditional = true;

                $.ajaxSetup ({
                    cache: false
                });
            });

            var sessionTimeout = <%=request.getSession().getServletContext().getInitParameter("sessionTimeout")%>;
            var sessionWarningTime = <%=request.getSession().getServletContext().getInitParameter("sessionWarningTime")%>;
            var sessionWarningSecond = '<fmt:message key="sessionWarning.second"/>';
            var sessionWarningSeconds = '<fmt:message key="sessionWarning.seconds"/>';
            var sessionWarningMinute = '<fmt:message key="sessionWarning.minute"/>';
            var sessionWarningMinutes = '<fmt:message key="sessionWarning.minutes"/>';
        </script>
        <script type="text/javascript" language="javascript" src="<c:url value="/scripts/dropdown_menu.js"/>"></script>
        <script type="text/javascript" language="javascript" src="<c:url value='/scripts/subModal.js'/>"></script>
        <script type="text/javascript" language="javascript" src="<c:url value='/scripts/tissuelocator.js'/>"></script>
        <decorator:head/>
    </head>

    <body class="submodal" ${extraBodyTags}
        onkeypress="window.top.monitorActivity();" onclick="window.top.monitorActivity();">
        <c:if test="${skipHeader != true}">
            <fmt:message key="tissuelocator.title" var="defaultTitle" />
            <h1><decorator:title default="${defaultTitle}"/></h1>
        </c:if>

        <decorator:body/>

    </body>
</html>
