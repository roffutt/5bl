<link href="<c:url value='/styles/biolocator.css'/>" rel="stylesheet" type="text/css" media="all"/>
<link href="<c:url value='/styles/subModal.css'/>" rel="stylesheet" type="text/css" media="all"/>
<link href="<c:url value='/styles/overwrites.css'/>" rel="stylesheet" type="text/css" media="all"/>
<link href="<c:url value='/jquery-ui/css/custom-theme/jquery-ui-1.8.10.custom.css'/>" rel="stylesheet" type="text/css" media="all"/>
<link href="<c:url value='/jquery-ui/css/jquery.multiselect.css'/>" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" language="javascript" src="<c:url value="/scripts/dropdown_menu.js"/>"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/struts/js/base/jquery-1.4.4.min.js"/>"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/struts/js/plugins/jquery.subscribe.min.js"/>"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/struts/js/struts2/jquery.struts2.min.js"/>"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/jquery-ui/js/jquery.text-overflow.js"/>"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/jquery-ui/js/jquery-ui-1.8.10.custom.min.js"/>"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/jquery-ui/js/jquery.multiselect.min.js"/>"></script>
<script type="text/javascript" language="javascript">
    var contextPath = "<%=request.getContextPath()%>";

    $(document).ready(function () {
        $.scriptPath = '<c:url value="/struts/"/>';
        $.ajaxSettings.traditional = true;

        $.ajaxSetup ({
            cache: false
        });
    });

    var sessionTimeout = <%=request.getSession().getServletContext().getInitParameter("sessionTimeout")%>;
    var sessionWarningTime = <%=request.getSession().getServletContext().getInitParameter("sessionWarningTime")%>;
</script>
<script type="text/javascript" language="javascript" src="<c:url value='/scripts/subModal.js'/>"></script>
<script type="text/javascript" language="javascript">
    setPopUpLoadingPage('');
</script>
<script type="text/javascript" language="javascript" src="<c:url value='/scripts/tissuelocator.js'/>"></script>
