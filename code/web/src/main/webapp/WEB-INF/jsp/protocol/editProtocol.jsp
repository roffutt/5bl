<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url value="/admin/protocol/list.action" var="listUrl"/>
<html>
<head>
    <title><fmt:message key="protocol.title" /></title>
</head>
<body onload="setFocusToFirstControl();">
<div class="topbtns">
    <a href="${listUrl}" class="btn">
        <fmt:message key="btn.backToList"/>
    </a>
</div>
<s:form id="protocolForm" action="/admin/protocol/save.action" onsubmit="">
    <tissuelocator:messages/>

    <s:hidden key="object.id" />

    <h2 class="formtop"><fmt:message key="protocol.sectionHeader"/></h2>

    <div class="formcol">
        <s:textfield name="object.name" label="%{getText('protocol.name')}" size="30" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" required="true" requiredposition="right" id="name" />
    </div>

    <div class="formcol_wide">
        <c:choose>
            <c:when test="${TissueLocatorUser.crossInstitution}">
                <tissuelocator:autocompleter fieldName="object.institution" fieldId="institution"
                    searchFieldName="institutionSelection" cssStyle="width:27em"
                    autocompleteUrl="protected/json/autocompleteInstitution.action" required="true"
                    labelKey="protocol.institution.name" noteKey="protocol.institution.note"/>
            </c:when>
            <c:otherwise>
                <label><s:property value="%{getText('protocol.institution.name')}"/></label>
                ${TissueLocatorUser.institution.name}
                <s:hidden name="institutionSelection" value="%{#attr.TissueLocatorUser.institution.name}"/>
                <s:hidden name="object.institution" id="hiddenInstitution"
                    value="%{#attr.TissueLocatorUser.institution.id}"/>
            </c:otherwise>
        </c:choose>
    </div>

    <div class="clear"><br /></div>

    <fmt:message key="datepicker.format" var="dateFormat"/>

    <div class="formcol">
        <sj:datepicker id="startDate" name="object.startDate" displayFormat="%{#attr.dateFormat}"
            label="%{getText('protocol.startDate')}" labelposition="left" labelSeparator="" buttonImageOnly="true"
            changeMonth="true" changeYear="true"/>

    </div>

    <div class="formcol">
        <sj:datepicker id="endDate" name="object.endDate" displayFormat="%{#attr.dateFormat}"
            label="%{getText('protocol.endDate')}" labelposition="left" labelSeparator="" buttonImageOnly="true"
            changeMonth="true" changeYear="true"/>
    </div>

    <div class="clear"><br /></div>

    <h2><fmt:message key="protocol.contact.info.heading"/></h2>

    <div class="formcol">
        <s:textfield name="object.contact.firstName" label="%{getText('protocol.contact.firstName')}"
            size="30" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" id="firstName" />
    </div>

    <div class="formcol">
        <s:textfield name="object.contact.lastName" label="%{getText('protocol.contact.lastName')}"
            size="30" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" id="lastName" />
    </div>

    <div class="clear"><br /></div>

        <div class="formcol">
        <s:textfield name="object.contact.email" label="%{getText('protocol.contact.email')}"
            size="30" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" id="email" />
    </div>

    <div class="formcol">
        <s:textfield name="object.contact.phone" label="%{getText('protocol.contact.phone')}"
            size="50" maxlength="254" cssStyle="width:20em"
            labelposition="top" labelSeparator="" id="phone" />
    </div>
    
    <div class="formcol">       
        <s:textfield name="object.contact.phoneExtension" label="%{getText('protocol.contact.phoneExtension')}"
            size="10" maxlength="254" cssStyle="width:6em"
            labelposition="top" labelSeparator="" id="phoneExtension" />
    </div>

    <div class="clear"><br /></div>

    <div class="btn_bar">
        <s:submit value="%{getText('btn.save')}" name="btn_save" cssClass="btn" theme="simple" title="%{getText('btn.save.button')}"/>
        &nbsp;&nbsp;&nbsp;|&nbsp;
        <a href="${listUrl}"><fmt:message key="btn.cancel"/></a>
    </div>
</s:form>
</body>
</html>
