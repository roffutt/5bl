<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="home.title" /></title>
</head>
<body>

<c:url value="/protected/myAccount/input.action" var="myAccountUrl"/>
<div id="focus">

    <tissuelocator:splashImage showRegisterButton="false"/>

    <div id="focus_right_wrapper">
        <div id="focus_right">

            <!--Welcome-->
            <h2>
                <fmt:message key="home.welcome"/>
                <a href="${myAccountUrl}" title="${TissueLocatorUser.firstName}">
                    <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${false}" value="${TissueLocatorUser.firstName}"
                        maxWordLength="20" manual="true"/>
                </a>
            </h2>

            <h3 class="ico_notifications"><fmt:message key="home.pendingRequests"/></h3>

            <c:url value="/admin/signedMta/list.action" var="mtaReviewUrl"/>
            <tissuelocator:toDoListMessage messageKeyBase="home.mtaReview" url="${mtaReviewUrl}"
                count="${mtaReviewCount}"/>
            <c:url value="/admin/request/review/filter.action" var="reviewUrl">
                <c:param name="actionNeededOnly" value="true" />
            </c:url>
            <tissuelocator:toDoListMessage messageKeyBase="home.assignReviewer" url="${reviewUrl}"
                count="${assignReviewerCount}"/>
            <c:url value="/admin/request/review/filter.action" var="reviewUrl">
                <c:param name="object.status" value="PENDING" />
                <c:param name="actionNeededOnly" value="true" />
            </c:url>
            <tissuelocator:toDoListMessage messageKeyBase="home.leadReviewer" url="${reviewUrl}"
                count="${leadReviewerCount}"/>
            <tissuelocator:toDoListMessage messageKeyBase="home.consortiumReview" url="${reviewUrl}"
                count="${consortiumReviewCount}"/>
            <tissuelocator:toDoListMessage messageKeyBase="home.institutionalReview" url="${reviewUrl}"
                count="${institutionalReviewCount}"/>
            <tissuelocator:toDoListMessage messageKeyBase="home.lineItemReview" url="${reviewUrl}"
                count="${lineItemReviewCount}"/>
            <c:url value="/admin/request/review/filter.action" var="reviewUrl">
                <c:param name="object.status" value="PENDING_FINAL_DECISION" />
                <c:param name="actionNeededOnly" value="true" />
            </c:url>
            <tissuelocator:toDoListMessage messageKeyBase="home.finalComment" url="${reviewUrl}"
                count="${finalCommentCount}"/>

            <tissuelocator:determinePersistenceType urlPrefix="/protected/request/filter" urlEnding=".action"/>
            <c:url value="${attr.request.outputUrl}" var="requestorActionRequiredUrl">
                <c:param name="object.requestor" value="${TissueLocatorUser.id}" />
                <c:param name="actionNeededOnly" value="true"/>
            </c:url>
            <tissuelocator:toDoListMessage messageKeyBase="home.requestorActionRequired" url="${requestorActionRequiredUrl}"
                count="${requestorActionRequiredCount}"/>

            <c:url value="/admin/shipment/filter.action" var="shipmentUrl">
                <c:param name="object.status" value="PENDING" />
            </c:url>
            <tissuelocator:toDoListMessage messageKeyBase="home.approvedRequest" url="${shipmentUrl}"
                count="${approvedRequestCount}"/>

            <c:url value="/admin/request/review/filter.action" var="reviewDecisionUrl">
                <c:param name="object.status" value="PENDING_FINAL_DECISION" />
                <c:param name="actionNeededOnly" value="true" />
            </c:url>
            <tissuelocator:toDoListMessage messageKeyBase="home.reviewDecisionNotification" url="${reviewDecisionUrl}"
                count="${researcherDecisionNotificationCount}"/>

            <c:url value="/admin/user/filter.action" var="userListUrl">
                <c:param name="object.status" value="PENDING" />
            </c:url>
            <tissuelocator:toDoListMessage messageKeyBase="home.registrationReview" url="${userListUrl}"
                count="${registrationReviewCount}"/>

            <c:url value="/admin/support/letter/filter.action" var="letterListUrl">
                <c:param name="object.status" value="PENDING" />
            </c:url>
            <tissuelocator:toDoListMessage messageKeyBase="home.supportLetterRequest" url="${letterListUrl}"
                count="${supportLetterRequestCount}"/>

            <c:url value="/admin/support/question/response/filter.action" var="questionResponseListUrl">
                <c:param name="object.status" value="PENDING" />
                <c:param name="object.institution" value="${TissueLocatorUser.institution.id}" />
            </c:url>
            <tissuelocator:toDoListMessage messageKeyBase="home.questionResponse" url="${questionResponseListUrl}"
                count="${questionResponseCount}"/>

            <h3 class="ico_cart">
                <fmt:message key="home.cart"/>
                <tissuelocator:determinePersistenceType urlPrefix="/protected/request/viewCart" urlEnding=".action" asUrl="true"/>
                <a href="${outputUrl}">
                    <s:set value="%{0}" var="cartSize"/>
                    <s:if test="%{#attr.specimenCart != null}">
                        <s:set value="%{#attr.specimenCart.lineItems.size() + #attr.specimenCart.aggregateLineItems.size()}" var="cartSize"/>
                    </s:if>
                    <s:if test="%{#cartSize == 1}">
                        <fmt:message key="home.cart.single"/>
                    </s:if>
                    <s:else>
                        <fmt:message key="home.cart.multiple">
                            <fmt:param>
                                <s:property value="%{#cartSize}"/>
                            </fmt:param>
                        </fmt:message>
                    </s:else>
                </a>
            </h3>
            <!--/Welcome-->

        </div>
    </div>

    <div class="clear"></div>
</div>

<div id="subfocus">

    <tissuelocator:biospecimenBrowse/>
    <div class="clear"></div>

</div>

</body>
</html>
