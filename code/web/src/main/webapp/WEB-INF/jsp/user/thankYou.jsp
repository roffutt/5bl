<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<html>
<head>
    <title><fmt:message key="user.register.thankyou.title" /></title>
</head>
<body>

<div class="topconfirm">
    <p>
        <fmt:message key="user.register.thankyou.topconfirm"/>
    </p>
</div>
<br />

<s:if test="%{accountApprovalActive}">
    <p>
        <fmt:message key="user.register.pendingApproval.text1"/>
    </p>
</s:if>

<h2 class="form_noline"><fmt:message key="user.register.thankyou.next"/></h2>

<s:if test="%{accountApprovalActive}">
    <p>
        <fmt:message key="user.register.pendingApproval.text2"/>
    </p>
</s:if>
<s:else>
    <p>
        <fmt:message key="user.register.thankyou.text"/>
    </p>
</s:else>

<ul>
    <li>
        <c:url value="/" var="homeUrl"/>
        <a href="${homeUrl}">
            <fmt:message key="user.register.thankyou.home"/>
        </a>
    </li>
    <s:if test="%{!accountApprovalActive}">
        <li>
            <c:url value="/protected/specimen/search/list.action" var="searchUrl"/>
            <a href="${searchUrl}">
                <fmt:message key="user.register.thankyou.search"/>
            </a>
        </li>
    </s:if>
</ul>

</body>
</html>
