<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
    <head>
        <title><fmt:message key="changePassword.title"/></title>
    </head>
    <body>
        <tissuelocator:messages/>
        <div class="loginfloat">
            <div class="box">
                <h2><fmt:message key="changePassword.subtitle"/></h2>

                <s:form  action="changePassword/execute" id="changePasswordForm">
                    <%-- Pull from the URL if the key is present.  For error conditions,
                    struts will put the value in nonce directly --%>
                    <c:if test="${!empty param.key}">
                        <input type="hidden" name="nonce" value="<c:out value="${param.key}"/>"/>
                    </c:if>
                    <c:if test="${empty param.key}">
                        <s:hidden name="nonce"/>
                    </c:if>

                    <s:textfield name="username" id="username"  size="30" maxlength="254"
                            labelposition="left" label="%{getText('forgotPassword.username')}"
                            required="true" requiredposition="right"/>
                    <br/>

                    <s:password name="requested" id="requested"  size="30" maxlength="254"
                            labelposition="left" label="%{getText('changePassword.requested')}"
                            required="true" requiredposition="right"/>
                    <br/>

                    <s:password name="retyped" id="retyped"  size="30" maxlength="254"
                            labelposition="left" label="%{getText('changePassword.retyped')}"
                            required="true" requiredposition="right"/>
                    <br/>

                    <s:submit name="submit" value="%{getText('changePassword.save')}" theme="simple" cssClass="btn"
                        id="changePasswordSubmit" title="%{getText('changePassword.save.button')}"/>
                </s:form>

            </div>
        </div>
    </body>
</html>
