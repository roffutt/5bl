<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
    <head>
        <title><fmt:message key="user.list.title" /></title>
    </head>
    <body>

        <div class="topbtns">
            <c:url value="/admin/user/input.action" var="createUrl"/>
            <a href="${createUrl}" class="btn">
                <fmt:message key="user.list.add"/>
            </a>
        </div>

        <tissuelocator:messages/>

        <s:form id="filterForm" action="user/filter">
            <!--Filters-->
            <div class="roundbox_gray_wrapper">
                <div class="roundbox_gray">
                    <div class="float_left">
                        <label class="inline" for="status">
                          <strong><fmt:message key="user.list.filter"/></strong>
                        </label>
                        <s:select name="object.status" id="status"
                            onchange="submit()" theme="simple"
                            list="%{@com.fiveamsolutions.nci.commons.data.security.AccountStatus@values()}"
                            listValue="%{getText('AccountStatus.' + name())}"
                            headerKey="" headerValue="%{getText('AccountStatus.ALL')}" />
                    </div>

                    <div class="float_right">
                    </div>

                    <div class="clear"></div>
                </div>
            </div>
            <!--/Filters-->
            <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
            <fmt:message key="default.datetime.format" var="datetimeFormat"/>
            <display:table name="objects" requestURI="/admin/user/list.action"
                uid="user" pagesize="${tablePageSize}" sort="external" export="true">
                <tissuelocator:displaytagProperties/>
                <display:setProperty name="export.csv.filename" value="users.csv"/>
                <display:column titleKey="user.list.name" class="title" sortProperty="LAST_NAME,FIRST_NAME"
                    sortable="true" media="html">
                    <c:choose>
                        <c:when test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == user.institution.id}">
                            <c:url value="/admin/user/input.action" var="editUrl">
                                <c:param name="object.id" value="${user.id}"/>
                            </c:url>
                            <a href="${editUrl}">
                                <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${true}" value="${user.displayName}"
                                    maxWordLength="20"/>
                            </a>
                        </c:when>
                        <c:otherwise>
                            <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${true}" value="${user.displayName}"
                                    maxWordLength="20"/>
                        </c:otherwise>
                    </c:choose>
                </display:column>
                <display:column titleKey="user.list.name" class="title" sortProperty="LAST_NAME,FIRST_NAME" sortable="true"
                    media="csv">
                    ${user.displayName}
                </display:column>
                <display:column titleKey="user.list.email" sortable="true" sortProperty="EMAIL">
                    <tissuelocator:truncateText cssMaxWidth="200px" isTableCell="${true}" value="${user.email}"
                                    maxWordLength="20"/>
                </display:column>
                <display:column titleKey="user.list.institution" property="institution.name"
                    sortable="true" sortProperty="INSTITUTION_NAME"/>
                <display:column titleKey="user.list.status" sortProperty="STATUS" sortable="true">
                    <fmt:message key="AccountStatus.${user.status}"/>
                </display:column>
                <display:column titleKey="user.list.updated" sortProperty="UPDATED_DATE" sortable="true">
                    <fmt:formatDate pattern="${datetimeFormat}" value="${user.updatedDate}"/>
                </display:column>
                <display:column titleKey="user.list.role">
                    <c:forEach items="${user.groups}" var="group" varStatus="loopStatus">
                        <c:if test="${!loopStatus.first}">, </c:if><c:out value="${group.name}"/>
                    </c:forEach>
                </display:column>
                <display:column titleKey="column.action" headerClass="action" class="action" media="html">
                    <c:if test="${TissueLocatorUser.crossInstitution || TissueLocatorUser.institution.id == user.institution.id}">
                        <c:url value="/admin/user/input.action" var="editUrl">
                            <c:param name="object.id" value="${user.id}"/>
                        </c:url>
                        <a href="${editUrl}" class="btn_fw_action_col">
                            <fmt:message key="btn.edit"/>
                        </a>
                    </c:if>
                </display:column>
            </display:table>
        </s:form>
    </body>
</html>
