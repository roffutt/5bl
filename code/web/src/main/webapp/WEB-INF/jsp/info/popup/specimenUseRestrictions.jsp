<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>

<html>
    <head>
        <title><fmt:message key="specimenUseRestrictions.info.title"/></title>
    </head>
    <body>

        <div>
            <tissuelocator:specimenUseRestrictions institution="${object}"
                emptyResourceKey="specimen.specimenUseRestrictions.default"/>
        </div>

        <div class="clear"></div>

        <div class="btn_bar">
            <a href="#" onclick="window.parent.hidePopWin(true);return false">Close</a>

        </div>

    </body>
</html>