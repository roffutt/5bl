<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:set var="skipHeader" value="true" scope="request"/>
<html>
    <head>
        <title><fmt:message key="specimen.search.results.configure.displayed.fields.title" /></title>
    </head>
    <body>
        <script>
            $(function() {
                $( "#displayOptions" ).accordion({
                    collapsible : true
                });
            });

            function checkAllFromCategory(category, flag) {
                var inputField = $('input[id^=' + category + "_" + "]");
                if(flag) {
                    inputField.attr('checked','checked');
                } else {
                    inputField.removeAttr('checked');
                }
            }

        </script>
        <div>
            <p><fmt:message key="specimen.search.results.display.options.banner" /></p>

            <s:form id="specimenResultsDisplayOptionsForm" 
                    namespace="/protected"
                    action="specimen/popup/saveSpecimenResultsDisplayOptions"
                    target="_self">

                <div id="displayOptions">
                    <s:iterator value="categoryNames" status="categoryStatus" var="categoryName">
                        <%-- category heading --%>
                        <h3><a href="#"><s:property value="%{#categoryName}"/></a></h3>
                        <div>
                            <s:set value="%{getNormalizedName(#categoryName)}" var="normalizedCategory"/>
                            <div style="margin:2px;">
                                <ul style="height:10px;">
                                    <li style="float:left; margin-right: 10px;">
                                        <a href="#" onclick="checkAllFromCategory('${normalizedCategory}',true);
                                                             return false;"
                                           style="cursor:pointer;"
                                           id="${normalizedCategory}_all">
                                            <span class="ui-icon ui-icon-check" style="float:left;"></span>
                                            <span style="float:left;">Check all</span>
                                        </a> 
                                    </li>
                                    <li style="float:left;">
                                        <a href="#" onclick="checkAllFromCategory('${normalizedCategory}',false);
                                                             return false;"
                                           style="cursor:pointer;"
                                           id="${normalizedCategory}_none">
                                            <span class="ui-icon ui-icon-closethick" style="float:left;"></span>
                                            <span style="float:left;">Uncheck all</span>
                                        </a> 
                                    </li>
                                </ul>
                            </div>
                            <div style="margin:2px;">
                                <%-- expanded category fields --%>
                                <s:iterator value="%{categoryDisplayConfig[#categoryName]}"
                                            status= "fieldStatus"
                                            var="fieldDisplaySetting">
                                    <s:if test="%{#fieldStatus.index % 2 == 0}">
                                        <br/>
                                    </s:if>
                                    <span class="specimendisplayoption">
                                        <s:checkbox id="%{#normalizedCategory}_%{#fieldDisplaySetting.fieldConfig.fieldName}"
                                                name="categoryDisplayConfig['%{#categoryName}'][%{#fieldStatus.index}].displayFlagAsBoolean"
                                                value="%{categoryDisplayConfig[#categoryName][#fieldStatus.index].displayFlagAsBoolean}"
                                                theme="simple"/>
                                        <label class="inline" 
                                           for="${normalizedCategory}_${fieldDisplaySetting.fieldConfig.fieldName}">
                                           ${fieldDisplaySetting.fieldConfig.fieldDisplayName}
                                        </label>
                                    </span>
                                </s:iterator>
                            </div>
                        </div>
                    </s:iterator>
                </div>

                <div class="clear"></div>

                <div class="btn_bar">
                    <s:checkbox id="makeSettingsPersistendId"
                                name="makeCurrentSettingsPersistentFlag"
                                theme="simple"/>
                    <label class="inline" 
                       for="makeSettingsPersistendId">
                       <s:text name="specimen.displayOptions.makeSettingsPersistent.label"></s:text>
                    </label>
                    <br/>
                    <s:submit id="btn_savedisplaypreferences" 
                              value="%{getText('specimen.search.display.options.save.button')}" 
                              title="%{getText('specimen.search.display.options.save.title')}"
                              name="btn_savedisplaypreferences" 
                              cssClass="btn"
                              cssStyle="float:left;"
                              theme="simple"/>
                    <a href="#" class="btn" onclick="window.parent.hidePopWin(false);return false;"
                       style="float:left; padding:0px 8px;">
                        Cancel
                    </a>
                </div>

            </s:form>
        </div>
    </body>
</html>
