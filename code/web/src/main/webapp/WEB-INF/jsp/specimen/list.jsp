<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="specimen.title" /></title>
</head>
<body>
    <div class="topbtns">
        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.SpecimenStatus@AVAILABLE}" name="available" scope="page"/>
        <c:url value="/admin/specimen/input.action" var="addSpecimenUrl">
            <c:param name="object.status" value="${available}"/>
        </c:url>
        <a href="${addSpecimenUrl}" class="btn">
            <fmt:message key="specimen.add"/>
        </a>
    </div>

    <tissuelocator:messages/>

    <s:form id="filterForm" namespace="/admin" action="specimen/filter.action">
        <!--Filters-->
        <div class="roundbox_gray_wrapper">
            <div class="roundbox_gray">
                <div class="float_left">
                    <label class="inline" for="status">
                      <strong><fmt:message key="specimen.list.filter.status"/></strong>
                    </label>
                    <s:select name="object.status" id="status" value="%{object.status}"
                        list="%{@com.fiveamsolutions.tissuelocator.data.SpecimenStatus@values()}" theme="simple"
                        onchange="submit()" listValue="%{getText(resourceKey)}"
                        headerKey="" headerValue="%{getText('specimenStatus.all')}" />
                    <div class="clear" style="padding-top:3px"></div>
                    <s:checkbox theme="simple" id="filterByInstitution" name="filterByInstitution" onclick="submit()"
                        cssStyle="margin-left:0px;" title="%{getText(specimen.list.filter.institution)}"/>
                    <label class="inline" for="filterByInstitution">
                        <strong><fmt:message key="specimen.list.filter.institution"/></strong>
                    </label>
                </div>

                <div class="float_right">
                    <label class="inline" for="externalId">
                      <strong><fmt:message key="specimen.list.filter.externalId"/></strong>
                    </label>
                    <s:textfield name="object.externalId" theme="simple" id="externalId"/>
                    <s:submit value="%{getText('btn.search')}" name="btn_search" cssClass="btn" theme="simple" title="%{getText('btn.search.button')}"/>
                </div>

                <div class="clear"></div>
            </div>
        </div>

        <tissuelocator:specimenTable showEditButtons="true" showCheckboxes="false" listAction="/admin/specimen/list.action"
            viewAction="/protected/specimen/view.action" exportEnabled="true" showStatus="true" showFee="false" showInstitution="true"/>

    </s:form>
</body>
</html>
