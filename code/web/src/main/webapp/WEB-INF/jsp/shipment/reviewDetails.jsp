<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title>
        <fmt:message key="shipment.view.title">
            <fmt:param value="${object.request.id}" />
            <fmt:param>
                <fmt:message key="${object.request.status.resourceKey}"/>
            </fmt:param>
        </fmt:message>
    </title>
</head>
<body>
    <div class="topbtns">
        <c:url var="backUrl" value="/admin/shipment/edit.action">
            <c:param name="object.id" value="${object.id}"/>
        </c:url>
        <a href="${backUrl}" class="btn"><fmt:message key="shipment.reviewDetails.back"/></a>
    </div>

    <div id="requestform">
        <h1>
            <fmt:message key="shipment.order.title.edit">
                <fmt:param value="${object.id}" />
                <fmt:param>
                    <fmt:message key="${object.status.resourceKey}"/>
                </fmt:param>
            </fmt:message>
        </h1>

        <tissuelocator:reviewDetail titleKey="shipment.reviewDetails.consortium" vote="${consortiumReviewVote}"/>
        <tissuelocator:reviewDetail titleKey="shipment.reviewDetails.institutional" vote="${institutionalReviewVote}"/>

        <s:if test="%{object.aggregateLineItems.size() > 0}">
            <table class="data">

                <s:if test="%{specificLineItems.length > 0}">
                    <tissuelocator:lineItemReviewList lineItems="${specificLineItems}" lineItemPrefix="specific"
                        titleKey="aggregateCartTable.specificCart.title" />
                </s:if>

                <s:if test="%{generalLineItems.length > 0}">
                    <tissuelocator:lineItemReviewList lineItems="${generalLineItems}" lineItemPrefix="general"
                        titleKey="aggregateCartTable.generalCart.title" />
                </s:if>

            </table>
        </s:if>
    </div>
</body>
</html>
