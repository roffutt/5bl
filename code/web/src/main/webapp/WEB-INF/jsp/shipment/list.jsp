<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title><fmt:message key="shipment.list.title" /></title>
</head>
<body>
    <tissuelocator:messages/>

    <s:form id="filterForm" namespace="/admin" action="shipment/filter.action">
        <!--Filters-->
        <div class="roundbox_gray_wrapper">
            <div class="roundbox_gray">
                <div class="float_left">
                </div>

                <div class="float_right">
                    <strong><fmt:message key="specimenRequest.review.list.filter"/></strong>
                    <s:hidden name="object.sendingInstitution" value="%{object.sendingInstitution.id}"/>
                    <s:select name="object.status" id="status"
                            onchange="submit()" theme="simple"
                            list="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@values()}"
                            listValue="%{getText(resourceKey)}" title="%{getText('shipmentstatus.select')}"
                            headerKey="" headerValue="%{getText('shipmentstatus.all')}" />
                </div>

                <div class="clear"></div>
            </div>
        </div>

        <s:set value="%{pageSize}" name="tablePageSize" scope="page"/>
        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@PENDING}" name="pendingShipment" scope="page"/>

        <display:table name="objects" requestURI="/admin/shipment/list.action"
                uid="shipment" pagesize="${tablePageSize}" sort="external" export="true">
                <tissuelocator:displaytagProperties/>
                <display:setProperty name="export.csv.filename" value="shipments.csv"/>

            <display:column titleKey="shipment.id" sortProperty="ID" sortable="true" media="html">
                <c:choose>
                    <c:when test="${shipment.status == pendingShipment}">
                        <c:url var="shipmentUrl" value="/admin/shipment/edit.action">
                            <c:param name="object.id" value="${shipment.id}"/>
                        </c:url>
                    </c:when>
                    <c:otherwise>
                        <c:url var="shipmentUrl" value="/admin/shipment/view.action">
                            <c:param name="object.id" value="${shipment.id}"/>
                        </c:url>
                    </c:otherwise>
                </c:choose>
                <a href="${shipmentUrl}">${shipment.id}</a>
            </display:column>
            <display:column titleKey="shipment.id" sortProperty="ID" sortable="true" media="csv">
                ${shipment.id}
            </display:column>

            <display:column titleKey="shipment.request.id" property="request.id"
                sortable="true" sortProperty="REQUEST_ID"/>
            <display:column titleKey="specimenRequest.requestor.name" sortable="true" sortProperty="REQUESTOR_FIRST_NAME,REQUESTOR_LAST_NAME">
                ${shipment.request.requestor.firstName} ${shipment.request.requestor.lastName}
            </display:column>
            <display:column titleKey="specimenRequest.requestor.institution.name"
                property="request.requestor.institution.name" sortable="true" sortProperty="REQUESTOR_INSTITUTION"/>
            <display:column titleKey="shipment.status" sortable="true" sortProperty="STATUS">
                <fmt:message key="${shipment.status.resourceKey}" />
            </display:column>
            <display:column titleKey="shipment.trackingNumber" sortable="true" sortProperty="TRACKING_NUMBER">
                <tissuelocator:orderTrackingLink order="${shipment}" showLabel="false"/>
            </display:column>
            <display:column titleKey="shipment.specimenCount">
                <s:property value="%{#attr.shipment.specimenCount}"/>
            </display:column>
        </display:table>
    </s:form>
</body>
</html>
