<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html>
<head>
    <title>
        <fmt:message key="shipment.order.title">
            <fmt:param value="${object.id}" />
            <fmt:param>
                <fmt:message key="${object.status.resourceKey}"/>
            </fmt:param>
            <fmt:param>
                <s:property value="%{object.sendingInstitution.name}"/>
            </fmt:param>
        </fmt:message>
    </title>
</head>
<body>
    <div class="topbtns">
        <c:url value="/admin/shipment/viewRequestDetails.action" var="detailsUrl">
            <c:param name="object.id" value="${object.id}"/>
        </c:url>

        <a href="${detailsUrl}" class="btn">
            <fmt:message key="specimenRequest.review.details"/>
        </a>

        <a href="#" onclick="history.back();return false" class="btn">
            <fmt:message key="btn.back"/>
        </a>
    </div>

    <tissuelocator:messages breakCount="2"/>

    <tissuelocator:orderReadOnlyView />

    <s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@CANCELED}" name="canceled" scope="page"/>
    <c:if test="${object.status != canceled}">
        <div class="btn_bar">
            <c:url value="/admin/shipment/print/invoice.action" var="invoiceUrl">
                <c:param name="object.id" value="${object.id}"/>
            </c:url>
            <a class="btn"  onclick="Popup=window.open('${invoiceUrl}','Popup','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=800,height=540,left=430,top=23'); return false;"><fmt:message key="invoice.print"/></a>
        </div>
    </c:if>
</body>
</html>
