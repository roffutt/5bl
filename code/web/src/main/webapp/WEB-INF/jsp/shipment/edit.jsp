<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<c:url var="refreshUrl" value="/admin/shipment/edit.action">
    <c:param name="object.id" value="${object.id}"/>
    <c:param name="cartSize">
        <s:property value="%{object.lineItems.size() + object.aggregateLineItems.size()}"/>
    </c:param>
</c:url>
<c:url value="/admin/shipment/save.action" var="saveUrl">
</c:url>
<c:url value="/admin/shipment/markAsShipped.action" var="markAsShippedUrl">
</c:url>
<c:url value="/admin/shipment/cancel.action" var="cancelUrl">
</c:url>
<fmt:message key="shipment.cancel.confirm" var="cancelConfirm"/>
<c:url value="/admin/shipment/print/invoice.action" var="invoiceUrl">
    <c:param name="object.id" value="${object.id}"/>
</c:url>
<c:url var="loadShipmentDateFormUrl" value="/admin/shipment/popup/loadShipmentDateForm.action" >
    <c:param name="object.id" value="${object.id}"/>
</c:url>

<html>
<head>
    <title>
        <fmt:message key="shipment.view.title">
            <fmt:param value="${object.request.id}" />
            <fmt:param>
                <fmt:message key="${object.request.status.resourceKey}"/>
            </fmt:param>
        </fmt:message>
    </title>
    <script type="text/javascript">
        function returnRefresh(returnVal) {
            window.location = '${refreshUrl}';
        }

        function showShipmentDatePopWin() {
            showPopWin('${loadShipmentDateFormUrl}', 400, 225, markAsShipped);
        }

        function markAsShipped() {
            $('#shipmentForm').get(0).action=getFormActionToMarkShipped();
            removeShipmentFormFields();
            $('#shipmentForm').get(0).submit();
        }

        //now that we're submitting a form instead of following an href the confirm logic has to be separated out
        function confirmAndCancel() {
            var cancelShipment = confirm('${cancelConfirm}');
            if(cancelShipment){
                $('#shipmentForm').get(0).action='${cancelUrl}';
                removeShipmentFormFields();
                $('#shipmentForm').get(0).submit();
            }
        }

        //an s:submit does not permit jstl in its onclick event so it has to be here instead
        function getFormActionSave() {
          return '${saveUrl}';
        }

        function getFormActionToMarkShipped() {
          return '${markAsShippedUrl}';
        }
        
        function removeShipmentFormFields() {
        	<c:if test="${displayShipmentBillingRecipient}">removeBillingRecipientFields();</c:if>
        }
    </script>
    <style>
        .ui-tabs .ui-tabs-panel
        { padding: 0px; }
        li.ui-state-default, li.ui-widget-content .ui-state-default, li.ui-widget-header .ui-state-default,
        li.ui-state-hover, li.ui-widget-content .ui-state-hover, li.ui-widget-header .ui-state-hover,
        li.ui-state-focus, li.ui-widget-content .ui-state-focus, li.ui-widget-header .ui-state-focus,
        li.ui-state-active, li.ui-widget-content .ui-state-active, li.ui-widget-header .ui-state-active
        { border: none; background: none; }
    </style>
</head>
<body>
    <div class="topbtns" style="width:40em;">
        <c:url value="/admin/shipment/list.action" var="shipmentUrl"/>
        <a href="${shipmentUrl}" class="btn">
            <fmt:message key="btn.backToList"/>
        </a>

        <c:url value="/admin/shipment/viewRequestDetails.action" var="detailsUrl">
            <c:param name="object.id" value="${object.id}"/>
        </c:url>
        <a href="${detailsUrl}" class="btn">
            <fmt:message key="specimenRequest.review.details"/>
        </a>

        <c:url value="/admin/shipment/reviewDetails.action" var="reviewDetailsUrl">
            <c:param name="object.id" value="${object.id}"/>
        </c:url>
        <a href="${reviewDetailsUrl}" class="btn">
            <fmt:message key="shipment.reviewDetails.view"/>
        </a>
    </div>

    <div id="requestform">
        <h1>
            <fmt:message key="shipment.order.title.edit">
                <fmt:param value="${object.id}" />
                <fmt:param>
                    <fmt:message key="${object.status.resourceKey}"/>
                </fmt:param>
            </fmt:message>
        </h1>

        <div class="topbtns">
            <c:url value="/admin/shipment/popup/specimenSearch/list.action" var="searchUrl">
                <c:param name="order.id" value="${object.id}"/>
            </c:url>
            <a id="addToOrder_btn" class="btn" href="javascript: showPopWin('${searchUrl}', 1000, 500, returnRefresh);"><fmt:message key="shipment.addtoorder.btn" /></a>
            <s:if test="%{displayAggregateSearchResults}">
                <c:url var="emptyUrl" value="/admin/shipment/emptyAggregateOrder.action">
                    <c:param name="object.id" value="${object.id}"/>
                </c:url>
            </s:if>
            <s:else>
                <c:url var="emptyUrl" value="/admin/shipment/emptyOrder.action">
                    <c:param name="object.id" value="${object.id}"/>
                </c:url>
            </s:else>

            <a href="${emptyUrl}" class="btn" style="white-space: nowrap"><fmt:message key="shipment.empty.btn"/></a>
        </div>

        <tissuelocator:messages breakCount="0"/>

        <s:form namespace="/admin" action="shipment/updatePricing.action" id="pricingForm">
            <s:hidden name="object.id" />

            <s:if test="%{displayAggregateSearchResults}">
                <s:set var="specificLineItems" value="%{specificLineItemList}"/>
                <s:set var="generalLineItems" value="%{generalLineItemList}"/>
                <tissuelocator:aggregateCartTable specificLineItems="${specificLineItems}" generalLineItems="${generalLineItems}" showFinalPriceColumn="true" showOrderFulfillmentForms="true" order="${object}"
                    cartPrefix="aggregate" showEmpty="true"/>
            </s:if>

            <s:else>
                <tissuelocator:cartTable showFinalPriceColumn="true" showOrderFulfillmentForms="true" requestLineItems="${object.lineItems}"
                    showSpecimenLinks="true" order="${object}"/>
            </s:else>
        </s:form>

        <!-- this forms action is set by the buttons that submit it(save, mark as shipped, cancel) -->
        <s:form id="shipmentForm" enctype="multipart/form-data" method="POST">
            <s:hidden name="object.id" />

            <req:isUserInRole role="signedMtaViewer">
                <h2 class="noline">
                    <a href="javascript:;" onclick="toggleVisibility('documentation');" class="toggle">
                        <fmt:message key="shipment.mta.title"/>
                    </a>
                </h2>

                <div id="documentation">
                    <tissuelocator:orderMta mode="edit" viewer="sender"/>
                </div>
            </req:isUserInRole>

            <h2 class="noline">
                <a href="javascript:;" onclick="toggleVisibility('shipping_details');" class="toggle">
                    <fmt:message key="shipment.shippingDetails"/>
                </a>
            </h2>

            <div id="shipping_details">
                <div class="formcol" style="width:200px">
                    <s:select name="object.shippingMethod" id="sv" value="%{object.shippingMethod}"
                        list="%{@com.fiveamsolutions.tissuelocator.data.ShippingMethod@values()}"
                        listValue="%{getText(resourceKey)}" labelposition="top" cssStyle="width:180px"
                        label="%{getText('shipment.shippingMethod')}" labelSeparator=""/>
                </div>

                <div class="formcol" style="width:200px">
                    <s:textfield name="object.trackingNumber" id="tn" size="30" maxlength="254" cssStyle="width:180px"
                       labelposition="top" label="%{getText('shipment.trackingNumber')}" labelSeparator=""/>
                </div>

                <div class="formcol_wide" style="width:610px">
                    <s:textarea name="object.shippingInstructions" id="si"
                                rows="3" cols="20" cssStyle="width:583px; height:3em;"
                                labelposition="top" label="%{getText('specimenRequest.shipment.instructions')}"
                                labelSeparator=""/>
                </div>

                <div class="clear"></div>
                <br />
                <div class="line"></div>
            </div>

            <h2 class="noline">
                <a href="javascript:;" onclick="toggleVisibility('shipping_address');" class="toggle">
                    <fmt:message key="shipment.recipient"/>
                </a>
            </h2>

            <div id="shipping_address">
                <tissuelocator:editPerson copyEnabled="false" propertyPrefix="object.recipient"
                        resourcePrefix="specimenRequest.shipment.recipient" idPrefix="recipient"
                        countryValue="${object.recipient.address.country}"
                        stateValue="${object.recipient.address.state}"
                        showInstitution="true" showFax="false"/>
                 <div class="clear"><br /></div>
                 <div class="line"></div>
            </div>

            <c:if test="${displayShipmentBillingRecipient}">
                <fmt:message key="shipment.billingRecipient" var="billingSectionTitle"/>
                <tissuelocator:editBillingRecipient sectionTitle="${billingSectionTitle}" propertyPrefix="object.billingRecipient"
                     billingRecipient="${object.billingRecipient}" copyEnabled="false" resourcePrefix="shipment.billingRecipient"
                     formName="shipmentForm"/>
            </c:if>
                
            <h2 class="noline">
                <a href="javascript:;" onclick="toggleVisibility('shipping_notes');" class="toggle">
                    <fmt:message key="shipment.notes"/>
                </a>
            </h2>

            <div id="shipping_notes">
                <s:textarea name="object.notes" value="%{object.notes}" id="notes"
                            rows="3" cols="40" cssStyle="width:550px; height:5.9em;"
                            title="%{getText('shipment.notes.textarea')}"/>
                 <div class="clear"><br /></div>
                 <div class="line"></div>
            </div>

            <h2 class="noline">
                <a href="javascript:;" onclick="toggleVisibility('researcher_review');" class="toggle">
                    <fmt:message key="shipment.readyForResearcherReview"/>
                </a>
            </h2>

            <div id="researcher_review">
                <s:checkbox theme="simple" name="object.readyForResearcherReview" id="readyForResearcherReview"/>
                <label class="inline" for="readyForResearcherReview">
                    <fmt:message key="shipment.readyForResearcherReview"/>
                </label>
                <s:fielderror fieldName="object.readyForResearcherReview" cssClass="fielderror"/>
            </div>
            <div class="clear"><br /></div>

        </s:form>
    </div>

    <div class="clear"><br /></div>
    <div class="btn_bar">
        <s:submit value="%{getText('btn.save')}" name="btn_save_shipment" cssClass="btn" theme="simple" onclick="$('#shipmentForm').get(0).action=getFormActionSave();
        removeShipmentFormFields();$('#shipmentForm').get(0).submit();" title="%{getText('btn.save.button')}"/>
        &nbsp;
        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@CANCELED}" name="canceled" scope="page"/>
        <c:if test="${object.status != canceled}">
            <a class="btn"  onclick="Popup=window.open('${invoiceUrl}','Popup','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=800,height=540,left=430,top=23'); return false;"><fmt:message key="invoice.print"/></a>
        </c:if>
        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@PENDING}" name="pendingShipment"
            scope="page"/>
        <c:if test="${object.status == pendingShipment}">
            <!-- we have to use submits instead of hrefs here because if we don't the shipmentForm fields(like shippingMethod) won't be set on the actions object -->
            <a  class="btn" href="javascript: showShipmentDatePopWin();"><fmt:message key="btn.markAsShipped"/></a>
            <s:submit value="%{getText('shipment.cancel')}" id="btn_cancel_shipment" name="btn_cancel_shipment" cssClass="btn" theme="simple" onclick="confirmAndCancel();"></s:submit>
        </c:if>

    </div>
</body>
</html>
