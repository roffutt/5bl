<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<fmt:message key="shipment.view.title" var="titleMessage">
    <fmt:param value="${object.request.id}" />
    <fmt:param>
        <fmt:message key="${object.status.resourceKey}"/>
    </fmt:param>
</fmt:message>
<html>
<head>
    <title>${titleMessage}
    </title>
</head>
<body onload="setFocusToFirstControl();">
    <div class="topbtns">
        <s:set value="%{@com.fiveamsolutions.tissuelocator.data.ShipmentStatus@PENDING}" name="pendingShipment" scope="page"/>
        <c:choose>
            <c:when test="${object.status == pendingShipment}">
                <c:url var="backUrl" value="/admin/shipment/edit.action">
                    <c:param name="object.id" value="${object.id}"/>
                </c:url>
            </c:when>
            <c:otherwise>
                <c:url var="backUrl" value="/admin/shipment/view.action">
                    <c:param name="object.id" value="${object.id}"/>
                </c:url>
            </c:otherwise>
        </c:choose> 
        <a href="${backUrl}" class="btn">Go Back To Order Administration</a>
    </div>

    <tissuelocator:requestReadOnlyView specimenRequest="${object.request}" showCart="false"/>
</body>
</html>
