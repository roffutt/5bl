<%
if (request.getRemoteUser() == null) {
    response.sendRedirect(request.getContextPath() + "/browse.action");
} else {
    response.sendRedirect(request.getContextPath() + "/protected/home.action");
}
%>
