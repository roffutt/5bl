Asinus asinum fricat

This product includes software developed by 5AM and the National Cancer Institute.

THIS SOFTWARE IS PROVIDED "AS IS," AND ANY EXPRESSED OR IMPLIED WARRANTIES, (INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE) ARE DISCLAIMED. IN NO EVENT SHALL THE NATIONAL CANCER INSTITUTE, 5AM SOLUTIONS, INC.
OR THEIR AFFILIATES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.

Beginner's Guide to Biolocator Dev Environment (Wiki):
======================================================

https://intranet.5amsolutions.com/display/TECH/Beginner%27s+Guide+to+Biolocator+Dev+Env+Setup

Development Environment:
========================
In order to configure your development environment for this project, you must perform the following steps.

1.  Read and complete the instructions found in readmeRuntimeEnvironment.txt to prepare your 
	environment to run the Biolocator locally.

2.  Download and Install Maven 3.0.x from http://maven.apache.org/.

3.  Download and install NCI-COMMONS  (only needed if we are currently using a snapshot version of nci-commons)
    From: https://svn.5amsolutions.com/opensource/nci-commons/trunk/
    Run: mvn clean install

4.  (If needed) Configure your settings.xml to contain any needed properties.

5.  Make sure your JAVA_HOME and PATH variables are set to use Java 6 and maven 3.0.x.

6.  Build and Deploy the application:

    Ensure the client theme source has been downloaded from: https://svn.5amsolutions.com/opensource/5bl/clients/
    For the client you plan to deploy (as configured in settings.xml), build the client theme jar:
    From <CLIENT>/trunk/
    mvn clean install

    cd <BL_HOME>/code
    mvn -Pnuke-db,local install cargo:deploy
    
    NOTE:  Only add -Pnuke-db if you want to clean out and rebuild the database.  It needs to be there on the first
    build, but subsequent builds only need it if you want to nuke the db.

    NOTE: If you get a java.lang.OutOfMemoryError: Java heap space error,  run "export MAVEN_OPTS=-Xmx512m"

7. Start JBoss:
    JBOSS_HOME/bin/run.sh

    Note: As of JBoss 4.5 the server is started as localhost only and will not be accessible from VMs. To get around this,
    execute the run.sh script with the parameter of -b 0.0.0.0

8. Access the application at http://localhost:8080/tissuelocator-web/
    For ABRC, Log in with user@example.com/tissueLocator1 or admin@example.com/tissueLocator1.
    For NFCR, Log in with researcher@example.com/tissueLocator1 or coordinator@example.com/tissueLocator1.

9. To trigger the vote tallying process build the app with test-war profile and go to
   
   http://localhost:8080/tissuelocator-web/test/backgroundThread.jsp

   To set the voting period to end the same day, set the <votingPeriod> value to 0 in the pom.xml in the main directory 
   and rebuild.

10. To run the integration tests for tissuelocator-integrator:
  Core app: mvn install cargo:deploy
  Integrator: mvn install cargo:deploy
  Start JBoss: JBOSS_HOME/bin/run.sh
  Run all tests: mvn -Pci-nostart-nodeploy integration-test
  Run only client specific tests: mvn -Pci-nostart-nodeploy -Dskip-main-selenium=true integration-test

11. To verify the validity of the data in the database, build the app with the test-war profile and go to 

  http://localhost:8080/tissuelocator-web/test/validateData.jsp

  Select a class from the select list to run the hibernate validator against all the objects of that class saved in the
  database.  This is useful to verify that all the data in a client's demo and qa tiers is still valid after upgrade 
  scripts have been run or other business logic has changed.  

Important commands to know during development:
====================================================================
mvn install -Dskip-liquibase=true  (the skip liquibase flag is needed anytime
    you don't want the latest db upgrades to run)
cd services  followed by mvn compile hibernate3:hbm2ddl (produce a fresh hibernate schema
         which is useful when writing db update scripts)
mvn install (compiles code, builds packages, tests and verifies coding
            metrics - the most commonly used target)
mvn clean (deletes all built artifacts)
mvn compile (just compiles the source)
mvn test (compiles and executes tests)
mvn site (compiles, packages, and builds a site of reports, but does not fail build if metrics do not pass)
mvn -Pci integration-test  (deploys to the container and runs the full integration test suite
            - only needs to be run in the ear project)
mvn -Pci-nostart-nodeploy integration-test  (runs the full integration test suite - assumes app has been deployed
            and jboss has been started, only needs to be run in the ear project)
mvn -f ear/pom.xml -Plocal,ci-nostart-nodeploy integration-test -Dtest=<TestName> (runs the <TestName> integration test by itself)           
mvn -Pnuke-db initialize  (completely deletes and recreates the db, only needs to be run in the services project,
        also, using other phases will run the rest of the build as normal after the db is recreated.)
mvn process-resources (process application resources, which includes running all of the main and client-specific
    db update scripts the current db has not had executed against it.  Only needs to be run in the services project)
mvn cargo:deploy (deploys to your local jboss)
mvn nci-commons:copy-web-files (copys html, jsp, css,images, etc to therunning container)
mvn nci-commons:jboss-undeploy (undeploys from the local jboss)
mvn -f ear/pom.xml -Plocal,ci clean generate-test-resources resources:resources resources:testResources compiler:compile compiler:testCompile
                (builds everything a selenium test needs to run from eclipse)


Running the selenium tests in eclipse instead of the command line
=================================================================
* Copy testui.properties.example to testui.properties and adjust them to match your enviroment
* Go to com.fiveamsolutions.tissuelocator.test.TestProperties.java and set the PropertiesType
  to PropertiesType.UI(do not commit this)
* Start the selenium server.
* Right click on the test you want to run, then pick "Debug->Debug Configurations..." and make sure
  that the eclipse-out folder is on the tests classpath.  If its not go to the classpath tab, then
  click User Entries and select Advanced... then pick folder and eclipse-out. Then click apply and debug.

Steps to execute when switching clients
=======================================

1. Stop JBoss
2. Remove all files under: $JBOSS_HOME/server/default/tmp
3. Remove all files under: $JBOSS_HOME/server/default/work/jboss.web/localhost/tissuelocator-web
4. Go to the desired Client directory and run mvn clean install
5. From the 5bl base directory, run the following commands:
  mvn -f services/pom.xml -Plocal,nuke-db process-resources
  mvn -f services/pom.xml -Plocal,demo-data sql:execute  (only works for clients whose demo data is not run with the normal build)
  mvn -f services/pom.xml -Plocal,qa-data sql:execute  (only works for clients whose qa data is not run with the normal build)
6. In the ~/.m2/settings.xml file, make sure to change the "client.name" property in the local profile to the desired client
7. Run "mvn -Dskip-liquibase=true clean install cargo:deploy" to build and deploy the application.
8. Clear the cached resources for the browser just to make sure you will be using all necessary resources


Code Layout overview:
=====================
At the code level, there are four major components to the application:

1.  core application (https://svn.5amsolutions.com/opensource/5bl/trunk/code/) - where this file is found.
2.  client theme jars (https://svn.5amsolutions.com/opensource/5bl/clients/)
3.  integrator (https://svn.5amsolutions.com/opensource/5bl/trunk/integrator-code/)
4.  extractor (https://svn.5amsolutions.com/opensource/5bl/trunk/extractor-code/).

The core app includes all the user facing functionality of the application.  It also includes background tasks for the specimen
request review processing. The client theme jars include client specific stylesheets, layout, images and text.  The integrator
includes functionality for importing specimen data from external data sources into the core application's database.  The integrator
depends on the core application in that it uses some remote services exposed by the core application.  The integrator itself does
not include any UI. It just periodically performs its data imports.  The extractor is a small utility that we wrote to help
integrate caTissue with the biolocator.  It depends on a web service exposed by the integrator, and one of the selenium tests in
the integrator actually runs the extractor as part of its execution.

Our automated builds build and test all four components whenever they run.  Therefore, when making any change you need to at least
be thinking about how our changes in one place affect the whole architecture.  You can run all the tests for all the various clients
and components, but that's time consuming given the number of possible combinations.

Which leads us to....

Running the CI build:
=====================
Because the Biolocator has a number of configurations and components, developers are only expected to ensure that the
configuration they are building for will pass CI before committing code.  However, if any other portion of the
ci build breaks after you commit, it is expected that fixing whatever broke is your new top priority.

For reference the ci build completes the following steps for every client:
1.  build all components of the application for the appropriate client (theme jars, core code, integrator, extractor)
2.  nuke the database
3.  populate the database
4.  deploy the application (integrator and core)
5.  run client-specific selenium tests for all components of the application that have them (core app and integrator)
6.  run client-agnostic selenium tests for the core app and integrator if the client is ABRC

So to avoid the sombrero of shame you must ensure that the theme jar and the core application build and pass
all tests prior to committing.  This includes the client specific and client agnostic selenium tests.  If you
are working on the integrator or extractor, then those components must also pass the build.  To simulate the ci
build for the core application execute the following steps:

- from theme jar directory
mvn clean install site

- from main code directory
shutdown jboss
mvn -Dskip-liquibase=true clean install site cargo:deploy
mvn -f services/pom.xml -Plocal,nuke-db process-resources
mvn -f ear/pom.xml -Plocal,ci integration-test

If you need to run client specific tests for a different client you can build for that client and execute:
mvn -f ear/pom.xml -Plocal,ci -Dskip-main-selenium=true integration-test

If you want to start jboss yourself, replace the profile ci to ci-nostart-nodeploy

Dev, QA, Demo Builds:
==================
As a reference here is a description of the other build environments hudson can execute for us.

Dev:
Number: 1 (hudson build biolocator-dev)
When does it run:  nightly
What it does:
For each client:
1.  build all components of the application for the appropriate client
2.  nuke the database
3.  populate the database
4.  deploy the application
5.  run client-specific selenium tests for the core app and integrator
6.  run client-agnostic selenium tests for the core app and integrator
What it doesn't do:  Nothing, this does everything.

QA:
Number: 1 per client (hudson builds abrc-qa, nfcr-qa, 5bl-biolocator-qa, and nbstrn-qa)
When do they run:  nightly
What it does:
1.  build all components of the application for the appropriate client
2.  run recent database upgrade scripts
3.  deploy the application
What it doesn't do:  Nuke the database, run any selenium tests

Demo:
Number: 1 per client (hudson builds abrc-demo, nfcr-demo, nbstrn-demo, 5bl-biolocator-demo)
When do they run:  manually started (generally do this at the end of each iteration)
What it does:
1.  build all components of the application for the appropriate client
2.  run recent database upgrade scripts
3.  deploy the application
What it doesn't do:  Nuke the database, run any selenium tests

The build server that runs these builds can be found here https://5am-int1.5amsolutions.com/hudson/.
