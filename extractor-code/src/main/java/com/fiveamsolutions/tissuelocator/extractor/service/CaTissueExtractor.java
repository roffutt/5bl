/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.extractor.service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Contact;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.extractor.data.CaTissueExtractionConfig;
import com.fiveamsolutions.tissuelocator.extractor.util.TissueLocatorExtractorRegistry;
import com.fiveamsolutions.tissuelocator.integrator.data.ImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;
import com.fiveamsolutions.tissuelocator.integrator.service.CaTissueSpecimenService;

/**
 * @author ddasgupta
 *
 */
public class CaTissueExtractor implements TissueBankExtractor {

    private static final Logger LOG = Logger.getLogger(CaTissueExtractor.class);

    private static final String SPEC_ID = "specId";
    private static final String DATE_REPLACEMENT = "%DATE%";
    private static final String SUBSELECT_REPLACEMENT = "%SUBSELECT%";

    private static final String UPDATED_SPECIMEN_IDS_QUERY = " select distinct ael.object_identifier as " + SPEC_ID
        + " from catissue_audit_event_log ael join catissue_audit_event ae on ael.audit_event_id = ae.identifier "
        + " where lower(ael.object_name) like 'catissue_%_specimen' "
        + " and ae.event_timestamp >= '" + DATE_REPLACEMENT + "' ";

    private static final String UPDATED_SPECIMEN_CHAR_QUERY = " select distinct spec.identifier as " + SPEC_ID
        + " from catissue_audit_event_log ael "
        + " join catissue_audit_event ae on ael.audit_event_id = ae.identifier "
        + " join catissue_specimen_char sc on ael.object_identifier = sc.identifier"
        + " join catissue_abstract_specimen abspec on abspec.specimen_characteristics_id = sc.identifier"
        + " join catissue_specimen spec on abspec.identifier = spec.identifier "
        + " where lower(ael.object_name) = 'catissue_specimen_char' "
        + " and ae.event_timestamp >= '" + DATE_REPLACEMENT + "' ";

    private static final String ANATOMIC_SITE_QUERY = "select spec.identifier as " + SPEC_ID  + " , "
        + " sc.tissue_site as site, spec.specimen_class as class from catissue_abstract_specimen spec "
        + " left join catissue_specimen_char sc on spec.specimen_characteristics_id = sc.identifier "
        + " where spec.identifier in (" + SUBSELECT_REPLACEMENT + ") ";

    private static final String PARTICIPANT_SITE_QUERY = " select spec.identifier as " + SPEC_ID + ", "
        + " site.name as site from catissue_specimen spec "
        + " left join catissue_specimen_coll_group scg on spec.specimen_collection_group_id = scg.identifier "
        + " left join catissue_coll_prot_reg cpr on scg.collection_protocol_reg_id = cpr.identifier "
        + " left join catissue_participant part on cpr.participant_id = part.identifier "
        + " left join catissue_part_medical_id mrn on part.identifier = mrn.participant_id "
        + " left join catissue_site site on mrn.site_id = site.identifier "
        + " where spec.identifier in (" + SUBSELECT_REPLACEMENT + ") ";

    private static final String PROTOCOL_PI_PHONE_QUERY = " select spec.identifier as " + SPEC_ID + ", "
        + " address.phone_number as phone from catissue_specimen spec "
        + " left join catissue_specimen_coll_group scg on spec.specimen_collection_group_id = scg.identifier "
        + " left join catissue_coll_prot_reg cpr on scg.collection_protocol_reg_id = cpr.identifier "
        + " left join catissue_collection_protocol prot on cpr.collection_protocol_id = prot.identifier "
        + " left join catissue_specimen_protocol spec_prot on prot.identifier = spec_prot.identifier "
        + " left join catissue_user pi on spec_prot.principal_investigator_id = pi.identifier "
        + " left join catissue_address address on pi.address_id = address.identifier "
        + " where spec.identifier in (" + SUBSELECT_REPLACEMENT + ") ";

    private static final String DYNAMIC_EXTENSION_QUERY =
        " select * from " + CaTissueExtractionConfig.getInstance().getDynamicExtensionTable()
        + " where ACTIVITY_STATUS = 'Active' "
        + " and " + CaTissueExtractionConfig.getInstance().getSpecimenIdColumn()
        + " in (" + SUBSELECT_REPLACEMENT + ") ";

    /**
     * {@inheritDoc}
     */
    public void extractData() throws SQLException {
        Connection conn = null;
        Statement s = null;
        try {
            conn = getConnection();
            s = conn.createStatement();

            Map<Long, Specimen> specimens = new HashMap<Long, Specimen>();
            getSpecimenIds(specimens, s, UPDATED_SPECIMEN_IDS_QUERY);
            getSpecimenIds(specimens, s, UPDATED_SPECIMEN_CHAR_QUERY);
            getAnatomicSite(specimens, s, UPDATED_SPECIMEN_IDS_QUERY);
            getAnatomicSite(specimens, s, UPDATED_SPECIMEN_CHAR_QUERY);
            getParticipantSite(specimens, s, UPDATED_SPECIMEN_IDS_QUERY);
            getParticipantSite(specimens, s, UPDATED_SPECIMEN_CHAR_QUERY);
            getProtocolPiPhone(specimens, s, UPDATED_SPECIMEN_IDS_QUERY);
            getProtocolPiPhone(specimens, s, UPDATED_SPECIMEN_CHAR_QUERY);
            getDynamicExtensions(specimens, s, UPDATED_SPECIMEN_IDS_QUERY);
            getDynamicExtensions(specimens, s, UPDATED_SPECIMEN_CHAR_QUERY);

            pushSpecimens(specimens.values());

        } finally {
            if (s != null) {
                s.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    private String replaceDate(String query) {
        Calendar cal = DateUtils.truncate(Calendar.getInstance(), Calendar.DATE);
        cal.add(Calendar.DATE, -CaTissueExtractionConfig.getInstance().getPeriod());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return query.replace(DATE_REPLACEMENT, sdf.format(cal.getTime()));
    }

    private void getSpecimenIds(Map<Long, Specimen> specimens, Statement s, String query) throws SQLException {
        ResultSet rs = s.executeQuery(replaceDate(query));
        while (rs.next()) {
            Long specId = Long.valueOf(rs.getInt(SPEC_ID));
            Specimen specimen = new Specimen();
            specimen.setId(specId);
            specimens.put(specId, specimen);
        }
    }

    private void getAnatomicSite(Map<Long, Specimen> specimens, Statement s, String subselect)
        throws SQLException {
        ResultSet rs = s.executeQuery(ANATOMIC_SITE_QUERY.replace(SUBSELECT_REPLACEMENT, replaceDate(subselect)));
        while (rs.next()) {
            Long specId = Long.valueOf(rs.getInt(SPEC_ID));
            Specimen specimen = specimens.get(specId);
            String specimenClass = rs.getString("class");
            if ("Cell".equals(specimenClass)) {
                specimenClass = "Cells";
            }
            SpecimenType st = new SpecimenType();
            st.setSpecimenClass(SpecimenClass.valueOf(StringUtils.upperCase(specimenClass)));
            specimen.setSpecimenType(st);
        }
    }

    private void getParticipantSite(Map<Long, Specimen> specimens, Statement s, String subselect)
        throws SQLException {
        ResultSet rs = s.executeQuery(PARTICIPANT_SITE_QUERY.replace(SUBSELECT_REPLACEMENT, replaceDate(subselect)));
        while (rs.next()) {
            Long specId = Long.valueOf(rs.getInt(SPEC_ID));
            Specimen specimen = specimens.get(specId);
            Institution i = new Institution();
            i.setName(rs.getString("site"));
            Participant p = new Participant();
            p.setExternalIdAssigner(i);
            specimen.setParticipant(p);
        }
    }

    private void getProtocolPiPhone(Map<Long, Specimen> specimens, Statement s, String subselect)
        throws SQLException {
        ResultSet rs = s.executeQuery(PROTOCOL_PI_PHONE_QUERY.replace(SUBSELECT_REPLACEMENT, replaceDate(subselect)));
        while (rs.next()) {
            Long specId = Long.valueOf(rs.getInt(SPEC_ID));
            Specimen specimen = specimens.get(specId);
            Contact c = new Contact();
            c.setPhone(rs.getString("phone"));
            CollectionProtocol cp = new CollectionProtocol();
            cp.setContact(c);
            specimen.setProtocol(cp);
        }
    }

    private void getDynamicExtensions(Map<Long, Specimen> specimens, Statement s, String subselect)
        throws SQLException {
        CaTissueExtractionConfig conf = CaTissueExtractionConfig.getInstance();
        String query = DYNAMIC_EXTENSION_QUERY.replace(SUBSELECT_REPLACEMENT, replaceDate(subselect));
        ResultSet rs = s.executeQuery(query);
        while (rs.next()) {
            Long specId = Long.valueOf(rs.getInt(conf.getSpecimenIdColumn()));
            Specimen specimen = specimens.get(specId);
            specimen.setPriceNegotiable(rs.getBoolean(conf.getPriceNegotiableColumn()));
            if (rs.getObject(conf.getMinimumPriceColumn()) != null) {
                specimen.setMinimumPrice(new BigDecimal(rs.getDouble(conf.getMinimumPriceColumn())));
            }
            if (rs.getObject(conf.getMaximumPriceColumn()) != null) {
                specimen.setMaximumPrice(new BigDecimal(rs.getDouble(conf.getMaximumPriceColumn())));
            }
        }
    }

    private Connection getConnection() throws SQLException {
        try {
            CaTissueExtractionConfig conf = CaTissueExtractionConfig.getInstance();
            String driver = conf.getDatabaseDriver();
            String url = conf.getDatabaseUrl();
            String username = conf.getDatabaseUsername();
            String password = conf.getDatabasePassword();
            Class.forName(driver).newInstance();
            return DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            LOG.error("exception getting connection", e);
            throw new IllegalArgumentException(e);
        }
    }

    private void pushSpecimens(Collection<Specimen> specimens) {
        CaTissueSpecimenService specService =
            TissueLocatorExtractorRegistry.getServiceLocator().getCaTissueSpecimenService();
        SpecimenList specimenList = new SpecimenList();
        specimenList.setSpecimens(new ArrayList<Specimen>(specimens));
        Long locationId = CaTissueExtractionConfig.getInstance().getLocationId();
        ImportBatch ib = specService.processSpecimens(specimenList, locationId);
        LOG.info("processing start time: " + ib.getStartTime());
        LOG.info("processing end time : " + ib.getEndTime());
        LOG.info("imports: " + ib.getImports());
        LOG.info("failures: " + ib.getFailures());
    }
}
