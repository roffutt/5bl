/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.web.test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.nci.commons.data.search.PageSortParams;
import com.fiveamsolutions.nci.commons.search.SearchCriteria;
import com.fiveamsolutions.tissuelocator.integrator.service.GenericServiceLocal;

/**
 * @param <T> type to manipulate
 * @author smiller
 */
public class GenericServiceStub<T extends PersistentObject> implements GenericServiceLocal<T> {

    private T object;
    private boolean searchCalled = false;
    private static final Logger LOG = Logger.getLogger(GenericServiceStub.class);

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public <TYPE extends PersistentObject> TYPE getPersistentObject(Class<TYPE> toClass, Long id) {
        try {
            PersistentObject obj = toClass.newInstance();
            BeanUtils.setProperty(obj, "id", id);
            return (TYPE) obj;
        } catch (IllegalAccessException e)  {
            return null;
        } catch (InstantiationException e) {
            return null;
        } catch (InvocationTargetException e) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    public Long savePersistentObject(T o) {
        this.object = o;
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public void deletePersistentObject(T o) {
        this.object = o;
    }

    /**
     * @return the object
     */
    public T getObject() {
        return this.object;
    }

    /**
     * {@inheritDoc}
     */
    public <TYPE extends PersistentObject> List<TYPE> getAll(Class<TYPE> type) {
        List<TYPE> result = new ArrayList<TYPE>();
        try {
            TYPE localObject = type.newInstance();
            result.add(localObject);
        } catch (InstantiationException e) {
            LOG.error(e);
        } catch (IllegalAccessException e) {
            LOG.error(e);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public int count(SearchCriteria<T> criteria) {
        searchCalled = true;
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    public List<T> search(SearchCriteria<T> criteria, PageSortParams<T> params) {
        searchCalled = true;
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    public List<T> search(SearchCriteria<T> criteria) {
        return search(criteria, null);
    }

    /**
     * @return whether search has been called
     */
    public boolean isSearchCalled() {
        boolean oldSearchCalled = searchCalled;
        searchCalled = false;
        return oldSearchCalled;
    }

}
