/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.web.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.integrator.service.adapter.AdapterLauncher;
import com.fiveamsolutions.tissuelocator.integrator.web.test.AbstractTissueLocatorIntegratorWebTest;

/**
 * @author bpickeral
 *
 */
public class ContextListenerHelperTest extends AbstractTissueLocatorIntegratorWebTest {
    private static final int STOP_START_SEQUENCE = 3;

    /**
     * test the context listener.
     */
    @Test
    public void testContextListener() {
        MockAdapterLauncher launcher = new MockAdapterLauncher();
        ContextListenerHelper.setLauncher(launcher);

        assertEquals(0, launcher.getLaunchCount());

        ContextListenerHelper.getInstance().relaunchAdapterThreads();

        // If Helper calls stop and then start, result should be 3
        assertEquals(STOP_START_SEQUENCE, launcher.getLaunchCount());
    }

    /**
     * mock adapter launcher.
     * @author bpickeral
     */
    class MockAdapterLauncher extends AdapterLauncher {
        private static final int STOP_INT = 6;
        private static final int START_INT = 2;

        private int launchSequence = 0;

        /**
         * {@inheritDoc}
         */
        @Override
        public int launchAdapterThreads() {
            launchSequence = launchSequence / START_INT;
            return 1;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void stopAdapterThreads() {
            launchSequence = launchSequence + STOP_INT;
        }

        /**
         * @return the launchCount
         */
        public int getLaunchCount() {
            return launchSequence;
        }
    }
}
