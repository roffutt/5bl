insert into integration_config (id, institutionid, interval, starttime, notification_interval, notification_email) values (nextval('hibernate_sequence'), 2, 30000, now(), 86400000, 'test@example.com');
insert into catissue_integration_config (config_id, gridserviceurl) values ((select max(id) from integration_config), 'http://192.168.182.123:8180/wsrf/services/cagrid/CaTissueSuite');
insert into config_code_set (config_id, code_set_name, index) values ((select max(id) from integration_config), 'caTissue 1.1.1 Base', 1);
