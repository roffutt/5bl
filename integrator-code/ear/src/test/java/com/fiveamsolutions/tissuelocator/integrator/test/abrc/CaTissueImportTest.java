/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.test.abrc;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.integrator.test.AbstractTissueLocatorSeleniumTest;
import com.fiveamsolutions.tissuelocator.integrator.test.SeleniumTestProperties;

/**
 * @author ddasgupta
 */
public class CaTissueImportTest extends AbstractTissueLocatorSeleniumTest {

    private static final Logger LOG = Logger.getLogger(CaTissueImportTest.class);
    private static final String EXTRACTOR_JAR_NAME = "tissuelocator-extractor.jar";
    private static final String EXTRACTOR_DEPLOY_DIRECTORY = "../../extractor-code/target/deploy";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "CaTissueImportTest.sql";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp() throws Exception {
        super.setUp();
        runSqlScript("caTissue-deleteData.sql", getCaTissueDatabaseConnection());
        runSqlScript("caTissue-addData.sql", getCaTissueDatabaseConnection());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        runSqlScript("caTissue-deleteData.sql", getCaTissueDatabaseConnection());
    }

    private Connection getCaTissueDatabaseConnection() throws Exception {
        String driver = SeleniumTestProperties.getProperties().getProperty("catissue.database.driver");
        String url = SeleniumTestProperties.getProperties().getProperty("catissue.database.url");
        String username = SeleniumTestProperties.getProperties().getProperty("catissue.database.username");
        String password = SeleniumTestProperties.getProperties().getProperty("catissue.database.password");
        Class.forName(driver).newInstance();
        return DriverManager.getConnection(url, username, password);
    }

    /**
     * Test the caTissue specimen import.
     * @throws Exception on error
     */
    @Test
    public void testCaTissueImport() throws Exception {
        runExtractor();
        verifyResult();
        runExtractor();
        verifyResult();
    }

    private void runExtractor() throws Exception {
        String jarName = EXTRACTOR_JAR_NAME;
        ProcessBuilder pb = new ProcessBuilder("java", "-jar", jarName, "CATISSUE");
        pb.directory(new File(EXTRACTOR_DEPLOY_DIRECTORY));
        Process p = pb.start();
        int exitVal = p.waitFor();
        LOG.debug("exit value: " + exitVal);
        assertEquals(0, exitVal);

        InputStream is = p.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line = br.readLine();
        while (line != null) {
            LOG.debug(line);
            line = br.readLine();
        }
    }

    private void verifyResult() {
        selenium.open(CONTEXT_ROOT + "/updateHomePageCache.jsp");
        assertTrue(selenium.isTextPresent("Home Page Data Cache Updated"));
        login("admin@example.com", "tissueLocator1", true);
        assertTrue(selenium.isTextPresent("Cells (0)"));
        assertTrue(selenium.isTextPresent("Fluid (1)"));
        assertTrue(selenium.isTextPresent("Molecular (0)"));
        assertTrue(selenium.isTextPresent("Tissue (1)"));
        verifySpecimen1();
        clickAndWait("link=Home");
        verifySpecimen2();
        clickAndWait("link=Sign Out");
    }

    private void verifySpecimen1() {
        clickAndWait("link=Tissue (1)");
        String disease = selenium.getTable("specimen.1.2");
        String expectedHeader = disease.replaceAll("\\s+", "");
        assertEquals("Tissue", selenium.getTable("specimen.1.4"));
        assertEquals("Spindle cell carcinoma (morphologic abnormality)", disease);
        assertEquals("50 milligrams", selenium.getTable("specimen.1.6"));
        assertEquals("test barcode 1", selenium.getTable("specimen.1.8"));
        assertEquals("$10.00 - $20.00", selenium.getTable("specimen.1.7"));

        clickAndWait("xpath=//table[@id='specimen']/tbody/tr/td[2]/a");
        String actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
        assertTrue(selenium.isTextPresent("Biospecimen Characteristics"));
        assertTrue(selenium.isTextPresent("Spindle cell carcinoma (morphologic abnormality)"));
        assertTrue(selenium.isTextPresent("Tissue"));
        assertTrue(selenium.isTextPresent("50 milligrams"));
        assertTrue(selenium.isTextPresent("1 years"));
        assertTrue(selenium.isTextPresent("2010"));
        assertTrue(selenium.isTextPresent("test-participant"));
        assertTrue(selenium.isTextPresent("test barcode 1"));
        assertTrue(selenium.isTextPresent("$10.00 - $20.00"));
        assertTrue(selenium.isTextPresent("Male"));
        assertTrue(selenium.isTextPresent("Black or African American"));
        assertTrue(selenium.isTextPresent("Hispanic or Latino"));
    }

    private void verifySpecimen2() {
        clickAndWait("link=Fluid (1)");
        String disease = selenium.getTable("specimen.1.2");
        String expectedHeader = disease.replaceAll("\\s+", "");
        assertEquals("Bile", selenium.getTable("specimen.1.4"));
        assertEquals("Spindle cell carcinoma (morphologic abnormality)", disease);
        assertEquals("75 milliliters", selenium.getTable("specimen.1.6"));
        assertEquals("test barcode 2", selenium.getTable("specimen.1.8"));
        assertEquals("To be negotiated", selenium.getTable("specimen.1.7"));

        clickAndWait("xpath=//table[@id='specimen']/tbody/tr/td[2]/a");
        String actualHeader = selenium.getText("//div[@id='content']/h1").replaceAll("\\s+", "");
        assertEquals(expectedHeader, actualHeader);
        assertTrue(selenium.isTextPresent("Biospecimen Characteristics"));
        assertTrue(selenium.isTextPresent("Spindle cell carcinoma (morphologic abnormality)"));
        assertTrue(selenium.isTextPresent("Bile"));
        assertTrue(selenium.isTextPresent("75 milliliters"));
        assertTrue(selenium.isTextPresent("1 years"));
        assertTrue(selenium.isTextPresent("2010"));
        assertTrue(selenium.isTextPresent("test-participant"));
        assertTrue(selenium.isTextPresent("test barcode 2"));
        assertTrue(selenium.isTextPresent("To be negotiated"));
        assertTrue(selenium.isTextPresent("Male"));
        assertTrue(selenium.isTextPresent("Black or African American"));
        assertTrue(selenium.isTextPresent("Hispanic or Latino"));
    }
}
