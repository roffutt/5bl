/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.test;

import java.text.NumberFormat;

/**
 * @author smiller
 */
public abstract class AbstractTissueLocatorSeleniumTest extends AbstractSeleniumTest {

    /**
     * password.
     */
    protected static final String PASSWORD = "tissueLocator1";

    /**
     * basic error message.
     */
    protected static final String ERROR_MESSAGE = "Sorry, there is a problem with one or more fields in the form.";

    /**
     * Default delay.
     */
    protected static final int DELAY = 1500;

    /**
     * long delay.
     */
    protected static final int LONG_DELAY = 3000;

    /**
     * Short delay.
     */
    protected static final int SHORT_DELAY = 100;

    /**
     * Context root.
     */
    protected static final String CONTEXT_ROOT = "/tissuelocator-web";

    /**
     * login to the app.
     * @param username the username.
     * @param password the password
     * @param shouldWork expected pass or fail?
     */
    protected void login(String username, String password, boolean shouldWork) {
        selenium.open(CONTEXT_ROOT);

        //verify that the svn info was properly substituted
        assertTrue(selenium.isTextPresent("Version"));
        assertFalse(selenium.isTextPresent("${svn.version}"));
        assertFalse(selenium.isTextPresent("${svn.revision}"));

        if (selenium.isElementPresent("link=Sign Out")) {
            testLoggedIn(true);
        } else {
            testLoggedIn(false);
            assertTrue(selenium.isTextPresent("Sign In"));
            assertTrue(selenium.isTextPresent("Email Address"));
            assertTrue(selenium.isTextPresent("Password"));
            assertTrue(selenium.isElementPresent("j_username"));
            assertTrue(selenium.isElementPresent("j_password"));
            assertTrue(selenium.isElementPresent("submitLogin"));

            selenium.type("j_username", username);
            selenium.type("j_password", password);
            clickAndWait("submitLogin");
        }

        if (shouldWork) {
            testLoggedIn(true);
            assertTrue(selenium.isTextPresent("Home"));
        } else {
            testLoggedIn(false);
            assertTrue(selenium.isTextPresent("Invalid username and/or password, please try again."));
        }
    }

    private void testLoggedIn(boolean loggedIn) {
        assertEquals(loggedIn, selenium.isTextPresent("Welcome,"));
        assertTrue(selenium.isElementPresent("link=Home"));
        assertEquals(loggedIn, selenium.isElementPresent("link=Sign Out"));
        assertEquals(!loggedIn, selenium.isElementPresent("link=Sign In"));
        assertEquals(!loggedIn, selenium.isElementPresent("link=Forgot password?"));
    }

    /**
     * login expecting success.
     * @param username the username.
     * @param password the password.
     */
    protected void login(String username, String password) {
        login(username, password, true);
    }

    /**
     * login as user1 expecting success.
     */
    protected void loginAsUser1() {
        login(ClientProperties.getResearcherEmail(), PASSWORD, true);
    }

    /**
     * login as admin, expecting success.
     */
    protected void loginAsAdmin() {
        login(ClientProperties.getConsortiumAdminEmail(), PASSWORD, true);
    }

    /**
     * test collapsing and expanding of a div.
     * @param linkText the link to trigger the expand and collapse
     * @param divId the id of the div to be shown or hidden
     */
    protected void collapseSection(String linkText, String divId) {
        assertTrue(selenium.isVisible(divId));
        assertTrue(selenium.isElementPresent("link=" + linkText));
        selenium.click("link=" + linkText);
        pause(DELAY);
        assertFalse(selenium.isVisible(divId));
        selenium.click("link=" + linkText);
        pause(DELAY);
        assertTrue(selenium.isVisible(divId));
    }

    /**
     * Tests the results count.
     * @param resultCount the number of results
     * @param pageSize the page size
     */
    protected void resultsCount(int resultCount, int pageSize) {
        String formatedResults = NumberFormat.getInstance().format(resultCount);
        if (resultCount == 0) {
            assertTrue(selenium.isTextPresent("0 Results found."));
        } else if (resultCount == 1) {
            assertTrue(selenium.isTextPresent("1-1 of 1 Result"));
        } else if (pageSize < resultCount) {
            assertTrue(selenium.isTextPresent("1-" + pageSize + " of " + formatedResults + " Results"));
        } else {
            assertTrue(selenium.isTextPresent("1-" + resultCount + " of " + formatedResults + " Results"));
        }
    }

    /**
     * Shows only test results.
     */
    protected void showOnlyTestResults() {
        selenium.type("externalId", "test");
        clickAndWait("btn_search");
    }

    /**
     * Shows all results.
     */
    protected void showAllResults() {
        selenium.type("externalId", "");
        clickAndWait("btn_search");
    }

    /**
     * Mouses over the locator and pasues for a delay.
     * @param locator the location to mouseover.
     */
    protected void mouseOverAndPause(String locator) {
        selenium.mouseOver(locator);
        pause(SHORT_DELAY);
    }
}
