/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.test.abrc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.fiveamsolutions.tissuelocator.integrator.cbm.service.CbmPersistenceService;
import com.fiveamsolutions.tissuelocator.integrator.data.CbmDataExportConfig;
import com.fiveamsolutions.tissuelocator.integrator.test.AbstractTissueLocatorSeleniumTest;
import com.fiveamsolutions.tissuelocator.integrator.test.SeleniumTestProperties;

/**
 * @author smiller
 *
 */
public class CbmExportTest extends AbstractTissueLocatorSeleniumTest {
    private static final int START_TIME_DELAY = 20;
    private static final int MILLIS_PER_SIX_SECONDS = 6000;
    private static final int MILLIS_PER_MIN = 60000;
    private static final int MILLIS_PER_THREE_MIN = 180000;
    private static final int MILLIS_PER_HOUR = 60 * MILLIS_PER_MIN;
    private static final int NUMBER_OF_SPECIMENS_TO_EXPORT = 2;
    private static final Logger LOG = Logger.getLogger(CbmExportTest.class);

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getScriptName() {
        return "cbmExportTest.sql";
    }

    /**
     * Test the round trip of the flat-file import.  Note that the specific Synchronization adapters are
     *  individually tested in the unit tests.
     * @throws Exception if error occurs
     */
    @Test
    public void testCbmExport() throws Exception {
        Calendar st = Calendar.getInstance();
        st.add(Calendar.SECOND, START_TIME_DELAY);
        runSqlCommand("insert into export_config (id, interval, notification_email, starttime, code_set) values "
                + " (1, " + MILLIS_PER_HOUR + ", 'test@example.com', '" + st.get(Calendar.HOUR_OF_DAY) + ":"
                + st.get(Calendar.MINUTE) + ":" + st.get(Calendar.SECOND) + "', 'CBM 1.0')");
        runSqlCommand("insert into cbm_export_config (config_id, batchsize, db_driver_class, db_url, db_password, "
                + "db_username) values (1, 100, '" + SeleniumTestProperties.getCbmDriver() + "', '"
                + SeleniumTestProperties.getCbmUrl() + "', '" + SeleniumTestProperties.getCbmPassworString() + "', '"
                + SeleniumTestProperties.getCbmUsername() + "')");
        selenium.open("/tissuelocator-integrator-web/launchAdapterThreads.jsp");
        pause(MILLIS_PER_MIN);
        verifyExport();
    }

    private void verifyExport() throws Exception {
        int totalTime = 0;
        int actualExports = testExportCount();
        while (actualExports < NUMBER_OF_SPECIMENS_TO_EXPORT && totalTime < MILLIS_PER_THREE_MIN) {
            totalTime += MILLIS_PER_SIX_SECONDS;
            pause(MILLIS_PER_SIX_SECONDS);
            actualExports = testExportCount();
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Exported: " + NUMBER_OF_SPECIMENS_TO_EXPORT + " specimen(s), seen in CBM: " + actualExports);
        }
        assertEquals(NUMBER_OF_SPECIMENS_TO_EXPORT, actualExports);
    }
    
    private int testExportCount() throws Exception {
        String sql = "select SpecimenCollectionSummary.count from "
            + "SpecimenCollectionSummary "
            + "where "
            + "SpecimenCollectionSummary.patientAgeAtCollection = 10 and "
            + "SpecimenCollectionSummary.specimenType = 'Bone Marrow'";
        CbmPersistenceService service = getCbmService();
        service.connect();
        Statement s = null;
        ResultSet rs = null;
        int actualCount = -1;
        try {
            Connection c = service.getConnection();
            s = c.createStatement();
            rs = s.executeQuery(sql);
            if (rs.next()) {
                actualCount = rs.getInt(1);
                assertFalse(rs.next());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (s != null) {
                s.close();
            }
            service.disconnect();
        }
        return actualCount;
    }


    /**
     * @return
     */
    private CbmPersistenceService getCbmService() {
        CbmDataExportConfig config = new CbmDataExportConfig();
        config.setDbDriverClass(SeleniumTestProperties.getCbmDriver());
        config.setDbPassword(SeleniumTestProperties.getCbmPassworString());
        config.setDbUrl(SeleniumTestProperties.getCbmUrl());
        config.setDbUsername(SeleniumTestProperties.getCbmUsername());
        return new CbmPersistenceService(config);
    }
}
