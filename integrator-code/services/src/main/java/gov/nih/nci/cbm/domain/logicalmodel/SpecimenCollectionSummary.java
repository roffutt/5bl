/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package gov.nih.nci.cbm.domain.logicalmodel;

/**
 * Distinguishable portion of biomaterial.
 * 
 * @version 1.0
 */
public class SpecimenCollectionSummary {

    /**
     * Anatomic source from which the Specimen was collected.
     */
    private String anatomicSource;

    /**
     * Number of specimens with the same collection summary information, originating from the same profile of patient.
     */
    private Integer count;

    /**
     * Age of patient on tissue collection date.
     */
    private String patientAgeAtCollection;

    /**
     * A description of the type of specimen that is stored (blood, serum, tissue, DNA, ...).
     */
    private String specimenType;

    /**
     * A Patient provides at least one Specimen.
     */
    private ParticipantCollectionSummary participantCollectionSummary;

    /**
     * A Preservation method is applied to zero or more Specimens.
     */
    private Preservation undergoes;
    
    private Long id;
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the anatomicSource
     */
    public String getAnatomicSource() {
        return anatomicSource;
    }

    /**
     * @param anatomicSource the anatomicSource to set
     */
    public void setAnatomicSource(String anatomicSource) {
        this.anatomicSource = anatomicSource;
    }

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return the patientAgeAtCollection
     */
    public String getPatientAgeAtCollection() {
        return patientAgeAtCollection;
    }

    /**
     * @param patientAgeAtCollection the patientAgeAtCollection to set
     */
    public void setPatientAgeAtCollection(String patientAgeAtCollection) {
        this.patientAgeAtCollection = patientAgeAtCollection;
    }

    /**
     * @return the specimenType
     */
    public String getSpecimenType() {
        return specimenType;
    }

    /**
     * @param specimenType the specimenType to set
     */
    public void setSpecimenType(String specimenType) {
        this.specimenType = specimenType;
    }

    /**
     * @return the participantCollectionSummary
     */
    public ParticipantCollectionSummary getParticipantCollectionSummary() {
        return participantCollectionSummary;
    }

    /**
     * @param participantCollectionSummary the participantCollectionSummary to set
     */
    public void setParticipantCollectionSummary(ParticipantCollectionSummary participantCollectionSummary) {
        this.participantCollectionSummary = participantCollectionSummary;
    }

    /**
     * @return the undergoes
     */
    public Preservation getUndergoes() {
        return undergoes;
    }

    /**
     * @param undergoes the undergoes to set
     */
    public void setUndergoes(Preservation undergoes) {
        this.undergoes = undergoes;
    }
}