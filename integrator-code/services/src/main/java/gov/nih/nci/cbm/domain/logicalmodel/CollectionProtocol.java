/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package gov.nih.nci.cbm.domain.logicalmodel;

import java.util.Date;

/**
 * The Protocol or Study for which the Specimens are collected.
 * 
 * @version 1.0
 */
public class CollectionProtocol {

    /**
     * Date collection information was most recently updated.
     */
    private Date dateLastUpdated;
    
    /**
     * Date when the study officially closes.
     */
    private Date endDate;
    
    private Long id;

    private String identifier;

    /**
     * The name of the study for which the specimens are identified.
     */
    private String name;

    /**
     * The date on which the collection begins collecting specimens.
     */
    private Date startDate;

    /**
     * Rules contained in Access Data restrict access to one Collection by creating rules for requestors. Each set of
     * Access Data rules applies to exactly one Collection.
     */
    private SpecimenAvailabilitySummaryProfile specimenAvailabilitySummary;

    /**
     * A Contact person is responsible for zero or more Collections.
     */
    private SpecimenCollectionContact contact;

    /**
     * Zero or more Collection Summary Data indicates whether information is stored for one Collection.
     */
    private AnnotationAvailabilityProfile annotationAvailability;

    /**
     * An Institution owns at least one Collection.
     */
    private Institution institution;
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the dateLastUpdated
     */
    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    /**
     * @param dateLastUpdated the dateLastUpdated to set
     */
    public void setDateLastUpdated(Date dateLastUpdated) {
        this.dateLastUpdated = dateLastUpdated;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the specimenAvailabilitySummary
     */
    public SpecimenAvailabilitySummaryProfile getSpecimenAvailabilitySummary() {
        return specimenAvailabilitySummary;
    }

    /**
     * @param specimenAvailabilitySummary the specimenAvailabilitySummary to set
     */
    public void setSpecimenAvailabilitySummary(SpecimenAvailabilitySummaryProfile specimenAvailabilitySummary) {
        this.specimenAvailabilitySummary = specimenAvailabilitySummary;
    }

    /**
     * @return the contact
     */
    public SpecimenCollectionContact getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(SpecimenCollectionContact contact) {
        this.contact = contact;
    }

    /**
     * @return the annotationAvailability
     */
    public AnnotationAvailabilityProfile getAnnotationAvailability() {
        return annotationAvailability;
    }

    /**
     * @param annotationAvailability the annotationAvailability to set
     */
    public void setAnnotationAvailability(AnnotationAvailabilityProfile annotationAvailability) {
        this.annotationAvailability = annotationAvailability;
    }

    /**
     * @return the institution
     */
    public Institution getInstitution() {
        return institution;
    }

    /**
     * @param institution the institution to set
     */
    public void setInstitution(Institution institution) {
        this.institution = institution;
    }
}