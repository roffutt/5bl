/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.fiveamsolutions.dynamicextensions.struts2.converter.DynamicExtensionsTypeConversionException;
import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.DefaultImportValueConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.IntegrationConfigurationException;
import com.fiveamsolutions.tissuelocator.integrator.util.NotificationHelper;
import com.fiveamsolutions.tissuelocator.integrator.util.ThreadsyncedCache;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceRemote;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceRemote;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;
import com.fiveamsolutions.tissuelocator.service.extension.DynamicExtensionsServiceRemote;

/**
 * @author ddasgupta, smiller
 */
public class SpecimenBatchProcessor {

    private final ThreadsyncedCache<CollectionProtocol> protocolCache;
    private final ThreadsyncedCache<Participant> participantCache;
    private CollectionProtocol defaultProtocol;

    /**
     * Build the processor.
     * @param protocolCache the protocol cache.
     * @param participantCache the participant cache.
     */
    public SpecimenBatchProcessor(ThreadsyncedCache<CollectionProtocol> protocolCache,
            ThreadsyncedCache<Participant> participantCache) {
        this.participantCache = participantCache;
        this.protocolCache = protocolCache;
    }

    /**
     * Process a specimen batch.
     * @param batch the file import batch
     * @param fileSpecimens the batch of specimens
     * @param adapter the synchronization adapter
     * @param institution the institution
     * @param translationMap the translation map
     */
    public void processSpecimenBatch(FileImportBatch batch, Collection<Specimen> fileSpecimens,
            SpecimenSynchronizationAdapter adapter, Institution institution,
            Map<String, Map<String, Map<String, String>>> translationMap) {
        try {
            Long instId = batch.getConfig().getInstitutionId();
            Map<String, Specimen> specimens = preloadSpecimens(fileSpecimens, instId);
            preloadParticipants(fileSpecimens, specimens.values(), instId);
            preloadProtocols(instId);

            for (Specimen specimen : fileSpecimens) {
                if (specimen.getProtocol() == null && defaultProtocol != null) {
                    specimen.setProtocol(protocolCache.getFromCache(this, defaultProtocol.getName()));
                }

                Specimen preloadedSpecimen = specimens.get(specimen.getExternalId());
                Map<String, PersistentObject> dependencies = new HashMap<String, PersistentObject>();
                Participant preloadedParticipant = participantCache.getFromCache(this,
                        specimen.getParticipant().getExternalId());
                dependencies.put("participant", preloadedParticipant);
                CollectionProtocol preloadedProtocol = specimen.getProtocol() != null
                    ? protocolCache.getFromCache(this, specimen.getProtocol().getName())
                    : null;
                dependencies.put("protocol", preloadedProtocol);
                Specimen synced = processSpecimen(batch, specimen, adapter, institution, translationMap,
                        preloadedSpecimen, dependencies);
                updateDependencies(synced);
            }
        } finally {
            participantCache.clearCache(this);
            protocolCache.clearCache(this);
        }
    }

    private void updateDependencies(Specimen synced) {
        if (synced == null) {
            return;
        }

        if (synced.getParticipant() != null && synced.getParticipant().getId() != null) {
            participantCache.addToAllCaches(this, synced.getParticipant().getExternalId(), synced.getParticipant());
        }
        if (synced.getProtocol() != null && synced.getProtocol().getId() != null) {
            protocolCache.addToAllCaches(this, synced.getProtocol().getName(), synced.getProtocol());
        }
    }

    private Map<String, Specimen> preloadSpecimens(Collection<Specimen> importedSpecimens, Long institutionId) {
        Set<String> extIds = new HashSet<String>();
        for (Specimen specimen : importedSpecimens) {
            extIds.add(specimen.getExternalId());
        }
        SpecimenServiceRemote specimenService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getSpecimenService();
        return specimenService.getSpecimens(institutionId, extIds);
    }

    private void preloadParticipants(Collection<Specimen> importedSpecimens,
            Collection<Specimen> preloadedSpecimens, Long institutionId) {
        Set<String> extIds = new HashSet<String>();
        for (Specimen specimen : importedSpecimens) {
            if (specimen.getParticipant() != null) {
                extIds.add(specimen.getParticipant().getExternalId());
            }
        }
        for (Specimen specimen : preloadedSpecimens) {
            participantCache.addToCache(this, specimen.getParticipant().getExternalId(), specimen.getParticipant());
            extIds.remove(specimen.getParticipant().getExternalId());
        }
        ParticipantServiceRemote participantService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getParticipantService();
        Map<String, Participant> participants = participantService.getParticipants(institutionId, extIds);
        for (Entry<String, Participant> entry : participants.entrySet()) {
            participantCache.addToCache(this, entry.getKey(), entry.getValue());
        }
    }

    private void preloadProtocols(Long institutionId) {
        CollectionProtocolServiceRemote cpService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getCollectionProtocolService();
        List<CollectionProtocol> institutionProtocols = cpService.getProtocolsByInstitution(institutionId);
        for (CollectionProtocol protocol : institutionProtocols) {
            protocolCache.addToCache(this, protocol.getName(), protocol);
        }

        if (defaultProtocol == null && institutionProtocols.size() == 1) {
            defaultProtocol = institutionProtocols.get(0);
        }
    }

    @SuppressWarnings("PMD.ExcessiveParameterList")
    private Specimen processSpecimen(FileImportBatch batch, Specimen specimen, SpecimenSynchronizationAdapter adapter,
            Institution institution, Map<String, Map<String, Map<String, String>>> translationMap,
            Specimen preloadedSpecimen, Map<String, PersistentObject> dependencies) {
        Specimen synced = specimen;
        try {
            setDefaultValues(specimen, batch.getConfig());
            Specimen convertedSpecimen = convertCustomProperties(specimen);
            synced =
                adapter.synchronize(convertedSpecimen, preloadedSpecimen, dependencies, institution, translationMap);
        // If Specimen can not be imported, increase the number of failures by 1
        } catch (Exception e) {
            StringBuffer message = new StringBuffer();
            message.append("An Exception occured while synchronizing Specimen with external id ");
            message.append(specimen.getExternalId());
            message.append(" and institution id ");
            message.append(batch.getConfig().getInstitutionId());
            NotificationHelper.getInstance().sendErrorNotification(message.toString(),
                    batch.getConfig().getNotificationEmail(), e);
            batch.incrementFailures();
            synced = null;
        }
        batch.incrementImports();
        return synced;
    }

    private void setDefaultValues(Specimen specimen, AbstractTissueBankIntegrationConfig config)
        throws IntegrationConfigurationException {
        for (DefaultImportValueConfig valueConfig : config.getDefaultImportValueConfigs()) {
            valueConfig.getValueType().setValue(specimen, valueConfig);
        }
    }

    private Specimen convertCustomProperties(Specimen specimen) throws DynamicExtensionsTypeConversionException {
       DynamicExtensionsServiceRemote deService =
           TissueLocatorIntegratorRegistry.getServiceLocator().getDynamicExtensionsService();

       return (Specimen) deService.convertObject(specimen);
    }
}
