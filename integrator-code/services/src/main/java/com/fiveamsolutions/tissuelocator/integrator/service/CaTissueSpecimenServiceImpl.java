/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jws.WebService;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Period;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Contact;
import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.ImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.SpecimenSynchronizationAdapter;
import com.fiveamsolutions.tissuelocator.integrator.util.NotificationHelper;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorHibernateUtil;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceRemote;
import com.fiveamsolutions.tissuelocator.service.ParticipantServiceRemote;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;

/**
 * @author ddasgupta
 */
@WebService(serviceName = "CaTissueService", portName = "CaTissuePort",
        endpointInterface = "com.fiveamsolutions.tissuelocator.integrator.service.CaTissueSpecimenService")
@SuppressWarnings({ "PMD.SignatureDeclareThrowsException", "PMD.TooManyMethods" })
public class CaTissueSpecimenServiceImpl implements CaTissueSpecimenService {

    private static final Logger LOG = Logger.getLogger(CaTissueSpecimenServiceImpl.class);

    private static final Map<SpecimenClass, QuantityUnits> UNITS_MAP;
    static {
        UNITS_MAP = new HashMap<SpecimenClass, QuantityUnits>();
        UNITS_MAP.put(SpecimenClass.CELLS, QuantityUnits.CELLS);
        UNITS_MAP.put(SpecimenClass.FLUID, QuantityUnits.ML);
        UNITS_MAP.put(SpecimenClass.MOLECULAR, QuantityUnits.UG);
        UNITS_MAP.put(SpecimenClass.TISSUE, QuantityUnits.MG);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("PMD.ExcessiveMethodLength")
    public ImportBatch processSpecimens(SpecimenList specimens, Long locationId) {
        try {
            TissueLocatorIntegratorHibernateUtil.getHibernateHelper().openAndBindSession();
            TissueBankIntegrationConfigServiceLocal configService =
                TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
            AbstractTissueBankIntegrationConfig config = configService.getByLocationId(locationId);
            ImportBatchServiceLocal service =
                TissueLocatorIntegratorRegistry.getServiceLocator().getImportBatchService();
            ImportBatch ib = null;
            if (!specimens.getSpecimens().isEmpty()) {
                ib = new ImportBatch();
                ib.setStartTime(new Date());
                ib.setConfig(config);
                SpecimenSynchronizationAdapter adapter = new SpecimenSynchronizationAdapter();
                Institution institution = TissueLocatorIntegratorRegistry.getServiceLocator()
                    .getInstitutionService().getInstitution(locationId);
                TranslationServiceLocal translationService =
                    TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
                Map<String, Map<String, Map<String, String>>> translationMap =
                    translationService.getTranslationMap(locationId);
                for (Specimen s : specimens.getSpecimens()) {
                    try {
                        LOG.debug(ToStringBuilder.reflectionToString(s));
                        CaTissueGridService gridService =
                            TissueLocatorIntegratorRegistry.getServiceLocator().getCaTissueGridService();
                        Date createdDate = specimenData(s, locationId, gridService);
                        specimenCollectionGroupData(s, locationId, gridService);
                        scgSiteData(s, locationId, gridService);
                        Date createdDate2 = collectionEventParametersData(s, locationId, gridService);
                        protocolData(s, locationId, gridService);
                        protocolSiteData(s, locationId, gridService);
                        protocolPiData(s, locationId, gridService);
                        Date birthDate = participantData(s, locationId, gridService, translationMap);
                        participantRaceData(s, locationId, gridService, translationMap);
                        participantIdData(s, locationId, gridService);
                        calculateCollectionAge(s, createdDate, createdDate2, birthDate);
                        s.setId(null);

                        SpecimenServiceRemote specService =
                            TissueLocatorIntegratorRegistry.getServiceLocator().getSpecimenService();
                        Specimen preloadedSpecimen = specService.getSpecimens(institution.getId(),
                                Collections.singleton(s.getExternalId())).get(s.getExternalId());
                        Map<String, PersistentObject> dependencies = new HashMap<String, PersistentObject>();
                        if (s.getParticipant() != null) {
                            String partExtId = s.getParticipant().getExternalId();
                            ParticipantServiceRemote partService =
                                TissueLocatorIntegratorRegistry.getServiceLocator().getParticipantService();
                            Participant preloadedParticipant = partService.getParticipants(institution.getId(),
                                    Collections.singleton(partExtId)).get(partExtId);
                            dependencies.put("participant", preloadedParticipant);
                        }
                        if (s.getProtocol() != null) {
                            String protName = s.getProtocol().getName();
                            CollectionProtocolServiceRemote protService =
                                TissueLocatorIntegratorRegistry.getServiceLocator().getCollectionProtocolService();
                            CollectionProtocol preloadedProtocol = protService.getProtocols(institution.getId(),
                                    Collections.singleton(protName)).get(protName);
                            dependencies.put("protocol", preloadedProtocol);
                        }

                        LOG.debug(ToStringBuilder.reflectionToString(s));
                        LOG.debug(ToStringBuilder.reflectionToString(s.getParticipant()));
                        LOG.debug(ToStringBuilder.reflectionToString(s.getProtocol()));
                        adapter.synchronize(s, preloadedSpecimen, dependencies, institution, translationMap);
                    } catch (Exception e) {
                        StringBuffer message = new StringBuffer();
                        message.append("An Exception occured while synchronizing caTissue Specimen with external id ");
                        message.append(s.getExternalId());
                        message.append(" and institution id ");
                        message.append(config.getInstitutionId());
                        NotificationHelper.getInstance().sendErrorNotification(message.toString(),
                                config.getNotificationEmail(), e);
                        ib.incrementFailures();
                    }
                    ib.incrementImports();
                }
                ib.setEndTime(new Date());
                service.savePersistentObject(ib);
            }
            service.checkSpecimenImportInterval(config);
            return ib;
        } finally {
            TissueLocatorIntegratorHibernateUtil.getHibernateHelper().unbindAndCleanupSession();
        }
    }

    private Date specimenData(Specimen s, Long locationId, CaTissueGridService gridService) throws Exception {
        edu.wustl.catissuecore.domain.Specimen caTissueSpec = gridService.getSpecimen(s.getId(),
                s.getSpecimenType().getSpecimenClass(), locationId);
        s.setStatus(SpecimenStatus.AVAILABLE);
        if (caTissueSpec != null) {
            LOG.trace(ToStringBuilder.reflectionToString(caTissueSpec));
            s.setExternalId(caTissueSpec.getBarcode());
            if (!"Active".equals(caTissueSpec.getActivityStatus())) {
                s.setStatus(SpecimenStatus.UNAVAILABLE);
            }
            s.setAvailableQuantity(new BigDecimal(caTissueSpec.getAvailableQuantity()));
            s.setAvailableQuantityUnits(UNITS_MAP.get(s.getSpecimenType().getSpecimenClass()));

            s.setSpecimenType(new SpecimenType());
            s.getSpecimenType().setName(caTissueSpec.getSpecimenType());

            if (caTissueSpec.getCreatedOn() != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(caTissueSpec.getCreatedOn());
                s.setCollectionYear(cal.get(Calendar.YEAR));
            }
            return caTissueSpec.getCreatedOn();
        }
        return null;
    }

    private void specimenCollectionGroupData(Specimen s, Long locationId, CaTissueGridService gridService)
        throws Exception {
        edu.wustl.catissuecore.domain.SpecimenCollectionGroup caTissueScg =
            gridService.getSpecimenCollectionGroup(s.getId(), locationId);
        if (caTissueScg != null) {
            LOG.trace(ToStringBuilder.reflectionToString(caTissueScg));
            s.setPathologicalCharacteristic(new AdditionalPathologicFinding());
            s.getPathologicalCharacteristic().setName(caTissueScg.getClinicalDiagnosis());
        }
    }

    private void scgSiteData(Specimen s, Long locationId, CaTissueGridService gridService) throws Exception {
        edu.wustl.catissuecore.domain.Site caTissueSite =
            gridService.getSpecimenCollectionGroupSite(s.getId(), locationId);
        if (caTissueSite != null) {
            LOG.trace(ToStringBuilder.reflectionToString(caTissueSite));
            s.setExternalIdAssigner(new Institution());
            s.getExternalIdAssigner().setId(locationId);
            s.getExternalIdAssigner().setName(caTissueSite.getName());
        }
    }

    private Date collectionEventParametersData(Specimen s, Long locationId, CaTissueGridService gridService)
        throws Exception {
        edu.wustl.catissuecore.domain.CollectionEventParameters caTissueCep =
            gridService.getCollectionEventParameters(s.getId(), locationId);
        if (caTissueCep != null) {
            LOG.trace(ToStringBuilder.reflectionToString(caTissueCep));
            if (caTissueCep.getTimestamp() != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(caTissueCep.getTimestamp());
                s.setCollectionYear(cal.get(Calendar.YEAR));
            }
            return caTissueCep.getTimestamp();
        }
        return null;
    }

    private void protocolData(Specimen s, Long locationId, CaTissueGridService gridService) throws Exception {
        edu.wustl.catissuecore.domain.CollectionProtocol caTissueProtocol =
            gridService.getCollectionProtocol(s.getId(), locationId);
        if (caTissueProtocol != null) {
            LOG.trace(ToStringBuilder.reflectionToString(caTissueProtocol));
            getProtocol(s).setName(caTissueProtocol.getTitle());
            getProtocol(s).setStartDate(caTissueProtocol.getStartDate());
            getProtocol(s).setEndDate(caTissueProtocol.getEndDate());
        }
    }

    private CollectionProtocol getProtocol(Specimen s) {
        if (s.getProtocol() == null) {
            s.setProtocol(new CollectionProtocol());
        }
        return s.getProtocol();
    }

    private void protocolSiteData(Specimen s, Long locationId, CaTissueGridService gridService) throws Exception {
        edu.wustl.catissuecore.domain.Site caTissueSite =
            gridService.getCollectionProtocolSite(s.getId(), locationId);
        if (caTissueSite != null) {
            LOG.trace(ToStringBuilder.reflectionToString(caTissueSite));
            if (getProtocol(s).getInstitution() == null) {
                getProtocol(s).setInstitution(new Institution());
            }
            getProtocol(s).getInstitution().setName(caTissueSite.getName());
        }
    }

    private void protocolPiData(Specimen s, Long locationId, CaTissueGridService gridService) throws Exception {
        edu.wustl.catissuecore.domain.User caTissueUser =
            gridService.getCollectionProtocolPI(s.getId(), locationId);
        if (caTissueUser != null) {
            LOG.trace(ToStringBuilder.reflectionToString(caTissueUser));
            if (getProtocol(s).getContact() == null) {
                getProtocol(s).setContact(new Contact());
            }
            getProtocol(s).getContact().setEmail(caTissueUser.getEmailAddress());
            getProtocol(s).getContact().setFirstName(caTissueUser.getFirstName());
            getProtocol(s).getContact().setLastName(caTissueUser.getLastName());
        }
    }

    private Date participantData(Specimen s, Long locationId, CaTissueGridService gridService,
            Map<String, Map<String, Map<String, String>>> translationMap) throws Exception {
        edu.wustl.catissuecore.domain.Participant caTissueParticipant =
            gridService.getParticipant(s.getId(), locationId);
        if (caTissueParticipant != null) {
            LOG.trace(ToStringBuilder.reflectionToString(caTissueParticipant));
            TranslationServiceLocal service =
                TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
            Gender g = service.getEnum(caTissueParticipant.getGender(), locationId, Gender.class, translationMap);
            getParticipant(s).setGender(g);
            Ethnicity e = service.getEnum(caTissueParticipant.getEthnicity(), locationId, Ethnicity.class,
                    translationMap);
            getParticipant(s).setEthnicity(e);
            return caTissueParticipant.getBirthDate();
        }
        return null;
    }

    private Participant getParticipant(Specimen s) {
        if (s.getParticipant() == null) {
            s.setParticipant(new Participant());
        }
        return s.getParticipant();
    }

    private void participantRaceData(Specimen s, Long locationId, CaTissueGridService gridService,
            Map<String, Map<String, Map<String, String>>> translationMap) throws Exception {
        Collection<edu.wustl.catissuecore.domain.Race> caTissueRaces =
            gridService.getParticipantRaces(s.getId(), locationId);
        for (edu.wustl.catissuecore.domain.Race caTissueRace : caTissueRaces) {
            LOG.trace(ToStringBuilder.reflectionToString(caTissueRace));
            if (getParticipant(s).getRaces() == null) {
                getParticipant(s).setRaces(new ArrayList<Race>());
            }
            TranslationServiceLocal service =
                TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
            Race race = service.getEnum(caTissueRace.getRaceName(), locationId, Race.class, translationMap);
            getParticipant(s).getRaces().add(race);
        }
    }

    private void participantIdData(Specimen s, Long locationId, CaTissueGridService gridService) throws Exception {
        edu.wustl.catissuecore.domain.ParticipantMedicalIdentifier caTissueMrn =
            gridService.getParticipantId(s.getId(), locationId);
        if (caTissueMrn != null) {
            LOG.trace(ToStringBuilder.reflectionToString(caTissueMrn));
            getParticipant(s).setExternalId(caTissueMrn.getMedicalRecordNumber());
        }
    }

    private void calculateCollectionAge(Specimen s, Date creationDate, Date alternateCreationDate, Date birthDate) {
        int patientAgeAtCollection = 0;
        TimeUnits patientAgeAtCollectionUnits = null;

        Period p = null;
        if (birthDate != null) {
            if (creationDate != null) {
                p = new Period(new DateTime(birthDate.getTime()), new DateTime(creationDate.getTime()));
            } else if (alternateCreationDate != null) {
                p = new Period(new DateTime(birthDate.getTime()), new DateTime(alternateCreationDate.getTime()));
            } else {
                p = new Period(new DateTime(birthDate.getTime()), new DateTime(birthDate.getTime()));
            }

            patientAgeAtCollection = p.getYears();
            patientAgeAtCollectionUnits = TimeUnits.YEARS;
            if (patientAgeAtCollection < 1) {
                patientAgeAtCollection = p.getMonths();
                patientAgeAtCollectionUnits = TimeUnits.MONTHS;
            }
        }

        s.setPatientAgeAtCollection(patientAgeAtCollection);
        s.setPatientAgeAtCollectionUnits(patientAgeAtCollectionUnits);
    }

}
