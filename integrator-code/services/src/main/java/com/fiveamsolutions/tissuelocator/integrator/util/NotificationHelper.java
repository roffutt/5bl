/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.validator.InvalidStateException;
import org.hibernate.validator.InvalidValue;
import org.joda.time.DateTime;
import org.joda.time.Period;

import com.fiveamsolutions.tissuelocator.data.code.Code;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.CsvParseException;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.CsvParseException.InvalidCsvValue;

/**
 * @author bpickeral
 *
 */
public class NotificationHelper {

    private static final Logger LOG = Logger.getLogger(NotificationHelper.class);
    private static final NotificationHelper INSTANCE = new NotificationHelper();
    private static final IntegratorEmailHelper EMAIL_HELPER = new IntegratorEmailHelper();


    /**
     * Sends an error notification with the message passed in.  Used to contact system admin for various types
     *  of errors.
     * @param message type of error that occurred
     * @param emailAddress admin address
     */
    public void sendErrorNotification(String message, String emailAddress) {
        String[] email = new String[1];
        email[0] = emailAddress;
        // Set days since last upload and ftp site
        String[] replacementValues = new String[1];
        replacementValues[0] = message;
        LOG.error("error occurred: " + message);
        EMAIL_HELPER.sendEmail("specimenSynchronization.email.error", email, replacementValues);
    }

    /**
     * Sends an error notification with the message passed in.  Used to contact system admin for various types
     *  of errors. Includes the stacktrace of the Exception.
     * @param message type of error that occurred
     * @param emailAddress admin address
     * @param e The Exception that occurred
     */
    public void sendErrorNotification(String message, String emailAddress, Exception e) {
        StringBuffer fullMessage = new StringBuffer(message);
        fullMessage.append(getErrorMessages(e));
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        fullMessage.append(": ");
        fullMessage.append(result.toString());
        sendErrorNotification(fullMessage.toString(), emailAddress);
    }

    private String getErrorMessages(Exception e) {
        if (CsvParseException.class.isAssignableFrom(e.getClass())) {
            return getCsvParseErrors((CsvParseException) e);
        }
        return getValidationErrors(e);
    }
    
    private String getCsvParseErrors(CsvParseException e) {
        StringBuffer errors = new StringBuffer();
        errors.append("\n CSV Parse Errors: Row ");
        errors.append(e.getRow());
        errors.append('\n');
        for (InvalidCsvValue iv : e.getInvalidCsvValues()) {
            errors.append("Column: ");
            errors.append(iv.getColumnHeader());
            errors.append("; Value: ");
            errors.append(iv.getInvalidValue());
            errors.append("; Exception: ");
            Writer result = new StringWriter();
            PrintWriter printWriter = new PrintWriter(result);
            iv.getException().printStackTrace(printWriter);
            errors.append(result.toString());
            errors.append('\n');
        }
        return errors.toString();
    }
    
    private String getValidationErrors(Exception e) {
        StringBuffer errors = new StringBuffer();
        Throwable t = e;
        InvalidStateException ise = null;
        while (t.getCause() != null) {
            t = t.getCause();
            if (t.getClass().isAssignableFrom(InvalidStateException.class)) {
                ise = (InvalidStateException) t;
            }
        }
        if (ise != null) {
            errors.append("\n Validation Errors: \n");
            for (InvalidValue iv : ise.getInvalidValues()) {
                errors.append(iv.getPropertyPath());
                errors.append(" = ");
                errors.append(iv.getValue());
                errors.append(": ");
                errors.append(iv.getMessage());
                errors.append("\n ");
            }
        }
        return errors.toString();
    }

    /**
     * Sends a notification to system admin listing number of days since last synchronization for flat-file import.
     * @param config FileImportIntegrationConfig object containing email, ftp site, and root directory
     * @param lastSynchronization date of last synchronization
     */
    public void sendSpecimenIntervalNotification(AbstractTissueBankIntegrationConfig config,
            Date lastSynchronization) {
        String[] email = new String[1];
        email[0] = config.getNotificationEmail();
        // Set days since last upload and ftp site
        String[] replacementValues = new String[2];
        replacementValues[0] = calculateDaysSinceLastSynchronization(lastSynchronization);
        replacementValues[1] = String.valueOf(config.getInstitutionId());
        EMAIL_HELPER.sendEmail("specimenSynchronization.email.intervalElapsed", email, replacementValues);
    }

    /**
     * Calculates the days since the last synchronization.
     * @param lastSynchronization date of last synchronization
     * @return days in String format
     */
    public String calculateDaysSinceLastSynchronization(Date lastSynchronization) {
        Period p = new Period(new DateTime(lastSynchronization.getTime()), new DateTime(new Date().getTime()));
        return String.valueOf(p.getDays());
    }

    /**
     * Get the instance of this class.
     * @return instance of NotificationHelper
     */
    public static NotificationHelper getInstance() {
        return INSTANCE;
    }

    /**
     * Send a notification of an unmapped code.
     * @param <T> the type of code
     * @param config the integration configuration
     * @param clientValue the client value
     * @param translatedValue the translated client value
     * @param codeType the code type
     */
    public <T extends Code> void sendUnmappedCodeNotification(AbstractTissueBankIntegrationConfig config,
            String clientValue, String translatedValue, Class<T> codeType) {
        sendUnmappedValueNotification(config, clientValue, translatedValue, codeType.getSimpleName());
    }

    /**
     * Send a notification of an unmapped enum value.
     * @param <T> the type of enum
     * @param config the integration configuration
     * @param clientValue the client value
     * @param translatedValue the translated client value
     * @param codeType the code type
     */
    @SuppressWarnings("rawtypes")
    public <T extends Enum> void sendUnmappedEnumNotification(AbstractTissueBankIntegrationConfig config,
            String clientValue, String translatedValue, Class<T> codeType) {
        sendUnmappedValueNotification(config, clientValue, translatedValue, codeType.getSimpleName());
    }

    private void sendUnmappedValueNotification(AbstractTissueBankIntegrationConfig config, String clientValue,
            String translatedValue, String codeType) {
        String[] email = new String[1];
        email[0] = config.getNotificationEmail();
        String[] replacementValues = {clientValue, translatedValue, codeType,
                String.valueOf(config.getInstitutionId()), StringUtils.join(config.getCodeSets(), ", ")};
        EMAIL_HELPER.sendEmail("unmappedCode.email", email, replacementValues);
    }
}
