/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service;

import java.util.Collection;

import com.fiveamsolutions.tissuelocator.data.SpecimenClass;

import edu.wustl.catissuecore.domain.CollectionEventParameters;
import edu.wustl.catissuecore.domain.CollectionProtocol;
import edu.wustl.catissuecore.domain.Participant;
import edu.wustl.catissuecore.domain.ParticipantMedicalIdentifier;
import edu.wustl.catissuecore.domain.Race;
import edu.wustl.catissuecore.domain.Site;
import edu.wustl.catissuecore.domain.Specimen;
import edu.wustl.catissuecore.domain.SpecimenCollectionGroup;
import edu.wustl.catissuecore.domain.User;

/**
 * @author ddasgupta
 */
@SuppressWarnings("PMD.SignatureDeclareThrowsException")
public interface CaTissueGridService {

    /**
     * Get a specimen from caTissue.
     * @param specimenId the id of the specimen in caTissue
     * @param specimenClass the class of the specimen
     * @param locationId the id of the institution running caTissue
     * @return a caTissue specimen
     * @throws Exception on error
     */
    Specimen getSpecimen(Long specimenId, SpecimenClass specimenClass, Long locationId) throws Exception;

    /**
     * Get the specimen collection group for a specimen from caTissue.
     * @param specimenId the id of the specimen in caTissue
     * @param locationId the id of the institution running caTissue
     * @return a caTissue specimen collection group
     * @throws Exception on error
     */
    SpecimenCollectionGroup getSpecimenCollectionGroup(Long specimenId, Long locationId) throws Exception;

    /**
     * Get the specimen collection group site for a specimen from caTissue.
     * @param specimenId the id of the specimen in caTissue
     * @param locationId the id of the institution running caTissue
     * @return a caTissue site
     * @throws Exception on error
     */
    Site getSpecimenCollectionGroupSite(Long specimenId, Long locationId) throws Exception;

    /**
     * Get the collection event parameters for a specimen from caTissue.
     * @param specimenId the id of the specimen in caTissue
     * @param locationId the id of the institution running caTissue
     * @return a caTissue collection event parameters object
     * @throws Exception on error
     */
    CollectionEventParameters getCollectionEventParameters(Long specimenId, Long locationId) throws Exception;

    /**
     * Get the collection protocol for a specimen from caTissue.
     * @param specimenId the id of the specimen in caTissue
     * @param locationId the id of the institution running caTissue
     * @return a caTissue collection protocol
     * @throws Exception on error
     */
    CollectionProtocol getCollectionProtocol(Long specimenId, Long locationId) throws Exception;

    /**
     * Get the collection protocol site for a specimen from caTissue.
     * @param specimenId the id of the specimen in caTissue
     * @param locationId the id of the institution running caTissue
     * @return a caTissue site
     * @throws Exception on error
     */
    Site getCollectionProtocolSite(Long specimenId, Long locationId) throws Exception;

    /**
     * Get the collection protocol principal investigator for a specimen from caTissue.
     * @param specimenId the id of the specimen in caTissue
     * @param locationId the id of the institution running caTissue
     * @return a caTissue user
     * @throws Exception on error
     */
    User getCollectionProtocolPI(Long specimenId, Long locationId) throws Exception;

    /**
     * Get the participant for a specimen from caTissue.
     * @param specimenId the id of the specimen in caTissue
     * @param locationId the id of the institution running caTissue
     * @return a caTissue participant
     * @throws Exception on error
     */
    Participant getParticipant(Long specimenId, Long locationId) throws Exception;

    /**
     * Get the participant identifier for a specimen from caTissue.
     * @param specimenId the id of the specimen in caTissue
     * @param locationId the id of the institution running caTissue
     * @return a caTissue participant medical identifiers
     * @throws Exception on error
     */
    ParticipantMedicalIdentifier getParticipantId(Long specimenId, Long locationId) throws Exception;

    /**
     * Get the races for a specimen from caTissue.
     * @param specimenId the id of the specimen in caTissue
     * @param locationId the id of the institution running caTissue
     * @return a collection of caTissue races
     * @throws Exception on error
     */
    Collection<Race> getParticipantRaces(Long specimenId, Long locationId) throws Exception;
}
