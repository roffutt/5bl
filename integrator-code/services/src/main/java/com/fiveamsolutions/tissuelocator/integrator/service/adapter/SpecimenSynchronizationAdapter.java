/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.math.BigDecimal;
import java.util.Map;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;
import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.validation.SpecimenPatientAgeValidator;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.util.CodeHelper;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;
import com.fiveamsolutions.tissuelocator.service.SpecimenServiceRemote;

/**
 * @author bpickeral
 *
 */
public class SpecimenSynchronizationAdapter implements SynchronizationAdapter<Specimen> {

    private final ParticipantSynchronizationAdapter participantSynchronizationAdapter =
        new ParticipantSynchronizationAdapter();
    private final CollectionProtocolSynchronizationAdapter collectionProtocolSynchronizationAdapter =
        new CollectionProtocolSynchronizationAdapter();
    private final CodeHelper codeHelper = new CodeHelper();

    /**
     * {@inheritDoc}
     */
    public Specimen synchronize(Specimen importedSpecimen, Specimen specimenToUpdate,
            Map<String, PersistentObject> preloadedDependencies, Institution institution,
            Map<String, Map<String, Map<String, String>>> translationMap) {
        if (importedSpecimen == null) {
            return null;
        }

        // Synchronize the Participant
        Participant preloadedParticipant = (Participant) preloadedDependencies.get("participant");
        importedSpecimen.setParticipant(participantSynchronizationAdapter.synchronize(
                importedSpecimen.getParticipant(), preloadedParticipant, null, institution, translationMap));

        // Synchronize the Protocol
        CollectionProtocol preloadedProtocol = (CollectionProtocol) preloadedDependencies.get("protocol");
        importedSpecimen.setProtocol(collectionProtocolSynchronizationAdapter.synchronize(
                importedSpecimen.getProtocol(), preloadedProtocol, null, institution, translationMap));

        codeHelper.setSystemCodes(importedSpecimen, institution.getId(), translationMap);

        setPrice(importedSpecimen);
        // cap the age at 90 to avoid hippa rules
        Integer maxAge = SpecimenPatientAgeValidator.getHippaMaxAge(importedSpecimen.getPatientAgeAtCollectionUnits());
        if (maxAge != null && importedSpecimen.getPatientAgeAtCollection() > maxAge.intValue()) {
            importedSpecimen.setPatientAgeAtCollection(maxAge);
        }

        TissueBankIntegrationConfigServiceLocal configService =
            TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
        AbstractTissueBankIntegrationConfig config = configService.getByLocationId(institution.getId());
        translateCustomProperties(importedSpecimen, translationMap, config);
        SpecimenServiceRemote specimenService = TissueLocatorIntegratorRegistry.getServiceLocator()
            .getSpecimenService();
        if (specimenToUpdate != null) {
            // copy data from file in to object returned from 5bl core
            updateSpecimen(specimenToUpdate, importedSpecimen);
            // send back the updated object for update
            return specimenService.importSpecimen(specimenToUpdate);
        } else {
            validateStatus(importedSpecimen.getStatus());
            importedSpecimen.setExternalIdAssigner(institution);
            return specimenService.importSpecimen(importedSpecimen);
        }
    }

    private void setPrice(Specimen importedSpecimen) {
        if (importedSpecimen.getMinimumPrice() == null && importedSpecimen.getMaximumPrice() == null) {
            importedSpecimen.setPriceNegotiable(true);
        } else {
            // trim excess 0's on price to avoid the failed validation for having too many
            // fractional digits in a price field.
            importedSpecimen.setMinimumPrice(stripPriceVal(importedSpecimen.getMinimumPrice()));
            importedSpecimen.setMaximumPrice(stripPriceVal(importedSpecimen.getMaximumPrice()));
        }
    }
    
    private BigDecimal stripPriceVal(BigDecimal val) {
        if (val != null) {
            if (BigDecimal.ZERO.equals(val)) {
                return BigDecimal.ZERO;
            } else {
                return val.stripTrailingZeros();
            }
        }
        return null;
    }

    /**
     * Validates the status of a newly imported Specimen that does not exist in the system.  A status of shipped is
     *  not allowed and is overwritten with no status.
     * @param importedStatus imported SpecimenStatus to validate
     */
    public void validateStatus(SpecimenStatus importedStatus) {
        if (!(SpecimenStatus.AVAILABLE.equals(importedStatus)
                || SpecimenStatus.UNAVAILABLE.equals(importedStatus)
                || SpecimenStatus.DESTROYED.equals(importedStatus))) {
            throw new IllegalArgumentException("A new Specimen can only have the status of AVAILABLE, "
                    + "UNAVAILABLE, or DESTROYED.");
        }
    }

    private void updateSpecimen(Specimen storedSpecimen, Specimen importedSpecimen) {
        storedSpecimen.setAvailableQuantity(importedSpecimen.getAvailableQuantity());
        storedSpecimen.setAvailableQuantityUnits(importedSpecimen.getAvailableQuantityUnits());
        storedSpecimen.setMaximumPrice(importedSpecimen.getMaximumPrice());
        storedSpecimen.setMinimumPrice(importedSpecimen.getMinimumPrice());
        storedSpecimen.setParticipant(importedSpecimen.getParticipant());
        storedSpecimen.setPathologicalCharacteristic(importedSpecimen.getPathologicalCharacteristic());
        storedSpecimen.setPatientAgeAtCollection(importedSpecimen.getPatientAgeAtCollection());
        storedSpecimen.setPatientAgeAtCollectionUnits(importedSpecimen.getPatientAgeAtCollectionUnits());
        storedSpecimen.setCollectionYear(importedSpecimen.getCollectionYear());
        storedSpecimen.setPreviousStatus(importedSpecimen.getPreviousStatus());
        storedSpecimen.setCreatedDate(importedSpecimen.getCreatedDate());
        storedSpecimen.setPriceNegotiable(importedSpecimen.isPriceNegotiable());
        storedSpecimen.setProtocol(importedSpecimen.getProtocol());
        storedSpecimen.setSpecimenType(importedSpecimen.getSpecimenType());
        storedSpecimen.setCustomProperties(importedSpecimen.getCustomProperties());
        updateSpecimenStatus(storedSpecimen, importedSpecimen.getStatus());
    }

    /**
     * Updates the Specimen status of the stored Specimen as long as the status was not shipped.
     * @param storedSpecimen Specimen to update
     * @param importedStatus the imported SpecimenStatus
     */
    public void updateSpecimenStatus(Specimen storedSpecimen, SpecimenStatus importedStatus) {
        // Throw an Exception and ultimately send an email when an illegal status change occurs.
        validateStatus(importedStatus);

        // if the current status is SHIPPED or RETURNED or DESTROYED - then the value provided in the
        // file is ignored and the stored status is unchanged.
        if (!(illegalShipmentChange(storedSpecimen.getStatus(), importedStatus)
                || SpecimenStatus.SHIPPED.equals(storedSpecimen.getStatus())
                || SpecimenStatus.RETURNED_TO_SOURCE_INSTITUTION.equals(storedSpecimen.getStatus())
                || SpecimenStatus.RETURNED_TO_PARTICIPANT.equals(storedSpecimen.getStatus())
                || SpecimenStatus.DESTROYED.equals(storedSpecimen.getStatus()))) {
            storedSpecimen.setStatus(importedStatus);
        }
    }

    /**
     * If the current status is UNDER_REVIEW or PENDING_SHIPMENT and the provided status is AVAILABLE,
     *  then the status remains as UNDER_REVIEW or PENDING_SHIPMENT.
     * @param storedStatus that status of the Specimen in the DB
     * @param importedStatus the imported SpecimenStatus
     * @return true if illegal, otherwise false
     */
    private boolean illegalShipmentChange(SpecimenStatus storedStatus, SpecimenStatus importedStatus) {
        return (SpecimenStatus.UNDER_REVIEW.equals(storedStatus)
                || SpecimenStatus.PENDING_SHIPMENT.equals(storedStatus))
                && SpecimenStatus.AVAILABLE.equals(importedStatus);
    }
    
    private void translateCustomProperties(Specimen specimen, Map<String, Map<String, Map<String, String>>> 
        translationMap, AbstractTissueBankIntegrationConfig config) {
        TranslationServiceLocal service = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        for (String customProperty : specimen.getCustomProperties().keySet()) {
            Object clientValue = specimen.getCustomProperty(customProperty);
            if (String.class.isAssignableFrom(clientValue.getClass())) {
                String translatedValue = service.getCustomProperty((String) clientValue, config, 
                        customProperty, translationMap);
                specimen.setCustomProperty(customProperty, translatedValue);
            }
        }
    }
}
