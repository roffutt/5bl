/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.data;

import java.lang.reflect.Field;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.fiveamsolutions.tissuelocator.data.Specimen;

/**
 * Enumeration of possible default import value types.
 * 
 * @author gvaughn
 *
 */
public enum DefaultImportValueType {

    /**
     * A nested object.
     */
    OBJECT {
        @Override
        public void setValue(Specimen specimen, DefaultImportValueConfig config) 
            throws IntegrationConfigurationException {
            
            try {
                Class<?> type = Class.forName(config.getPropertyClassName());
                String specimenPath = config.getPropertyPath().substring(0, config.getPropertyPath().indexOf('.'));
                String objectPath = config.getPropertyPath().substring(config.getPropertyPath().indexOf('.') + 1);
                
                if (PropertyUtils.getProperty(specimen, specimenPath) == null) {
                    Object o = type.newInstance();
                    PropertyUtils.setProperty(o, objectPath, config.getPropertyValue());
                    PropertyUtils.setProperty(specimen, specimenPath, o);
                } else if (PropertyUtils.getNestedProperty(specimen, config.getPropertyPath()) == null) {
                    PropertyUtils.setNestedProperty(specimen, config.getPropertyPath(), config.getPropertyValue());
                }
            } catch (Exception e) {
                throw new IntegrationConfigurationException("Exception setting default value.", e);
            }
            
        }
    },
    
    /**
     * An enum value.
     */
    ENUM {
        @Override
        @SuppressWarnings({ "unchecked", "rawtypes" })
        public void setValue(Specimen specimen, DefaultImportValueConfig config)
                throws IntegrationConfigurationException {
            try {
                if (PropertyUtils.getProperty(specimen, config.getPropertyPath()) == null) {
                    Field field = specimen.getClass().getDeclaredField(config.getPropertyPath()); 
                    PropertyUtils.setProperty(specimen, config.getPropertyPath(), 
                            Enum.valueOf((Class<Enum>) field.getType(), config.getPropertyValue()));
                } 
            } catch (Exception e) {
                throw new IntegrationConfigurationException("Exception setting default value.", e);
            }                      
        }
    },
    
    /**
     * A primitive value (including strings).
     */
    PRIMITIVE {
        @Override
        public void setValue(Specimen specimen, DefaultImportValueConfig config)
                throws IntegrationConfigurationException {
            try {
                if (isValueEmpty(PropertyUtils.getProperty(specimen, config.getPropertyPath()))) {
                    BeanUtils.setProperty(specimen, config.getPropertyPath(), config.getPropertyValue());
                }
            } catch (Exception e) {
                throw new IntegrationConfigurationException("Exception setting default value.", e);
            }                        
        }
        
        private boolean isValueEmpty(Object object) {
            return (object == null 
                    || object.equals(StringUtils.EMPTY)
                    || object.equals(NumberUtils.INTEGER_ZERO)
                    || object.equals(NumberUtils.LONG_ZERO));
        }
    };
    
    /**
     * Sets a specimen with a default value per the given configuration.
     * @param specimen Specimen to set.
     * @param config Default value configuration.
     * @throws IntegrationConfigurationException on error.
     */
    public abstract void setValue(Specimen specimen, DefaultImportValueConfig config) 
        throws IntegrationConfigurationException;
}
