/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import gov.nih.nci.cbm.domain.logicalmodel.CollectionProtocol;
import gov.nih.nci.cbm.domain.logicalmodel.SpecimenCollectionContact;

/**
 * @author smiller
 *
 */
public class CollectionProtocolDao extends AbstractCbmDao {
    
    private static final String SEARCH_BY_ID_SQL = "select collectionProtocolID from CollectionProtocol "
        + "where identifier = ?";

    private static final String CREATE_PROTOCOL = "insert into CollectionProtocol (collectionProtocolID, name, "
        + "startDate, endDate, identifier, is_assigned_to) values (?, ?, ?, ?, ?, ?)";

    private static final String CREATE_CP_TO_INST_JOIN = "insert into JoinCollectionProtocolToInstitution " 
        + "(institutionID, collectionProtocolID) values (?, ?)";
    
    private static final String CREATE_SPECIMEN_COLLECTION_CONTACT = "insert into SpecimenCollectionContact ("
        + "specimenCollectionContactID, phone) values (?, ?)";

    private static final String CREATE_PERSON = "insert into Person (personID, firstName, lastName, fullName, "
        + "emailAddress) values (?, ?, ?, ?, ?)";
    
    private static final String DELETE_ALL_JOIN = "delete from JoinCollectionProtocolToInstitution";

    private static final String DELETE_ALL_CP = "delete from CollectionProtocol";

    private static final String DELETE_ALL_CONTACT = "delete from SpecimenCollectionContact";

    private static final String DELETE_ALL_PERSON = "delete from Person";
    
    /**
     * Create the dao.
     * @param connection the connection to use.
     */
    public CollectionProtocolDao(Connection connection) {
        super(connection);
    }
    
    /**
     * delete all data saved.
     * @throws SQLException on error.
     */
    public void deleteAllProtocols() throws SQLException {
        executeUpdate(DELETE_ALL_JOIN, new Object[] {});
        executeUpdate(DELETE_ALL_CP, new Object[] {});
        executeUpdate(DELETE_ALL_CONTACT, new Object[] {});
        executeUpdate(DELETE_ALL_PERSON, new Object[] {});
        
        InstitutionDao institutionDao = new InstitutionDao(getConnection());
        institutionDao.deleteAllInstitutions();
    }
    
    /**
     * create or update the given protocol.
     * @param cp the collection protocol.
     * @return the id of the protocol
     * @throws SQLException on error.
     */
    public long createOrRetrieveProtocol(CollectionProtocol cp) throws SQLException {
        Long id = getProtocolByIdentifier(cp.getIdentifier());
        if (id == null) {
            id = createProtocol(cp);
        }
        return id;
    }
    
    @SuppressWarnings("rawtypes")
    private Long getProtocolByIdentifier(String identifier) throws SQLException {
        List results = executePreparedQuery(SEARCH_BY_ID_SQL, new String[] {identifier});
        if (!results.isEmpty()) {
            Integer id = (Integer) results.get(0);
            return Long.valueOf(id.longValue());
        }
        return null;
    }
    
    private Long createProtocol(CollectionProtocol cp) throws SQLException {
        InstitutionDao institutionDao = new InstitutionDao(getConnection());
        institutionDao.createOrRetrieveInstitution(cp.getInstitution());
        createContact(cp.getContact());
        
        executeUpdate(CREATE_PROTOCOL, new Object[] {cp.getId(), cp.getName(), cp.getStartDate(), cp.getEndDate(),
                cp.getIdentifier(), cp.getContact().getId()});
        
        executeUpdate(CREATE_CP_TO_INST_JOIN, new Object[] {cp.getInstitution().getId(), cp.getId()});
        return cp.getId();
    }
    
    private void createContact(SpecimenCollectionContact contact) throws SQLException {
        executeUpdate(CREATE_PERSON, new Object[] {contact.getId(), contact.getFirstName(), contact.getLastName(),
                contact.getFullName(), contact.getEmailAddress()});
        executeUpdate(CREATE_SPECIMEN_COLLECTION_CONTACT, new Object[] {contact.getId(), contact.getPhone()});
    }
}