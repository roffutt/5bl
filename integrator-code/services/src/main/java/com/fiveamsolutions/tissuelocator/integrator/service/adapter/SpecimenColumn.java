/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.QuantityUnits;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;

/**
 * Enumeration of CSV import file columns corresponding to specimen core fields, with logic
 * for setting specimen values with data retrieved from a CSV file, and for retrieving specimen
 * data for inclusion in a CSV file.
 * 
 * @author gvaughn
 *
 */
public enum SpecimenColumn {

    /**
     * External id.
     */
    EXERNAL_ID("specimenId") {       
        @Override
        public void setValue(Specimen specimen, String value) {
            specimen.setExternalId(value);
        }
    },
    
    /**
     * Available quantity.
     */
    AVAILABLE_QUANTITY("availableQuantity") {
        @Override
        public void setValue(Specimen specimen, String value) {
            if (!StringUtils.isEmpty(value)) {
                specimen.setAvailableQuantity(new BigDecimal(value));
            }
        }               
    },
    
    /**
     * Available quantity units.
     */
    AVAILABLE_QUANTITY_UNITS("availableQuantityUnits") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (!StringUtils.isEmpty(value)) {
                specimen.setAvailableQuantityUnits(QuantityUnits.valueOf(StringUtils.upperCase(value)));
            }            
        }       
    },
    
    /**
     * Pathological characteristic.
     */
    PATHOLOGICAL_CHARACTERISTIC("condition") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (specimen.getPathologicalCharacteristic() == null) {
                specimen.setPathologicalCharacteristic(new AdditionalPathologicFinding());
            }
            specimen.getPathologicalCharacteristic().setName(value);
        }        
    },
    
    /**
     * Collection year.
     */
    COLLECTION_YEAR("collectionYear") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (!StringUtils.isEmpty(value)) {
                specimen.setCollectionYear(Integer.parseInt(value));
            }            
        }       
    },
    
    /**
     * Patient age at collection.
     */
    PATIENT_AGE_AT_COLLECTION("ageAtCollection") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (!StringUtils.isEmpty(value)) {
                specimen.setPatientAgeAtCollection(Integer.parseInt(value));
            }          
        }       
    },
    
    /**
     * Patient age at collection units.
     */
    PATIENT_AGE_AT_COLLECTION_UNITS("ageAtCollectionUnits") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (!StringUtils.isEmpty(value)) {
                specimen.setPatientAgeAtCollectionUnits(TimeUnits.valueOf(StringUtils.upperCase(value)));
            }           
        }      
    },
    
    /**
     * Specimen type.
     */
    SPECIMEN_TYPE("specimenType") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (specimen.getSpecimenType() == null) {
                specimen.setSpecimenType(new SpecimenType());
            }
            specimen.getSpecimenType().setName(value);
        }
    },
    
    /**
     * Minimum price.
     */
    MINIMUM_PRICE("minimumPrice") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (!StringUtils.isEmpty(value)) {
                specimen.setMinimumPrice(new BigDecimal(value));
            }          
        }              
    },
    
    /**
     * Maximum price.
     */
    MAXIMUM_PRICE("maximumPrice") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (!StringUtils.isEmpty(value)) {
                specimen.setMaximumPrice(new BigDecimal(value));
            }           
        }       
    },
    
    /**
     * Specimen status.
     */
    STATUS("status") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (!StringUtils.isEmpty(value)) {
                specimen.setStatus(SpecimenStatus.valueOf(StringUtils.upperCase(value)));
            }          
        }       
    },
    
    /**
     * Participant external id.
     */
    PARTICIPANT_ID("patientIdentifier") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (specimen.getParticipant() == null) {
                specimen.setParticipant(new Participant());
            }
            specimen.getParticipant().setExternalId(value);
        }
    },
    
    /**
     * Participant gender.
     */
    GENDER("gender") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (specimen.getParticipant() == null) {
                specimen.setParticipant(new Participant());
            }
            if (!StringUtils.isEmpty(value)) {
                specimen.getParticipant().setGender(Gender.valueOf(StringUtils.upperCase(value)));
            }
        }       
    },
    
    /**
     * Participant ethnicity.
     */
    ETHNICITY("ethnicity") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (specimen.getParticipant() == null) {
                specimen.setParticipant(new Participant());
            }
            if (!StringUtils.isEmpty(value)) {
                specimen.getParticipant().setEthnicity(Ethnicity.valueOf(StringUtils.upperCase(value)));
            }
        }       
    },
    
    /**
     * Race - white.
     */
    WHITE("white") {

        @Override
        public void setValue(Specimen specimen, String value) {
            setRace(specimen, value, Race.WHITE);
        }       
    },
    
    /**
     * Race - black.
     */
    BLACK("black") {
        @Override
        public void setValue(Specimen specimen, String value) {
            setRace(specimen, value, Race.BLACK_OR_AFRICAN_AMERICAN);
        }       
    },
    
    /**
     * Race - asian.
     */
    ASIAN("asian") {
        @Override
        public void setValue(Specimen specimen, String value) {
            setRace(specimen, value, Race.ASIAN);
        }        
    },
    
    /**
     * Race - native american.
     */
    NATIVE_AMERICAN("nativeAmerican") {
        @Override
        public void setValue(Specimen specimen, String value) {
            setRace(specimen, value, Race.NATIVE_AMERICAN);
        }        
    },
    
    /**
     * Race - islander.
     */
    ISLANDER("islander") {
        @Override
        public void setValue(Specimen specimen, String value) {
            setRace(specimen, value, Race.ISLANDER);
        }       
    },
    
    /**
     * Race - not reported.
     */
    RACE_NOT_REPORTED("raceNotReported") {
        @Override
        public void setValue(Specimen specimen, String value) {
            setRace(specimen, value, Race.NOT_REPORTED);
        }       
    },
    
    /**
     * Race - unknown.
     */
    RACE_UNKNOWN("raceUnknown") {
        @Override
        public void setValue(Specimen specimen, String value) {
            setRace(specimen, value, Race.UNKNOWN);
        }
    },
    
    /**
     * Collection protocol.
     */
    COLLECTION_PROTOCOL("collectionProtocol") {

        @Override
        public void setValue(Specimen specimen, String value) {
            if (specimen.getProtocol() == null) {
                specimen.setProtocol(new CollectionProtocol());
            }
            specimen.getProtocol().setName(value);
        }       
    };
    
    
    private String columnHeader;
    
    SpecimenColumn(String columnHeader) {
        this.columnHeader = columnHeader;
    }
    
    /**
     * Set the specimen value associated with this column.
     * @param specimen Specimen to set.
     * @param value Value to set.
     */
    public abstract void setValue(Specimen specimen, String value);

    /**
     * @return The header associated with this column.
     */
    public String getColumnHeader() {
        return columnHeader;
    }
    
    private static void setRace(Specimen specimen, String value, Race race) {
        if (specimen.getParticipant() == null) {
            specimen.setParticipant(new Participant());
        }
        if (specimen.getParticipant().getRaces() == null) {
            specimen.getParticipant().setRaces(new ArrayList<Race>());
        }
        if (!StringUtils.isEmpty(value) && Boolean.parseBoolean(value)) {
            specimen.getParticipant().getRaces().add(race);
        }
    }
}
