/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.integrator.data.AbstractDataExportConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorHibernateUtil;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author smiller
 *
 */
public class AdapterLauncher {

    private static final AdapterLauncher INSTANCE = new AdapterLauncher();
    private static final Logger LOG = Logger.getLogger(AdapterLauncher.class);
    private static Timer integrationTimer = new Timer();
    private static int threads = 0;
    
    /**
     * Get instance of this class.
     * @return instance of AdapterLauncher
     */
    public static AdapterLauncher getInstance() {
        return INSTANCE;
    }

    /**
     * Method to launch the background threads.
     * @return the number of threads launched.
     */
    public synchronized int launchAdapterThreads() {
        
        try {
            // Manually managing session because this method is called from a background thread,
            // so the OSIV filter is not in effect
            TissueLocatorIntegratorHibernateUtil.getHibernateHelper().openAndBindSession();

            initializeIntegrationAdapters();
            
            initializeDataExportAdpaters();
        } finally {
            TissueLocatorIntegratorHibernateUtil.getHibernateHelper().unbindAndCleanupSession();
        }
        return threads;
    }

    private void initializeDataExportAdpaters() {
        LOG.debug("Initializing the export tasks.");
        List<AbstractDataExportConfig> configs = TissueLocatorIntegratorRegistry.getServiceLocator().
            getDataExportConfigService().getAll(AbstractDataExportConfig.class);
        
        for (AbstractDataExportConfig config : configs) {
            if (config.getAdapter() != null) {
                LOG.debug("Adding an exporter task of type: " + config.getClass());
                TimerTask task = new DataExportTimerTask(config);
                Calendar firstExecution = getFirstExecutionTime(config.getStartTime());
                schedule(config.getInterval(), task, firstExecution);
            }
        }
    }

    private void initializeIntegrationAdapters() {
        LOG.debug("Initializing the integration tasks.");
        List<AbstractTissueBankIntegrationConfig> configs = TissueLocatorIntegratorRegistry.getServiceLocator().
            getTissueBankIntegrationConfigService().getAll(AbstractTissueBankIntegrationConfig.class);

        for (AbstractTissueBankIntegrationConfig config : configs) {
            if (config.getAdapter() != null) {
                LOG.debug("Adding an integration task of type: " + config.getClass());
                TimerTask task = new TissueBankIntegrationTimerTask(config);
                Calendar firstExecution = getFirstExecutionTime(config.getStartTime());
                schedule(config.getInterval(), task, firstExecution);
            }
        }
    }

    private void schedule(long interval, TimerTask task, Calendar firstExecution) {
        integrationTimer.scheduleAtFixedRate(task, firstExecution.getTime(), interval);
        DateFormat df = new SimpleDateFormat("MMMMM dd, yyyy hh:mm:ss aaa", Locale.ENGLISH);
        LOG.debug("First execution time: " + df.format(firstExecution.getTime()));
        threads++;
    }

    private Calendar getFirstExecutionTime(Date configStartTime) {
        Calendar configTime = Calendar.getInstance();
        configTime.setTime(configStartTime);

        Calendar firstExecution = Calendar.getInstance();
        firstExecution.set(Calendar.HOUR_OF_DAY, configTime.get(Calendar.HOUR_OF_DAY));
        firstExecution.set(Calendar.MINUTE, configTime.get(Calendar.MINUTE));
        firstExecution.set(Calendar.SECOND, configTime.get(Calendar.SECOND));
        return firstExecution;
    }

    /**
     * stop the threads.
     */
    public synchronized void stopAdapterThreads() {
        if (threads > 0) {
            integrationTimer.cancel();
            integrationTimer.purge();
            // Once a timer is canceled, no new tasks may be scheduled, create a new timer
            integrationTimer = new Timer();
            LOG.debug("Stopping threads: " + threads);
            threads = 0;
        }
    }
}
