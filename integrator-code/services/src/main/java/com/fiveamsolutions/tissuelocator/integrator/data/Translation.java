/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;

import com.fiveamsolutions.nci.commons.data.persistent.PersistentObject;

/**
 * @author ddasgupta, smiller
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = { "source_value", "code_type", "code_set_name" }) })
public class Translation implements PersistentObject {

    private static final long serialVersionUID = 8155029964254693324L;
    private static final int LENGTH = 254;

    private Long id;
    private String sourceValue;
    private String destinationValue;
    private String codeType;
    private String codeSetName;

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the sourceValue
     */
    @Length(max = LENGTH)
    @Column(name = "source_value")
    public String getSourceValue() {
        return this.sourceValue;
    }

    /**
     * @param sourceValue the sourceValue to set
     */
    public void setSourceValue(String sourceValue) {
        this.sourceValue = sourceValue;
    }

    /**
     * @return the destinationValue
     */
    @Length(max = LENGTH)
    @Column(name = "destination_value")
    public String getDestinationValue() {
        return this.destinationValue;
    }

    /**
     * @param destinationValue the destinationValue to set
     */
    public void setDestinationValue(String destinationValue) {
        this.destinationValue = destinationValue;
    }

    /**
     * Note that the code type is assumed to be the simple name of the class into which the client
     * value is being translated.  For example, if this Translation object is indicating how a client
     * code is translated into an entry from the Gender enum, the code type property should be "Gender".
     * @return the codeType
     */
    @NotEmpty
    @Length(max = LENGTH)
    @Column(name = "code_type")
    public String getCodeType() {
        return this.codeType;
    }

    /**
     * @param codeType the codeType to set
     */
    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    /**
     * @return the codeSetName
     */
    @NotEmpty
    @Length(max = LENGTH)
    @Column(name = "code_set_name")
    public String getCodeSetName() {
        return this.codeSetName;
    }

    /**
     * @param codeSetName the codeSetName to set
     */
    public void setCodeSetName(String codeSetName) {
        this.codeSetName = codeSetName;
    }
}
