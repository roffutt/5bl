/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.service.adapter;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;

import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.ImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;
import com.fiveamsolutions.tissuelocator.integrator.util.NotificationHelper;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author smiller
 */
public class FileImportAdapter implements TissueBankAdapter {
    
    private static final Logger LOG = Logger.getLogger(FileImportAdapter.class);
    private JAXBContext context = null;
    private FtpFileImportHelper ftpHelper;
    private FileImportIntegrationConfig config;

    /**
     *  {@inheritDoc}
     *  Launch the file import adapter to download new files from the ftp site, unmarshal the
     *  files, and store the Specimen result in 5bl core.
     *  @param abstractConfig the FileImportIntegrationConfig containing information about connecting to the ftp
     *  @return List of type ImportBatch
     */
    public List<ImportBatch> launch(AbstractTissueBankIntegrationConfig abstractConfig) {
        LOG.debug("Looking for files and moving them to the processed directory if they exist.");
        config = (FileImportIntegrationConfig) abstractConfig;
        setFtpHelper(createHelper());
        ftpHelper.connect();
        List<FileImportBatch> batches = ftpHelper.createImportBatches();
        LOG.debug("Search complete, " + batches.size() + " batches found to import.");

        Long locId = abstractConfig.getInstitutionId();
        Institution institution = TissueLocatorIntegratorRegistry.getServiceLocator()
            .getInstitutionService().getInstitution(locId);
        Map<String, Map<String, Map<String, String>>> translationMap =
            TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService().getTranslationMap(locId);
        for (FileImportBatch currBatch : batches) {
            processBatch(currBatch, institution, translationMap);
        }

        ftpHelper.disconnect();
        return new ArrayList<ImportBatch>(batches);
    }

    /**
     * Create the import helper.
     * @return the helper.
     */
    public FtpFileImportHelper createHelper() {
        return new FtpFileImportHelper(config);
    }

    /**
     * @param ftpHelper the helper
     */
    public void setFtpHelper(FtpFileImportHelper ftpHelper) {
        this.ftpHelper = ftpHelper;
    }

    private void processBatch(FileImportBatch currBatch, Institution institution, 
            Map<String, Map<String, Map<String, String>>> translationMap) {
        try {
            currBatch.setConfig(config);
            importSpecimens(currBatch, institution, translationMap);
            // If file is not in correct format, store the batch as a failed import
        } catch (Exception e) {
            NotificationHelper.getInstance().sendErrorNotification(
                    "An Exception occured while reading the import file.",
                    currBatch.getConfig().getNotificationEmail(), e);
            currBatch.incrementFailures();
        } finally {
            currBatch.setEndTime(new Date());
        }
    }

    /**
     * Imports the xml and Saves/Updates the Specimen and related objects into the 5BL database.
     * @param batch ImportBatch object to store number of successful imports and number of failed imports
     * @param institution the institution the import is being done for
     * @param translationMap the map of code translations for the institution
     * @throws FileParseException On file parse error.
     */
    public void importSpecimens(FileImportBatch batch, Institution institution,
            Map<String, Map<String, Map<String, String>>> translationMap)
            throws FileParseException {
        LOG.debug("Starting import of a batch.");
        
        FileImportIntegrationConfig curConfig = (FileImportIntegrationConfig) batch.getConfig();
        ImportFileConverter converter = curConfig.getImportFileConverter();
        SpecimenProcessor specimenProcessor = new SpecimenProcessor(curConfig.getNumberOfProcessingThreads(), 
                batch, institution, translationMap);

        converter.init(batch, ftpHelper, translationMap);
        Specimen specimen = getNextSpecimen(converter, batch);
        while (specimen != null) {
            specimenProcessor.addSpecimenToQueue(specimen);
            specimen = getNextSpecimen(converter, batch);
        } 
        specimenProcessor.waitForCompletion();
        LOG.debug("Importing complete. " + batch.getImports() + " imported.");
    }
    
    private Specimen getNextSpecimen(ImportFileConverter converter, FileImportBatch batch) 
        throws FileParseException {
        Specimen specimen = null;
        boolean specimenReady = false;
        while (!specimenReady) {
            try {
                specimen = converter.getNext();
                specimenReady = true;
            } catch (CsvParseException e) {
                NotificationHelper.getInstance().sendErrorNotification(
                        "An exception occured while parsing a CSV file.", batch.getConfig().getNotificationEmail(), e);
                batch.incrementFailures();
            }
        }
        
        return specimen;
    }

    /**
     * Marshal Specimen into xml.
     * @param specimenList SpecimenList object
     * @throws JAXBException if error occurs during marshal
     * @return String xml
     */
    public String marshal(SpecimenList specimenList) throws JAXBException {
        Marshaller m = getInstance().createMarshaller();
        StringWriter sw = new StringWriter();
        m.marshal(specimenList, sw);
        return sw.toString();
    }

    private JAXBContext getInstance() throws JAXBException {
        if (context == null) {
            context = JAXBContext.newInstance(SpecimenList.class);
        }
        return context;
    }
}
