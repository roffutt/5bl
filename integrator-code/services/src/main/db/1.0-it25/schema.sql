create table file_import_integration_config (ftpSite varchar(254) not null, config_id int8 not null, primary key (config_id));
create table integration_config (id int8 not null, institutionId int8 not null unique, interval int8 not null, startTime time not null, primary key (id));
alter table file_import_integration_config add constraint FK27755DE4F233ADC2 foreign key (config_id) references integration_config;
