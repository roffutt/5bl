create table default_import_value_config (
    id int8 not null,
    property_path varchar(255) not null,
    property_class_name varchar(255),
    property_value varchar(255) not null,
    value_type varchar(255) not null,
    primary key (id)
);
create table default_import_value_configs (
    integration_config_id int8 not null,
    default_import_value_config_id int8 not null,
    primary key (integration_config_id, default_import_value_config_id)
);
alter table default_import_value_configs add constraint CONFIG_VALUE_FK foreign key (integration_config_id) references integration_config;
alter table default_import_value_configs add constraint VALUE_CONFIG_FK foreign key (default_import_value_config_id) references default_import_value_config;