alter table file_import_integration_config add column num_processing_threads int8;
update file_import_integration_config set num_processing_threads = 1;
alter table file_import_integration_config alter column num_processing_threads set not null;