create table translation (
    id int8 not null,
    client_value varchar(255),
    local_value varchar(255),
    code_set_name varchar(255) not null,
    code_type int4 not null,
    primary key (id),
    unique (client_value, code_type, code_set_name)
);
create table config_code_set (
    config_id int8 not null,
    code_set_name varchar(255) not null,
    index int4 not null,
    primary key (config_id, index)
);
alter table config_code_set add constraint FK6921136DF233ADC2 foreign key (config_id) references integration_config;
