create table catissue_integration_config (gridServiceUrl varchar(254) not null, config_id int8 not null, primary key (config_id));
alter table catissue_integration_config add constraint FK33CDC709F233ADC2 foreign key (config_id) references integration_config;
