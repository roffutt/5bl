alter table file_import_integration_config add column file_converter_class varchar(255);
update file_import_integration_config set file_converter_class = 'com.fiveamsolutions.tissuelocator.integrator.service.adapter.XmlImportFileConverter';
alter table file_import_integration_config alter column file_converter_class set not null;
alter table file_import_integration_config add column delimiter varchar(255);