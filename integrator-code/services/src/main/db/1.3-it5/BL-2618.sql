alter table file_import_batch drop column lob;
alter table file_import_batch add column file_name varchar(254) not null;