alter table translation drop column code_type;
alter table translation add column code_type varchar(254) not null;

insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Hispanic or Latino', 'HISPANIC_OR_LATINO', 'caTissue Base', 'Ethnicity');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Not Hispanic or Latino', 'NON_HISPANIC_OR_LATINO', 'caTissue Base', 'Ethnicity');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Not Reported', 'NOT_REPORTED', 'caTissue Base', 'Ethnicity');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Unknown', 'UNKNOWN', 'caTissue Base', 'Ethnicity');

insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'American Indian or Alaska Native', 'NATIVE_AMERICAN', 'caTissue Base', 'Race');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Asian', 'ASIAN', 'caTissue Base', 'Race');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Black or African American', 'BLACK_OR_AFRICAN_AMERICAN', 'caTissue Base', 'Race');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Native Hawaiian or Other Pacific Islander', 'ISLANDER', 'caTissue Base', 'Race');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Not Reported', 'NOT_REPORTED', 'caTissue Base', 'Race');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Unknown', 'UNKNOWN', 'caTissue Base', 'Race');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'White', 'WHITE', 'caTissue Base', 'Race');

insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Female Gender', 'FEMALE', 'caTissue Base', 'Gender');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Male Gender', 'MALE', 'caTissue Base', 'Gender');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Unknown', 'UNKNOWN', 'caTissue Base', 'Gender');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Unspecified', 'UNSPECIFIED', 'caTissue Base', 'Gender');

insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Indwelling Catheter', 'SURGICALLY_RESECTED', 'caTissue Base', 'CollectionType');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Needle Aspirate', 'SURGICALLY_RESECTED', 'caTissue Base', 'CollectionType');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Needle Core Biopsy', 'SURGICALLY_RESECTED', 'caTissue Base', 'CollectionType');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Not Specified', null, 'caTissue Base', 'CollectionType');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Surgical Resection', 'SURGICALLY_RESECTED', 'caTissue Base', 'CollectionType');
insert into translation (id, client_value, local_value, code_set_name, code_type) values (nextval('hibernate_sequence'), 'Venipuncture', 'SURGICALLY_RESECTED', 'caTissue Base', 'CollectionType');

