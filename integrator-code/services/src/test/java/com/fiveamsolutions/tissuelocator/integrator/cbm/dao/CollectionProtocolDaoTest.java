/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.dao;

import static org.junit.Assert.*;
import gov.nih.nci.cbm.domain.logicalmodel.CollectionProtocol;
import gov.nih.nci.cbm.domain.logicalmodel.SpecimenCollectionContact;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

/**
 * @author smiller
 *
 */
public class CollectionProtocolDaoTest extends AbstractCbmDaoTest {
    
    private static final int SIXTH_PARAM = 6;
    private static final int FIFTH_PARAM = 5;
    private static final int FOURTH_PARAM = 4;
    private static final int THIRD_PARAM = 3;

    /**
     * test creating and deleting.
     * @throws Exception on error.
     */
    @Test
    public void testCreateAndDelete() throws Exception {
        Connection con = getService().getConnection();
        Statement s = con.createStatement();

        CollectionProtocolDao dao = new CollectionProtocolDao(con);
        dao.deleteAllProtocols();
        InstitutionDaoTest.verifyDeletion(s);
        
        CollectionProtocol cp = CollectionProtocolDaoTest.getTestCollectionProtocol();
        dao.createOrRetrieveProtocol(cp);
        CollectionProtocolDaoTest.verifyCollectionProtocol(s, cp);
        
        dao.createOrRetrieveProtocol(cp);
        CollectionProtocolDaoTest.verifyCollectionProtocol(s, cp);
        
        dao.deleteAllProtocols();
        InstitutionDaoTest.verifyDeletion(s);
        s.close();
    }
    
    /**
     * verify.
     * @param s statement
     * @param cp protocol
     * @throws SQLException on error.
     */
    public static void verifyCollectionProtocol(Statement s, CollectionProtocol cp) throws SQLException {
        SpecimenCollectionContact c = cp.getContact();
        
        InstitutionDaoTest.verifyInstitution(s, cp.getInstitution());
        InstitutionDaoTest.verifyOrg(s, cp.getInstitution());
        
        ResultSet rs = s.executeQuery("select specimenCollectionContactID, phone from SpecimenCollectionContact");
        assertTrue(rs.next());
        assertEquals(cp.getId(), Long.valueOf(rs.getLong(1)));
        assertEquals(c.getPhone(), rs.getString(2));
        assertFalse(rs.next());
        rs.close();
        
        rs = s.executeQuery("select personID, firstName, lastName, fullName, emailAddress from Person");
        assertTrue(rs.next());
        assertEquals(cp.getId(), Long.valueOf(rs.getLong(1)));
        assertEquals(c.getFirstName(), rs.getString(2));
        assertEquals(c.getLastName(), rs.getString(THIRD_PARAM));
        assertEquals(c.getFullName(), rs.getString(FOURTH_PARAM));
        assertEquals(c.getEmailAddress(), rs.getString(FIFTH_PARAM));
        assertFalse(rs.next());
        rs.close();
        
        rs = s.executeQuery("select institutionID, collectionProtocolID from JoinCollectionProtocolToInstitution");
        assertTrue(rs.next());
        assertEquals(cp.getInstitution().getId(), Long.valueOf(rs.getLong(1)));
        assertEquals(cp.getId(), Long.valueOf(rs.getLong(2)));
        assertFalse(rs.next());
        rs.close();
        
        rs = s.executeQuery("select collectionProtocolID, name, startDate, endDate, identifier, is_assigned_to from "
                + "CollectionProtocol");
        assertTrue(rs.next());
        assertEquals(cp.getId(), Long.valueOf(rs.getLong(1)));
        assertEquals(cp.getName(), rs.getString(2));
        assertEquals(cp.getStartDate(), new Date(rs.getDate(THIRD_PARAM).getTime()));
        assertEquals(cp.getEndDate(), new Date(rs.getDate(FOURTH_PARAM).getTime()));
        assertEquals(cp.getIdentifier(), rs.getString(FIFTH_PARAM));
        assertEquals(cp.getId(), Long.valueOf(rs.getLong(SIXTH_PARAM)));
        assertFalse(rs.next());
        rs.close();
    }

    
    
    /**
     * verify.
     * @param s statement
     * @throws SQLException on error.
     */
    public static void verifyDeletion(Statement s) throws SQLException {
        InstitutionDaoTest.verifyDeletion(s);
        
        ResultSet rs = s.executeQuery("select count(*) from SpecimenCollectionContact, Person, "
                + "JoinCollectionProtocolToInstitution, CollectionProtocol");
        rs.next();
        assertEquals(0, rs.getInt(1));
    }
    
    /**
     * get a test protocol.
     * @return the protocol.
     */
    public static CollectionProtocol getTestCollectionProtocol() {
        CollectionProtocol cp = new CollectionProtocol();
        cp.setId(2L);
        cp.setIdentifier("2");
        cp.setEndDate(DateUtils.truncate(Calendar.getInstance(), Calendar.DATE).getTime());
        cp.setStartDate(DateUtils.truncate(Calendar.getInstance(), Calendar.DATE).getTime());
        cp.setInstitution(InstitutionDaoTest.getTestInstitution());
        cp.setContact(new SpecimenCollectionContact());
        cp.getContact().setEmailAddress("testEmail@example.com");
        cp.getContact().setFirstName("fname");
        cp.getContact().setLastName("lname");
        cp.getContact().setFullName("full name");
        cp.getContact().setPhone("phone");
        cp.getContact().setId(2L);
        cp.setName("testName");
        return cp;
    }
}