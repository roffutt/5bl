/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.cbm.dao;

import static org.junit.Assert.*;
import gov.nih.nci.cbm.domain.logicalmodel.Preservation;
import gov.nih.nci.cbm.domain.logicalmodel.SpecimenCollectionSummary;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

/**
 * @author smiller
 *
 */
public class SpecimenCollectionSummaryDaoTest extends AbstractCbmDaoTest {

    /**
     * 
     */
    private static final int STORAGE_TEMPERATURE_IN_CENTEGRADES = -20;
    private static final int EIGTH_PARAM = 8;
    private static final int SEVENTH_PARAM = 7;
    private static final int SIXTH_PARAM = 6;
    private static final int FIFTH_PARAM = 5;
    private static final int FOURTH_PARAM = 4;
    private static final int THIRD_PARAM = 3;
    private static final long SPECIMEN_ID = 4L;

    /**
     * test creating and deleting.
     * @throws Exception on error.
     */
    @Test
    public void testCreateAndDelete() throws Exception {
        Connection con = getService().getConnection();
        Statement s = con.createStatement();

        SpecimenCollectionSummaryDao dao = new SpecimenCollectionSummaryDao(con);
        dao.deleteAllSpecimens();
        SpecimenCollectionSummaryDaoTest.verifyDeletion(s);
        
        SpecimenCollectionSummary specimen = SpecimenCollectionSummaryDaoTest.getTestSpecimen();
        dao.createOrIncrementSpecimen(specimen);
        SpecimenCollectionSummaryDaoTest.verifySpecimen(s, specimen, 1);
        
        dao.createOrIncrementSpecimen(specimen);
        SpecimenCollectionSummaryDaoTest.verifySpecimen(s, specimen, 2);
        
        dao.deleteAllSpecimens();
        SpecimenCollectionSummaryDaoTest.verifyDeletion(s);
        s.close();
    }
    
    /**
     * verify.
     * @param s statement
     * @param specimen participant
     * @param count the count
     * @throws SQLException on error.
     */
    public static void verifySpecimen(Statement s, SpecimenCollectionSummary specimen, int count) throws SQLException {
        ParticipantCollectionSummaryDaoTest.verifyParticipant(s, specimen.getParticipantCollectionSummary(), count);
        
        ResultSet rs = s.executeQuery("select SpecimenCollectionSummary.specimenCollectionSummaryID, " 
                + "SpecimenCollectionSummary.anatomicSource, SpecimenCollectionSummary.patientAgeAtCollection, "
                + "SpecimenCollectionSummary.specimenType, SpecimenCollectionSummary.count, "
                + "SpecimenCollectionSummary.is_collected_from,  Preservation.storageTemperatureInCentegrades, "
                + "Preservation.preservationType "
                + "from SpecimenCollectionSummary, Preservation where "
                + "Preservation.preservationID = SpecimenCollectionSummary.undergoes");
        assertTrue(rs.next());
        assertEquals(specimen.getId(), Long.valueOf(rs.getLong(1)));
        assertEquals(specimen.getAnatomicSource(), rs.getString(2));
        assertEquals(specimen.getPatientAgeAtCollection(), rs.getString(THIRD_PARAM));
        assertEquals(specimen.getSpecimenType(), rs.getString(FOURTH_PARAM));
        assertEquals(count, rs.getInt(FIFTH_PARAM));
        assertEquals(Long.valueOf(1), Long.valueOf(rs.getLong(SIXTH_PARAM)));
        assertEquals(specimen.getUndergoes().getStorageTemperatureInCentegrades().intValue(), rs.getInt(SEVENTH_PARAM));
        assertEquals(specimen.getUndergoes().getPreservationType(), rs.getString(EIGTH_PARAM));
        assertFalse(rs.next());
        rs.close();
    }

    /**
     * verify.
     * @param s statement
     * @throws SQLException on error.
     */
    public static void verifyDeletion(Statement s) throws SQLException {
        ParticipantCollectionSummaryDaoTest.verifyDeletion(s);
        
        ResultSet rs = s.executeQuery("select count(*) from SpecimenCollectionSummary, "
                + "Preservation");
        rs.next();
        assertEquals(0, rs.getInt(1));
    }
    
    /**
     * @return the test specimen.
     */
    public static SpecimenCollectionSummary getTestSpecimen() {
        SpecimenCollectionSummary specimen = new SpecimenCollectionSummary();
        specimen.setAnatomicSource("test src");
        specimen.setId(SPECIMEN_ID);
        specimen.setParticipantCollectionSummary(ParticipantCollectionSummaryDaoTest.getTestParticipant());
        specimen.setPatientAgeAtCollection("21");
        specimen.setSpecimenType("test type");
        specimen.setUndergoes(new Preservation());
        specimen.getUndergoes().setPreservationType("test pres type");
        specimen.getUndergoes().setStorageTemperatureInCentegrades(STORAGE_TEMPERATURE_IN_CENTEGRADES);
        return specimen;
    }
}
