/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.services.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.SpecimenColumn;

/**
 * @author gvaughn
 *
 */
public class SpecimenColumnTest {

    /**
     * Test specimen column setValue method.
     */
    @Test
    public void testGetSetValues() {
        
        // test population
        Specimen specimen = SpecimenHelper.INSTANCE.createSpecimen();
        for (SpecimenColumn column : SpecimenColumn.values()) {
            column.setValue(specimen, SpecimenColumnHelper.getValue(specimen, column));
        }
        SpecimenValidator.INSTANCE.checkCsvSpecimen(specimen, false);
        
        // test for exception cases
        specimen = new Specimen();
        specimen.setPathologicalCharacteristic(new AdditionalPathologicFinding());
        specimen.setSpecimenType(new SpecimenType());
        specimen.setParticipant(new Participant());
        specimen.setProtocol(new CollectionProtocol());
        for (SpecimenColumn column : SpecimenColumn.values()) {
            column.setValue(specimen, SpecimenColumnHelper.getValue(specimen, column));
        }
        
        specimen = new Specimen();
        for (SpecimenColumn column : SpecimenColumn.values()) {
            column.setValue(specimen, SpecimenColumnHelper.getValue(specimen, column));
        }
        
        specimen = new Specimen();
        for (SpecimenColumn column : SpecimenColumn.values()) {
            column.setValue(specimen, null);
        }
        
        SpecimenColumn.GENDER.setValue(new Specimen(), Gender.FEMALE.name());
        SpecimenColumn.ETHNICITY.setValue(new Specimen(), Ethnicity.UNKNOWN.name());
        
        Map<Race, SpecimenColumn> raceMap = new HashMap<Race, SpecimenColumn>();
        raceMap.put(Race.WHITE, SpecimenColumn.WHITE);
        raceMap.put(Race.BLACK_OR_AFRICAN_AMERICAN, SpecimenColumn.BLACK);
        raceMap.put(Race.ASIAN, SpecimenColumn.ASIAN);
        raceMap.put(Race.NATIVE_AMERICAN, SpecimenColumn.NATIVE_AMERICAN);
        raceMap.put(Race.ISLANDER, SpecimenColumn.ISLANDER);
        raceMap.put(Race.NOT_REPORTED, SpecimenColumn.RACE_NOT_REPORTED);
        raceMap.put(Race.UNKNOWN, SpecimenColumn.RACE_UNKNOWN);
        Specimen accumulate = new Specimen();
        int raceCount = 0;
        for (Race race : Race.values()) {
            raceMap.get(race).setValue(accumulate, Boolean.toString(false));
            assertEquals(raceCount, accumulate.getParticipant().getRaces().size());
            assertFalse(accumulate.getParticipant().getRaces().contains(race));
            
            raceMap.get(race).setValue(accumulate, Boolean.toString(true));
            assertEquals(++raceCount, accumulate.getParticipant().getRaces().size());
            assertTrue(accumulate.getParticipant().getRaces().contains(race));
            
            specimen = new Specimen();
            raceMap.get(race).setValue(specimen, Boolean.toString(true));
            assertEquals(1, specimen.getParticipant().getRaces().size());
            assertTrue(specimen.getParticipant().getRaces().contains(race));
        }
    }
}
