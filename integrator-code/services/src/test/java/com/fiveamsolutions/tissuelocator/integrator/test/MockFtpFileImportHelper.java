/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.fiveamsolutions.tissuelocator.integrator.test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.fiveamsolutions.tissuelocator.integrator.data.FileImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.FileImportAdapter;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.FtpFileImportHelper;
import com.fiveamsolutions.tissuelocator.integrator.services.adapter.SpecimenHelper;

/**
 * @author smiller
 *
 */
public class MockFtpFileImportHelper extends FtpFileImportHelper {
    private static final long ERROR_ID = 1234L;
    private final FileImportIntegrationConfig theConfig;
    private final FileImportAdapter adapter;

    /**
     * build the helper.
     * @param config the config
     * @param adapter the adapter.
     */
    public MockFtpFileImportHelper(FileImportIntegrationConfig config, FileImportAdapter adapter) {
        super(config);
        theConfig = config;
        this.adapter = adapter;
    }

    /**
     * 
     * @return the adapter.
     */
    public FileImportAdapter getAdapter() {
        return adapter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean connect() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FileImportBatch> createImportBatches() {
        List<FileImportBatch> batches = new ArrayList<FileImportBatch>();
        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(theConfig);
        fileBatch.setFileName("fakefile.xml");
        batches.add(fileBatch);
        return batches;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream downloadFile(FileImportBatch batch) {
        if (batch.getConfig().getInstitutionId() != ERROR_ID) {
            SpecimenList specimenList = new SpecimenList();
            specimenList.getSpecimens().add(SpecimenHelper.INSTANCE.createSpecimen());
            try {
                String output = adapter.marshal(specimenList);
                return new ByteArrayInputStream(output.getBytes());
            } catch (JAXBException jax) {
                // return error stream, not expected for tests
            }
        }
        return new ByteArrayInputStream("invalid.xml".getBytes());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void disconnect() {
       // no op
    }
}