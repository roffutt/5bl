/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fiveamsolutions.dynamicextensions.AbstractDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.IntegerDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.StringDynamicFieldDefinition;
import com.fiveamsolutions.dynamicextensions.struts2.converter.DynamicExtensionsTypeConversionException;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.service.extension.DynamicExtensionsServiceRemote;

/**
 * Mock implementation of the DynamicExtensionsServiceRemote interface.
 *
 * @author jstephens
 */
public class MockDynamicExtensionsServiceRemote implements DynamicExtensionsServiceRemote {

    /**
     * This method simply returns the object that was passed in as the parameter in order to support
     * file import and marshaling functionality.  Testing for dynamic extensions type conversion has
     * been covered in other projects.
     * {@inheritDoc}
     */
    public Object convertObject(Object object) throws DynamicExtensionsTypeConversionException {
        return object;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<AbstractDynamicFieldDefinition> getDynamicFieldDefinitions(String clazz) {
        List<AbstractDynamicFieldDefinition> fieldDefs = new ArrayList<AbstractDynamicFieldDefinition>();
        fieldDefs.add(createStringExtension("collectionType"));
        fieldDefs.add(createStringExtension("anatomicSource"));
        fieldDefs.add(createStringExtension("preservationType"));
        fieldDefs.add(createIntegerExtension("timeLapseToProcessing"));
        fieldDefs.add(createIntegerExtension("specimenDensity"));
        fieldDefs.add(createIntegerExtension("storageTemperature"));
        return fieldDefs;
    }

    private StringDynamicFieldDefinition createStringExtension(String name) {
        StringDynamicFieldDefinition fd = new StringDynamicFieldDefinition();
        fd.setEntityClassName(Specimen.class.getName());
        fd.setFieldName(name);
        fd.setFieldDisplayName(name);
        fd.setMaxLength(2);
        fd.setNullable(true);
        fd.setSearchable(false);
        return fd;
    }

    private IntegerDynamicFieldDefinition createIntegerExtension(String name) {
        IntegerDynamicFieldDefinition ifd = new IntegerDynamicFieldDefinition();
        ifd.setEntityClassName(Specimen.class.getName());
        ifd.setFieldName(name);
        ifd.setFieldDisplayName(name);
        ifd.setSearchable(false);
        ifd.setNullable(true);
        ifd.setIntegerDigits(2);
        ifd.setMaxValue(new BigDecimal(2));
        ifd.setMinValue(new BigDecimal(0));
        return ifd;
    }
}
