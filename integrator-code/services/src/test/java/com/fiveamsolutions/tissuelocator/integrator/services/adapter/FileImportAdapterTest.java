/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.services.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.filters.StringInputStream;
import org.junit.Before;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import com.csvreader.CsvWriter;
import com.fiveamsolutions.nci.commons.util.MailUtils;
import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.SpecimenStatus;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;
import com.fiveamsolutions.tissuelocator.integrator.data.AbstractTissueBankIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.DefaultImportValueConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig;
import com.fiveamsolutions.tissuelocator.integrator.data.FileImportIntegrationConfig.Delimiter;
import com.fiveamsolutions.tissuelocator.integrator.data.ImportBatch;
import com.fiveamsolutions.tissuelocator.integrator.data.jaxb.SpecimenList;
import com.fiveamsolutions.tissuelocator.integrator.service.TissueBankIntegrationConfigServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.TranslationServiceLocal;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.CsvImportFileConverter;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.CsvParseException;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.CsvParseException.InvalidCsvValue;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.FileImportAdapter;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.FileParseException;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.FtpFileImportHelper;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.ImportFileConverter;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.InvalidCsvHeaderException;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.SpecimenColumn;
import com.fiveamsolutions.tissuelocator.integrator.service.adapter.XmlImportFileConverter;
import com.fiveamsolutions.tissuelocator.integrator.services.TissueBankIntegrationHelper;
import com.fiveamsolutions.tissuelocator.integrator.test.AbstractHibernateTestCase;
import com.fiveamsolutions.tissuelocator.integrator.test.MockFtpFileImportHelper;
import com.fiveamsolutions.tissuelocator.integrator.util.TissueLocatorIntegratorRegistry;

/**
 * @author smiller
 *
 */
public class FileImportAdapterTest extends AbstractHibernateTestCase {
    private FileImportIntegrationConfig config;
    private static final long INSTITUTION_ID = 1234L;
    private static final long MULTIPLE_PROTOCOLS_ID = 5678L;
    private static final int BATCH_SIZE = 1000;
    private static final String CSV_CONVERTER_CLASS =
        "com.fiveamsolutions.tissuelocator.integrator.service.adapter.CsvImportFileConverter";
    private static final String INVALID_COLUMN_NAME = "invalidColumn";

    /**
     * Before method.
     */
    @Before
    public void before() {
        TissueBankIntegrationConfigServiceLocal service =
            TissueLocatorIntegratorRegistry.getServiceLocator().getTissueBankIntegrationConfigService();
        config = TissueBankIntegrationHelper.createConfig(1L, 1, new Date());
        Long id = service.savePersistentObject(config);
        config = (FileImportIntegrationConfig) service.getPersistentObject(AbstractTissueBankIntegrationConfig.class,
                id);
    }

    /**
     * Test the adapter.
     * @throws JAXBException on error
     */
    @Test
    public void launchTest() throws JAXBException {
        TestFileImportAdapter adapter = new TestFileImportAdapter();
        List<ImportBatch> importBatchList = adapter.launch(config);
        FileImportBatch importBatch = (FileImportBatch) importBatchList.get(0);
        assertEquals(0, importBatch.getFailures().intValue());
        assertEquals(1, importBatch.getImports().intValue());
        // Do not need to check start time as it is set by the service, but check the end time
        assertNotNull(importBatch.getEndTime());

        SpecimenList specimenList = unmarshal(adapter.getHelper().downloadFile(importBatch));
        SpecimenValidator.INSTANCE.checkSpecimen(specimenList.getSpecimens().get(0));
    }

    /**
     * Test the adapter with a csv import.
     * @throws Exception on error.
     */
    @Test
    public void launchCsvTest() throws Exception {
        TestCsvFileImportAdapter csvAdapter = new TestCsvFileImportAdapter(
                SpecimenHelper.INSTANCE.createSpecimen(), SpecimenColumn.values(), null);
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        List<ImportBatch> importBatchList = csvAdapter.launch(config);
        FileImportBatch importBatch = (FileImportBatch) importBatchList.get(0);
        assertEquals(0, importBatch.getFailures().intValue());
        assertEquals(1, importBatch.getImports().intValue());
        assertNotNull(importBatch.getEndTime());

        CsvImportFileConverter converter = new CsvImportFileConverter();
        converter.init(importBatch, csvAdapter.getHelper(), initTranslationMap());
        Specimen specimen = converter.getNext();
        SpecimenValidator.INSTANCE.checkCsvSpecimen(specimen, true);

        //test completion of multiple threads
        List<Specimen> specimens = new ArrayList<Specimen>();
        for (int i = 0; i < BATCH_SIZE + 1; i++) {
            specimens.add(SpecimenHelper.INSTANCE.createSpecimen());
        }
        csvAdapter = new TestCsvFileImportAdapter(specimens, SpecimenColumn.values(), null);
        importBatchList = csvAdapter.launch(config);
        importBatch = (FileImportBatch) importBatchList.get(0);
        assertEquals(0, importBatch.getFailures().intValue());
        assertEquals(BATCH_SIZE + 1, importBatch.getImports().intValue());
        assertNotNull(importBatch.getEndTime());
    }

    private SpecimenList unmarshal(InputStream s) throws JAXBException {
        Unmarshaller um = JAXBContext.newInstance(SpecimenList.class).createUnmarshaller();
        SpecimenList list = (SpecimenList) um.unmarshal(s);
        for (Specimen specimen : list.getSpecimens()) {
            specimen.setId(null);
        }
        return list;
    }

    /**
     * Test the case where a test fails due to error in entire xml.
     * @throws FileParseException on error
     * @throws AddressException if address error occurs
     */
    @Test
    public void failedLaunchTest() throws FileParseException, AddressException {
        FileImportAdapter adapter = new TestFileImportAdapter();
        // The Mock service returns an ImportBatch that was not set correctly in order to test
        // the case where an Exception occurs
        config.setInstitutionId(INSTITUTION_ID);
        List<ImportBatch> importBatchList = adapter.launch(config);
        FileImportBatch importBatch = (FileImportBatch) importBatchList.get(0);
        assertEquals(1, importBatch.getFailures().intValue());
        assertNotNull(importBatch.getEndTime());

        TestInvalidCsvFileImportAdapter csvAdapter = new TestInvalidCsvFileImportAdapter();
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        importBatchList = csvAdapter.launch(config);
        importBatch = (FileImportBatch) importBatchList.get(0);
        assertEquals(1, importBatch.getFailures().intValue());
        assertNotNull(importBatch.getEndTime());
    }

    /**
     * Tests that default values are set when not provided.
     * @throws Exception on error.
     */
    @Test
    public void testDefaultValues() throws Exception {
        // test that no exceptions occur, as no value is returned to check
        Specimen specimen = SpecimenHelper.INSTANCE.createSpecimen();
        specimen.setProtocol(null);
        TestCustomFileImportAdapter adapter = new TestCustomFileImportAdapter(specimen);
        adapter.setFtpHelper(adapter.createHelper());

        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);
        adapter.importSpecimens(fileBatch, null, translationMap);

        config.setDefaultImportValueConfigs(new ArrayList<DefaultImportValueConfig>());
        config.getDefaultImportValueConfigs().add(TissueBankIntegrationHelper.createDefaultValueConfig());
        specimen.setProtocol(new CollectionProtocol());
        specimen.setSpecimenType(null);
        adapter = new TestCustomFileImportAdapter(specimen);
        adapter.setFtpHelper(adapter.createHelper());

        fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);
        adapter.importSpecimens(fileBatch, null, translationMap);

        // Test no default found, no exceptions
        specimen = SpecimenHelper.INSTANCE.createSpecimen();
        specimen.setProtocol(null);
        List<SpecimenColumn> columns = new ArrayList<SpecimenColumn>(Arrays.asList(SpecimenColumn.values()));
        columns.remove(SpecimenColumn.COLLECTION_PROTOCOL);
        TestCsvFileImportAdapter csvAdapter = new TestCsvFileImportAdapter(specimen,
                columns.toArray(new SpecimenColumn[columns.size()]), null);
        csvAdapter.setFtpHelper(csvAdapter.createHelper());
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        config.setInstitutionId(INSTITUTION_ID);
        fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);
        csvAdapter.importSpecimens(fileBatch, null, translationMap);

        // no default due to multiple protocols
        config.setInstitutionId(MULTIPLE_PROTOCOLS_ID);
        fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);
        csvAdapter.importSpecimens(fileBatch, null, translationMap);
    }

    /**
     * Tests the fail import of an invalid Specimen.
     * @throws Exception on error
     */
    @Test
    public void testImportSpecimensException() throws Exception {
        MailUtils.setMailEnabled(true);
        Mailbox.clearAll();
        TestFailingFileImportAdapter adapter = new TestFailingFileImportAdapter();
        adapter.setFtpHelper(adapter.createHelper());

        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);
        adapter.importSpecimens(fileBatch, null, translationMap);
        assertEquals(1, Mailbox.get(TissueBankIntegrationHelper.NOTIFICATION_EMAIL).size());
        testEmail(TissueBankIntegrationHelper.NOTIFICATION_EMAIL, "An Error occurred during Specimen synchronization",
                "with external id test external id and institution id 1",
                "The following error occurred during Specimen synchronization",
                "Please contact the System administrator.");
        Mailbox.clearAll();

        Specimen specimen = SpecimenHelper.INSTANCE.createSpecimen();
        specimen.setStatus(SpecimenStatus.PENDING_SHIPMENT);
        TestCsvFileImportAdapter csvAdapter = new TestCsvFileImportAdapter(specimen, SpecimenColumn.values(), null);
        csvAdapter.setFtpHelper(csvAdapter.createHelper());
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);
        csvAdapter.importSpecimens(fileBatch, null, translationMap);
        assertEquals(1, Mailbox.get(TissueBankIntegrationHelper.NOTIFICATION_EMAIL).size());
        testEmail(TissueBankIntegrationHelper.NOTIFICATION_EMAIL, "An Error occurred during Specimen synchronization",
                "with external id test external id and institution id 1",
                "The following error occurred during Specimen synchronization",
                "Please contact the System administrator.");
        Mailbox.clearAll();

        specimen = SpecimenHelper.INSTANCE.createSpecimen();
        List<SpecimenColumn> invalidColumns = new ArrayList<SpecimenColumn>();
        invalidColumns.add(SpecimenColumn.STATUS);
        csvAdapter = new TestCsvFileImportAdapter(specimen, SpecimenColumn.values(), invalidColumns);
        csvAdapter.setFtpHelper(csvAdapter.createHelper());
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);
        csvAdapter.importSpecimens(fileBatch, null, translationMap);
        assertEquals(1, Mailbox.get(TissueBankIntegrationHelper.NOTIFICATION_EMAIL).size());
        testEmail(TissueBankIntegrationHelper.NOTIFICATION_EMAIL, "An Error occurred during Specimen synchronization",
                "An exception occured while parsing a CSV file.",
                "CSV Parse Errors: Row 2",
                "Column: " + SpecimenColumn.STATUS.getColumnHeader(),
                "Value: bad value",
                "Please contact the System administrator.");

        Mailbox.clearAll();
        MailUtils.setMailEnabled(false);
     }

    /**
     * Test the xml file converter.
     * @throws Exception on error.
     */
    @Test
    public void testXmlImportFileConverter() throws Exception {
        TestFileImportAdapter adapter = new TestFileImportAdapter();
        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        XmlImportFileConverter converter = new XmlImportFileConverter();
        converter.init(fileBatch, adapter.createHelper(), initTranslationMap());
        Specimen specimen = converter.getNext();
        assertNotNull(specimen);
        SpecimenValidator.INSTANCE.checkSpecimen(specimen);

        specimen = converter.getNext();
        assertNull(specimen);

        adapter = new TestFileImportAdapter();
        config.setInstitutionId(INSTITUTION_ID);
        fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);
        converter = new XmlImportFileConverter();
        try {
            converter.init(fileBatch, adapter.createHelper(), initTranslationMap());
            fail("Invalid xml file.");
        } catch (Exception e) {
            // expected
        }

    }

    /**
     * Test the csv file converter.
     * @throws Exception on error.
     */
    @Test
    public void testCsvImportFileConvertor() throws Exception {
        TestCsvFileImportAdapter adapter = new TestCsvFileImportAdapter(
            SpecimenHelper.INSTANCE.createSpecimen(), SpecimenColumn.values(), null);
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        CsvImportFileConverter converter = new CsvImportFileConverter();
        converter.init(fileBatch, adapter.createHelper(), initTranslationMap());
        Specimen specimen = converter.getNext();
        assertNotNull(specimen);
        SpecimenValidator.INSTANCE.checkCsvSpecimen(specimen, true);

        specimen = converter.getNext();
        assertNull(specimen);

        // test tab delimiter
        adapter = new TestCsvFileImportAdapter(SpecimenHelper.INSTANCE.createSpecimen(), SpecimenColumn.values(), null);
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.TAB);
        fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        converter = new CsvImportFileConverter();
        converter.init(fileBatch, adapter.createHelper(), initTranslationMap());
        specimen = converter.getNext();
        assertNotNull(specimen);
        SpecimenValidator.INSTANCE.checkCsvSpecimen(specimen, true);

        specimen = converter.getNext();
        assertNull(specimen);

        // test unpopulated specimens fields, confirm no exceptions occur
        adapter = new TestCsvFileImportAdapter(new Specimen(), SpecimenColumn.values(), null);
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        converter = new CsvImportFileConverter();
        converter.init(fileBatch, adapter.createHelper(), initTranslationMap());
        specimen = converter.getNext();
        assertNotNull(specimen);

        specimen = converter.getNext();
        assertNull(specimen);

        // test invalid csv file
        TestInvalidCsvFileImportAdapter invalid = new TestInvalidCsvFileImportAdapter();
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        converter = new CsvImportFileConverter();
        try {
            converter.init(fileBatch, invalid.createHelper(), initTranslationMap());
            fail("Invalid csv file.");
        } catch (FileParseException e) {
            // expected
        }

        // test various null conditions
        specimen = new Specimen();
        specimen.setPathologicalCharacteristic(new AdditionalPathologicFinding());
        specimen.setSpecimenType(new SpecimenType());
        specimen.setParticipant(new Participant());
        specimen.setProtocol(new CollectionProtocol());

        adapter = new TestCsvFileImportAdapter(specimen, SpecimenColumn.values(), null);
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        converter = new CsvImportFileConverter();
        converter.init(fileBatch, adapter.createHelper(), initTranslationMap());
        specimen = converter.getNext();
        assertNotNull(specimen);

        specimen = converter.getNext();
        assertNull(specimen);
    }

    /**
     * Test the handling of invalid csv values.
     * @throws Exception on error.
     */
    @Test
    public void testCsvParseException() throws Exception {
        List<SpecimenColumn> invalidColumns = new ArrayList<SpecimenColumn>();
        invalidColumns.add(SpecimenColumn.STATUS);
        verifyCsvParseException(invalidColumns);
        invalidColumns.add(SpecimenColumn.AVAILABLE_QUANTITY);
        verifyCsvParseException(invalidColumns);
        invalidColumns.add(SpecimenColumn.AVAILABLE_QUANTITY_UNITS);
        verifyCsvParseException(invalidColumns);
        invalidColumns.add(SpecimenColumn.COLLECTION_YEAR);
        verifyCsvParseException(invalidColumns);
        invalidColumns.add(SpecimenColumn.ETHNICITY);
        verifyCsvParseException(invalidColumns);
        invalidColumns.add(SpecimenColumn.GENDER);
        verifyCsvParseException(invalidColumns);
        invalidColumns.add(SpecimenColumn.MAXIMUM_PRICE);
        verifyCsvParseException(invalidColumns);
        invalidColumns.add(SpecimenColumn.MINIMUM_PRICE);
        verifyCsvParseException(invalidColumns);
        invalidColumns.add(SpecimenColumn.PATIENT_AGE_AT_COLLECTION);
        verifyCsvParseException(invalidColumns);
        invalidColumns.add(SpecimenColumn.PATIENT_AGE_AT_COLLECTION_UNITS);
        verifyCsvParseException(invalidColumns);
    }

    private void verifyCsvParseException(List<SpecimenColumn> invalidColumns)
        throws FileParseException {
        List<Specimen> specimenList = new ArrayList<Specimen>();
        List<List<SpecimenColumn>> invalidColumnList = new ArrayList<List<SpecimenColumn>>();
        specimenList.add(SpecimenHelper.INSTANCE.createSpecimen());
        specimenList.add(SpecimenHelper.INSTANCE.createSpecimen());
        invalidColumnList.add(invalidColumns);
        invalidColumnList.add(new ArrayList<SpecimenColumn>());
        TestCsvFileImportAdapter adapter = new TestCsvFileImportAdapter(
                specimenList, SpecimenColumn.values(), invalidColumnList);
        config
                .setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        CsvImportFileConverter converter = new CsvImportFileConverter();
        converter.init(fileBatch, adapter.createHelper(), initTranslationMap());
        try {
            converter.getNext();
            fail("CsvParseException expected.");
        } catch (CsvParseException e) {
            assertEquals(2L, e.getRow());
            assertEquals(invalidColumns.size(), e.getInvalidCsvValues().size());
            List<String> errorColumns = new ArrayList<String>();
            for (InvalidCsvValue val : e.getInvalidCsvValues()) {
                assertEquals("bad value", val.getInvalidValue());
                assertNotNull(val.getException());
                errorColumns.add(val.getColumnHeader());
            }
            for (SpecimenColumn col : invalidColumns) {
                assertTrue(errorColumns.contains(col.getColumnHeader()));
            }
        }

        try {
            assertNotNull(converter.getNext());
        } catch (CsvParseException e) {
            fail("No exception expected.");
        }
        try {
            assertNull(converter.getNext());
        } catch (CsvParseException e) {
            fail("No exception expected.");
        }
    }

    /**
     * Test an invalid converter class.
     * @throws Exception on error.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testInvalidFileConvertor() throws Exception {
        config.setFileConvertorClass("invalid");
        TestFileImportAdapter adapter = new TestFileImportAdapter();
        List<ImportBatch> importBatchList = adapter.launch(config);
        FileImportBatch importBatch = (FileImportBatch) importBatchList.get(0);

        TranslationServiceLocal tService = TissueLocatorIntegratorRegistry.getServiceLocator().getTranslationService();
        Map<String, Map<String, Map<String, String>>> translationMap = tService.getTranslationMap(1L);
        try {
            adapter.importSpecimens(importBatch, null, translationMap);
            fail("Invalid convertor class, exception expected.");
        } catch (IllegalArgumentException e) {
            // expected
        }

        config.setFileConvertorClass(ImportFileConverter.class.getName());
        try {
            adapter.importSpecimens(importBatch, null, translationMap);
            fail("Invalid convertor class, exception expected.");
        } catch (IllegalArgumentException e) {
            // expected
        }
        config.setFileConvertorClass(TestFileImportAdapter.class.getName());
        adapter.importSpecimens(importBatch, null, translationMap);
    }

    /**
     * Test the handling of invalid csv header values.
     * @throws FileParseException on error
     */
    @Test
    public void testCsvParseHeaderException() throws FileParseException {
        TestCsvFileImportAdapter csvAdapter = new TestCsvFileImportAdapter(
                SpecimenHelper.INSTANCE.createSpecimen(), SpecimenColumn.values(), null);
        csvAdapter.setIncludeInvalidExtensionColumn(true);
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        CsvImportFileConverter converter = new CsvImportFileConverter();
        try {
            converter.init(fileBatch, csvAdapter.createHelper(), initTranslationMap());
            fail("InvalidCsvHeaderException expected.");
        } catch (InvalidCsvHeaderException e) {
            assertNotNull(e.getInvalidHeaders());
            assertFalse(e.getInvalidHeaders().isEmpty());
            assertEquals(1, e.getInvalidHeaders().size());
            assertEquals(StringUtils.lowerCase(INVALID_COLUMN_NAME), e.getInvalidHeaders().iterator().next());
        }
    }

    /**
     * Test the handling of ignored columns when validating the header row.
     * @throws FileParseException on error
     */
    @Test
    public void testIgnoredColumnsHeaderValidation() throws FileParseException {
        TestCsvFileImportAdapter csvAdapter = new TestCsvFileImportAdapter(
                SpecimenHelper.INSTANCE.createSpecimen(), SpecimenColumn.values(), null);
        csvAdapter.setIncludeInvalidExtensionColumn(true);
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        config.getIgnoredColumns().add(INVALID_COLUMN_NAME);
        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        CsvImportFileConverter converter = new CsvImportFileConverter();
        converter.init(fileBatch, csvAdapter.createHelper(), initTranslationMap());
        Specimen specimen = converter.getNext();
        assertNotNull(specimen);
    }

    /**
     * Test the handling of ignored columns when populating a specimen.
     * @throws FileParseException on error
     */
    @Test
    public void testIgnoredColumnsSpecimenPopulation() throws FileParseException {
        TestCsvFileImportAdapter csvAdapter = new TestCsvFileImportAdapter(
                SpecimenHelper.INSTANCE.createSpecimen(), SpecimenColumn.values(), null);
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        config.getIgnoredColumns().add("availableQuantityUnits");
        config.getIgnoredColumns().add("collectionType");
        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);

        CsvImportFileConverter converter = new CsvImportFileConverter();
        converter.init(fileBatch, csvAdapter.createHelper(), initTranslationMap());
        Specimen specimen = converter.getNext();
        assertNotNull(specimen);
        assertNull(specimen.getAvailableQuantityUnits());
        assertFalse(specimen.getCustomProperties().containsKey("collectionType"));
    }
    
    /**
     * Test csv imports with client-specific column headers.
     * @throws FileParseException on error.
     */
    @Test
    public void testClientSpecificColumnHeaders() throws FileParseException { 
        Specimen specimen = SpecimenHelper.INSTANCE.createSpecimen();
        TestCsvFileImportAdapter csvAdapter = new TestCsvFileImportAdapter(
                specimen, SpecimenColumn.values(), null);
        Map<String, String> headerReplacements = new HashMap<String, String>();
        for (SpecimenColumn column : SpecimenColumn.values()) {
            headerReplacements.put(column.getColumnHeader(), "client" + column.name());
        }
        for (String customProperty : specimen.getCustomProperties().keySet()) {
            headerReplacements.put(customProperty, "client" + customProperty);
        }
        csvAdapter.setHeaderReplacements(headerReplacements);
        config.setFileConvertorClass(CSV_CONVERTER_CLASS);
        config.setDelimiter(Delimiter.COMMA);
        FileImportBatch fileBatch = new FileImportBatch();
        fileBatch.setStartTime(new Date());
        fileBatch.setConfig(config);
        
        CsvImportFileConverter converter = new CsvImportFileConverter();
        Map<String, Map<String, Map<String, String>>> translationMap = initTranslationMap();
        Map<String, Map<String, String>> codeSetMap = translationMap.get(config.getCodeSets().get(0));
        Map<String, String> headerMap = new HashMap<String, String>();
        for (String header : headerReplacements.keySet()) {
            headerMap.put(StringUtils.lowerCase(headerReplacements.get(header)), header);
        }
        codeSetMap.put(SpecimenColumn.class.getSimpleName(), headerMap);
        converter.init(fileBatch, csvAdapter.createHelper(), translationMap);
        specimen = converter.getNext();
        assertNotNull(specimen);
        SpecimenValidator.INSTANCE.checkCsvSpecimen(specimen, true);
    }

    private Map<String, Map<String, Map<String, String>>> initTranslationMap() {
       Map<String, Map<String, Map<String, String>>> translationMap = 
           new HashMap<String, Map<String, Map<String, String>>>();
       for (String codeSet : config.getCodeSets()) {
           translationMap.put(codeSet, new HashMap<String, Map<String, String>>());
       }
       return translationMap;
    }
    
    /**
     * @author smiller
     */
    class TestFileImportAdapter extends FileImportAdapter {

        private MockFtpFileImportHelper helper;

        /**
         * {@inheritDoc}
         */
        @Override
        public FtpFileImportHelper createHelper() {
            helper = new MockFtpFileImportHelper(config, this);
            return helper;
        }

        /**
         * get the helper.
         * @return the helper.
         */
        public MockFtpFileImportHelper getHelper() {
            return helper;
        }
    }

    /**
     *
     * @author gvaughn
     *
     */
    class TestCustomFileImportAdapter extends TestFileImportAdapter {

        private final Specimen specimen;
        private MockFtpFileImportHelper helper;
        /**
         *
         */
        public TestCustomFileImportAdapter(Specimen specimen) {
           this.specimen = specimen;
        }

        /**
        *
        * {@inheritDoc}
        */
       @Override
       public FtpFileImportHelper createHelper() {
           helper = new MockFtpFileImportHelper(config, this) {
               /**
                * {@inheritDoc}
                */
               @Override
               public InputStream downloadFile(FileImportBatch batch) {
                   SpecimenList specimenList = new SpecimenList();
                   specimenList.getSpecimens().add(specimen);
                   try {
                       String output = marshal(specimenList);
                       return new ByteArrayInputStream(output.getBytes());
                   } catch (JAXBException jax) {
                       // return error stream, not expected for tests
                   }
                   return new ByteArrayInputStream("invalid.xml".getBytes());
               }

           };
           return helper;
       }
    }

    /**
     *
     * @author gvaughn
     *
     */
    class TestCsvFileImportAdapter extends FileImportAdapter {
        private MockFtpFileImportHelper helper;
        private final List<Specimen> specimens = new ArrayList<Specimen>();
        private final SpecimenColumn[] columns;
        private final List<List<SpecimenColumn>> invalidColumnsList = new ArrayList<List<SpecimenColumn>>();
        private boolean includeInvalidExtensionColumn;
        private Map<String, String> replacementMap = new HashMap<String, String>();

        public TestCsvFileImportAdapter(Specimen specimen, SpecimenColumn[] columns,
                List<SpecimenColumn> invalidColumns) {
            specimens.add(specimen);
            this.columns = columns;
            if (invalidColumns != null) {
                invalidColumnsList.add(invalidColumns);
            }
        }

        public TestCsvFileImportAdapter(List<Specimen> specimens, SpecimenColumn[] columns,
                List<List<SpecimenColumn>> invalidColumns) {
            this.specimens.addAll(specimens);
            this.columns = columns;
            if (invalidColumns != null) {
                invalidColumnsList.addAll(invalidColumns);
            }
        }

        /**
         * @param includeInvalidExtensionColumn the includeInvalidExtensionColumn to set
         */
        public void setIncludeInvalidExtensionColumn(boolean includeInvalidExtensionColumn) {
            this.includeInvalidExtensionColumn = includeInvalidExtensionColumn;
        }
        
        public void setHeaderReplacements(Map<String, String> headerReplacements) {
            this.replacementMap = headerReplacements;
        }
        
        private void replaceHeaders(String[] headers) {
            for (String original : replacementMap.keySet()) {
                int index = ArrayUtils.indexOf(headers, original);
                headers[index] = replacementMap.get(original);
            }            
        }

       /**
        * {@inheritDoc}
        */
       @Override
        public FtpFileImportHelper createHelper() {

            helper = new MockFtpFileImportHelper(config, this) {

                /**
                 * {@inheritDoc}
                 */
                @Override
                public InputStream downloadFile(FileImportBatch batch) {

                    try {
                        StringWriter out = new StringWriter();
                        CsvWriter writer = new CsvWriter(out,
                                ((FileImportIntegrationConfig) batch
                                        .getConfig()).getDelimiterCharacter());
                        int fileWidth = columns.length + specimens.get(0).getCustomProperties().size();
                        if (includeInvalidExtensionColumn) {
                            fileWidth++;
                        }
                        String[] headers = new String[fileWidth];
                        for (int i = 0; i < columns.length; i++) {
                            headers[i] = columns[i].getColumnHeader();
                        }
                        List<String> deHeaders = new ArrayList<String>(specimens.get(0).getCustomProperties().keySet());
                        for (int i = 0; i < deHeaders.size(); i++) {
                            headers[i + columns.length] = deHeaders.get(i);
                        }
                        if (includeInvalidExtensionColumn) {
                            headers[fileWidth - 1] = INVALID_COLUMN_NAME;
                        }
                        String[][] valueMatrix = new String[specimens.size()][fileWidth];
                        int valueIndex = 0;
                        for (Specimen specimen : specimens) {
                            List<SpecimenColumn> invalidColumns = invalidColumnsList.size() > valueIndex
                                    ? invalidColumnsList.get(valueIndex)
                                    : new ArrayList<SpecimenColumn>();
                            String[] values = new String[fileWidth];
                            for (int i = 0; i < columns.length; i++) {
                                if (invalidColumns.contains(columns[i])) {
                                    values[i] = "bad value";
                                } else {
                                    values[i] = SpecimenColumnHelper.getValue(specimen, columns[i]);
                                }
                            }
                            for (int i = 0; i < deHeaders.size(); i++) {
                                values[i + columns.length] = specimen.getCustomProperty(deHeaders.get(i)).toString();
                            }
                            if (includeInvalidExtensionColumn) {
                                values[fileWidth - 1] = "bad value";
                            }
                            valueMatrix[valueIndex] = values;
                            valueIndex++;
                        }

                        replaceHeaders(headers);
                        writer.writeRecord(headers);
                        for (String[] values : valueMatrix) {
                            writer.writeRecord(values);
                        }
                        writer.flush();
                        writer.close();
                        return new StringInputStream(out.toString());
                    } catch (IOException e) {
                        // return error stream, not expected for tests
                    }

                    return new ByteArrayInputStream("invalid.csv".getBytes());
                }

            };
            return helper;
        }


       /**
        * get the helper.
        * @return the helper.
        */
       public MockFtpFileImportHelper getHelper() {
           return helper;
       }
    }


    /**
     *
     * @author smiller
     */
    class TestFailingFileImportAdapter extends FileImportAdapter {

        private MockFtpFileImportHelper helper;

        /**
         * {@inheritDoc}
         */
        @Override
        public FtpFileImportHelper createHelper() {
            helper = new MockFtpFileImportHelper(config, this) {

                        @Override
                        public InputStream downloadFile(FileImportBatch batch) {
                            SpecimenList specimenList = new SpecimenList();
                            specimenList.getSpecimens().add(SpecimenHelper.INSTANCE.createSpecimen());
                            specimenList.getSpecimens().get(0).setStatus(SpecimenStatus.PENDING_SHIPMENT);
                            try {
                                String output = getAdapter().marshal(specimenList);
                                return new ByteArrayInputStream(output.getBytes());
                            } catch (JAXBException jax) {
                                // return error stream, not expected for tests
                            }
                            return new ByteArrayInputStream("invalid.xml".getBytes());
                        }

            };
            return helper;
        }

        /**
         * get the helper.
         * @return the helper.
         */
        public MockFtpFileImportHelper getHelper() {
            return helper;
        }
    }

    /**
    *
    * @author gvaughn
    */
   class TestInvalidCsvFileImportAdapter extends FileImportAdapter {

       private MockFtpFileImportHelper helper;

       /**
        * {@inheritDoc}
        */
       @Override
       public FtpFileImportHelper createHelper() {
           helper = new MockFtpFileImportHelper(config, this) {

                       @Override
                       public InputStream downloadFile(FileImportBatch batch) {
                           return new InputStream() {

                            @Override
                            public int read() throws IOException {
                                return -1;
                            }
                        };
                       }

           };
           return helper;
       }

       /**
        * get the helper.
        * @return the helper.
        */
       public MockFtpFileImportHelper getHelper() {
           return helper;
       }
   }
}
