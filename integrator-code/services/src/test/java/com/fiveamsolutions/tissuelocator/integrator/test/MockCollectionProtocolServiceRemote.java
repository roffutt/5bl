/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.integrator.services.adapter.SpecimenHelper;
import com.fiveamsolutions.tissuelocator.service.CollectionProtocolServiceRemote;

/**
 * @author bpickeral
 *
 */
public class MockCollectionProtocolServiceRemote implements CollectionProtocolServiceRemote {

    private static final long NO_RESULTS_ID = 1234L;
    private static final long MULTIPLE_RESULTS_ID = 5678L;
    /**
     * {@inheritDoc}
     */
    public Map<String, CollectionProtocol> getProtocols(Long institutionId, Collection<String> names) {
        Map<String, CollectionProtocol> result = new HashMap<String, CollectionProtocol>();
        // If the externalId is equal to EXTERNAL_ID, we are testing the case that a Specimen exists
        // that we want to update, otherwise we are testing the case that a Specimen does not exist
        if (names != null && !names.isEmpty() && names.iterator().next() != null
                && names.iterator().next().equals(SpecimenHelper.NAME)) {
            CollectionProtocol cp = new CollectionProtocol();
            cp.setId(1L);
            cp.setName(SpecimenHelper.NAME);
            cp.setInstitution(new Institution());
            result.put(SpecimenHelper.NAME, cp);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public List<CollectionProtocol> getProtocolsByInstitution(Long institutionId) {
        List<CollectionProtocol> result = new ArrayList<CollectionProtocol>();
        if (institutionId != NO_RESULTS_ID) {
            CollectionProtocol cp = new CollectionProtocol();
            cp.setId(1L);
            cp.setName(SpecimenHelper.NAME);
            cp.setInstitution(new Institution());
            result.add(cp);
            if (institutionId == MULTIPLE_RESULTS_ID) {
                cp = new CollectionProtocol();
                cp.setId(2L);
                cp.setName(SpecimenHelper.NAME);
                cp.setInstitution(new Institution());
                result.add(cp);
            }
        }
        return result;
    }
    
}
