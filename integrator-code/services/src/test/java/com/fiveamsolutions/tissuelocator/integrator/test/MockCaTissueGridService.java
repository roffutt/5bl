/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import com.fiveamsolutions.tissuelocator.data.SpecimenClass;
import com.fiveamsolutions.tissuelocator.integrator.service.CaTissueGridService;

import edu.wustl.catissuecore.domain.CollectionEventParameters;
import edu.wustl.catissuecore.domain.CollectionProtocol;
import edu.wustl.catissuecore.domain.Participant;
import edu.wustl.catissuecore.domain.ParticipantMedicalIdentifier;
import edu.wustl.catissuecore.domain.Race;
import edu.wustl.catissuecore.domain.Site;
import edu.wustl.catissuecore.domain.Specimen;
import edu.wustl.catissuecore.domain.SpecimenCollectionGroup;
import edu.wustl.catissuecore.domain.User;

/**
 * @author ddasgupta
 *
 */
public class MockCaTissueGridService implements CaTissueGridService {

    /**
     * {@inheritDoc}
     */
    public CollectionEventParameters getCollectionEventParameters(Long specimenId, Long locationId) throws Exception {
        if (specimenId == 0) {
            return null;
        }
        CollectionEventParameters cep = new CollectionEventParameters();
        cep.setCollectionProcedure("Surgical Resection");
        if (specimenId > 1) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -1);
            cep.setTimestamp(cal.getTime());
        }
        return cep;
    }

    /**
     * {@inheritDoc}
     */
    public CollectionProtocol getCollectionProtocol(Long specimenId, Long locationId) throws Exception {
        if (specimenId == 0) {
            return null;
        }
        CollectionProtocol cp = new CollectionProtocol();
        cp.setStartDate(new Date());
        cp.setEndDate(new Date());
        cp.setTitle("protocol");
        return cp;
    }

    /**
     * {@inheritDoc}
     */
    public User getCollectionProtocolPI(Long specimenId, Long locationId) throws Exception {
        if (specimenId == 0) {
            return null;
        }
        User u = new User();
        u.setEmailAddress("email address");
        u.setFirstName("first name");
        u.setLastName("last name");
        return u;
    }

    /**
     * {@inheritDoc}
     */
    public Site getCollectionProtocolSite(Long specimenId, Long locationId) throws Exception {
        if (specimenId == 0) {
            return null;
        }
        Site s = new Site();
        s.setName("site name");
        return s;
    }

    /**
     * {@inheritDoc}
     */
    public Participant getParticipant(Long specimenId, Long locationId) throws Exception {
        if (specimenId == 0) {
            return null;
        }
        Participant p = new Participant();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);
        cal.add(Calendar.YEAR, -1);
        cal.add(Calendar.MONTH, -1);
        p.setBirthDate(cal.getTime());
        p.setGender("Male Gender");
        p.setEthnicity("Hispanic or Latino");
        return p;
    }

    /**
     * {@inheritDoc}
     */
    public ParticipantMedicalIdentifier getParticipantId(Long specimenId, Long locationId) throws Exception {
        if (specimenId == 0) {
            return null;
        }
        ParticipantMedicalIdentifier pmi = new ParticipantMedicalIdentifier();
        pmi.setMedicalRecordNumber("MRN");
        return pmi;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<Race> getParticipantRaces(Long specimenId, Long locationId) throws Exception {
        String[] raceArray = new String[] {"Asian", "Black or African American", "White"};
        Collection<Race> races = new ArrayList<Race>();
        if (specimenId != 0) {
            for (String raceName : raceArray) {
                Race race = new Race();
                race.setRaceName(raceName);
                races.add(race);
            }
        }
        return races;
    }

    /**
     * {@inheritDoc}
     */
    public Specimen getSpecimen(Long specimenId, SpecimenClass specimenClass, Long locationId) throws Exception {
        if (specimenId < 0) {
            throw new IllegalArgumentException("id must be greater than or equal to 0");
        }
        if (specimenId == 0) {
            return null;
        }
        Specimen s = new Specimen();
        s.setBarcode("barcode");
        s.setActivityStatus("Active");
        if (specimenId > 1) {
            s.setActivityStatus("Inactive");
        }
        s.setAvailableQuantity(1.0);
        s.setSpecimenType("frozen tissue");
        if (specimenId > 2) {
            s.setCreatedOn(new Date());
        }
        return s;
    }

    /**
     * {@inheritDoc}
     */
    public SpecimenCollectionGroup getSpecimenCollectionGroup(Long specimenId, Long locationId) throws Exception {
        if (specimenId == 0) {
            return null;
        }
        SpecimenCollectionGroup scg = new SpecimenCollectionGroup();
        scg.setClinicalDiagnosis("disease");
        return scg;
    }

    /**
     * {@inheritDoc}
     */
    public Site getSpecimenCollectionGroupSite(Long specimenId, Long locationId) throws Exception {
        if (specimenId == 0) {
            return null;
        }
        Site s = new Site();
        s.setName("site name");
        return s;
    }
}
