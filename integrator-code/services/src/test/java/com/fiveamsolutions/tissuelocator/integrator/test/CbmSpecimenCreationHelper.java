/**
 * Copyright (c) 2009, 5AM Solutions, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * - Neither the name of the author nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.fiveamsolutions.tissuelocator.integrator.test;

import java.util.ArrayList;
import java.util.Date;

import com.fiveamsolutions.tissuelocator.data.CollectionProtocol;
import com.fiveamsolutions.tissuelocator.data.Contact;
import com.fiveamsolutions.tissuelocator.data.Ethnicity;
import com.fiveamsolutions.tissuelocator.data.Gender;
import com.fiveamsolutions.tissuelocator.data.Institution;
import com.fiveamsolutions.tissuelocator.data.Participant;
import com.fiveamsolutions.tissuelocator.data.Race;
import com.fiveamsolutions.tissuelocator.data.Specimen;
import com.fiveamsolutions.tissuelocator.data.TimeUnits;
import com.fiveamsolutions.tissuelocator.data.code.AdditionalPathologicFinding;
import com.fiveamsolutions.tissuelocator.data.code.SpecimenType;

/**
 * @author smiller
 *
 */
public class CbmSpecimenCreationHelper {
    
    /**
     * 
     */
    private static final int PATIENT_AGE_IN_MONTHS = 19;

    /**
     * Create a specimen with all fields used by cbm populated.
     * @param suffix a unique suffix to add to all fields
     * @param p the participant, or null to create a new one.
     * @param cp the protocol, or null to create a new one.
     * @return the specimen.
     */
    public Specimen createSpecimen(int suffix, Participant p, CollectionProtocol cp) {
        Specimen s = new Specimen();
        s.setId(Long.valueOf(suffix));
        
        s.setSpecimenType(new SpecimenType());
        s.getSpecimenType().setActive(true);
        s.getSpecimenType().setName("Test" + SpecimenType.class.getSimpleName() + suffix);
        
        s.setPatientAgeAtCollection(PATIENT_AGE_IN_MONTHS);
        s.setPatientAgeAtCollectionUnits(TimeUnits.MONTHS);
        s.setPathologicalCharacteristic(new AdditionalPathologicFinding());
        s.getPathologicalCharacteristic().setActive(true);
        s.getPathologicalCharacteristic().setName("Test" + AdditionalPathologicFinding.class.getSimpleName() + suffix);

        if (p == null) {
            s.setParticipant(createParticipant(suffix));
        } else {
            s.setParticipant(p);
        }

        if (cp == null) {
            s.setProtocol(createCollectionProtocol(suffix));
        } else {
            s.setProtocol(cp);
        }
        
        return s;
    }
    
    /**
     * Create a protocol.
     * @param suffix unique suffix to add to fields.
     * @return the protocol
     */
    public CollectionProtocol createCollectionProtocol(int suffix) {
        CollectionProtocol cp = new CollectionProtocol();
        cp.setId(new Long(suffix));
        cp.setContact(new Contact());
        cp.getContact().setEmail("test" + suffix + "@example.com");
        cp.getContact().setFirstName("fName" + suffix);
        cp.getContact().setLastName("lName" + suffix);
        cp.getContact().setPhone("123-456-7890");
        cp.setEndDate(new Date());
        cp.setInstitution(new Institution());
        cp.getInstitution().setName("Test" + Institution.class.getSimpleName() + suffix);
        cp.getInstitution().setHomepage("http://www.example-" + suffix + ".com");
        cp.setName("TesT" + CollectionProtocol.class.getSimpleName() + suffix);
        cp.setStartDate(new Date());
        return cp;
    }

    /**
     * Create a participant.
     * @param suffix unique suffix to add to fields.
     * @return the participant.
     */
    public Participant createParticipant(int suffix) {
        Participant p = new Participant();
        p.setEthnicity(Ethnicity.NON_HISPANIC_OR_LATINO);
        p.setGender(Gender.MALE);
        p.setRaces(new ArrayList<Race>());
        p.getRaces().add(Race.WHITE);
        p.getRaces().add(Race.NATIVE_AMERICAN);
        return p;
    }

}
